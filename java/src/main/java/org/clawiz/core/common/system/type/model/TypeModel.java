/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.model;

import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;

public abstract class TypeModel extends Service {

    @NotInitializeService
    private TypeModel parentModel;

    private TypeFieldModel joinField;

    public TypeModel getParentModel() {
        return parentModel;
    }

    public void setParentModel(TypeModel parentModel) {
        this.parentModel = parentModel;
    }

    public TypeFieldModel getJoinField() {
        return joinField;
    }

    public void setJoinField(TypeFieldModel joinField) {
        this.joinField = joinField;
    }

    abstract public Type getType();

    abstract public TypeRecordIdModel id();

    public TypeFieldModel getByFieldName(String fieldName) {
        return null;
    }

    public TypeFieldModel getByField(TypeField field) {
        if ( field == null ) {
            throwException("Cannot return field model for null field");
        }
        return getByFieldName(field.getName());
    }
}
