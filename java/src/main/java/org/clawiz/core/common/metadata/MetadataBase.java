/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.metadata;

import org.clawiz.core.common.Core;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeList;
import org.clawiz.core.common.metadata.storage.AbstractMetadataStorage;
import org.clawiz.core.common.storage.metadatanode.MetadataNodeObject;
import org.clawiz.core.common.storage.metadatanode.MetadataNodeService;
import org.clawiz.core.common.storage.metadatanodeclass.MetadataNodeClassService;
import org.clawiz.core.common.storage.metadatastorage.MetadataStorageObject;
import org.clawiz.core.common.storage.metadatastorage.MetadataStorageService;
import org.clawiz.core.common.storage.metadatastorageclass.MetadataStorageClassObject;
import org.clawiz.core.common.storage.metadatastorageclass.MetadataStorageClassService;
import org.clawiz.core.common.storage.type.TypeService;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.reflection.ReflectionUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class MetadataBase extends Service {

    private MetadataNodeService         metadataNodeService         ;
    private MetadataNodeClassService    metadataNodeClassService    ;
    private MetadataStorageService      metadataStorageService      ;
    private MetadataStorageClassService metadataStorageClassService ;

    private static ConcurrentHashMap<BigDecimal, MetadataNodeList>   storagesCache     = new ConcurrentHashMap<>();
    private static HashMap<BigDecimal, Boolean>                      storageParseLocks = new HashMap<>();

    private static ConcurrentHashMap<BigDecimal, MetadataNode>       nodesCache        = new ConcurrentHashMap<>();
    private static HashMap<BigDecimal, Boolean>                      nodeParseLocks    = new HashMap<>();

    private static ConcurrentHashMap<BigDecimal, MetadataNodeObject> nodeObjectsCache = new ConcurrentHashMap<>();

    private static Boolean cacheLock = new Boolean(true);

    public void clearCaches() {
        cacheLock = true;
        storagesCache.clear();
        storageParseLocks.clear();

        nodesCache.clear();
        nodeParseLocks.clear();

        nodeObjectsCache.clear();
        cacheLock = false;
    }

    public MetadataNodeList getStorageNodeList(BigDecimal storageId) {

        if ( storageId == null ) {
            throwException("Cannot parse NULL metadata storage");
        }

        // pause on cache clear
        synchronized (cacheLock) {
        }

        MetadataNodeList list = storagesCache.get(storageId);
        if ( list != null ) {
            return list;
        }

        MetadataStorageObject storageObject = metadataStorageService.load(storageId);
        if ( storageObject == null ) {
            throwException("Storage '@' not found in database", new Object[]{storageId});
        }

        if ( storageObject.getStorageClassId() == null ) {
            throwException("Storage type not defined in storage '@'", new Object[]{storageId});
        }

        Boolean locker;
        boolean parseThread = false;
        synchronized (storageParseLocks) {
            locker = storageParseLocks.get(storageId);
            if ( locker == null ) {
                locker = new Boolean(true);
                storageParseLocks.put(storageId, locker);
                parseThread = true;
            }
        }

        // pause thread to guarantee correct start parsing in parse thread
        if ( ! parseThread ) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        synchronized (locker) {
            if ( parseThread ) {
                MetadataStorageClassObject storageClassObject = metadataStorageClassService.load(storageObject.getStorageClassId());
                if ( storageClassObject == null ) {
                    throwException("Metadata storage class '@' not found in database", new Object[]{storageObject.getStorageClassId()});
                }
                AbstractMetadataStorage storage = null;
                try {
                    storage = (AbstractMetadataStorage) getService(Core.getClassByName(storageClassObject.getName()));
                } catch (CoreException e) {
                    throwException("Exception on get storage service of metadata storage class '@'", new Object[]{storageClassObject.getId()}, e);
                }

                MetadataNodeList nodeList = storage.fetchNodeList(storageObject);
                for (Object object : nodeList ) {
                    ((MetadataNode) object).prepare(getSession());
                }
                storagesCache.put(storageId, nodeList);

                storageParseLocks.remove(storageId);
            }
        }

        list = storagesCache.get(storageId);
        return list;
    }


    public MetadataNode getNode(BigDecimal nodeObjectId) {
        if ( nodeObjectId == null ) {
            return null;
        }

        // pause on cache clear
        synchronized (cacheLock) {
        }

        MetadataNode node = nodesCache.get(nodeObjectId);
        if ( node != null ) {
            return node;
        }

        MetadataNodeObject nodeObject = metadataNodeService.load(nodeObjectId);
        if ( nodeObject == null ) {
            throwException("Node '@' not exists in nodes table", new Object[]{nodeObjectId});
        }
        if ( nodeObject.getStorageId() == null ) {
            throwException("Cannot parse metadata node '@' with NULL storage id", new Object[]{nodeObjectId});
        }
        nodeObjectsCache.put(nodeObjectId, nodeObject);

        Boolean locker;
        boolean parseThread = false;
        synchronized (nodeParseLocks) {
            locker = nodeParseLocks.get(nodeObjectId);
            if ( locker == null ) {
                parseThread = true;
                locker = new Boolean(true);
                nodeParseLocks.put(nodeObjectId, locker);
            }
        }


        // pause thread to guarantee correct start parsing in parse thread
        if ( ! parseThread ) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // parse node or wait locker from parse thread
        synchronized (locker) {
            if ( parseThread ) {

                node = null;
                MetadataNodeList nodeList = getStorageNodeList(nodeObject.getStorageId());
                for ( int i=0; i < nodeList.size(); i++) {
                    if ( nodeObject.getName().equalsIgnoreCase(nodeList.get(i).getName())
                            && nodeObject.getPackageName().equals(nodeList.get(i).getPackageName())) {
                        node = nodeList.get(i);
                        break;
                    }
                }
                if ( node == null ) {
                    throwException("Metadata storage '@' not contain node with name '?' of class '@'. Check storage consistence", new Object[]{nodeObject.getStorageId(), nodeObject.getName(), nodeObject.getNodeClassId()});
                }
                node.setNodeObjectId(nodeObjectId);
                node.setPackageName(nodeObject.getPackageName());

                nodesCache.put(nodeObjectId, node);
                nodeParseLocks.remove(nodeObjectId);

                for (Object object : nodeList ) {
                    ((MetadataNode) object).prepareNode(getSession());
                }

            }
        }

        node = nodesCache.get(nodeObjectId);
        return node;
    }

    public <T extends MetadataNode> T getNode(Class<T> nodeClass, String packageName, String name, boolean throwNotFoundError) {

        BigDecimal classId = metadataNodeClassService.nameToId(nodeClass.getName());
        if ( classId == null ) {
            if ( throwNotFoundError ) {
                throwException("Node '?.?' class '?' not found in metadata storages", packageName, name, nodeClass.getName());
            }
        }

        BigDecimal nodeId  = metadataNodeService.nodeClassPackageNameToId(classId, packageName, name);
        if ( nodeId == null ) {
            if ( throwNotFoundError ) {
                throwException("Node '?.?' of class '?' not found in metadata", packageName, name, nodeClass.getName());
            } else {
                return null;
            }
        }

        return (T) getNode(nodeId);
    }

    public <T extends MetadataNode> T getNodeByPath(String path, boolean throwNotFoundError) {

        if ( path == null ) {
            if ( throwNotFoundError ) {
                throwException("Node path NULL not found in metadata storages");
            }
            return null;
        }
        if ( !StringUtils.isLike(path, "name://*")) {
            throwException("Wrong format on node path '?'", path);
        }
        int      colonIndex             = path.indexOf(':', 7);
        if ( colonIndex < 0 ) {
            throwException("Metadata node path not in format 'class:name' : ?", path);
        }
        String   className         = path.substring(7, colonIndex);
        int      leftBracketIndex  = path.indexOf('[', colonIndex);

        String   nodeName          = path.substring(colonIndex + 1, leftBracketIndex > 0 ? leftBracketIndex : path.length());
        String[] tokens            = StringUtils.splitByLastTokenOccurrence(nodeName, "\\.");

        MetadataNode node          = getNode(Core.getClassByName(className), tokens[0], tokens[1], throwNotFoundError);
        if ( node == null ) {
            return null;
        }
        if ( leftBracketIndex < 0) {
            return (T) node;
        }

        while ( leftBracketIndex >= 0 ) {
            int rightBracketIndex = path.indexOf("\"]", leftBracketIndex);
            while (rightBracketIndex > 0 && path.charAt(rightBracketIndex-1) == '\\') {
                rightBracketIndex = path.indexOf("\"]", rightBracketIndex + 1);
            }
            if ( rightBracketIndex < 0 ) {
                throwException("Wrong metadata node path format : cannot find closed bracket at index ? : ?", leftBracketIndex, path);
            }
            String nameValue = path.substring(leftBracketIndex+1, rightBracketIndex+1);
            tokens = StringUtils.splitAndTrim(nameValue, "=");
            Method method = ReflectionUtils.findMethod(node.getClass(), "get" + StringUtils.toUpperFirstChar(tokens[0]));
            Object object = null;
            try {
                object = method.invoke(node);
            } catch (Exception e) {
                throwException("Exception on call getter method for ?.? : ?", node.getClass().getName(), tokens[0], e.getMessage(), e);
            }
            if ( tokens[1].charAt(0) != '"' || tokens[1].charAt(tokens[1].length()-1) != '"') {
                throwException("Wrong metadata node path format : value ? is not '\"value\"' format : ?", leftBracketIndex, path);
            }
            String searchName = tokens[1].substring(1, tokens[1].length()-1);
            if ( MetadataNodeList.class.isAssignableFrom(object.getClass())) {
                node = ((MetadataNodeList) object).get(searchName);
            } else if ( List.class.isAssignableFrom(object.getClass()) ) {
                node = null;
                for(Object listObject : ((List) object)) {
                    if ( ((MetadataNode) listObject).getName().equals(searchName) ) {
                        node = (MetadataNode) listObject;
                        break;
                    }
                }
            } else {
                node = (MetadataNode) object;
            }

            if ( node == null ) {
                if (throwNotFoundError) {
                    throwException("Metadata node name '?' not found in position ? of path ?", nameValue, leftBracketIndex, path);
                } else {
                    return null;
                }
            }

            leftBracketIndex = path.indexOf('[', rightBracketIndex);

        }


        return (T) node;
    }

    public BigDecimal getNodeObjectId(MetadataNode node) {
        if ( node == null ) {
            throwException("Cannot get NULL metadata node");
        }

        BigDecimal classId = metadataNodeClassService.nameToId(node.getClass().getName());
        if ( classId == null ) {
            return null;
        }

        return metadataNodeService.nodeClassPackageNameToId(classId, node.getPackageName(), node.getName());
    }

    public MetadataNodeObject getNodeObject(MetadataNode node) {
        BigDecimal nodeId = getNodeObjectId(node);
        return nodeId != null ? metadataNodeService.load(nodeId) : null;
    }

    public void removeStorageFromCache(BigDecimal storageId) {
        if ( storageId == null ) {
            return;
        }
        synchronized (cacheLock) {
            MetadataNodeList nodeList = storagesCache.get(storageId);
            if ( nodeList == null ) {
                return;
            }
            storagesCache.remove(storageId);

            for (int i=0; i < nodeList.size(); i++) {
                BigDecimal nodeId = nodeList.get(i).getId();
                if ( nodeId != null ) {
                    nodesCache.remove(nodeId );
                    nodeObjectsCache.remove(nodeId);
                }
            }

        }
    }

    public <T extends AbstractMetadataStorage> BigDecimal resolveStorage(Class<T> storageClass, String packageName, String url) {
        if ( storageClass == null ) {
            throwException("Metadata storage class cannot be null");
        }
        if ( packageName == null ) {
            throwException("Metadata storage package cannot be null");
        }
        if ( url == null ) {
            throwException("Metadata storage url cannot be null");
        }
        BigDecimal storageClassId = metadataStorageClassService.nameToId(storageClass.getName(), true);

        return metadataStorageService.storageClassPackageUrlToId(storageClassId, packageName, url.replaceAll("\\\\", "/"), true);
    }

    public void saveNode(MetadataNode node, BigDecimal storageId) {
        if ( node == null ) {
            throwException("Canont save NULL metadata node");
        }
        if ( node.getName() == null ) {
            throwException("Metadata node '?' name cannot be null", new Object[]{node.getClass().getName()});
        }
        if ( node.getPackageName() == null ) {
            throwException("Metadata node '?' package name cannot be null", new Object[]{node.getName()});
        }
        if ( storageId == null ) {
            throwException("Cannot save metadata '?' with null storage id", new Object[]{node.getClass().getName() + "." + node.getName()});
        }
        BigDecimal         nodeClassId = metadataNodeClassService.nameToId(node.getClass().getName(), true);
        MetadataNodeObject nodeObject  = metadataNodeService.load(metadataNodeService.nodeClassPackageNameToId(nodeClassId, node.getPackageName(), node.getName(), true));
        nodeObject.setStorageId(storageId);
        nodeObject.save();
        node.setNodeObjectId(nodeObject.getId());
    }

    public <T extends AbstractMetadataStorage> void saveNode(MetadataNode node, Class<T> storageClass, String url) {

        logDebug("SAVE NODE : " + storageClass.getName() + " " + node.getFullName() + " : " + url);

        saveNode(node, resolveStorage(storageClass, node.getMetadataStoragePackageName(), url));
    }


}
