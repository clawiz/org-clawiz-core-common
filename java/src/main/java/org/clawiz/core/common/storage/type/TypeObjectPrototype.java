package org.clawiz.core.common.storage.type;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class TypeObjectPrototype extends AbstractObject {
    
    /**
    * PackageName
    */
    private java.lang.String packageName;
    
    /**
    * Name
    */
    private java.lang.String name;
    
    /**
    * JavaName
    */
    private java.lang.String javaName;
    
    /**
    * Description
    */
    private java.lang.String description;
    
    /**
    * TableName
    */
    private java.lang.String tableName;
    
    public TypeService service;
    
    /**
    * 
    * @return     PackageName
    */
    public java.lang.String getPackageName() {
        return this.packageName;
    }
    
    /**
    * Set 'PackageName' value
    * 
    * @param      packageName PackageName
    */
    public void setPackageName(java.lang.String packageName) {
        
        if ( packageName != null && packageName.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "Type.PackageName", "250");
        }
        
         this.packageName = packageName;
    }
    
    /**
    * Set 'PackageName' value and return this object
    * 
    * @param      packageName PackageName
    * @return     Type object
    */
    public TypeObjectPrototype withPackageName(java.lang.String packageName) {
        setPackageName(packageName);
        return this;
    }
    
    /**
    * 
    * @return     Name
    */
    public java.lang.String getName() {
        return this.name;
    }
    
    /**
    * Set 'Name' value
    * 
    * @param      name Name
    */
    public void setName(java.lang.String name) {
        
        if ( name != null && name.length() > 128) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "Type.Name", "128");
        }
        
         this.name = name;
    }
    
    /**
    * Set 'Name' value and return this object
    * 
    * @param      name Name
    * @return     Type object
    */
    public TypeObjectPrototype withName(java.lang.String name) {
        setName(name);
        return this;
    }
    
    /**
    * 
    * @return     JavaName
    */
    public java.lang.String getJavaName() {
        return this.javaName;
    }
    
    /**
    * Set 'JavaName' value
    * 
    * @param      javaName JavaName
    */
    public void setJavaName(java.lang.String javaName) {
        
        if ( javaName != null && javaName.length() > 128) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "Type.JavaName", "128");
        }
        
         this.javaName = javaName;
    }
    
    /**
    * Set 'JavaName' value and return this object
    * 
    * @param      javaName JavaName
    * @return     Type object
    */
    public TypeObjectPrototype withJavaName(java.lang.String javaName) {
        setJavaName(javaName);
        return this;
    }
    
    /**
    * 
    * @return     Description
    */
    public java.lang.String getDescription() {
        return this.description;
    }
    
    /**
    * Set 'Description' value
    * 
    * @param      description Description
    */
    public void setDescription(java.lang.String description) {
        
        if ( description != null && description.length() > 1024) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "Type.Description", "1024");
        }
        
         this.description = description;
    }
    
    /**
    * Set 'Description' value and return this object
    * 
    * @param      description Description
    * @return     Type object
    */
    public TypeObjectPrototype withDescription(java.lang.String description) {
        setDescription(description);
        return this;
    }
    
    /**
    * 
    * @return     TableName
    */
    public java.lang.String getTableName() {
        return this.tableName;
    }
    
    /**
    * Set 'TableName' value
    * 
    * @param      tableName TableName
    */
    public void setTableName(java.lang.String tableName) {
        
        if ( tableName != null && tableName.length() > 75) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "Type.TableName", "75");
        }
        
         this.tableName = tableName;
    }
    
    /**
    * Set 'TableName' value and return this object
    * 
    * @param      tableName TableName
    * @return     Type object
    */
    public TypeObjectPrototype withTableName(java.lang.String tableName) {
        setTableName(tableName);
        return this;
    }
    
    public TypeService getService() {
        return this.service;
    }
    
    public void setService(TypeService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(TypeObjectPrototype  target) {
        target.setPackageName(getPackageName());
        target.setName(getName());
        target.setJavaName(getJavaName());
        target.setDescription(getDescription());
        target.setTableName(getTableName());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((TypeObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        service.save((TypeObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(TypeObjectPrototype object) {
        return 
               isObjectsEquals(this.getPackageName(), object.getPackageName() ) 
            && isObjectsEquals(this.getName(), object.getName() ) 
            && isObjectsEquals(this.getJavaName(), object.getJavaName() ) 
            && isObjectsEquals(this.getDescription(), object.getDescription() ) 
            && isObjectsEquals(this.getTableName(), object.getTableName() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getPackageName() != null ? getPackageName().hashCode() : 0);
        result = result * 31 + (getName() != null ? getName().hashCode() : 0);
        result = result * 31 + (getJavaName() != null ? getJavaName().hashCode() : 0);
        result = result * 31 + (getDescription() != null ? getDescription().hashCode() : 0);
        result = result * 31 + (getTableName() != null ? getTableName().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'PackageName' fields
    * 
    * @return     Concatenated string values of key 'toPackageName' fields
    */
    public String toPackageName() {
        return getPackageName() + "," + getName();
    }
    
    /**
    * Prepare concatenated string values of key 'PackageJavaName' fields
    * 
    * @return     Concatenated string values of key 'toPackageJavaName' fields
    */
    public String toPackageJavaName() {
        return getPackageName() + "," + getJavaName();
    }
    
    /**
    * Prepare concatenated string values of key 'TableName' fields
    * 
    * @return     Concatenated string values of key 'toTableName' fields
    */
    public String toTableName() {
        return getTableName();
    }
}
