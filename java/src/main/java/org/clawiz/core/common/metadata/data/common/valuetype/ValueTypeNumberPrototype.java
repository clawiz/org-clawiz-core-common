package org.clawiz.core.common.metadata.data.common.valuetype;

import java.math.BigDecimal;
import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ValueTypeNumberPrototype extends org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType {
    
    @ExchangeAttribute
    private BigDecimal precision;
    
    @ExchangeAttribute
    private BigDecimal scale;
    
    public ValueTypeNumber withName(String value) {
        setName(value);
        return (ValueTypeNumber) this;
    }
    
    public BigDecimal getPrecision() {
        return this.precision;
    }
    
    public void setPrecision(BigDecimal value) {
        this.precision = value;
    }
    
    public ValueTypeNumber withPrecision(BigDecimal value) {
        setPrecision(value);
        return (ValueTypeNumber) this;
    }
    
    public BigDecimal getScale() {
        return this.scale;
    }
    
    public void setScale(BigDecimal value) {
        this.scale = value;
    }
    
    public ValueTypeNumber withScale(BigDecimal value) {
        setScale(value);
        return (ValueTypeNumber) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
