/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.java.component.element;

import org.clawiz.core.common.system.generator.java.component.AbstractJavaClassComponent;
import org.clawiz.core.common.utils.StringUtils;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: abdrashitovta
 * Date: 31.05.13
 * Time: 10?2
 * To change this template use File | Settings | File Templates.
 */
public class JavaVariableElement extends JavaMemberElement {

    boolean isStatic = false;
    boolean isFinal = false;

    String initStatement;

    boolean addGetter;
    boolean addSetter;

    JavaMethodElement getter;
    JavaMethodElement setter;

    ArrayList<JavaAnnotationElement> annotations = new ArrayList<>();

    public boolean isStatic() {
        return isStatic;
    }

    public void setStatic(boolean aStatic) {
        isStatic = aStatic;
    }

    public JavaVariableElement withStatic(boolean aStatic) {
        isStatic = aStatic;
        return this;
    }

    public JavaVariableElement withFinal(boolean value) {
        isFinal = value;
        return this;
    }

    public JavaVariableElement withAccessLevel(AccessLevel accessLevel) {
        super.setAccessLevel(accessLevel);
        return this;
    }

    public boolean isFinal() {
        return isFinal;
    }

    public void setFinal(boolean aFinal) {
        isFinal = aFinal;
    }

    public String getInitStatement() {
        return initStatement;
    }

    public void setInitStatement(String initStatement) {
        this.initStatement = initStatement;
    }

    public boolean isAddGetter() {
        return addGetter;
    }

    public void setAddGetter(boolean addGetter) {
        this.addGetter = addGetter;
    }

    public boolean isAddSetter() {
        return addSetter;
    }

    public void setAddSetter(boolean addSetter) {
        this.addSetter = addSetter;
    }

    public ArrayList<JavaAnnotationElement> getAnnotations() {
        return annotations;
    }

    @SuppressWarnings("Duplicates")
    public JavaAnnotationElement addAnnotation(Class annotationClass) {
        JavaAnnotationElement annotationElement = addElement(JavaAnnotationElement.class);
        annotationElement.setAnnotationClass(annotationClass);

        if ( getComponent() instanceof AbstractJavaClassComponent ) {
            ((AbstractJavaClassComponent) getComponent()).addImport(annotationClass);
        }

        annotationElement.setLpad(getLpad());
        annotations.add(annotationElement);
        return annotationElement;
    }

    private JavaMethodElement _addMethod(String methodType, String methodName) {
        if ( getParent() != null ) {
            throwException("Cannot add '?' for '?' to element with not null parent '?'", new Object[]{methodName, getName(), getParent().getClass().getName()});
        }
        if ( !  (getComponent() instanceof AbstractJavaClassComponent )) {
            throwException("Cannot add '?' for '?' to not java class component '?'", new Object[]{methodName, getName(), getComponent().getClass().getName()});
        }
        JavaMethodElement method = getComponent().addElement(JavaMethodElement.class);
        method.setName(methodName);
        method.setType(methodType);

        return method;
    }

    public String getGetterName() {
        return (
                (
                        getType().equalsIgnoreCase("boolean") || getType().equalsIgnoreCase("java.lang.Boolean")
                ) ? "is" : "get"
               )
                + StringUtils.toUpperFirstChar(getName());
    }

    public JavaMethodElement addGetter() {
        getter = _addMethod(getType(), getGetterName());
        getter.addText("return this." + getName() + ";");
        return getter;
    }

    public String getSetterName() {
        return "set" + StringUtils.toUpperFirstChar(getName());
    }

    public JavaMethodElement addSetter() {
        setter = _addMethod(null, getSetterName());
        setter.addParameter(getType(), "value");
        setter.addText("this." + getName() + " = value;");
        return setter;
    }

    @Override
    public void process() {
        super.process();
    }

    @Override
    public void write() {

        String str = getAccessLevel() + " "
                + (isStatic ? "static " : "")
                + (isFinal ? "final " : "")
                + type + " "
                + getName()
                + (initStatement != null ? " = " + initStatement : "")
                + ";";
        pln("");

        //noinspection Duplicates
        if ( javaDoc.size() > 0 ) {
            pln("/**");
            for( String doc : javaDoc ) {
                String[] tokens = StringUtils.splitAndTrim(doc, "\\n");
                for ( String s : tokens) {
                    pln("* " + s);
                }
            }
            pln("*/");
        }

        for (JavaAnnotationElement annotation : getAnnotations() ) {
            annotation.write();
        }
        pln(str);

    }
}
