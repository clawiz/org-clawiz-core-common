package org.clawiz.core.common.storage.servicehandler;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;

public class ServiceHandlerServicePrototype extends AbstractTypeService<ServiceHandlerObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.core.common.storage", "ServiceHandler");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.ServiceHandler"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      serviceHandler Checked object
    */
    public void check(ServiceHandlerObject serviceHandler) {
        
        if ( serviceHandler == null ) {
            throwException("Cannot check null ?", new Object[]{"ServiceHandlerObject"});
        }
        
        serviceHandler.fillDefaults();
        
        
        if ( nameToId(serviceHandler.getServiceName(), serviceHandler.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "ServiceHandler", serviceHandler.toName() });
        }
        
    }
    
    /**
    * Create new ServiceHandlerObject instance and fill default values
    * 
    * @return     Created object
    */
    public ServiceHandlerObject create() {
        
        ServiceHandlerObject serviceHandler = new ServiceHandlerObject();
        serviceHandler.setService((ServiceHandlerService) this);
        
        serviceHandler.fillDefaults();
        
        return serviceHandler;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public ServiceHandlerObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select service_name, description from cw_core_service_handlers where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"ServiceHandler", id});
        }
        
        ServiceHandlerObject result = new ServiceHandlerObject();
        
        result.setService((ServiceHandlerService) this);
        result.setId(id);
        result.setServiceName(statement.getString(1));
        result.setDescription(statement.getString(2));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of ServiceHandlerObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public ServiceHandlerList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of ServiceHandlerObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public ServiceHandlerList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        ServiceHandlerList result = new ServiceHandlerList();
        
        
        Statement statement = executeQuery("select id, service_name, description from cw_core_service_handlers"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            ServiceHandlerObject object = new ServiceHandlerObject();
        
            object.setService((ServiceHandlerService) this);
            object.setId(statement.getBigDecimal(1));
            object.setServiceName(statement.getString(2));
            object.setDescription(statement.getString(3));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'Name' fields
    * 
    * @param      serviceName ServiceName
    * @return     Id of found record or null
    */
    public BigDecimal nameToId(java.lang.String serviceName) {
        return nameToId(serviceName, null);
    }
    
    /**
    * Find id of record by key 'Name' fields with id not equal skipId
    * 
    * @param      serviceName ServiceName
    * @param      skipId      Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal nameToId(java.lang.String serviceName, BigDecimal skipId) {
        
        if ( serviceName == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_service_handlers where upper_service_name = ?", serviceName.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_service_handlers where upper_service_name = ? and id != ?", serviceName.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'Name' fields or create new record with values set to given parameters
    * 
    * @param      serviceName         ServiceName
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal nameToId(java.lang.String serviceName, boolean createNewIfNotFound) {
        
        BigDecimal id = nameToId(serviceName, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        ServiceHandlerObject object = create();
        object.setServiceName(serviceName);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'Name' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select service_name from cw_core_service_handlers where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        ServiceHandlerObject object = new ServiceHandlerObject();
        object.setService((ServiceHandlerService)this);
        object.setId(id);
        object.setServiceName(statement.getString(1));
        
        statement.close();
        
        return object.toName();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToName(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      serviceHandlerObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(ServiceHandlerObject serviceHandlerObject) {
        return serviceHandlerObject.toName();
    }
    
    protected void saveAudit(TransactionAction transactionAction, ServiceHandlerObject oldServiceHandlerObject, ServiceHandlerObject newServiceHandlerObject) {
        
        ServiceHandlerObject o = oldServiceHandlerObject != null ? oldServiceHandlerObject : new ServiceHandlerObject();
        ServiceHandlerObject n = newServiceHandlerObject != null ? newServiceHandlerObject : new ServiceHandlerObject();
        
        
        executeUpdate("insert into a_cw_core_service_handlers (scn, action_type, id , o_service_name, o_upper_service_name, o_description, n_service_name, n_upper_service_name, n_description) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getServiceName(), o.getServiceName() != null ? o.getServiceName().toUpperCase() : null, o.getDescription(), n.getServiceName(), n.getServiceName() != null ? n.getServiceName().toUpperCase() : null, n.getDescription());
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      serviceHandler Saved object
    */
    public void save(ServiceHandlerObject serviceHandler) {
        
        if ( serviceHandler == null ) {
            throwException("Cannot save NULL ?", new Object[]{"ServiceHandlerObject"});
        }
        
        TransactionAction transactionAction;
        ServiceHandlerObject oldServiceHandler;
        if ( serviceHandler.getService() == null ) {
            serviceHandler.setService((ServiceHandlerService)this);
        }
        
        check(serviceHandler);
        
        if ( serviceHandler.getId() == null ) {
        
            serviceHandler.setId(getObjectService().createObject(getTypeId()));
        
            oldServiceHandler = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, serviceHandler.getId());
        
            executeUpdate("insert into cw_core_service_handlers" 
                          + "( id, service_name, upper_service_name, description ) "
                          + "values"
                          + "(?, ?, ?, ?)",
                          serviceHandler.getId(), serviceHandler.getServiceName(), ( serviceHandler.getServiceName() != null ? serviceHandler.getServiceName().toUpperCase() : null ), serviceHandler.getDescription() );
        
        } else {
        
            oldServiceHandler = load(serviceHandler.getId());
            if ( oldServiceHandler.equals(serviceHandler) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, serviceHandler.getId());
        
            executeUpdate("update cw_core_service_handlers set "
                          + "service_name = ?, upper_service_name = ?, description = ? "
                          + "where id = ?",
                          serviceHandler.getServiceName(), ( serviceHandler.getServiceName() != null ? serviceHandler.getServiceName().toUpperCase() : null ), serviceHandler.getDescription(), serviceHandler.getId() );
        
        }
        
        saveAudit(transactionAction, oldServiceHandler, serviceHandler);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        ServiceHandlerObject oldServiceHandlerObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldServiceHandlerObject, null);
        
        getObjectService().deleteObject(id);
        
        executeUpdate("delete from cw_core_service_handlers where id = ?", id);
        
    }
}
