/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator.component.service.element.load;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.generator.component.service.element.AbstractTypeServiceMethodElement;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;

public class TypeServiceLoadOrderedListMethodElement extends AbstractTypeServiceMethodElement {

    @Override
    public void process() {
        super.process();

        setName("loadOrderedList");

        setType(getDataObjectListClassName(), "List of found objects");

        addParameter("String",   "whereClause",   "Where part of SQL statement");
        addParameter("String",   "orderByClause", "Order by part of SQL statement");
        addParameter("Object...", "parameters",    "Where parameter values");

        addJavaDoc("Load list of " + getDataObjectClassName() + " from database");


        addText("");
        String sql    = "select id";
        for (TypeField field : getFields() ) {
            if ( ! (field.getValueType() instanceof ValueTypeList) ) {
                sql += ", " + field.getColumnName();
            }
        }

        addText("");
        addText(getDataObjectListClassName() + " result = new " + getDataObjectListClassName() + "();");
        addText("");
        addText("");
        addText("Statement statement = executeQuery(\"" + sql + " from " + getTableName() + "\"");
        addText("                                           + (whereClause   != null ? \" where \" + whereClause : \"\")");
        addText("                                           + (orderByClause != null ? \" order by \" + orderByClause : \"\")");
        addText("                                           , parameters);");
        addText("while ( statement.next() ) {");
        addText("    " + getDataObjectClassName() + " object = new " + getDataObjectClassName() + "();");
        addText("");
        addText("    object.setService((" + getServiceClassName() + ") this);");
        addText("    object.setId(statement.getBigDecimal(1));");

        int index = 2;
        for (TypeField field : getFields() ) {
            if ( ! (field.getValueType() instanceof ValueTypeList) ) {
                addText("    object." + field.getSetMethodName() + "(" + getFieldStatementGetter(field, index) + ");");
                index++;
            }
        }
        addText("");
        addText("    result.add(object);");
        addText("}");
        addText("statement.close();");
        addText("");
        addText("return result;");

    }
}
