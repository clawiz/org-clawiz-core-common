/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator.component.service.element.load;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.generator.component.service.element.AbstractTypeServiceMethodElement;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;

public class TypeServiceLoadMethodElement extends AbstractTypeServiceMethodElement {


    @Override
    public void process() {
        super.process();

        setName("load");
        setType(getDataObjectClassName(), "Loaded object");
        addParameter("BigDecimal", "id", "Id of loaded record");

        addJavaDoc("Load object from database by record id");

        addText("");

        String sql    = "select";
        String prefix = " ";
        for (TypeField field : getFields() ) {
            if ( ! (field.getValueType() instanceof ValueTypeList) ) {
                sql += prefix + field.getColumnName();
                prefix = ", ";
            }
        }

        sql += " from " + getTableName() + " where id = ?";
        addText("Statement statement = executeQuery(\"" + sql + "\", id);");
        addText("if ( ! statement.next() ) { ");
        addText("    statement.close();");
        addText("    throwException(\"? with id ? not found in database\", new Object[]{\"" + getTypeName() + "\", id});");
        addText("}");

        addText("");
        addText(getDataObjectClassName() + " result = new " + getDataObjectClassName() + "();");
        addText("");

        addText("result.setService((" + getServiceClassName() + ") this);");
        addText("result.setId(id);");
        int index = 1;
        for (TypeField field : getFields() ) {
            if ( ! (field.getValueType() instanceof ValueTypeList) ) {
                addText("result." + field.getSetMethodName() + "(" + getFieldStatementGetter(field, index) + ");");
                index++;
            }
        }

        addText("");
        addText("statement.close();");
        addText("");
        addText("return result;");

    }
}
