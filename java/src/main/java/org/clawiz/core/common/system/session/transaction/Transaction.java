/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.session.transaction;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: abdrashitovta
 * Date: 29.07.13
 * Time: 17?3
 * To change this template use File | Settings | File Templates.
 */
public class Transaction {

    String key;
    Session session;
    Date startTime;

    public class TransactionObject {
        AbstractObject object;
        Method         saveMethod;
    }

    ArrayList<BigDecimal>        lockedObjects = new ArrayList<>();
    ArrayList<TransactionObject> newObjects    = new ArrayList<>();
    ArrayList<TransactionAction> actions       = new ArrayList<>();

    public Transaction(String key, Session session) {
        this.key = key;
        this.session = session;
        this.startTime = new Date();
    }

    public String getKey() {
        return key;
    }

    public Session getSession() {
        return session;
    }

    public BigDecimal getUserId()  {
        return session.getUserId();
    }

    public Date getStartTime() {
        return startTime;
    }

    public ArrayList<BigDecimal> getLockedObjects() {
        return lockedObjects;
    }

    public void registerObject(AbstractObject object, Method saveMethod) {
        TransactionObject obj = new TransactionObject();
        newObjects.add(obj);
    }

    public void addAction(TransactionAction action) {
        actions.add(action);
    }
}
