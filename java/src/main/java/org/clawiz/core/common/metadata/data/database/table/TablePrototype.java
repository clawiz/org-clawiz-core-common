package org.clawiz.core.common.metadata.data.database.table;

import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class TablePrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeElement
    private org.clawiz.core.common.metadata.data.database.table.column.TableColumnList columns = new org.clawiz.core.common.metadata.data.database.table.column.TableColumnList();
    
    public Table withName(String value) {
        setName(value);
        return (Table) this;
    }
    
    public org.clawiz.core.common.metadata.data.database.table.column.TableColumnList getColumns() {
        return this.columns;
    }
    
    public Table withColumn(org.clawiz.core.common.metadata.data.database.table.column.TableColumn value) {
        getColumns().add(value);
        return (Table) this;
    }
    
    public <T extends org.clawiz.core.common.metadata.data.database.table.column.TableColumn> T createColumn(Class<T> nodeClass) {
        org.clawiz.core.common.metadata.data.database.table.column.TableColumn value = createChildNode(nodeClass, "columns");
        getColumns().add(value);
        return (T) value;
    }
    
    public org.clawiz.core.common.metadata.data.database.table.column.TableColumn createColumn() {
        return createColumn(org.clawiz.core.common.metadata.data.database.table.column.TableColumn.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        for (MetadataNode node : getColumns()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        for (MetadataNode node : getColumns()) {
            references.add(node);
        }
        
    }
}
