package org.clawiz.core.common.storage.remotenodeguid;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;
import org.clawiz.core.common.system.service.NotInitializeService;

public class RemoteNodeGuidServicePrototype extends AbstractTypeService<RemoteNodeGuidObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.core.common.storage", "RemoteNodeGuid");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.RemoteNodeGuid"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      remoteNodeGuid Checked object
    */
    public void check(RemoteNodeGuidObject remoteNodeGuid) {
        
        if ( remoteNodeGuid == null ) {
            throwException("Cannot check null ?", new Object[]{"RemoteNodeGuidObject"});
        }
        
        remoteNodeGuid.fillDefaults();
        
        
        if ( remoteNodeObjectToId(remoteNodeGuid.getRemoteNodeId(), remoteNodeGuid.getObjectId(), remoteNodeGuid.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "RemoteNodeGuid", remoteNodeGuid.toRemoteNodeObject() });
        }
        
        if ( remoteNodeGuidToId(remoteNodeGuid.getRemoteNodeId(), remoteNodeGuid.getNodeGuid(), remoteNodeGuid.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "RemoteNodeGuid", remoteNodeGuid.toRemoteNodeGuid() });
        }
        
    }
    
    /**
    * Create new RemoteNodeGuidObject instance and fill default values
    * 
    * @return     Created object
    */
    public RemoteNodeGuidObject create() {
        
        RemoteNodeGuidObject remoteNodeGuid = new RemoteNodeGuidObject();
        remoteNodeGuid.setService((RemoteNodeGuidService) this);
        
        remoteNodeGuid.fillDefaults();
        
        return remoteNodeGuid;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public RemoteNodeGuidObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select remote_node_id, object_id, node_guid from cw_core_remote_node_guids where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"RemoteNodeGuid", id});
        }
        
        RemoteNodeGuidObject result = new RemoteNodeGuidObject();
        
        result.setService((RemoteNodeGuidService) this);
        result.setId(id);
        result.setRemoteNodeId(statement.getBigDecimal(1));
        result.setObjectId(statement.getBigDecimal(2));
        result.setNodeGuid(statement.getString(3));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of RemoteNodeGuidObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public RemoteNodeGuidList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of RemoteNodeGuidObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public RemoteNodeGuidList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        RemoteNodeGuidList result = new RemoteNodeGuidList();
        
        
        Statement statement = executeQuery("select id, remote_node_id, object_id, node_guid from cw_core_remote_node_guids"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            RemoteNodeGuidObject object = new RemoteNodeGuidObject();
        
            object.setService((RemoteNodeGuidService) this);
            object.setId(statement.getBigDecimal(1));
            object.setRemoteNodeId(statement.getBigDecimal(2));
            object.setObjectId(statement.getBigDecimal(3));
            object.setNodeGuid(statement.getString(4));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'RemoteNodeObject' fields
    * 
    * @param      remoteNodeId RemoteNode
    * @param      objectId     Object
    * @return     Id of found record or null
    */
    public BigDecimal remoteNodeObjectToId(java.math.BigDecimal remoteNodeId, java.math.BigDecimal objectId) {
        return remoteNodeObjectToId(remoteNodeId, objectId, null);
    }
    
    /**
    * Find id of record by key 'RemoteNodeObject' fields with id not equal skipId
    * 
    * @param      remoteNodeId RemoteNode
    * @param      objectId     Object
    * @param      skipId       Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal remoteNodeObjectToId(java.math.BigDecimal remoteNodeId, java.math.BigDecimal objectId, BigDecimal skipId) {
        
        if ( remoteNodeId == null || objectId == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_remote_node_guids where remote_node_id = ? and object_id = ?", remoteNodeId, objectId);
        } else {
            return executeQueryBigDecimal("select id from cw_core_remote_node_guids where remote_node_id = ? and object_id = ? and id != ?", remoteNodeId, objectId, skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'RemoteNodeObject' fields or create new record with values set to given parameters
    * 
    * @param      remoteNodeId        RemoteNode
    * @param      objectId            Object
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal remoteNodeObjectToId(java.math.BigDecimal remoteNodeId, java.math.BigDecimal objectId, boolean createNewIfNotFound) {
        
        BigDecimal id = remoteNodeObjectToId(remoteNodeId, objectId, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        RemoteNodeGuidObject object = create();
        object.setRemoteNodeId(remoteNodeId);
        object.setObjectId(objectId);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'RemoteNodeObject' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToRemoteNodeObject(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select remote_node_id, object_id from cw_core_remote_node_guids where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        RemoteNodeGuidObject object = new RemoteNodeGuidObject();
        object.setService((RemoteNodeGuidService)this);
        object.setId(id);
        object.setRemoteNodeId(statement.getBigDecimal(1));
        object.setObjectId(statement.getBigDecimal(2));
        
        statement.close();
        
        return object.toRemoteNodeObject();
    }
    
    /**
    * Find id of record by key 'RemoteNodeGuid' fields
    * 
    * @param      remoteNodeId RemoteNode
    * @param      nodeGuid     NodeGuid
    * @return     Id of found record or null
    */
    public BigDecimal remoteNodeGuidToId(java.math.BigDecimal remoteNodeId, java.lang.String nodeGuid) {
        return remoteNodeGuidToId(remoteNodeId, nodeGuid, null);
    }
    
    /**
    * Find id of record by key 'RemoteNodeGuid' fields with id not equal skipId
    * 
    * @param      remoteNodeId RemoteNode
    * @param      nodeGuid     NodeGuid
    * @param      skipId       Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal remoteNodeGuidToId(java.math.BigDecimal remoteNodeId, java.lang.String nodeGuid, BigDecimal skipId) {
        
        if ( remoteNodeId == null || nodeGuid == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_remote_node_guids where remote_node_id = ? and upper_node_guid = ?", remoteNodeId, nodeGuid.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_remote_node_guids where remote_node_id = ? and upper_node_guid = ? and id != ?", remoteNodeId, nodeGuid.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'RemoteNodeGuid' fields or create new record with values set to given parameters
    * 
    * @param      remoteNodeId        RemoteNode
    * @param      nodeGuid            NodeGuid
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal remoteNodeGuidToId(java.math.BigDecimal remoteNodeId, java.lang.String nodeGuid, boolean createNewIfNotFound) {
        
        BigDecimal id = remoteNodeGuidToId(remoteNodeId, nodeGuid, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        RemoteNodeGuidObject object = create();
        object.setRemoteNodeId(remoteNodeId);
        object.setNodeGuid(nodeGuid);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'RemoteNodeGuid' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToRemoteNodeGuid(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select remote_node_id, node_guid from cw_core_remote_node_guids where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        RemoteNodeGuidObject object = new RemoteNodeGuidObject();
        object.setService((RemoteNodeGuidService)this);
        object.setId(id);
        object.setRemoteNodeId(statement.getBigDecimal(1));
        object.setNodeGuid(statement.getString(2));
        
        statement.close();
        
        return object.toRemoteNodeGuid();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToRemoteNodeObject(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      remoteNodeGuidObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(RemoteNodeGuidObject remoteNodeGuidObject) {
        return remoteNodeGuidObject.toRemoteNodeObject();
    }
    
    protected void saveAudit(TransactionAction transactionAction, RemoteNodeGuidObject oldRemoteNodeGuidObject, RemoteNodeGuidObject newRemoteNodeGuidObject) {
        
        RemoteNodeGuidObject o = oldRemoteNodeGuidObject != null ? oldRemoteNodeGuidObject : new RemoteNodeGuidObject();
        RemoteNodeGuidObject n = newRemoteNodeGuidObject != null ? newRemoteNodeGuidObject : new RemoteNodeGuidObject();
        
        
        executeUpdate("insert into a_cw_core_remote_node_guids (scn, action_type, id , o_remote_node_id, o_object_id, o_node_guid, o_upper_node_guid, n_remote_node_id, n_object_id, n_node_guid, n_upper_node_guid) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getRemoteNodeId(), o.getObjectId(), o.getNodeGuid(), o.getNodeGuid() != null ? o.getNodeGuid().toUpperCase() : null, n.getRemoteNodeId(), n.getObjectId(), n.getNodeGuid(), n.getNodeGuid() != null ? n.getNodeGuid().toUpperCase() : null);
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      remoteNodeGuid Saved object
    */
    public void save(RemoteNodeGuidObject remoteNodeGuid) {
        
        if ( remoteNodeGuid == null ) {
            throwException("Cannot save NULL ?", new Object[]{"RemoteNodeGuidObject"});
        }
        
        TransactionAction transactionAction;
        RemoteNodeGuidObject oldRemoteNodeGuid;
        if ( remoteNodeGuid.getService() == null ) {
            remoteNodeGuid.setService((RemoteNodeGuidService)this);
        }
        
        check(remoteNodeGuid);
        
        if ( remoteNodeGuid.getId() == null ) {
        
            remoteNodeGuid.setId(getObjectService().createObject(getTypeId()));
        
            oldRemoteNodeGuid = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, remoteNodeGuid.getId());
        
            executeUpdate("insert into cw_core_remote_node_guids" 
                          + "( id, remote_node_id, object_id, node_guid, upper_node_guid ) "
                          + "values"
                          + "(?, ?, ?, ?, ?)",
                          remoteNodeGuid.getId(), remoteNodeGuid.getRemoteNodeId(), remoteNodeGuid.getObjectId(), remoteNodeGuid.getNodeGuid(), ( remoteNodeGuid.getNodeGuid() != null ? remoteNodeGuid.getNodeGuid().toUpperCase() : null ) );
        
            if ( remoteNodeGuid.getRemoteNodeId() != null ) { getObjectService().setLink(remoteNodeGuid.getRemoteNodeId(), remoteNodeGuid.getId()); }
            if ( remoteNodeGuid.getObjectId() != null ) { getObjectService().setLink(remoteNodeGuid.getObjectId(), remoteNodeGuid.getId()); }
        
        } else {
        
            oldRemoteNodeGuid = load(remoteNodeGuid.getId());
            if ( oldRemoteNodeGuid.equals(remoteNodeGuid) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, remoteNodeGuid.getId());
        
            executeUpdate("update cw_core_remote_node_guids set "
                          + "remote_node_id = ?, object_id = ?, node_guid = ?, upper_node_guid = ? "
                          + "where id = ?",
                          remoteNodeGuid.getRemoteNodeId(), remoteNodeGuid.getObjectId(), remoteNodeGuid.getNodeGuid(), ( remoteNodeGuid.getNodeGuid() != null ? remoteNodeGuid.getNodeGuid().toUpperCase() : null ), remoteNodeGuid.getId() );
        
            getObjectService().changeLinkParent(oldRemoteNodeGuid.getRemoteNodeId(), remoteNodeGuid.getRemoteNodeId(), remoteNodeGuid.getId());
            getObjectService().changeLinkParent(oldRemoteNodeGuid.getObjectId(), remoteNodeGuid.getObjectId(), remoteNodeGuid.getId());
        
        }
        
        saveAudit(transactionAction, oldRemoteNodeGuid, remoteNodeGuid);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        RemoteNodeGuidObject oldRemoteNodeGuidObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldRemoteNodeGuidObject, null);
        
        getObjectService().deleteObject(id);
        
        if (oldRemoteNodeGuidObject.getRemoteNodeId() != null) { getObjectService().deleteLink(oldRemoteNodeGuidObject.getRemoteNodeId(), id); }
        if (oldRemoteNodeGuidObject.getObjectId() != null) { getObjectService().deleteLink(oldRemoteNodeGuidObject.getObjectId(), id); }
        
        executeUpdate("delete from cw_core_remote_node_guids where id = ?", id);
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.remotenode.RemoteNodeService remoteNodeService;
    
    public org.clawiz.core.common.storage.remotenode.RemoteNodeService getRemoteNodeService() {
        if ( remoteNodeService == null ) {
            remoteNodeService = getService(org.clawiz.core.common.storage.remotenode.RemoteNodeService.class);
        }
        return remoteNodeService;
    }
    
    public org.clawiz.core.common.storage.remotenode.RemoteNodeObject createRemoteNode() {
        return getRemoteNodeService().create();
    }
    
    public org.clawiz.core.common.storage.remotenode.RemoteNodeObject loadRemoteNode(BigDecimal id) {
        return getRemoteNodeService().load(id);
    }
}
