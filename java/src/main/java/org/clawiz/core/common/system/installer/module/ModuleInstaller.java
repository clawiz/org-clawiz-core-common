/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.installer.module;

import org.clawiz.core.common.storage.module.ModuleObject;
import org.clawiz.core.common.storage.module.ModuleService;
import org.clawiz.core.common.system.module.ModuleRequire;
import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.core.common.system.installer.AbstractInstaller;
import org.clawiz.core.common.system.installer.script.xml.XMLScriptInstaller;
import org.clawiz.core.common.system.module.Module;
import org.clawiz.core.common.system.module.ModuleDefinition;
import org.clawiz.core.common.system.service.extension.ServiceExtensionManager;
import org.clawiz.core.common.utils.file.FileUtils;

import java.math.BigDecimal;
import java.util.HashMap;

public class ModuleInstaller extends AbstractInstaller {

    @NotInitializeService
    Module               module;

    FileUtils            fileUtils;

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    HashMap<String, String> includedModuleInstallers = new HashMap<>();

    protected String getModuleInstallResourcesPath(Module module) {
        return  "/" + module.getDefinition().getPackage().replaceAll("\\.", "/") + "/install/";
    }

    protected void fillIncludeModuleInstallers(Class moduleClass) {
        Module module = (Module) getService(moduleClass);

        String include = getModuleInstallResourcesPath(module) + "module-installers.xml";
        includedModuleInstallers.put(include, include);

        for (ModuleRequire require : module.getDefinition().getRequires()) {
            fillIncludeModuleInstallers(require.getModuleClass());
        }
    }

    private static boolean protectedMode = false;
    public static boolean isProtectedMode() {
        return protectedMode;
    }

    private ModuleInstaller getParentModuleInstaller(AbstractInstaller parent) {
        if ( parent == null ) {
            return null;
        }
        if ( parent instanceof ModuleInstaller ) {
            return (ModuleInstaller) parent;
        }
        return getParentModuleInstaller(parent.getParent());
    }

    private ModuleInstaller _parentModuleInstaller;

    @Override
    public void process() {

        _parentModuleInstaller = getParentModuleInstaller(getParent());

        protectedMode = true;
        ServiceExtensionManager.clearServiceExtensionConfigs();
        protectedMode = false;
        ServiceExtensionManager.registerServiceExtensions(getModule().getServiceExtensionConfigList());

        String installScriptFileName = getModuleInstallResourcesPath(getModule()) + "install.xml";

        XMLScriptInstaller installer = addInstaller(XMLScriptInstaller.class);
        installer.setPackageName(getModule().getDefinition().getPackage());
        installer.setScriptFileName(installScriptFileName);

        fillIncludeModuleInstallers(getModule().getClass());

        for (String script : includedModuleInstallers.values()) {
            if (fileUtils.fileExists(script, true)) {
                installer.addInstaller(XMLScriptInstaller.class).setScriptFileName(script);
            }
        }
    }

    @Override
    public void done() {
        super.done();

        ModuleDefinition moduleDefinition = getModule().getDefinition();
        String moduleClassName            = getModule().getClass().getName();

        // do not put in class variables - produce exception on new core installation
        ModuleService        moduleService = getService(ModuleService.class);

        BigDecimal moduleId               = moduleService.moduleClassNameToId(moduleClassName);

        ModuleObject moduleObject         = moduleId != null ? moduleService.load(moduleId) : moduleService.create();

        moduleObject.setPackageName(moduleDefinition.getPackage());
        moduleObject.setModuleVersion(moduleDefinition.getVersion() != null ? moduleDefinition.getVersion().toString() : null);
        moduleObject.setModuleClassName(moduleClassName);

        moduleObject.save();

        if ( _parentModuleInstaller != null ) {
            protectedMode = true;
            ServiceExtensionManager.clearServiceExtensionConfigs();
            protectedMode = false;
            ServiceExtensionManager.registerServiceExtensions(_parentModuleInstaller.getModule().getServiceExtensionConfigList());
        }

    }

}
