package org.clawiz.core.common.storage.remotenode;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;
import org.clawiz.core.common.system.service.NotInitializeService;

public class RemoteNodeServicePrototype extends AbstractTypeService<RemoteNodeObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.core.common.storage", "RemoteNode");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.RemoteNode"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      remoteNode Checked object
    */
    public void check(RemoteNodeObject remoteNode) {
        
        if ( remoteNode == null ) {
            throwException("Cannot check null ?", new Object[]{"RemoteNodeObject"});
        }
        
        remoteNode.fillDefaults();
        
        
        if ( nameToId(remoteNode.getName(), remoteNode.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "RemoteNode", remoteNode.toName() });
        }
        
    }
    
    /**
    * Create new RemoteNodeObject instance and fill default values
    * 
    * @return     Created object
    */
    public RemoteNodeObject create() {
        
        RemoteNodeObject remoteNode = new RemoteNodeObject();
        remoteNode.setService((RemoteNodeService) this);
        
        remoteNode.fillDefaults();
        
        return remoteNode;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public RemoteNodeObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select name, password, password_hash, previous_password, last_received_scn, registered_user_id from cw_core_remote_nodes where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"RemoteNode", id});
        }
        
        RemoteNodeObject result = new RemoteNodeObject();
        
        result.setService((RemoteNodeService) this);
        result.setId(id);
        result.setName(statement.getString(1));
        result.setPassword(statement.getString(2));
        result.setPasswordHash(statement.getString(3));
        result.setPreviousPassword(statement.getString(4));
        result.setLastReceivedScn(statement.getBigDecimal(5));
        result.setRegisteredUserId(statement.getBigDecimal(6));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of RemoteNodeObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public RemoteNodeList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of RemoteNodeObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public RemoteNodeList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        RemoteNodeList result = new RemoteNodeList();
        
        
        Statement statement = executeQuery("select id, name, password, password_hash, previous_password, last_received_scn, registered_user_id from cw_core_remote_nodes"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            RemoteNodeObject object = new RemoteNodeObject();
        
            object.setService((RemoteNodeService) this);
            object.setId(statement.getBigDecimal(1));
            object.setName(statement.getString(2));
            object.setPassword(statement.getString(3));
            object.setPasswordHash(statement.getString(4));
            object.setPreviousPassword(statement.getString(5));
            object.setLastReceivedScn(statement.getBigDecimal(6));
            object.setRegisteredUserId(statement.getBigDecimal(7));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'Name' fields
    * 
    * @param      name Name
    * @return     Id of found record or null
    */
    public BigDecimal nameToId(java.lang.String name) {
        return nameToId(name, null);
    }
    
    /**
    * Find id of record by key 'Name' fields with id not equal skipId
    * 
    * @param      name   Name
    * @param      skipId Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal nameToId(java.lang.String name, BigDecimal skipId) {
        
        if ( name == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_remote_nodes where upper_name = ?", name.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_remote_nodes where upper_name = ? and id != ?", name.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'Name' fields or create new record with values set to given parameters
    * 
    * @param      name                Name
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal nameToId(java.lang.String name, boolean createNewIfNotFound) {
        
        BigDecimal id = nameToId(name, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        RemoteNodeObject object = create();
        object.setName(name);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'Name' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select name from cw_core_remote_nodes where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        RemoteNodeObject object = new RemoteNodeObject();
        object.setService((RemoteNodeService)this);
        object.setId(id);
        object.setName(statement.getString(1));
        
        statement.close();
        
        return object.toName();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToName(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      remoteNodeObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(RemoteNodeObject remoteNodeObject) {
        return remoteNodeObject.toName();
    }
    
    protected void saveAudit(TransactionAction transactionAction, RemoteNodeObject oldRemoteNodeObject, RemoteNodeObject newRemoteNodeObject) {
        
        RemoteNodeObject o = oldRemoteNodeObject != null ? oldRemoteNodeObject : new RemoteNodeObject();
        RemoteNodeObject n = newRemoteNodeObject != null ? newRemoteNodeObject : new RemoteNodeObject();
        
        
        executeUpdate("insert into a_cw_core_remote_nodes (scn, action_type, id , o_name, o_upper_name, o_password, o_password_hash, o_previous_password, o_last_received_scn, o_registered_user_id, n_name, n_upper_name, n_password, n_password_hash, n_previous_password, n_last_received_scn, n_registered_user_id) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getName(), o.getName() != null ? o.getName().toUpperCase() : null, o.getPassword(), o.getPasswordHash(), o.getPreviousPassword(), o.getLastReceivedScn(), o.getRegisteredUserId(), n.getName(), n.getName() != null ? n.getName().toUpperCase() : null, n.getPassword(), n.getPasswordHash(), n.getPreviousPassword(), n.getLastReceivedScn(), n.getRegisteredUserId());
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      remoteNode Saved object
    */
    public void save(RemoteNodeObject remoteNode) {
        
        if ( remoteNode == null ) {
            throwException("Cannot save NULL ?", new Object[]{"RemoteNodeObject"});
        }
        
        TransactionAction transactionAction;
        RemoteNodeObject oldRemoteNode;
        if ( remoteNode.getService() == null ) {
            remoteNode.setService((RemoteNodeService)this);
        }
        
        check(remoteNode);
        
        if ( remoteNode.getId() == null ) {
        
            remoteNode.setId(getObjectService().createObject(getTypeId()));
        
            oldRemoteNode = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, remoteNode.getId());
        
            executeUpdate("insert into cw_core_remote_nodes" 
                          + "( id, name, upper_name, password, password_hash, previous_password, last_received_scn, registered_user_id ) "
                          + "values"
                          + "(?, ?, ?, ?, ?, ?, ?, ?)",
                          remoteNode.getId(), remoteNode.getName(), ( remoteNode.getName() != null ? remoteNode.getName().toUpperCase() : null ), remoteNode.getPassword(), remoteNode.getPasswordHash(), remoteNode.getPreviousPassword(), remoteNode.getLastReceivedScn(), remoteNode.getRegisteredUserId() );
        
            if ( remoteNode.getRegisteredUserId() != null ) { getObjectService().setLink(remoteNode.getRegisteredUserId(), remoteNode.getId()); }
        
        } else {
        
            oldRemoteNode = load(remoteNode.getId());
            if ( oldRemoteNode.equals(remoteNode) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, remoteNode.getId());
        
            executeUpdate("update cw_core_remote_nodes set "
                          + "name = ?, upper_name = ?, password = ?, password_hash = ?, previous_password = ?, last_received_scn = ?, registered_user_id = ? "
                          + "where id = ?",
                          remoteNode.getName(), ( remoteNode.getName() != null ? remoteNode.getName().toUpperCase() : null ), remoteNode.getPassword(), remoteNode.getPasswordHash(), remoteNode.getPreviousPassword(), remoteNode.getLastReceivedScn(), remoteNode.getRegisteredUserId(), remoteNode.getId() );
        
            getObjectService().changeLinkParent(oldRemoteNode.getRegisteredUserId(), remoteNode.getRegisteredUserId(), remoteNode.getId());
        
        }
        
        saveAudit(transactionAction, oldRemoteNode, remoteNode);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        RemoteNodeObject oldRemoteNodeObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldRemoteNodeObject, null);
        
        getObjectService().deleteObject(id);
        
        if (oldRemoteNodeObject.getRegisteredUserId() != null) { getObjectService().deleteLink(oldRemoteNodeObject.getRegisteredUserId(), id); }
        
        executeUpdate("delete from cw_core_remote_nodes where id = ?", id);
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.user.UserService userService;
    
    public org.clawiz.core.common.storage.user.UserService getUserService() {
        if ( userService == null ) {
            userService = getService(org.clawiz.core.common.storage.user.UserService.class);
        }
        return userService;
    }
    
    public org.clawiz.core.common.storage.user.UserObject createUser() {
        return getUserService().create();
    }
    
    public org.clawiz.core.common.storage.user.UserObject loadUser(BigDecimal id) {
        return getUserService().load(id);
    }
}
