package org.clawiz.core.common.metadata.generator.node.dataclass.component;

import org.clawiz.core.common.metadata.data.language.node.enumeration.Enumeration;
import org.clawiz.core.common.metadata.generator.node.dataclass.component.element.MetadataNodeDataClassPrototypeFillReferencesMethodElement;
import org.clawiz.core.common.metadata.generator.node.dataclass.component.element.MetadataNodeDataClassPrototypePrepareMethodElement;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import org.clawiz.core.common.metadata.data.language.node.element.Structure;
import org.clawiz.core.common.metadata.data.language.node.element.field.AbstractStructureField;
import org.clawiz.core.common.metadata.data.language.node.element.field.AttributeStructureField;
import org.clawiz.core.common.metadata.data.language.node.element.field.ElementStructureField;
import org.clawiz.core.common.system.generator.java.component.element.JavaAnnotationElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaMemberElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaVariableElement;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

public class MetadataNodeDataClassPrototypeComponent extends AbstractMetadataNodeDataClassComponent {

    public class PreparedVariable {
        JavaVariableElement        variableElement;
        AbstractStructureField field;
    }
    ArrayList<PreparedVariable> preparedVariables = new ArrayList<>();

    @Override
    public void setStructure(Structure structure) {
        super.setStructure(structure);
        setName(getName() + "Prototype");
    }


    public void addImports() {
        addImport(BigDecimal.class);
    }

    protected void addField(AbstractStructureField field) {

        String type            = null;
        Class  annotationClass = null;

        if ( field instanceof AttributeStructureField) {

            annotationClass = ExchangeAttribute.class;

            AttributeStructureField attribute     = (AttributeStructureField) field;
            Class              valueTypeJavaClass = attribute.getValueType().getJavaClass();
            if ( ((AttributeStructureField) field).getEnumeration() != null ) {
                Enumeration enumeration = ((AttributeStructureField) field).getEnumeration();
                type = enumeration.getPackageName() + "." + enumeration.getJavaClassName();
            } else if ( valueTypeJavaClass == String.class ) {
                type = "String";
            } else if (valueTypeJavaClass == BigDecimal.class || valueTypeJavaClass == Date.class ) {
                type = valueTypeJavaClass.getSimpleName();
                addImport(valueTypeJavaClass);
            } else {
                type = valueTypeJavaClass.getName();
                addImport(valueTypeJavaClass);
            }

            if ( field.isArray() ) {
                addImport(ArrayList.class);
                type = "ArrayList<" + type + ">";
            }

        } else if ( field instanceof ElementStructureField) {

            ElementStructureField element = (ElementStructureField) field;
            annotationClass = element.isReference() ? ExchangeReference.class : ExchangeElement.class;

            type = field.isArray() ? element.getElementStructure().getDataListClassName() : element.getElementStructure().getDataClassName();


        } else {
            throwException("Wrong structure ? field ? class ?", getStructure().getFullName(), field.getName(), field.getClass().getName());
        }


        JavaVariableElement   ve = addVariable(type, field.getName());
        ve.setAccessLevel(JavaMemberElement.AccessLevel.PRIVATE);
        JavaAnnotationElement ae = ve.addAnnotation(annotationClass);
        if ( field.isRequired() ) {
                ae.addValue("required", field.isRequired());
        }
        if ( field.isArray() ) {
            ve.setInitStatement("new " + type + "()");
        }

        PreparedVariable preparedVariable = new PreparedVariable();

        preparedVariable.variableElement = ve;
        preparedVariable.field           = field;

        preparedVariables.add(preparedVariable);

    }

    public void addVariables() {

        for (AbstractStructureField field : getStructure().getFields() ) {
            addField(field);
        }


    }

    protected void addVariableWithCreateMethods(PreparedVariable preparedVariable) {

        String methodSuffix = preparedVariable.field.getJavaClassName();
        if ( preparedVariable.field.isArray() && methodSuffix.charAt(methodSuffix.length()-1) == 's') {
            methodSuffix = methodSuffix.substring(0, methodSuffix.length()-1);
        }

        JavaMethodElement method = addMethod("with" + methodSuffix);
        method.setType(getStructure().getJavaClassName());


        String dataClassName = null;
        if ( preparedVariable.field instanceof ElementStructureField ) {
            dataClassName = ((ElementStructureField) preparedVariable.field).getElementStructure().getDataClassName();
        } else if ( preparedVariable.field instanceof AttributeStructureField ) {
            dataClassName = preparedVariable.variableElement.getType();
        } else {
            throwException("Wrong structure field '?' class ?", preparedVariable.field.getFullName(), preparedVariable.field.getClass());
        }


        method.addParameter(dataClassName, "value");

        if ( preparedVariable.field.isArray() ) {
            method.addText(preparedVariable.variableElement.getGetterName() + "().add(value);");
        } else {
            method.addText(preparedVariable.variableElement.getSetterName() + "(value);");
        }

        method.addText("return (" + getStructure().getJavaClassName() + ") this;");

        if ( ! (preparedVariable.field instanceof ElementStructureField) ) {
            return;
        }

        ElementStructureField field = (ElementStructureField) preparedVariable.field;
        if ( field.isReference() ) {
            return;
        }

        method = addMethod("create" + methodSuffix);
        method.addGeneric("T extends " + dataClassName);
        method.setType("T");
        method.addParameter("Class<T>", "nodeClass");

        if ( ! field.isArray() ) {
            method.addText("if ( " + preparedVariable.variableElement.getGetterName() + "() != null ) {");
            method.addText("    throwException(\"Cannot create already defined value of '?' for '?'\", \"" + field.getName() + "\", this.getFullName());");
            method.addText("}");
        }

        method.addText(dataClassName + " value = createChildNode(nodeClass, \"" + field.getName() +"\");");

        if ( field.isArray() ) {
            method.addText(preparedVariable.variableElement.getGetterName() + "().add(value);");
        } else {
            method.addText(preparedVariable.variableElement.getSetterName() + "(value);");
        }

        method.addText("return (T) value;");

        method = addMethod("create" + methodSuffix);
        method.setType(dataClassName);
        method.addText("return create" + methodSuffix + "(" + dataClassName + ".class);");

    }

    public void addVariableGetSetMethods() {

        JavaMethodElement method = addMethod("withName");
        method.setType(getStructure().getJavaClassName());
        method.addParameter("String", "value");
        method.addText("setName(value);");
        method.addText("return (" + getStructure().getJavaClassName() + ") this;");

        for (PreparedVariable preparedVariable : preparedVariables ) {

            preparedVariable.variableElement.addGetter();

            if ( ! preparedVariable.field.isArray() ) {
                preparedVariable.variableElement.addSetter();
            }

            addVariableWithCreateMethods(preparedVariable);

        }
   }

    @Override
    public void process() {
        super.process();

        if ( getStructure().getExtendsNode() == null ) {
            setExtends(MetadataNode.class);
        } else {
            setExtends(getStructure().getExtendsNode().getPackageName() + "." + getStructure().getExtendsNode().getJavaClassName());
        }

        addVariables();
        addVariableGetSetMethods();


        addElement(MetadataNodeDataClassPrototypePrepareMethodElement.class);
        addElement(MetadataNodeDataClassPrototypeFillReferencesMethodElement.class);

    }
}
