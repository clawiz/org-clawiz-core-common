package org.clawiz.core.common.metadata.data.service.method;

import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ServiceMethodPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeElement
    private org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType valueType;
    
    @ExchangeElement
    private org.clawiz.core.common.metadata.data.service.method.parameter.ServiceMethodParameterList parameters = new org.clawiz.core.common.metadata.data.service.method.parameter.ServiceMethodParameterList();
    
    @ExchangeElement
    private org.clawiz.core.common.metadata.data.service.method.statement.MethodStatementList statements = new org.clawiz.core.common.metadata.data.service.method.statement.MethodStatementList();
    
    public ServiceMethod withName(String value) {
        setName(value);
        return (ServiceMethod) this;
    }
    
    public org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType getValueType() {
        return this.valueType;
    }
    
    public void setValueType(org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType value) {
        this.valueType = value;
    }
    
    public ServiceMethod withValueType(org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType value) {
        setValueType(value);
        return (ServiceMethod) this;
    }
    
    public <T extends org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType> T createValueType(Class<T> nodeClass) {
        if ( getValueType() != null ) {
            throwException("Cannot create already defined value of '?' for '?'", "valueType", this.getFullName());
        }
        org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType value = createChildNode(nodeClass, "valueType");
        setValueType(value);
        return (T) value;
    }
    
    public org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType createValueType() {
        return createValueType(org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType.class);
    }
    
    public org.clawiz.core.common.metadata.data.service.method.parameter.ServiceMethodParameterList getParameters() {
        return this.parameters;
    }
    
    public ServiceMethod withParameter(org.clawiz.core.common.metadata.data.service.method.parameter.ServiceMethodParameter value) {
        getParameters().add(value);
        return (ServiceMethod) this;
    }
    
    public <T extends org.clawiz.core.common.metadata.data.service.method.parameter.ServiceMethodParameter> T createParameter(Class<T> nodeClass) {
        org.clawiz.core.common.metadata.data.service.method.parameter.ServiceMethodParameter value = createChildNode(nodeClass, "parameters");
        getParameters().add(value);
        return (T) value;
    }
    
    public org.clawiz.core.common.metadata.data.service.method.parameter.ServiceMethodParameter createParameter() {
        return createParameter(org.clawiz.core.common.metadata.data.service.method.parameter.ServiceMethodParameter.class);
    }
    
    public org.clawiz.core.common.metadata.data.service.method.statement.MethodStatementList getStatements() {
        return this.statements;
    }
    
    public ServiceMethod withStatement(org.clawiz.core.common.metadata.data.service.method.statement.AbstractMethodStatement value) {
        getStatements().add(value);
        return (ServiceMethod) this;
    }
    
    public <T extends org.clawiz.core.common.metadata.data.service.method.statement.AbstractMethodStatement> T createStatement(Class<T> nodeClass) {
        org.clawiz.core.common.metadata.data.service.method.statement.AbstractMethodStatement value = createChildNode(nodeClass, "statements");
        getStatements().add(value);
        return (T) value;
    }
    
    public org.clawiz.core.common.metadata.data.service.method.statement.AbstractMethodStatement createStatement() {
        return createStatement(org.clawiz.core.common.metadata.data.service.method.statement.AbstractMethodStatement.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getValueType() != null ) { 
            getValueType().prepare(session);
        }
        for (MetadataNode node : getParameters()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        for (MetadataNode node : getStatements()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getValueType());
        
        for (MetadataNode node : getParameters()) {
            references.add(node);
        }
        
        for (MetadataNode node : getStatements()) {
            references.add(node);
        }
        
    }
}
