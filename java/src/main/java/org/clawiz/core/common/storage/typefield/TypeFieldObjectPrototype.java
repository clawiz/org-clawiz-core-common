package org.clawiz.core.common.storage.typefield;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class TypeFieldObjectPrototype extends AbstractObject {
    
    /**
    * Type
    */
    private java.math.BigDecimal typeId;
    
    /**
    * OrderNo
    */
    private java.math.BigDecimal orderNo;
    
    /**
    * Name
    */
    private java.lang.String name;
    
    /**
    * Description
    */
    private java.lang.String description;
    
    /**
    * ColumnName
    */
    private java.lang.String columnName;
    
    /**
    * ValueType
    */
    private java.lang.String valueType;
    
    /**
    * ReferencedType
    */
    private java.math.BigDecimal referencedTypeId;
    
    /**
    * ValuePrecision
    */
    private java.math.BigDecimal valuePrecision;
    
    /**
    * ValueScale
    */
    private java.math.BigDecimal valueScale;
    
    /**
    * DefaultValueDefinition
    */
    private java.lang.String defaultValueDefinition;
    
    /**
    * CalculateExpressionDefinition
    */
    private java.lang.String calculateExpressionDefinition;
    
    /**
    * ValidateDefinition
    */
    private java.lang.String validateDefinition;
    
    /**
    * AutoUppercase
    */
    private java.lang.Boolean autoUppercase;
    
    /**
    * Required
    */
    private java.lang.Boolean required;
    
    public TypeFieldService service;
    
    /**
    * 
    * @return     Type
    */
    public java.math.BigDecimal getTypeId() {
        return this.typeId;
    }
    
    /**
    * Set 'Type' value
    * 
    * @param      typeId Type
    */
    public void setTypeId(java.math.BigDecimal typeId) {
         this.typeId = typeId;
    }
    
    /**
    * Set 'Type' value and return this object
    * 
    * @param      typeId Type
    * @return     TypeField object
    */
    public TypeFieldObjectPrototype withTypeId(java.math.BigDecimal typeId) {
        setTypeId(typeId);
        return this;
    }
    
    /**
    * 
    * @return     OrderNo
    */
    public java.math.BigDecimal getOrderNo() {
        return this.orderNo;
    }
    
    /**
    * Set 'OrderNo' value
    * 
    * @param      orderNo OrderNo
    */
    public void setOrderNo(java.math.BigDecimal orderNo) {
         this.orderNo = orderNo;
    }
    
    /**
    * Set 'OrderNo' value and return this object
    * 
    * @param      orderNo OrderNo
    * @return     TypeField object
    */
    public TypeFieldObjectPrototype withOrderNo(java.math.BigDecimal orderNo) {
        setOrderNo(orderNo);
        return this;
    }
    
    /**
    * 
    * @return     Name
    */
    public java.lang.String getName() {
        return this.name;
    }
    
    /**
    * Set 'Name' value
    * 
    * @param      name Name
    */
    public void setName(java.lang.String name) {
        
        if ( name != null && name.length() > 75) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "TypeField.Name", "75");
        }
        
         this.name = name;
    }
    
    /**
    * Set 'Name' value and return this object
    * 
    * @param      name Name
    * @return     TypeField object
    */
    public TypeFieldObjectPrototype withName(java.lang.String name) {
        setName(name);
        return this;
    }
    
    /**
    * 
    * @return     Description
    */
    public java.lang.String getDescription() {
        return this.description;
    }
    
    /**
    * Set 'Description' value
    * 
    * @param      description Description
    */
    public void setDescription(java.lang.String description) {
        
        if ( description != null && description.length() > 1024) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "TypeField.Description", "1024");
        }
        
         this.description = description;
    }
    
    /**
    * Set 'Description' value and return this object
    * 
    * @param      description Description
    * @return     TypeField object
    */
    public TypeFieldObjectPrototype withDescription(java.lang.String description) {
        setDescription(description);
        return this;
    }
    
    /**
    * 
    * @return     ColumnName
    */
    public java.lang.String getColumnName() {
        return this.columnName;
    }
    
    /**
    * Set 'ColumnName' value
    * 
    * @param      columnName ColumnName
    */
    public void setColumnName(java.lang.String columnName) {
        
        if ( columnName != null && columnName.length() > 75) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "TypeField.ColumnName", "75");
        }
        
         this.columnName = columnName;
    }
    
    /**
    * Set 'ColumnName' value and return this object
    * 
    * @param      columnName ColumnName
    * @return     TypeField object
    */
    public TypeFieldObjectPrototype withColumnName(java.lang.String columnName) {
        setColumnName(columnName);
        return this;
    }
    
    /**
    * 
    * @return     ValueType
    */
    public java.lang.String getValueType() {
        return this.valueType;
    }
    
    /**
    * Set 'ValueType' value
    * 
    * @param      valueType ValueType
    */
    public void setValueType(java.lang.String valueType) {
        
        if ( valueType != null && valueType.length() > 16000) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "TypeField.ValueType", "16000");
        }
        
         this.valueType = valueType;
    }
    
    /**
    * Set 'ValueType' value and return this object
    * 
    * @param      valueType ValueType
    * @return     TypeField object
    */
    public TypeFieldObjectPrototype withValueType(java.lang.String valueType) {
        setValueType(valueType);
        return this;
    }
    
    /**
    * 
    * @return     ReferencedType
    */
    public java.math.BigDecimal getReferencedTypeId() {
        return this.referencedTypeId;
    }
    
    /**
    * Set 'ReferencedType' value
    * 
    * @param      referencedTypeId ReferencedType
    */
    public void setReferencedTypeId(java.math.BigDecimal referencedTypeId) {
         this.referencedTypeId = referencedTypeId;
    }
    
    /**
    * Set 'ReferencedType' value and return this object
    * 
    * @param      referencedTypeId ReferencedType
    * @return     TypeField object
    */
    public TypeFieldObjectPrototype withReferencedTypeId(java.math.BigDecimal referencedTypeId) {
        setReferencedTypeId(referencedTypeId);
        return this;
    }
    
    /**
    * 
    * @return     ValuePrecision
    */
    public java.math.BigDecimal getValuePrecision() {
        return this.valuePrecision;
    }
    
    /**
    * Set 'ValuePrecision' value
    * 
    * @param      valuePrecision ValuePrecision
    */
    public void setValuePrecision(java.math.BigDecimal valuePrecision) {
         this.valuePrecision = valuePrecision;
    }
    
    /**
    * Set 'ValuePrecision' value and return this object
    * 
    * @param      valuePrecision ValuePrecision
    * @return     TypeField object
    */
    public TypeFieldObjectPrototype withValuePrecision(java.math.BigDecimal valuePrecision) {
        setValuePrecision(valuePrecision);
        return this;
    }
    
    /**
    * 
    * @return     ValueScale
    */
    public java.math.BigDecimal getValueScale() {
        return this.valueScale;
    }
    
    /**
    * Set 'ValueScale' value
    * 
    * @param      valueScale ValueScale
    */
    public void setValueScale(java.math.BigDecimal valueScale) {
         this.valueScale = valueScale;
    }
    
    /**
    * Set 'ValueScale' value and return this object
    * 
    * @param      valueScale ValueScale
    * @return     TypeField object
    */
    public TypeFieldObjectPrototype withValueScale(java.math.BigDecimal valueScale) {
        setValueScale(valueScale);
        return this;
    }
    
    /**
    * 
    * @return     DefaultValueDefinition
    */
    public java.lang.String getDefaultValueDefinition() {
        return this.defaultValueDefinition;
    }
    
    /**
    * Set 'DefaultValueDefinition' value
    * 
    * @param      defaultValueDefinition DefaultValueDefinition
    */
    public void setDefaultValueDefinition(java.lang.String defaultValueDefinition) {
        
        if ( defaultValueDefinition != null && defaultValueDefinition.length() > 1024) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "TypeField.DefaultValueDefinition", "1024");
        }
        
         this.defaultValueDefinition = defaultValueDefinition;
    }
    
    /**
    * Set 'DefaultValueDefinition' value and return this object
    * 
    * @param      defaultValueDefinition DefaultValueDefinition
    * @return     TypeField object
    */
    public TypeFieldObjectPrototype withDefaultValueDefinition(java.lang.String defaultValueDefinition) {
        setDefaultValueDefinition(defaultValueDefinition);
        return this;
    }
    
    /**
    * 
    * @return     CalculateExpressionDefinition
    */
    public java.lang.String getCalculateExpressionDefinition() {
        return this.calculateExpressionDefinition;
    }
    
    /**
    * Set 'CalculateExpressionDefinition' value
    * 
    * @param      calculateExpressionDefinition CalculateExpressionDefinition
    */
    public void setCalculateExpressionDefinition(java.lang.String calculateExpressionDefinition) {
        
        if ( calculateExpressionDefinition != null && calculateExpressionDefinition.length() > 1024) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "TypeField.CalculateExpressionDefinition", "1024");
        }
        
         this.calculateExpressionDefinition = calculateExpressionDefinition;
    }
    
    /**
    * Set 'CalculateExpressionDefinition' value and return this object
    * 
    * @param      calculateExpressionDefinition CalculateExpressionDefinition
    * @return     TypeField object
    */
    public TypeFieldObjectPrototype withCalculateExpressionDefinition(java.lang.String calculateExpressionDefinition) {
        setCalculateExpressionDefinition(calculateExpressionDefinition);
        return this;
    }
    
    /**
    * 
    * @return     ValidateDefinition
    */
    public java.lang.String getValidateDefinition() {
        return this.validateDefinition;
    }
    
    /**
    * Set 'ValidateDefinition' value
    * 
    * @param      validateDefinition ValidateDefinition
    */
    public void setValidateDefinition(java.lang.String validateDefinition) {
        
        if ( validateDefinition != null && validateDefinition.length() > 512) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "TypeField.ValidateDefinition", "512");
        }
        
         this.validateDefinition = validateDefinition;
    }
    
    /**
    * Set 'ValidateDefinition' value and return this object
    * 
    * @param      validateDefinition ValidateDefinition
    * @return     TypeField object
    */
    public TypeFieldObjectPrototype withValidateDefinition(java.lang.String validateDefinition) {
        setValidateDefinition(validateDefinition);
        return this;
    }
    
    /**
    * 
    * @return     AutoUppercase
    */
    public java.lang.Boolean isAutoUppercase() {
        return this.autoUppercase != null ? this.autoUppercase : false;
    }
    
    /**
    * Set 'AutoUppercase' value
    * 
    * @param      autoUppercase AutoUppercase
    */
    public void setAutoUppercase(java.lang.Boolean autoUppercase) {
         this.autoUppercase = autoUppercase;
    }
    
    /**
    * Set 'AutoUppercase' value
    * 
    * @param      autoUppercase AutoUppercase
    */
    public void setAutoUppercase(boolean autoUppercase) {
        this.autoUppercase= new Boolean(autoUppercase);
    }
    
    /**
    * Set 'AutoUppercase' value and return this object
    * 
    * @param      autoUppercase AutoUppercase
    * @return     TypeField object
    */
    public TypeFieldObjectPrototype withAutoUppercase(java.lang.Boolean autoUppercase) {
        setAutoUppercase(autoUppercase);
        return this;
    }
    
    /**
    * Set 'AutoUppercase' value
    * 
    * @param      autoUppercase AutoUppercase
    * @return     
    */
    public TypeFieldObjectPrototype withAutoUppercase(boolean autoUppercase) {
        setAutoUppercase(autoUppercase);
        return this;
    }
    
    /**
    * 
    * @return     Required
    */
    public java.lang.Boolean isRequired() {
        return this.required != null ? this.required : false;
    }
    
    /**
    * Set 'Required' value
    * 
    * @param      required Required
    */
    public void setRequired(java.lang.Boolean required) {
         this.required = required;
    }
    
    /**
    * Set 'Required' value
    * 
    * @param      required Required
    */
    public void setRequired(boolean required) {
        this.required= new Boolean(required);
    }
    
    /**
    * Set 'Required' value and return this object
    * 
    * @param      required Required
    * @return     TypeField object
    */
    public TypeFieldObjectPrototype withRequired(java.lang.Boolean required) {
        setRequired(required);
        return this;
    }
    
    /**
    * Set 'Required' value
    * 
    * @param      required Required
    * @return     
    */
    public TypeFieldObjectPrototype withRequired(boolean required) {
        setRequired(required);
        return this;
    }
    
    public TypeFieldService getService() {
        return this.service;
    }
    
    public void setService(TypeFieldService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(TypeFieldObjectPrototype  target) {
        target.setTypeId(getTypeId());
        target.setOrderNo(getOrderNo());
        target.setName(getName());
        target.setDescription(getDescription());
        target.setColumnName(getColumnName());
        target.setValueType(getValueType());
        target.setReferencedTypeId(getReferencedTypeId());
        target.setValuePrecision(getValuePrecision());
        target.setValueScale(getValueScale());
        target.setDefaultValueDefinition(getDefaultValueDefinition());
        target.setCalculateExpressionDefinition(getCalculateExpressionDefinition());
        target.setValidateDefinition(getValidateDefinition());
        target.setAutoUppercase(isAutoUppercase());
        target.setRequired(isRequired());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((TypeFieldObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        service.save((TypeFieldObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(TypeFieldObjectPrototype object) {
        return 
               isObjectsEquals(this.getTypeId(), object.getTypeId() ) 
            && isObjectsEquals(this.getOrderNo(), object.getOrderNo() ) 
            && isObjectsEquals(this.getName(), object.getName() ) 
            && isObjectsEquals(this.getDescription(), object.getDescription() ) 
            && isObjectsEquals(this.getColumnName(), object.getColumnName() ) 
            && isObjectsEquals(this.getValueType(), object.getValueType() ) 
            && isObjectsEquals(this.getReferencedTypeId(), object.getReferencedTypeId() ) 
            && isObjectsEquals(this.getValuePrecision(), object.getValuePrecision() ) 
            && isObjectsEquals(this.getValueScale(), object.getValueScale() ) 
            && isObjectsEquals(this.getDefaultValueDefinition(), object.getDefaultValueDefinition() ) 
            && isObjectsEquals(this.getCalculateExpressionDefinition(), object.getCalculateExpressionDefinition() ) 
            && isObjectsEquals(this.getValidateDefinition(), object.getValidateDefinition() ) 
            && isObjectsEquals(this.isAutoUppercase(), object.isAutoUppercase() ) 
            && isObjectsEquals(this.isRequired(), object.isRequired() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getTypeId() != null ? getTypeId().hashCode() : 0);
        result = result * 31 + (getOrderNo() != null ? getOrderNo().hashCode() : 0);
        result = result * 31 + (getName() != null ? getName().hashCode() : 0);
        result = result * 31 + (getDescription() != null ? getDescription().hashCode() : 0);
        result = result * 31 + (getColumnName() != null ? getColumnName().hashCode() : 0);
        result = result * 31 + (getValueType() != null ? getValueType().hashCode() : 0);
        result = result * 31 + (getReferencedTypeId() != null ? getReferencedTypeId().hashCode() : 0);
        result = result * 31 + (getValuePrecision() != null ? getValuePrecision().hashCode() : 0);
        result = result * 31 + (getValueScale() != null ? getValueScale().hashCode() : 0);
        result = result * 31 + (getDefaultValueDefinition() != null ? getDefaultValueDefinition().hashCode() : 0);
        result = result * 31 + (getCalculateExpressionDefinition() != null ? getCalculateExpressionDefinition().hashCode() : 0);
        result = result * 31 + (getValidateDefinition() != null ? getValidateDefinition().hashCode() : 0);
        result = result * 31 + (isAutoUppercase() != null ? isAutoUppercase().hashCode() : 0);
        result = result * 31 + (isRequired() != null ? isRequired().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'TypeName' fields
    * 
    * @return     Concatenated string values of key 'toTypeName' fields
    */
    public String toTypeName() {
        return service.getObjectService().idToString(getTypeId()) + "," + getName();
    }
    
    /**
    * Prepare concatenated string values of key 'TypeColumnName' fields
    * 
    * @return     Concatenated string values of key 'toTypeColumnName' fields
    */
    public String toTypeColumnName() {
        return service.getObjectService().idToString(getTypeId()) + "," + getColumnName();
    }
}
