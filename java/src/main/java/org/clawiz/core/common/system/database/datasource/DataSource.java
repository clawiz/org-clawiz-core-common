/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.database.datasource;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.database.Connection;
import org.clawiz.core.common.system.database.adapter.DatabaseAdapter;
import org.clawiz.core.common.system.database.adapter.DatabaseType;
import org.clawiz.core.common.system.database.adapter.derby.DerbyDatabaseAdapter;
import org.clawiz.core.common.system.database.adapter.hsqldb.HsqldbDatabaseAdapter;
import org.clawiz.core.common.system.database.adapter.mysql.MysqlDatabaseAdapter;
import org.clawiz.core.common.system.database.adapter.oracle.OracleDatabaseAdapter;
import org.clawiz.core.common.system.database.adapter.postgresql.PostgreSQLDatabaseAdapter;
import org.clawiz.core.common.system.database.adapter.sqlserver.SQLServerDatabaseAdapter;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.utils.StringUtils;

public abstract class DataSource {

    
    DataSourceConfig config;

    public DataSourceConfig getConfig() {
        return config;
    }

    public void setConfig(DataSourceConfig config) {
        this.config = config;
    }

    public void setAutoCommit(boolean autoCommit) {
        config.setAutoCommit(autoCommit);
    }

    public boolean isAutoCommit() {
        return getConfig().isAutoCommit();
    }

    public Connection getConnection(Session session) {
        return new Connection(session);
    }

    public abstract java.sql.Connection getJdbcConnection();

    DatabaseType databaseType;

    public DatabaseType getDatabaseType() {
        if ( databaseType == null ) {
            if (StringUtils.isLike(getConfig().getUrl(), "*derby*") ) {
                databaseType = DatabaseType.DERBY;
            } else if (StringUtils.isLike(getConfig().getUrl(), "*hsqldb*") ) {
                databaseType = DatabaseType.HSQLDB;
            } else if (StringUtils.isLike(getConfig().getUrl(), "*mysql*") ) {
                databaseType = DatabaseType.MYSQL;
            } else if (StringUtils.isLike(getConfig().getUrl(), "*oracle*") ) {
                databaseType = DatabaseType.ORACLE;
            } else if (StringUtils.isLike(getConfig().getUrl(), "*postgres*") ) {
                databaseType = DatabaseType.POSTGRESQL;
            } else if (StringUtils.isLike(getConfig().getUrl(), "*microsoft*") ) {
                databaseType = DatabaseType.SQLSERVER;
            } else  {
                throw new CoreException("Cannot determine supported database type for jdbc url ?", getConfig().getUrl());
            }
        }
        return databaseType;
    }

    public <T extends DatabaseAdapter> Class<T> getDatabaseAdapterClass() {
        switch (getDatabaseType()) {
            case DERBY:      return (Class<T>) DerbyDatabaseAdapter.class;
            case HSQLDB:     return (Class<T>) HsqldbDatabaseAdapter.class;
            case MYSQL:      return (Class<T>) MysqlDatabaseAdapter.class;
            case ORACLE:     return (Class<T>) OracleDatabaseAdapter.class;
            case POSTGRESQL: return (Class<T>) PostgreSQLDatabaseAdapter.class;
            case SQLSERVER:  return (Class<T>) SQLServerDatabaseAdapter.class;
        }
        throw new CoreException("Cannot determine database adapter class for database type ?", getDatabaseType());
    }

}
