/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.queue;

import org.clawiz.core.common.system.service.Service;

/**
 * Сервис каждую минуту проверяет состояние задач и запускает на выполнение те задачи
 */
public class QueueDispatcher extends Service {

/*
    private Timer timer;

    private ArrayList<BigDecimal> disabledQueues = new ArrayList<BigDecimal>();
    private ArrayList<BigDecimal> enabledQueues  = new ArrayList<BigDecimal>();


    private static int threadCount = 0;

    private ThreadGroup mainThreadGroup;
    private HashMap<String, ThreadGroup> threadsGroups;

    @NotInitializeService
    QueueBase queueBase;

    private ArrayList<BigDecimal> queueNamesToIdList(String queueNames) {
        ArrayList<BigDecimal> result = new ArrayList<BigDecimal>();
        if (queueNames == null ) {
            return result;
        }
        String[] tokens = StringUtils.splitAndTrim(queueNames, ";");
        for (String name : tokens) {
            BigDecimal id = queueBase.queueNameToId(name);
            if ( id != null ) {
                result.add(id);
            } else {
                logError("Queue '" + name + "' not exists in queues list");
            }
        }

        return result;
    }

    public void start() {

        mainThreadGroup = new ThreadGroup("jobThreads");
        threadsGroups    = new HashMap<>();
        queueBase = getService(QueueBase.class);

        disabledQueues = queueNamesToIdList(Core.getConfig().getString("krn.queueDispatcher.disabled-queues", null));
        enabledQueues  = queueNamesToIdList(Core.getConfig().getString("krn.queueDispatcher.enabled-queues", null));

        DataSet jobDataSet = executeSelect("select * from cw_core_queue_jobs where upper_state like ?", new Object[]{"ACTIVE"});

        BigDecimal id;
        while (!jobDataSet.isEOF()) {
            id = jobDataSet.getBigDecimal("id");
            queueBase.addTextLine(id, "Info", "Задача возобновлена в связи с перезагрузкой сервера");
            executeStatement("update cw_core_queue_jobs set state = 'CREATED', upper_state = 'CREATED' where id = ?", new Object[]{id});
            jobDataSet.next();
        }
        commit();

        TimerTask task = new TimerTask() {
            public void run() {
                try {
*/
/*
                    getSession().reconnect();
                    if (!getSession().getDatabaseAdapter().checkConnection()) {
                        getSession().reconnect();
                    }
*//*

                    perform();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        timer = new Timer();
        timer.schedule(task, 100, 1000);

        logInfo(this.getClass().getName() + " started");
    }

    */
/**
     * Метод получения списка задач, которые необходимо выполнить в новом потоке и новой сессии
     *
     * @CoreException
     *//*

    private void perform() {

        if ( ! queueBase.getSession().checkConnection() ) {
            queueBase.getSession().reconnect();
        }
        QueueList queueList = queueBase.getQueueList();

        if ( enabledQueues.size() > 0 ) {
            QueueList newList = new QueueList();
            for (QueueObject obj : queueList ) {
                boolean isFound = false;
                for (BigDecimal id : enabledQueues ) {
                    if ( obj.getId().equals(id) ) {
                        isFound = true;
                        break;
                    }
                }
                if ( isFound ) {
                    newList.add( obj);
                }
            }
            queueList = newList;
        }

        if ( disabledQueues.size() > 0 ) {
            QueueList newList = new QueueList();
            for (QueueObject obj : queueList ) {
                boolean isFound = false;
                for (BigDecimal id : disabledQueues ) {
                    if ( obj.getId().equals(id) ) {
                        isFound = true;
                        break;
                    }
                }
                if ( ! isFound ) {
                    newList.add(obj);
                }
            }
            queueList = newList;
        }

        ThreadGroup group;
        for (QueueObject queue : queueList) {

            group = threadsGroups.get(queue.getCode());

            if (group == null) {
                group = new ThreadGroup(mainThreadGroup, queue.getCode());
                threadsGroups.put(queue.getCode(), group);
            }

            //максимальное количество процессов данной очереди
            int processCount = queue.getProcessesCount() != null ? queue.getProcessesCount().intValue() : 0;
            if ( processCount == 0 || processCount > 25) {
                processCount = 25;
            }

            int c = processCount - getActiveCurrentThreadCount(queue.getCode());

            if (c <= 0) {
                continue;
            }

            ArrayList<BigDecimal> jobs = queueBase.getQueueJobListForRun(queue.getId(), processCount);
            int jobCount = jobs.size();

            if (c > jobCount) {
                c = jobCount;
            }
            for (int iJob=0; iJob < c; iJob++) {
                queueBase.setJobState(jobs.get(iJob), "ACTIVE");
                commit();
                runJob(jobs.get(iJob), queue.getCode());
            }

        }
    }

    */
/**
     * Метод для обработки выполняемой задачи
     *
     * @param jobNo     - номер задачи
     * @param queueCode - код очереди
     *//*

    public void runJob(BigDecimal jobNo, String queueCode) {
        //имена для потоков и для соотвествующих сессий
        String threadName = "thread_" + jobNo + "_" + threadCount;
        String sessionKey = "key_" + jobNo + "_" + threadCount;
        threadCount++;

        JobProcessThread processThread = new JobProcessThread(threadName, queueCode);
        processThread.setJobId(jobNo);
        processThread.setSessionKey(sessionKey);
        processThread.setDaemon(true); //чтобы потоки убивались при закрытии джибоса

        processThread.start();

    }

    public Thread[] getActiveCurrentThreads() {

        int activeCount = mainThreadGroup.activeCount();
        Thread[] activeThreadList = new Thread[activeCount];
        mainThreadGroup.enumerate(activeThreadList);
        return activeThreadList;
    }

    public int getActiveCurrentThreadCount(String queueCode) {

        ThreadGroup thread = threadsGroups.get(queueCode);
        return thread.activeCount();
    }

    public Thread getActiveThreadByName(String threadName) {
        Thread[] currentThreads = getActiveCurrentThreads();

        for (Thread currentThread : currentThreads) {
            if (currentThread.getName().equals(threadName)) {
                return currentThread;
            }
        }
        return null;
    }

    public class JobProcessThread extends Thread {

        private BigDecimal jobNo;

        private String sessionKey;

        private String threadName;
        private String queueCode;


        public JobProcessThread(String threadName, String queueCode) {
            super(threadsGroups.get(queueCode), threadName);
            this.threadName = threadName;
            this.queueCode = queueCode;
        }

        public void setJobId(BigDecimal jobNo) {
            this.jobNo = jobNo;
        }

        public void setSessionKey(String sessionKey) {
            this.sessionKey = sessionKey;
        }

        public void run() {

            long startTime = (new Date()).getTime();

            try {
                logDebug("Job started  : " + jobNo + " (" + queueCode + ")");
            } catch (CoreException e) {
                e.printStackTrace();
            }

            Session runSession = null;
            String stateName = "DONE";
            try {

                runSession = Core.newDispatcherSession(sessionKey);

                if ("CREATED".equals(queueBase.getJobState(jobNo))) {
                    queueBase.setStartTime(jobNo, new Date());
                }

                queueBase.setJobState(jobNo, "ACTIVE");
                queueBase.setSessionName(jobNo, sessionKey);
                queueBase.setThreadName(jobNo, threadName);

                commit();

                QueueJobService queueJobService = (QueueJobService) runSession.getService(QueueJobService.class);
                JobObject job = queueJobService.fetchJob(jobNo);

                {
                    String serviceName = queueBase.getServiceNameByQueueId(job.getQueueId());

                    if (serviceName == null || "".equals(serviceName)) {
                        throwException("Service name not defined for queue '@'", new Object[]{jobNo, job.getQueueId()});
                    }

                    String methodName = queueBase.getMethodNameByQueueId(job.getQueueId());

                    if (methodName == null || "".equals(methodName)) {
                        throwException("Method name not defined for queue '@'", new Object[]{jobNo, job.getQueueId()});
                    }

                    Service jobService = runSession.getService(serviceName);

                    InstallerContext parameterList = queueBase.getParameterListByQueueId(job.getQueueId());

                    Class[] parameterTypes = new Class[parameterList.size()];
                    Object[] parameterValues = new Object[parameterList.size()];

                    ParameterObject parameterObject;
                    for (int i = 0; i < parameterTypes.length; i++) {

                        parameterObject = (ParameterObject) parameterList.get(i);

                        parameterTypes[i] = queueBase.getQueueParameterClass(job.getQueueId(), parameterObject.getName());

                        if (parameterTypes[i] == String.class) {
                            parameterValues[i] = queueBase.getQueueJobParameterString(job.getId(), parameterObject.getName());
                        } else if (parameterTypes[i] == BigDecimal.class) {
                            parameterValues[i] = queueBase.getQueueJobParameterNumber(job.getId(), parameterObject.getName());
                        } else if (parameterTypes[i] == Date.class) {
                            parameterValues[i] = queueBase.getQueueJobParameterDate(job.getId(), parameterObject.getName());
                        }
                    }

                    Method method = jobService.getClass().getMethod(methodName, parameterTypes);
                    method.invoke(jobService, parameterValues);

                }

                logDebug("Job finished : " + jobNo + " (" + queueCode + "). Total time = " + ((new Date()).getTime() - startTime));
                
            } catch (Exception e) {
                stateName = "ERROR";
                e.printStackTrace();
                String msg =  StringUtils.substring("Ошибка " + (e.getCause() != null ? e.getCause().getMessage() : e.getMessage()), 0, 4096);
                try {
                    queueBase.addTextLine(jobNo, "ERROR", msg);
                } catch (CoreException e1) {
                    try {
                        logError("job error line save exception\n" + jobNo + "\n" + msg + "\""+ e1.getMessage());
                    } catch (CoreException e2) {
                        e2.printStackTrace();
                    }
                    e1.printStackTrace();
                }
            }

            try {
                String currentState = queueBase.getJobState(jobNo);
                if (!"CANCELED".equals(currentState)) {
                    queueBase.setJobState(jobNo, stateName);
                    commit();
                }
                runSession.destroy();
            } catch (CoreException e) {
                e.printStackTrace();
                try {
                    logError("System error on set job '" + jobNo + "' state : \n" + ( e.getCause() != null ? e.getCause().getMessage() : e.getMessage() ));
                } catch (CoreException e1) {
                    e1.printStackTrace();
                }
            }
            this.interrupt();

        }

    }

*/
}
