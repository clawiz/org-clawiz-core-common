/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator.component.model.element;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.generator.component.service.element.AbstractTypeServiceMethodElement;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeBLOB;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;

public class TypeModelGetTypeFieldModelByNameMethodElement extends AbstractTypeServiceMethodElement {

    @Override
    public void process() {

        super.process();
        setName("getByFieldName");
        setType("TypeFieldModel");

        addParameter("String", "fieldName");

        addText("if ( fieldName == null ) {");
        addText("    throwException(\"Cannot return field model for null field name\");");
        addText("}");
        addText("switch (fieldName.toUpperCase()) {");

        for (TypeField field : getFields() ) {
            if ( field.getValueType() instanceof ValueTypeBLOB || field.getValueType() instanceof ValueTypeList) {
                continue;
            }

            addText("case \"" + field.getName().toUpperCase() + "\" : return " + field.getJavaVariableName() + "();");
        }

        addText("    default : throwException(\"Wrong field name '?' in call to get model field of '?'\", new Object[]{fieldName, getType().getFullName()});");
        addText("}");
        addText("return null;");

    }
}
