/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator.component.dataobject.element;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;

public class TypeDataObjectSaveMethodElement extends AbstractTypeDataObjectMethodElement {

    @Override
    public void process() {
        super.process();

        addAnnotation(SuppressWarnings.class)
                .withValue(null, "Duplicates");

        setName("save");

        addJavaDoc("Save object to database");

        addText("if ( service == null ) {");
        addText("    throw new CoreException(\"Service must be defined before save object\");");
        addText("}");


        for (TypeField field : getFields() ) {

            if ( field.getValueType() instanceof ValueTypeObject && ((ValueTypeObject) field.getValueType()).getReferencedType() != null ) {

                String objectVarName = field.getJavaVariableNameWithoutId();
                String idVarName     = field.getJavaVariableName();

                addText("if ( " + objectVarName + " != null ) {");
                addText("    " + objectVarName + ".save();" );
                addText("    " + idVarName + " = " + objectVarName + ".getId();");
                addText("}");

            }

        }

        addText("service.save((" + getDataObjectClassName() + ") this);");

        for (TypeField field : getFields() ) {

            if ( field.getValueType() instanceof ValueTypeList && ((ValueTypeList) field.getValueType()).getReferencedType() != null ) {

                ValueTypeList vt = (ValueTypeList) field.getValueType();
                addText("if ( " + field.getJavaVariableName() + " != null ) {");
                addText("    for (" + vt.getReferencedType().getServicePackageName() + "." + vt.getReferencedType().getJavaClassName() + "Object object : " + field.getJavaVariableName() + " ) {");
                addText("        object." + getReferencedTypeLinkedField(vt.getReferencedType()).getSetMethodName() + "(id);");
                addText("        object.save();");
                addText("    }");
                addText("}");
            }

        }

    }
}
