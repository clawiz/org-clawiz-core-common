/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.es6.component.element;

import org.clawiz.core.common.system.generator.abstractgenerator.component.element.AbstractElement;

public class ES6ClassElement extends AbstractES6Element {

    boolean export;
    String  extendsString;

    public boolean isExport() {
        return export;
    }

    public void setExport(boolean export) {
        this.export = export;
    }

    public ES6ClassElement withExport(boolean export) {
        this.export = export;
        return this;
    }

    public String getExtends() {
        return extendsString;
    }

    public void setExtends(String extendsString) {
        this.extendsString = extendsString;
    }

    public ES6ClassElement withExtends(String extendsString) {
        this.extendsString = extendsString;
        return this;
    }

    public ES6ClassMethodElement addMethod(String name) {
        return addElement(ES6ClassMethodElement.class, name);
    }

    public ES6ClassGetterMethodElement addGetter(String name) {
        return addElement(ES6ClassGetterMethodElement.class, name);
    }

    public ES6ClassSetterMethodElement addSetter(String name) {
        return addElement(ES6ClassSetterMethodElement.class, name);
    }

    public ES6ClassProperty addProperty(String name) {

        ES6ClassProperty property = new ES6ClassProperty();

        property.setGetter(addGetter(name));
        property.getGetter().addText("return this._" + name + ";");

        property.setSetter(addSetter(name));
        property.getSetter().addText("this._" + name + " = value;");

        return property;
    }

    @Override
    public void write() {

        pln("");
        pln((isExport() ? "export " : "") + "class " + getName() + (getExtends() != null ? " extends " + getExtends() : "") + " {");

        for (int i = 0; i< getElements().size(); i++ ) {
            AbstractElement child = getElements().get(i);
            pln("");
            child.write();
        }

        pln("");
        pln("}");
    }
}
