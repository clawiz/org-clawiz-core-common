package org.clawiz.core.common.metadata.data.database.table.column;

import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class TableColumnPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeElement
    private org.clawiz.core.common.metadata.data.database.table.column.type.AbstractTableColumnType type;
    
    public TableColumn withName(String value) {
        setName(value);
        return (TableColumn) this;
    }
    
    public org.clawiz.core.common.metadata.data.database.table.column.type.AbstractTableColumnType getType() {
        return this.type;
    }
    
    public void setType(org.clawiz.core.common.metadata.data.database.table.column.type.AbstractTableColumnType value) {
        this.type = value;
    }
    
    public TableColumn withType(org.clawiz.core.common.metadata.data.database.table.column.type.AbstractTableColumnType value) {
        setType(value);
        return (TableColumn) this;
    }
    
    public <T extends org.clawiz.core.common.metadata.data.database.table.column.type.AbstractTableColumnType> T createType(Class<T> nodeClass) {
        if ( getType() != null ) {
            throwException("Cannot create already defined value of '?' for '?'", "type", this.getFullName());
        }
        org.clawiz.core.common.metadata.data.database.table.column.type.AbstractTableColumnType value = createChildNode(nodeClass, "type");
        setType(value);
        return (T) value;
    }
    
    public org.clawiz.core.common.metadata.data.database.table.column.type.AbstractTableColumnType createType() {
        return createType(org.clawiz.core.common.metadata.data.database.table.column.type.AbstractTableColumnType.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getType() != null ) { 
            getType().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getType());
        
    }
}
