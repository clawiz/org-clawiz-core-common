package org.clawiz.core.common.storage.synonymtype;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;

public class SynonymTypeServicePrototype extends AbstractTypeService<SynonymTypeObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.core.common.storage", "SynonymType");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.SynonymType"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      synonymType Checked object
    */
    public void check(SynonymTypeObject synonymType) {
        
        if ( synonymType == null ) {
            throwException("Cannot check null ?", new Object[]{"SynonymTypeObject"});
        }
        
        synonymType.fillDefaults();
        
        
        if ( nameToId(synonymType.getName(), synonymType.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "SynonymType", synonymType.toName() });
        }
        
    }
    
    /**
    * Create new SynonymTypeObject instance and fill default values
    * 
    * @return     Created object
    */
    public SynonymTypeObject create() {
        
        SynonymTypeObject synonymType = new SynonymTypeObject();
        synonymType.setService((SynonymTypeService) this);
        
        synonymType.fillDefaults();
        
        return synonymType;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public SynonymTypeObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select name, description from cw_core_synonym_types where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"SynonymType", id});
        }
        
        SynonymTypeObject result = new SynonymTypeObject();
        
        result.setService((SynonymTypeService) this);
        result.setId(id);
        result.setName(statement.getString(1));
        result.setDescription(statement.getString(2));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of SynonymTypeObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public SynonymTypeList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of SynonymTypeObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public SynonymTypeList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        SynonymTypeList result = new SynonymTypeList();
        
        
        Statement statement = executeQuery("select id, name, description from cw_core_synonym_types"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            SynonymTypeObject object = new SynonymTypeObject();
        
            object.setService((SynonymTypeService) this);
            object.setId(statement.getBigDecimal(1));
            object.setName(statement.getString(2));
            object.setDescription(statement.getString(3));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'Name' fields
    * 
    * @param      name Name
    * @return     Id of found record or null
    */
    public BigDecimal nameToId(java.lang.String name) {
        return nameToId(name, null);
    }
    
    /**
    * Find id of record by key 'Name' fields with id not equal skipId
    * 
    * @param      name   Name
    * @param      skipId Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal nameToId(java.lang.String name, BigDecimal skipId) {
        
        if ( name == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_synonym_types where upper_name = ?", name.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_synonym_types where upper_name = ? and id != ?", name.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'Name' fields or create new record with values set to given parameters
    * 
    * @param      name                Name
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal nameToId(java.lang.String name, boolean createNewIfNotFound) {
        
        BigDecimal id = nameToId(name, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        SynonymTypeObject object = create();
        object.setName(name);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'Name' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select name from cw_core_synonym_types where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        SynonymTypeObject object = new SynonymTypeObject();
        object.setService((SynonymTypeService)this);
        object.setId(id);
        object.setName(statement.getString(1));
        
        statement.close();
        
        return object.toName();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToName(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      synonymTypeObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(SynonymTypeObject synonymTypeObject) {
        return synonymTypeObject.toName();
    }
    
    protected void saveAudit(TransactionAction transactionAction, SynonymTypeObject oldSynonymTypeObject, SynonymTypeObject newSynonymTypeObject) {
        
        SynonymTypeObject o = oldSynonymTypeObject != null ? oldSynonymTypeObject : new SynonymTypeObject();
        SynonymTypeObject n = newSynonymTypeObject != null ? newSynonymTypeObject : new SynonymTypeObject();
        
        
        executeUpdate("insert into a_cw_core_synonym_types (scn, action_type, id , o_name, o_upper_name, o_description, n_name, n_upper_name, n_description) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getName(), o.getName() != null ? o.getName().toUpperCase() : null, o.getDescription(), n.getName(), n.getName() != null ? n.getName().toUpperCase() : null, n.getDescription());
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      synonymType Saved object
    */
    public void save(SynonymTypeObject synonymType) {
        
        if ( synonymType == null ) {
            throwException("Cannot save NULL ?", new Object[]{"SynonymTypeObject"});
        }
        
        TransactionAction transactionAction;
        SynonymTypeObject oldSynonymType;
        if ( synonymType.getService() == null ) {
            synonymType.setService((SynonymTypeService)this);
        }
        
        check(synonymType);
        
        if ( synonymType.getId() == null ) {
        
            synonymType.setId(getObjectService().createObject(getTypeId()));
        
            oldSynonymType = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, synonymType.getId());
        
            executeUpdate("insert into cw_core_synonym_types" 
                          + "( id, name, upper_name, description ) "
                          + "values"
                          + "(?, ?, ?, ?)",
                          synonymType.getId(), synonymType.getName(), ( synonymType.getName() != null ? synonymType.getName().toUpperCase() : null ), synonymType.getDescription() );
        
        } else {
        
            oldSynonymType = load(synonymType.getId());
            if ( oldSynonymType.equals(synonymType) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, synonymType.getId());
        
            executeUpdate("update cw_core_synonym_types set "
                          + "name = ?, upper_name = ?, description = ? "
                          + "where id = ?",
                          synonymType.getName(), ( synonymType.getName() != null ? synonymType.getName().toUpperCase() : null ), synonymType.getDescription(), synonymType.getId() );
        
        }
        
        saveAudit(transactionAction, oldSynonymType, synonymType);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        SynonymTypeObject oldSynonymTypeObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldSynonymTypeObject, null);
        
        getObjectService().deleteObject(id);
        
        executeUpdate("delete from cw_core_synonym_types where id = ?", id);
        
    }
}
