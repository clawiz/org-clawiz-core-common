package org.clawiz.core.common.metadata.data.common.valuetype;

import java.math.BigDecimal;
import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ValueTypeStringPrototype extends org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType {
    
    @ExchangeAttribute
    private BigDecimal length;
    
    public ValueTypeString withName(String value) {
        setName(value);
        return (ValueTypeString) this;
    }
    
    public BigDecimal getLength() {
        return this.length;
    }
    
    public void setLength(BigDecimal value) {
        this.length = value;
    }
    
    public ValueTypeString withLength(BigDecimal value) {
        setLength(value);
        return (ValueTypeString) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
