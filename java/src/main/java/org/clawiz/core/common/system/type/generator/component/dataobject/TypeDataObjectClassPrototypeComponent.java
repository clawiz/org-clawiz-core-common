/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator.component.dataobject;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.generator.java.component.element.JavaMemberElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaVariableElement;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.type.key.TypeKey;
import org.clawiz.core.common.system.type.generator.component.AbstractTypeGeneratorJavaClassComponent;
import org.clawiz.core.common.system.type.generator.component.dataobject.element.*;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeBoolean;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;

import java.math.BigDecimal;
import java.sql.Clob;
import java.util.ArrayList;
import java.util.Date;

public class TypeDataObjectClassPrototypeComponent extends AbstractTypeGeneratorJavaClassComponent {


    protected void addVariables() {

        ArrayList<JavaVariableElement> variables = new ArrayList<>();

        for ( TypeField field : getFields() ) {

            JavaVariableElement ve = addVariable(field.getValueTypeJavaClassName(),
                    field.getJavaVariableName()).withAccessLevel(JavaMemberElement.AccessLevel.PRIVATE);
            if ( field.getDescription() != null ) {
                ve.addJavaDoc(field.getDescription());
            }
            if ( field.getValueType() instanceof ValueTypeObject
                    && ((ValueTypeObject) field.getValueType()).getReferencedType() != null ) {
                addVariable(((ValueTypeObject) field.getValueType()).getValueTypeFullClassName(),
                        field.getJavaVariableNameWithoutId()).withAccessLevel(JavaMemberElement.AccessLevel.PRIVATE);
            }

        }

        JavaVariableElement typeServiceVariable = addVariable(getServiceClassName(), "service");

        for ( TypeField field : getFields() ) {

            if ( field.getValueType() instanceof ValueTypeList
                    && ((ValueTypeList) field.getValueType()).getReferencedType() != null ) {
                addElement(TypeDataObjectFieldGetListMethodElement.class).setField(field);
                addElement(TypeDataObjectFieldCreateListMethodElement.class).setField(field);
            } else {
                addElement(TypeDataObjectFieldGetMethodElement.class).setField(field);
            }

            if ( field.getValueType() instanceof ValueTypeObject
                    && ((ValueTypeObject) field.getValueType()).getReferencedType() != null ) {
                addElement(TypeDataObjectFieldGetObjectMethodElement.class).setField(field);
                addElement(TypeDataObjectFieldGetOrCreateObjectMethodElement.class).setField(field);
//                addElement(TypeDataObjectFieldCreateObjectMethodElement.class).setField(field);
            }


            addElement(TypeDataObjectFieldSetMethodElement.class).setField(field);
            if ( field.getValueType() instanceof ValueTypeBoolean) {
                addElement(TypeDataObjectFieldSet2MethodElement.class).setField(field);
            }


            addElement(TypeDataObjectFieldWithMethodElement.class).setField(field);
            if ( field.getValueType() instanceof ValueTypeBoolean ) {
                addElement(TypeDataObjectFieldWith2MethodElement.class).setField(field);
            }

        }

        for (JavaVariableElement ve : variables ) {
            ve.setAccessLevel(JavaMemberElement.AccessLevel.PRIVATE);
            ve.addGetter();
            ve.addSetter();
        }

        typeServiceVariable.addGetter();
        JavaMethodElement setTypeService = addMethod("setService");
        setTypeService.addParameter(typeServiceVariable.getType(), "service");
        setTypeService.addText("this.service = service;");

    }


    @Override
    public void process() {
        super.process();

        setName(getGenerator().getDataObjectClassName() + "Prototype");

        setExtends("AbstractObject");

        addImport(CoreException.class);
        addImport(AbstractObject.class);
        addImport(Session.class);
        addImport(BigDecimal.class);
        addImport(Date.class);
        addImport(Clob.class);
        addImport(StringUtils.class);
        addImport(DateUtils.class);

        addVariables();

        addElement(TypeDataObjectCopyMethodElement.class);
        addElement(TypeDataObjectFillDefaultsMethodElement.class);
        addElement(TypeDataObjectCheckMethodElement.class);
        addElement(TypeDataObjectSaveMethodElement.class);
        addElement(TypeDataObjectEqualsMethodElement.class);
        addElement(TypeDataObjectHashCodeMethodElement.class);

        for (TypeKey key : getKeys() ) {
            addElement(TypeDataObjectToKeyStringMethodElement.class).setKey(key);
        }

    }
}
