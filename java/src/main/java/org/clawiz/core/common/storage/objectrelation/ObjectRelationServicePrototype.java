package org.clawiz.core.common.storage.objectrelation;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;

public class ObjectRelationServicePrototype extends AbstractTypeService<ObjectRelationObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.core.common.storage", "ObjectRelation");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.ObjectRelation"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      objectRelation Checked object
    */
    public void check(ObjectRelationObject objectRelation) {
        
        if ( objectRelation == null ) {
            throwException("Cannot check null ?", new Object[]{"ObjectRelationObject"});
        }
        
        objectRelation.fillDefaults();
        
        
        if ( sourceDestinationTypeToId(objectRelation.getSourceObjectId(), objectRelation.getDestinationObjectId(), objectRelation.getRelationTypeId(), objectRelation.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "ObjectRelation", objectRelation.toSourceDestinationType() });
        }
        
    }
    
    /**
    * Create new ObjectRelationObject instance and fill default values
    * 
    * @return     Created object
    */
    public ObjectRelationObject create() {
        
        ObjectRelationObject objectRelation = new ObjectRelationObject();
        objectRelation.setService((ObjectRelationService) this);
        
        objectRelation.fillDefaults();
        
        return objectRelation;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public ObjectRelationObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select source_object_id, destination_object_id, relation_type_id, probability, distance, weight from cw_core_object_relations where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"ObjectRelation", id});
        }
        
        ObjectRelationObject result = new ObjectRelationObject();
        
        result.setService((ObjectRelationService) this);
        result.setId(id);
        result.setSourceObjectId(statement.getBigDecimal(1));
        result.setDestinationObjectId(statement.getBigDecimal(2));
        result.setRelationTypeId(statement.getBigDecimal(3));
        result.setProbability(statement.getBigDecimal(4));
        result.setDistance(statement.getBigDecimal(5));
        result.setWeight(statement.getBigDecimal(6));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of ObjectRelationObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public ObjectRelationList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of ObjectRelationObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public ObjectRelationList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        ObjectRelationList result = new ObjectRelationList();
        
        
        Statement statement = executeQuery("select id, source_object_id, destination_object_id, relation_type_id, probability, distance, weight from cw_core_object_relations"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            ObjectRelationObject object = new ObjectRelationObject();
        
            object.setService((ObjectRelationService) this);
            object.setId(statement.getBigDecimal(1));
            object.setSourceObjectId(statement.getBigDecimal(2));
            object.setDestinationObjectId(statement.getBigDecimal(3));
            object.setRelationTypeId(statement.getBigDecimal(4));
            object.setProbability(statement.getBigDecimal(5));
            object.setDistance(statement.getBigDecimal(6));
            object.setWeight(statement.getBigDecimal(7));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'SourceDestinationType' fields
    * 
    * @param      sourceObjectId      SourceObject
    * @param      destinationObjectId DestinationObject
    * @param      relationTypeId      RelationType
    * @return     Id of found record or null
    */
    public BigDecimal sourceDestinationTypeToId(java.math.BigDecimal sourceObjectId, java.math.BigDecimal destinationObjectId, java.math.BigDecimal relationTypeId) {
        return sourceDestinationTypeToId(sourceObjectId, destinationObjectId, relationTypeId, null);
    }
    
    /**
    * Find id of record by key 'SourceDestinationType' fields with id not equal skipId
    * 
    * @param      sourceObjectId      SourceObject
    * @param      destinationObjectId DestinationObject
    * @param      relationTypeId      RelationType
    * @param      skipId              Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal sourceDestinationTypeToId(java.math.BigDecimal sourceObjectId, java.math.BigDecimal destinationObjectId, java.math.BigDecimal relationTypeId, BigDecimal skipId) {
        
        if ( sourceObjectId == null || destinationObjectId == null || relationTypeId == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_object_relations where source_object_id = ? and destination_object_id = ? and relation_type_id = ?", sourceObjectId, destinationObjectId, relationTypeId);
        } else {
            return executeQueryBigDecimal("select id from cw_core_object_relations where source_object_id = ? and destination_object_id = ? and relation_type_id = ? and id != ?", sourceObjectId, destinationObjectId, relationTypeId, skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'SourceDestinationType' fields or create new record with values set to given parameters
    * 
    * @param      sourceObjectId      SourceObject
    * @param      destinationObjectId DestinationObject
    * @param      relationTypeId      RelationType
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal sourceDestinationTypeToId(java.math.BigDecimal sourceObjectId, java.math.BigDecimal destinationObjectId, java.math.BigDecimal relationTypeId, boolean createNewIfNotFound) {
        
        BigDecimal id = sourceDestinationTypeToId(sourceObjectId, destinationObjectId, relationTypeId, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        ObjectRelationObject object = create();
        object.setSourceObjectId(sourceObjectId);
        object.setDestinationObjectId(destinationObjectId);
        object.setRelationTypeId(relationTypeId);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'SourceDestinationType' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToSourceDestinationType(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select source_object_id, destination_object_id, relation_type_id from cw_core_object_relations where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        ObjectRelationObject object = new ObjectRelationObject();
        object.setService((ObjectRelationService)this);
        object.setId(id);
        object.setSourceObjectId(statement.getBigDecimal(1));
        object.setDestinationObjectId(statement.getBigDecimal(2));
        object.setRelationTypeId(statement.getBigDecimal(3));
        
        statement.close();
        
        return object.toSourceDestinationType();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'SourceRelation' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToSourceRelation(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select source_object_id, relation_type_id from cw_core_object_relations where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        ObjectRelationObject object = new ObjectRelationObject();
        object.setService((ObjectRelationService)this);
        object.setId(id);
        object.setSourceObjectId(statement.getBigDecimal(1));
        object.setRelationTypeId(statement.getBigDecimal(2));
        
        statement.close();
        
        return object.toSourceRelation();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'DestinationRelation' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToDestinationRelation(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select destination_object_id, relation_type_id from cw_core_object_relations where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        ObjectRelationObject object = new ObjectRelationObject();
        object.setService((ObjectRelationService)this);
        object.setId(id);
        object.setDestinationObjectId(statement.getBigDecimal(1));
        object.setRelationTypeId(statement.getBigDecimal(2));
        
        statement.close();
        
        return object.toDestinationRelation();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToSourceDestinationType(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      objectRelationObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(ObjectRelationObject objectRelationObject) {
        return objectRelationObject.toSourceDestinationType();
    }
    
    protected void saveAudit(TransactionAction transactionAction, ObjectRelationObject oldObjectRelationObject, ObjectRelationObject newObjectRelationObject) {
        
        ObjectRelationObject o = oldObjectRelationObject != null ? oldObjectRelationObject : new ObjectRelationObject();
        ObjectRelationObject n = newObjectRelationObject != null ? newObjectRelationObject : new ObjectRelationObject();
        
        
        executeUpdate("insert into a_cw_core_object_relations (scn, action_type, id , o_source_object_id, o_destination_object_id, o_relation_type_id, o_probability, o_distance, o_weight, n_source_object_id, n_destination_object_id, n_relation_type_id, n_probability, n_distance, n_weight) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getSourceObjectId(), o.getDestinationObjectId(), o.getRelationTypeId(), o.getProbability(), o.getDistance(), o.getWeight(), n.getSourceObjectId(), n.getDestinationObjectId(), n.getRelationTypeId(), n.getProbability(), n.getDistance(), n.getWeight());
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      objectRelation Saved object
    */
    public void save(ObjectRelationObject objectRelation) {
        
        if ( objectRelation == null ) {
            throwException("Cannot save NULL ?", new Object[]{"ObjectRelationObject"});
        }
        
        TransactionAction transactionAction;
        ObjectRelationObject oldObjectRelation;
        if ( objectRelation.getService() == null ) {
            objectRelation.setService((ObjectRelationService)this);
        }
        
        check(objectRelation);
        
        if ( objectRelation.getId() == null ) {
        
            objectRelation.setId(getObjectService().createObject(getTypeId()));
        
            oldObjectRelation = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, objectRelation.getId());
        
            executeUpdate("insert into cw_core_object_relations" 
                          + "( id, source_object_id, destination_object_id, relation_type_id, probability, distance, weight ) "
                          + "values"
                          + "(?, ?, ?, ?, ?, ?, ?)",
                          objectRelation.getId(), objectRelation.getSourceObjectId(), objectRelation.getDestinationObjectId(), objectRelation.getRelationTypeId(), objectRelation.getProbability(), objectRelation.getDistance(), objectRelation.getWeight() );
        
            if ( objectRelation.getSourceObjectId() != null ) { getObjectService().setLink(objectRelation.getSourceObjectId(), objectRelation.getId()); }
            if ( objectRelation.getDestinationObjectId() != null ) { getObjectService().setLink(objectRelation.getDestinationObjectId(), objectRelation.getId()); }
            if ( objectRelation.getRelationTypeId() != null ) { getObjectService().setLink(objectRelation.getRelationTypeId(), objectRelation.getId()); }
        
        } else {
        
            oldObjectRelation = load(objectRelation.getId());
            if ( oldObjectRelation.equals(objectRelation) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, objectRelation.getId());
        
            executeUpdate("update cw_core_object_relations set "
                          + "source_object_id = ?, destination_object_id = ?, relation_type_id = ?, probability = ?, distance = ?, weight = ? "
                          + "where id = ?",
                          objectRelation.getSourceObjectId(), objectRelation.getDestinationObjectId(), objectRelation.getRelationTypeId(), objectRelation.getProbability(), objectRelation.getDistance(), objectRelation.getWeight(), objectRelation.getId() );
        
            getObjectService().changeLinkParent(oldObjectRelation.getSourceObjectId(), objectRelation.getSourceObjectId(), objectRelation.getId());
            getObjectService().changeLinkParent(oldObjectRelation.getDestinationObjectId(), objectRelation.getDestinationObjectId(), objectRelation.getId());
            getObjectService().changeLinkParent(oldObjectRelation.getRelationTypeId(), objectRelation.getRelationTypeId(), objectRelation.getId());
        
        }
        
        saveAudit(transactionAction, oldObjectRelation, objectRelation);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        ObjectRelationObject oldObjectRelationObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldObjectRelationObject, null);
        
        getObjectService().deleteObject(id);
        
        if (oldObjectRelationObject.getSourceObjectId() != null) { getObjectService().deleteLink(oldObjectRelationObject.getSourceObjectId(), id); }
        if (oldObjectRelationObject.getDestinationObjectId() != null) { getObjectService().deleteLink(oldObjectRelationObject.getDestinationObjectId(), id); }
        if (oldObjectRelationObject.getRelationTypeId() != null) { getObjectService().deleteLink(oldObjectRelationObject.getRelationTypeId(), id); }
        
        executeUpdate("delete from cw_core_object_relations where id = ?", id);
        
    }
}
