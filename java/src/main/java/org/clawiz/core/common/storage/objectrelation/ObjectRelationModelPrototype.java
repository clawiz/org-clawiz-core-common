package org.clawiz.core.common.storage.objectrelation;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;

public class ObjectRelationModelPrototype extends TypeModel {
    
    private static TypeField SOURCE_OBJECT_ID_FIELD;
    
    private static TypeField DESTINATION_OBJECT_ID_FIELD;
    
    private static TypeField RELATION_TYPE_ID_FIELD;
    
    private static TypeField PROBABILITY_FIELD;
    
    private static TypeField DISTANCE_FIELD;
    
    private static TypeField WEIGHT_FIELD;
    
    private static Type type;
    
    private ObjectRelationService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public ObjectRelationService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(ObjectRelationService.class).getType();
        
        
        SOURCE_OBJECT_ID_FIELD = type.getFields().get("SourceObject");
        if ( SOURCE_OBJECT_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.ObjectRelation.SourceObject"}); }
        
        DESTINATION_OBJECT_ID_FIELD = type.getFields().get("DestinationObject");
        if ( DESTINATION_OBJECT_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.ObjectRelation.DestinationObject"}); }
        
        RELATION_TYPE_ID_FIELD = type.getFields().get("RelationType");
        if ( RELATION_TYPE_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.ObjectRelation.RelationType"}); }
        
        PROBABILITY_FIELD = type.getFields().get("Probability");
        if ( PROBABILITY_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.ObjectRelation.Probability"}); }
        
        DISTANCE_FIELD = type.getFields().get("distance");
        if ( DISTANCE_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.ObjectRelation.distance"}); }
        
        WEIGHT_FIELD = type.getFields().get("weight");
        if ( WEIGHT_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.ObjectRelation.weight"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _sourceObjectId;
    
    public TypeFieldModel sourceObjectId() {
        
        if ( _sourceObjectId != null ) {
            return _sourceObjectId;
        }
        
        _sourceObjectId = new TypeFieldModel(this, SOURCE_OBJECT_ID_FIELD);
        return _sourceObjectId;
        
    }
    
    private TypeFieldModel _destinationObjectId;
    
    public TypeFieldModel destinationObjectId() {
        
        if ( _destinationObjectId != null ) {
            return _destinationObjectId;
        }
        
        _destinationObjectId = new TypeFieldModel(this, DESTINATION_OBJECT_ID_FIELD);
        return _destinationObjectId;
        
    }
    
    private TypeFieldModel _relationTypeId;
    
    public TypeFieldModel relationTypeId() {
        
        if ( _relationTypeId != null ) {
            return _relationTypeId;
        }
        
        _relationTypeId = new TypeFieldModel(this, RELATION_TYPE_ID_FIELD);
        return _relationTypeId;
        
    }
    
    private TypeFieldModel _probability;
    
    public TypeFieldModel probability() {
        
        if ( _probability != null ) {
            return _probability;
        }
        
        _probability = new TypeFieldModel(this, PROBABILITY_FIELD);
        return _probability;
        
    }
    
    private TypeFieldModel _distance;
    
    public TypeFieldModel distance() {
        
        if ( _distance != null ) {
            return _distance;
        }
        
        _distance = new TypeFieldModel(this, DISTANCE_FIELD);
        return _distance;
        
    }
    
    private TypeFieldModel _weight;
    
    public TypeFieldModel weight() {
        
        if ( _weight != null ) {
            return _weight;
        }
        
        _weight = new TypeFieldModel(this, WEIGHT_FIELD);
        return _weight;
        
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "SOURCEOBJECT" : return sourceObjectId();
        case "DESTINATIONOBJECT" : return destinationObjectId();
        case "RELATIONTYPE" : return relationTypeId();
        case "PROBABILITY" : return probability();
        case "DISTANCE" : return distance();
        case "WEIGHT" : return weight();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
