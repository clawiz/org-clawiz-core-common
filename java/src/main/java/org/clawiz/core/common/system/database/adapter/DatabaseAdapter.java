/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.database.adapter;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.database.metadata.*;
import org.clawiz.core.common.utils.StringUtils;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * (c) Clawiz
 */
@NotInitializeService
public abstract class DatabaseAdapter extends Service {

    private DatabaseMetaData metaData;


    /*
        Common database specific methods
     */

    protected String getColumnNumberTypeName() {
        return "NUMBER";
    }

    protected String getColumnDateTypeName() {
        return "DATE";
    }
    protected String getColumnDateTimeTypeName() {
        return getColumnDateTypeName();
    }
    protected String getColumnVarcharTypeName() {
        return "VARCHAR";
    }

    protected DatabaseTableColumn.Type metadataColumnTypeToDatabaseColumnType(DatabaseTableColumn column, String typeName) {

        if ( typeName == null ) {
            throw new CoreException("Table ? column ? type cannot be NULL", column.getTable().getName(), column.getName());
        }
        if ( getColumnNumberTypeName().equalsIgnoreCase(typeName)) {
            return DatabaseTableColumn.Type.NUMBER;
        }
        if ( getColumnDateTypeName().equalsIgnoreCase(typeName)) {
            return DatabaseTableColumn.Type.DATE;
        }
        if ( getColumnVarcharTypeName().equalsIgnoreCase(typeName)) {
            return DatabaseTableColumn.Type.VARCHAR;
        }
        throw new CoreException("Unsupported table ? column ? type ?", column.getTable().getName(), column.getName(), typeName);
    }


    protected void prepareDatabaseColumnType(DatabaseTableColumn column, String typeName, int columnSize, int decimalDigits) {
        column.setType(metadataColumnTypeToDatabaseColumnType(column, typeName));
        column.setPrecision(columnSize);
        column.setScale(decimalDigits);
    }
    protected String getTableColumnTypeDDL(DatabaseTableColumn column) {

        if ( column.getType() == null ) {
            throwException("Table ? column ? type cannot be null", column.getTable().getName(), column.getName());
        }

        switch (column.getType() ) {
            case VARCHAR: return getColumnVarcharTypeName() + "(" + column.getPrecision() + ")";
            case NUMBER:  return getColumnNumberTypeName()
                    + (column.getPrecision() == 0 ? ""
                    : (column.getScale() == 0 ? "(" + column.getPrecision() + ")"
                    : "(" + column.getPrecision() + "," + column.getScale()+ ")"));
            case DATE:
            case DATETIME:    return getColumnDateTimeTypeName();
        }

        throw new CoreException("Unsupported table ? column ? type ?", column.getTable().getName(), column.getName(), column.getType());
    }

    public String getUpperStringFunctionName() {
        return "UPPER";
    }

    abstract protected String getStringConcatenationOperator();


    public synchronized DatabaseMetaData getMetaData() {
        if ( metaData == null ) {
            try {
                metaData = getSession().getConnection().getJDBCConnection().getMetaData();
            } catch (SQLException e) {
                throwException("Exception on get database metadata ?", e.getMessage(), e);
            }
        }
        return metaData;
    }

    public boolean isTableExists(String name) {
        if (name == null) {
            return false;
        }

        boolean found;
        try {
            ResultSet rs = getMetaData().getTables(null, null, name.toUpperCase(), null); //tableName.toUpperCase(), null);
            found        = rs.next();
            rs.close();

            if ( ! found ) {
                rs = getMetaData().getTables(null, null, name.toLowerCase(), null); //tableName.toUpperCase(), null);
                found        = rs.next();
                rs.close();
            }

            if ( ! found ) {
                rs = getMetaData().getTables(null, null, null, null); //tableName.toUpperCase(), null);
                while ( rs.next() ) {
                    if ( name.equalsIgnoreCase(rs.getString(3))) {
                        found = true;
                        break;
                    }
                }
                rs.close();
            }

        } catch (SQLException e) {
            throw new CoreException("Exception on check is table '?' existing in metadata : ?", name, e.getMessage(), e);
        }
        return found;
    }


    protected DatabaseTable loadTableDefinition(String tableName) throws SQLException {


        ResultSet rs  = getMetaData().getTables(null, null, tableName.toUpperCase(), null); //tableName.toUpperCase(), null);
        boolean found = rs.next();

        if ( ! found ) {
            rs.close();
            rs = getMetaData().getTables(null, null, tableName.toLowerCase(), null); //tableName.toUpperCase(), null);
            found        = rs.next();
        }

        if ( ! found ) {
            rs.close();
            rs = getMetaData().getTables(null, null, null, null); //tableName.toUpperCase(), null);
            while ( rs.next() ) {
                if ( tableName.equalsIgnoreCase(rs.getString(3))) {
                    found = true;
                    break;
                }
            }
        }

        if ( ! found ) {
            rs.close();
            return null;
        }

        DatabaseTable table = new DatabaseTable(rs.getString(3), rs.getString("REMARKS"));
        rs.close();

        return table;
    }

    protected void loadTableColumns(DatabaseTable table) throws SQLException {

        try {
            ResultSet columnResultSet = getMetaData().getColumns(null, null, table.getName(), null);
            while ( columnResultSet.next()) {
                DatabaseTableColumn column = new DatabaseTableColumn();
                table.addColumn(column);
                column.setName(columnResultSet.getString("COLUMN_NAME"));
                prepareDatabaseColumnType(column
                        ,columnResultSet.getString("TYPE_NAME")
                        ,columnResultSet.getInt("COLUMN_SIZE")
                        ,columnResultSet.getInt("DECIMAL_DIGITS"));
                column.setRequired("NO".equals(columnResultSet.getString("IS_NULLABLE")));
                column.setComment(columnResultSet.getString("REMARKS"));
            }
            columnResultSet.close();
        } catch (SQLException e) {
            throw new CoreException("Exception on get table '?' columns : ?", table.getName(), e.getMessage(), e);
        }

    }

    protected void loadTableIndexes(DatabaseTable table) throws SQLException  {
        ResultSet indexResultSet = getMetaData().getIndexInfo(null, null, table.getName(), false, false);
        DatabaseTableIndex index = null;
        while (indexResultSet.next() ) {
            if ( index == null || ! indexResultSet.getString("INDEX_NAME").equalsIgnoreCase(index.getName()) ) {
                index = new DatabaseTableIndex();
                table.addIndex(index);
                index.setName(indexResultSet.getString("INDEX_NAME"));
                index.setUnique(! indexResultSet.getBoolean("NON_UNIQUE"));
            }
            index.addColumn(new DatabaseTableIndexColumn(indexResultSet.getString("COLUMN_NAME")));
        }

    }

    public DatabaseTable getTable(String name) {

        try {
            DatabaseTable table = loadTableDefinition(name);
            if ( table == null ) {
                return null;
            }

            loadTableColumns(table);

            loadTableIndexes(table);

            return table;
        } catch (SQLException e) {
            throw new CoreException("Exception on get table '?' definition : ?", name, e.getMessage(), e);
        }

    }



    protected String getCreateTableDDL(DatabaseTable table) {
        StringBuilder sb = new StringBuilder();
        sb.append("create table " + table.getName().toLowerCase() + " ( ");

        for (int i = 0; i < table.getColumns().size(); i++) {
            sb.append((i > 0 ? ", " : ""));
            sb.append(table.getColumns().get(i).getName() + " " + getTableColumnTypeDDL(table.getColumns().get(i)));
        }

        sb.append(")");
        return sb.toString();
    }


    protected String getCreateIndexDDL(DatabaseTableIndex index) {
        return "create " + (index.isUnique() ? "unique " : "") + "index " + index.getName() + " on " + index.getTable().getName() + " ( " + index.getColumnsString() + " )";
    }

    protected String getDropIndexDDL(DatabaseTableIndex index) {
        return "drop index " + index.getName();
    }


    public String getAlterTableAddColumnStatement(DatabaseTable table, DatabaseTableColumn column) {
        return "alter table " + table.getName() + " add " + column.getName() + " " + getTableColumnTypeDDL(column);
    }

    public String getAlterTableModifyColumnStatement(DatabaseTable table, DatabaseTableColumn column) {
        return "alter table " + table.getName() + " modify " + column.getName() + " " + getTableColumnTypeDDL(column);
    }

    public void implementTable(DatabaseTable table) {

        DatabaseTable databaseTable = getTable(table.getName());
        if ( databaseTable == null ) {
            executeUpdate(getCreateTableDDL(table));
        } else {
            for (DatabaseTableColumn column : table.getColumns() ) {
                DatabaseTableColumn databaseColumn = databaseTable.getColumns().get(column.getName());
                if ( databaseColumn == null ) {
                    executeUpdate(getAlterTableAddColumnStatement(table, column));
                } else if ( ! databaseColumn.equals(column) ) {
                    String alterStatement = getAlterTableModifyColumnStatement(table, column);
                    if ( alterStatement != null ) {
                        executeUpdate(alterStatement);
                    }
                }
            }
        }

        HashMap<String, String> usedIndexes      = new HashMap<>();
        ArrayList<DatabaseTableIndex> newIndexes = new ArrayList<>();
        HashMap<String, String> droppedIndexes   = new HashMap<>();

        if (databaseTable == null ) {
            databaseTable = new DatabaseTable(table.getName());
        }

        for (DatabaseTableIndex index : table.getIndexes() ) {
            DatabaseTableIndex found = null;
            for ( DatabaseTableIndex databaseIndex : databaseTable.getIndexes() ) {
                if ( databaseIndex.equals(index)) {
                    found = databaseIndex;
                    break;
                }
            }
            if ( found == null ) {
                newIndexes.add(index);
            }
        }

        for (DatabaseTableIndex databaseIndex : databaseTable.getIndexes() ) {
            DatabaseTableIndex found = null;
            for ( DatabaseTableIndex index : table.getIndexes() ) {
                if ( databaseIndex.equals(index)) {
                    found = databaseIndex;
                    break;
                }
            }
            if ( found != null ) {
                usedIndexes.put(found.getName().toUpperCase(), found.getName());
            }
        }

        for (DatabaseTableIndex index : databaseTable.getIndexes() ) {
            if ( ! usedIndexes.containsKey(index.getName().toUpperCase())) {
                logDebug("Drop unused index " + table.getName() + "." + index.getName() + "( " + index.getColumnsString() + " )");
                executeUpdate(getDropIndexDDL(index));
                droppedIndexes.put(index.getName().toUpperCase(), index.getName());
            }
        }


        int counter = 1;
        HashMap<String, String> createdIndexes = new HashMap<>();
        for ( DatabaseTableIndex index : newIndexes ) {

            String indexName = index.getName() != null ? index.getName() : StringUtils.substring(table.getName(), 0, 25) + "_i" + counter++;
            while (
                         databaseTable.getIndexes().get(indexName.toUpperCase()) != null
                    && ! droppedIndexes.containsKey(indexName.toUpperCase())
                    || createdIndexes.containsKey(indexName.toUpperCase())) {
                indexName = StringUtils.substring(table.getName(), 0, 25) + "_i" + counter++;
            }
            droppedIndexes.remove(indexName.toUpperCase());
            index.setName(indexName);

            executeUpdate(getCreateIndexDDL(index));

            createdIndexes.put(indexName.toUpperCase(), indexName.toUpperCase());
        }


    }

    public void updateTableUpperColumn(String tableName, String columnName, String upperColumnName ) {
        executeUpdate("update " + tableName + " set " + columnName + " = " + getUpperStringFunctionName() + "(" + columnName + " )"
                + " where "
                + columnName + " is null and " + upperColumnName + " is not null "
                + " or " + columnName + " is null and " + upperColumnName + " is not null "
                + " or " + getUpperStringFunctionName() + "(" + columnName + ") <> " + upperColumnName
        );
    }


    public abstract boolean isSequenceExists(DatabaseSequence sequence);

    public abstract void createSequence(DatabaseSequence sequence);

    public void implementSequence(DatabaseSequence sequence) {
        if ( ! isSequenceExists(sequence)) {
            createSequence(sequence);
        }
    }

    public abstract BigDecimal getSequenceNextValue(String sequenceName);

    public String concatenateStringExpressions(ArrayList<String> expressions) {
        StringBuilder sb = new StringBuilder();
        for ( int i=0; i < expressions.size(); i++ ) {
            if ( i > 0 ) { sb.append(" " + getStringConcatenationOperator() + " "); }
            sb.append(expressions.get(i));
        }
        return sb.toString();
    }

    public String prepareSQL(String sql) {
        return sql;
    }

    public Savepoint setSavepoint(String savepointName) {
        try {
            return getSession().getConnection().getJDBCConnection().setSavepoint();
        } catch (SQLException e) {
            throw new CoreException("SQL exception on set savepoint : ?", e.getMessage(), e);
        }
    }

    public void rollbackToSavepoint(Savepoint savepoint) {
        try {
            getSession().getConnection().getJDBCConnection().rollback(savepoint);
        } catch (SQLException e) {
            throw new CoreException("SQL exception on rollback to savepoint : ?", e.getMessage(), e);
        }
    }


    public void initDatabaseConnection() {

    }

    public void setStatementNullParameter(Statement statement, int index ) {
        try {
            statement.getPreparedStatement().setNull(index, Types.VARCHAR);
        } catch (SQLException e) {
            throwException("Exception on set null value for parameter '?' of statement : ?", index, statement.getSql(), e.getMessage(), e);
        }
    }

    public void shutdownDatabase() {}

    /*
       Database specific properties
     */

}
