
/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator;

import java.util.regex.Pattern;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.utils.StringUtils;

public class GeneratorUtils extends Service {

    public static String toJavaClassName(String str ) {
        return toJavaClassName(str, false);
    }

    public static String toJavaClassName(String str, boolean withOriginalRegisters) {
        if ( str == null ) {
            return null;
        }

        String name = toJavaName( str, withOriginalRegisters );

        if( name.length() > 0 && !Character.isJavaIdentifierStart( name.charAt( 0 ) ) ) {
            name = "C" + name;
        }

        if( name.length() > 0 && !Character.isUpperCase( name.charAt( 0 ) ) ) {
            name = Character.toUpperCase( name.charAt( 0 ) ) + name.substring( 1 );
        }

        return removeUnderscore(name, withOriginalRegisters);
    }

    private static String removeUnderscore(String str, boolean withOriginalRegister ) {
        String rex = "_";

        Pattern pattern = Pattern.compile( rex );

        String res[] = pattern.split( str );

        StringBuffer buf = new StringBuffer();

        for( int i = 0; i < res.length; i++ ) {
            String s = withOriginalRegister ? res[i] : res[i].toLowerCase();
            if( s.length() > 1 ) {
                buf.append( Character.toUpperCase( s.charAt( 0 ) ) );
                buf.append( s.substring( 1 ) );
            } else if( s.length() == 1 ) {
                buf.append( Character.toUpperCase( s.charAt( 0 ) ) );
            } else {
                buf.append( "X" );
            }
        }

        return buf.toString();
    }

    public static String toJavaName(String str ) {
        return toJavaName(str, false);
    }

    public static String toJavaName(String str, boolean withOriginalRegisters ) {

        if( str == null ) {
            return null;
        }

        StringBuilder buf = new StringBuilder();

        char[] ch = str.toCharArray();

        String s;

        boolean makeNextUpper = false;
        boolean inSkipNotChar = true;
        for( int i = 0; i < ch.length; i++ ) {

            if ( inSkipNotChar && ! Character.isLetter(ch[i]) ) {
                continue;
            }
            inSkipNotChar = false;

            if ( ch[i] == ' ') {
                makeNextUpper = true;
                continue;
            }
            s = transliterate( ch[i] );

            if ( withOriginalRegisters ) {
                makeNextUpper = makeNextUpper || Character.isUpperCase(ch[i]);
            }

            if ( makeNextUpper ) {
                makeNextUpper = false;
                if ( s != null ) {
                    s = s.toUpperCase();
                }
            }
            if( s != null ) {
                if ( s.length() > 1 ) {
                    s = s.charAt(0) + s.substring(1).toLowerCase();
                }
                buf.append( s );
            } else {
                buf.append( ch[i] );
            }
        }

        if( buf.length() > 0 && buf.charAt( 0 ) == '_' ) {
            buf.delete( 0, 0 );
        }

        if( buf.length() > 0 && buf.charAt( buf.length() - 1 ) == '_' ) {
            buf.delete( buf.length() - 1, buf.length() );
        }

        return buf.toString();
    }

    public static String toJavaNameWithUnderscores(String str) {
        if ( str == null ) {
            return null;
        }
        char[] ch = str.toCharArray();
        StringBuilder sb = new StringBuilder();
        for(int i=0; i < str.length(); i++) {
            char c = ch[i];
            if ( i > 0 && Character.isUpperCase(c)) {
                sb.append("_");
            }
            sb.append(transliterate(c));
        }
        return sb.toString();
    }

    private static String transliterate(char ch ) {

        switch( ch ) {
            case 'а' : return "a";
            case 'б' : return "b";
            case 'в' : return "v";
            case 'г' : return "g";
            case 'д' : return "d";
            case 'е' : return "e";
            case 'ё' : return "e";
            case 'ж' : return "z";
            case 'з' : return "z";
            case 'и' : return "i";
            case 'й' : return "i";
            case 'к' : return "k";
            case 'л' : return "l";
            case 'м' : return "m";
            case 'н' : return "n";
            case 'о' : return "o";
            case 'п' : return "p";
            case 'р' : return "r";
            case 'с' : return "s";
            case 'т' : return "t";
            case 'у' : return "u";
            case 'ф' : return "f";
            case 'х' : return "h";
            case 'ц' : return "c";
            case 'ч' : return "ch";
            case 'ь' : return "b";
            case 'ъ' : return "b";
            case 'ш' : return "sh";
            case 'щ' : return "sch";
            case 'ы' : return "i";
            case 'э' : return "e";
            case 'ю' : return "u";
            case 'я' : return "ia";
            case 'А' : return "A";
            case 'Б' : return "B";
            case 'В' : return "V";
            case 'Г' : return "G";
            case 'Д' : return "D";
            case 'Е' : return "E";
            case 'Ё' : return "E";
            case 'Ж' : return "Z";
            case 'З' : return "Z";
            case 'И' : return "I";
            case 'Й' : return "I";
            case 'К' : return "K";
            case 'Л' : return "L";
            case 'М' : return "M";
            case 'Н' : return "N";
            case 'О' : return "O";
            case 'П' : return "P";
            case 'Р' : return "R";
            case 'С' : return "S";
            case 'Т' : return "T";
            case 'У' : return "U";
            case 'Ф' : return "F";
            case 'Х' : return "H";
            case 'Ц' : return "C";
            case 'Ч' : return "CH";
            case 'Ь' : return "b";
            case 'Ъ' : return "b";
            case 'Ш' : return "SH";
            case 'Щ' : return "SCH";
            case 'Ы' : return "I";
            case 'Э' : return "E";
            case 'Ю' : return "U";
            case 'Я' : return "IA";
        }

        if( Character.isJavaIdentifierPart( ch ) ) {
            return "" + ch;
        }

        return "";
    }


    public static String toUpperWithUnderscore(String str) {

        if (str == null ) {
            return null;
        }

        StringBuilder sb              = new StringBuilder();
        boolean       previousLowCase = false;
        for(char c : str.toCharArray()) {
            if ( Character.isUpperCase(c) && previousLowCase ) {
                sb.append('_');
            }
            previousLowCase = Character.isLowerCase(c);
            sb.append(Character.toUpperCase(c));
        }

        return sb.toString();
    }

    public static String toLowerWithUnderscore(String str) {

        if (str == null ) {
            return null;
        }

        StringBuilder sb              = new StringBuilder();
        boolean       previousLowCase = false;
        for(char c : str.toCharArray()) {
            if ( Character.isUpperCase(c) && previousLowCase ) {
                sb.append('_');
            }
            previousLowCase = Character.isLowerCase(c);
            sb.append(Character.toLowerCase(c));
        }

        return sb.toString();
    }


    public static String getPackageRelativePath(String packagePrefix, String packageName) {
        if ( packageName == null || packagePrefix == null ) {
            throw new CoreException("Package prefix ? and package name ? cannot be null", packagePrefix, packageName);
        }
        if ( packagePrefix.equals(packageName)) {
            return "";
        }
        if ( packagePrefix.length() > packageName.length()
                || packageName.indexOf(packagePrefix) != 0
                || packageName.charAt(packagePrefix.length()) != '.'
                ) {
            throw new CoreException("Package ? is not prefix for ?", packagePrefix, packageName);
        }
        String result = packageName.substring(packagePrefix.length());
        result = result.replaceAll("\\.", "/");
        return result;
    }




    public static String removeDotsAndCapitalize(String name) {
        if ( name == null ) {
            return name;
        }
        String[] tokens = StringUtils.splitAndTrim(name, "\\.");
        StringBuilder sb = new StringBuilder();
        sb.append(tokens[0]);
        for (int i=1; i < tokens.length; i++) {
            sb.append(StringUtils.toUpperFirstChar(tokens[i]));
        }
        return sb.toString();
    }

}
