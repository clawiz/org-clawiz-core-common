package org.clawiz.core.common.storage.synonym;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class SynonymObjectPrototype extends AbstractObject {
    
    /**
    * Object
    */
    private java.math.BigDecimal objectId;
    
    /**
    * SynonymType
    */
    private java.math.BigDecimal synonymTypeId;
    
    private org.clawiz.core.common.storage.synonymtype.SynonymTypeObject synonymType;
    
    /**
    * Value
    */
    private java.lang.String value;
    
    public SynonymService service;
    
    /**
    * 
    * @return     Object
    */
    public java.math.BigDecimal getObjectId() {
        return this.objectId;
    }
    
    /**
    * Set 'Object' value
    * 
    * @param      objectId Object
    */
    public void setObjectId(java.math.BigDecimal objectId) {
         this.objectId = objectId;
    }
    
    /**
    * Set 'Object' value and return this object
    * 
    * @param      objectId Object
    * @return     Synonym object
    */
    public SynonymObjectPrototype withObjectId(java.math.BigDecimal objectId) {
        setObjectId(objectId);
        return this;
    }
    
    /**
    * 
    * @return     SynonymType
    */
    public java.math.BigDecimal getSynonymTypeId() {
        return this.synonymTypeId;
    }
    
    public org.clawiz.core.common.storage.synonymtype.SynonymTypeObject getSynonymType() {
        return getSynonymType(false);
    }
    
    @SuppressWarnings("Duplicates")
    public org.clawiz.core.common.storage.synonymtype.SynonymTypeObject getSynonymType(boolean autoCreate) {
        
        if ( synonymType != null ) { return synonymType; }
        
        if ( synonymTypeId != null ) {
            synonymType = service.loadSynonymType(synonymTypeId);
        } else if ( autoCreate ) {
            synonymType = service.createSynonymType();
        }
        
        return synonymType;
    }
    
    /**
    * Set 'SynonymType' value
    * 
    * @param      synonymTypeId SynonymType
    */
    public void setSynonymTypeId(java.math.BigDecimal synonymTypeId) {
         this.synonymTypeId = synonymTypeId;
    }
    
    /**
    * Set 'SynonymType' value and return this object
    * 
    * @param      synonymTypeId SynonymType
    * @return     Synonym object
    */
    public SynonymObjectPrototype withSynonymTypeId(java.math.BigDecimal synonymTypeId) {
        setSynonymTypeId(synonymTypeId);
        return this;
    }
    
    /**
    * 
    * @return     Value
    */
    public java.lang.String getValue() {
        return this.value;
    }
    
    /**
    * Set 'Value' value
    * 
    * @param      value Value
    */
    public void setValue(java.lang.String value) {
        
        if ( value != null && value.length() > 512) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "Synonym.Value", "512");
        }
        
         this.value = value;
    }
    
    /**
    * Set 'Value' value and return this object
    * 
    * @param      value Value
    * @return     Synonym object
    */
    public SynonymObjectPrototype withValue(java.lang.String value) {
        setValue(value);
        return this;
    }
    
    public SynonymService getService() {
        return this.service;
    }
    
    public void setService(SynonymService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(SynonymObjectPrototype  target) {
        target.setObjectId(getObjectId());
        target.setSynonymTypeId(getSynonymTypeId());
        target.setValue(getValue());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((SynonymObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        if ( synonymType != null ) {
            synonymType.save();
            synonymTypeId = synonymType.getId();
        }
        service.save((SynonymObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(SynonymObjectPrototype object) {
        return 
               isObjectsEquals(this.getObjectId(), object.getObjectId() ) 
            && isObjectsEquals(this.getSynonymTypeId(), object.getSynonymTypeId() ) 
            && isObjectsEquals(this.getValue(), object.getValue() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getObjectId() != null ? getObjectId().hashCode() : 0);
        result = result * 31 + (getSynonymTypeId() != null ? getSynonymTypeId().hashCode() : 0);
        result = result * 31 + (getValue() != null ? getValue().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'ObjectSynonymTypeValue' fields
    * 
    * @return     Concatenated string values of key 'toObjectSynonymTypeValue' fields
    */
    public String toObjectSynonymTypeValue() {
        return service.getObjectService().idToString(getObjectId()) + "," + service.getObjectService().idToString(getSynonymTypeId()) + "," + getValue();
    }
    
    /**
    * Prepare concatenated string values of key 'SynonymTypeValue' fields
    * 
    * @return     Concatenated string values of key 'toSynonymTypeValue' fields
    */
    public String toSynonymTypeValue() {
        return service.getObjectService().idToString(getSynonymTypeId()) + "," + getValue();
    }
}
