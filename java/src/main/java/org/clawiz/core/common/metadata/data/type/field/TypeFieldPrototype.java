package org.clawiz.core.common.metadata.data.type.field;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class TypeFieldPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeAttribute
    private String description;
    
    @ExchangeElement
    private org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType valueType;
    
    @ExchangeElement
    private org.clawiz.core.common.metadata.data.type.field.property.TypeFieldPropertyList properties = new org.clawiz.core.common.metadata.data.type.field.property.TypeFieldPropertyList();
    
    public TypeField withName(String value) {
        setName(value);
        return (TypeField) this;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String value) {
        this.description = value;
    }
    
    public TypeField withDescription(String value) {
        setDescription(value);
        return (TypeField) this;
    }
    
    public org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType getValueType() {
        return this.valueType;
    }
    
    public void setValueType(org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType value) {
        this.valueType = value;
    }
    
    public TypeField withValueType(org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType value) {
        setValueType(value);
        return (TypeField) this;
    }
    
    public <T extends org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType> T createValueType(Class<T> nodeClass) {
        if ( getValueType() != null ) {
            throwException("Cannot create already defined value of '?' for '?'", "valueType", this.getFullName());
        }
        org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType value = createChildNode(nodeClass, "valueType");
        setValueType(value);
        return (T) value;
    }
    
    public org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType createValueType() {
        return createValueType(org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType.class);
    }
    
    public org.clawiz.core.common.metadata.data.type.field.property.TypeFieldPropertyList getProperties() {
        return this.properties;
    }
    
    public TypeField withPropertie(org.clawiz.core.common.metadata.data.type.field.property.AbstractTypeFieldProperty value) {
        getProperties().add(value);
        return (TypeField) this;
    }
    
    public <T extends org.clawiz.core.common.metadata.data.type.field.property.AbstractTypeFieldProperty> T createPropertie(Class<T> nodeClass) {
        org.clawiz.core.common.metadata.data.type.field.property.AbstractTypeFieldProperty value = createChildNode(nodeClass, "properties");
        getProperties().add(value);
        return (T) value;
    }
    
    public org.clawiz.core.common.metadata.data.type.field.property.AbstractTypeFieldProperty createPropertie() {
        return createPropertie(org.clawiz.core.common.metadata.data.type.field.property.AbstractTypeFieldProperty.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getValueType() != null ) { 
            getValueType().prepare(session);
        }
        for (MetadataNode node : getProperties()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getValueType());
        
        for (MetadataNode node : getProperties()) {
            references.add(node);
        }
        
    }
}
