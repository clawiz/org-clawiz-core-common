/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.database.adapter.sqlserver;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.database.metadata.DatabaseSequence;
import org.clawiz.core.common.system.database.metadata.DatabaseTable;
import org.clawiz.core.common.system.database.adapter.DatabaseAdapter;

import java.math.BigDecimal;


public class SQLServerDatabaseAdapter extends DatabaseAdapter {


    @Override
    protected String getStringConcatenationOperator() {
        return "+";
    }

    @Override
    public void implementTable(DatabaseTable table) {
        throw new CoreException("Current SQL Server adapter version not support implementation of database objects");
    }

    @Override
    public void createSequence(DatabaseSequence sequence) {
        throw new CoreException("Current SQL Server adapter version not support implementation of database objects");
    }

    @Override
    public BigDecimal getSequenceNextValue(String sequenceName) {
        throw new CoreException("Current SQL Server adapter version not support implementation of database objects");
    }

    @Override
    public void implementSequence(DatabaseSequence sequence) {
        throw new CoreException("Current SQL Server adapter version not support implementation of database objects");
    }

    @Override
    public boolean isSequenceExists(DatabaseSequence sequence) {
        throw new CoreException("Current SQL Server adapter version not support implementation of database objects");
    }
}
