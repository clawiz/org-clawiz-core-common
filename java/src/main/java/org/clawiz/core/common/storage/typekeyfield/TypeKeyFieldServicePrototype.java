package org.clawiz.core.common.storage.typekeyfield;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;

public class TypeKeyFieldServicePrototype extends AbstractTypeService<TypeKeyFieldObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).fullNameToId("org.clawiz.core.common.storage.TypeKeyField");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeKeyField"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      typeKeyField Checked object
    */
    public void check(TypeKeyFieldObject typeKeyField) {
        
        if ( typeKeyField == null ) {
            throwException("Cannot check null ?", new Object[]{"TypeKeyFieldObject"});
        }
        
        typeKeyField.fillDefaults();
        
        
        if ( keyFieldToId(typeKeyField.getKeyId(), typeKeyField.getFieldId(), typeKeyField.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "TypeKeyField", typeKeyField.toKeyField() });
        }
        
        if ( keyOrderNoToId(typeKeyField.getKeyId(), typeKeyField.getOrderNo(), typeKeyField.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "TypeKeyField", typeKeyField.toKeyOrderNo() });
        }
        
    }
    
    /**
    * Create new TypeKeyFieldObject instance and fill default values
    * 
    * @return     Created object
    */
    public TypeKeyFieldObject create() {
        
        TypeKeyFieldObject typeKeyField = new TypeKeyFieldObject();
        typeKeyField.setService((TypeKeyFieldService) this);
        
        typeKeyField.fillDefaults();
        
        return typeKeyField;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public TypeKeyFieldObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select key_id, order_no, field_id from cw_core_type_key_fields where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"TypeKeyField", id});
        }
        
        TypeKeyFieldObject result = new TypeKeyFieldObject();
        
        result.setService((TypeKeyFieldService) this);
        result.setId(id);
        result.setKeyId(statement.getBigDecimal(1));
        result.setOrderNo(statement.getBigDecimal(2));
        result.setFieldId(statement.getBigDecimal(3));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of TypeKeyFieldObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public TypeKeyFieldList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of TypeKeyFieldObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public TypeKeyFieldList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        TypeKeyFieldList result = new TypeKeyFieldList();
        
        
        Statement statement = executeQuery("select id, key_id, order_no, field_id from cw_core_type_key_fields"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            TypeKeyFieldObject object = new TypeKeyFieldObject();
        
            object.setService((TypeKeyFieldService) this);
            object.setId(statement.getBigDecimal(1));
            object.setKeyId(statement.getBigDecimal(2));
            object.setOrderNo(statement.getBigDecimal(3));
            object.setFieldId(statement.getBigDecimal(4));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'KeyField' fields
    * 
    * @param      keyId   Key
    * @param      fieldId Field
    * @return     Id of found record or null
    */
    public BigDecimal keyFieldToId(java.math.BigDecimal keyId, java.math.BigDecimal fieldId) {
        return keyFieldToId(keyId, fieldId, null);
    }
    
    /**
    * Find id of record by key 'KeyField' fields with id not equal skipId
    * 
    * @param      keyId   Key
    * @param      fieldId Field
    * @param      skipId  Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal keyFieldToId(java.math.BigDecimal keyId, java.math.BigDecimal fieldId, BigDecimal skipId) {
        
        if ( keyId == null || fieldId == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_type_key_fields where key_id = ? and field_id = ?", keyId, fieldId);
        } else {
            return executeQueryBigDecimal("select id from cw_core_type_key_fields where key_id = ? and field_id = ? and id != ?", keyId, fieldId, skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'KeyField' fields or create new record with values set to given parameters
    * 
    * @param      keyId               Key
    * @param      fieldId             Field
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal keyFieldToId(java.math.BigDecimal keyId, java.math.BigDecimal fieldId, boolean createNewIfNotFound) {
        
        BigDecimal id = keyFieldToId(keyId, fieldId, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        TypeKeyFieldObject object = create();
        object.setKeyId(keyId);
        object.setFieldId(fieldId);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'KeyField' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToKeyField(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select key_id, field_id from cw_core_type_key_fields where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        TypeKeyFieldObject object = new TypeKeyFieldObject();
        object.setService((TypeKeyFieldService)this);
        object.setId(id);
        object.setKeyId(statement.getBigDecimal(1));
        object.setFieldId(statement.getBigDecimal(2));
        
        statement.close();
        
        return object.toKeyField();
    }
    
    /**
    * Find id of record by key 'KeyOrderNo' fields
    * 
    * @param      keyId   Key
    * @param      orderNo OrderNo
    * @return     Id of found record or null
    */
    public BigDecimal keyOrderNoToId(java.math.BigDecimal keyId, java.math.BigDecimal orderNo) {
        return keyOrderNoToId(keyId, orderNo, null);
    }
    
    /**
    * Find id of record by key 'KeyOrderNo' fields with id not equal skipId
    * 
    * @param      keyId   Key
    * @param      orderNo OrderNo
    * @param      skipId  Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal keyOrderNoToId(java.math.BigDecimal keyId, java.math.BigDecimal orderNo, BigDecimal skipId) {
        
        if ( keyId == null || orderNo == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_type_key_fields where key_id = ? and order_no = ?", keyId, orderNo);
        } else {
            return executeQueryBigDecimal("select id from cw_core_type_key_fields where key_id = ? and order_no = ? and id != ?", keyId, orderNo, skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'KeyOrderNo' fields or create new record with values set to given parameters
    * 
    * @param      keyId               Key
    * @param      orderNo             OrderNo
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal keyOrderNoToId(java.math.BigDecimal keyId, java.math.BigDecimal orderNo, boolean createNewIfNotFound) {
        
        BigDecimal id = keyOrderNoToId(keyId, orderNo, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        TypeKeyFieldObject object = create();
        object.setKeyId(keyId);
        object.setOrderNo(orderNo);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'KeyOrderNo' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToKeyOrderNo(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select key_id, order_no from cw_core_type_key_fields where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        TypeKeyFieldObject object = new TypeKeyFieldObject();
        object.setService((TypeKeyFieldService)this);
        object.setId(id);
        object.setKeyId(statement.getBigDecimal(1));
        object.setOrderNo(statement.getBigDecimal(2));
        
        statement.close();
        
        return object.toKeyOrderNo();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToKeyField(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      typeKeyFieldObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(TypeKeyFieldObject typeKeyFieldObject) {
        return typeKeyFieldObject.toKeyField();
    }
    
    protected void saveAudit(TransactionAction transactionAction, TypeKeyFieldObject oldTypeKeyFieldObject, TypeKeyFieldObject newTypeKeyFieldObject) {
        
        TypeKeyFieldObject o = oldTypeKeyFieldObject != null ? oldTypeKeyFieldObject : new TypeKeyFieldObject();
        TypeKeyFieldObject n = newTypeKeyFieldObject != null ? newTypeKeyFieldObject : new TypeKeyFieldObject();
        
        
        executeUpdate("insert into a_cw_core_type_key_fields (scn, action_type, id , o_key_id, o_order_no, o_field_id, n_key_id, n_order_no, n_field_id) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getKeyId(), o.getOrderNo(), o.getFieldId(), n.getKeyId(), n.getOrderNo(), n.getFieldId());
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      typeKeyField Saved object
    */
    public void save(TypeKeyFieldObject typeKeyField) {
        
        if ( typeKeyField == null ) {
            throwException("Cannot save NULL ?", new Object[]{"TypeKeyFieldObject"});
        }
        
        TransactionAction transactionAction;
        TypeKeyFieldObject oldTypeKeyField;
        if ( typeKeyField.getService() == null ) {
            typeKeyField.setService((TypeKeyFieldService)this);
        }
        
        check(typeKeyField);
        
        if ( typeKeyField.getId() == null ) {
        
            typeKeyField.setId(getObjectService().createObject(getTypeId()));
        
            oldTypeKeyField = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, typeKeyField.getId());
        
            executeUpdate("insert into cw_core_type_key_fields" 
                          + "( id, key_id, order_no, field_id ) "
                          + "values"
                          + "(?, ?, ?, ?)",
                          typeKeyField.getId(), typeKeyField.getKeyId(), typeKeyField.getOrderNo(), typeKeyField.getFieldId() );
        
            if ( typeKeyField.getKeyId() != null ) { getObjectService().setLink(typeKeyField.getKeyId(), typeKeyField.getId()); }
            if ( typeKeyField.getFieldId() != null ) { getObjectService().setLink(typeKeyField.getFieldId(), typeKeyField.getId()); }
        
        } else {
        
            oldTypeKeyField = load(typeKeyField.getId());
            if ( oldTypeKeyField.equals(typeKeyField) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, typeKeyField.getId());
        
            executeUpdate("update cw_core_type_key_fields set "
                          + "key_id = ?, order_no = ?, field_id = ? "
                          + "where id = ?",
                          typeKeyField.getKeyId(), typeKeyField.getOrderNo(), typeKeyField.getFieldId(), typeKeyField.getId() );
        
            getObjectService().changeLinkParent(oldTypeKeyField.getKeyId(), typeKeyField.getKeyId(), typeKeyField.getId());
            getObjectService().changeLinkParent(oldTypeKeyField.getFieldId(), typeKeyField.getFieldId(), typeKeyField.getId());
        
        }
        
        saveAudit(transactionAction, oldTypeKeyField, typeKeyField);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        TypeKeyFieldObject oldTypeKeyFieldObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldTypeKeyFieldObject, null);
        
        getObjectService().deleteObject(id);
        
        if (oldTypeKeyFieldObject.getKeyId() != null) { getObjectService().deleteLink(oldTypeKeyFieldObject.getKeyId(), id); }
        if (oldTypeKeyFieldObject.getFieldId() != null) { getObjectService().deleteLink(oldTypeKeyFieldObject.getFieldId(), id); }
        
        executeUpdate("delete from cw_core_type_key_fields where id = ?", id);
        
    }
}
