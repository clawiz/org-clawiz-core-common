package org.clawiz.core.common.metadata.data.language.node;

import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;

public class AbstractLanguageNode extends MetadataNode {

    @ExchangeReference
    AbstractLanguageNode extendsNode;

    public AbstractLanguageNode getExtendsNode() {
        return extendsNode;
    }

    public void setExtendsNode(AbstractLanguageNode extendsNode) {
        this.extendsNode = extendsNode;
    }
}
