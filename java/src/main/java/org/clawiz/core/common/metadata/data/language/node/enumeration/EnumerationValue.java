package org.clawiz.core.common.metadata.data.language.node.enumeration;

import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;

public class EnumerationValue extends MetadataNode {

    Enumeration enumeration;

    @ExchangeAttribute
    String description;

    public Enumeration getEnumeration() {
        return enumeration;
    }

    public void setEnumeration(Enumeration enumeration) {
        this.enumeration = enumeration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
