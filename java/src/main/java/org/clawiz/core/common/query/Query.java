/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.query;

import org.clawiz.core.common.query.element.AbstractQueryElement;
import org.clawiz.core.common.query.element.column.QueryColumnList;

import org.clawiz.core.common.query.element.filter.component.QueryFilterComponent;
import org.clawiz.core.common.query.element.order.QueryOrderList;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.Session;

public class Query extends AbstractQueryElement {

    private QueryBuilder      builder;

    private QueryColumnList      columns = new QueryColumnList();
    private QueryFilterComponent filter  = new QueryFilterComponent();
    private QueryOrderList       orders  = new QueryOrderList();

    private void internalPrepare() {
        columns.setQuery(this);
        filter.setQuery(this);
    }

    public Query(QueryBuilder builder) {
        this.builder = builder;
        internalPrepare();
    }

    public Query(Session session) {
        this.builder = session.getService(QueryBuilder.class);
        internalPrepare();
    }

    public QueryBuilder getBuilder() {
        return builder;
    }

    @Override
    public Query getQuery() {
        return this;
    }

    public QueryColumnList select() {
        return this.columns;
    }

    public QueryFilterComponent where() {
        return this.filter;
    }

    public QueryOrderList orderBy() {
        return this.orders;
    }

    private  String _sql;
    public String getSQL() {
        if ( _sql != null ) {
            return _sql;
        }
        _sql = builder.toSQL(this);
        return _sql;
    }

    public Statement execute(Object... parameters) {
        return getBuilder().executeQuery(getSQL(), parameters);
    }

}
