package org.clawiz.core.common.metadata.data.common.valuetype;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ValueTypeScaledNumberPrototype extends org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeNumber {
    
    public ValueTypeScaledNumber withName(String value) {
        setName(value);
        return (ValueTypeScaledNumber) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
