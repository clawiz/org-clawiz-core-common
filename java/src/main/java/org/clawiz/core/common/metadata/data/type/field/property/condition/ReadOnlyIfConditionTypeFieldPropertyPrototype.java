package org.clawiz.core.common.metadata.data.type.field.property.condition;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ReadOnlyIfConditionTypeFieldPropertyPrototype extends org.clawiz.core.common.metadata.data.type.field.property.condition.AbstractConditionTypeFieldProperty {
    
    public ReadOnlyIfConditionTypeFieldProperty withName(String value) {
        setName(value);
        return (ReadOnlyIfConditionTypeFieldProperty) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
