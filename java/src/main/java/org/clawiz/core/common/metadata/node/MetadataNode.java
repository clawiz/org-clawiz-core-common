/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.metadata.node;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.install.AbstractMetadataNodeInstaller;
import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.utils.StringUtils;

import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.security.MessageDigest;

public abstract class MetadataNode {

    private MetadataNode     parentNode;

    @ExchangeAttribute
    private String           name;

    @ExchangeAttribute
    private String           packageName;

    private BigDecimal       id;

    private String           metadataStoragePackageName;
    private BigDecimal       nodeObjectId;

    private String           parentNodeFieldName;

    protected CoreException coreException(String message, Object... parameters) {
        return new CoreException(message, parameters);
    }

    protected void throwException(String message, Object... parameters) {
        throw coreException(message, parameters);
    }

    public <T extends MetadataNode> T createChildNode(Class<T> nodeClass) {
        return createChildNode(nodeClass, null);
    }

    public <T extends MetadataNode> T createChildNode(Class<T> nodeClass, String parentNodeFieldName) {
        try {
            T node = nodeClass.newInstance();
            node.setParentNode(this, parentNodeFieldName);
            return node;
        } catch (Exception e) {
            throw coreException("Exception on create node class ? instance : ?", nodeClass.getName(), e.getMessage(), e);
        }
    }

    public MetadataNode getParentNode() {
        return parentNode;
    }

    public void setParentNode(MetadataNode parentNode) {
        setParentNode(parentNode, null);
    }

    public void setParentNode(MetadataNode parentNode, String parentNodeFieldName) {
        this.parentNode           = parentNode;
        this.parentNodeFieldName = parentNodeFieldName;
    }

    public String getParentNodeFieldName() {
        return parentNodeFieldName;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public <T extends AbstractMetadataNodeInstaller> Class<T> getInstallerClass() {
        return null;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getMetadataStoragePackageName() {
        return metadataStoragePackageName != null ? metadataStoragePackageName : getPackageName();
    }

    public void setMetadataStoragePackageName(String metadataStoragePackageName) {
        this.metadataStoragePackageName = metadataStoragePackageName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return  getParentNode() != null ?
                   getParentNode().getFullName() + "." + getName()
                : (getPackageName() != null ? getPackageName() + "." : "") + getName();
    }

    public String getReferencePath() {
        if ( getParentNode() == null ) {
            return "name://" + getClass().getName() + ":" + getPackageName() + "." + getName();
        }
        if ( getParentNodeFieldName() == null) {
            throwException("Parent node field name not defined for node '?' of class '?'", getFullName(), getClass().getName());
        }
        return getParentNode().getReferencePath() + "[" + getParentNodeFieldName() + "=\"" + StringUtils.escapeJavaString(getName()) + "\"]";
    }


    String _javaName;

    public String getJavaName() {
        if ( _javaName == null ) {
            _javaName = GeneratorUtils.toJavaClassName(getName(), true);
        }
        return _javaName;
    }

    public void setJavaName(String javaName) {
        this._javaName         = javaName;
        this._javaClassName    = null;
        this._javaVariableName = null;
    }

    String _javaClassName;
    public String getJavaClassName() {
        if ( _javaClassName == null ) {
            _javaClassName = StringUtils.toUpperFirstChar(getJavaName());
        }
        return _javaClassName;
    }

    String _javaVariableName;
    public String getJavaVariableName() {
        if ( _javaVariableName == null ) {
            _javaVariableName = StringUtils.toLowerFirstChar(getJavaClassName());
        }
        return _javaVariableName;
    }

    public BigDecimal getNodeObjectId() {
        return nodeObjectId;
    }

    public void setNodeObjectId(BigDecimal nodeObjectId) {
        this.nodeObjectId = nodeObjectId;
    }

    public void fillReferences(MetadataNodeReferencesList references) {

    }

    public String getChecksum() {
        ByteArrayOutputStream baos     = null;
        ObjectOutputStream    oos      = null;
        String                checksum = null;
        try {
            baos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(baos);
            oos.writeObject(this);
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] thedigest = md.digest(baos.toByteArray());
            checksum = DatatypeConverter.printHexBinary(thedigest);
        } catch (Exception e) {
            throwException("Exception on get checksum of '?' : ?", getPackageName() + "." + getName(), e.getMessage(), e);
        } finally {
            try {
                oos.close();
                baos.close();
            } catch (IOException e) {
                throwException("Exception on get checksum of '?' : ?", getPackageName() + "." + getName(), e.getMessage(), e);
            }
        }
        return checksum;
    }

    private boolean inPrepare = false;

    protected boolean isInPrepare() {
        return inPrepare;
    }

    protected void setInPrepare(boolean inPrepare) {
        this.inPrepare = inPrepare;
    }

    public void prepare(Session session) {

    }

    private boolean _prepared;
    public void prepareNode(Session session) {
        if ( _prepared ) {
            return;
        }
        _prepared = true;
        prepare(session);
    }

}
