/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.install;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.storage.language.LanguageObject;
import org.clawiz.core.common.storage.language.LanguageService;
import org.clawiz.core.common.storage.permissiontype.PermissionTypeObject;
import org.clawiz.core.common.storage.permissiontype.PermissionTypeService;
import org.clawiz.core.common.storage.role.RoleObject;
import org.clawiz.core.common.storage.role.RoleService;
import org.clawiz.core.common.storage.rolepermission.RolePermissionService;
import org.clawiz.core.common.storage.type.TypeService;
import org.clawiz.core.common.storage.userrole.UserRoleService;
import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.system.database.adapter.DatabaseType;
import org.clawiz.core.common.system.object.ObjectService;
import org.clawiz.core.common.system.security.SecurityBase;
import org.clawiz.core.common.storage.user.UserObject;
import org.clawiz.core.common.storage.user.UserService;
import org.clawiz.core.common.utils.RandomGUID;

import java.math.BigDecimal;

/**
 * (C) Clawiz
 * User: abdrashitovta
 * Date: 27.11.2005
 * Time: 13?9?7
 */
public class PrepareCoreData extends Service {

    @NotInitializeService
    ObjectService _objectService;

    @NotInitializeService
    UserService _userService;

    @NotInitializeService
    UserRoleService _userRoleService;

    @NotInitializeService
    PermissionTypeService _permissionTypeService;

    @NotInitializeService
    RoleService _roleService;

    @NotInitializeService
    RolePermissionService _rolePermissionService;

    @NotInitializeService
    TypeService _typeService;

    @NotInitializeService
    LanguageService _languageService;

    private ObjectService getObjectService() {
        if ( _objectService == null ) {
            _objectService = getService(ObjectService.class);
        }
        return _objectService;
    }
    
    private UserService getUserService() {
        if ( _userService == null ) {
            _userService = getService(UserService.class);
        }
        return _userService;
    }

    private UserRoleService getUserRoleService() {
        if ( _userRoleService == null ) {
            _userRoleService = getService(UserRoleService.class);
        }
        return _userRoleService;
    }

    private PermissionTypeService getPermissionTypeService() {
        if ( _permissionTypeService == null ) {
            _permissionTypeService = getService(PermissionTypeService.class);
        }
        return _permissionTypeService;
    }

    private RoleService getRoleService() {
        if ( _roleService == null ) {
            _roleService = getService(RoleService.class);
        }
        return _roleService;
    }

    private RolePermissionService getRolePermissionService() {
        if ( _rolePermissionService == null ) {
            _rolePermissionService = getService(RolePermissionService.class);
        }
        return _rolePermissionService;
    }

    private LanguageService getLanguageService() {
        if ( _languageService == null ) {
            _languageService = getService(LanguageService.class);
        }
        return _languageService;
    }

    private TypeService getTypeService() {
        if ( _typeService == null ) {
            _typeService = getService(TypeService.class);
        }
        return _typeService;
    }



    private BigDecimal saveObject(BigDecimal id, BigDecimal typeId) {
        if ( id != null ) {
            String guid = (new RandomGUID(false)).toString();
            executeUpdate("delete from cw_core_objects where id = ?", id);
            executeUpdate("insert into cw_core_objects (id, type_id, guid) values (?, ?, ? )",id, typeId, guid);
            return id;
        } else {
            return getObjectService().createObject(typeId);
        }
    }


    private void saveCoreType(BigDecimal _typeId, String name) {

        String     packageName = "org.clawiz.core.common.storage";

        if ( executeQueryBigDecimal("select id from cw_core_types where upper_package_name = ? and upper_name = ?"
                ,packageName.toUpperCase(), name.toUpperCase()) != null ) {
            return;
        }
        BigDecimal typeId      = saveObject(_typeId, new BigDecimal(1));

        logDebug("Insert into cw_core_types values (" + typeId + ", " + name + ")");

        executeUpdate("insert into cw_core_types (id, package_name, upper_package_name, name, upper_name, java_name, upper_java_name) values (?, ?, ?, ?, ?, ?, ? )",
                typeId, packageName, packageName.toUpperCase(), name, name.toUpperCase(), name, name.toUpperCase());

    }

    private void saveCoreType(String name) {
        saveCoreType(null, name);
    }

    public void prepareDualTable() {

        boolean found = false;
        logDebug("Preparing dual table for database '"  + getSession().getDataSource().getDatabaseType() + "'");

        if ( getSession().getDataSource().getDatabaseType() == DatabaseType.HSQLDB
                || getSession().getDataSource().getDatabaseType() == DatabaseType.DERBY ) {
            try {
                found = getSession().getDatabaseAdapter().isTableExists("DUAL");
            } catch (CoreException e) {
                throwException("Error on DUAL exists check", e);
            }

            if ( found ) {
                return;
            }

            logDebug("Creating dual table");
            executeUpdate("create table dual (id numeric(1))");
            executeUpdate("insert into dual values (1)");
        }
        logDebug("Dual table prepared");
    }

    private void createAdminUser() {
/*
        if ( getUserService().nameToId(SecurityBase.ADMIN_USER_NAME) != null ) {
            return;
        }
        BigDecimal userId = getObjectService().createObject(getUserService().getTypeId());
        executeUpdate("insert into cw_core_users (id, name, upper_name) values (?, ?, ?)", userId, SecurityBase.ADMIN_USER_NAME, SecurityBase.ADMIN_USER_NAME.toUpperCase());
*/
    }

    public void pass1() {
        prepareDualTable();

        saveCoreType(new BigDecimal(1), "Type");
        saveCoreType("ValueType");
        saveCoreType("User");
        saveCoreType("MetadataStorageClass");
        saveCoreType("MetadataStorage");
        saveCoreType("MetadataNodeClass");
        saveCoreType("MetadataNode");
        saveCoreType("ChecksumType");
        saveCoreType("Checksum");
        commit();

        createAdminUser();

    }

    protected void saveLanguage(String code, String name) {
        BigDecimal     id       = getLanguageService().codeToId(code);
        LanguageObject language = id != null ? getLanguageService().load(id) : getLanguageService().create();
        language.setCode(code);
        language.setName(name);
        language.save();
    }

    protected void fillLanguages() {
        saveLanguage("en", "English");
    }


    private void addRolePermissionType(BigDecimal roleId, String permissionTypeName) {
        getRolePermissionService().roleObjectPermissionToId(
                roleId
                , getTypeService().getTypeId()
                , getPermissionTypeService().nameToId(permissionTypeName, true)
                ,true
        );
    }

    protected RoleObject saveRole(String name, String description) {
        RoleObject role = getRoleService().load(getRoleService().nameToId(name, true));
        role.setDescription(description);
        role.save();
        return role;
    }

    protected PermissionTypeObject savePermissionType(String name, String description) {
        PermissionTypeObject type = getPermissionTypeService().load(getPermissionTypeService().nameToId(SecurityBase.READ_PERMISSION_TYPE_NAME, true));
        type.setDescription(description);
        type.save();
        return type;
    }

    protected void prepareSecuirtyData() {
        UserObject adminUser = getUserService().load(getUserService().nameToId(SecurityBase.ADMIN_USER_NAME, true));
        if ( adminUser.getPasswordHash() == null ) {
            adminUser.setPassword(SecurityBase.ADMIN_USER_NAME);
        }
        adminUser.setFirstName("System administrator");
        adminUser.save();

        RoleObject adminRole = saveRole(SecurityBase.ADMINISTRATOR_ROLE_NAME, "System administrator");
        getUserRoleService().userRoleToId(adminUser.getId(), adminRole.getId(), true);


        savePermissionType(SecurityBase.READ_PERMISSION_TYPE_NAME,  "read data");
        savePermissionType(SecurityBase.WRITE_PERMISSION_TYPE_NAME, "write data");


    }

    public void pass2() {

        prepareSecuirtyData();

        fillLanguages();

        commit();

    }

}
