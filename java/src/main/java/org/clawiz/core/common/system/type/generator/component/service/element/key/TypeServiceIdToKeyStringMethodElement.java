/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator.component.service.element.key;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.type.key.TypeKey;
import org.clawiz.core.common.metadata.data.type.key.TypeKeyField;
import org.clawiz.core.common.system.type.generator.component.service.element.AbstractTypeServiceMethodElement;

public class TypeServiceIdToKeyStringMethodElement extends AbstractTypeServiceMethodElement {

    TypeKey key;

    public TypeKey getKey() {
        return key;
    }

    public void setKey(TypeKey key) {
        this.key = key;
    }

    @Override
    public void process() {
        super.process();

        setName("idTo" + key.getJavaClassName());
        setType("String", "Concatenated string values of key fields");

        addParameter("BigDecimal", "id", "Id of record");

        addJavaDoc("Load record by id and prepare concatenated string values of key '" + key.getName() + "' fields");

        addText("");
        addText("if ( id == null ) { return null; }");
        addText("");

        String sql    = "select ";
        String prefix = "";
        for(TypeKeyField keyField : key.getFields() ) {
            TypeField field = keyField.getField();

            sql    += prefix + field.getColumnName();
            prefix =  ", ";
        }

        addText("Statement statement = executeQuery(\"" + sql + " from " + getTableName() + " where id = ?\", id);");
        addText("if ( ! statement.next()  ) { statement.close(); return null; }");
        addText("");
        addText(getDataObjectClassName() + " object = new " + getDataObjectClassName() + "();");
        addText("object.setService((" + getServiceClassName() + ")this);");
        addText("object.setId(id);");
        int index = 1;
        for(TypeKeyField keyField : key.getFields() ) {
            TypeField field = keyField.getField();
            addText("object." + field.getSetMethodName() + "(" + getFieldStatementGetter(field, index) + ");");
            index++;
        }


        addText("");
        addText("statement.close();");
        addText("");
        addText("return object.to" + key.getJavaClassName() + "();");

    }
}
