package org.clawiz.core.common.metadata.data.database.table.column.type;


import org.clawiz.core.common.system.database.metadata.DatabaseTableColumn;

import java.math.BigDecimal;

public class AbstractTableColumnType extends AbstractTableColumnTypePrototype {


    public DatabaseTableColumn.Type getDatabaseType() {
        throw coreException("Not implemented for class ?", this.getClass());
    }

    public BigDecimal getPrecision() {
        throw coreException("Not implemented for class ?", this.getClass());
    }

    public BigDecimal getScale() {
        throw coreException("Not implemented for class ?", this.getClass());
    }

}
