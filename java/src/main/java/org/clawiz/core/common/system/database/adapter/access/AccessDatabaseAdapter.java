/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.database.adapter.access;

import org.clawiz.core.common.system.database.metadata.DatabaseSequence;
import org.clawiz.core.common.system.database.metadata.DatabaseTableColumn;
import org.clawiz.core.common.system.database.adapter.DatabaseAdapter;

import java.math.BigDecimal;

/**
 * (C) Clawiz
 */
public class AccessDatabaseAdapter extends DatabaseAdapter {


    @Override
    protected String getStringConcatenationOperator() {
        return "&";
    }

    @Override
    public String getUpperStringFunctionName() {
        return "ucase";
    }

    @Override
    protected String getTableColumnTypeDDL(DatabaseTableColumn column) {
        if ( column.getType() == null ) {
            return super.getTableColumnTypeDDL(column);
        }

        switch (column.getType() ) {
            case VARCHAR: return "VARCHAR";
            case NUMBER:  return "NUMBER";
        }

        return super.getTableColumnTypeDDL(column);
    }



    @Override
    public void createSequence(DatabaseSequence sequence) {
        executeUpdate("create table " + sequence.getName() + " (seqid number)");
        if ( sequence.getStartWith() != null ) {
            executeUpdate("insert into " + sequence.getName() + " (seqqid) values (?)", sequence.getStartWith());
        }
        executeUpdate("create unique index " + sequence.getName() + "_i on "+sequence.getName()+"(seqid)");
    }

    @Override
    public BigDecimal getSequenceNextValue(String sequenceName) {
        BigDecimal nextValue = executeQueryBigDecimal("select max(seqid) from "+sequenceName);
        nextValue = nextValue != null ? nextValue.add(new BigDecimal(1)) : new BigDecimal(1);
        executeUpdate("insert into " + sequenceName + " (seqqid) values (?)", nextValue);
        return nextValue;
    }

    @Override
    public boolean isSequenceExists(DatabaseSequence sequence) {
        return isTableExists(sequence.getName());
    }
}
