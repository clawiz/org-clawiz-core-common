/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.metadata.install;

import org.clawiz.core.common.metadata.MetadataBase;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeList;
import org.clawiz.core.common.metadata.node.loader.MetadataLoader;
import org.clawiz.core.common.metadata.node.storage.MetadataNodeResourcesXMLStorage;
import org.clawiz.core.common.system.installer.script.xml.AbstractXMLScriptElementInstaller;
import org.clawiz.core.common.utils.file.FileUtils;

public class XMLScriptMetadataNodeXMLInstaller extends AbstractXMLScriptElementInstaller  {

    FileUtils                       fileUtils;
    MetadataLoader                  metadataLoader;
    MetadataBase                    metadataBase;
    MetadataNodeResourcesXMLStorage resourcesXMLStorage;


    @Override
    public void process() {

        String fileName = getAttribute( "file" );
        String nodeName = getAttribute("node-name");

        if ( fileName != null && nodeName != null ) {
            throwException("Both of attributes 'file' or 'name' defined for 'install-metadata-node-xml' in script ?", getScriptFileName() );
        }

        if ( fileName == null ) {
            if ( nodeName == null ) {
                throwException("Attribute 'file' or 'name' not defined for 'install-metadata-node-xml' in script ?", getScriptFileName() );
            }
            fileName = resourcesXMLStorage.getNodeResourcesPath(getPackageName() + "." + nodeName);
        }

        logDebug("Installing node from " + fileName);

        String xml = fileUtils.readToString(fileName);

        MetadataNodeListInstaller nodeListInstaller = addInstaller(MetadataNodeListInstaller.class);
        MetadataNodeList nodes = metadataLoader.xmlToNodes(xml);
        nodeListInstaller.setNodes(nodes);

        for (Object object : nodes ) {
            MetadataNode node = (MetadataNode) object;
            metadataBase.saveNode(node, MetadataNodeResourcesXMLStorage.class, resourcesXMLStorage.getNodeResourcesUrl(node));
        }

    }

    @Override
    public void done() {
        super.done();


    }
}
