/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.database.datasource.hikaricp;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.database.datasource.DataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class HikariDataSource extends DataSource {

    public static final int DEFAULT_MAXIMUUM_POOL_SIZE = 10;

    com.zaxxer.hikari.HikariDataSource _hikariDataSource;
    public synchronized com.zaxxer.hikari.HikariDataSource getHikariDataSource() {
        if ( _hikariDataSource == null ) {
            _hikariDataSource = new com.zaxxer.hikari.HikariDataSource();
            _hikariDataSource.setJdbcUrl(getConfig().getUrl());
            _hikariDataSource.setUsername(getConfig().getUser());
            _hikariDataSource.setPassword(getConfig().getPassword());
            _hikariDataSource.setAutoCommit(getConfig().isAutoCommit());
            _hikariDataSource.setMaximumPoolSize(getConfig().getConnectionPoolSize() != 0 ? getConfig().getConnectionPoolSize() : DEFAULT_MAXIMUUM_POOL_SIZE);

        }
        return _hikariDataSource;
    }



    @Override
    public boolean isAutoCommit() {
        return _hikariDataSource.isAutoCommit();
    }

    @Override
    public void setAutoCommit(boolean autoCommit) {
        super.setAutoCommit(autoCommit);
        getHikariDataSource().setAutoCommit(autoCommit);
    }

    @Override
    public Connection getJdbcConnection() {
        try {
            return getHikariDataSource().getConnection();
        } catch (SQLException e) {
            throw new CoreException("Exception on get connection : ?", e.getMessage(), e);
        }
    }
}
