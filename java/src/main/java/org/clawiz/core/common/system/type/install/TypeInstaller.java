/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.install;

import org.clawiz.core.common.metadata.install.AbstractMetadataNodeInstaller;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.storage.type.TypeService;
import org.clawiz.core.common.system.type.generator.GenerateTypeContext;
import org.clawiz.core.common.system.type.generator.TypeGenerator;

public class TypeInstaller extends AbstractMetadataNodeInstaller {

    Type type;

    @Override
    public void setNode(MetadataNode node) {
        super.setNode(node);
        type = (Type) node;
    }

    public Type getType() {
        return type;
    }

    @Override
    public void process() {

        logDebug("Install type " + type.getName());

        getService(TypeService.class).packageNameToId(type.getPackageName(), type.getName(), true);

        GenerateTypeContext context = new GenerateTypeContext();
        context.setDestinationPath(getDestinationPath());
        context.setPackageName(type.getPackageName());
        context.setType(type);

        TypeGenerator generator = getService(TypeGenerator.class, true);
        generator.setContext(context);
        generator.setIncrementalMode(isIncrementalMode());
        generator.run();


    }
}
