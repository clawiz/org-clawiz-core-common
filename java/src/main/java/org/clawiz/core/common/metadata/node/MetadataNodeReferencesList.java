package org.clawiz.core.common.metadata.node;


import java.math.BigDecimal;
import java.util.*;
import java.util.function.Consumer;

public class MetadataNodeReferencesList<T extends MetadataNode> implements Iterable<T> {

    ArrayList<T>           nodes          = new ArrayList<>();
    HashMap<T, T>          nodesCache     = new HashMap<>();
    HashMap<BigDecimal, T> nodesIdCache   = new HashMap<>();
    HashMap<String, T>     nodesNameCache = new HashMap<>();

    MetadataNodeList lookupList;


    public void add(T node) {
        if ( node == null ) {
            return;
        }

        if ( nodesCache.containsKey(node) ) {
            return;
        }
        nodes.add(node);
        nodesCache.put(node, node);
        if ( node.getId() != null ) {
            nodesIdCache.put(node.getId(), node);
        }
        if ( node.getName() != null ) {
            nodesNameCache.put(node.getFullName(), node);
        }

        node.fillReferences(this);
    }


    public MetadataNodeList getLookupList() {
        return lookupList;
    }

    public void setLookupList(MetadataNodeList lookupList) {
        this.lookupList = lookupList;
    }

    @Override
    public Iterator<T> iterator() {
        return nodes.iterator();
    }

    @Override
    public void forEach(Consumer<? super T> action) {
        nodes.forEach(action);
    }

    @Override
    public Spliterator<T> spliterator() {
        return nodes.spliterator();
    }

    public int size() {
        return nodes.size();
    }

    public T get(int index) {
        return nodes.get(index);
    }

    public boolean containNode(MetadataNode node) {
        if ( node == null ) {
            return false;
        }
        if ( nodesCache.containsKey(node)) {
            return true;
        }
        if ( node.getId() != null && nodesIdCache.containsKey(node.getId())) {
            return true;
        }
        return node.getName() != null && nodesNameCache.containsKey(node.getFullName());
    }

}
