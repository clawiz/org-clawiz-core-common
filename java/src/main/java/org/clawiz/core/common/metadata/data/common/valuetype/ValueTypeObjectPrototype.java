package org.clawiz.core.common.metadata.data.common.valuetype;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ValueTypeObjectPrototype extends org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType {
    
    @ExchangeReference
    private org.clawiz.core.common.metadata.data.type.Type referencedType;
    
    public ValueTypeObject withName(String value) {
        setName(value);
        return (ValueTypeObject) this;
    }
    
    public org.clawiz.core.common.metadata.data.type.Type getReferencedType() {
        return this.referencedType;
    }
    
    public void setReferencedType(org.clawiz.core.common.metadata.data.type.Type value) {
        this.referencedType = value;
    }
    
    public ValueTypeObject withReferencedType(org.clawiz.core.common.metadata.data.type.Type value) {
        setReferencedType(value);
        return (ValueTypeObject) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getReferencedType() != null ) { 
            getReferencedType().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getReferencedType());
        
    }
}
