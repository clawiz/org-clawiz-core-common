/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator.component.service.element.key;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.type.key.TypeKey;
import org.clawiz.core.common.metadata.data.type.key.TypeKeyField;
import org.clawiz.core.common.system.type.generator.component.service.element.AbstractTypeServiceMethodElement;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeString;
import org.clawiz.core.common.utils.StringUtils;

public class TypeServiceKeyToIdWithSkipIdMethodElement extends AbstractTypeServiceMethodElement {

    TypeKey key;

    public TypeKey getKey() {
        return key;
    }

    public void setKey(TypeKey key) {
        this.key = key;
    }

    @Override
    public void process() {
        super.process();

        setName(StringUtils.toLowerFirstChar(key.getJavaClassName()) + "ToId");
        setType("BigDecimal", "Id of found record or null");

        for (TypeKeyField keyField : key.getFields() ) {
            TypeField field = keyField.getField();
            addParameter(field.getValueTypeJavaClassName(), field.getJavaVariableName(), field.getDescription());
        }

        addParameter("BigDecimal", "skipId", "Skip records with this id");

        addJavaDoc("Find id of record by key '" + key.getName() + "' fields with id not equal skipId");

        String sql           = "select id from " + getTableName() + " where";
        String objects       = "";
        String wherePrefix   = " ";
        String objectsPrefix = "";
        int    index         = 1;

        String nullCheck       = "";
        String nullCheckPrefix = "";

        for(TypeKeyField keyField : key.getFields() ) {
            TypeField field = keyField.getField();

            sql     += wherePrefix   + (field.getValueType() instanceof ValueTypeString ? field.getUpperColumnName() : field.getColumnName() ) + " = ?";
            objects += objectsPrefix + field.getJavaVariableName() + (field.getValueType() instanceof ValueTypeString ? ".toUpperCase()" : "");

            wherePrefix   = " and ";
            objectsPrefix = ", ";

            nullCheck += nullCheckPrefix + field.getJavaVariableName() + " == null";
            nullCheckPrefix = " || ";

        }

        addText("");
        addText("if ( " + nullCheck + " ) {");
        addText("    return null;");
        addText("}");
        addText("");
        addText("if ( skipId == null ) { ");
        addText("    return executeQueryBigDecimal(\"" + sql + "\", " + objects + ");");
        addText("} else {");
        addText("    return executeQueryBigDecimal(\"" + sql + " and id != ?" + "\", " + objects + ", skipId);");
        addText("}");


        addText("");

    }
}
