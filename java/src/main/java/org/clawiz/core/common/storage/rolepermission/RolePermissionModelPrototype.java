package org.clawiz.core.common.storage.rolepermission;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;
import org.clawiz.core.common.system.service.NotInitializeService;

public class RolePermissionModelPrototype extends TypeModel {
    
    private static TypeField ROLE_ID_FIELD;
    
    private static TypeField OBJECT_ID_FIELD;
    
    private static TypeField PERMISSION_TYPE_ID_FIELD;
    
    private static Type type;
    
    private RolePermissionService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public RolePermissionService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(RolePermissionService.class).getType();
        
        
        ROLE_ID_FIELD = type.getFields().get("Role");
        if ( ROLE_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.RolePermission.Role"}); }
        
        OBJECT_ID_FIELD = type.getFields().get("Object");
        if ( OBJECT_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.RolePermission.Object"}); }
        
        PERMISSION_TYPE_ID_FIELD = type.getFields().get("PermissionType");
        if ( PERMISSION_TYPE_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.RolePermission.PermissionType"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _roleId;
    
    public TypeFieldModel roleId() {
        
        if ( _roleId != null ) {
            return _roleId;
        }
        
        _roleId = new TypeFieldModel(this, ROLE_ID_FIELD);
        return _roleId;
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.role.RoleModel _roleTypeModel;
    
    public org.clawiz.core.common.storage.role.RoleModel role() {
        
        if ( _roleTypeModel == null ) {
            org.clawiz.core.common.storage.role.RoleModel model = getService(org.clawiz.core.common.storage.role.RoleModel.class, this.toString() + "_Role");
            model.setJoinField(roleId());
            _roleTypeModel = model;
        }
        
        return _roleTypeModel;
    }
    
    private TypeFieldModel _objectId;
    
    public TypeFieldModel objectId() {
        
        if ( _objectId != null ) {
            return _objectId;
        }
        
        _objectId = new TypeFieldModel(this, OBJECT_ID_FIELD);
        return _objectId;
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.type.TypeModel _objectTypeModel;
    
    public org.clawiz.core.common.storage.type.TypeModel object() {
        
        if ( _objectTypeModel == null ) {
            org.clawiz.core.common.storage.type.TypeModel model = getService(org.clawiz.core.common.storage.type.TypeModel.class, this.toString() + "_Object");
            model.setJoinField(objectId());
            _objectTypeModel = model;
        }
        
        return _objectTypeModel;
    }
    
    private TypeFieldModel _permissionTypeId;
    
    public TypeFieldModel permissionTypeId() {
        
        if ( _permissionTypeId != null ) {
            return _permissionTypeId;
        }
        
        _permissionTypeId = new TypeFieldModel(this, PERMISSION_TYPE_ID_FIELD);
        return _permissionTypeId;
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.permissiontype.PermissionTypeModel _permissionTypeTypeModel;
    
    public org.clawiz.core.common.storage.permissiontype.PermissionTypeModel permissionType() {
        
        if ( _permissionTypeTypeModel == null ) {
            org.clawiz.core.common.storage.permissiontype.PermissionTypeModel model = getService(org.clawiz.core.common.storage.permissiontype.PermissionTypeModel.class, this.toString() + "_PermissionType");
            model.setJoinField(permissionTypeId());
            _permissionTypeTypeModel = model;
        }
        
        return _permissionTypeTypeModel;
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "ROLE" : return roleId();
        case "OBJECT" : return objectId();
        case "PERMISSIONTYPE" : return permissionTypeId();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
