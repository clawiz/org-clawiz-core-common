/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.java.component.element;

import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.core.common.system.generator.abstractgenerator.component.element.AbstractElement;

/**
 * Created with IntelliJ IDEA.
 * User: abdrashitovta
 * Date: 31.05.13
 * Time: 14?7
 * To change this template use File | Settings | File Templates.
 */
public class JavaIfElement extends AbstractJavaClassElement {

    String condition;

    @NotInitializeService
    JavaBlockElement trueBlock;

    @NotInitializeService
    JavaBlockElement falseBlock;

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }


    public JavaBlockElement getTrueBlock() {
        if ( trueBlock == null ) {
            trueBlock = addElement(JavaBlockElement.class);
        }
        return trueBlock;
    }

    public JavaBlockElement getFalseBlock() {
        if ( falseBlock == null ) {
            falseBlock = addElement(JavaBlockElement.class);
        }
        return falseBlock;
    }

    @Override
    public void write() {

        pln("if ( " + condition + " ) {");
        for (AbstractElement el : getTrueBlock().getElements() ) {
            el.write();
        }
        if ( falseBlock != null ) {
            pln("} else ");
            for (AbstractElement el : getFalseBlock().getElements() ) {
                el.write();
            }
        }
        pln("}");

    }
}
