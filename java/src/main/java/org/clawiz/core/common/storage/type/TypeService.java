/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.storage.type;


import org.clawiz.core.common.metadata.MetadataBase;
import org.clawiz.core.common.metadata.data.type.Type;

import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.core.common.utils.StringUtils;

import java.math.BigDecimal;

public class TypeService extends TypeServicePrototype {

    MetadataBase metadataBase;

    public BigDecimal fullNameToId(String name) {

        String[] names = StringUtils.splitByLastTokenOccurrence(name, "\\.");
        if ( names.length < 2 ) {
            throwException("Wrong format of type name '?'", name);
        }
        BigDecimal typeId = packageNameToId(names[0], names[1]);

        return typeId;
    }

    public Type getById(BigDecimal id) {

        TypeObject typeObject = load(id);
        return metadataBase.getNode(Type.class, typeObject.getPackageName(), typeObject.getName(), true);

    }

    public Type getByFullName(String name) {
        BigDecimal id = fullNameToId(name);
        if ( id == null ) {
            return null;
        }
        return getById(id);
    }


    @Override
    public void check(TypeObject type) {
        super.check(type);
        if ( type.getJavaName() == null ) {
            type.setJavaName(GeneratorUtils.toJavaName(type.getName()));
        }
    }
}
