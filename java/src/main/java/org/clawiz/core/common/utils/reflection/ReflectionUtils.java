/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.utils.reflection;

import org.reflections.Reflections;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.service.Service;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: abdrashitovta
 * Date: 26.04.13
 * Time: 10?5
 * To change this template use File | Settings | File Templates.
 */
public class ReflectionUtils extends Service {


    private static ConcurrentHashMap<String, Reflections> reflectionsPackageCache = new ConcurrentHashMap<>();

    public static void clearStaticCaches() {
        reflectionsPackageCache.clear();
    }

    private static Reflections getPackageReflections(String packageName) {
        if ( packageName == null ) {
            throw new CoreException("Package name for reflactions cannot be null");
        }
        Reflections reflections = reflectionsPackageCache.get(packageName);
        if ( reflections == null ) {
            reflections = new Reflections(packageName);
            reflectionsPackageCache.put(packageName, reflections);
        }
        return reflections;
    }


    public static ArrayList<Class> getPackageClasses(String packageName, Class superClass) {
        Set<Class> x = getPackageReflections(packageName).getSubTypesOf(superClass);
        return new ArrayList<>(x);
    }

    public static Method findMethod(Class clazz, String name) {
        return findMethod(clazz, name, null);
    }


    public static Method findMethod(Class clazz, String name, Class<?>... parameterTypes) {

        if ( name == null ) {
            return null;
        }

        for (Method method : clazz.getDeclaredMethods() ) {
            if ( method.getName().equalsIgnoreCase(name)) {
                if ( parameterTypes != null ) {
                    Class<?>[] params = method.getParameterTypes();
                    if ( params.length == parameterTypes.length) {
                        boolean eq = true;
                        for (int i=0; i < params.length; i++) {
                            if ( ! parameterTypes[i].isAssignableFrom(params[i]) ) {
                                eq = false;
                                break;
                            }
                        }
                        if ( eq ) {
                            return method;
                        }
                    }
                } else {
                    return method;
                }
            }
        }

        if ( clazz.getSuperclass() != null ) {
            return findMethod(clazz.getSuperclass(), name);
        }
        return null;
    }

    private Object cast(Object obj, Class targetClass) {
        if ( obj == null ) {
            return  null;
        }
        if ( obj.getClass() == targetClass ) {
            return obj;
        }
        if ( targetClass.isAssignableFrom(obj.getClass()) ) {
            return obj;
        }
        if ( obj.getClass() == String.class ) {
            if ( targetClass == BigDecimal.class ) {
                return StringUtils.toBigDecimal((String) obj);
            }
            if ( targetClass == Boolean.class ) {
                return StringUtils.toBoolean((String) obj);
            }
        }

        throwException("Cannot automatically cast '?' to '?'", new Object[]{obj.getClass(), targetClass});
        return null;
    }


    public void invokeVoid(Method method, Object object, Object... args) {
        if ( method == null ) {
            throwException("Method and object cannot be null on invoke method (?, ?)", new Object[]{method, object});
        }
        Object[] objects = new Object[args.length];
        Class[]  types   = method.getParameterTypes();
        if ( types.length != args.length ) {
            throwException("Method '?' parameters count ? not equal passed arguments count ?", new Object[]{method.getDeclaringClass() + "." + method.getName(), types.length, args.length});
        }
        for ( int i =0; i < types.length; i++) {
            try {
                objects[i] = cast(args[i], types[i]);
            } catch (CoreException e) {
                throwException("Error on prepare parameter '?' for method '?' from value '?' of class '?' : ?",
                        types[i], method.getDeclaringClass() + "." + method.getName(), args[i], args[i].getClass(), e);
            }
        }
        try {
            method.invoke(object, objects);
        } catch (Exception e) {
            throwException("Method '?' invoke exception : ?", method.getDeclaringClass() + "." + method.getName(), e.getMessage(), e);
        }

    }

    public static Field findField(Class clazz, String name) {
        for (Field field :  clazz.getDeclaredFields()) {
            if ( field.getName().equalsIgnoreCase(name) ) {
                return field;
            }
        }
        if ( clazz.getSuperclass() != null ) {
            return findField(clazz.getSuperclass(), name);
        }
        return null;
    }

    public static <T extends Annotation> boolean isAnnotationPresent(Class clazz, Class<T> annotation) {
        if ( clazz.isAnnotationPresent(annotation)) {
            return true;
        }
        if ( clazz.getSuperclass() != null ) {
            return isAnnotationPresent(clazz.getSuperclass(), annotation);
        }
        return false;
    }


}
