package org.clawiz.core.common.storage.userrole;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class UserRoleObjectPrototype extends AbstractObject {
    
    /**
    * User
    */
    private java.math.BigDecimal userId;
    
    private org.clawiz.core.common.storage.user.UserObject user;
    
    /**
    * Role
    */
    private java.math.BigDecimal roleId;
    
    private org.clawiz.core.common.storage.role.RoleObject role;
    
    public UserRoleService service;
    
    /**
    * 
    * @return     User
    */
    public java.math.BigDecimal getUserId() {
        return this.userId;
    }
    
    public org.clawiz.core.common.storage.user.UserObject getUser() {
        return getUser(false);
    }
    
    @SuppressWarnings("Duplicates")
    public org.clawiz.core.common.storage.user.UserObject getUser(boolean autoCreate) {
        
        if ( user != null ) { return user; }
        
        if ( userId != null ) {
            user = service.loadUser(userId);
        } else if ( autoCreate ) {
            user = service.createUser();
        }
        
        return user;
    }
    
    /**
    * Set 'User' value
    * 
    * @param      userId User
    */
    public void setUserId(java.math.BigDecimal userId) {
         this.userId = userId;
    }
    
    /**
    * Set 'User' value and return this object
    * 
    * @param      userId User
    * @return     UserRole object
    */
    public UserRoleObjectPrototype withUserId(java.math.BigDecimal userId) {
        setUserId(userId);
        return this;
    }
    
    /**
    * 
    * @return     Role
    */
    public java.math.BigDecimal getRoleId() {
        return this.roleId;
    }
    
    public org.clawiz.core.common.storage.role.RoleObject getRole() {
        return getRole(false);
    }
    
    @SuppressWarnings("Duplicates")
    public org.clawiz.core.common.storage.role.RoleObject getRole(boolean autoCreate) {
        
        if ( role != null ) { return role; }
        
        if ( roleId != null ) {
            role = service.loadRole(roleId);
        } else if ( autoCreate ) {
            role = service.createRole();
        }
        
        return role;
    }
    
    /**
    * Set 'Role' value
    * 
    * @param      roleId Role
    */
    public void setRoleId(java.math.BigDecimal roleId) {
         this.roleId = roleId;
    }
    
    /**
    * Set 'Role' value and return this object
    * 
    * @param      roleId Role
    * @return     UserRole object
    */
    public UserRoleObjectPrototype withRoleId(java.math.BigDecimal roleId) {
        setRoleId(roleId);
        return this;
    }
    
    public UserRoleService getService() {
        return this.service;
    }
    
    public void setService(UserRoleService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(UserRoleObjectPrototype  target) {
        target.setUserId(getUserId());
        target.setRoleId(getRoleId());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((UserRoleObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        if ( user != null ) {
            user.save();
            userId = user.getId();
        }
        if ( role != null ) {
            role.save();
            roleId = role.getId();
        }
        service.save((UserRoleObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(UserRoleObjectPrototype object) {
        return 
               isObjectsEquals(this.getUserId(), object.getUserId() ) 
            && isObjectsEquals(this.getRoleId(), object.getRoleId() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getUserId() != null ? getUserId().hashCode() : 0);
        result = result * 31 + (getRoleId() != null ? getRoleId().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'UserRole' fields
    * 
    * @return     Concatenated string values of key 'toUserRole' fields
    */
    public String toUserRole() {
        return service.getObjectService().idToString(getUserId()) + "," + service.getObjectService().idToString(getRoleId());
    }
}
