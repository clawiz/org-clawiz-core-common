package org.clawiz.core.common.metadata.data.type.property.extension.action;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class CallServiceMethodExtensionTypePropertyActionPrototype extends org.clawiz.core.common.metadata.data.type.property.extension.action.AbstractExtensionTypePropertyAction {
    
    @ExchangeReference
    private org.clawiz.core.common.metadata.data.service.method.ServiceMethod serviceMethod;
    
    public CallServiceMethodExtensionTypePropertyAction withName(String value) {
        setName(value);
        return (CallServiceMethodExtensionTypePropertyAction) this;
    }
    
    public org.clawiz.core.common.metadata.data.service.method.ServiceMethod getServiceMethod() {
        return this.serviceMethod;
    }
    
    public void setServiceMethod(org.clawiz.core.common.metadata.data.service.method.ServiceMethod value) {
        this.serviceMethod = value;
    }
    
    public CallServiceMethodExtensionTypePropertyAction withServiceMethod(org.clawiz.core.common.metadata.data.service.method.ServiceMethod value) {
        setServiceMethod(value);
        return (CallServiceMethodExtensionTypePropertyAction) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getServiceMethod() != null ) { 
            getServiceMethod().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getServiceMethod());
        
    }
}
