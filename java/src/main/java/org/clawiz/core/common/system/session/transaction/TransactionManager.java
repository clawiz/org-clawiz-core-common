/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.session.transaction;


import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.object.ObjectService;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.utils.RandomGUID;

import java.math.BigDecimal;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: abdrashitovta
 * Date: 29.07.13
 * Time: 17?5
 * To change this template use File | Settings | File Templates.
 */

public class TransactionManager extends Service {

    private static ConcurrentHashMap<String, Transaction>     transactionsCache   = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<BigDecimal, Transaction> lockedObjectsCache = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, Savepoint>       savepointsCache = new ConcurrentHashMap<>();

    public static int savepointsCount = 1;


    private static  Object  scnLock = new Object();

    public static String tsActive     = "A";
    public static String tsCommited   = "C";
    public static String tsRolledBack = "R";

    @NotInitializeService
    ObjectService objectService;

    private boolean coreInstalled = false;

    private boolean isCoreInstalled() {
        if ( coreInstalled ) {
            return true;
        }

        try {
            objectService = getService(ObjectService.class);
        } catch (CoreException e) {
            return false;
        }

        coreInstalled = true;
        return true;
    }

    public Transaction startTransaction() {
        if ( ! isCoreInstalled() ) {
            return null;
        }
        String key = new RandomGUID(true).toString();
        Transaction transaction = new Transaction(key, getSession());
        transactionsCache.put(key, transaction);

        executeUpdate("insert into cw_core_transactions (transaction_key, session_key, user_id, start_time, state) values (?, ?, ?, ?, ?)",
                transaction.getKey(),
                transaction.getSession().getKey().toString(),
                transaction.getUserId(),
                transaction.getStartTime(), tsActive);

        return transaction;
    }

    public Transaction getTransaction(String key) {
        if ( ! isCoreInstalled() ) {
            return null;
        }
        if ( key == null ) {
            return null;
        }
        return transactionsCache.get(key);
    }

    private void destroyTransaction(Transaction transaction) {
        if ( ! isCoreInstalled() ) {
            return;
        }
        synchronized ( lockedObjectsCache ) {
            for (BigDecimal obj : transaction.getLockedObjects()) {
                lockedObjectsCache.remove(obj);
            }
        }
        transactionsCache.remove(transaction.getKey());
        transaction.getLockedObjects().clear();
        executeUpdate("delete from cw_core_transaction_locks where transaction_key = ?", transaction.getKey());
    }

    public void commit(Transaction transaction) {
        if ( ! isCoreInstalled() ) {
            return;
        }
        if ( transaction == null ) {
            return;
        }
        executeUpdate("update cw_core_transactions set state = ? where transaction_key = ?", tsCommited, transaction.getKey());
        destroyTransaction(transaction);
    }

    public void rollback(Transaction transaction) {
        if ( ! isCoreInstalled() ) {
            return;
        }
        if ( transaction == null ) {
            return;
        }
        executeUpdate("update cw_core_transactions set state = ? where transaction_key = ?", tsRolledBack, transaction.getKey());
        destroyTransaction(transaction);
    }

    public void rollbackToSavepoint(Savepoint savepoint) {
        if ( ! isCoreInstalled() ) {
            return;
        }
/*
        DataSet ds = executeSelect("select scn, action_type, object, object_id, type_id from cw_core_transaction_log " +
                " where transaction_key =  ?" +
                "   and scn             <= ?" +
                " order by scn desc", new Object[]{savepoint.getTransaction().getKey(), savepoint.getScn()} );


        ds.first();
        while (! ds.isEOF()) {


            ds.next();
        }

        executeStatement("delete from cw_core_transaction_log" +
                " where transaction_key =  ?" +
                "   and scn             <= ?" , new Object[]{savepoint.getTransaction().getKey(), savepoint.getScn()} );

*/

        try {
            savepoint.getTransaction().getSession().getDatabaseAdapter().rollbackToSavepoint(savepoint.getJavaSqlSavepoint());
        } catch (CoreException e) {
            logError("EXCEPTION ON SAVEPOINT ROLLBACK " + e.getMessage());
            e.printStackTrace();
            savepoint.getTransaction().getSession().rollback();
        }
        savepointsCache.remove(savepoint.getName());
    }

    public void lockObject(Transaction transaction, BigDecimal objectId) {
        lockObject(transaction, objectId, 7000);
    }

    public void lockObject(Transaction transaction, BigDecimal objectId, int waitTime) {
        if ( ! isCoreInstalled() ) {
            return;
        }

        return;
/*
        if ( objectId == null ) {
            throwException("Cannot lock null object");
        }

        if ( transaction == null ) {
            throwException("Cannot lock in null transaction");
        }

        int wait = waitTime;
        boolean success = false;
        Transaction lockedTransaction = null;

        while ( ! success ) {
            synchronized ( lockedObjectsCache ) {
                lockedTransaction = lockedObjectsCache.get(objectId);
                if ( lockedTransaction == null || lockedTransaction == transaction ) {
                    lockedObjectsCache.put(objectId, transaction);
                    success = true;
                }
            }
            if ( ! success ) {
                if ( wait > 0 ) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    wait -= 500;
                } else {
                    throwException("Object @ already locked by user @ in session ?", new Object[]{objectId, lockedTransaction.getUserId(), lockedTransaction.getSession().getKey().toString() });
                }
            }
        }

        // Если мы не заблокированы сами собой
        if ( lockedTransaction == null ) {
            executeStatement("insert into cw_core_transaction_locks (transaction_key, object_id) values (?, ?)", new Object[]{transaction.getKey(), objectId});
            transaction.getLockedObjects().add(objectId);
        }
*/

    }

    public Savepoint getSavepoint(Transaction transaction) {
        if ( ! isCoreInstalled() ) {
            return null;
        }
        Savepoint savepoint;
        synchronized (savepointsCache) {
            savepoint = new Savepoint(transaction, "sp" + (savepointsCount++));
            savepointsCache.put(savepoint.getName(), savepoint);
        }
        savepoint.setJavaSqlSavepoint(transaction.getSession().getDatabaseAdapter().setSavepoint(savepoint.getName()));

//        TransactionAction action = newAction(transaction, TransactionActionType.SAVEPOINT, savepoint.getName());
//        savepoint.setScn(action.getScn());
        savepoint.setScn(new BigDecimal(newScn(transaction.getSession())));
        return savepoint;
    }

    private static  long       scn     = 0;
    private static synchronized long newScn(Session session) {
        if ( scn == 0 ) {
            BigDecimal _scn = session.executeQueryBigDecimal("select max(scn) from cw_core_transaction_log");
            scn = _scn != null ? _scn.longValue() + 1 : 1;
        } else {
            scn++;
        }
        return scn;
    }

    public long getCurrentScn(Session session) {
        if ( scn != 0 ) {
            return scn;
        }
        BigDecimal _scn = session.executeQueryBigDecimal("select max(scn) from cw_core_transaction_log");
        return _scn != null ? _scn.longValue() : 0;
    }

    public TransactionAction newAction(Transaction transaction, TransactionActionType actionType, BigDecimal objectId) {
        if ( ! isCoreInstalled() ) {
            return null;
        }
        if ( transaction == null ) {
            throwException("Cannot create action for null transaction");
        }
        if ( actionType == null ) {
            throwException("Cannot create action for null action type");
        }
        if ( objectId == null ) {
            throwException("Cannot create action for null object");
        }

        String guid       = null;
        BigDecimal typeId = null;
        Statement statement = executeQuery("select guid, type_id from cw_core_objects where id = ?", objectId);
        if ( statement.next() ) {
            guid = statement.getString(1);
            typeId = statement.getBigDecimal(2);
            statement.close();
        } else {
            statement.close();
//            throwException("Unknown transaction action object id ?", objectId);
        }

        TransactionAction action = new TransactionAction();
        action.setScn(new BigDecimal(newScn(transaction.getSession())));
        action.setTime(new Date());
        action.setTransaction(transaction);
        action.setObjectGuid(guid);
        action.setObjectId(objectId);
        action.setType(actionType);
        transaction.addAction(action);

        try {
            executeUpdate("insert into cw_core_transaction_log (scn, scn_time, transaction_key, action_type, object_guid, object_id, type_id)" +
                    "values (?, ?, ?, ?, ?, ?, ?)",

                            action.getScn(),
                            action.getTime(),
                            transaction.getKey(),
                            actionType.toString(),
                            guid,
                            objectId,
                            typeId
                    );
        } catch (Exception e) {
            e.printStackTrace();
            logError(action.getScn() + "," +
                    action.getTime() + "," +
                    transaction.getKey() + "," +
                    actionType.toString() + "," +
                    guid + "," +
                    objectId + "," +
                    typeId);
            throwException("Exception on create transaction log : ?", e.getMessage(), e);
        }

        return action;
    }

}
