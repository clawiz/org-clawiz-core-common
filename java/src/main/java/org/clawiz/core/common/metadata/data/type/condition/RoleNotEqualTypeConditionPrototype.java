package org.clawiz.core.common.metadata.data.type.condition;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class RoleNotEqualTypeConditionPrototype extends org.clawiz.core.common.metadata.data.type.condition.AbstractTypeCondition {
    
    @ExchangeReference
    private org.clawiz.core.common.metadata.data.security.role.RoleList roles = new org.clawiz.core.common.metadata.data.security.role.RoleList();
    
    public RoleNotEqualTypeCondition withName(String value) {
        setName(value);
        return (RoleNotEqualTypeCondition) this;
    }
    
    public org.clawiz.core.common.metadata.data.security.role.RoleList getRoles() {
        return this.roles;
    }
    
    public RoleNotEqualTypeCondition withRole(org.clawiz.core.common.metadata.data.security.role.Role value) {
        getRoles().add(value);
        return (RoleNotEqualTypeCondition) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        for (MetadataNode node : getRoles()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        for (MetadataNode node : getRoles()) {
            references.add(node);
        }
        
    }
}
