package org.clawiz.core.common.metadata.data.type;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class TypePrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeAttribute
    private String description;
    
    @ExchangeAttribute
    private org.clawiz.core.common.metadata.data.common.enumeration.BooleanDefaultTrueEnumeration install;
    
    @ExchangeAttribute
    private String tableName;
    
    @ExchangeAttribute
    private String javaName;
    
    @ExchangeReference
    private org.clawiz.core.common.metadata.data.type.key.TypeKey toStringKey;
    
    @ExchangeElement
    private org.clawiz.core.common.metadata.data.type.field.TypeFieldList fields = new org.clawiz.core.common.metadata.data.type.field.TypeFieldList();
    
    @ExchangeElement
    private org.clawiz.core.common.metadata.data.type.key.TypeKeyList keys = new org.clawiz.core.common.metadata.data.type.key.TypeKeyList();
    
    @ExchangeElement
    private org.clawiz.core.common.metadata.data.type.TypeList childs = new org.clawiz.core.common.metadata.data.type.TypeList();
    
    @ExchangeElement
    private org.clawiz.core.common.metadata.data.type.property.TypePropertyList properties = new org.clawiz.core.common.metadata.data.type.property.TypePropertyList();
    
    @ExchangeElement
    private org.clawiz.core.common.metadata.data.type.method.TypeMethodList serviceMethods = new org.clawiz.core.common.metadata.data.type.method.TypeMethodList();
    
    public Type withName(String value) {
        setName(value);
        return (Type) this;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String value) {
        this.description = value;
    }
    
    public Type withDescription(String value) {
        setDescription(value);
        return (Type) this;
    }
    
    public org.clawiz.core.common.metadata.data.common.enumeration.BooleanDefaultTrueEnumeration getInstall() {
        return this.install;
    }
    
    public void setInstall(org.clawiz.core.common.metadata.data.common.enumeration.BooleanDefaultTrueEnumeration value) {
        this.install = value;
    }
    
    public Type withInstall(org.clawiz.core.common.metadata.data.common.enumeration.BooleanDefaultTrueEnumeration value) {
        setInstall(value);
        return (Type) this;
    }
    
    public String getTableName() {
        return this.tableName;
    }
    
    public void setTableName(String value) {
        this.tableName = value;
    }
    
    public Type withTableName(String value) {
        setTableName(value);
        return (Type) this;
    }
    
    public String getJavaName() {
        return this.javaName;
    }
    
    public void setJavaName(String value) {
        this.javaName = value;
    }
    
    public Type withJavaName(String value) {
        setJavaName(value);
        return (Type) this;
    }
    
    public org.clawiz.core.common.metadata.data.type.key.TypeKey getToStringKey() {
        return this.toStringKey;
    }
    
    public void setToStringKey(org.clawiz.core.common.metadata.data.type.key.TypeKey value) {
        this.toStringKey = value;
    }
    
    public Type withToStringKey(org.clawiz.core.common.metadata.data.type.key.TypeKey value) {
        setToStringKey(value);
        return (Type) this;
    }
    
    public org.clawiz.core.common.metadata.data.type.field.TypeFieldList getFields() {
        return this.fields;
    }
    
    public Type withField(org.clawiz.core.common.metadata.data.type.field.TypeField value) {
        getFields().add(value);
        return (Type) this;
    }
    
    public <T extends org.clawiz.core.common.metadata.data.type.field.TypeField> T createField(Class<T> nodeClass) {
        org.clawiz.core.common.metadata.data.type.field.TypeField value = createChildNode(nodeClass, "fields");
        getFields().add(value);
        return (T) value;
    }
    
    public org.clawiz.core.common.metadata.data.type.field.TypeField createField() {
        return createField(org.clawiz.core.common.metadata.data.type.field.TypeField.class);
    }
    
    public org.clawiz.core.common.metadata.data.type.key.TypeKeyList getKeys() {
        return this.keys;
    }
    
    public Type withKey(org.clawiz.core.common.metadata.data.type.key.TypeKey value) {
        getKeys().add(value);
        return (Type) this;
    }
    
    public <T extends org.clawiz.core.common.metadata.data.type.key.TypeKey> T createKey(Class<T> nodeClass) {
        org.clawiz.core.common.metadata.data.type.key.TypeKey value = createChildNode(nodeClass, "keys");
        getKeys().add(value);
        return (T) value;
    }
    
    public org.clawiz.core.common.metadata.data.type.key.TypeKey createKey() {
        return createKey(org.clawiz.core.common.metadata.data.type.key.TypeKey.class);
    }
    
    public org.clawiz.core.common.metadata.data.type.TypeList getChilds() {
        return this.childs;
    }
    
    public Type withChild(org.clawiz.core.common.metadata.data.type.Type value) {
        getChilds().add(value);
        return (Type) this;
    }
    
    public <T extends org.clawiz.core.common.metadata.data.type.Type> T createChild(Class<T> nodeClass) {
        org.clawiz.core.common.metadata.data.type.Type value = createChildNode(nodeClass, "childs");
        getChilds().add(value);
        return (T) value;
    }
    
    public org.clawiz.core.common.metadata.data.type.Type createChild() {
        return createChild(org.clawiz.core.common.metadata.data.type.Type.class);
    }
    
    public org.clawiz.core.common.metadata.data.type.property.TypePropertyList getProperties() {
        return this.properties;
    }
    
    public Type withPropertie(org.clawiz.core.common.metadata.data.type.property.AbstractTypeProperty value) {
        getProperties().add(value);
        return (Type) this;
    }
    
    public <T extends org.clawiz.core.common.metadata.data.type.property.AbstractTypeProperty> T createPropertie(Class<T> nodeClass) {
        org.clawiz.core.common.metadata.data.type.property.AbstractTypeProperty value = createChildNode(nodeClass, "properties");
        getProperties().add(value);
        return (T) value;
    }
    
    public org.clawiz.core.common.metadata.data.type.property.AbstractTypeProperty createPropertie() {
        return createPropertie(org.clawiz.core.common.metadata.data.type.property.AbstractTypeProperty.class);
    }
    
    public org.clawiz.core.common.metadata.data.type.method.TypeMethodList getServiceMethods() {
        return this.serviceMethods;
    }
    
    public Type withServiceMethod(org.clawiz.core.common.metadata.data.type.method.TypeMethod value) {
        getServiceMethods().add(value);
        return (Type) this;
    }
    
    public <T extends org.clawiz.core.common.metadata.data.type.method.TypeMethod> T createServiceMethod(Class<T> nodeClass) {
        org.clawiz.core.common.metadata.data.type.method.TypeMethod value = createChildNode(nodeClass, "serviceMethods");
        getServiceMethods().add(value);
        return (T) value;
    }
    
    public org.clawiz.core.common.metadata.data.type.method.TypeMethod createServiceMethod() {
        return createServiceMethod(org.clawiz.core.common.metadata.data.type.method.TypeMethod.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getToStringKey() != null ) { 
            getToStringKey().prepare(session);
        }
        for (MetadataNode node : getFields()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        for (MetadataNode node : getKeys()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        for (MetadataNode node : getChilds()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        for (MetadataNode node : getProperties()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        for (MetadataNode node : getServiceMethods()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getToStringKey());
        
        for (MetadataNode node : getFields()) {
            references.add(node);
        }
        
        for (MetadataNode node : getKeys()) {
            references.add(node);
        }
        
        for (MetadataNode node : getChilds()) {
            references.add(node);
        }
        
        for (MetadataNode node : getProperties()) {
            references.add(node);
        }
        
        for (MetadataNode node : getServiceMethods()) {
            references.add(node);
        }
        
    }
}
