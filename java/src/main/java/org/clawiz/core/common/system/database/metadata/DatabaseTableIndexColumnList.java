/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.database.metadata;

import java.util.ArrayList;

public class DatabaseTableIndexColumnList {
    DatabaseTableIndex                  index;
    ArrayList<DatabaseTableIndexColumn> columns = new ArrayList<>();

    public DatabaseTableIndexColumnList(DatabaseTableIndex index) {
        this.index = index;
    }

    public void add(DatabaseTableIndexColumn column) {
        columns.add(column);
        column.setIndex(index);
    }

    public int size() {
        return columns.size();
    }

    public DatabaseTableIndexColumn get(int index) {
        return (DatabaseTableIndexColumn) columns.get(index);
    }

    public boolean equals(DatabaseTableIndexColumnList checkList) {

        if ( this.size() != checkList.size() ) {
            return false;
        }

        for (int i=0; i < this.size(); i++) {
            if ( ! this.get(i).getName().equalsIgnoreCase(checkList.get(i).getName())) {
                return false;
            }
        }

        return true;
    }

}
