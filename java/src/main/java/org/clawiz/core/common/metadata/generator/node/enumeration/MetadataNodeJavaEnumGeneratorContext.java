package org.clawiz.core.common.metadata.generator.node.enumeration;

import org.clawiz.core.common.metadata.generator.node.AbstractMetadataNodeGeneratorContext;
import org.clawiz.core.common.metadata.data.language.node.AbstractLanguageNode;
import org.clawiz.core.common.metadata.data.language.node.enumeration.Enumeration;

public class MetadataNodeJavaEnumGeneratorContext extends AbstractMetadataNodeGeneratorContext  {

    Enumeration structure;

    @Override
    public Enumeration getLanguageNode() {
        return structure;
    }

    @Override
    public void setLanguageNode(AbstractLanguageNode structure) {
        super.setLanguageNode(structure);
        this.structure = (Enumeration) structure;
    }
}
