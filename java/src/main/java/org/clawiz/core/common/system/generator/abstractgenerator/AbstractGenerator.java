/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.abstractgenerator;

import org.clawiz.core.common.metadata.MetadataBase;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.storage.checksum.ChecksumService;
import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.core.common.system.generator.abstractgenerator.component.AbstractComponent;
import org.clawiz.core.common.system.generator.file.component.AbstractFileComponent;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.service.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: abdrashitovta
 * Date: 12.01.13
 * Time: 14?9
 * To change this template use File | Settings | File Templates.
 */
@NotInitializeService
public abstract class AbstractGenerator extends Service {

    private      String                             destinationPath;
    private      HashMap<String, AbstractComponent> componentsCache;
    private      ArrayList<AbstractComponent>       components;

    int          iUID = 1;
    String       UIDPrefix;

    @NotInitializeService
    private      MetadataBase _metadataBase;

    public MetadataBase getMetadataBase(){
        if ( _metadataBase == null ) {
            _metadataBase = getService(MetadataBase.class);
        }
        return _metadataBase;
    }

    boolean incrementalMode;

    public  String createUID() {
        return getUIDPrefix() + (iUID++);
    }

    public String getUIDPrefix() {
        if ( UIDPrefix == null ) {
            String[] tokens = StringUtils.splitAndTrim(this.getClass().getName(), "\\.");
            UIDPrefix = tokens[tokens.length-1];
        }
        return UIDPrefix;
    }

    public void setUIDPrefix(String UIDPrefix) {
        this.UIDPrefix = UIDPrefix;
    }

    public String getDestinationPath() {
        return destinationPath;
    }

    public void setDestinationPath(String destinationPath) {
        this.destinationPath = destinationPath;
    }

    public MetadataNode getMetadataNode() {
        return null;
    }

    public boolean isIncrementalMode() {
        return incrementalMode;
    }

    public void setIncrementalMode(boolean incrementalMode) {
        this.incrementalMode = incrementalMode;
    }

    HashMap<Class<AbstractComponent>, Class<AbstractComponent>> componentsMap = new HashMap<>();
    protected <C extends AbstractComponent, M extends AbstractComponent> void addComponentMap(Class<C> component, Class<M> mapComponent) {
        componentsMap.put((Class<AbstractComponent>) component, (Class<AbstractComponent>) mapComponent);
    }

    protected void prepareComponentsMap() {

    }

    protected void prepare() {
        prepareComponentsMap();
    }

    protected void process() {
    }

    protected void done() {
        for ( AbstractComponent component : components) {
            component.done();
            component.callElementsDone();
        }
    }

    public <T extends AbstractComponent> T addComponent(Class<T> clazz) {
        return addComponent(createUID(), clazz);
    }

    protected <T extends  AbstractComponent> T addComponent(String componentName, Class<T> clazz) {
        if ( componentName == null ) {
            throwException("Component name cannot be NULL");
        }
        String key = componentName.toUpperCase();
        if ( componentsCache.containsKey(key)) {
            throwException("Component '?' already exists", new Object[]{componentName});
        }

        Class<T> _clazz = (Class<T>) componentsMap.get(clazz);
        if ( _clazz == null ) {
            _clazz = clazz;
        }

        T component =  getService(_clazz, true);
        componentsCache.put(key, component);
        components.add(component);
        component.setName(componentName);
        if (AbstractFileComponent.class.isAssignableFrom(component.getClass())) {
            ((AbstractFileComponent) component).setDestinationPath(getDestinationPath());
        }

        component.setGenerator(this);

        component.prepare();

        return component;
    }

    protected boolean  isComponentExists(String name) {
        if (name == null) {
            return false;
        }
        String _name = name.toUpperCase();
        return componentsCache.containsKey(_name);
    }

    public ArrayList<AbstractComponent> getComponents() {
        return components;
    }

    public <T extends  AbstractComponent> T getComponentByClass(Class<T> clazz) {
        T result = null;
        for (AbstractComponent component : getComponents() ) {
            if ( clazz.isAssignableFrom(component.getClass())) {
                if (result != null ) {
                    throwException("More then one component of class '?' exists", new Object[]{clazz.getName()});
                }
                result = (T) component;
            }
        }
        return result;
    }

    public <T extends  AbstractComponent> ArrayList<T> getComponentListByClass(Class<T> clazz) {
        ArrayList<T> result = new ArrayList<T>();
        for (AbstractComponent component : getComponents() ) {
            if ( clazz.isAssignableFrom(component.getClass())) {
                result.add((T) component);
            }
        }
        return result;
    }

    protected BigDecimal getMetadataNodeObjectId() {
        return getMetadataNode() != null ? getMetadataBase().getNodeObjectId(getMetadataNode()) : null;
    }

    public void run() {

        BigDecimal         nodeObjectId      = getMetadataNodeObjectId();
        String             generatorChecksum = null;
        ChecksumService    checksumService   = null;
        if ( nodeObjectId != null ) {
            checksumService       = getService(ChecksumService.class);
        }

        if ( isIncrementalMode() && nodeObjectId != null ) {
            generatorChecksum     = getMetadataNode().getChecksum();
            String storedChecksum = checksumService.getObjectChecksum(nodeObjectId, getClass().getName());
            if ( storedChecksum != null && storedChecksum.equals(generatorChecksum)) {
                logDebug("Incremental mode skip generation of '" + getMetadataNode().getPackageName() + "." + getMetadataNode().getName() + "'");
                return;
            }
        }

        components      = new ArrayList<>();
        componentsCache = new HashMap<>();
        prepare();
        process();
        for ( AbstractComponent component : components) {
            component.process();
            component.callElementsProcess();
        }
        done();

        for ( AbstractComponent component : getComponents() ) {
            if ( component.isDestinationExists() ) {
                switch (component.getOverwriteMode() ) {
                    case OVERWRITE: component.write(); break;
                    case SKIP: break;
                    case MERGE: component.merge(); break;
                }
            } else {
                component.write();
            }
        }

        for(AbstractComponent component : components) {
            component.destroy();
        }

        if ( nodeObjectId != null ) {
            checksumService.saveObjectChecksum(nodeObjectId, getClass().getName(), generatorChecksum);
        }

    }

}
