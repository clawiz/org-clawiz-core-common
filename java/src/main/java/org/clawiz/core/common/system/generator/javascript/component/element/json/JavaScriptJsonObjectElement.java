/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.javascript.component.element.json;

import org.clawiz.core.common.system.generator.abstractgenerator.component.element.AbstractElement;
import org.clawiz.core.common.system.generator.javascript.component.element.AbstractJavaScriptElement;
import org.clawiz.core.common.utils.StringUtils;

import java.util.ArrayList;

public class JavaScriptJsonObjectElement extends AbstractJavaScriptElement {

    ArrayList<JavaScriptJsonFieldElement> fields = new ArrayList<>();

    private boolean           addComma = false;

    public boolean isAddComma() {
        return addComma;
    }

    public void setAddComma(boolean addComma) {
        this.addComma = addComma;
    }

    public JavaScriptJsonFieldElement addField(String name) {
        AbstractElement el = getElement(name);
        if ( el != null ) {
            throwException("Field '?' already exists in json object", name);
        }
        JavaScriptJsonFieldElement field = addElement(JavaScriptJsonFieldElement.class, name);
        field.setLpad(getLpad() + "    ");
        fields.add(field);
        return field;
    }

    public JavaScriptJsonFieldElement addArrayField(String name) {
        JavaScriptJsonFieldElement field = addField(name);
        field.setType(JavaScriptJsonFieldElement.Type.ARRAY);
        return field;
    }

    protected JavaScriptJsonFieldElement resolveField(String name) {
        if (StringUtils.isEmpty(name)) {
            throwException("Json object field name cannot be null");
        }

        AbstractElement el = getElement(name);
        if ( el != null && ! (el instanceof JavaScriptJsonFieldElement) ) {
            throwException("Existing json object value '?' class ? is not JavaScriptJsonFieldElement", name, el.getClass().getName());
        }
        JavaScriptJsonFieldElement field = (JavaScriptJsonFieldElement) el;

        if ( field == null ) {
            field = addField(name);
        }
        return field;
    }

    public void setValue(String name, Object value) {
        resolveField(name).setValue(value);
    }

    public void addValue(String name, Object value) {
        JavaScriptJsonFieldElement field = resolveField(name);
        if ( field.getType() != null && field.getType() != JavaScriptJsonFieldElement.Type.ARRAY ) {
            throwException("Cannot add value for non-array field ?", field.getName());
        }
        if ( field.getType() == null ) {
            field.setType(JavaScriptJsonFieldElement.Type.ARRAY);
        }
        field.addValue(value);
    }


    public JavaScriptJsonFieldElement getField(String name) {
        return (JavaScriptJsonFieldElement) getElement(name);
    }

    boolean writeCurlyBraces = true;

    public void setWriteCurlyBraces(boolean writeCurlyBraces) {
        this.writeCurlyBraces = writeCurlyBraces;
    }

    public ArrayList<JavaScriptJsonFieldElement> getFields() {
        return fields;
    }

    @Override
    public void write() {
        if ( writeCurlyBraces ) {
            pln("{");
        }

        int maxNameLength = 0;
        for (JavaScriptJsonFieldElement field : fields) {
            if ( field.getName().length() > maxNameLength ) {
                maxNameLength = field.getName().length();
            }
        }

        for (int i = 0; i < fields.size(); i++) {

            JavaScriptJsonFieldElement field = fields.get(i);

            field.setNamesColumnLength(maxNameLength);
            field.setAddComma(i < (fields.size()-1));
            field.setLpad(getLpad() + "  ");
            field.write();

        }

        if ( writeCurlyBraces ) {
            pln("}" + (addComma ? "," : ""));
        }

    }
}
