/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.file.component;

import org.clawiz.core.common.system.generator.abstractgenerator.component.AbstractComponent;
import org.clawiz.core.common.system.generator.abstractgenerator.writer.AbstractWriter;
import org.clawiz.core.common.system.generator.file.writer.FileWriter;
import org.clawiz.core.common.utils.file.FileUtils;

import java.io.File;

public abstract class AbstractFileComponent extends AbstractComponent {

    private String destinationPath;
    private String fileName;

    FileUtils fileUtils;

    public void setDestinationPath(String targetPath) {
        this.destinationPath = targetPath;
    }

    public String getDestinationPath() {
        return destinationPath;
    }

    public String getFileName() {
        return fileName;
    }

    @Override
    public boolean isDestinationExists() {
        return new File( getDestinationPath() + File.separator + getFileName()).exists();
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    protected <T extends AbstractWriter> Class<T> getWriterClass() {
        return (Class<T>) FileWriter.class;
    }

    protected AbstractWriter createWriter() {

        if ( getDestinationPath() == null ) {
            throwException("Destination path for component '?' not defined", new Object[]{this.toString()});
        }
        fileUtils.createDirectory(getDestinationPath());
        if ( getFileName() == null ) {
            throwException("File name for component '?' not defined", new Object[]{this.toString()});
        }
        File file = new File( getDestinationPath() + File.separator + getFileName());


        FileWriter result = (FileWriter) super.createWriter();
        result.setFile(file);

        return result;
    }



}
