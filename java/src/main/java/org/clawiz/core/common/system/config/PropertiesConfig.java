/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.config;


import org.clawiz.core.common.CoreException;

/**
 * (c) Clawiz
 * Date: 04.03.2010
 * Time: 22:07:07
 */
public class  PropertiesConfig extends AbstractConfig implements Config {

    public void load(String fileName, String charsetName) {
        /*

        Must be rewrite to new Config logic (childs tree, not local properties cache)
         */

/*
        properties = new HashMap<String, String>();

        ArrayList<String> lines =  new ArrayList<String>();
        try {
            FileInputStream inputStream = new FileInputStream(file.getPath());
            BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, charsetName));
            String Line;
            while ((Line = in.readLine()) != null) {
              lines.add(Line);
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new CoreException("File open error : '?' \n ?", file, e.getMessage(), e);
        }

        for ( String line : lines) {
            if ( line != null && StringUtils.trim(line).length() > 0 &&
                   StringUtils.trim(line).charAt(0) != '#' ) {
                int pos = line.indexOf('=');
                if ( pos > 0 ) {
                    String name  = line.substring(0,pos).toUpperCase();
                    if ( properties.get(name) != null ) {
                        throw new CoreException("Parameter '?' already defined in '?'", name, file.getPath());
                    }
                    String value = line.substring(pos+1);
                    properties.put(StringUtils.trim(name), StringUtils.trim(value));
                }
            }
        }
*/

    }


}
