package org.clawiz.core.common.storage.typefieldvalue;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class TypeFieldValueObjectPrototype extends AbstractObject {
    
    /**
    * TypeField
    */
    private java.math.BigDecimal typeFieldId;
    
    /**
    * OrderNo
    */
    private java.math.BigDecimal orderNo;
    
    /**
    * Value
    */
    private java.lang.String value;
    
    /**
    * Description
    */
    private java.lang.String description;
    
    public TypeFieldValueService service;
    
    /**
    * 
    * @return     TypeField
    */
    public java.math.BigDecimal getTypeFieldId() {
        return this.typeFieldId;
    }
    
    /**
    * Set 'TypeField' value
    * 
    * @param      typeFieldId TypeField
    */
    public void setTypeFieldId(java.math.BigDecimal typeFieldId) {
         this.typeFieldId = typeFieldId;
    }
    
    /**
    * Set 'TypeField' value and return this object
    * 
    * @param      typeFieldId TypeField
    * @return     TypeFieldValue object
    */
    public TypeFieldValueObjectPrototype withTypeFieldId(java.math.BigDecimal typeFieldId) {
        setTypeFieldId(typeFieldId);
        return this;
    }
    
    /**
    * 
    * @return     OrderNo
    */
    public java.math.BigDecimal getOrderNo() {
        return this.orderNo;
    }
    
    /**
    * Set 'OrderNo' value
    * 
    * @param      orderNo OrderNo
    */
    public void setOrderNo(java.math.BigDecimal orderNo) {
         this.orderNo = orderNo;
    }
    
    /**
    * Set 'OrderNo' value and return this object
    * 
    * @param      orderNo OrderNo
    * @return     TypeFieldValue object
    */
    public TypeFieldValueObjectPrototype withOrderNo(java.math.BigDecimal orderNo) {
        setOrderNo(orderNo);
        return this;
    }
    
    /**
    * 
    * @return     Value
    */
    public java.lang.String getValue() {
        return this.value;
    }
    
    /**
    * Set 'Value' value
    * 
    * @param      value Value
    */
    public void setValue(java.lang.String value) {
        
        if ( value != null && value.length() > 75) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "TypeFieldValue.Value", "75");
        }
        
         this.value = value;
    }
    
    /**
    * Set 'Value' value and return this object
    * 
    * @param      value Value
    * @return     TypeFieldValue object
    */
    public TypeFieldValueObjectPrototype withValue(java.lang.String value) {
        setValue(value);
        return this;
    }
    
    /**
    * 
    * @return     Description
    */
    public java.lang.String getDescription() {
        return this.description;
    }
    
    /**
    * Set 'Description' value
    * 
    * @param      description Description
    */
    public void setDescription(java.lang.String description) {
        
        if ( description != null && description.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "TypeFieldValue.Description", "250");
        }
        
         this.description = description;
    }
    
    /**
    * Set 'Description' value and return this object
    * 
    * @param      description Description
    * @return     TypeFieldValue object
    */
    public TypeFieldValueObjectPrototype withDescription(java.lang.String description) {
        setDescription(description);
        return this;
    }
    
    public TypeFieldValueService getService() {
        return this.service;
    }
    
    public void setService(TypeFieldValueService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(TypeFieldValueObjectPrototype  target) {
        target.setTypeFieldId(getTypeFieldId());
        target.setOrderNo(getOrderNo());
        target.setValue(getValue());
        target.setDescription(getDescription());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((TypeFieldValueObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        service.save((TypeFieldValueObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(TypeFieldValueObjectPrototype object) {
        return 
               isObjectsEquals(this.getTypeFieldId(), object.getTypeFieldId() ) 
            && isObjectsEquals(this.getOrderNo(), object.getOrderNo() ) 
            && isObjectsEquals(this.getValue(), object.getValue() ) 
            && isObjectsEquals(this.getDescription(), object.getDescription() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getTypeFieldId() != null ? getTypeFieldId().hashCode() : 0);
        result = result * 31 + (getOrderNo() != null ? getOrderNo().hashCode() : 0);
        result = result * 31 + (getValue() != null ? getValue().hashCode() : 0);
        result = result * 31 + (getDescription() != null ? getDescription().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'TypeFieldOrderNo' fields
    * 
    * @return     Concatenated string values of key 'toTypeFieldOrderNo' fields
    */
    public String toTypeFieldOrderNo() {
        return service.getObjectService().idToString(getTypeFieldId()) + "," + ( getOrderNo() != null ? getOrderNo().toString() : null );
    }
    
    /**
    * Prepare concatenated string values of key 'TypeFieldValue' fields
    * 
    * @return     Concatenated string values of key 'toTypeFieldValue' fields
    */
    public String toTypeFieldValue() {
        return service.getObjectService().idToString(getTypeFieldId()) + "," + getValue();
    }
}
