/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.session;

import org.clawiz.core.common.Core;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.storage.remotenode.RemoteNodeObject;
import org.clawiz.core.common.storage.remotenode.RemoteNodeService;
import org.clawiz.core.common.system.database.Connection;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.database.datasource.DataSource;
import org.clawiz.core.common.system.security.SecurityBase;
import org.clawiz.core.common.system.logger.Logger;
import org.clawiz.core.common.system.service.extension.ServiceExtensionManager;
import org.clawiz.core.common.system.database.adapter.DatabaseAdapter;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.system.session.transaction.*;
import org.clawiz.core.common.storage.user.UserObject;
import org.clawiz.core.common.storage.user.UserService;

import java.math.BigDecimal;
import java.util.*;

/**
 * (c) Clawiz
 */
public class Session {

    private String           key;
    private Logger           log;

    private Session          parentSession;
    private DataSource       dataSource;
    private Connection       connection;


    private ArrayList<Session>                        childSessions         = new ArrayList<>();

    private HashMap<String, HashMap<Class, Service>>  servicesCache         = new HashMap<>();

    private HashMap<BigDecimal, Permission>           permissionsCache      = new HashMap<>();

    private UserObject       currentUser       = null;
    private RemoteNodeObject currentRemoteNode = null;


    public Session(String key, DataSource dataSource) {
        this.key = key;
        this.dataSource = dataSource;
        this.connection = dataSource.getConnection(this);
    }

    public Connection getConnection() {
        return connection;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void init() {
        log                   = Core.getLogger(this.getClass());
    }

    public String getKey() {
        return key;
    }

    public DatabaseAdapter getDatabaseAdapter() {
        return connection.getDatabaseAdapter();
    }

    public Logger getLog() {
        return log;
    }

    public Session getParentSession() {
        return parentSession;
    }

    public void setParentSession(Session parentSession) {
        this.parentSession = parentSession;
    }

    public void addChildSession(Session session) {
        childSessions.add(session);
        session.setParentSession(this);
    }

    public ArrayList<Session> getChildSessions() {
        return childSessions;
    }

    private static final String DEFAULT_SERVICES_SCOPE = "#";

    private HashMap<String, String> serviceInitializationQueue = new HashMap<>();

    public synchronized <T extends Service> T getService(Class<T> clazz) {
        return getService(clazz, null);
    }

    public synchronized <T extends Service> T getService(Class<T> clazz, String scope) {

        if ( clazz == null ) {
            throw new CoreException("Cannot get service of NULL class");
        }

        String scopeKey   = scope != null ? scope : DEFAULT_SERVICES_SCOPE;

        HashMap<Class, Service> scopeCache = servicesCache.get(scopeKey);
        if ( scopeCache == null ) {
            scopeCache = new HashMap<>();
            servicesCache.put(scopeKey, scopeCache);
        }

        T service = (T) scopeCache.get(clazz);
        if ( service != null ) {
            return service;
        }

        try {
            service = clazz.newInstance();
        } catch (InstantiationException e) {
            throw new CoreException("InstantiationException on service '?' new instance : ?", clazz.getName(), e.getMessage(), e);
        } catch (IllegalAccessException e) {
            throw new CoreException("IllegalAccessException on service '?' new instance : ?", clazz.getName(), e.getMessage(), e);
        }

        scopeCache.put(clazz, service);
        service.setSession(this);
        service.setServiceScope(scopeKey);
        service.init();

        return service;
    }

    public synchronized void removeServiceFromCache(Service service) {

        HashMap<Class, Service> scopeCache = servicesCache.get(service.getServiceScope());
        scopeCache.remove(service.getClass());

        if ( scopeCache.size() == 0 ) {
            servicesCache.remove(service.getServiceScope());
        }

    }

    public void destroy() {

        for ( Session child : childSessions ){
            child.destroy();
        }
        childSessions.clear();

        for (HashMap<Class,Service> scopeCache : servicesCache.values()) {

            for(Object object : scopeCache.values().toArray()) {
                ((Service) object).destroy();
            }
            scopeCache.clear();
        }
        servicesCache.clear();

        ServiceExtensionManager.clearSessionCache(this.key);

        rollback();
        connection.close();

    }

    private boolean inCommit;
    public boolean isInCommit() {
        return inCommit;
    }


    public void commit() {
        if ( connection != null ) {
            connection.commit();
        }
        connection.commit();
        try {
            inCommit = true;

            for (Session child : childSessions) {
                child.commit();
            }
            connection.commit();
            currentTransaction = null;

        } catch (Exception e) {
            e.printStackTrace();
            throw new CoreException("System exception on commit : ?", new Object[]{e.getMessage()}, e);
        } finally {
            inCommit = false;
        }

    }

    private boolean inRollback;
    public boolean isInRollback() {
        return inRollback;
    }

    public void rollback() {
        if ( connection != null ) {
            connection.rollback();
        }
        connection.rollback();
        try {
            inRollback = true;

            for (Session child : childSessions ) {
                child.rollback();
            }
            connection.rollback();
            currentTransaction = null;
        } catch (Exception e) {
            throw new CoreException("System exception on rollback : ?", new Object[]{e.getMessage()}, e);
        } finally {
            inRollback = false;
        }
    }

    public Savepoint getSavepoint() {
        return getTransactionManager().getSavepoint(getCurrentTransaction());
    }

    public void rollbackToSavepoint(Savepoint savepoint) {
        getTransactionManager().rollbackToSavepoint(savepoint);
        for (Session child : childSessions) {
            child.rollback();
        }
    }

    TransactionManager transactionManager;
    public TransactionManager getTransactionManager() {
        if ( transactionManager == null ) {
            transactionManager = getService(TransactionManager.class);
        }
        return transactionManager;
    }

    Transaction currentTransaction;

    public Transaction startTransation() {
        if ( currentTransaction == null ) {
            currentTransaction = getTransactionManager().startTransaction();
        }
        return currentTransaction;
    }

    public Transaction getCurrentTransaction() {
        if ( currentTransaction == null ) {
            startTransation();
        }
        return currentTransaction;
    }

    public TransactionAction newObjectTransactionAction(TransactionActionType actionType, BigDecimal objectId) {
//        getTransactionManager().lockObject(getCurrentTransaction(), objectId);
        return getTransactionManager().newAction(getCurrentTransaction(), actionType, objectId);
    }

    private UserService userService;
    private UserService getUserService() {
        if ( userService == null ) {
            userService = getService(UserService.class);
        }
        return userService;
    }

    private RemoteNodeService remoteNodeService;
    private RemoteNodeService getRemoteNodeService() {
        if ( remoteNodeService == null ) {
            remoteNodeService = getService(RemoteNodeService.class);
        }
        return remoteNodeService;
    }

    public boolean isRegistered() {
        return currentUser != null;
    }

    public BigDecimal getUserId() {
        return currentUser != null ? currentUser.getId() : null;
    }

    public String getUserName() {
        return currentUser != null ? currentUser.getName() : null;
    }

    public BigDecimal getRemoteNodeId() {
        return currentRemoteNode != null ? currentRemoteNode.getId() : null;
    }

    public RemoteNodeObject getRemoteNode() {
        return currentRemoteNode;
    }

    SecurityBase securityBase;
    private SecurityBase getSecurityBase() {
        if ( securityBase == null ) {
            securityBase = getService(SecurityBase.class);
        }
        return securityBase;
    }

    private boolean checkPassword(String password, String passwordHash) {
        return getSecurityBase().validatePassword(password, passwordHash);
    }


    private void setCurrentNodeRegisteredUserId(BigDecimal userId) {
        if ( currentRemoteNode != null ) {
            currentRemoteNode.setRegisteredUserId(userId);
            // Save in another session to avoid commit in current session
            Session saveSession = Core.newSession();
            RemoteNodeObject node = saveSession.getService(RemoteNodeService.class).load(currentRemoteNode.getId());
            node.setRegisteredUserId(userId);
            node.save();
            saveSession.commit();
            saveSession.destroy();
        }

    }

    private void setCurrentUser(UserObject user) {
        currentUser = user;

        setCurrentNodeRegisteredUserId(user.getId());

/*
        Statement statement = executeQuery("select p.id, p.name\n" +
                " from cw_core_user_roles ur, cw_core_role_permissions rp, cw_core_permissions p\n" +
                " where rp.role_id = ur.role_id\n" +
                "   and ur.user_id = ?\n" +
                "   and p.id       = rp.permission_id", user.getId());

*/
        permissionsCache = new HashMap<>();
/*
        while ( statement.next() ) {
            Permission permission = new Permission();

            permission.setId(statement.getBigDecimal(1));
            permission.setName(statement.getString(2));

            permissionsCache.put(permission.getId(), permission);
        }
        statement.close();
*/
    }

    private void logout(boolean clearNodeRegisteredUser) {
        currentUser      = null;
        permissionsCache = new HashMap<>();
        if ( clearNodeRegisteredUser ) {
            setCurrentNodeRegisteredUserId(null);
        }
    }

    public void logout() {
        logout(true);
    }


    public boolean login(String login, String password) {
        logout();

        BigDecimal userId = getUserService().nameToId(login);
        if ( userId == null ) {
            return false;
        }

        UserObject user = getUserService().load(userId);
        if ( ! checkPassword(password, user.getPasswordHash())) {
            return false;
        }

        setCurrentUser(user);

        return true;
    }

    public void login(String login) {
        if ( ! Core.isProtectedLoginMode()) {
            throw new CoreException("Wrong protected login method call");
        }

        BigDecimal userId = getUserService().nameToId(login);
        if ( userId == null ) {
            throw new CoreException("Unknown login '?'", login);
        }
        UserObject user = getUserService().load(userId);

        setCurrentUser(user);
    }


    public void logoutRemotNode() {
        logout(false);
        currentRemoteNode = null;
    }

    public boolean loginRemoteNode(String nodeName, String password) {
        logoutRemotNode();

        BigDecimal       nodeId = getRemoteNodeService().nameToId(nodeName);
        if ( nodeId == null ) {
            return false;
        }
        RemoteNodeObject node = getRemoteNodeService().load(nodeId);
        if ( ! checkPassword(password, node.getPasswordHash())) {
            return false;
        }
        currentRemoteNode = node;
        if ( currentRemoteNode.getRegisteredUserId() != null ) {
            setCurrentUser(getUserService().load(currentRemoteNode.getRegisteredUserId()));
        }

        return true;
    }

    public Statement executeQuery(String sql, Object... parameters) {
        return getConnection().executeQuery(sql, parameters);
    }

    public BigDecimal executeQueryBigDecimal(String sql, Object... parameters) {
        return getConnection().executeQueryBigDecimal(sql, parameters);
    }

    public String executeQueryString(String sql, Object... parameters) {
        return getConnection().executeQueryString(sql, parameters);
    }

    public Date executeQueryDate(String sql, Object... parameters) {
        return getConnection().executeQueryDate(sql, parameters);
    }

    public int executeUpdate(String sql, Object... parameters) {
        return getConnection().executeUpdate(sql, parameters);
    }

    public void throwException(String code, Object... parameters ) {
        throw new CoreException(this, code, parameters);
    }

}
