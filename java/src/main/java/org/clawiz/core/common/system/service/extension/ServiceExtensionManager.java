/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.service.extension;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.service.Service;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

public class ServiceExtensionManager extends Service {

    private static ServiceExtensionConfigList configs = new ServiceExtensionConfigList();

    public Object executeCallExtensionFucntion(Object... parameters) {
        return null;
    }

    public static void clearServiceExtensionConfigs() {
        configs.clear();
    }

    public static void registerServiceExtension(ServiceExtensionConfig extensionConfig) {
        configs.add(extensionConfig);
    }

    public static void registerServiceExtensions(ServiceExtensionConfigList extensionConfigs) {

        for (ServiceExtensionConfig extensionConfig : extensionConfigs) {
            registerServiceExtension(extensionConfig);
        }

    }

    private static final ConcurrentHashMap<Class, ConcurrentHashMap<String, ServiceExtensionMethodExecuteContext>> methodContextsCache = new ConcurrentHashMap<>();

    private static ServiceExtensionMethodExecuteContext getMethodContext(Class clazz, String methodName, Object... parameters) {

        if (methodName == null ) {
            throw new CoreException("Cannot call service extension ? method with null name", clazz.getName());

        }

        ConcurrentHashMap<String, ServiceExtensionMethodExecuteContext> namesCache = methodContextsCache.get(clazz);
        if ( namesCache == null ) {
            namesCache = new ConcurrentHashMap<>();
            methodContextsCache.put(clazz, namesCache);
        }

        ServiceExtensionMethodExecuteContext context = namesCache.get(methodName);
        if ( context != null ) {
            return context;
        }

        context = new ServiceExtensionMethodExecuteContext();

        for (Method method : clazz.getMethods() ) {
            ServiceExtensionMethod annotation = method.getAnnotation(ServiceExtensionMethod.class);
            if ( annotation != null && methodName.equals(annotation.name())) {
                context.method = method;
                break;
            }
        }


        namesCache.put(methodName, context);

        return context;

    }

    private static ConcurrentHashMap<String, ConcurrentHashMap<String, ConcurrentHashMap<Class, ServiceExtension>>> sessionExtensionsCache = new ConcurrentHashMap<>();


    public static Object processExtensionMethods(Service service, String serviceScope, String methodName, ExtensionExecuteMode executeMode, Object... parameters) {

        for ( ServiceExtensionConfig config : configs.getServiceExtensions(service.getClass()) ) {

            ConcurrentHashMap<String, ConcurrentHashMap<Class, ServiceExtension>> scopesCache = sessionExtensionsCache.get(service.getSession().getKey());
            if ( scopesCache == null ) {
                scopesCache = new ConcurrentHashMap<>();
                sessionExtensionsCache.put(service.getSession().getKey(), scopesCache);
            }

            ConcurrentHashMap<Class, ServiceExtension> extensionsCache = scopesCache.get(serviceScope);
            if ( extensionsCache == null ) {
                extensionsCache = new ConcurrentHashMap<>();
                scopesCache.put(serviceScope, extensionsCache);
            }

            ServiceExtension extension = extensionsCache.get(config.getExtensionClass());
            if ( extension == null ) {
                extension = (ServiceExtension) service.getService(config.extensionClass, serviceScope);
                extension.setExtendedService(service);
                extensionsCache.put(config.extensionClass, extension);
            }

            Method method = getMethodContext(config.extensionClass, methodName, parameters).method;
            if ( method == null ) {
                continue;
            }

            try {
                Object object = method.invoke(extension, parameters);
                if ( object != null ) {
                    if ( executeMode == ExtensionExecuteMode.FIRST_NOT_NULL) {
                        return object;
                    } else if ( executeMode == ExtensionExecuteMode.FIRST_TRUE && ((Boolean) object) == true) {
                        return object;
                    }
                }
            } catch (Exception e) {
                service.throwException("Exception on call service ? extension ? method ? : ?", service.getClass().getName(), config.extensionClass.getName(), methodName, e.getCause(), e);
            }


        }

        if ( executeMode == ExtensionExecuteMode.FIRST_TRUE ) {
            return new Boolean(false);
        }
        return null;

    }

    public static void clearSessionCache(String sessionKey) {
        sessionExtensionsCache.remove(sessionKey);
    }

}
