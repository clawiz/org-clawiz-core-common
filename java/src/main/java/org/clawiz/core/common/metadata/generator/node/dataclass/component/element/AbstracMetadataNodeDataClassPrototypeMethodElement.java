package org.clawiz.core.common.metadata.generator.node.dataclass.component.element;

import org.clawiz.core.common.metadata.generator.node.dataclass.component.MetadataNodeDataClassPrototypeComponent;
import org.clawiz.core.common.metadata.data.language.node.element.Structure;
import org.clawiz.core.common.system.generator.abstractgenerator.component.AbstractComponent;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;

public class AbstracMetadataNodeDataClassPrototypeMethodElement extends JavaMethodElement {

    private MetadataNodeDataClassPrototypeComponent component;

    @Override
    public MetadataNodeDataClassPrototypeComponent getComponent() {
        return component;
    }

    @Override
    public void setComponent(AbstractComponent component) {
        super.setComponent(component);
        this.component = (MetadataNodeDataClassPrototypeComponent) component;
    }

    public Structure getStructure() {
        return component.getStructure();
    }

}
