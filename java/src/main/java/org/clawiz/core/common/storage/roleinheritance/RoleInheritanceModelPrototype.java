package org.clawiz.core.common.storage.roleinheritance;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;
import org.clawiz.core.common.system.service.NotInitializeService;

public class RoleInheritanceModelPrototype extends TypeModel {
    
    private static TypeField ROLE_ID_FIELD;
    
    private static TypeField INHERITED_ROLE_ID_FIELD;
    
    private static Type type;
    
    private RoleInheritanceService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public RoleInheritanceService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(RoleInheritanceService.class).getType();
        
        
        ROLE_ID_FIELD = type.getFields().get("Role");
        if ( ROLE_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.RoleInheritance.Role"}); }
        
        INHERITED_ROLE_ID_FIELD = type.getFields().get("InheritedRole");
        if ( INHERITED_ROLE_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.RoleInheritance.InheritedRole"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _roleId;
    
    public TypeFieldModel roleId() {
        
        if ( _roleId != null ) {
            return _roleId;
        }
        
        _roleId = new TypeFieldModel(this, ROLE_ID_FIELD);
        return _roleId;
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.role.RoleModel _roleTypeModel;
    
    public org.clawiz.core.common.storage.role.RoleModel role() {
        
        if ( _roleTypeModel == null ) {
            org.clawiz.core.common.storage.role.RoleModel model = getService(org.clawiz.core.common.storage.role.RoleModel.class, this.toString() + "_Role");
            model.setJoinField(roleId());
            _roleTypeModel = model;
        }
        
        return _roleTypeModel;
    }
    
    private TypeFieldModel _inheritedRoleId;
    
    public TypeFieldModel inheritedRoleId() {
        
        if ( _inheritedRoleId != null ) {
            return _inheritedRoleId;
        }
        
        _inheritedRoleId = new TypeFieldModel(this, INHERITED_ROLE_ID_FIELD);
        return _inheritedRoleId;
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.role.RoleModel _inheritedRoleTypeModel;
    
    public org.clawiz.core.common.storage.role.RoleModel inheritedRole() {
        
        if ( _inheritedRoleTypeModel == null ) {
            org.clawiz.core.common.storage.role.RoleModel model = getService(org.clawiz.core.common.storage.role.RoleModel.class, this.toString() + "_InheritedRole");
            model.setJoinField(inheritedRoleId());
            _inheritedRoleTypeModel = model;
        }
        
        return _inheritedRoleTypeModel;
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "ROLE" : return roleId();
        case "INHERITEDROLE" : return inheritedRoleId();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
