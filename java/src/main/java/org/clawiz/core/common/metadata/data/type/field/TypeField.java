/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.metadata.data.type.field;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.property.database.TableColumnTypeFieldProperty;
import org.clawiz.core.common.metadata.data.type.field.property.validator.RequiredTypeFieldProperty;
import org.clawiz.core.common.metadata.data.type.key.TypeKey;
import org.clawiz.core.common.metadata.data.type.key.TypeKeyField;
import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;
import org.clawiz.core.common.utils.StringUtils;


public class TypeField extends TypeFieldPrototype {

    public Type getType() {
        return (Type) getParentNode();
    }

    @Override
    public String getDescription() {
        return super.getDescription() != null ? super.getDescription() : getName();
    }

    public static String fieldNameToColumnName(String name, AbstractValueType valueType) {
        String _columnName = GeneratorUtils.toLowerWithUnderscore(GeneratorUtils.toJavaName(name, true));
        if ( valueType instanceof ValueTypeObject) {
            _columnName = StringUtils.substring(_columnName, 0, 27) + "_id";
        } else {
            _columnName = StringUtils.substring(_columnName, 0, 30);
        }
        return _columnName;
    }

    private String _columnName;
    public String getColumnName() {
        if ( _columnName != null ) {
            return _columnName;
        }
        TableColumnTypeFieldProperty property = getProperties().getNodeByClass(TableColumnTypeFieldProperty.class);

        _columnName = property != null ? property.getColumnName() : fieldNameToColumnName(getName(), getValueType());

        return _columnName;
    }

    public String getUpperColumnName() {
        if ( ! isUpperFieldExists() ) {
            return null;
        }
        return StringUtils.substring("upper_" + getColumnName(), 0, 30);
    }


    String valueTypeJavaClassName;
    public String getValueTypeJavaClassName() {
        if ( valueTypeJavaClassName != null ) {
            return valueTypeJavaClassName;
        }
        if ( getValueType() instanceof ValueTypeEnumeration) {
            valueTypeJavaClassName = StringUtils.toUpperFirstChar(getJavaVariableName());

        } else if ( getValueType() instanceof ValueTypeList ) {

            Type referencedType  = ((ValueTypeList) getValueType()).getReferencedType();
            if ( referencedType != null ) {
                valueTypeJavaClassName = referencedType.getServicePackageName() + "." + referencedType.getJavaClassName() + "List";
            } else {
                // in fact this impossible, because list type cannot be processed without referenced type
            }

        } else {
            valueTypeJavaClassName = getValueType().getJavaClass().getName();
        }
        return valueTypeJavaClassName;
    }

    public boolean isAutoUppercase() {
        return false;
    }

    public boolean isRequired() {
        return getProperties().getNodeByClass(RequiredTypeFieldProperty.class) != null;
    }

    private String javaNameSuffix;
    private String javaNameSuffixWithoutId;
    private String javaVariableName;
    private String javaVariableNameWithoutId;
    private String getTypeModelMethodName;
    public String getJavaNameSuffix() {

        if ( javaNameSuffix == null ) {

            String _javaNameSuffix = GeneratorUtils.toJavaName(getName(), true);

            getTypeModelMethodName    = StringUtils.toLowerFirstChar(_javaNameSuffix);

            _javaNameSuffix         = StringUtils.toUpperFirstChar(_javaNameSuffix);
            javaNameSuffixWithoutId = _javaNameSuffix;

            if ( getValueType() != null && StringUtils.isLike(getValueType().getClass().getName(), "*Process*") ) {
                int i = 1;
            }

            _javaNameSuffix   = _javaNameSuffix + ( getValueType() instanceof ValueTypeObject ? "Id" : "" );

            javaVariableName          = StringUtils.toLowerFirstChar(_javaNameSuffix);
            javaVariableNameWithoutId = StringUtils.toLowerFirstChar(javaNameSuffixWithoutId);

            javaNameSuffix = _javaNameSuffix;

        }

        return javaNameSuffix;
    }

    public String getJavaVariableName() {
        
        if ( javaVariableName == null ) {
            getJavaNameSuffix();
        }

        return javaVariableName;
    }

    public String getJavaVariableNameWithoutId() {

        if ( javaVariableName == null ) {
            getJavaNameSuffix();
        }

        return javaVariableNameWithoutId;
    }

    String _javaClassName;
    @Override
    public String getJavaClassName() {
        if ( _javaClassName == null ) {
            _javaClassName = StringUtils.toLowerFirstChar(getJavaVariableName());
        }
        return _javaClassName;
    }

    public String getGetTypeModelMethodName() {
        if ( getTypeModelMethodName == null ) {
            getJavaNameSuffix();
        }
        return getTypeModelMethodName;
    }

    public String getGetMethodName() {
        return getValueType().getGetNamePrefix() + getJavaNameSuffix();
    }

    public String getSetMethodName() {
        return "set" + getJavaNameSuffix();
    }

    public boolean isUpperFieldExists() {

        if ( ! getValueType().isCanBeUpperCased() ) {
            return false;
        }

        for ( TypeKey key : getType().getKeys() ) {
            for ( TypeKeyField keyField : key.getFields() ) {
                if ( this.getName().equals( keyField.getField().getName() )  ) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean equals( Object object ){
        if ( object == null ) {
            return false;
        }
        TypeField field = (TypeField) object;
        if (getType().getFullName().equalsIgnoreCase(field.getType().getFullName()) &&
                this.getName().equalsIgnoreCase(field.getName())){
            return true;
        }
        return false;
    }

    @Override
    public String getParentNodeFieldName() {
        return "fields";
    }

}
