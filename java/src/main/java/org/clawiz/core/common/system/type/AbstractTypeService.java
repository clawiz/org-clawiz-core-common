/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type;

import org.clawiz.core.common.storage.type.TypeService;
import org.clawiz.core.common.system.object.ObjectService;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.object.AbstractObjectList;
import org.clawiz.core.common.metadata.data.type.Type;

import java.math.BigDecimal;

public abstract class AbstractTypeService <T extends AbstractObject> extends Service {

    private   BigDecimal typeId;
    protected Type       type;

    public BigDecimal getTypeId() {
        return typeId;
    }

    protected void setTypeId(BigDecimal typeId) {
        this.typeId = typeId;
    }

    public Type getType() {
        if ( type == null ) {
            type = getService(TypeService.class).getById(getTypeId());
        }
        return type;
    }

    protected void setType(Type type) {
        this.type = type;
    }

    abstract public void check(T object);

    abstract public T create();

    abstract public T load(BigDecimal id);

    abstract public AbstractObjectList<T> loadList(String whereClause, Object[] parameters);

    public AbstractObjectList<T> loadList(String whereClause, String orderByClause, Object[] parameters) {
        return null;
    }

    public AbstractObjectList<T> loadOrderedList(String whereClause, String orderByClause, Object[] parameters) {
        return null;
    }

    abstract public String idToString(BigDecimal id);

    abstract public void save(T object);

    @NotInitializeService
    private ObjectService _objectService;

    public ObjectService getObjectService() {
        if ( _objectService == null ) {
            _objectService = getService(ObjectService.class);
        }
        return _objectService;
    }

}
