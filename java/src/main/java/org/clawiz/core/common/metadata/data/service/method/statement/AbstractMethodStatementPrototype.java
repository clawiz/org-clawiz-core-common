package org.clawiz.core.common.metadata.data.service.method.statement;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractMethodStatementPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    public AbstractMethodStatement withName(String value) {
        setName(value);
        return (AbstractMethodStatement) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
