package org.clawiz.core.common.metadata.data.type.field.property.condition;

import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractConditionTypeFieldPropertyPrototype extends org.clawiz.core.common.metadata.data.type.field.property.AbstractTypeFieldProperty {
    
    @ExchangeElement
    private org.clawiz.core.common.metadata.data.type.condition.AbstractTypeCondition condition;
    
    public AbstractConditionTypeFieldProperty withName(String value) {
        setName(value);
        return (AbstractConditionTypeFieldProperty) this;
    }
    
    public org.clawiz.core.common.metadata.data.type.condition.AbstractTypeCondition getCondition() {
        return this.condition;
    }
    
    public void setCondition(org.clawiz.core.common.metadata.data.type.condition.AbstractTypeCondition value) {
        this.condition = value;
    }
    
    public AbstractConditionTypeFieldProperty withCondition(org.clawiz.core.common.metadata.data.type.condition.AbstractTypeCondition value) {
        setCondition(value);
        return (AbstractConditionTypeFieldProperty) this;
    }
    
    public <T extends org.clawiz.core.common.metadata.data.type.condition.AbstractTypeCondition> T createCondition(Class<T> nodeClass) {
        if ( getCondition() != null ) {
            throwException("Cannot create already defined value of '?' for '?'", "condition", this.getFullName());
        }
        org.clawiz.core.common.metadata.data.type.condition.AbstractTypeCondition value = createChildNode(nodeClass, "condition");
        setCondition(value);
        return (T) value;
    }
    
    public org.clawiz.core.common.metadata.data.type.condition.AbstractTypeCondition createCondition() {
        return createCondition(org.clawiz.core.common.metadata.data.type.condition.AbstractTypeCondition.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getCondition() != null ) { 
            getCondition().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getCondition());
        
    }
}
