package org.clawiz.core.common.storage.sequence;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;

public class SequenceServicePrototype extends AbstractTypeService<SequenceObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.core.common.storage", "Sequence");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Sequence"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      sequence Checked object
    */
    public void check(SequenceObject sequence) {
        
        if ( sequence == null ) {
            throwException("Cannot check null ?", new Object[]{"SequenceObject"});
        }
        
        sequence.fillDefaults();
        
        
        if ( nameToId(sequence.getName(), sequence.getObject1Id(), sequence.getObject2Id(), sequence.getObject3Id(), sequence.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "Sequence", sequence.toName() });
        }
        
    }
    
    /**
    * Create new SequenceObject instance and fill default values
    * 
    * @return     Created object
    */
    public SequenceObject create() {
        
        SequenceObject sequence = new SequenceObject();
        sequence.setService((SequenceService) this);
        
        sequence.fillDefaults();
        
        return sequence;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public SequenceObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select name, object1_id, object2_id, object3_id, start_value, max_value, last_cached_value, cache_length from cw_core_sequences where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"Sequence", id});
        }
        
        SequenceObject result = new SequenceObject();
        
        result.setService((SequenceService) this);
        result.setId(id);
        result.setName(statement.getString(1));
        result.setObject1Id(statement.getBigDecimal(2));
        result.setObject2Id(statement.getBigDecimal(3));
        result.setObject3Id(statement.getBigDecimal(4));
        result.setStartValue(statement.getBigDecimal(5));
        result.setMaxValue(statement.getBigDecimal(6));
        result.setLastCachedValue(statement.getBigDecimal(7));
        result.setCacheLength(statement.getBigDecimal(8));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of SequenceObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public SequenceList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of SequenceObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public SequenceList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        SequenceList result = new SequenceList();
        
        
        Statement statement = executeQuery("select id, name, object1_id, object2_id, object3_id, start_value, max_value, last_cached_value, cache_length from cw_core_sequences"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            SequenceObject object = new SequenceObject();
        
            object.setService((SequenceService) this);
            object.setId(statement.getBigDecimal(1));
            object.setName(statement.getString(2));
            object.setObject1Id(statement.getBigDecimal(3));
            object.setObject2Id(statement.getBigDecimal(4));
            object.setObject3Id(statement.getBigDecimal(5));
            object.setStartValue(statement.getBigDecimal(6));
            object.setMaxValue(statement.getBigDecimal(7));
            object.setLastCachedValue(statement.getBigDecimal(8));
            object.setCacheLength(statement.getBigDecimal(9));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'Name' fields
    * 
    * @param      name      Name
    * @param      object1Id Object1
    * @param      object2Id Object2
    * @param      object3Id Object3
    * @return     Id of found record or null
    */
    public BigDecimal nameToId(java.lang.String name, java.math.BigDecimal object1Id, java.math.BigDecimal object2Id, java.math.BigDecimal object3Id) {
        return nameToId(name, object1Id, object2Id, object3Id, null);
    }
    
    /**
    * Find id of record by key 'Name' fields with id not equal skipId
    * 
    * @param      name      Name
    * @param      object1Id Object1
    * @param      object2Id Object2
    * @param      object3Id Object3
    * @param      skipId    Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal nameToId(java.lang.String name, java.math.BigDecimal object1Id, java.math.BigDecimal object2Id, java.math.BigDecimal object3Id, BigDecimal skipId) {
        
        if ( name == null || object1Id == null || object2Id == null || object3Id == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_sequences where upper_name = ? and object1_id = ? and object2_id = ? and object3_id = ?", name.toUpperCase(), object1Id, object2Id, object3Id);
        } else {
            return executeQueryBigDecimal("select id from cw_core_sequences where upper_name = ? and object1_id = ? and object2_id = ? and object3_id = ? and id != ?", name.toUpperCase(), object1Id, object2Id, object3Id, skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'Name' fields or create new record with values set to given parameters
    * 
    * @param      name                Name
    * @param      object1Id           Object1
    * @param      object2Id           Object2
    * @param      object3Id           Object3
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal nameToId(java.lang.String name, java.math.BigDecimal object1Id, java.math.BigDecimal object2Id, java.math.BigDecimal object3Id, boolean createNewIfNotFound) {
        
        BigDecimal id = nameToId(name, object1Id, object2Id, object3Id, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        SequenceObject object = create();
        object.setName(name);
        object.setObject1Id(object1Id);
        object.setObject2Id(object2Id);
        object.setObject3Id(object3Id);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'Name' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select name, object1_id, object2_id, object3_id from cw_core_sequences where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        SequenceObject object = new SequenceObject();
        object.setService((SequenceService)this);
        object.setId(id);
        object.setName(statement.getString(1));
        object.setObject1Id(statement.getBigDecimal(2));
        object.setObject2Id(statement.getBigDecimal(3));
        object.setObject3Id(statement.getBigDecimal(4));
        
        statement.close();
        
        return object.toName();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToName(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      sequenceObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(SequenceObject sequenceObject) {
        return sequenceObject.toName();
    }
    
    protected void saveAudit(TransactionAction transactionAction, SequenceObject oldSequenceObject, SequenceObject newSequenceObject) {
        
        SequenceObject o = oldSequenceObject != null ? oldSequenceObject : new SequenceObject();
        SequenceObject n = newSequenceObject != null ? newSequenceObject : new SequenceObject();
        
        
        executeUpdate("insert into a_cw_core_sequences (scn, action_type, id , o_name, o_upper_name, o_object1_id, o_object2_id, o_object3_id, o_start_value, o_max_value, o_last_cached_value, o_cache_length, n_name, n_upper_name, n_object1_id, n_object2_id, n_object3_id, n_start_value, n_max_value, n_last_cached_value, n_cache_length) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getName(), o.getName() != null ? o.getName().toUpperCase() : null, o.getObject1Id(), o.getObject2Id(), o.getObject3Id(), o.getStartValue(), o.getMaxValue(), o.getLastCachedValue(), o.getCacheLength(), n.getName(), n.getName() != null ? n.getName().toUpperCase() : null, n.getObject1Id(), n.getObject2Id(), n.getObject3Id(), n.getStartValue(), n.getMaxValue(), n.getLastCachedValue(), n.getCacheLength());
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      sequence Saved object
    */
    public void save(SequenceObject sequence) {
        
        if ( sequence == null ) {
            throwException("Cannot save NULL ?", new Object[]{"SequenceObject"});
        }
        
        TransactionAction transactionAction;
        SequenceObject oldSequence;
        if ( sequence.getService() == null ) {
            sequence.setService((SequenceService)this);
        }
        
        check(sequence);
        
        if ( sequence.getId() == null ) {
        
            sequence.setId(getObjectService().createObject(getTypeId()));
        
            oldSequence = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, sequence.getId());
        
            executeUpdate("insert into cw_core_sequences" 
                          + "( id, name, upper_name, object1_id, object2_id, object3_id, start_value, max_value, last_cached_value, cache_length ) "
                          + "values"
                          + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                          sequence.getId(), sequence.getName(), ( sequence.getName() != null ? sequence.getName().toUpperCase() : null ), sequence.getObject1Id(), sequence.getObject2Id(), sequence.getObject3Id(), sequence.getStartValue(), sequence.getMaxValue(), sequence.getLastCachedValue(), sequence.getCacheLength() );
        
            if ( sequence.getObject1Id() != null ) { getObjectService().setLink(sequence.getObject1Id(), sequence.getId()); }
            if ( sequence.getObject2Id() != null ) { getObjectService().setLink(sequence.getObject2Id(), sequence.getId()); }
            if ( sequence.getObject3Id() != null ) { getObjectService().setLink(sequence.getObject3Id(), sequence.getId()); }
        
        } else {
        
            oldSequence = load(sequence.getId());
            if ( oldSequence.equals(sequence) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, sequence.getId());
        
            executeUpdate("update cw_core_sequences set "
                          + "name = ?, upper_name = ?, object1_id = ?, object2_id = ?, object3_id = ?, start_value = ?, max_value = ?, last_cached_value = ?, cache_length = ? "
                          + "where id = ?",
                          sequence.getName(), ( sequence.getName() != null ? sequence.getName().toUpperCase() : null ), sequence.getObject1Id(), sequence.getObject2Id(), sequence.getObject3Id(), sequence.getStartValue(), sequence.getMaxValue(), sequence.getLastCachedValue(), sequence.getCacheLength(), sequence.getId() );
        
            getObjectService().changeLinkParent(oldSequence.getObject1Id(), sequence.getObject1Id(), sequence.getId());
            getObjectService().changeLinkParent(oldSequence.getObject2Id(), sequence.getObject2Id(), sequence.getId());
            getObjectService().changeLinkParent(oldSequence.getObject3Id(), sequence.getObject3Id(), sequence.getId());
        
        }
        
        saveAudit(transactionAction, oldSequence, sequence);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        SequenceObject oldSequenceObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldSequenceObject, null);
        
        getObjectService().deleteObject(id);
        
        if (oldSequenceObject.getObject1Id() != null) { getObjectService().deleteLink(oldSequenceObject.getObject1Id(), id); }
        if (oldSequenceObject.getObject2Id() != null) { getObjectService().deleteLink(oldSequenceObject.getObject2Id(), id); }
        if (oldSequenceObject.getObject3Id() != null) { getObjectService().deleteLink(oldSequenceObject.getObject3Id(), id); }
        
        executeUpdate("delete from cw_core_sequences where id = ?", id);
        
    }
}
