package org.clawiz.core.common.storage.user;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;

public class UserModelPrototype extends TypeModel {
    
    private static TypeField NAME_FIELD;
    
    private static TypeField PASSWORD_FIELD;
    
    private static TypeField PASSWORD_HASH_FIELD;
    
    private static TypeField PREVIOUS_PASSWORD_FIELD;
    
    private static TypeField EMAIL_FIELD;
    
    private static TypeField FIRST_NAME_FIELD;
    
    private static TypeField LAST_NAME_FIELD;
    
    private static Type type;
    
    private UserService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public UserService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(UserService.class).getType();
        
        
        NAME_FIELD = type.getFields().get("Name");
        if ( NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.User.Name"}); }
        
        PASSWORD_FIELD = type.getFields().get("Password");
        if ( PASSWORD_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.User.Password"}); }
        
        PASSWORD_HASH_FIELD = type.getFields().get("PasswordHash");
        if ( PASSWORD_HASH_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.User.PasswordHash"}); }
        
        PREVIOUS_PASSWORD_FIELD = type.getFields().get("PreviousPassword");
        if ( PREVIOUS_PASSWORD_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.User.PreviousPassword"}); }
        
        EMAIL_FIELD = type.getFields().get("email");
        if ( EMAIL_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.User.email"}); }
        
        FIRST_NAME_FIELD = type.getFields().get("FirstName");
        if ( FIRST_NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.User.FirstName"}); }
        
        LAST_NAME_FIELD = type.getFields().get("LastName");
        if ( LAST_NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.User.LastName"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _name;
    
    public TypeFieldModel name() {
        
        if ( _name != null ) {
            return _name;
        }
        
        _name = new TypeFieldModel(this, NAME_FIELD);
        return _name;
        
    }
    
    private TypeFieldModel _password;
    
    public TypeFieldModel password() {
        
        if ( _password != null ) {
            return _password;
        }
        
        _password = new TypeFieldModel(this, PASSWORD_FIELD);
        return _password;
        
    }
    
    private TypeFieldModel _passwordHash;
    
    public TypeFieldModel passwordHash() {
        
        if ( _passwordHash != null ) {
            return _passwordHash;
        }
        
        _passwordHash = new TypeFieldModel(this, PASSWORD_HASH_FIELD);
        return _passwordHash;
        
    }
    
    private TypeFieldModel _previousPassword;
    
    public TypeFieldModel previousPassword() {
        
        if ( _previousPassword != null ) {
            return _previousPassword;
        }
        
        _previousPassword = new TypeFieldModel(this, PREVIOUS_PASSWORD_FIELD);
        return _previousPassword;
        
    }
    
    private TypeFieldModel _email;
    
    public TypeFieldModel email() {
        
        if ( _email != null ) {
            return _email;
        }
        
        _email = new TypeFieldModel(this, EMAIL_FIELD);
        return _email;
        
    }
    
    private TypeFieldModel _firstName;
    
    public TypeFieldModel firstName() {
        
        if ( _firstName != null ) {
            return _firstName;
        }
        
        _firstName = new TypeFieldModel(this, FIRST_NAME_FIELD);
        return _firstName;
        
    }
    
    private TypeFieldModel _lastName;
    
    public TypeFieldModel lastName() {
        
        if ( _lastName != null ) {
            return _lastName;
        }
        
        _lastName = new TypeFieldModel(this, LAST_NAME_FIELD);
        return _lastName;
        
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "NAME" : return name();
        case "PASSWORD" : return password();
        case "PASSWORDHASH" : return passwordHash();
        case "PREVIOUSPASSWORD" : return previousPassword();
        case "EMAIL" : return email();
        case "FIRSTNAME" : return firstName();
        case "LASTNAME" : return lastName();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
