/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.service;

import org.clawiz.core.common.Core;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.logger.Logger;
import org.clawiz.core.common.system.service.extension.ExtensionExecuteMode;
import org.clawiz.core.common.system.service.extension.ServiceExtensionManager;
import org.clawiz.core.common.utils.RandomGUID;
import org.clawiz.core.common.utils.reflection.ReflectionUtils;
import org.clawiz.core.common.system.session.transaction.Savepoint;
import org.clawiz.core.common.system.session.Session;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

/**
 * (c) Clawiz
 */
public class Service {

    private Session session;

    private String  serviceScope;

    private Logger  logger;

    public void init() {
        logger = Core.getLogger(this.getClass());
        initServices(this.getClass());
    }

    public Session getSession() {
        return session;
    }

    public String getServiceScope() {
        return serviceScope;
    }

    public void setServiceScope(String serviceScope) {
        this.serviceScope = serviceScope;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public void logInfo(String message) {
        logger.info(message);
    }

    public void logDebug(String message)  {
        logger.debug(message);
    }

    public void logError(String message) {
        logger.error(message);
    }

    public Logger getLogger() {
        return logger;
    }

    private static ConcurrentHashMap<Class, ArrayList<Field>> classServicesInitializationCache = new ConcurrentHashMap<>();

    private void initServices(Class clazz) {
        if ( clazz == null ) {
            return;
        }

        initServices(clazz.getSuperclass());

        ArrayList<Field> serviceFields = classServicesInitializationCache.get(clazz);
        if ( serviceFields == null ) {
            serviceFields = new ArrayList<>();

            for (Field field : clazz.getDeclaredFields() ) {
                Class fieldClazz = field.getType();
                if ( Service.class.isAssignableFrom(fieldClazz) ) {
                    if ( Modifier.isStatic(field.getModifiers()) ) {
                        continue;
                    }


                    boolean skipInitialization = false;
                    for ( Annotation a : field.getDeclaredAnnotations() ) {
                        if ( a.annotationType() == NotInitializeService.class ) {
                            skipInitialization = true;
                            break;
                        }
                    }

                    if (ReflectionUtils.isAnnotationPresent(fieldClazz, NotInitializeService.class)) {
                        skipInitialization = true;
                    }

                    if ( ! skipInitialization ) {
                        field.setAccessible(true);
                        serviceFields.add(field);
                    }

                }
            }

            classServicesInitializationCache.put(clazz, serviceFields);
        }

        for (Field field : serviceFields ) {

            try {
                Class fieldClazz = field.getType();
                Service s = getSession().getService(fieldClazz);
                field.set(this, s);
            } catch (Exception e) {
                throwException("System exception on init service methods of '?' : ?", new Object[]{this.getClass().getName(), e.getMessage()}, e);
            }
        }

    }

    private void clearServices(Class clazz) {
        if ( clazz == null ) {
            return;
        }

        ArrayList<Field> serviceFields = classServicesInitializationCache.get(clazz);
        for (Field field : serviceFields ) {

            try {
                field.set(this, null);
            } catch (Exception e) {
                throwException("System exception on init service methods of '?' : ?", new Object[]{this.getClass().getName(), e.getMessage()}, e);
            }
        }
    }


    public void destroy() {
        clearServices(this.getClass());
        session.removeServiceFromCache(this);
    }

    public void throwException(String code, Object... parameters ) {
        throw new CoreException(getSession(), code, parameters);
    }

    public CoreException coreException(String code, Object... parameters ) {
        return new CoreException(getSession(), code, parameters);
    }

    public <T extends Service> T getService(Class<T> clazz) {
        return getSession().getService(clazz);
    }


    public <T extends Service> T getService(Class<T> clazz, boolean newInstance) {
        return getService(clazz, new RandomGUID().toString());
    }

    public <T extends Service> T getService(Class<T> clazz, String scope) {
        return getSession().getService(clazz, scope);
    }

    public void commit() {
        session.commit();
    }

    public void rollback() {
        session.rollback();
    }

    public Savepoint getSavepoint() {
        return getSession().getSavepoint();
    }

    public void rollbackToSavepoint(Savepoint savepoint) {
        getSession().rollbackToSavepoint(savepoint);
    }

    public Statement executeQuery(String sql, Object... parameters) {
        return getSession().executeQuery(sql, parameters);
    }

    public BigDecimal executeQueryBigDecimal(String sql, Object... parameters) {
        return getSession().executeQueryBigDecimal(sql, parameters);
    }

    public String executeQueryString(String sql, Object... parameters) {
        return getSession().executeQueryString(sql, parameters);
    }

    public Date executeQueryDate(String sql, Object... parameters) {
        return getSession().executeQueryDate(sql, parameters);
    }


    public int executeUpdate(String sql, Object... parameters) {
        return getSession().executeUpdate(sql, parameters);
    }


    protected static <T> T notNull(T value, T replacement) {
        return (value != null) ? value : replacement;
    }


    public Object processExtensions(String methodName, Object... parameters) {
        return ServiceExtensionManager.processExtensionMethods(this, this.serviceScope, methodName, ExtensionExecuteMode.ALL, parameters);
    }

    public Object processExtensions(String methodName, ExtensionExecuteMode callMode, Object... parameters) {
        return ServiceExtensionManager.processExtensionMethods(this, this.serviceScope, methodName, callMode, parameters);
    }

}
