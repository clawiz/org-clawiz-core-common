/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.typescript.component;

import java.util.ArrayList;
import java.util.HashMap;

public class AbstractTypeScriptEnumComponent extends AbstractTypeScriptClassComponent {

    ArrayList<String> values            = new ArrayList<>();
    HashMap<String, String> valuesCache = new HashMap<>();

    @SuppressWarnings("Duplicates")
    public void addValue(String value) {
        if ( value == null ) {
            throwException("Enumeration ? value cannot be null", getName());
        }
        String key = value.toUpperCase();
        if ( valuesCache.containsKey(key)) {
            throwException("Enumeration ? already have value '?'", getName(), value);
        }
        valuesCache.put(key,key);
        values.add(key);
    }

    public ArrayList<String> getValues() {
        return values;
    }


    @Override
    public void write() {
        writeImports();

        writeDecorators();

        String str    = "";
        String prefix = "";
        for (String value : getValues() ) {
            str += prefix + value;
            prefix = ", ";
        }

        pln("export enum " + getName() + " {");

        pln("");
        pln("  " + str + "");
        pln("");

        writeElements();

        pln("");
        pln("}");

    }
}
