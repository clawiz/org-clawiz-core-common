package org.clawiz.core.common.metadata.data.service.method.valuetype;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ServiceMethodValueTypeStringPrototype extends org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType {
    
    public ServiceMethodValueTypeString withName(String value) {
        setName(value);
        return (ServiceMethodValueTypeString) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
