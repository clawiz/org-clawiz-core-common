/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.typescript.component.element.decorator;

import org.clawiz.core.common.system.generator.file.component.AbstractTextFileComponent;
import org.clawiz.core.common.system.generator.javascript.component.element.json.JavaScriptJsonObjectElement;

import java.util.ArrayList;

public abstract class AbstractTypeScriptDecoratorElement extends JavaScriptJsonObjectElement {

    abstract public String getName();

    ArrayList<String> parameters = new ArrayList<>();

    public void addParameter(String parameter) {
        parameters.add(parameter);
    }

    public ArrayList<String> getParameters() {
        return parameters;
    }

    @Override
    public void write() {
        setWriteCurlyBraces(false);

        String parametersString = "";
        String prefix           = "";
        for(String parameter : getParameters() ) {
            parametersString += prefix + parameter;
            prefix           = "";
        }

        p("@" + getName()
                + ( getParameters().size() > 0 || getElements().size() > 0 ? "(" : "")
                + parametersString
                + (getElements().size() > 0 ? ( (getParameters().size() > 0 ? ", " : "") +  "{\n") : ""
        ));

        super.write();


        if ( getElements().size() > 0 || getParameters().size() > 0) {
            ((AbstractTextFileComponent)getComponent()).pln( (getElements().size() > 0 ? getLpad() + "}" : "") + ")");
        }
    }

}
