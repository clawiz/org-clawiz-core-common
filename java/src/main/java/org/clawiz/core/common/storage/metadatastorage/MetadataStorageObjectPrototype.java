package org.clawiz.core.common.storage.metadatastorage;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class MetadataStorageObjectPrototype extends AbstractObject {
    
    /**
    * StorageClass
    */
    private java.math.BigDecimal storageClassId;
    
    private org.clawiz.core.common.storage.metadatastorageclass.MetadataStorageClassObject storageClass;
    
    /**
    * PackageName
    */
    private java.lang.String packageName;
    
    /**
    * Url
    */
    private java.lang.String url;
    
    public MetadataStorageService service;
    
    /**
    * 
    * @return     StorageClass
    */
    public java.math.BigDecimal getStorageClassId() {
        return this.storageClassId;
    }
    
    public org.clawiz.core.common.storage.metadatastorageclass.MetadataStorageClassObject getStorageClass() {
        return getStorageClass(false);
    }
    
    @SuppressWarnings("Duplicates")
    public org.clawiz.core.common.storage.metadatastorageclass.MetadataStorageClassObject getStorageClass(boolean autoCreate) {
        
        if ( storageClass != null ) { return storageClass; }
        
        if ( storageClassId != null ) {
            storageClass = service.loadMetadataStorageClass(storageClassId);
        } else if ( autoCreate ) {
            storageClass = service.createMetadataStorageClass();
        }
        
        return storageClass;
    }
    
    /**
    * Set 'StorageClass' value
    * 
    * @param      storageClassId StorageClass
    */
    public void setStorageClassId(java.math.BigDecimal storageClassId) {
         this.storageClassId = storageClassId;
    }
    
    /**
    * Set 'StorageClass' value and return this object
    * 
    * @param      storageClassId StorageClass
    * @return     MetadataStorage object
    */
    public MetadataStorageObjectPrototype withStorageClassId(java.math.BigDecimal storageClassId) {
        setStorageClassId(storageClassId);
        return this;
    }
    
    /**
    * 
    * @return     PackageName
    */
    public java.lang.String getPackageName() {
        return this.packageName;
    }
    
    /**
    * Set 'PackageName' value
    * 
    * @param      packageName PackageName
    */
    public void setPackageName(java.lang.String packageName) {
        
        if ( packageName != null && packageName.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "MetadataStorage.PackageName", "250");
        }
        
         this.packageName = packageName;
    }
    
    /**
    * Set 'PackageName' value and return this object
    * 
    * @param      packageName PackageName
    * @return     MetadataStorage object
    */
    public MetadataStorageObjectPrototype withPackageName(java.lang.String packageName) {
        setPackageName(packageName);
        return this;
    }
    
    /**
    * 
    * @return     Url
    */
    public java.lang.String getUrl() {
        return this.url;
    }
    
    /**
    * Set 'Url' value
    * 
    * @param      url Url
    */
    public void setUrl(java.lang.String url) {
        
        if ( url != null && url.length() > 1024) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "MetadataStorage.Url", "1024");
        }
        
         this.url = url;
    }
    
    /**
    * Set 'Url' value and return this object
    * 
    * @param      url Url
    * @return     MetadataStorage object
    */
    public MetadataStorageObjectPrototype withUrl(java.lang.String url) {
        setUrl(url);
        return this;
    }
    
    public MetadataStorageService getService() {
        return this.service;
    }
    
    public void setService(MetadataStorageService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(MetadataStorageObjectPrototype  target) {
        target.setStorageClassId(getStorageClassId());
        target.setPackageName(getPackageName());
        target.setUrl(getUrl());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((MetadataStorageObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        if ( storageClass != null ) {
            storageClass.save();
            storageClassId = storageClass.getId();
        }
        service.save((MetadataStorageObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(MetadataStorageObjectPrototype object) {
        return 
               isObjectsEquals(this.getStorageClassId(), object.getStorageClassId() ) 
            && isObjectsEquals(this.getPackageName(), object.getPackageName() ) 
            && isObjectsEquals(this.getUrl(), object.getUrl() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getStorageClassId() != null ? getStorageClassId().hashCode() : 0);
        result = result * 31 + (getPackageName() != null ? getPackageName().hashCode() : 0);
        result = result * 31 + (getUrl() != null ? getUrl().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'StorageClassPackageUrl' fields
    * 
    * @return     Concatenated string values of key 'toStorageClassPackageUrl' fields
    */
    public String toStorageClassPackageUrl() {
        return service.getObjectService().idToString(getStorageClassId()) + "," + getPackageName() + "," + getUrl();
    }
    
    /**
    * Prepare concatenated string values of key 'Url' fields
    * 
    * @return     Concatenated string values of key 'toUrl' fields
    */
    public String toUrl() {
        return getUrl();
    }
}
