package org.clawiz.core.common.metadata.data.type.property.extension.method;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class CheckExtensionTypePropertyMethodPrototype extends org.clawiz.core.common.metadata.data.type.property.extension.method.AbstractExtensionTypePropertyMethod {
    
    public CheckExtensionTypePropertyMethod withName(String value) {
        setName(value);
        return (CheckExtensionTypePropertyMethod) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
