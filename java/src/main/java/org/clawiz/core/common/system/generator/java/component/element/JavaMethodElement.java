/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.java.component.element;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.generator.abstractgenerator.component.element.AbstractElement;
import org.clawiz.core.common.system.generator.java.component.AbstractJavaClassComponent;
import org.clawiz.core.common.utils.StringUtils;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: abdrashitovta
 * Date: 26.05.13
 * Time: 16?2
 * To change this template use File | Settings | File Templates.
 */
public class JavaMethodElement extends JavaMemberElement {


    class Parameter {
        String type;
        String name;
        String javaDoc;
    }

    ArrayList<Parameter> parameters = new ArrayList<>();
    boolean parameterJavaDocExists  = false;

    ArrayList<String>  generics    = new ArrayList<>();


    String returnJavaDoc;
    String seeJavaDoc;
    String deprecatedJavaDoc;

    String throwsException;

    ArrayList<JavaAnnotationElement> annotations = new ArrayList<>();

    public String getThrows() {
        return throwsException;
    }

    public void setThrows(String throwsException) {
        this.throwsException = throwsException;
    }

    public void setType(String type, String returnJavaDoc) {
        super.setType(type);
        this.returnJavaDoc = returnJavaDoc;
    }

    public void addGeneric(String generic) {
        generics.add(generic);
    }

    public JavaMethodElement withGeneric(String generic) {
        addGeneric(generic);
        return this;
    }

    public JavaMethodElement withType(String type, String returnJavaDoc) {
        super.setType(type);
        this.returnJavaDoc = returnJavaDoc;
        return this;
    }

    public JavaMethodElement withAccessLevel(AccessLevel accessLevel) {
        super.setAccessLevel(accessLevel);
        return this;
    }

    public String getSeeJavaDoc() {
        return seeJavaDoc;
    }

    public void setSeeJavaDoc(String seeJavaDoc) {
        this.seeJavaDoc = seeJavaDoc;
    }

    public JavaMethodElement withSeeJavaDoc(String seeJavaDoc) {
        this.seeJavaDoc = seeJavaDoc;
        return this;
    }

    public String getDeprecatedJavaDoc() {
        return deprecatedJavaDoc;
    }

    public void setDeprecatedJavaDoc(String deprecatedJavaDoc) {
        this.deprecatedJavaDoc = deprecatedJavaDoc;
    }

    public JavaMethodElement withDeprecatedJavaDoc(String deprecatedJavaDoc) {
        this.deprecatedJavaDoc = deprecatedJavaDoc;
        return this;
    }

/*
    public void addParameter(String declaration) {
        String[] tokens = StringUtils.splitAndTrim(declaration, " ");
        if ( tokens.length != 2) {
            throw new CoreException("Parameter '?' not in format \"Type name\"", declaration);
        }

        Parameter p = new Parameter();
        p.type = tokens[0];
        p.name = tokens[1];
        parameters.add(p);
    }
*/

    public void addParameter(Class clazz, String name) {
        ((AbstractJavaClassComponent) getComponent()).addImport(clazz);
        addParameter(clazz.getSimpleName(), name);
    }


    public void addParameter(String type, String name) {
        Parameter p = new Parameter();
        if ( type == null ) { throw new CoreException("Parameter type cannot be null"); }
        if ( name == null ) { throw new CoreException("Parameter name cannot be null"); }
        p.type = type;
        p.name = name;
        parameters.add(p);
    }

    public void addParameter(String type, String name, String javaDoc) {
        Parameter p = new Parameter();
        if ( type == null ) {
            throw new CoreException("Parameter type cannot be null");
        }
        if ( name == null ) { throw new CoreException("Parameter name cannot be null"); }
        p.type    = type;
        p.name    = name;
        p.javaDoc = javaDoc;
        if ( javaDoc != null ) {
            parameterJavaDocExists = true;
        }
        parameters.add(p);
    }


    public ArrayList<JavaAnnotationElement> getAnnotations() {
        return annotations;
    }


    @SuppressWarnings("Duplicates")
    public JavaAnnotationElement addAnnotation(Class annotationClass) {
        JavaAnnotationElement annotationElement = addElement(JavaAnnotationElement.class);
        annotationElement.setAnnotationClass(annotationClass);

        if ( getComponent() instanceof AbstractJavaClassComponent) {
            ((AbstractJavaClassComponent) getComponent()).addImport(annotationClass);
        }

        annotationElement.setLpad(getLpad());
        annotations.add(annotationElement);
        return annotationElement;
    }

    private String getGenericsString() {
        if ( generics.size() == 0) {
            return "";
        }
        return  " <" + StringUtils.concatenateStrings(", ", generics) + ">";
    }

    @Override
    public void write() {
        String str = getAccessLevel()
                + (isStatic() ? " static" : "")
                + getGenericsString()
                + " " +  ( getType() != null ? getType() : "void")
                + " " + getName() + "(";
        for ( int i = 0; i < parameters.size(); i++) {
            str += (i > 0 ? ", " : "") + ( parameters.get(i).type + " " + parameters.get(i).name);
        }

        pln("");
        if ( javaDoc.size() > 0 || parameterJavaDocExists || returnJavaDoc != null || seeJavaDoc != null || deprecatedJavaDoc != null)  {
            pln("/**");
            for ( String doc : javaDoc ) {
                pln("* " + doc);
            }
            if ( type != null || throwsException != null || parameterJavaDocExists || seeJavaDoc != null || deprecatedJavaDoc != null) {
                pln("* ");
            }

            if ( parameters.size() > 0 ) {
                int parameterNameMaxLength = 0;
                for ( Parameter p : parameters ) {
                    if ( p.name.length() > parameterNameMaxLength ) {
                        parameterNameMaxLength = p.name.length();
                    }
                }

                String emptyPrefix = "*             " + StringUtils.space(parameterNameMaxLength + 1);

                for ( Parameter p : parameters ) {
                    String[] tokens = StringUtils.splitAndTrim((p.javaDoc != null ? p.javaDoc : ""), "\\n");
                    String   prefix = "* @param      "
                            + p.name
                            + StringUtils.space(parameterNameMaxLength - p.name.length() + 1);
                    for ( String doc : tokens ) {
                        pln(prefix + doc);
                        prefix = emptyPrefix;
                    }
                }

            }

            if ( type != null ) {
                pln("* @return     " + (returnJavaDoc != null ? returnJavaDoc : ""));
            }
/*
            if ( throwsException != null ) {
                pln("* @throws     " + throwsException);
            }
*/
            if ( seeJavaDoc != null ) {
                pln("* @see        " + seeJavaDoc);
            }
            if ( deprecatedJavaDoc != null ) {
                pln("* @deprecated " + deprecatedJavaDoc);
            }
            pln("*/");
        }

        for (JavaAnnotationElement annotation : getAnnotations() ) {
            annotation.write();
        }
        pln(str + ") " + ( throwsException != null ? "throws " + throwsException + " " : "" ) +  "{");

        for (AbstractElement element : getElements() ) {
            if ( ! (element instanceof JavaAnnotationElement)) {
                element.write();
            }
        }

        pln("}");
    }
}
