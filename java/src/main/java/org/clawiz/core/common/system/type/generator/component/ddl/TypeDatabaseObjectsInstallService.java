/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator.component.ddl;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.system.database.metadata.*;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.type.key.TypeKey;
import org.clawiz.core.common.metadata.data.type.key.TypeKeyField;
import org.clawiz.core.common.metadata.data.type.key.TypeKeyList;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.core.common.utils.StringUtils;

public class TypeDatabaseObjectsInstallService extends Service {
    
    Type type;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
    
    public static String getTypeAuditTableName(Type type) {
        String auditTableName;
        auditTableName = "a_"+StringUtils.substring(type.getTableName(), 0, 27);
        while ( auditTableName.charAt(auditTableName.length()-1)== '_') {
            auditTableName  = auditTableName .substring(0, auditTableName.length() - 1);

        }
        return auditTableName;
    }

    public static String getOldFieldColumnName(TypeField field) {
        return StringUtils.substring("o_" + field.getColumnName(), 0, 30);
    }

    public static String getNewFieldColumnName(TypeField field) {
        return StringUtils.substring("n_" + field.getColumnName(), 0, 30);
    }

    public static String getOldFieldUpperColumnName(TypeField field) {
        return StringUtils.substring("o_" + field.getUpperColumnName(), 0, 30);
    }

    public static String getNewFieldUpperColumnName(TypeField field) {
        return StringUtils.substring("n_"  + field.getUpperColumnName(), 0, 30);
    }

    public void run() {

        if ( type.getTableName().length() > 27 ) {
            throwException("Type ? database table name ? has more than 27 chars length", type.getFullName(), type.getTableName());
        }

        DatabaseTable table  = new DatabaseTable(type.getTableName(),        type.getDescription());
        DatabaseTable atable = new DatabaseTable(getTypeAuditTableName(type),   type.getDescription() + " audit");


        DatabaseTableColumnList columns  = table.getColumns();
        DatabaseTableColumnList acolumns = atable.getColumns();

        acolumns.add(new DatabaseTableColumn("scn",        DatabaseTableColumn.Type.NUMBER , 17, 0, false, "System change number"));
        acolumns.add(new DatabaseTableColumn("action_type", DatabaseTableColumn.Type.VARCHAR, 15, 0, false, "Action type"));

        columns.add( new DatabaseTableColumn("id", DatabaseTableColumn.Type.NUMBER, 15, 0, false, "Object number"));
        acolumns.add(new DatabaseTableColumn("id", DatabaseTableColumn.Type.NUMBER, 15, 0, false, "Object number"));

        for (TypeField field : type.getFields()) {

            if ( field.getValueType() instanceof ValueTypeList ) {
                continue;
            }

            int precision = field.getValueType().getDatabaseTableColumnPrecision() != null ? field.getValueType().getDatabaseTableColumnPrecision().intValue() : 0;
            int scale     = field.getValueType().getDatabaseTableColumnScale()     != null ? field.getValueType().getDatabaseTableColumnScale().intValue() : 0;

            DatabaseTableColumn.Type columnType = field.getValueType().getDatabaseTableColumnType();

            if ( field.getColumnName() == null ) {
                throwException("Column name for field '?' not defined", new Object[]{field.getName()});
            }

            columns.add(new DatabaseTableColumn(field.getColumnName(), columnType,
                    precision,  scale, false, field.getDescription()));

            acolumns.add(new DatabaseTableColumn(getOldFieldColumnName(field), columnType,
                    precision,  scale, false, null));
            acolumns.add(new DatabaseTableColumn(getNewFieldColumnName(field), columnType,
                    precision,  scale, false, null));

            if ( field.isUpperFieldExists() ) {
                columns.add( new DatabaseTableColumn( field.getUpperColumnName(), columnType,
                        precision, scale, false, field.getDescription() ) );
                acolumns.add( new DatabaseTableColumn( getOldFieldUpperColumnName(field), columnType,
                        precision, scale, false, field.getDescription() ) );
                acolumns.add( new DatabaseTableColumn( getNewFieldUpperColumnName(field), columnType,
                        precision, scale, false, field.getDescription() ) );
            }
        }

        DatabaseTableIndexList indexes =  table.getIndexes();
        DatabaseTableIndexList aindexes = atable.getIndexes();

        DatabaseTableIndex index;

        index = new DatabaseTableIndex(null, true);
        indexes.add(index);
        index.getColumns().add(new DatabaseTableIndexColumn("id"));

        // Type always have at least one key
        DatabaseTableIndex aindex = new DatabaseTableIndex(StringUtils.substring(getTypeAuditTableName(type), 0, 27) + "_pk", true);
        aindexes.add(aindex);
        aindex.getColumns().add(new DatabaseTableIndexColumn("scn"));
        aindex.getColumns().add(new DatabaseTableIndexColumn("id"));


        TypeKeyList keys = type.getKeys();
        for( int i = 0; i < keys.size(); i++ ) {
            TypeKey key = keys.get( i );

            index   = new DatabaseTableIndex(null, key.isUnique());
            indexes.add(index);

            for (int j = 0; j < key.getFields().size(); j++) {
                TypeKeyField keyField = key.getFields().get( j );

                if( keyField.getField().isUpperFieldExists() ) {

                    index.addColumn( new DatabaseTableIndexColumn( keyField.getField().getUpperColumnName() ) );

                } else {

                    index.addColumn( new DatabaseTableIndexColumn( keyField.getField().getColumnName() ) );

                }
            }
        }

        getSession().getDatabaseAdapter().implementTable(table);
        getSession().getDatabaseAdapter().implementTable(atable);

        for ( TypeField field : getType().getFields() ) {
            if ( field.isUpperFieldExists() ) {
                getSession().getDatabaseAdapter().updateTableUpperColumn(getType().getTableName(), field.getColumnName(), field.getUpperColumnName());
                commit();
            }
        }


    }
}
