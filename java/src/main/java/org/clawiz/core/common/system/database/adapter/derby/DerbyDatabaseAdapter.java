/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.database.adapter.derby;

import org.clawiz.core.common.system.database.metadata.DatabaseSequence;
import org.clawiz.core.common.system.database.metadata.DatabaseTable;
import org.clawiz.core.common.system.database.metadata.DatabaseTableColumn;
import org.clawiz.core.common.system.database.adapter.DatabaseAdapter;

import java.math.BigDecimal;

/**
 * (c) Clawiz
 * Date: 10.03.2010
 * Time: 3:07?9
 */
public class DerbyDatabaseAdapter extends DatabaseAdapter {



    @Override
    protected String getColumnDateTypeName() {
        return "TIMESTAMP";
    }

    @Override
    protected void prepareDatabaseColumnType(DatabaseTableColumn column, String typeName, int columnSize, int decimalDigits) {
        switch (typeName) {
            case "NUMERIC":
            case "FLOAT":  column.setType(DatabaseTableColumn.Type.NUMBER); return;
        }
        super.prepareDatabaseColumnType(column, typeName, columnSize, decimalDigits);
    }

    @Override
    protected String getTableColumnTypeDDL(DatabaseTableColumn column) {
        if ( column.getType() == DatabaseTableColumn.Type.NUMBER ) {
            if ( column.getPrecision() == 0 )
                return "FLOAT";
            if ( column.getScale() == 0 )
                return "NUMERIC(" + column.getPrecision() + ")";
            else
                return "NUMERIC(" + column.getPrecision() + "," + column.getScale() + ")";

        }
        return super.getTableColumnTypeDDL(column);
    }

    @Override
    protected String getStringConcatenationOperator() {
        return "||";
    }

    @Override
    public void createSequence(DatabaseSequence sequence) {
        executeUpdate("create table " + sequence.getName() + " ( ROW_ID BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH " + sequence.getStartWith() + "), VL CHAR(1)) ", new Object[]{});
    }

    public BigDecimal getSequenceNextValue(String sequenceName) {

        executeUpdate("insert into " + sequenceName + " (vl) values ('X')");
        return executeQueryBigDecimal("select IDENTITY_VAL_LOCAL() from dual");
    }

    @Override
    public boolean isSequenceExists(DatabaseSequence sequence) {
        return isTableExists(sequence.getName());
    }

    public String getAlterTableModifyColumnStatement(DatabaseTable table, DatabaseTableColumn column) {
        if ( column.getType() == DatabaseTableColumn.Type.VARCHAR ) {
            return "alter table " + table.getName() + " alter " + column.getName() + " set data type " + getTableColumnTypeDDL(column);
        } else {
            return null;
        }
    }
}
