package org.clawiz.core.common.storage.metadatanode;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;
import org.clawiz.core.common.system.service.NotInitializeService;

public class MetadataNodeServicePrototype extends AbstractTypeService<MetadataNodeObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.core.common.storage", "MetadataNode");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.MetadataNode"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      metadataNode Checked object
    */
    public void check(MetadataNodeObject metadataNode) {
        
        if ( metadataNode == null ) {
            throwException("Cannot check null ?", new Object[]{"MetadataNodeObject"});
        }
        
        metadataNode.fillDefaults();
        
        
        if ( nodeClassPackageNameToId(metadataNode.getNodeClassId(), metadataNode.getPackageName(), metadataNode.getName(), metadataNode.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "MetadataNode", metadataNode.toNodeClassPackageName() });
        }
        
    }
    
    /**
    * Create new MetadataNodeObject instance and fill default values
    * 
    * @return     Created object
    */
    public MetadataNodeObject create() {
        
        MetadataNodeObject metadataNode = new MetadataNodeObject();
        metadataNode.setService((MetadataNodeService) this);
        
        metadataNode.fillDefaults();
        
        return metadataNode;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public MetadataNodeObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select package_name, name, node_class_id, storage_id, node_object_id from cw_core_mdata_nodes where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"MetadataNode", id});
        }
        
        MetadataNodeObject result = new MetadataNodeObject();
        
        result.setService((MetadataNodeService) this);
        result.setId(id);
        result.setPackageName(statement.getString(1));
        result.setName(statement.getString(2));
        result.setNodeClassId(statement.getBigDecimal(3));
        result.setStorageId(statement.getBigDecimal(4));
        result.setNodeObjectId(statement.getBigDecimal(5));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of MetadataNodeObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public MetadataNodeList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of MetadataNodeObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public MetadataNodeList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        MetadataNodeList result = new MetadataNodeList();
        
        
        Statement statement = executeQuery("select id, package_name, name, node_class_id, storage_id, node_object_id from cw_core_mdata_nodes"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            MetadataNodeObject object = new MetadataNodeObject();
        
            object.setService((MetadataNodeService) this);
            object.setId(statement.getBigDecimal(1));
            object.setPackageName(statement.getString(2));
            object.setName(statement.getString(3));
            object.setNodeClassId(statement.getBigDecimal(4));
            object.setStorageId(statement.getBigDecimal(5));
            object.setNodeObjectId(statement.getBigDecimal(6));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'Name' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select name from cw_core_mdata_nodes where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        MetadataNodeObject object = new MetadataNodeObject();
        object.setService((MetadataNodeService)this);
        object.setId(id);
        object.setName(statement.getString(1));
        
        statement.close();
        
        return object.toName();
    }
    
    /**
    * Find id of record by key 'NodeClassPackageName' fields
    * 
    * @param      nodeClassId NodeClass
    * @param      packageName PackageName
    * @param      name        Name
    * @return     Id of found record or null
    */
    public BigDecimal nodeClassPackageNameToId(java.math.BigDecimal nodeClassId, java.lang.String packageName, java.lang.String name) {
        return nodeClassPackageNameToId(nodeClassId, packageName, name, null);
    }
    
    /**
    * Find id of record by key 'NodeClassPackageName' fields with id not equal skipId
    * 
    * @param      nodeClassId NodeClass
    * @param      packageName PackageName
    * @param      name        Name
    * @param      skipId      Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal nodeClassPackageNameToId(java.math.BigDecimal nodeClassId, java.lang.String packageName, java.lang.String name, BigDecimal skipId) {
        
        if ( nodeClassId == null || packageName == null || name == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_mdata_nodes where node_class_id = ? and upper_package_name = ? and upper_name = ?", nodeClassId, packageName.toUpperCase(), name.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_mdata_nodes where node_class_id = ? and upper_package_name = ? and upper_name = ? and id != ?", nodeClassId, packageName.toUpperCase(), name.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'NodeClassPackageName' fields or create new record with values set to given parameters
    * 
    * @param      nodeClassId         NodeClass
    * @param      packageName         PackageName
    * @param      name                Name
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal nodeClassPackageNameToId(java.math.BigDecimal nodeClassId, java.lang.String packageName, java.lang.String name, boolean createNewIfNotFound) {
        
        BigDecimal id = nodeClassPackageNameToId(nodeClassId, packageName, name, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        MetadataNodeObject object = create();
        object.setNodeClassId(nodeClassId);
        object.setPackageName(packageName);
        object.setName(name);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'NodeClassPackageName' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToNodeClassPackageName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select node_class_id, package_name, name from cw_core_mdata_nodes where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        MetadataNodeObject object = new MetadataNodeObject();
        object.setService((MetadataNodeService)this);
        object.setId(id);
        object.setNodeClassId(statement.getBigDecimal(1));
        object.setPackageName(statement.getString(2));
        object.setName(statement.getString(3));
        
        statement.close();
        
        return object.toNodeClassPackageName();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'Storage' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToStorage(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select storage_id from cw_core_mdata_nodes where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        MetadataNodeObject object = new MetadataNodeObject();
        object.setService((MetadataNodeService)this);
        object.setId(id);
        object.setStorageId(statement.getBigDecimal(1));
        
        statement.close();
        
        return object.toStorage();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'Object' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToObject(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select node_object_id from cw_core_mdata_nodes where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        MetadataNodeObject object = new MetadataNodeObject();
        object.setService((MetadataNodeService)this);
        object.setId(id);
        object.setNodeObjectId(statement.getBigDecimal(1));
        
        statement.close();
        
        return object.toObject();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToNodeClassPackageName(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      metadataNodeObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(MetadataNodeObject metadataNodeObject) {
        return metadataNodeObject.toNodeClassPackageName();
    }
    
    protected void saveAudit(TransactionAction transactionAction, MetadataNodeObject oldMetadataNodeObject, MetadataNodeObject newMetadataNodeObject) {
        
        MetadataNodeObject o = oldMetadataNodeObject != null ? oldMetadataNodeObject : new MetadataNodeObject();
        MetadataNodeObject n = newMetadataNodeObject != null ? newMetadataNodeObject : new MetadataNodeObject();
        
        
        executeUpdate("insert into a_cw_core_mdata_nodes (scn, action_type, id , o_package_name, o_upper_package_name, o_name, o_upper_name, o_node_class_id, o_storage_id, o_node_object_id, n_package_name, n_upper_package_name, n_name, n_upper_name, n_node_class_id, n_storage_id, n_node_object_id) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getPackageName(), o.getPackageName() != null ? o.getPackageName().toUpperCase() : null, o.getName(), o.getName() != null ? o.getName().toUpperCase() : null, o.getNodeClassId(), o.getStorageId(), o.getNodeObjectId(), n.getPackageName(), n.getPackageName() != null ? n.getPackageName().toUpperCase() : null, n.getName(), n.getName() != null ? n.getName().toUpperCase() : null, n.getNodeClassId(), n.getStorageId(), n.getNodeObjectId());
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      metadataNode Saved object
    */
    public void save(MetadataNodeObject metadataNode) {
        
        if ( metadataNode == null ) {
            throwException("Cannot save NULL ?", new Object[]{"MetadataNodeObject"});
        }
        
        TransactionAction transactionAction;
        MetadataNodeObject oldMetadataNode;
        if ( metadataNode.getService() == null ) {
            metadataNode.setService((MetadataNodeService)this);
        }
        
        check(metadataNode);
        
        if ( metadataNode.getId() == null ) {
        
            metadataNode.setId(getObjectService().createObject(getTypeId()));
        
            oldMetadataNode = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, metadataNode.getId());
        
            executeUpdate("insert into cw_core_mdata_nodes" 
                          + "( id, package_name, upper_package_name, name, upper_name, node_class_id, storage_id, node_object_id ) "
                          + "values"
                          + "(?, ?, ?, ?, ?, ?, ?, ?)",
                          metadataNode.getId(), metadataNode.getPackageName(), ( metadataNode.getPackageName() != null ? metadataNode.getPackageName().toUpperCase() : null ), metadataNode.getName(), ( metadataNode.getName() != null ? metadataNode.getName().toUpperCase() : null ), metadataNode.getNodeClassId(), metadataNode.getStorageId(), metadataNode.getNodeObjectId() );
        
            if ( metadataNode.getNodeClassId() != null ) { getObjectService().setLink(metadataNode.getNodeClassId(), metadataNode.getId()); }
            if ( metadataNode.getStorageId() != null ) { getObjectService().setLink(metadataNode.getStorageId(), metadataNode.getId()); }
            if ( metadataNode.getNodeObjectId() != null ) { getObjectService().setLink(metadataNode.getNodeObjectId(), metadataNode.getId()); }
        
        } else {
        
            oldMetadataNode = load(metadataNode.getId());
            if ( oldMetadataNode.equals(metadataNode) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, metadataNode.getId());
        
            executeUpdate("update cw_core_mdata_nodes set "
                          + "package_name = ?, upper_package_name = ?, name = ?, upper_name = ?, node_class_id = ?, storage_id = ?, node_object_id = ? "
                          + "where id = ?",
                          metadataNode.getPackageName(), ( metadataNode.getPackageName() != null ? metadataNode.getPackageName().toUpperCase() : null ), metadataNode.getName(), ( metadataNode.getName() != null ? metadataNode.getName().toUpperCase() : null ), metadataNode.getNodeClassId(), metadataNode.getStorageId(), metadataNode.getNodeObjectId(), metadataNode.getId() );
        
            getObjectService().changeLinkParent(oldMetadataNode.getNodeClassId(), metadataNode.getNodeClassId(), metadataNode.getId());
            getObjectService().changeLinkParent(oldMetadataNode.getStorageId(), metadataNode.getStorageId(), metadataNode.getId());
            getObjectService().changeLinkParent(oldMetadataNode.getNodeObjectId(), metadataNode.getNodeObjectId(), metadataNode.getId());
        
        }
        
        saveAudit(transactionAction, oldMetadataNode, metadataNode);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        MetadataNodeObject oldMetadataNodeObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldMetadataNodeObject, null);
        
        getObjectService().deleteObject(id);
        
        if (oldMetadataNodeObject.getNodeClassId() != null) { getObjectService().deleteLink(oldMetadataNodeObject.getNodeClassId(), id); }
        if (oldMetadataNodeObject.getStorageId() != null) { getObjectService().deleteLink(oldMetadataNodeObject.getStorageId(), id); }
        if (oldMetadataNodeObject.getNodeObjectId() != null) { getObjectService().deleteLink(oldMetadataNodeObject.getNodeObjectId(), id); }
        
        executeUpdate("delete from cw_core_mdata_nodes where id = ?", id);
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.metadatastorage.MetadataStorageService metadataStorageService;
    
    public org.clawiz.core.common.storage.metadatastorage.MetadataStorageService getMetadataStorageService() {
        if ( metadataStorageService == null ) {
            metadataStorageService = getService(org.clawiz.core.common.storage.metadatastorage.MetadataStorageService.class);
        }
        return metadataStorageService;
    }
    
    public org.clawiz.core.common.storage.metadatastorage.MetadataStorageObject createMetadataStorage() {
        return getMetadataStorageService().create();
    }
    
    public org.clawiz.core.common.storage.metadatastorage.MetadataStorageObject loadMetadataStorage(BigDecimal id) {
        return getMetadataStorageService().load(id);
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.metadatanodeclass.MetadataNodeClassService metadataNodeClassService;
    
    public org.clawiz.core.common.storage.metadatanodeclass.MetadataNodeClassService getMetadataNodeClassService() {
        if ( metadataNodeClassService == null ) {
            metadataNodeClassService = getService(org.clawiz.core.common.storage.metadatanodeclass.MetadataNodeClassService.class);
        }
        return metadataNodeClassService;
    }
    
    public org.clawiz.core.common.storage.metadatanodeclass.MetadataNodeClassObject createMetadataNodeClass() {
        return getMetadataNodeClassService().create();
    }
    
    public org.clawiz.core.common.storage.metadatanodeclass.MetadataNodeClassObject loadMetadataNodeClass(BigDecimal id) {
        return getMetadataNodeClassService().load(id);
    }
}
