package org.clawiz.core.common.metadata.generator.node.dataclass.component;

import org.clawiz.core.common.metadata.generator.node.dataclass.MetadataNodeJavaDataClassGenerator;
import org.clawiz.core.common.metadata.generator.node.dataclass.MetadataNodeJavaDataClassGeneratorContext;
import org.clawiz.core.common.metadata.data.language.node.element.Structure;
import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.core.common.system.generator.java.component.AbstractJavaClassComponent;

public class AbstractMetadataNodeDataClassComponent extends AbstractJavaClassComponent {

    private MetadataNodeJavaDataClassGenerator generator;
    private MetadataNodeJavaDataClassGeneratorContext context;
    private Structure structure;

    @Override
    public MetadataNodeJavaDataClassGenerator getGenerator() {
        return generator;
    }

    @Override
    public void setGenerator(AbstractGenerator generator) {
        super.setGenerator(generator);
        this.generator = (MetadataNodeJavaDataClassGenerator) generator;
        setContext(getGenerator().getContext());
    }

    public MetadataNodeJavaDataClassGeneratorContext getContext() {
        return context;
    }

    public void setContext(MetadataNodeJavaDataClassGeneratorContext context) {
        this.context = context;
        setStructure(getContext().getLanguageNode());
    }

    public Structure getStructure() {
        return structure;
    }

    public void setStructure(Structure structure) {
        this.structure = structure;
        setName(getStructure().getJavaClassName());
        setPackageName(structure.getPackageName());
    }
}
