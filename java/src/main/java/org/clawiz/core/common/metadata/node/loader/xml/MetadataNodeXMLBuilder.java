/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.metadata.node.loader.xml;

import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeList;
import org.clawiz.core.common.metadata.node.loader.MetadataLoader;
import org.clawiz.core.common.metadata.node.loader.data.MetadataLoaderNodeClassField;
import org.clawiz.core.common.metadata.node.loader.data.MetadataLoaderNodeClass;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.xml.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.HashMap;
import java.util.List;

import static org.clawiz.core.common.metadata.node.loader.MetadataLoader.*;

public class MetadataNodeXMLBuilder extends Service {

    MetadataLoader metadataLoader;
    XMLUtils       xmlUtils;

    private class BuildContext {
        Document       document;
        Element exchangeElement;
        Element        dictionaryElement;
        Element        nodesElement;

        HashMap<String, String> classAliases    = new HashMap<>();
        HashMap<String, String> aliasNamesCache = new HashMap<>();

        public Element createElement(Element parentElement, String tagName) {
            Element element = document.createElement(tagName);
            if ( parentElement != null ) {
                parentElement.appendChild(element);
            }
            return element;
        }
    }


    protected String getClassAlias(BuildContext context, String className) {

        String alias = context.classAliases.get(className);

        if ( alias == null ) {

            int    index = 2;
            String name  = StringUtils.splitByLastTokenOccurrence(className, "\\.")[1];
            alias = name;
            if ( context.aliasNamesCache.containsKey(alias) ) {
                alias = name + (index++);
            }

            context.classAliases.put(className, alias);

            Element aliasElement = context.createElement(context.dictionaryElement, CLASS_ALIAS_ELEMENT_NAME);
            aliasElement.setAttribute("alias", alias);
            aliasElement.setAttribute("class", className);
        }

        return alias;
    }


    protected void appendAttributeField(BuildContext context, MetadataNode node, Element element, MetadataLoaderNodeClassField field) {

        if ( field.isArray() ) {
            List values = (List) field.getObject(node);
            if ( values.size() == 0 ) {
                return;
            }
            Element list = context.createElement(element, field.getName());
            for (Object object : values ) {
                appendNode(context, list, (MetadataNode) object);
            }
        } else {

            String value = field.getAsString(node);
            if ( value != null ) {
                Element child = context.createElement(element, field.getName());
                child.setTextContent(value);
            } else if ( field.isRequired() ) {
                throwException("Required metadata node ? (?) field ? value is null", node.getFullName(), node.getClass().getName(), field.getName());
            }

        }

    }

    protected void appendElementField(BuildContext context, MetadataNode node, Element element, MetadataLoaderNodeClassField field) {


        if ( field.isArray() ) {

            Object list = field.getObject(node);

            if ( MetadataNodeList.class.isAssignableFrom(list.getClass()) ) {
                if (((MetadataNodeList) list).size() == 0 ) {
                    return;
                }
                Element fieldElement = context.createElement(element, field.getName());

                for (Object object : (MetadataNodeList) field.getObject(node) ) {
                    appendNode(context, fieldElement, (MetadataNode) object);
                }

            } else if (List.class.isAssignableFrom(list.getClass())) {
                if (((List) list).size() == 0 ) {
                    return;
                }

                Element fieldElement = context.createElement(element, field.getName());

                for (Object object : (List) field.getObject(node) ) {
                    appendNode(context, fieldElement, (MetadataNode) object);
                }

            } else {
                throwException("Wrong attribute ? list class ?", field.getName(), list.getClass().getName());
            }
        } else {
            MetadataNode value = (MetadataNode) field.getObject(node);
            if ( value == null ) {
                return;
            }

            Element fieldElement = context.createElement(element, field.getName());

            appendNode(context, fieldElement, value);

        }

    }

    protected void addReferenceToElement(BuildContext context, Element element, MetadataNode referenceNode) {
        if ( referenceNode == null ) {
            return;
        }


        context.createElement(context.createElement(element, "node"), "path")
                .setTextContent(referenceNode.getReferencePath());
    }

    protected void appendReferenceField(BuildContext context, MetadataNode node, Element element, MetadataLoaderNodeClassField field) {

        if ( field.isArray() ) {
            MetadataNodeList<MetadataNode> list = field.getAsNodeList(node);
            if ( list != null && list.size() > 0 ) {
                Element listElement = context.createElement(element, field.getName());
                for ( MetadataNode referencedNode : list) {
                    addReferenceToElement(context, listElement, referencedNode);
                }
            }
        } else {
            MetadataNode referencedNode = field.getAsNode(node);
            if ( referencedNode != null ) {
                addReferenceToElement(context, context.createElement(element, field.getName()), referencedNode);
            }
        }

    }


    protected void appendNode(BuildContext context, Element parentElement, MetadataNode node) {

        if ( node == null ) {
            return;
        }

        Element element = context.createElement(parentElement, getClassAlias(context, node.getClass().getName()));

        MetadataLoaderNodeClass structure = metadataLoader.getNodeClassStructure(node.getClass());

        for (MetadataLoaderNodeClassField field : structure.getFields() ) {

            if ( field.isElement() ) {

                appendElementField(context, node, element, field);

            } else if ( field.isReference() ) {

                appendReferenceField(context, node, element, field);

            } else {

                appendAttributeField(context, node, element, field);

            }


        }


    }

    public String build(MetadataNodeList nodes) {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throwException("Exception on create XML document builder : ?", e.getMessage(), e);
        }
        Document document        = builder.newDocument();

        BuildContext context = new BuildContext();
        context.document = document;

        context.exchangeElement = document.createElement(CLAWIZ_EXCHANGE_ELEMENT_NAME);
        document.appendChild(context.exchangeElement);

        context.dictionaryElement = context.createElement(context.exchangeElement, DICTIONARY_ELEMENT_NAME);

        context.nodesElement = context.createElement(context.exchangeElement, NODES_ELEMENT_NAME);

        for (Object object : nodes ) {
            appendNode(context, context.nodesElement, (MetadataNode) object);
        }

        return xmlUtils.documentToString(document);
    }

}
