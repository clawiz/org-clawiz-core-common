/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.security;

import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.system.session.Permission;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by abdrashitovta on 17.11.2014.
 */
public class SecurityBase extends Service {

    public static String ADMIN_USER_NAME             = "admin";
    public static String ADMINISTRATOR_ROLE_NAME     = "Administrator";

    public static String READ_PERMISSION_TYPE_NAME   = "read";
    public static String WRITE_PERMISSION_TYPE_NAME  = "write";

    private static ConcurrentHashMap<String, Permission> permissionsCache = new ConcurrentHashMap<>();

    public String createHash(String password) {
        String hash = null;
        try {
            hash = password != null ? PasswordHash.createHash(password) : null;
        } catch (NoSuchAlgorithmException e) {
            throwException("NoSuchAlgorithmException on create password hash", e);
        } catch (InvalidKeySpecException e) {
            throwException("InvalidKeySpecException on create password hash", e);
        }
        return hash;
    }

    public boolean validatePassword(String password, String goodHash) {
        try {
            if ( goodHash == null ) {
                return password == null;
            }
            return PasswordHash.validatePassword(password, goodHash);
        } catch (NoSuchAlgorithmException e) {
            throwException("NoSuchAlgorithmException on validate password hash", e);
        } catch (InvalidKeySpecException e) {
            throwException("InvalidKeySpecException on validate password hash", e);
        }
        return false;
    }

}
