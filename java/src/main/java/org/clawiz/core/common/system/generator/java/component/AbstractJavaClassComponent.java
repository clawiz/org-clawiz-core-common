/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.java.component;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.generator.abstractgenerator.component.element.AbstractElement;
import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.core.common.system.generator.file.component.AbstractTextFileComponent;
import org.clawiz.core.common.system.generator.file.component.element.StringTextElement;
import org.clawiz.core.common.system.generator.java.AbstractJavaClassGenerator;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaVariableElement;
import org.clawiz.core.common.utils.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: abdrashitovta
 * Date: 26.05.13
 * Time: 15?7
 * To change this template use File | Settings | File Templates.
 */
public class AbstractJavaClassComponent extends AbstractTextFileComponent {

    public enum ClassType {

        CLASS, INTERFACE, ENUM
    }


    String packageName;
    ClassType javaClassType = ClassType.CLASS;
    OverwriteMode overwriteMode = OverwriteMode.OVERWRITE;

    ArrayList<String> imports = new ArrayList<>();
    HashMap<String, String> importsCache = new HashMap<>();
    String extendsStr;
    String implementsStr;

    String typeParameters;

    ArrayList<String> javaDoc = new ArrayList<>();

    boolean _abstract = false;


    @Override
    public void setName(String name) {
        super.setName(name);
        setFileName(name + ".java");
    }

    public String getTypeParameters() {
        return typeParameters;
    }

    public void setTypeParameters(String typeParameters) {
        this.typeParameters = typeParameters;
    }

    public String getExtends() {
        return extendsStr;
    }

    public void setExtends(String extendsStr) {
        this.extendsStr = extendsStr;
    }

    public void setExtends(Class clazz) {
        setExtends(clazz.getName());
    }

    public boolean isAbstract() {
        return _abstract;
    }

    public void setAbstract(boolean _abstract) {
        this._abstract = _abstract;
    }

    public String getImplements() {
        return implementsStr;
    }

    public void setImplements(String implementsStr) {
        this.implementsStr = implementsStr;
    }

    public void addImport(String str) {
        if ( ! importsCache.containsKey(str) ) {
            imports.add(str);
            importsCache.put(str, str);
        }
    }

    public void addImport(Class clazz) {
        addImport(clazz.getName());
    }

    public boolean isImportExists(Class clazz) {
        return importsCache.containsKey(clazz.getName());
    }

    public void addImport(AbstractJavaClassComponent component) {
        addImport(component.getFullName());
    }

    public ClassType getJavaClassType() {
        return javaClassType;
    }

    public void setJavaClassType(ClassType javaClassType) {
        this.javaClassType = javaClassType;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getFullName() {
        return getPackageName() + "." + getName();
    }

        @Override
    public void setGenerator(AbstractGenerator generator) {
        super.setGenerator(generator);
        if ( generator instanceof AbstractJavaClassGenerator ) {
            setPackageName(((AbstractJavaClassGenerator) generator).getPackageName());
        }
    }

    @Override
    public <T extends AbstractElement> T addElement(String elementName, Class<T> clazz) {
        AbstractElement element = super.addElement(elementName, clazz);
        ((StringTextElement)element).setLpad("    ");
        return (T) element;
    }

    public ArrayList<String> getJavaDoc() {
        return javaDoc;
    }

    public AbstractJavaClassComponent addJavaDoc(String annotation) {
        javaDoc.add(annotation);
        return this;
    }

    @Override
    public boolean isDestinationExists() {
        File javaFile = new File( getDestinationPath() + File.separator + getFileName());
        File kotlinFile = new File( getDestinationPath() + File.separator + StringUtils.splitByLastTokenOccurrence(getFileName(), "\\.")[0] + ".kt");
        return javaFile.exists() || kotlinFile.exists();
    }

    @Override
    public void write() {

        if ( getDestinationPath() == null ) {
            logDebug("Destination path is null, skip write " + getFullName());
            return;
        }

        pln("package " + packageName + ";");
        pln("");
        for (String s : imports) {
            pln("import " + s + ";");
        }
        pln("");

        //noinspection Duplicates
        if ( javaDoc.size() > 0 ) {
            pln("/**");
            for( String doc : javaDoc ) {
                String[] tokens = StringUtils.splitAndTrim(doc, "\\n");
                for ( String s : tokens) {
                    pln("* " + s);
                }
            }
            pln("*/");
        }

        String str = "public " + (isAbstract() ? " abstract " : "") +  getJavaClassType().toString().toLowerCase() + " " + getName();
        if ( getTypeParameters() != null ) {
            str += "<" + getTypeParameters() + ">";
        }
        if ( extendsStr != null ) {
            str += " extends " + extendsStr;
        }
        if ( implementsStr != null ) {
            str += " implements " + implementsStr;
        }
        pln(str + " {");

        super.writeElements();

        pln("}");
    }

    public JavaMethodElement addMethod(String name) {
        return addMethod(null, name);
    }


    public JavaMethodElement addMethod(String type, String name) {
        JavaMethodElement method = addElement(name, JavaMethodElement.class);
        method.setType(type);
        return method;
    }

    public JavaVariableElement addVariable(String type, String name) {
        return addVariable(type, name, null);
    }

    HashMap<String, JavaVariableElement> variablesCache = new HashMap<>();
    public JavaVariableElement addVariable(String type, String name, String initStatement) {
        return addVariable(type, name, initStatement, false, false);
    }

    public JavaVariableElement addVariable(String type, String name, String initStatement, boolean addGetter, boolean addSetter) {
        return addVariable(type, name, initStatement, addGetter, addSetter,false);
    }

    public JavaVariableElement addVariable(String type, String name, String initStatement, boolean addGetter, boolean addSetter, boolean throwExistsException) {
        if ( variablesCache.containsKey(name) ) {
            if ( throwExistsException ) {
                throwException("Variable ? already exists in class ? ", name, this.getFullName());
            }
            JavaVariableElement var = variablesCache.get(name);
            if ( ! var.getType().equals(type) ) {
                throwException("Variable ? already exists in class ?, but its type ? not equal passed type ?", name, this.getFullName(), var.getType(), type);
            }
            return var;
        }

        JavaVariableElement var = addElement(name, JavaVariableElement.class);
        var.setType(type);
        var.setInitStatement(initStatement);
        variablesCache.put(name, var);

        if ( addGetter ) {
            var.addGetter();
        }

        if ( addSetter ) {
            var.addSetter();
        }

        return var;
    }

    public boolean isVariableExists(String name) {
        return variablesCache.containsKey(name);
    }


}
