/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.database.adapter.mysql;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.database.adapter.DatabaseAdapter;
import org.clawiz.core.common.system.database.metadata.*;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;

public class MysqlDatabaseAdapter extends DatabaseAdapter {


    String _databaseName;
    protected String getDatabaseName() {
        if ( _databaseName == null ) {
            try {
                _databaseName = getSession().getConnection().getJDBCConnection().getCatalog();
            } catch (SQLException e) {
                throwException("Exception in get MySQL catalog : ?", e.getMessage(), e);
            }
        }
        return _databaseName;
    }

    @Override
    protected String getColumnNumberTypeName() {
        return "NUMERIC";
    }

    @Override
    protected String getColumnDateTypeName() {
        return "TIMESTAMP";
    }

    @Override
    protected DatabaseTable loadTableDefinition(String tableName) throws SQLException {
        Statement statement = executeQuery(
                "SELECT table_name, table_comment FROM information_schema.tables WHERE table_schema = ? AND upper(table_name) = ?"
                , getDatabaseName(), tableName.toUpperCase());
        if ( ! statement.next() ) {
            statement.close();
            return null;
        }

        DatabaseTable table = new DatabaseTable(statement.getString(1), statement.getString(2));
        statement.close();
        return table;
    }

    @Override
    protected void loadTableColumns(DatabaseTable table) throws SQLException {

        Statement statement = executeQuery("SELECT COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, NUMERIC_PRECISION, NUMERIC_SCALE, IS_NULLABLE \n" +
                "FROM information_schema.COLUMNS \n" +
                "WHERE \n" +
                "    TABLE_SCHEMA = ? AND upper(TABLE_NAME) = ?", getDatabaseName(), table.getName().toUpperCase());
        while ( statement.next() ) {


            DatabaseTableColumn column = new DatabaseTableColumn();
            table.addColumn(column);
            column.setName(statement.getString(1));
            column.setType(metadataColumnTypeToDatabaseColumnType(column, statement.getString(2)));
            if ( column.getType() == DatabaseTableColumn.Type.NUMBER ) {
                column.setPrecision(statement.getResultSet().getInt(4));
                column.setScale(statement.getResultSet().getInt(5));
            } else {
                column.setPrecision(statement.getResultSet().getInt(3));
            }
            column.setRequired("N".equalsIgnoreCase(statement.getString(6)));

        }
        statement.close();

    }

    @Override
    protected void loadTableIndexes(DatabaseTable table) throws SQLException {

        DatabaseTableIndex index = null;

        Statement statement = executeQuery("SELECT index_name, non_unique, column_name FROM information_schema.statistics " +
                        " WHERE table_schema =  ? and upper(table_name) = ?  order by index_name, seq_in_index"
                , getDatabaseName(), table.getName().toUpperCase());
        while ( statement.next() ) {
            String indexName = statement.getString(1);
            if ( index == null || ! index.getName().equalsIgnoreCase(indexName) ) {
                index = new DatabaseTableIndex();
                index.setName(indexName);
                index.setUnique(statement.getResultSet().getInt(2) == 0);
                table.addIndex(index);
            }
            index.addColumn(new DatabaseTableIndexColumn(statement.getString(3)));
        }
        statement.close();
    }

    @Override
    protected String getCreateTableDDL(DatabaseTable table) {
        return super.getCreateTableDDL(table) + " ENGINE=InnoDB DEFAULT CHARSET=utf8"
                + (table.getComment() != null ? " COMMENT='" + table.getComment() + "'" : "");
    }

    @Override
    protected String getDropIndexDDL(DatabaseTableIndex index) {
        return "alter table " + index.getTable().getName().toUpperCase()+" drop index " + index.getName().toUpperCase();
    }

    @Override
    public boolean isSequenceExists(DatabaseSequence sequence) {
        return isTableExists(sequence.getName());
    }

    @Override
    public void createSequence(DatabaseSequence sequence) {
        executeUpdate("create table " + sequence.getName() + " ( ROW_ID BIGINT NOT NULL AUTO_INCREMENT, VL CHAR(1), PRIMARY KEY (ROW_ID)) AUTO_INCREMENT = " + sequence.getStartWith());
    }

    @Override
    public BigDecimal getSequenceNextValue(String sequenceName) {
        executeUpdate("insert into "+ sequenceName + " (vl) values ('X')");
        return executeQueryBigDecimal("SELECT LAST_INSERT_ID()");
    }

    @Override
    public String prepareSQL(String sql) {
        return sql.replace('"', '`');
    }

    @Override
    protected String getStringConcatenationOperator() {
        throw new CoreException("String concatenation operator not applicable for MySQL database. Use concatenateStringExpressions method");
    }

    @Override
    public String concatenateStringExpressions(ArrayList<String> strings) {
        StringBuilder sb = new StringBuilder();
        sb.append("concat(");
        String prefix = "";
        for (String str : strings ) {
            sb.append(prefix).append(str);
            prefix = ",";

        }
        sb.append(")");
        return sb.toString();
    }

    @Override
    public void initDatabaseConnection() {
        executeUpdate("set names utf8;");
    }


}
