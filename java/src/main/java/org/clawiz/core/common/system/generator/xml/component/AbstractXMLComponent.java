/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.xml.component;

import org.clawiz.core.common.system.generator.abstractgenerator.writer.AbstractWriter;
import org.clawiz.core.common.system.generator.abstractgenerator.component.element.AbstractElement;
import org.clawiz.core.common.system.generator.file.component.AbstractTextFileComponent;
import org.clawiz.core.common.system.generator.file.writer.TextFileWriter;
import org.clawiz.core.common.system.generator.xml.component.element.AbstractXMLElement;
import org.clawiz.core.common.system.generator.xml.component.element.EmptyXMLElement;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;

public class AbstractXMLComponent extends AbstractTextFileComponent {

    String xmlHeader = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";

    public String getXmlHeader() {
        return xmlHeader;
    }

    public void setXmlHeader(String xmlHeader) {
        this.xmlHeader = xmlHeader;
    }


    protected void mergetModeLists(ArrayList<AbstractElement> elements, NodeList nodes) {

        for(int i=0; i < nodes.getLength(); i++ ) {
            Node node = nodes.item(i);
            String nodeTagName = node.getNodeName();
            AbstractXMLElement foundElement = null;

            for(AbstractElement _element : elements  )  {
                if ( ! ( _element instanceof AbstractXMLElement )) {
                    continue;
                }
                AbstractXMLElement element = (AbstractXMLElement) _element;
                if ( element.getTagName().equals(nodeTagName)) {
                    if ( element.someAsNode(node)) {
                        foundElement = element;
                        break;
                    }
                }
            }

            if ( foundElement == null ) {
                foundElement = addElement(EmptyXMLElement.class);
                ((EmptyXMLElement)foundElement).setTagName(nodeTagName);
            }

            foundElement.mergeWithNode(node);

        }


    }

    protected void mergeWithFile(File file) {

        Document document = null;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse(file);
        } catch (Exception e) {
            throwException("Exception on parse existed file '?' : ?", new Object[]{file.getPath(), e.getMessage()}, e);
            e.printStackTrace();
        }

        document.getDocumentElement().normalize();

        mergetModeLists(getElements(), document.getChildNodes());
    }

    @Override
    protected <T extends AbstractWriter> Class<T> getWriterClass() {
        return (Class<T>) TextFileWriter.class;
    }

    @Override
    public void merge() {
        File file = new File( getDestinationPath() + File.separator + getFileName());
        mergeWithFile(file);
    }

    @Override
    public void write() {
        ((TextFileWriter) getWriter()).write(xmlHeader + "\n");
        writeElements();
    }
}
