package org.clawiz.core.common.metadata.data.type.key;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class TypeKeyPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeAttribute
    private org.clawiz.core.common.metadata.data.common.enumeration.BooleanDefaultFalseEnumeration unique;
    
    @ExchangeElement
    private org.clawiz.core.common.metadata.data.type.key.TypeKeyFieldList fields = new org.clawiz.core.common.metadata.data.type.key.TypeKeyFieldList();
    
    public TypeKey withName(String value) {
        setName(value);
        return (TypeKey) this;
    }
    
    public org.clawiz.core.common.metadata.data.common.enumeration.BooleanDefaultFalseEnumeration getUnique() {
        return this.unique;
    }
    
    public void setUnique(org.clawiz.core.common.metadata.data.common.enumeration.BooleanDefaultFalseEnumeration value) {
        this.unique = value;
    }
    
    public TypeKey withUnique(org.clawiz.core.common.metadata.data.common.enumeration.BooleanDefaultFalseEnumeration value) {
        setUnique(value);
        return (TypeKey) this;
    }
    
    public org.clawiz.core.common.metadata.data.type.key.TypeKeyFieldList getFields() {
        return this.fields;
    }
    
    public TypeKey withField(org.clawiz.core.common.metadata.data.type.key.TypeKeyField value) {
        getFields().add(value);
        return (TypeKey) this;
    }
    
    public <T extends org.clawiz.core.common.metadata.data.type.key.TypeKeyField> T createField(Class<T> nodeClass) {
        org.clawiz.core.common.metadata.data.type.key.TypeKeyField value = createChildNode(nodeClass, "fields");
        getFields().add(value);
        return (T) value;
    }
    
    public org.clawiz.core.common.metadata.data.type.key.TypeKeyField createField() {
        return createField(org.clawiz.core.common.metadata.data.type.key.TypeKeyField.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        for (MetadataNode node : getFields()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        for (MetadataNode node : getFields()) {
            references.add(node);
        }
        
    }
}
