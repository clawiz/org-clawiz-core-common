/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.object;

import com.google.common.base.Optional;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.clawiz.core.common.Core;
import org.clawiz.core.common.storage.type.TypeService;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.utils.RandomGUID;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.service.Service;


import org.clawiz.core.common.utils.StringUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

public class ObjectService extends Service {

    private static ConcurrentHashMap<String, BigDecimal>    typeIdCache             = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<BigDecimal, Class>     typeServiceClassesCache = new ConcurrentHashMap<>();

    private HashMap<BigDecimal, AbstractTypeService>        typeServicesCache       = new HashMap<>();

    public BigDecimal createObject(BigDecimal typeId) {

        if ( typeId == null) {
            throwException("TypeId of creating object not defined");
        }

        BigDecimal id = getSession().getDatabaseAdapter().getSequenceNextValue("cw_core_objects_id_s");

        String guid = new RandomGUID(false).toString();

        executeUpdate("insert into cw_core_objects (id, type_id, guid) values (?, ?, ?)", id, typeId, guid);
        executeUpdate("insert into cw_core_object_links (parent_id, child_id, links_count) values (?, ?, 1)", typeId, id);

        return id;
    }

    public BigDecimal getChildsCount(BigDecimal id) {
        return notNull(executeQueryBigDecimal("select sum(links_count) from cw_core_object_links where parent_id = ?", id), new BigDecimal(0));
    }

    public void deleteObject(BigDecimal id) {

        BigDecimal childsCount = getChildsCount(id);
        if ( childsCount.longValue() > 0 ) {
            Statement statement = executeQuery("select l.child_id as id,t.name as type from cw_core_object_links l, cw_core_objects o, cw_core_types t where l.parent_id = ? and l.child_id=o.id and o.type_id=t.id", id);
            String    childs    = "";
            int       i         = 1;
            while ( statement.next() && i < 10){
                BigDecimal childId =  statement.getBigDecimal(1);
                childs             += "  "+ (i++)+ "." + statement.getString(2)+"["+idToString(childId) + "("+childId+")";
            }
            statement.close();
            throwException("Object '@' cannot be deleted because have ? references : ?", id, childsCount, childs);
        }

        executeUpdate("delete from cw_core_objects where id = ?",                     id);
        executeUpdate("delete from cw_core_object_links where child_id = ?",          id);
    }

    public void setLink(BigDecimal parentId, BigDecimal childId) {
        if ( parentId == null || childId == null ) {
            throwException("Both linked objects must be not null : @, @", new Object[]{parentId, childId});
        }

        BigDecimal linksCount = executeQueryBigDecimal("select links_count from cw_core_object_links where parent_id = ? and child_id = ?", parentId, childId);
        if ( linksCount == null ) {
            executeUpdate("insert into cw_core_object_links (parent_id, child_id, links_count) values (?, ?, 1)", parentId, childId);
        } else {
            executeUpdate("update cw_core_object_links set links_count = links_count+1 where parent_id=? and child_id=?", parentId, childId);
        }
    }

    public void deleteLink(BigDecimal parentId, BigDecimal childId) {
        if ( parentId == null || childId == null ) {
            throwException("Both deleted link object must be not null : @, @", new Object[]{parentId, childId});
        }

        Object[] params = new Object[]{parentId, childId};
        BigDecimal linksCount = executeQueryBigDecimal("select links_count from cw_core_object_links where parent_id = ? and child_id = ?", parentId, childId);
        if (linksCount != null){
            if ( linksCount.longValue() == 1 ) {
                executeUpdate("delete from cw_core_object_links where parent_id=? and child_id=?", parentId, childId);
            } else {
                executeUpdate("update cw_core_object_links set links_count = links_count-1 where parent_id=? and child_id=?", parentId, childId);
            }
        }
    }

    public void changeLinkParent(BigDecimal oldparentId, BigDecimal newparentId, BigDecimal childId) {
        if ( oldparentId != null && ! oldparentId.equals(newparentId)) {
            deleteLink(oldparentId, childId);
        }
        if ( newparentId != null && ! newparentId.equals(oldparentId)) {
            setLink(newparentId, childId);
        }
    }

    public BigDecimal typeNameToId(String typeName) {
        return getService(TypeService.class).fullNameToId(typeName);
    }

    LoadingCache<BigDecimal, Optional<BigDecimal>> idToTypeIdCache;
    public BigDecimal idToTypeId(BigDecimal id) {
        if ( id == null ) {
            return null;
        }
        if ( idToTypeIdCache == null ) {
            idToTypeIdCache = CacheBuilder.newBuilder().maximumSize(250)
                    .build(new CacheLoader<BigDecimal, Optional<BigDecimal>>() {
                        public Optional<BigDecimal> load(BigDecimal key) {
                            BigDecimal typeId = executeQueryBigDecimal("select type_id from cw_core_objects where id = ?", key);
                            Optional<BigDecimal> result = Optional.absent();
                            if ( typeId != null ) {
                                return Optional.of(typeId);
                            }
                            return Optional.absent();
                        }
                    });
        }
        try {
            Optional<BigDecimal> typeId = idToTypeIdCache.get(id);
            if ( typeId.isPresent() ) {
                return typeId.get();
            }
            idToTypeIdCache.invalidate(id);
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            throwException("System error on get object typeId ?", new Object[]{e.getMessage()}, e);
        }
        return null;
    }

    public BigDecimal guidToId(String guid) {
        return guid != null ? executeQueryBigDecimal("select id from cw_core_objects where guid = ?", guid) : null;
    }

    public String idToGuid(BigDecimal id) {
        return id != null ? executeQueryString("select guid from cw_core_objects where id = ?", id) : null;
    }

    public <T extends Service> Class<T> typeIdToServiceClass(BigDecimal typeId) {

        if ( typeId == null ) {
            return null;
        }
        Class clazz = typeServiceClassesCache.get(typeId);
        if ( clazz != null ) {
            return clazz;
        }

        Statement statement = executeQuery("select package_name, java_name from cw_core_types where id = ?", typeId);
        if ( ! statement.next() ) {
            statement.close();
            throwException("Cannot get service class of unknown type id ?", typeId);
        }

        String packageName;
        String serviceName;
        packageName = statement.getString(1);
        serviceName = statement.getString(2);

        if ( packageName == null || serviceName == null ) {
            throwException("Cannot get service class of type @ : both package and service must be not null (?, ?)", new Object[]{packageName, serviceName});
        }

        clazz            = Core.getClassByName(packageName + "." + serviceName.toLowerCase() + "." + StringUtils.toUpperFirstChar(serviceName) + "Service");
        typeServiceClassesCache.put(typeId, clazz);
        return clazz;
    }

    public AbstractTypeService typeIdToService(BigDecimal typeId) {
        if ( typeId == null ) {
            return  null;
        }
        AbstractTypeService service = typeServicesCache.get(typeId);
        if ( service != null ) {
            return service;
        }

        Class clazz = typeIdToServiceClass(typeId);
        try {
            service =  (AbstractTypeService) getSession().getService(clazz);
        } catch (Exception e) {
            throw new CoreException("Error on get type ? service." , new Object[]{typeId}, e);
        }
        typeServicesCache.put(typeId, service);

        return service;
    }

    public AbstractTypeService idToService(BigDecimal id) {
        return typeIdToService(idToTypeId(id));
    }

    public String idToString(BigDecimal id) {
        if ( id == null ) {
            return null;
        }
        AbstractTypeService typeService =  idToService(id);
        return typeService != null ? typeService.idToString(id) : null;
    }

}
