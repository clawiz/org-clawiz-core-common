package org.clawiz.core.common.storage.module;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class ModuleObjectPrototype extends AbstractObject {
    
    /**
    * PackageName
    */
    private java.lang.String packageName;
    
    /**
    * ModuleClassName
    */
    private java.lang.String moduleClassName;
    
    /**
    * ModuleVersion
    */
    private java.lang.String moduleVersion;
    
    public ModuleService service;
    
    /**
    * 
    * @return     PackageName
    */
    public java.lang.String getPackageName() {
        return this.packageName;
    }
    
    /**
    * Set 'PackageName' value
    * 
    * @param      packageName PackageName
    */
    public void setPackageName(java.lang.String packageName) {
        
        if ( packageName != null && packageName.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "Module.PackageName", "250");
        }
        
         this.packageName = packageName;
    }
    
    /**
    * Set 'PackageName' value and return this object
    * 
    * @param      packageName PackageName
    * @return     Module object
    */
    public ModuleObjectPrototype withPackageName(java.lang.String packageName) {
        setPackageName(packageName);
        return this;
    }
    
    /**
    * 
    * @return     ModuleClassName
    */
    public java.lang.String getModuleClassName() {
        return this.moduleClassName;
    }
    
    /**
    * Set 'ModuleClassName' value
    * 
    * @param      moduleClassName ModuleClassName
    */
    public void setModuleClassName(java.lang.String moduleClassName) {
        
        if ( moduleClassName != null && moduleClassName.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "Module.ModuleClassName", "250");
        }
        
         this.moduleClassName = moduleClassName;
    }
    
    /**
    * Set 'ModuleClassName' value and return this object
    * 
    * @param      moduleClassName ModuleClassName
    * @return     Module object
    */
    public ModuleObjectPrototype withModuleClassName(java.lang.String moduleClassName) {
        setModuleClassName(moduleClassName);
        return this;
    }
    
    /**
    * 
    * @return     ModuleVersion
    */
    public java.lang.String getModuleVersion() {
        return this.moduleVersion;
    }
    
    /**
    * Set 'ModuleVersion' value
    * 
    * @param      moduleVersion ModuleVersion
    */
    public void setModuleVersion(java.lang.String moduleVersion) {
        
        if ( moduleVersion != null && moduleVersion.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "Module.ModuleVersion", "250");
        }
        
         this.moduleVersion = moduleVersion;
    }
    
    /**
    * Set 'ModuleVersion' value and return this object
    * 
    * @param      moduleVersion ModuleVersion
    * @return     Module object
    */
    public ModuleObjectPrototype withModuleVersion(java.lang.String moduleVersion) {
        setModuleVersion(moduleVersion);
        return this;
    }
    
    public ModuleService getService() {
        return this.service;
    }
    
    public void setService(ModuleService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(ModuleObjectPrototype  target) {
        target.setPackageName(getPackageName());
        target.setModuleClassName(getModuleClassName());
        target.setModuleVersion(getModuleVersion());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((ModuleObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        service.save((ModuleObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(ModuleObjectPrototype object) {
        return 
               isObjectsEquals(this.getPackageName(), object.getPackageName() ) 
            && isObjectsEquals(this.getModuleClassName(), object.getModuleClassName() ) 
            && isObjectsEquals(this.getModuleVersion(), object.getModuleVersion() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getPackageName() != null ? getPackageName().hashCode() : 0);
        result = result * 31 + (getModuleClassName() != null ? getModuleClassName().hashCode() : 0);
        result = result * 31 + (getModuleVersion() != null ? getModuleVersion().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'PackageName' fields
    * 
    * @return     Concatenated string values of key 'toPackageName' fields
    */
    public String toPackageName() {
        return getPackageName();
    }
    
    /**
    * Prepare concatenated string values of key 'ModuleClassName' fields
    * 
    * @return     Concatenated string values of key 'toModuleClassName' fields
    */
    public String toModuleClassName() {
        return getModuleClassName();
    }
}
