package org.clawiz.core.common.storage.metadatastorage;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;
import org.clawiz.core.common.system.service.NotInitializeService;

public class MetadataStorageModelPrototype extends TypeModel {
    
    private static TypeField STORAGE_CLASS_ID_FIELD;
    
    private static TypeField PACKAGE_NAME_FIELD;
    
    private static TypeField URL_FIELD;
    
    private static Type type;
    
    private MetadataStorageService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public MetadataStorageService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(MetadataStorageService.class).getType();
        
        
        STORAGE_CLASS_ID_FIELD = type.getFields().get("StorageClass");
        if ( STORAGE_CLASS_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.MetadataStorage.StorageClass"}); }
        
        PACKAGE_NAME_FIELD = type.getFields().get("PackageName");
        if ( PACKAGE_NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.MetadataStorage.PackageName"}); }
        
        URL_FIELD = type.getFields().get("Url");
        if ( URL_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.MetadataStorage.Url"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _storageClassId;
    
    public TypeFieldModel storageClassId() {
        
        if ( _storageClassId != null ) {
            return _storageClassId;
        }
        
        _storageClassId = new TypeFieldModel(this, STORAGE_CLASS_ID_FIELD);
        return _storageClassId;
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.metadatastorageclass.MetadataStorageClassModel _storageClassTypeModel;
    
    public org.clawiz.core.common.storage.metadatastorageclass.MetadataStorageClassModel storageClass() {
        
        if ( _storageClassTypeModel == null ) {
            org.clawiz.core.common.storage.metadatastorageclass.MetadataStorageClassModel model = getService(org.clawiz.core.common.storage.metadatastorageclass.MetadataStorageClassModel.class, this.toString() + "_StorageClass");
            model.setJoinField(storageClassId());
            _storageClassTypeModel = model;
        }
        
        return _storageClassTypeModel;
    }
    
    private TypeFieldModel _packageName;
    
    public TypeFieldModel packageName() {
        
        if ( _packageName != null ) {
            return _packageName;
        }
        
        _packageName = new TypeFieldModel(this, PACKAGE_NAME_FIELD);
        return _packageName;
        
    }
    
    private TypeFieldModel _url;
    
    public TypeFieldModel url() {
        
        if ( _url != null ) {
            return _url;
        }
        
        _url = new TypeFieldModel(this, URL_FIELD);
        return _url;
        
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "STORAGECLASS" : return storageClassId();
        case "PACKAGENAME" : return packageName();
        case "URL" : return url();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
