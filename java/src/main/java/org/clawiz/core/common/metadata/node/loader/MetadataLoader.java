/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.metadata.node.loader;

import org.clawiz.core.common.Core;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeList;
import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import org.clawiz.core.common.metadata.node.loader.data.MetadataLoaderNodeClassField;
import org.clawiz.core.common.metadata.node.loader.data.MetadataLoaderNodeClass;
import org.clawiz.core.common.metadata.node.loader.xml.MetadataNodeXMLBuilder;
import org.clawiz.core.common.metadata.node.loader.xml.MetadataNodeXMLParser;
import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.core.common.system.service.Service;

import java.lang.reflect.Field;
import java.util.concurrent.ConcurrentHashMap;

public class MetadataLoader extends Service {

    public static String CLAWIZ_EXCHANGE_ELEMENT_NAME = "clawiz-exchange";
    public static String DICTIONARY_ELEMENT_NAME      = "dictionary";
    public static String NODES_ELEMENT_NAME           = "nodes";
    public static String CLASS_ALIAS_ELEMENT_NAME     = "class-alias";


    @NotInitializeService
    MetadataNodeXMLBuilder xmlBuilder;

    @NotInitializeService
    MetadataNodeXMLParser  xmlParser;

    public MetadataNodeXMLBuilder getXmlBuilder() {
        if ( xmlBuilder == null ) {
            xmlBuilder = getService(MetadataNodeXMLBuilder.class);
        }
        return xmlBuilder;
    }

    public MetadataNodeXMLParser getXmlParser() {
        if ( xmlParser == null ) {
            xmlParser = getService(MetadataNodeXMLParser.class);
        }
        return xmlParser;
    }

    public String nodeToXml(MetadataNode node) {
        MetadataNodeList nodes = new MetadataNodeList();
        nodes.add(node);
        return getXmlBuilder().build(nodes);
    }

    public String nodesToXml(MetadataNodeList nodes) {
        return getXmlBuilder().build(nodes);
    }

    public MetadataNodeList xmlToNodes(String xml) {
        return getXmlParser().parse(xml);
    }

    public static ConcurrentHashMap<Class, MetadataLoaderNodeClass> nodeClasseStructuressCache = new ConcurrentHashMap<>();


    private void addNodeClassStructureFields(MetadataLoaderNodeClass structure, Class clazz) {

        if ( clazz.getSuperclass() != null ) {
            addNodeClassStructureFields(structure, clazz.getSuperclass());
        }

        for( Field field : clazz.getDeclaredFields() ) {

            if ( field.getAnnotation(ExchangeAttribute.class) == null
                    && field.getAnnotation(ExchangeElement.class) == null
                    && field.getAnnotation(ExchangeReference.class)  == null ) {
                continue;
            }

            MetadataLoaderNodeClassField attribute = new MetadataLoaderNodeClassField();
            attribute.setNodeStructure(structure);
            attribute.setField(field);
            structure.getFields().add(attribute);
        }


    }

    public MetadataLoaderNodeClass getNodeClassStructure(Class clazz) {
        MetadataLoaderNodeClass structure = nodeClasseStructuressCache.get(clazz);
        if ( structure != null ) {
            return structure;
        }
        structure = new MetadataLoaderNodeClass();
        structure.setNodeClass(clazz);

        if ( ! MetadataNode.class.isAssignableFrom(clazz) ) {
            throwException("Class ? is not extend MetadataNode", clazz.getName());
        }

        addNodeClassStructureFields(structure, clazz);

        structure.prepare(getSession());

        nodeClasseStructuressCache.put(clazz, structure);

        return structure;
    }

    public MetadataLoaderNodeClass getNodeClassStructure(String className) {
        return getNodeClassStructure(Core.getClassByName(className));
    }


}
