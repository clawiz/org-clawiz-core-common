/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common;

import org.clawiz.core.common.system.config.Config;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.database.datasource.DataSource;
import org.clawiz.core.common.system.database.datasource.DataSourceConfig;
import org.clawiz.core.common.system.logger.LogManager;
import org.clawiz.core.common.system.logger.Logger;
import org.clawiz.core.common.system.module.Module;
import org.clawiz.core.common.system.module.ModuleDefinition;
import org.clawiz.core.common.system.module.version.ModuleVersion;
import org.clawiz.core.common.utils.reflection.ReflectionUtils;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.system.security.SecurityBase;
import org.clawiz.core.common.system.module.dispatcher.ModuleDispatcher;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.system.session.Sessions;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

/**
 * (c) Clawiz
 */
public class Core {

    private static Config config;

    private static Sessions sessions;
    private static LogManager logManager;

    public static Config getConfig() {
        return config;
    }

    public static Config getConfig(String path) {
        return config.getConfig(path);
    }

    public static void setConfig(Config config) {
        Core.config = config;
    }

    private static Logger log;

    private static ModuleDispatcher moduleDispatcher;


    private static ConcurrentHashMap<String, ModuleDefinition> moduleDefinitionsCache = new ConcurrentHashMap<>();
    public static <T extends Module> ModuleDefinition getModuleDefinition(Class<T> moduleClass) {
        String key = moduleClass.getName();
        ModuleDefinition definition = moduleDefinitionsCache.get(key);
        if ( definition != null ) {
            return  definition;
        }
        definition = getCoreSession().getService(moduleClass).getDefinition();

        moduleDefinitionsCache.put(key, definition);
        return definition;
    }

    private static ConcurrentHashMap<String, Boolean> installedCheckCache = new ConcurrentHashMap<>();
    public static <T extends Module> boolean isModuleInstalled(Class<T> moduleClass) {
        String key = moduleClass.getName();
        Boolean check = installedCheckCache.get(key);
        if ( check != null ) {
            return check;
        }
        check = new Boolean(false);
        if ( getCoreSession().getDatabaseAdapter().isTableExists("CW_CORE_MODULES") ) {
            ModuleDefinition definition = getModuleDefinition(moduleClass);
            if ( definition != null ) {
                check = getCoreSession().executeQueryBigDecimal("select count(1) from cw_core_modules where upper_package_name = ? "
                        , definition.getPackage().toUpperCase())
                        .intValue() > 0;
            }
        }
        installedCheckCache.put(key, check);
        return check;
    }

    public static boolean isModuleInstalled(ModuleDefinition definition, boolean exactVersion) {
        String key = definition.toString();
        Boolean check = installedCheckCache.get(key);
        if ( check != null ) {
            return check;
        }
        check = new Boolean(false);
        if (  getCoreSession().getDatabaseAdapter().isTableExists("CW_CORE_MODULES") ) {
            Statement statement = getCoreSession().executeQuery("select module_version from cw_core_modules where upper_package_name = ? "
                    , definition.getPackage().toUpperCase());
            while ( statement.next() ) {
                ModuleVersion installedVersion = new ModuleVersion(statement.getString(1));
                if ( (exactVersion && definition.getVersion().compareTo(installedVersion) == 0)
                        || ( ! exactVersion ) && definition.getVersion().compareTo(installedVersion) <= 0 ) {
                    check = true;
                    break;
                }
            }
            statement.close();
        }
        installedCheckCache.put(key, check);
        return check;
    }

    public static ModuleDispatcher getModuleDispatcher() {
        if ( moduleDispatcher == null ) {
            moduleDispatcher = getCoreSession().getService(ModuleDispatcher.class);
            moduleDispatcher.start();
        }

        return moduleDispatcher;
    }

    private static void prepareModules() {
        if ( isModuleInstalled(CoreCommonModule.class) ) {

            getModuleDispatcher().getSession().init();

            for(Module module : moduleDispatcher.getModules() ) {
                module.prepare();
            }
        }
    }

    public static void init() {

        logManager = new LogManager();

        log = getLogger(Core.class);

        sessions  = new Sessions();
        sessions.init();

        prepareModules();

    }

    public static Logger getLogger(Class clazz) {
        return logManager.getLogger(clazz);
    }

    public static String getConfigString(String key) {
        return getConfigString(key, false);
    }

    public static String getConfigString(String key, boolean raiseException) {
        String value = config.getString(key);
        if ( StringUtils.isEmpty(value) && raiseException ) {
            throw new CoreException("Config parameter '?' not defined", key);
        }
        return value;
    }

    public static String getConfigString(String key, String nullValue) {
        String value = config.getString(key);
        if (value == null)
            return nullValue;

        return value;
    }


    public static DataSource createDataSource(DataSourceConfig dataSourceConfig) {
        DataSource ds = null;
        try {
            ds = dataSourceConfig.getDataSourceClass().newInstance();
        } catch (Exception e) {
            throw new CoreException("Exception on create data source ? instance : ?", dataSourceConfig.getDataSourceClass().getName(), e.getMessage(), e);
        }
        ds.setConfig(dataSourceConfig);
        return ds;
    }

    private static DataSource dataSource;
    public static final String ORG_CLAWIZ_COREDATASOURCE_CONFIG_PATH = "org.clawiz.core.datasource";
    private static synchronized DataSource getDataSource() {
        if ( dataSource != null ) {
            return dataSource;
        }
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        Config config = getConfig(ORG_CLAWIZ_COREDATASOURCE_CONFIG_PATH);
        if ( config == null ) {
            throw new CoreException("Configuration parameter '?' not  defined", ORG_CLAWIZ_COREDATASOURCE_CONFIG_PATH);
        }
        dataSourceConfig.loadConfig(config);

        dataSource = createDataSource(dataSourceConfig);

        return dataSource;
    }

    public static Session newSession() {
        return newSession(null);
    }

    public static Session newSession(String sessionKey) {
        Session session = sessions.newSession(getDataSource(), sessionKey);
        return session;
    }

    public static Session newSession(DataSource dataSource, String sessionKey) {
        Session session = sessions.newSession(dataSource, sessionKey);
        return session;
    }

    public static Session getSession(String sessionKey) {
         return sessionKey != null ? sessions.getSession(sessionKey) : null;
    }

    private static Session coreSession;
    private static Session getCoreSession() {
        if ( coreSession == null ) {
            coreSession = newSession();
        }
        return coreSession;
    }

    private static String dispatcherUserName;
    public static String getDispatcherUserName() {
        return SecurityBase.ADMIN_USER_NAME;
    }

    private static boolean protectedLoginMode;

    public static boolean isProtectedLoginMode() {
        return protectedLoginMode;
    }

    public static Session newDispatcherSession(String sessionKey) {

        Session session = newSession(sessionKey);

        protectedLoginMode = true;
        try {
            session.login(getDispatcherUserName());
        } catch (CoreException e) {
            throw e;
        } finally {
            protectedLoginMode = false;
        }

        return session;
    }

    private static BigDecimal adminPermissionId;

    private static ConcurrentHashMap<String, Class> classesCache = new ConcurrentHashMap<>();
    public static Class getClassByName(String className) {
        if ( className == null ) {
            throw new CoreException("Class name cannot be null");
        }
        Class result = classesCache.get(className);
        if ( result == null ) {
            try {
                result = Class.forName(className);
            } catch (ClassNotFoundException e) {
                throw new CoreException("Class ? not found", className);
            }
            classesCache.put(className, result);
        }
        return result;
    }

    public static boolean isClassExists(String className) {
        try {
            Class.forName(className);
        } catch (ClassNotFoundException e) {
            return false;
        }
        return true;
    }


    public static Sessions getSessions() {
        return sessions;
    }

    public static String getTemporaryDir() {
        return "work";
    }

    public static Collection<Module> getModules() {
        return getModuleDispatcher().getModules();
    }

    public static <T extends Module> T getModule(Class<T> clazz) {
        return (T) getModuleDispatcher().getModule(clazz);
    }

    public static <T extends Module> Config getModuleConfig(Class<T> clazz) {
        Module module = getModule(clazz);
        if ( module == null ) {
            throw new CoreException("Module '?' not defined in system configuration", new Object[]{clazz.getName()});
        }
        return module.getConfig();
    }

    public static void clearStaticCaches() {
        Session reloadSession = newSession();
        ReflectionUtils.clearStaticCaches();
        reloadSession.destroy();

        installedCheckCache.clear();
        moduleDefinitionsCache.clear();

    }

}
