/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.storage.typefield;


import org.clawiz.core.common.storage.type.TypeService;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.utils.StringUtils;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;

public class TypeFieldService extends TypeFieldServicePrototype {

    TypeService typeService;

    private static ConcurrentHashMap<String, TypeField> fullNameToFieldCache = new ConcurrentHashMap<>();

    public static void clearCaches() {
        fullNameToFieldCache.clear();
    }

    public TypeField getByFullName(String fullName) {

        if ( fullName == null ) {
            throwException("Field name cannot be null");
        }

        TypeField field = fullNameToFieldCache.get(fullName);
        if ( field != null ) {
            return field;
        }

        String[] names = StringUtils.splitByLastTokenOccurrence(fullName, "\\.");
        if ( names.length < 2 ) {
            throwException("Wrong field name format '?'", new Object[]{fullName});
        }

        Type type = typeService.getByFullName(names[0]);
        if ( type == null ) {
            return null;
        }

        field = type.getFields().get(names[1]);
        if ( field == null ) {
            return null;
        }

        fullNameToFieldCache.put(fullName, field);

        return field;
    }


}
