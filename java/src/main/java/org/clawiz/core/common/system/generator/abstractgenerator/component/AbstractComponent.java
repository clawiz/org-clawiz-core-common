/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.abstractgenerator.component;

import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.core.common.system.generator.abstractgenerator.component.element.AbstractElement;
import org.clawiz.core.common.system.generator.abstractgenerator.writer.*;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.system.service.NotInitializeService;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: abdrashitovta
 * Date: 12.01.13
 * Time: 15?5
 * To change this template use File | Settings | File Templates.
 */
@NotInitializeService
public abstract class AbstractComponent extends Service {

    public enum OverwriteMode {

        SKIP, OVERWRITE, MERGE

    }

    private OverwriteMode overwriteMode = OverwriteMode.OVERWRITE;

    private String name;

    @NotInitializeService
    private AbstractGenerator generator;

    private AbstractWriter currentWriter;

    private ArrayList<AbstractElement> elements = new ArrayList<>();

    public void setGenerator(AbstractGenerator generator) {
        this.generator = generator;
    }

    public AbstractGenerator getGenerator() {
        return generator;
    }

    public String getName() {
        if (StringUtils.isLike(name, "*obsoleted*")) {
            try {
                logDebug("obsoleted call");
            } catch (CoreException e) {
                e.printStackTrace();
            }
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OverwriteMode getOverwriteMode() {
        return overwriteMode;
    }

    public void setOverwriteMode(OverwriteMode overwriteMode) {
        this.overwriteMode = overwriteMode;
    }

    public void prepare() {

    }

    public void process() {

    }

    public void callElementsProcess() {

        // To avoid ConcurrentModificationException don't change to array iteration
        for(int i=0; i < elements.size(); i++) {
            AbstractElement element = elements.get(i);
            element.process();
            element.callChildsProcess();
        }
    }

    public void done() {

    }

    public void callElementsDone() {
        for(AbstractElement element : elements) {
            element.done();
            element.callChildsDone();
        }
    }

    private HashMap<String, Class> elementClassReplacers = new HashMap<>();
    protected  <S extends AbstractElement, T extends AbstractElement> void addElementClassReplacer(Class<S> sourceClass, Class<T> targetClass) {
        if ( sourceClass.getName().equals(targetClass.getName())) {
            throwException("Source and target replaced class elements cannot be both equals : ?", new Object[]{sourceClass.getName()});
        }
        elementClassReplacers.put(sourceClass.getName(), targetClass);
    }


    public <T extends AbstractElement> T _addElement(String elementName, Class<T> _clazz) {
        Class<T> clazz = elementClassReplacers.get(_clazz.getName());
        if ( clazz != null ) {
            return _addElement(elementName, clazz);
        }
        clazz = _clazz;
        if ( elementName == null ) {
            throwException("Element name for '?' cannot be NULL", new Object[]{clazz.getName()});
        }
        T element = getService(clazz, true);
        elements.add(element);
        element.setComponent(this);
        element.setName(elementName);
        element.prepare();
        return element;
    }

    public <T extends AbstractElement> T addElement(String elementName, Class<T> clazz) {
        return _addElement(elementName, clazz);
    }

    public <T extends AbstractElement> T addElement(Class<T> clazz) {
        return addElement(generator.createUID(), clazz);
    }

    public boolean isDestinationExists() {
        return false;
    }

    public void merge() {
        throwException("Merge method not implemented for component ?", this.getClass().getName());
    }

    protected void writeElements() {
        getWriter();
        for ( AbstractElement element : elements) {
            element.write();
        }
    }

    public void write() {
        writeElements();
    }

    protected AbstractWriter createWriter() {
        AbstractWriter writer = getService(getWriterClass(), this.getName() + super.toString());
        writer.setComponent(this);
        return writer;
    }

    protected abstract <T extends AbstractWriter> Class<T> getWriterClass();

    public AbstractWriter getWriter() {
        if ( currentWriter != null ) {
            return currentWriter;
        }

        currentWriter = createWriter();
        currentWriter.setComponent(this);
        currentWriter.open();
        return currentWriter;
    }

    public void closeWriter() {
        if ( currentWriter != null ) {
            currentWriter.close();
            currentWriter = null;
        }
    }


    @Override
    public String toString() {
        return name + " (" + this.getClass().getName() + ")";
    }

    public ArrayList<AbstractElement> getElements() {
        return elements;
    }

    @Override
    public void destroy() {
        for (AbstractElement element : getElements() ) {
            element.destroy();
        }
        elements.clear();
        if ( currentWriter != null ) {
            currentWriter.close();
            currentWriter = null;
        }
        super.destroy();
    }

    public <T extends AbstractElement> ArrayList<T> getElementsByClass(Class<T> elementClass) {
        return getElementsByClass(elementClass, true);
    }

    public <T extends AbstractElement> ArrayList<T> getElementsByClass(Class<T> elementClass, boolean withAllChildLevels) {
        ArrayList<T> result = new ArrayList<>();


        for (AbstractElement element : getElements() ) {
            if ( elementClass.isAssignableFrom(element.getClass())) {
                result.add((T)element);
            }
            if ( withAllChildLevels ) {
                result.addAll(element.getElementsByClass(elementClass));
            }
        }

        return result;
    }

}
