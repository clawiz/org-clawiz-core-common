/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.metadata.loader;

import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeList;
import org.clawiz.core.common.system.service.Service;

public class AbstractMetadataLoader<T extends AbstractMetadataLoaderContext> extends Service {

    AbstractMetadataLoader<T> parent;
    MetadataLoaderList<T>     childs = new MetadataLoaderList<>();

    T context;

    public AbstractMetadataLoader<T> getParent() {
        return parent;
    }

    public MetadataLoaderList<T> getChilds() {
        return childs;
    }

    public void setParent(AbstractMetadataLoader<T> parent) {
        this.parent = parent;
    }

    public T getContext() {

        if ( context != null ) {
            return context;
        }

        if ( getParent() != null ) {
            return getParent().getContext();
        }

        return null;
    }

    public void setContext(T context) {
        this.context = context;
    }
    
    public <L extends AbstractMetadataLoader<T>> L addLoader(Class<L> loaderClass) {

        L child = getService(loaderClass, true);
        child.setParent(this);
        getChilds().add(child);
        child.prepare();

        return child;
    }
    
    public <M extends MetadataNode> M createNode(Class<M> nodeClass, String name) {
        M node = null;
        try {
            node = nodeClass.newInstance();
            node.setPackageName(getContext().getPackageName());
            node.setName(name);
            getContext().getNodes().add(node);
        } catch (Exception e) {
            throwException("Exception on create metadata node '?' of class '?'", name, nodeClass.getName());
        }
        return node;
    }

    public void prepare() {

    }

    public void callChildsProcess() {
        for (AbstractMetadataLoader child : getChilds() ) {
            child.process();
            child.callChildsProcess();
        }
    }

    public void process() {

    }

    public void callChildsDone() {
        for (AbstractMetadataLoader child : getChilds() ) {
            child.done();
            child.callChildsDone();
        }
    }


    public void done() {
        callChildsDone();
    }

    public MetadataNodeList getNodes() {
        return getContext().getNodes();
    }


    public void run() {

        prepare();
        process();

        for (AbstractMetadataLoader child : getChilds() ) {
            child.process();
            child.callChildsProcess();
        }

        done();

    }

}
