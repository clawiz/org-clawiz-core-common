package org.clawiz.core.common.storage.servicehandler;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;

public class ServiceHandlerModelPrototype extends TypeModel {
    
    private static TypeField SERVICE_NAME_FIELD;
    
    private static TypeField DESCRIPTION_FIELD;
    
    private static Type type;
    
    private ServiceHandlerService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public ServiceHandlerService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(ServiceHandlerService.class).getType();
        
        
        SERVICE_NAME_FIELD = type.getFields().get("ServiceName");
        if ( SERVICE_NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.ServiceHandler.ServiceName"}); }
        
        DESCRIPTION_FIELD = type.getFields().get("Description");
        if ( DESCRIPTION_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.ServiceHandler.Description"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _serviceName;
    
    public TypeFieldModel serviceName() {
        
        if ( _serviceName != null ) {
            return _serviceName;
        }
        
        _serviceName = new TypeFieldModel(this, SERVICE_NAME_FIELD);
        return _serviceName;
        
    }
    
    private TypeFieldModel _description;
    
    public TypeFieldModel description() {
        
        if ( _description != null ) {
            return _description;
        }
        
        _description = new TypeFieldModel(this, DESCRIPTION_FIELD);
        return _description;
        
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "SERVICENAME" : return serviceName();
        case "DESCRIPTION" : return description();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
