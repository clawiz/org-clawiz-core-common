package org.clawiz.core.common.metadata.data.type.property.condition;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ChangeableIfConditionTypePropertyPrototype extends org.clawiz.core.common.metadata.data.type.property.condition.AbstractConditionTypeProperty {
    
    public ChangeableIfConditionTypeProperty withName(String value) {
        setName(value);
        return (ChangeableIfConditionTypeProperty) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
