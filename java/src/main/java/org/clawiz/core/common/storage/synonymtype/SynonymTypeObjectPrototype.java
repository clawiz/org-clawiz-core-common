package org.clawiz.core.common.storage.synonymtype;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class SynonymTypeObjectPrototype extends AbstractObject {
    
    /**
    * Name
    */
    private java.lang.String name;
    
    /**
    * Description
    */
    private java.lang.String description;
    
    public SynonymTypeService service;
    
    /**
    * 
    * @return     Name
    */
    public java.lang.String getName() {
        return this.name;
    }
    
    /**
    * Set 'Name' value
    * 
    * @param      name Name
    */
    public void setName(java.lang.String name) {
        
        if ( name != null && name.length() > 75) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "SynonymType.Name", "75");
        }
        
         this.name = name;
    }
    
    /**
    * Set 'Name' value and return this object
    * 
    * @param      name Name
    * @return     SynonymType object
    */
    public SynonymTypeObjectPrototype withName(java.lang.String name) {
        setName(name);
        return this;
    }
    
    /**
    * 
    * @return     Description
    */
    public java.lang.String getDescription() {
        return this.description;
    }
    
    /**
    * Set 'Description' value
    * 
    * @param      description Description
    */
    public void setDescription(java.lang.String description) {
        
        if ( description != null && description.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "SynonymType.Description", "250");
        }
        
         this.description = description;
    }
    
    /**
    * Set 'Description' value and return this object
    * 
    * @param      description Description
    * @return     SynonymType object
    */
    public SynonymTypeObjectPrototype withDescription(java.lang.String description) {
        setDescription(description);
        return this;
    }
    
    public SynonymTypeService getService() {
        return this.service;
    }
    
    public void setService(SynonymTypeService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(SynonymTypeObjectPrototype  target) {
        target.setName(getName());
        target.setDescription(getDescription());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((SynonymTypeObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        service.save((SynonymTypeObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(SynonymTypeObjectPrototype object) {
        return 
               isObjectsEquals(this.getName(), object.getName() ) 
            && isObjectsEquals(this.getDescription(), object.getDescription() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getName() != null ? getName().hashCode() : 0);
        result = result * 31 + (getDescription() != null ? getDescription().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'Name' fields
    * 
    * @return     Concatenated string values of key 'toName' fields
    */
    public String toName() {
        return getName();
    }
}
