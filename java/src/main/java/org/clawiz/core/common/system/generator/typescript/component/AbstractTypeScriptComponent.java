/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.typescript.component;

import org.clawiz.core.common.metadata.data.common.valuetype.*;
import org.clawiz.core.common.system.generator.file.component.AbstractTextFileComponent;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.utils.file.FileUtils;

import java.util.HashMap;

public class AbstractTypeScriptComponent extends AbstractTextFileComponent {

    public class Import {
        String name;
        String alias;
        String from;

        public Import(String name, String alias, String from) {
            this.name = name;
            this.alias = alias;
            this.from = from;
        }
    }
    HashMap<String, HashMap<String, HashMap<String, Import>>> importsCache = new HashMap<>();

    public void addImport(String name, String alias, String from) {
        addImport(name, alias, from, true);
    }

     public void addImport(String name, String alias, String from, boolean makeRelativeFrom) {

         String preparedFrom  = from;

         if ( makeRelativeFrom ) {

             int index              = 0;
             String componentPath   = getDestinationPath();
             int lastSeparatorIndex = -1;
             for ( ; index < componentPath.length() && index < from.length() && from.charAt(index) == componentPath.charAt(index); index++) {
                 if ( componentPath.charAt(index) == '/') {
                     lastSeparatorIndex = index;
                 }
             }

             if ( index > 0 ) {
                 if ( index < componentPath.length() ) {

                     preparedFrom = "../"
                             + (index < from.length() ? preparedFrom.substring(lastSeparatorIndex+1) : "");
                     index++;
                     for ( ; index < componentPath.length() ; index++) {
                         if ( componentPath.charAt(index) == '/') {
                             preparedFrom = "../" + preparedFrom;
                         }
                     }

                 } else {
                     preparedFrom = "./"
                             + ( index < from.length() - 1 ? from.substring(index+1) : "");
                 }
             }

         }

        HashMap<String, HashMap<String, Import>> fromCache = importsCache.get(preparedFrom);
        if ( fromCache == null ) {
            fromCache = new HashMap<>();
            importsCache.put(preparedFrom, fromCache);
        }

        HashMap<String, Import> aliasesCache = fromCache.get(name);
        if ( aliasesCache == null ) {
            aliasesCache = new HashMap<>();
            fromCache.put(name, aliasesCache);
        }

        Import imp = new Import(name, alias, preparedFrom);

        String aliasKey = alias != null ? alias : "_null_";
        aliasesCache.put(aliasKey, imp);

    }

    public void addImport(String name, String from) {
        addImport(name, null, from);
    }

    public String nameToDottedFileName(String name) {
        return FileUtils.nameToDottedFileName(name);
    }

    public void addImport(AbstractTypeScriptComponent component) {
        addImport(component.getName(), null, component.getDestinationPath() + "/" + nameToDottedFileName(component.getName()), true);
    }

    public void addImport(String name, AbstractTypeScriptComponent component) {
        addImport(name, null, component.getDestinationPath() + "/" + nameToDottedFileName(component.getName()), true);
    }
    protected void writeImports() {
        for (String from : importsCache.keySet()) {
            HashMap<String, HashMap<String, Import>> fromCache = importsCache.get(from);
            StringBuilder sb     = new StringBuilder();
            String        prefix = "";
            for (HashMap<String, Import> aliasesCache : fromCache.values() ) {
                for (Import imp : aliasesCache.values()) {
                    sb.append(prefix).append(imp.name);
                    if ( imp.alias != null ) {
                        sb.append(" as " + imp.alias);
                    }
                }
                prefix = ", ";
            }
            pln("import { " + sb.toString() + " } from '" + from + "';");
        }
        if ( importsCache.size() > 0 ) {
            pln("");
        }
    }

    public String toTypeScriptType(AbstractValueType valueType) {
        if ( valueType instanceof ValueTypeString ) {
            return "string";
        } else if ( valueType instanceof ValueTypeNumber ) {
            return "number";
        } else if ( valueType instanceof ValueTypeBoolean) {
            return "boolean";
        } else if ( valueType instanceof ValueTypeDate) {
            return "Date";
        } else if ( valueType instanceof ValueTypeDateTime) {
            return "Date";
        } else if ( valueType instanceof ValueTypeObject ) {
            return "number";
        }
        throwException("Wrong value type ? to TypeScript type", valueType.toString());
        return null;
    }

    public String toTypeScriptType(TypeField field) {
        return (field.getValueType() instanceof ValueTypeEnumeration)
                ? field.getValueTypeJavaClassName()
                : toTypeScriptType(field.getValueType());
    }

    public String toSQLiteType(TypeField field) {
        if ( field.getValueType() instanceof ValueTypeString ) {
            return "text";
        } else if ( field.getValueType() instanceof ValueTypeNumber ) {
            return "text";
        } else if ( field.getValueType() instanceof ValueTypeBoolean) {
            return "real";
        } else if ( field.getValueType() instanceof ValueTypeDate) {
            return "text";
        } else if ( field.getValueType() instanceof ValueTypeDateTime ) {
            return "text";
        } else if ( field.getValueType() instanceof ValueTypeEnumeration ) {
            return "text";
        } else if ( field.getValueType() instanceof ValueTypeObject ) {
            return "number";
        }
        throwException("Wrong value type ? to SQLIte type", field.getValueType().toString());
        return null;
    }

    @Override
    public void write() {

        writeImports();

        writeElements();
    }
}
