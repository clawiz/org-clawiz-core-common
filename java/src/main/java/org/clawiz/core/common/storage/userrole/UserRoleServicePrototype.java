package org.clawiz.core.common.storage.userrole;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;
import org.clawiz.core.common.system.service.NotInitializeService;

public class UserRoleServicePrototype extends AbstractTypeService<UserRoleObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.core.common.storage", "UserRole");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.UserRole"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      userRole Checked object
    */
    public void check(UserRoleObject userRole) {
        
        if ( userRole == null ) {
            throwException("Cannot check null ?", new Object[]{"UserRoleObject"});
        }
        
        userRole.fillDefaults();
        
        
        if ( userRoleToId(userRole.getUserId(), userRole.getRoleId(), userRole.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "UserRole", userRole.toUserRole() });
        }
        
    }
    
    /**
    * Create new UserRoleObject instance and fill default values
    * 
    * @return     Created object
    */
    public UserRoleObject create() {
        
        UserRoleObject userRole = new UserRoleObject();
        userRole.setService((UserRoleService) this);
        
        userRole.fillDefaults();
        
        return userRole;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public UserRoleObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select user_id, role_id from cw_core_user_roles where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"UserRole", id});
        }
        
        UserRoleObject result = new UserRoleObject();
        
        result.setService((UserRoleService) this);
        result.setId(id);
        result.setUserId(statement.getBigDecimal(1));
        result.setRoleId(statement.getBigDecimal(2));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of UserRoleObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public UserRoleList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of UserRoleObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public UserRoleList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        UserRoleList result = new UserRoleList();
        
        
        Statement statement = executeQuery("select id, user_id, role_id from cw_core_user_roles"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            UserRoleObject object = new UserRoleObject();
        
            object.setService((UserRoleService) this);
            object.setId(statement.getBigDecimal(1));
            object.setUserId(statement.getBigDecimal(2));
            object.setRoleId(statement.getBigDecimal(3));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'UserRole' fields
    * 
    * @param      userId User
    * @param      roleId Role
    * @return     Id of found record or null
    */
    public BigDecimal userRoleToId(java.math.BigDecimal userId, java.math.BigDecimal roleId) {
        return userRoleToId(userId, roleId, null);
    }
    
    /**
    * Find id of record by key 'UserRole' fields with id not equal skipId
    * 
    * @param      userId User
    * @param      roleId Role
    * @param      skipId Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal userRoleToId(java.math.BigDecimal userId, java.math.BigDecimal roleId, BigDecimal skipId) {
        
        if ( userId == null || roleId == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_user_roles where user_id = ? and role_id = ?", userId, roleId);
        } else {
            return executeQueryBigDecimal("select id from cw_core_user_roles where user_id = ? and role_id = ? and id != ?", userId, roleId, skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'UserRole' fields or create new record with values set to given parameters
    * 
    * @param      userId              User
    * @param      roleId              Role
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal userRoleToId(java.math.BigDecimal userId, java.math.BigDecimal roleId, boolean createNewIfNotFound) {
        
        BigDecimal id = userRoleToId(userId, roleId, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        UserRoleObject object = create();
        object.setUserId(userId);
        object.setRoleId(roleId);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'UserRole' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToUserRole(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select user_id, role_id from cw_core_user_roles where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        UserRoleObject object = new UserRoleObject();
        object.setService((UserRoleService)this);
        object.setId(id);
        object.setUserId(statement.getBigDecimal(1));
        object.setRoleId(statement.getBigDecimal(2));
        
        statement.close();
        
        return object.toUserRole();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToUserRole(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      userRoleObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(UserRoleObject userRoleObject) {
        return userRoleObject.toUserRole();
    }
    
    protected void saveAudit(TransactionAction transactionAction, UserRoleObject oldUserRoleObject, UserRoleObject newUserRoleObject) {
        
        UserRoleObject o = oldUserRoleObject != null ? oldUserRoleObject : new UserRoleObject();
        UserRoleObject n = newUserRoleObject != null ? newUserRoleObject : new UserRoleObject();
        
        
        executeUpdate("insert into a_cw_core_user_roles (scn, action_type, id , o_user_id, o_role_id, n_user_id, n_role_id) values ( ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getUserId(), o.getRoleId(), n.getUserId(), n.getRoleId());
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      userRole Saved object
    */
    public void save(UserRoleObject userRole) {
        
        if ( userRole == null ) {
            throwException("Cannot save NULL ?", new Object[]{"UserRoleObject"});
        }
        
        TransactionAction transactionAction;
        UserRoleObject oldUserRole;
        if ( userRole.getService() == null ) {
            userRole.setService((UserRoleService)this);
        }
        
        check(userRole);
        
        if ( userRole.getId() == null ) {
        
            userRole.setId(getObjectService().createObject(getTypeId()));
        
            oldUserRole = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, userRole.getId());
        
            executeUpdate("insert into cw_core_user_roles" 
                          + "( id, user_id, role_id ) "
                          + "values"
                          + "(?, ?, ?)",
                          userRole.getId(), userRole.getUserId(), userRole.getRoleId() );
        
            if ( userRole.getUserId() != null ) { getObjectService().setLink(userRole.getUserId(), userRole.getId()); }
            if ( userRole.getRoleId() != null ) { getObjectService().setLink(userRole.getRoleId(), userRole.getId()); }
        
        } else {
        
            oldUserRole = load(userRole.getId());
            if ( oldUserRole.equals(userRole) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, userRole.getId());
        
            executeUpdate("update cw_core_user_roles set "
                          + "user_id = ?, role_id = ? "
                          + "where id = ?",
                          userRole.getUserId(), userRole.getRoleId(), userRole.getId() );
        
            getObjectService().changeLinkParent(oldUserRole.getUserId(), userRole.getUserId(), userRole.getId());
            getObjectService().changeLinkParent(oldUserRole.getRoleId(), userRole.getRoleId(), userRole.getId());
        
        }
        
        saveAudit(transactionAction, oldUserRole, userRole);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        UserRoleObject oldUserRoleObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldUserRoleObject, null);
        
        getObjectService().deleteObject(id);
        
        if (oldUserRoleObject.getUserId() != null) { getObjectService().deleteLink(oldUserRoleObject.getUserId(), id); }
        if (oldUserRoleObject.getRoleId() != null) { getObjectService().deleteLink(oldUserRoleObject.getRoleId(), id); }
        
        executeUpdate("delete from cw_core_user_roles where id = ?", id);
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.role.RoleService roleService;
    
    public org.clawiz.core.common.storage.role.RoleService getRoleService() {
        if ( roleService == null ) {
            roleService = getService(org.clawiz.core.common.storage.role.RoleService.class);
        }
        return roleService;
    }
    
    public org.clawiz.core.common.storage.role.RoleObject createRole() {
        return getRoleService().create();
    }
    
    public org.clawiz.core.common.storage.role.RoleObject loadRole(BigDecimal id) {
        return getRoleService().load(id);
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.user.UserService userService;
    
    public org.clawiz.core.common.storage.user.UserService getUserService() {
        if ( userService == null ) {
            userService = getService(org.clawiz.core.common.storage.user.UserService.class);
        }
        return userService;
    }
    
    public org.clawiz.core.common.storage.user.UserObject createUser() {
        return getUserService().create();
    }
    
    public org.clawiz.core.common.storage.user.UserObject loadUser(BigDecimal id) {
        return getUserService().load(id);
    }
}
