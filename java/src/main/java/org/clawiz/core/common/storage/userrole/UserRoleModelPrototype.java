package org.clawiz.core.common.storage.userrole;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;
import org.clawiz.core.common.system.service.NotInitializeService;

public class UserRoleModelPrototype extends TypeModel {
    
    private static TypeField USER_ID_FIELD;
    
    private static TypeField ROLE_ID_FIELD;
    
    private static Type type;
    
    private UserRoleService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public UserRoleService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(UserRoleService.class).getType();
        
        
        USER_ID_FIELD = type.getFields().get("User");
        if ( USER_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.UserRole.User"}); }
        
        ROLE_ID_FIELD = type.getFields().get("Role");
        if ( ROLE_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.UserRole.Role"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _userId;
    
    public TypeFieldModel userId() {
        
        if ( _userId != null ) {
            return _userId;
        }
        
        _userId = new TypeFieldModel(this, USER_ID_FIELD);
        return _userId;
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.user.UserModel _userTypeModel;
    
    public org.clawiz.core.common.storage.user.UserModel user() {
        
        if ( _userTypeModel == null ) {
            org.clawiz.core.common.storage.user.UserModel model = getService(org.clawiz.core.common.storage.user.UserModel.class, this.toString() + "_User");
            model.setJoinField(userId());
            _userTypeModel = model;
        }
        
        return _userTypeModel;
    }
    
    private TypeFieldModel _roleId;
    
    public TypeFieldModel roleId() {
        
        if ( _roleId != null ) {
            return _roleId;
        }
        
        _roleId = new TypeFieldModel(this, ROLE_ID_FIELD);
        return _roleId;
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.role.RoleModel _roleTypeModel;
    
    public org.clawiz.core.common.storage.role.RoleModel role() {
        
        if ( _roleTypeModel == null ) {
            org.clawiz.core.common.storage.role.RoleModel model = getService(org.clawiz.core.common.storage.role.RoleModel.class, this.toString() + "_Role");
            model.setJoinField(roleId());
            _roleTypeModel = model;
        }
        
        return _roleTypeModel;
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "USER" : return userId();
        case "ROLE" : return roleId();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
