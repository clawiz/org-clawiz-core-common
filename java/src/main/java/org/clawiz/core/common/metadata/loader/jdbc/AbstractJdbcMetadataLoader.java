/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.metadata.loader.jdbc;

import org.clawiz.core.common.Core;
import org.clawiz.core.common.metadata.loader.AbstractMetadataLoader;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.database.datasource.DataSource;
import org.clawiz.core.common.system.database.datasource.DataSourceConfig;
import org.clawiz.core.common.system.database.datasource.persistent.PersistentDataSource;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.utils.RandomGUID;

public class AbstractJdbcMetadataLoader<T extends AbstractJdbcMetadataLoaderContext> extends AbstractMetadataLoader<T> {

    protected void createConnection() {

        if ( getContext().getSourceSession() != null ) {
            return;
        }

        logDebug("Connect to JDBC source : " + getContext().getSourceUrl());

        DataSource dataSource = new PersistentDataSource();
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setUrl(getContext().getSourceUrl());
        dataSourceConfig.setUser(getContext().getSourceUser());
        dataSourceConfig.setPassword(getContext().getSourcePassword());
        dataSource.setConfig(dataSourceConfig);

        Session sourceSession = Core.getSessions().newSession(dataSource, new RandomGUID().toString());
        sourceSession.getConnection().getJDBCConnection();
        getSession().addChildSession(sourceSession);

        getContext().setSourceSession(sourceSession);


    }

    @Override
    public void prepare() {
        super.prepare();
        createConnection();
    }


    public Statement executeSourceQuery(String sql, Object... parameters) {
        return getContext().getSourceSession().executeQuery(sql, parameters);
    }

    @Override
    public void done() {

        super.done();

        if ( getContext().getSourceSession() != null ) {
            getContext().getSourceSession().destroy();
            getContext().setSourceSession(null);
        }


    }

}
