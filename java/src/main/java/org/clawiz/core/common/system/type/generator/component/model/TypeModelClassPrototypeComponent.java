/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator.component.model;

import org.clawiz.core.common.storage.typefield.TypeFieldService;
import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.core.common.system.generator.java.component.element.JavaMemberElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaVariableElement;
import org.clawiz.core.common.system.type.generator.component.model.element.*;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.generator.component.AbstractTypeGeneratorJavaClassComponent;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;
import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeBLOB;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;

import java.util.ArrayList;

public class TypeModelClassPrototypeComponent extends AbstractTypeGeneratorJavaClassComponent {

    public class StaticTypeFieldVariable {
        public TypeField field;
        public String    name;

        public StaticTypeFieldVariable(TypeField field, String name) {
            this.field = field;
            this.name = name;
        }
    }
    private ArrayList<StaticTypeFieldVariable> staticTypeFieldVariables = new ArrayList<>();

    protected void addVariables() {


        for(TypeField field : getType().getFields() ) {
            if ( field.getValueType() instanceof ValueTypeBLOB || field.getValueType() instanceof ValueTypeList) {
                continue;
            }
            JavaVariableElement ve = addVariable("TypeField", GeneratorUtils.toUpperWithUnderscore(field.getJavaVariableName()) + "_FIELD");
            ve.setStatic(true);
            ve.setAccessLevel(JavaMemberElement.AccessLevel.PRIVATE);
            staticTypeFieldVariables.add(new StaticTypeFieldVariable(field, ve.getName()));
        }

        JavaVariableElement typeVariable        = addVariable("Type", "type").withAccessLevel(JavaMemberElement.AccessLevel.PRIVATE).withStatic(true);

        JavaVariableElement typeServiceVariable = addVariable(getServiceClassName(), "typeService").withAccessLevel(JavaMemberElement.AccessLevel.PRIVATE);

        addVariable("boolean", "staticMembersInitialized").withAccessLevel(JavaMemberElement.AccessLevel.PRIVATE).withStatic(true);

        typeVariable.addGetter();
        typeServiceVariable.addGetter();

    }

    protected void addImports() {
        addImport(Type.class);
        addImport(TypeField.class);
        addImport(TypeModel.class);
        addImport(TypeFieldModel.class);
        addImport(TypeRecordIdModel.class);
    }

    @Override
    public void process() {
        super.process();

        setName(getGenerator().getComponentByClass(TypeModelClassComponent.class).getName() + "Prototype");

        setExtends("TypeModel");

        addImports();

        addVariables();

        addElement(TypeModelInitMethodElement.class).setStaticTypeFieldVariables(staticTypeFieldVariables);
        addElement(TypeModelGetTypeRecordIdModelMethodElement.class);

        addVariable("TypeRecordIdModel", "_id").withAccessLevel(JavaMemberElement.AccessLevel.PRIVATE);

        for(TypeField field : getFields() ) {

            if ( field.getValueType() instanceof ValueTypeBLOB || field.getValueType() instanceof ValueTypeList) {
                continue;
            }

            String fieldModelVariableName = "_" + field.getJavaVariableName();
            addVariable("TypeFieldModel", fieldModelVariableName).withAccessLevel(JavaMemberElement.AccessLevel.PRIVATE);

            addElement(TypeModelGetTypeFieldModelMethodElement.class).withField(field);

            if ( field.getValueType() instanceof ValueTypeObject  ) {
                Type   refType               = ((ValueTypeObject) field.getValueType()).getReferencedType();
                if ( refType != null ) {
                    //!!! Do not add type model class to imports to avoid conflicts with some named classes from different packages
                    String modelClassName        = refType.getPackageName() + "." + refType.getJavaClassName().toLowerCase() + "." + refType.getJavaClassName() + "Model";
                    String typeModelVariableName = "_" + field.getGetTypeModelMethodName() + "TypeModel";

                    addVariable(modelClassName, typeModelVariableName)
                            .withAccessLevel(JavaMemberElement.AccessLevel.PRIVATE)
                            .addAnnotation(NotInitializeService.class);

                    addElement(TypeModelGetTypeModelMethodElement.class)
                            .withField(field)
                            .withFieldModelClassName(modelClassName)
                            .withFieldModelVariableName(typeModelVariableName)
                            .withJoinField(field);
                }

            }

        }

        addElement(TypeModelGetTypeFieldModelByNameMethodElement.class);

    }
}
