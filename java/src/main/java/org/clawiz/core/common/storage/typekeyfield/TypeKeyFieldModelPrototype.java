package org.clawiz.core.common.storage.typekeyfield;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.typefield.TypeFieldService;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;
import org.clawiz.core.common.system.service.NotInitializeService;

public class TypeKeyFieldModelPrototype extends TypeModel {
    
    private static TypeField KEY_ID_FIELD;
    
    private static TypeField ORDER_NO_FIELD;
    
    private static TypeField FIELD_ID_FIELD;
    
    private static Type type;
    
    private TypeKeyFieldService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public TypeKeyFieldService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(TypeKeyFieldService.class).getType();
        
        TypeFieldService typeFieldService = getService(TypeFieldService.class);
        
        KEY_ID_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeKeyField.Key");
        if ( KEY_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeKeyField.Key"}); }
        
        ORDER_NO_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeKeyField.OrderNo");
        if ( ORDER_NO_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeKeyField.OrderNo"}); }
        
        FIELD_ID_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeKeyField.Field");
        if ( FIELD_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeKeyField.Field"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _keyId;
    
    public TypeFieldModel keyId() {
        
        if ( _keyId != null ) {
            return _keyId;
        }
        
        _keyId = new TypeFieldModel(this, KEY_ID_FIELD);
        return _keyId;
        
    }
    
    private TypeFieldModel _orderNo;
    
    public TypeFieldModel orderNo() {
        
        if ( _orderNo != null ) {
            return _orderNo;
        }
        
        _orderNo = new TypeFieldModel(this, ORDER_NO_FIELD);
        return _orderNo;
        
    }
    
    private TypeFieldModel _fieldId;
    
    public TypeFieldModel fieldId() {
        
        if ( _fieldId != null ) {
            return _fieldId;
        }
        
        _fieldId = new TypeFieldModel(this, FIELD_ID_FIELD);
        return _fieldId;
        
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "KEY" : return keyId();
        case "ORDERNO" : return orderNo();
        case "FIELD" : return fieldId();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
