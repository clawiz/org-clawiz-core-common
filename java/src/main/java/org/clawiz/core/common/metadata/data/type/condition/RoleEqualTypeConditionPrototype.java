package org.clawiz.core.common.metadata.data.type.condition;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class RoleEqualTypeConditionPrototype extends org.clawiz.core.common.metadata.data.type.condition.AbstractTypeCondition {
    
    @ExchangeReference
    private org.clawiz.core.common.metadata.data.security.role.RoleList roles = new org.clawiz.core.common.metadata.data.security.role.RoleList();
    
    public RoleEqualTypeCondition withName(String value) {
        setName(value);
        return (RoleEqualTypeCondition) this;
    }
    
    public org.clawiz.core.common.metadata.data.security.role.RoleList getRoles() {
        return this.roles;
    }
    
    public RoleEqualTypeCondition withRole(org.clawiz.core.common.metadata.data.security.role.Role value) {
        getRoles().add(value);
        return (RoleEqualTypeCondition) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        for (MetadataNode node : getRoles()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        for (MetadataNode node : getRoles()) {
            references.add(node);
        }
        
    }
}
