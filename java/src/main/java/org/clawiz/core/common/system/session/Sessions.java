/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.session;

import org.clawiz.core.common.Core;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.database.datasource.DataSource;
import org.clawiz.core.common.system.logger.Logger;
import org.clawiz.core.common.utils.RandomGUID;

import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

public class Sessions {

    public ConcurrentHashMap<String, Session> sessions = new ConcurrentHashMap<String, Session>();

    private Logger log;

    public void init() {
        log = Core.getLogger(this.getClass());
    }

    public synchronized Session newSession(DataSource dataSource, String key) {
        String _key = key != null ? key : (new RandomGUID(true).toString());

        if ( sessions.get(_key) != null) {
            throw new CoreException("Session key '?' already exists", new Object[]{key});
        }

        Session session = new Session(_key, dataSource);

        session.init();

        sessions.put(_key, session);

        return session;
    }



    public synchronized Session getSession(String key) {
        return sessions.get(key);
    }

    public synchronized void destroySession(Session session) {
        sessions.remove(session.getKey());
        session.destroy();
    }


    public synchronized void clear() {
        Iterator it = sessions.values().iterator();

        while (it.hasNext()) {
            Session session = (Session) it.next();
            destroySession(session);
        }

        sessions.clear();

    }

    public ConcurrentHashMap<String, Session> getSessions() {
        return sessions;
    }
}
