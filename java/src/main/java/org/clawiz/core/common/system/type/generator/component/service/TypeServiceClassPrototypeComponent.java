/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator.component.service;

import org.clawiz.core.common.storage.type.TypeService;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.core.common.system.generator.java.component.element.JavaMemberElement;
import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;
import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.property.extension.AbstractExtensionTypeProperty;
import org.clawiz.core.common.metadata.data.type.property.extension.action.CallServiceMethodExtensionTypePropertyAction;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.type.key.TypeKey;
import org.clawiz.core.common.system.type.generator.component.AbstractTypeGeneratorJavaClassComponent;
import org.clawiz.core.common.system.type.generator.component.service.element.*;
import org.clawiz.core.common.system.type.generator.component.service.element.load.*;
import org.clawiz.core.common.system.type.generator.component.service.element.key.*;
import org.clawiz.core.common.system.type.generator.component.service.element.reference.*;
import org.clawiz.core.common.system.type.generator.component.service.element.save.TypeServiceSaveAuditMethodElement;
import org.clawiz.core.common.system.type.generator.component.service.element.save.TypeServiceSaveMethodElement;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

public class TypeServiceClassPrototypeComponent extends AbstractTypeGeneratorJavaClassComponent {

    public final static String AFTER_PROCESS_EXTENSION_NAME = "afterProcessTypeServiceClassPrototype";

    protected void addVariables() {

        addVariable("BigDecimal", "_typeId").withStatic(true).setAccessLevel(JavaMemberElement.AccessLevel.PRIVATE);

        for (AbstractExtensionTypeProperty extension : getType().getProperties().getNodeListByClass(AbstractExtensionTypeProperty.class) ) {
            if ( extension.getAction() instanceof CallServiceMethodExtensionTypePropertyAction) {
                CallServiceMethodExtensionTypePropertyAction csa = (CallServiceMethodExtensionTypePropertyAction) extension.getAction();
                String variableType  = csa.getServiceMethod().getService().getFullName();
                String variableName  = StringUtils.toLowerFirstChar(GeneratorUtils.toJavaClassName(variableType));
                addVariable(variableType, variableName);

            }
        }

    }

    protected void addImports() {

        addImport(AbstractTypeService.class);

        addImport(Type.class);
        addImport(TypeField.class);
        addImport(TypeService.class);

        addImport(BigDecimal.class);
        addImport(DateUtils.class);
        addImport(Date.class);

        addImport(Statement.class);
        addImport(TransactionAction.class);
        addImport(TransactionActionType.class);

    }

    protected void addReferencedObjectMethods() {

        HashMap<Type, Type> typesCache       = new HashMap<>();
        HashMap<Type, Type> asObjectCache    = new HashMap<>();
        HashMap<Type, Type> asListCache      = new HashMap<>();

        for ( TypeField field : getType().getFields() ) {

            Type referencedType = null;
            if (field.getValueType() instanceof ValueTypeObject) {
                referencedType = ((ValueTypeObject) field.getValueType()).getReferencedType();
                asObjectCache.put(referencedType, referencedType);
            } else if (field.getValueType() instanceof ValueTypeList) {
                referencedType = ((ValueTypeList) field.getValueType()).getReferencedType();
                asListCache.put(referencedType, referencedType);
            }

            if (referencedType != null) {
                typesCache.put(referencedType, referencedType);
                continue;
            }
        }

        for (Type referencedType : typesCache.keySet()) {

            addImport(NotInitializeService.class);
            addVariable(referencedType.getServicePackageName() + "." + referencedType.getJavaClassName() + "Service"
                    , StringUtils.toLowerFirstChar(referencedType.getJavaClassName()) + "Service")
                    .withAccessLevel(JavaMemberElement.AccessLevel.PRIVATE)
                    .addAnnotation(NotInitializeService.class);

            addElement(TypeServiceGetReferencedTypeServiceMethodElement.class).setReferencedType(referencedType);
            addElement(TypeServiceCreateReferencedTypeMethodElement.class).setReferencedType(referencedType);

            if ( asObjectCache.containsKey(referencedType)) {
                addElement(TypeServiceLoadReferencedTypeMethodElement.class).setReferencedType(referencedType);
            }
            if ( asListCache.containsKey(referencedType)) {
                addElement(TypeServiceLoadReferencedTypeListMethodElement.class).setReferencedType(referencedType);
            }

        }

    }

    @Override
    public void process() {
        super.process();

        setName(getGenerator().getComponentByClass(TypeServiceClassComponent.class).getName() + "Prototype");

        setExtends("AbstractTypeService<" + getDataObjectClassName() + ">");

        addImports();

        addVariables();

        addElement(TypeServiceInitMethodElement.class);

        addElement(TypeServiceCheckMethodElement.class);

        addElement(TypeServiceCreateMethodElement.class);

        addElement(TypeServiceLoadMethodElement.class);

        addElement(TypeServiceLoadListMethodElement.class);

        addElement(TypeServiceLoadOrderedListMethodElement.class);

        for (TypeKey key : getKeys() ) {

            if ( key.isUnique() ) {
                addElement(TypeServiceKeyToIdMethodElement.class).setKey(key);
                addElement(TypeServiceKeyToIdWithSkipIdMethodElement.class).setKey(key);
                addElement(TypeServiceKeyToIdAutoCreateMethodElement.class).setKey(key);
            }

            addElement(TypeServiceIdToKeyStringMethodElement.class).setKey(key);
        }

        TypeKey toStringKey = getGenerator().getType().getToStringKey();
        if ( toStringKey == null ) {
            throwException("toString key for type '?' not defined", new Object[]{getType().getFullName()});
        }

        addElement(TypeServiceIdToStringMethodElement.class).setKey(toStringKey);
        addElement(TypeServiceObjectToStringMethodElement.class).setKey(toStringKey);

        addElement(TypeServiceSaveAuditMethodElement.class);
        addElement(TypeServiceSaveMethodElement.class);
        addElement(TypeServiceDeleteMethodElement.class);

        addReferencedObjectMethods();

        processExtensions(AFTER_PROCESS_EXTENSION_NAME);

    }
}
