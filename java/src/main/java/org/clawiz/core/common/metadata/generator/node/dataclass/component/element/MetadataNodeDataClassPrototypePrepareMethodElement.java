package org.clawiz.core.common.metadata.generator.node.dataclass.component.element;

import org.clawiz.core.common.metadata.data.language.node.element.field.AbstractStructureField;
import org.clawiz.core.common.metadata.data.language.node.element.field.AttributeStructureField;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;
import org.clawiz.core.common.system.session.Session;

public class MetadataNodeDataClassPrototypePrepareMethodElement extends AbstracMetadataNodeDataClassPrototypeMethodElement {


    protected void processField(AbstractStructureField field) {

        if ( field instanceof AttributeStructureField) {
            return;
        }

        if ( field.isArray() ) {
            getComponent().addImport(MetadataNode.class);
            addText("for (MetadataNode node : " + field.getGetMethodName() + "()) {");
            addText("    if ( node != null ) { ");
            addText("        node.prepare(session);");
            addText("    }");
            addText("}");

        } else {
            addText("if ( " + field.getGetMethodName() +  "() != null ) { ");
            addText("    " + field.getGetMethodName() + "().prepare(session);");
            addText("}");
        }

    }


    @Override
    public void process() {
        super.process();

        setName("prepare");

        addAnnotation(SuppressWarnings.class).withValue("\"Duplicates\"");

        addParameter(Session.class, "session");


        addText("super.prepare(session);");
        addText("if ( isInPrepare() ) { return; }");
        addText("setInPrepare(true);");
        addText("");

        for (AbstractStructureField field : getStructure().getFields()) {
            processField(field);
        }

        addText("");
        addText("setInPrepare(false);");


    }
}
