package org.clawiz.core.common.metadata.data.type.key;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class TypeKeyFieldPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeReference
    private org.clawiz.core.common.metadata.data.type.field.TypeField field;
    
    public TypeKeyField withName(String value) {
        setName(value);
        return (TypeKeyField) this;
    }
    
    public org.clawiz.core.common.metadata.data.type.field.TypeField getField() {
        return this.field;
    }
    
    public void setField(org.clawiz.core.common.metadata.data.type.field.TypeField value) {
        this.field = value;
    }
    
    public TypeKeyField withField(org.clawiz.core.common.metadata.data.type.field.TypeField value) {
        setField(value);
        return (TypeKeyField) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getField() != null ) { 
            getField().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getField());
        
    }
}
