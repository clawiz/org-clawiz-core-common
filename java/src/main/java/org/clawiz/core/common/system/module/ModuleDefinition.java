/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.module;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.module.version.ModuleVersion;

import java.util.ArrayList;

/**
 *
 */
public class ModuleDefinition {

    private String        packageName;
    private ModuleVersion version;

    private ArrayList<ModuleRequire> requires = new ArrayList<>();

    public ModuleDefinition() {
    }

    public ModuleDefinition(String packageName) {
        setPackage(packageName);
    }

    public ModuleDefinition(String packageName, String version) {
        this.packageName = packageName;
        this.version = new ModuleVersion(version);
    }

    public String getPackage() {
        return packageName;
    }

    public void setPackage(String packageName) {
        if ( packageName == null ) {
            throw new CoreException("Module package namecannot be null !");
        }
        this.packageName = packageName;

    }

    public ModuleDefinition withPackage(String packageName) {
        setPackage(packageName);
        return this;
    }

    public ModuleVersion getVersion() {
        return version;
    }

    public void setVersion(ModuleVersion version) {
        this.version = version;
    }

    public void setVersion(String versionString) {
        this.version = new ModuleVersion(versionString);
    }

    public ModuleDefinition withVersion(ModuleVersion version) {
        this.version = version;
        return this;
    }

    public ModuleDefinition withVersion(String versionString) {
        this.version = new ModuleVersion(versionString);
        return this;
    }


    public ArrayList<ModuleRequire> getRequires() {
        return requires;
    }

    public <T extends Module> void addRequire(Class<T> moduleClass, String version, boolean exactVersion) {
        if ( moduleClass.getPackage().getName().equals(this.packageName) ) {
            throw new CoreException("Module ? cannot require itself", this.packageName);
        }
        ModuleRequire require = new ModuleRequire();
        require.setModuleClass(moduleClass);
        require.setExactVersion(exactVersion);
        requires.add(require);
    }

    public <T extends Module> void addRequire(Class<T> moduleClass, String version) {
        addRequire(moduleClass, version, false);
    }

    public <T extends Module> void addRequire(Class<T> moduleClass) {
        addRequire(moduleClass, null, false);
    }

    @Override
    public String toString() {
        return "{" +
                "packageName='" + packageName + '\'' +
                ( version != null ? ", version=" + version.toString() : "") +
                '}';
    }
}
