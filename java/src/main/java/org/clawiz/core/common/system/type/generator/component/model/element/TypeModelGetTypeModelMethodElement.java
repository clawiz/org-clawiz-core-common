/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator.component.model.element;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.generator.component.service.element.AbstractTypeServiceMethodElement;

public class TypeModelGetTypeModelMethodElement extends AbstractTypeServiceMethodElement {

    TypeField field;
    String    fieldModelVariableName;
    String    fieldModelClassName;
    TypeField joinField;

    public TypeField getField() {
        return field;
    }

    public TypeModelGetTypeModelMethodElement withField(TypeField field) {
        this.field = field;
        return this;
    }

    public TypeModelGetTypeModelMethodElement withFieldModelVariableName(String fieldModelVariableName) {
        this.fieldModelVariableName = fieldModelVariableName;
        return this;
    }

    public TypeModelGetTypeModelMethodElement withFieldModelClassName(String fieldModelClassName) {
        this.fieldModelClassName = fieldModelClassName;
        return this;
    }

    public TypeModelGetTypeModelMethodElement withJoinField(TypeField field) {
        this.joinField = field;
        return this;
    }

    @Override
    public void process() {

        super.process();
        setName(getField().getGetTypeModelMethodName());

        setType(fieldModelClassName);

        addText("");
        addText("if ( " + fieldModelVariableName + " == null ) {");
        addText("    " + fieldModelClassName + " model = getService(" + fieldModelClassName + ".class, this.toString() + \"_" + field.getName() + "\");");
        addText("    model.setJoinField(" + joinField.getJavaVariableName() + "());");
        addText("    " + fieldModelVariableName + " = model;");
        addText("}");
        addText("");
        addText("return " + fieldModelVariableName + ";");



    }
}
