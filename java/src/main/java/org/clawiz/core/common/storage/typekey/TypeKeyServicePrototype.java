package org.clawiz.core.common.storage.typekey;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;

public class TypeKeyServicePrototype extends AbstractTypeService<TypeKeyObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).fullNameToId("org.clawiz.core.common.storage.TypeKey");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeKey"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      typeKey Checked object
    */
    public void check(TypeKeyObject typeKey) {
        
        if ( typeKey == null ) {
            throwException("Cannot check null ?", new Object[]{"TypeKeyObject"});
        }
        
        typeKey.fillDefaults();
        
        
        if ( typeNameToId(typeKey.getTypeId(), typeKey.getName(), typeKey.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "TypeKey", typeKey.toTypeName() });
        }
        
    }
    
    /**
    * Create new TypeKeyObject instance and fill default values
    * 
    * @return     Created object
    */
    public TypeKeyObject create() {
        
        TypeKeyObject typeKey = new TypeKeyObject();
        typeKey.setService((TypeKeyService) this);
        
        typeKey.fillDefaults();
        
        return typeKey;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public TypeKeyObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select type_id, name, description, is_unique, use_as_to_string from cw_core_type_keys where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"TypeKey", id});
        }
        
        TypeKeyObject result = new TypeKeyObject();
        
        result.setService((TypeKeyService) this);
        result.setId(id);
        result.setTypeId(statement.getBigDecimal(1));
        result.setName(statement.getString(2));
        result.setDescription(statement.getString(3));
        result.setUnique(("T".equals(statement.getString(4))));
        result.setUseAsToString(("T".equals(statement.getString(5))));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of TypeKeyObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public TypeKeyList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of TypeKeyObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public TypeKeyList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        TypeKeyList result = new TypeKeyList();
        
        
        Statement statement = executeQuery("select id, type_id, name, description, is_unique, use_as_to_string from cw_core_type_keys"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            TypeKeyObject object = new TypeKeyObject();
        
            object.setService((TypeKeyService) this);
            object.setId(statement.getBigDecimal(1));
            object.setTypeId(statement.getBigDecimal(2));
            object.setName(statement.getString(3));
            object.setDescription(statement.getString(4));
            object.setUnique(("T".equals(statement.getString(5))));
            object.setUseAsToString(("T".equals(statement.getString(6))));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'TypeName' fields
    * 
    * @param      typeId Type
    * @param      name   Name
    * @return     Id of found record or null
    */
    public BigDecimal typeNameToId(java.math.BigDecimal typeId, java.lang.String name) {
        return typeNameToId(typeId, name, null);
    }
    
    /**
    * Find id of record by key 'TypeName' fields with id not equal skipId
    * 
    * @param      typeId Type
    * @param      name   Name
    * @param      skipId Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal typeNameToId(java.math.BigDecimal typeId, java.lang.String name, BigDecimal skipId) {
        
        if ( typeId == null || name == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_type_keys where type_id = ? and upper_name = ?", typeId, name.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_type_keys where type_id = ? and upper_name = ? and id != ?", typeId, name.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'TypeName' fields or create new record with values set to given parameters
    * 
    * @param      typeId              Type
    * @param      name                Name
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal typeNameToId(java.math.BigDecimal typeId, java.lang.String name, boolean createNewIfNotFound) {
        
        BigDecimal id = typeNameToId(typeId, name, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        TypeKeyObject object = create();
        object.setTypeId(typeId);
        object.setName(name);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'TypeName' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToTypeName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select type_id, name from cw_core_type_keys where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        TypeKeyObject object = new TypeKeyObject();
        object.setService((TypeKeyService)this);
        object.setId(id);
        object.setTypeId(statement.getBigDecimal(1));
        object.setName(statement.getString(2));
        
        statement.close();
        
        return object.toTypeName();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToTypeName(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      typeKeyObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(TypeKeyObject typeKeyObject) {
        return typeKeyObject.toTypeName();
    }
    
    protected void saveAudit(TransactionAction transactionAction, TypeKeyObject oldTypeKeyObject, TypeKeyObject newTypeKeyObject) {
        
        TypeKeyObject o = oldTypeKeyObject != null ? oldTypeKeyObject : new TypeKeyObject();
        TypeKeyObject n = newTypeKeyObject != null ? newTypeKeyObject : new TypeKeyObject();
        
        
        executeUpdate("insert into a_cw_core_type_keys (scn, action_type, id , o_type_id, o_name, o_upper_name, o_description, o_is_unique, o_use_as_to_string, n_type_id, n_name, n_upper_name, n_description, n_is_unique, n_use_as_to_string) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getTypeId(), o.getName(), o.getName() != null ? o.getName().toUpperCase() : null, o.getDescription(), o.isUnique() ? "T" : "F", o.isUseAsToString() ? "T" : "F", n.getTypeId(), n.getName(), n.getName() != null ? n.getName().toUpperCase() : null, n.getDescription(), n.isUnique() ? "T" : "F", n.isUseAsToString() ? "T" : "F");
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      typeKey Saved object
    */
    public void save(TypeKeyObject typeKey) {
        
        if ( typeKey == null ) {
            throwException("Cannot save NULL ?", new Object[]{"TypeKeyObject"});
        }
        
        TransactionAction transactionAction;
        TypeKeyObject oldTypeKey;
        if ( typeKey.getService() == null ) {
            typeKey.setService((TypeKeyService)this);
        }
        
        check(typeKey);
        
        if ( typeKey.getId() == null ) {
        
            typeKey.setId(getObjectService().createObject(getTypeId()));
        
            oldTypeKey = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, typeKey.getId());
        
            executeUpdate("insert into cw_core_type_keys" 
                          + "( id, type_id, name, upper_name, description, is_unique, use_as_to_string ) "
                          + "values"
                          + "(?, ?, ?, ?, ?, ?, ?)",
                          typeKey.getId(), typeKey.getTypeId(), typeKey.getName(), ( typeKey.getName() != null ? typeKey.getName().toUpperCase() : null ), typeKey.getDescription(), typeKey.isUnique() ? "T" : "F", typeKey.isUseAsToString() ? "T" : "F" );
        
            if ( typeKey.getTypeId() != null ) { getObjectService().setLink(typeKey.getTypeId(), typeKey.getId()); }
        
        } else {
        
            oldTypeKey = load(typeKey.getId());
            if ( oldTypeKey.equals(typeKey) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, typeKey.getId());
        
            executeUpdate("update cw_core_type_keys set "
                          + "type_id = ?, name = ?, upper_name = ?, description = ?, is_unique = ?, use_as_to_string = ? "
                          + "where id = ?",
                          typeKey.getTypeId(), typeKey.getName(), ( typeKey.getName() != null ? typeKey.getName().toUpperCase() : null ), typeKey.getDescription(), typeKey.isUnique() ? "T" : "F", typeKey.isUseAsToString() ? "T" : "F", typeKey.getId() );
        
            getObjectService().changeLinkParent(oldTypeKey.getTypeId(), typeKey.getTypeId(), typeKey.getId());
        
        }
        
        saveAudit(transactionAction, oldTypeKey, typeKey);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        TypeKeyObject oldTypeKeyObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldTypeKeyObject, null);
        
        getObjectService().deleteObject(id);
        
        if (oldTypeKeyObject.getTypeId() != null) { getObjectService().deleteLink(oldTypeKeyObject.getTypeId(), id); }
        
        executeUpdate("delete from cw_core_type_keys where id = ?", id);
        
    }
}
