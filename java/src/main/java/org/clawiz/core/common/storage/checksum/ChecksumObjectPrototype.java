package org.clawiz.core.common.storage.checksum;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class ChecksumObjectPrototype extends AbstractObject {
    
    /**
    * Object
    */
    private java.math.BigDecimal objectId;
    
    /**
    * ChecksumType
    */
    private java.math.BigDecimal checksumTypeId;
    
    private org.clawiz.core.common.storage.checksumtype.ChecksumTypeObject checksumType;
    
    /**
    * Checksum
    */
    private java.lang.String checksum;
    
    public ChecksumService service;
    
    /**
    * 
    * @return     Object
    */
    public java.math.BigDecimal getObjectId() {
        return this.objectId;
    }
    
    /**
    * Set 'Object' value
    * 
    * @param      objectId Object
    */
    public void setObjectId(java.math.BigDecimal objectId) {
         this.objectId = objectId;
    }
    
    /**
    * Set 'Object' value and return this object
    * 
    * @param      objectId Object
    * @return     Checksum object
    */
    public ChecksumObjectPrototype withObjectId(java.math.BigDecimal objectId) {
        setObjectId(objectId);
        return this;
    }
    
    /**
    * 
    * @return     ChecksumType
    */
    public java.math.BigDecimal getChecksumTypeId() {
        return this.checksumTypeId;
    }
    
    public org.clawiz.core.common.storage.checksumtype.ChecksumTypeObject getChecksumType() {
        return getChecksumType(false);
    }
    
    @SuppressWarnings("Duplicates")
    public org.clawiz.core.common.storage.checksumtype.ChecksumTypeObject getChecksumType(boolean autoCreate) {
        
        if ( checksumType != null ) { return checksumType; }
        
        if ( checksumTypeId != null ) {
            checksumType = service.loadChecksumType(checksumTypeId);
        } else if ( autoCreate ) {
            checksumType = service.createChecksumType();
        }
        
        return checksumType;
    }
    
    /**
    * Set 'ChecksumType' value
    * 
    * @param      checksumTypeId ChecksumType
    */
    public void setChecksumTypeId(java.math.BigDecimal checksumTypeId) {
         this.checksumTypeId = checksumTypeId;
    }
    
    /**
    * Set 'ChecksumType' value and return this object
    * 
    * @param      checksumTypeId ChecksumType
    * @return     Checksum object
    */
    public ChecksumObjectPrototype withChecksumTypeId(java.math.BigDecimal checksumTypeId) {
        setChecksumTypeId(checksumTypeId);
        return this;
    }
    
    /**
    * 
    * @return     Checksum
    */
    public java.lang.String getChecksum() {
        return this.checksum;
    }
    
    /**
    * Set 'Checksum' value
    * 
    * @param      checksum Checksum
    */
    public void setChecksum(java.lang.String checksum) {
        
        if ( checksum != null && checksum.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "Checksum.Checksum", "250");
        }
        
         this.checksum = checksum;
    }
    
    /**
    * Set 'Checksum' value and return this object
    * 
    * @param      checksum Checksum
    * @return     Checksum object
    */
    public ChecksumObjectPrototype withChecksum(java.lang.String checksum) {
        setChecksum(checksum);
        return this;
    }
    
    public ChecksumService getService() {
        return this.service;
    }
    
    public void setService(ChecksumService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(ChecksumObjectPrototype  target) {
        target.setObjectId(getObjectId());
        target.setChecksumTypeId(getChecksumTypeId());
        target.setChecksum(getChecksum());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((ChecksumObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        if ( checksumType != null ) {
            checksumType.save();
            checksumTypeId = checksumType.getId();
        }
        service.save((ChecksumObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(ChecksumObjectPrototype object) {
        return 
               isObjectsEquals(this.getObjectId(), object.getObjectId() ) 
            && isObjectsEquals(this.getChecksumTypeId(), object.getChecksumTypeId() ) 
            && isObjectsEquals(this.getChecksum(), object.getChecksum() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getObjectId() != null ? getObjectId().hashCode() : 0);
        result = result * 31 + (getChecksumTypeId() != null ? getChecksumTypeId().hashCode() : 0);
        result = result * 31 + (getChecksum() != null ? getChecksum().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'ObjectChecksumType' fields
    * 
    * @return     Concatenated string values of key 'toObjectChecksumType' fields
    */
    public String toObjectChecksumType() {
        return service.getObjectService().idToString(getObjectId()) + "," + service.getObjectService().idToString(getChecksumTypeId());
    }
}
