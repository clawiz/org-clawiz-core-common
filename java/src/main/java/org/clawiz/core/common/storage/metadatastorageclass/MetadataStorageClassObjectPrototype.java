package org.clawiz.core.common.storage.metadatastorageclass;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class MetadataStorageClassObjectPrototype extends AbstractObject {
    
    /**
    * Name
    */
    private java.lang.String name;
    
    public MetadataStorageClassService service;
    
    /**
    * 
    * @return     Name
    */
    public java.lang.String getName() {
        return this.name;
    }
    
    /**
    * Set 'Name' value
    * 
    * @param      name Name
    */
    public void setName(java.lang.String name) {
        
        if ( name != null && name.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "MetadataStorageClass.Name", "250");
        }
        
         this.name = name;
    }
    
    /**
    * Set 'Name' value and return this object
    * 
    * @param      name Name
    * @return     MetadataStorageClass object
    */
    public MetadataStorageClassObjectPrototype withName(java.lang.String name) {
        setName(name);
        return this;
    }
    
    public MetadataStorageClassService getService() {
        return this.service;
    }
    
    public void setService(MetadataStorageClassService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(MetadataStorageClassObjectPrototype  target) {
        target.setName(getName());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((MetadataStorageClassObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        service.save((MetadataStorageClassObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(MetadataStorageClassObjectPrototype object) {
        return 
               isObjectsEquals(this.getName(), object.getName() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getName() != null ? getName().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'Name' fields
    * 
    * @return     Concatenated string values of key 'toName' fields
    */
    public String toName() {
        return getName();
    }
}
