/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.object;

import org.clawiz.core.common.Core;
import org.clawiz.core.common.CoreException;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * (C) Clawiz
 * User: abdrashitovta
 * Date: 14.11.2005
 * Time: 8?8?6
 * To change this template use File | Settings | File Templates.
 */
public class AbstractObjectList <T extends AbstractObject> extends ArrayList<T> {

    public T get(int index)
    {
      return super.get(index);
    }

    public T getById(BigDecimal id) {

        if ( id == null ) {
            return null;
        }

        for ( T obj : this) {
            if ( id.equals(obj.getId())) {
                return obj;
            }
        }

        return null;
    }

    public int idToIndex(BigDecimal id) {
        if ( id == null ) {
            return  -1;
        }
        for (int i=0; i < this.size(); i++) {
            if ( id.equals(get(i).getId())) {
                return i;
            }
        }
        return  -1;
    }

    public void remove( T object ) {
        if ( object == null ) {
            throw new CoreException("Cannot remove NULL object from list");
        }
        removeById(object.getId());
    }

    public void removeById(BigDecimal id) {
        if ( id == null ) {
            throw new CoreException("Cannot remove object with NULL id from list");
        }
        int index = idToIndex(id);
        if ( index == -1 ) {
            throw new CoreException("List not contain object with id ?", id);
        }
        super.remove(index);
    }
    
    public BigDecimal[] toIdArray() {
        BigDecimal[] result = new BigDecimal[size()];
        for ( int i = 0; i<size(); i++ ) {
            result[i] = get(i).getId();
        }
        return result;
    }

}
