package org.clawiz.core.common.metadata.data.type.property.extension;

import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractExtensionTypePropertyPrototype extends org.clawiz.core.common.metadata.data.type.property.AbstractTypeProperty {
    
    @ExchangeElement
    private org.clawiz.core.common.metadata.data.type.property.extension.method.AbstractExtensionTypePropertyMethod method;
    
    @ExchangeElement
    private org.clawiz.core.common.metadata.data.type.property.extension.action.AbstractExtensionTypePropertyAction action;
    
    public AbstractExtensionTypeProperty withName(String value) {
        setName(value);
        return (AbstractExtensionTypeProperty) this;
    }
    
    public org.clawiz.core.common.metadata.data.type.property.extension.method.AbstractExtensionTypePropertyMethod getMethod() {
        return this.method;
    }
    
    public void setMethod(org.clawiz.core.common.metadata.data.type.property.extension.method.AbstractExtensionTypePropertyMethod value) {
        this.method = value;
    }
    
    public AbstractExtensionTypeProperty withMethod(org.clawiz.core.common.metadata.data.type.property.extension.method.AbstractExtensionTypePropertyMethod value) {
        setMethod(value);
        return (AbstractExtensionTypeProperty) this;
    }
    
    public <T extends org.clawiz.core.common.metadata.data.type.property.extension.method.AbstractExtensionTypePropertyMethod> T createMethod(Class<T> nodeClass) {
        if ( getMethod() != null ) {
            throwException("Cannot create already defined value of '?' for '?'", "method", this.getFullName());
        }
        org.clawiz.core.common.metadata.data.type.property.extension.method.AbstractExtensionTypePropertyMethod value = createChildNode(nodeClass, "method");
        setMethod(value);
        return (T) value;
    }
    
    public org.clawiz.core.common.metadata.data.type.property.extension.method.AbstractExtensionTypePropertyMethod createMethod() {
        return createMethod(org.clawiz.core.common.metadata.data.type.property.extension.method.AbstractExtensionTypePropertyMethod.class);
    }
    
    public org.clawiz.core.common.metadata.data.type.property.extension.action.AbstractExtensionTypePropertyAction getAction() {
        return this.action;
    }
    
    public void setAction(org.clawiz.core.common.metadata.data.type.property.extension.action.AbstractExtensionTypePropertyAction value) {
        this.action = value;
    }
    
    public AbstractExtensionTypeProperty withAction(org.clawiz.core.common.metadata.data.type.property.extension.action.AbstractExtensionTypePropertyAction value) {
        setAction(value);
        return (AbstractExtensionTypeProperty) this;
    }
    
    public <T extends org.clawiz.core.common.metadata.data.type.property.extension.action.AbstractExtensionTypePropertyAction> T createAction(Class<T> nodeClass) {
        if ( getAction() != null ) {
            throwException("Cannot create already defined value of '?' for '?'", "action", this.getFullName());
        }
        org.clawiz.core.common.metadata.data.type.property.extension.action.AbstractExtensionTypePropertyAction value = createChildNode(nodeClass, "action");
        setAction(value);
        return (T) value;
    }
    
    public org.clawiz.core.common.metadata.data.type.property.extension.action.AbstractExtensionTypePropertyAction createAction() {
        return createAction(org.clawiz.core.common.metadata.data.type.property.extension.action.AbstractExtensionTypePropertyAction.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getMethod() != null ) { 
            getMethod().prepare(session);
        }
        if ( getAction() != null ) { 
            getAction().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getMethod());
        
        references.add(getAction());
        
    }
}
