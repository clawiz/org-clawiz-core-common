/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.metadata.node;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.loader.MetadataLoaderList;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class MetadataNodeList<T extends MetadataNode> implements Iterable<T>, Serializable {

    ArrayList<T>                 nodes        = new ArrayList<>();
    HashMap<String, T>           namesCache   = new HashMap<>();
    HashMap<BigDecimal, T>       idCache      = new HashMap<>();
    HashMap<Class, ArrayList<T>> classesCahce = new HashMap<>();

    public int size() {
        return nodes.size();
    }

    public T get(int index) {
        return nodes.get(index);
    }

    public T get(BigDecimal id) {
        if ( id == null ) {
            return null;
        }
        T node = idCache.get(id);
        if ( node != null ) {
            return node;
        }
        for(T _node : nodes ) {
            if ( id.equals(_node.getId())) {
                idCache.put(id, _node);
                return _node;
            }
        }
        return null;
    }

    public T get(String name) {

        if ( name == null ) {
            return null;
        }
        T node = namesCache.get(name);
        if ( node != null ) {
            return node;
        }

        for(T _node : nodes ) {
            if ( name.equalsIgnoreCase(_node.getName())) {
                namesCache.put(name, _node);
                return _node;
            }
        }

        return null;
    }

/*
    public T getReferenced(T sourceNode) {
        Class<T> clazz     = (Class<T>) sourceNode.getClass();
        String packageName = sourceNode.getPackageName();
        String name        = sourceNode.getName();
        if ( clazz == null ) {
            throw new CoreException("Cannot get node of null class");
        }
        if ( packageName == null ) {
            throw new CoreException("Cannot get node ? with null package name", clazz.getName());
        }
        if ( name == null ) {
            throw new CoreException("Cannot get node ? with null name", clazz.getName());
        }
        for (MetadataNode node : this ) {
            if ( clazz.isAssignableFrom(node.getClass())) {
                if ( packageName.equals(node.getPackageName()) && name.equals(node.getName()) ) {
                    return (T) node;
                }
            }
        }

        T node = lookupMetadataBase.getNode(clazz, packageName, name, false);

        if ( node != null ) {
            return node;
        }

        throw new CoreException("Node ?.? of class ? not found in parser context", packageName, name, clazz.getName());
    }
*/


    public void sort() {

        ArrayList<T> result = new ArrayList<>();

        HashMap<MetadataNode, MetadataNodeReferencesList> referencesCache = new HashMap<>();

        for(T node : this) {
            MetadataNodeReferencesList referencesList = new MetadataNodeReferencesList();
            referencesList.setLookupList(this);
            node.fillReferences(referencesList);
            result.add(node);
            referencesCache.put(node, referencesList);
        }

        for (int loop = 0; loop < result.size(); loop++) {
            for ( int i1=0; i1 < result.size()-1; i1++) {
                for ( int i2=i1+1; i2 < result.size(); i2++) {
                    T n1 = result.get(i1);
                    T n2 = result.get(i2);

                    if ( referencesCache.get(n1).containNode(n2) ) {
                        result.set(i1, n2);
                        result.set(i2, n1);
                    }

                }
            }
        }

        nodes = result;
    }

    public void clearCaches() {
        namesCache.clear();
        idCache.clear();
        classesCahce.clear();
    }

    public void add(T node) {
        if ( node == null ) {
            throw new CoreException("Cannot add null node to ?", getClass().getName());
        }
        nodes.add(node);
        clearCaches();
    }

    public void remove(T node) {
        if ( node == null ) {
            throw new CoreException("Cannot remove NULL node");
        }
        int index = -1;
        for (int i=0; i < nodes.size(); i++) {
            T checkNode = nodes.get(i);
            if ( checkNode == node ||
                    (checkNode.getId() != null && node.getId() != null && node.getId().equals(checkNode.getId())) ) {
                index = i;
                break;
            }
        }
        if ( index == -1 ) {
            throw new CoreException("Node ? not found in nodes list", node.getFullName());
        }
        nodes.remove(index);
    }

    public void addAll(ArrayList<T> nodes) {
        this.nodes.addAll(nodes);
        clearCaches();
    }

    public void addAll(MetadataNodeList<T> nodes) {
        for ( T node : nodes ) {
            this.nodes.add(node);
        }
        clearCaches();
    }

    public void set(int index, T node) {
        nodes.set(index, node);
    }

    public void remove(int index) {
        nodes.remove(index);
        clearCaches();
    }

    public void clear() {
        nodes.clear();
        clearCaches();
    }

    @Override
    public Iterator<T> iterator() {
        return nodes.iterator();
    }

    @Override
    public void forEach(Consumer<? super T> action) {
        nodes.forEach(action);
    }

    @Override
    public Spliterator<T> spliterator() {
        return nodes.spliterator();
    }

    public <C extends T> ArrayList<C> getNodeListByClass(Class<C> nodeClass) {
        if ( nodeClass == null ) {
            throw new CoreException("Cannot get nodes by NULL class");
        }
        ArrayList<T> result = classesCahce.get(nodeClass);
        if ( result != null ) {
            return (ArrayList<C>) result;
        }
        result = new ArrayList<>();
        for ( T node : this ) {
            if ( nodeClass.isAssignableFrom(node.getClass())) {
                result.add(node);
            }
        }
        classesCahce.put(nodeClass, result);
        return (ArrayList<C>) result;
    }

    public <C extends T> C getNodeByClass(Class<C> propertyClass) {
        ArrayList<C> list = getNodeListByClass(propertyClass);
        if ( list.size() == 0 ) {
            return null;
        }
        if ( list.size() == 1 ) {
            return list.get(0);
        }
        throw new CoreException("Node has more then one property of class ?", propertyClass.getName());
    }

}
