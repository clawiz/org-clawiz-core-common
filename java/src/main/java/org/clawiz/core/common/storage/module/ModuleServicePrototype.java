package org.clawiz.core.common.storage.module;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;

public class ModuleServicePrototype extends AbstractTypeService<ModuleObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.core.common.storage", "Module");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Module"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      module Checked object
    */
    public void check(ModuleObject module) {
        
        if ( module == null ) {
            throwException("Cannot check null ?", new Object[]{"ModuleObject"});
        }
        
        module.fillDefaults();
        
        
        if ( packageNameToId(module.getPackageName(), module.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "Module", module.toPackageName() });
        }
        
        if ( moduleClassNameToId(module.getModuleClassName(), module.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "Module", module.toModuleClassName() });
        }
        
    }
    
    /**
    * Create new ModuleObject instance and fill default values
    * 
    * @return     Created object
    */
    public ModuleObject create() {
        
        ModuleObject module = new ModuleObject();
        module.setService((ModuleService) this);
        
        module.fillDefaults();
        
        return module;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public ModuleObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select package_name, module_class_name, module_version from cw_core_modules where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"Module", id});
        }
        
        ModuleObject result = new ModuleObject();
        
        result.setService((ModuleService) this);
        result.setId(id);
        result.setPackageName(statement.getString(1));
        result.setModuleClassName(statement.getString(2));
        result.setModuleVersion(statement.getString(3));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of ModuleObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public ModuleList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of ModuleObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public ModuleList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        ModuleList result = new ModuleList();
        
        
        Statement statement = executeQuery("select id, package_name, module_class_name, module_version from cw_core_modules"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            ModuleObject object = new ModuleObject();
        
            object.setService((ModuleService) this);
            object.setId(statement.getBigDecimal(1));
            object.setPackageName(statement.getString(2));
            object.setModuleClassName(statement.getString(3));
            object.setModuleVersion(statement.getString(4));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'PackageName' fields
    * 
    * @param      packageName PackageName
    * @return     Id of found record or null
    */
    public BigDecimal packageNameToId(java.lang.String packageName) {
        return packageNameToId(packageName, null);
    }
    
    /**
    * Find id of record by key 'PackageName' fields with id not equal skipId
    * 
    * @param      packageName PackageName
    * @param      skipId      Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal packageNameToId(java.lang.String packageName, BigDecimal skipId) {
        
        if ( packageName == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_modules where upper_package_name = ?", packageName.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_modules where upper_package_name = ? and id != ?", packageName.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'PackageName' fields or create new record with values set to given parameters
    * 
    * @param      packageName         PackageName
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal packageNameToId(java.lang.String packageName, boolean createNewIfNotFound) {
        
        BigDecimal id = packageNameToId(packageName, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        ModuleObject object = create();
        object.setPackageName(packageName);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'PackageName' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToPackageName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select package_name from cw_core_modules where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        ModuleObject object = new ModuleObject();
        object.setService((ModuleService)this);
        object.setId(id);
        object.setPackageName(statement.getString(1));
        
        statement.close();
        
        return object.toPackageName();
    }
    
    /**
    * Find id of record by key 'ModuleClassName' fields
    * 
    * @param      moduleClassName ModuleClassName
    * @return     Id of found record or null
    */
    public BigDecimal moduleClassNameToId(java.lang.String moduleClassName) {
        return moduleClassNameToId(moduleClassName, null);
    }
    
    /**
    * Find id of record by key 'ModuleClassName' fields with id not equal skipId
    * 
    * @param      moduleClassName ModuleClassName
    * @param      skipId          Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal moduleClassNameToId(java.lang.String moduleClassName, BigDecimal skipId) {
        
        if ( moduleClassName == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_modules where upper_module_class_name = ?", moduleClassName.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_modules where upper_module_class_name = ? and id != ?", moduleClassName.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'ModuleClassName' fields or create new record with values set to given parameters
    * 
    * @param      moduleClassName     ModuleClassName
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal moduleClassNameToId(java.lang.String moduleClassName, boolean createNewIfNotFound) {
        
        BigDecimal id = moduleClassNameToId(moduleClassName, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        ModuleObject object = create();
        object.setModuleClassName(moduleClassName);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'ModuleClassName' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToModuleClassName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select module_class_name from cw_core_modules where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        ModuleObject object = new ModuleObject();
        object.setService((ModuleService)this);
        object.setId(id);
        object.setModuleClassName(statement.getString(1));
        
        statement.close();
        
        return object.toModuleClassName();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToPackageName(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      moduleObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(ModuleObject moduleObject) {
        return moduleObject.toPackageName();
    }
    
    protected void saveAudit(TransactionAction transactionAction, ModuleObject oldModuleObject, ModuleObject newModuleObject) {
        
        ModuleObject o = oldModuleObject != null ? oldModuleObject : new ModuleObject();
        ModuleObject n = newModuleObject != null ? newModuleObject : new ModuleObject();
        
        
        executeUpdate("insert into a_cw_core_modules (scn, action_type, id , o_package_name, o_upper_package_name, o_module_class_name, o_upper_module_class_name, o_module_version, n_package_name, n_upper_package_name, n_module_class_name, n_upper_module_class_name, n_module_version) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getPackageName(), o.getPackageName() != null ? o.getPackageName().toUpperCase() : null, o.getModuleClassName(), o.getModuleClassName() != null ? o.getModuleClassName().toUpperCase() : null, o.getModuleVersion(), n.getPackageName(), n.getPackageName() != null ? n.getPackageName().toUpperCase() : null, n.getModuleClassName(), n.getModuleClassName() != null ? n.getModuleClassName().toUpperCase() : null, n.getModuleVersion());
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      module Saved object
    */
    public void save(ModuleObject module) {
        
        if ( module == null ) {
            throwException("Cannot save NULL ?", new Object[]{"ModuleObject"});
        }
        
        TransactionAction transactionAction;
        ModuleObject oldModule;
        if ( module.getService() == null ) {
            module.setService((ModuleService)this);
        }
        
        check(module);
        
        if ( module.getId() == null ) {
        
            module.setId(getObjectService().createObject(getTypeId()));
        
            oldModule = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, module.getId());
        
            executeUpdate("insert into cw_core_modules" 
                          + "( id, package_name, upper_package_name, module_class_name, upper_module_class_name, module_version ) "
                          + "values"
                          + "(?, ?, ?, ?, ?, ?)",
                          module.getId(), module.getPackageName(), ( module.getPackageName() != null ? module.getPackageName().toUpperCase() : null ), module.getModuleClassName(), ( module.getModuleClassName() != null ? module.getModuleClassName().toUpperCase() : null ), module.getModuleVersion() );
        
        } else {
        
            oldModule = load(module.getId());
            if ( oldModule.equals(module) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, module.getId());
        
            executeUpdate("update cw_core_modules set "
                          + "package_name = ?, upper_package_name = ?, module_class_name = ?, upper_module_class_name = ?, module_version = ? "
                          + "where id = ?",
                          module.getPackageName(), ( module.getPackageName() != null ? module.getPackageName().toUpperCase() : null ), module.getModuleClassName(), ( module.getModuleClassName() != null ? module.getModuleClassName().toUpperCase() : null ), module.getModuleVersion(), module.getId() );
        
        }
        
        saveAudit(transactionAction, oldModule, module);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        ModuleObject oldModuleObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldModuleObject, null);
        
        getObjectService().deleteObject(id);
        
        executeUpdate("delete from cw_core_modules where id = ?", id);
        
    }
}
