package org.clawiz.core.common.system.generator.java.component.element;

import org.clawiz.core.common.system.generator.file.component.element.StringTextElement;
import org.clawiz.core.common.system.generator.java.component.AbstractJavaClassComponent;

import java.util.ArrayList;

public class JavaAnnotationElement extends StringTextElement {

    Class  annotationClass;

    public class Value {
        String name;
        Object value;
    }

    ArrayList<Value> values = new ArrayList<>();

    public void addValue(String name) {
        Value element = new Value();
        element.name = name;
        values.add(element);
    }

    public void addValue(String name, Object value) {
        Value element = new Value();
        element.name  = name;
        element.value = value;
        values.add(element);
    }
    
    public JavaAnnotationElement withValue(String name) {
        addValue(name);
        return this;
    }

    public JavaAnnotationElement withValue(String name, Object value) {
        addValue(name, value);
        return this;
    }

    public Class getAnnotationClass() {
        return annotationClass;
    }

    public void setAnnotationClass(Class annotationClass) {
        this.annotationClass = annotationClass;
    }

    @Override
    public void write() {
        StringBuilder sb = new StringBuilder();

        String className = getAnnotationClass().getName();
        if ( getComponent() instanceof AbstractJavaClassComponent
                && ((AbstractJavaClassComponent) getComponent()).isImportExists(annotationClass)) {
            className = getAnnotationClass().getSimpleName();
        }
        sb.append("@").append(className);


        if ( values.size() > 0 ) {
            String suffix = "";
            sb.append("(");

            for ( Value v : values ) {

                sb.append(v.name != null ? v.name : "");

                if ( v.value != null ) {
                    sb.append(v.name != null ? " = " : "");
                    if ( v.value instanceof String ) {
                        sb.append("\"" + v.value + "\"");
                    } else if ( v.value instanceof Boolean ) {
                        sb.append(v.value.toString());
                    } else {
                        throwException("Wrong annotation ? element ? value  '?' class ?", getAnnotationClass().getSimpleName(), v.name, v.value, v.value.getClass().getName());
                    }
                }
                sb.append(suffix);
                suffix = ", ";
            }
            sb.append(")");
        }
        pln(sb.toString());
    }
}
