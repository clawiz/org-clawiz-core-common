/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.metadata.data.type;

import org.clawiz.core.common.metadata.install.AbstractMetadataNodeInstaller;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;
import org.clawiz.core.common.metadata.data.type.property.extension.AbstractExtensionTypeProperty;
import org.clawiz.core.common.metadata.data.type.property.extension.TypeExtensionList;
import org.clawiz.core.common.metadata.data.type.property.extension.action.CallServiceMethodExtensionTypePropertyAction;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.type.field.TypeFieldList;
import org.clawiz.core.common.metadata.data.type.key.TypeKey;
import org.clawiz.core.common.metadata.data.type.key.TypeKeyList;
import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.system.type.install.TypeInstaller;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;
import org.clawiz.core.common.utils.StringUtils;


public class Type extends TypePrototype {

    @Override
    public <T extends AbstractMetadataNodeInstaller> Class<T> getInstallerClass() {
        return (Class<T>) TypeInstaller.class;
    }


    public String getServicePackageName() {
        return getPackageName() + "." + getJavaClassName().toLowerCase();
    }


    public boolean equals(Object object  ){
        Type type = (Type) object;
        if (this.getName().equalsIgnoreCase(type.getName()) &&
                this.getPackageName().equalsIgnoreCase(type.getPackageName())){
            return true;
        }
        return false;
    }

    @Override
    public void prepare(Session session) {
        super.prepare(session);
        if ( getJavaClassName() == null ) {
            setJavaName(GeneratorUtils.toJavaClassName(getName(), true));
        }
    }
}
