/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.database.metadata;


public class DatabaseTableColumn {

    public enum Type {
        VARCHAR, NUMBER, DATE, DATETIME, BLOB, CLOB;

    }

    public DatabaseTableColumn() {
    }

    public DatabaseTableColumn(String name, Type type, int precision, int scale, boolean required, String comment) {
        this.name      = name;
        this.type      = type;
        this.precision = precision;
        this.scale     = scale;
        this.required  = required;
        this.comment   = comment;
    }

    DatabaseTable   table;

    private String  name;

    private Type    type;

    private int     precision;

    private int     scale;

    private boolean required;

    private String comment;

    public DatabaseTable getTable() {
        return table;
    }

    public void setTable(DatabaseTable table) {
        this.table = table;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DatabaseTableColumn withName(String name) {
        this.name = name;
        return this;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public DatabaseTableColumn withType(Type type) {
        this.type = type;
        return this;
    }

    public int getPrecision() {
        return precision;
    }

    public void setPrecision(int precision) {
        this.precision = precision;
    }

    public DatabaseTableColumn withPrecision(int precision) {
        this.precision = precision;
        return this;
    }

    public int getScale() {
        return scale;
    }

    public void setScale(int scale) {
        this.scale = scale;
    }

    public DatabaseTableColumn withScale(int scale) {
        this.scale = scale;
        return this;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public DatabaseTableColumn withRequired(boolean required) {
        this.required = required;
        return this;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public DatabaseTableColumn withComment(String comment) {
        this.comment = comment;
        return this;
    }

    private String nvl(String str) {
        return str != null ? str : "";
    }

    public boolean equals(DatabaseTableColumn column) {
        if ( ! this.name.equalsIgnoreCase(column.getName())
                || this.getType() == null || column.getType() == null || this.getType() != column.getType()
                || this.isRequired() != column.isRequired()
                ) {
            return false;
        }
        if ( this.getType() == Type.BLOB
                || this.getType() == Type.DATE
                || this.getType() == Type.DATETIME
                || this.getType() == Type.CLOB
                ) {
            return true;
        }
        if( this.getPrecision() != column.getPrecision()
                || this.getScale() != column.getScale()
//                || ! nvl(this.getComment()).equals(nvl(column.getComment()))
                ) {
            return false;
        }

        return true;
    }

}
