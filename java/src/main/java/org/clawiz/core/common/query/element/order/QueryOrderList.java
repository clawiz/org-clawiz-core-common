/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.query.element.order;

import org.clawiz.core.common.query.element.AbstractQueryElement;
import org.clawiz.core.common.system.type.model.TypeFieldModel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class QueryOrderList extends AbstractQueryElement implements Iterable<AbstractQueryOrder> {

    ArrayList<AbstractQueryOrder> orders = new ArrayList<>();

    public int size() {
        return orders.size();
    }

    @Override
    public Iterator<AbstractQueryOrder> iterator() {
        return orders.iterator();
    }

    @Override
    public void forEach(Consumer<? super AbstractQueryOrder> action) {
        orders.forEach(action);
    }

    @Override
    public Spliterator<AbstractQueryOrder> spliterator() {
        return orders.spliterator();
    }

    public QueryOrderList column(TypeFieldModel fieldModel) {
        return column(fieldModel, QueryOrderDirection.ASC);
    }

    public QueryOrderList column(TypeFieldModel fieldModel, QueryOrderDirection direction) {
        QueryOrderTypeField order = addElement(QueryOrderTypeField.class);
        order.setFieldModel(fieldModel);
        order.setDirection(direction);
        orders.add(order);
        return this;
    }
}
