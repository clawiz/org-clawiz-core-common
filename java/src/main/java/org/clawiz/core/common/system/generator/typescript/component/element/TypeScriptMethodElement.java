/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.typescript.component.element;


import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: abdrashitovta
 * Date: 26.05.13
 * Time: 16?2
 * To change this template use File | Settings | File Templates.
 */
public class TypeScriptMethodElement extends TypeScriptMemberElement {

    public class Parameter {
        String  name;
        String  type;
        boolean nullable = false;

        public Parameter(String name, String type) {
            this.name = name;
            this.type = type;
        }

        public Parameter withNullable(boolean nullable) {
            this.nullable = nullable;
            return this;
        }
    }

    public enum MethodModifier {
        GET, SET
    }
    MethodModifier methodModifier;

    boolean _static = false;

    ArrayList<Parameter> parameters = new ArrayList<>();

    public MethodModifier getMethodModifier() {
        return methodModifier;
    }

    public void setMethodModifier(MethodModifier methodModifier) {
        this.methodModifier = methodModifier;
    }

    public TypeScriptMethodElement withMethodModifier(MethodModifier methodModifier) {
        this.methodModifier = methodModifier;
        return this;
    }

    public Parameter addParameter(String name, String type) {
        Parameter parameter = new Parameter(name, type);
        parameters.add(parameter);
        return parameter;
    }

    public boolean isStatic() {
        return _static;
    }

    public void setStatic(boolean _static) {
        this._static = _static;
    }

    @Override
    public void write() {

        StringBuilder pl = new StringBuilder();
        pl.append("");
        for ( int i=0; i < parameters.size(); i++) {
            if ( i > 0 ) {
                pl.append(", ");
            }
            Parameter p = parameters.get(i);
            pl.append(p.name
                    + (p.nullable ? "?" : "")
                    + (p.type != null ? " : " + p.type : "")
             );
        }

        pln(
                ( getAccessLevel() != AccessLevel.PUBLIC ? getAccessLevel().toString() + " " : "")
                        + ( _static ? "static " : "")
                        + (methodModifier == MethodModifier.GET ? "get " : "")
                        + (methodModifier == MethodModifier.SET ? "set " : "")
                        + getName()
                        + "(" + pl.toString() + ") "
                        + ( getType() != null ? " : " + getType() : "")
                        +" {"
        );


        writeElements();

        pln("}");

    }
}
