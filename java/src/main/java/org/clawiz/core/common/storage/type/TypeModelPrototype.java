package org.clawiz.core.common.storage.type;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;

public class TypeModelPrototype extends TypeModel {
    
    private static TypeField PACKAGE_NAME_FIELD;
    
    private static TypeField NAME_FIELD;
    
    private static TypeField JAVA_NAME_FIELD;
    
    private static TypeField DESCRIPTION_FIELD;
    
    private static TypeField TABLE_NAME_FIELD;
    
    private static Type type;
    
    private TypeService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public TypeService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(TypeService.class).getType();
        
        
        PACKAGE_NAME_FIELD = type.getFields().get("PackageName");
        if ( PACKAGE_NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Type.PackageName"}); }
        
        NAME_FIELD = type.getFields().get("Name");
        if ( NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Type.Name"}); }
        
        JAVA_NAME_FIELD = type.getFields().get("JavaName");
        if ( JAVA_NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Type.JavaName"}); }
        
        DESCRIPTION_FIELD = type.getFields().get("Description");
        if ( DESCRIPTION_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Type.Description"}); }
        
        TABLE_NAME_FIELD = type.getFields().get("TableName");
        if ( TABLE_NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Type.TableName"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _packageName;
    
    public TypeFieldModel packageName() {
        
        if ( _packageName != null ) {
            return _packageName;
        }
        
        _packageName = new TypeFieldModel(this, PACKAGE_NAME_FIELD);
        return _packageName;
        
    }
    
    private TypeFieldModel _name;
    
    public TypeFieldModel name() {
        
        if ( _name != null ) {
            return _name;
        }
        
        _name = new TypeFieldModel(this, NAME_FIELD);
        return _name;
        
    }
    
    private TypeFieldModel _javaName;
    
    public TypeFieldModel javaName() {
        
        if ( _javaName != null ) {
            return _javaName;
        }
        
        _javaName = new TypeFieldModel(this, JAVA_NAME_FIELD);
        return _javaName;
        
    }
    
    private TypeFieldModel _description;
    
    public TypeFieldModel description() {
        
        if ( _description != null ) {
            return _description;
        }
        
        _description = new TypeFieldModel(this, DESCRIPTION_FIELD);
        return _description;
        
    }
    
    private TypeFieldModel _tableName;
    
    public TypeFieldModel tableName() {
        
        if ( _tableName != null ) {
            return _tableName;
        }
        
        _tableName = new TypeFieldModel(this, TABLE_NAME_FIELD);
        return _tableName;
        
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "PACKAGENAME" : return packageName();
        case "NAME" : return name();
        case "JAVANAME" : return javaName();
        case "DESCRIPTION" : return description();
        case "TABLENAME" : return tableName();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
