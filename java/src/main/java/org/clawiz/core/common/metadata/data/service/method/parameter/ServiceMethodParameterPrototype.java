package org.clawiz.core.common.metadata.data.service.method.parameter;

import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ServiceMethodParameterPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeElement
    private org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType valueType;
    
    public ServiceMethodParameter withName(String value) {
        setName(value);
        return (ServiceMethodParameter) this;
    }
    
    public org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType getValueType() {
        return this.valueType;
    }
    
    public void setValueType(org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType value) {
        this.valueType = value;
    }
    
    public ServiceMethodParameter withValueType(org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType value) {
        setValueType(value);
        return (ServiceMethodParameter) this;
    }
    
    public <T extends org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType> T createValueType(Class<T> nodeClass) {
        if ( getValueType() != null ) {
            throwException("Cannot create already defined value of '?' for '?'", "valueType", this.getFullName());
        }
        org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType value = createChildNode(nodeClass, "valueType");
        setValueType(value);
        return (T) value;
    }
    
    public org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType createValueType() {
        return createValueType(org.clawiz.core.common.metadata.data.service.method.valuetype.AbstractServiceMethodValueType.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getValueType() != null ) { 
            getValueType().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getValueType());
        
    }
}
