package org.clawiz.core.common.metadata.data.service.method.valuetype;


public class ServiceMethodValueTypeString extends ServiceMethodValueTypeStringPrototype {

    @Override
    public Class getJavaClass() {
        return String.class;
    }
}
