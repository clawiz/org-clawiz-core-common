package org.clawiz.core.common.storage.remotenodeguid;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class RemoteNodeGuidObjectPrototype extends AbstractObject {
    
    /**
    * RemoteNode
    */
    private java.math.BigDecimal remoteNodeId;
    
    private org.clawiz.core.common.storage.remotenode.RemoteNodeObject remoteNode;
    
    /**
    * Object
    */
    private java.math.BigDecimal objectId;
    
    /**
    * NodeGuid
    */
    private java.lang.String nodeGuid;
    
    public RemoteNodeGuidService service;
    
    /**
    * 
    * @return     RemoteNode
    */
    public java.math.BigDecimal getRemoteNodeId() {
        return this.remoteNodeId;
    }
    
    public org.clawiz.core.common.storage.remotenode.RemoteNodeObject getRemoteNode() {
        return getRemoteNode(false);
    }
    
    @SuppressWarnings("Duplicates")
    public org.clawiz.core.common.storage.remotenode.RemoteNodeObject getRemoteNode(boolean autoCreate) {
        
        if ( remoteNode != null ) { return remoteNode; }
        
        if ( remoteNodeId != null ) {
            remoteNode = service.loadRemoteNode(remoteNodeId);
        } else if ( autoCreate ) {
            remoteNode = service.createRemoteNode();
        }
        
        return remoteNode;
    }
    
    /**
    * Set 'RemoteNode' value
    * 
    * @param      remoteNodeId RemoteNode
    */
    public void setRemoteNodeId(java.math.BigDecimal remoteNodeId) {
         this.remoteNodeId = remoteNodeId;
    }
    
    /**
    * Set 'RemoteNode' value and return this object
    * 
    * @param      remoteNodeId RemoteNode
    * @return     RemoteNodeGuid object
    */
    public RemoteNodeGuidObjectPrototype withRemoteNodeId(java.math.BigDecimal remoteNodeId) {
        setRemoteNodeId(remoteNodeId);
        return this;
    }
    
    /**
    * 
    * @return     Object
    */
    public java.math.BigDecimal getObjectId() {
        return this.objectId;
    }
    
    /**
    * Set 'Object' value
    * 
    * @param      objectId Object
    */
    public void setObjectId(java.math.BigDecimal objectId) {
         this.objectId = objectId;
    }
    
    /**
    * Set 'Object' value and return this object
    * 
    * @param      objectId Object
    * @return     RemoteNodeGuid object
    */
    public RemoteNodeGuidObjectPrototype withObjectId(java.math.BigDecimal objectId) {
        setObjectId(objectId);
        return this;
    }
    
    /**
    * 
    * @return     NodeGuid
    */
    public java.lang.String getNodeGuid() {
        return this.nodeGuid;
    }
    
    /**
    * Set 'NodeGuid' value
    * 
    * @param      nodeGuid NodeGuid
    */
    public void setNodeGuid(java.lang.String nodeGuid) {
        
        if ( nodeGuid != null && nodeGuid.length() > 50) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "RemoteNodeGuid.NodeGuid", "50");
        }
        
         this.nodeGuid = nodeGuid;
    }
    
    /**
    * Set 'NodeGuid' value and return this object
    * 
    * @param      nodeGuid NodeGuid
    * @return     RemoteNodeGuid object
    */
    public RemoteNodeGuidObjectPrototype withNodeGuid(java.lang.String nodeGuid) {
        setNodeGuid(nodeGuid);
        return this;
    }
    
    public RemoteNodeGuidService getService() {
        return this.service;
    }
    
    public void setService(RemoteNodeGuidService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(RemoteNodeGuidObjectPrototype  target) {
        target.setRemoteNodeId(getRemoteNodeId());
        target.setObjectId(getObjectId());
        target.setNodeGuid(getNodeGuid());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((RemoteNodeGuidObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        if ( remoteNode != null ) {
            remoteNode.save();
            remoteNodeId = remoteNode.getId();
        }
        service.save((RemoteNodeGuidObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(RemoteNodeGuidObjectPrototype object) {
        return 
               isObjectsEquals(this.getRemoteNodeId(), object.getRemoteNodeId() ) 
            && isObjectsEquals(this.getObjectId(), object.getObjectId() ) 
            && isObjectsEquals(this.getNodeGuid(), object.getNodeGuid() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getRemoteNodeId() != null ? getRemoteNodeId().hashCode() : 0);
        result = result * 31 + (getObjectId() != null ? getObjectId().hashCode() : 0);
        result = result * 31 + (getNodeGuid() != null ? getNodeGuid().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'RemoteNodeObject' fields
    * 
    * @return     Concatenated string values of key 'toRemoteNodeObject' fields
    */
    public String toRemoteNodeObject() {
        return service.getObjectService().idToString(getRemoteNodeId()) + "," + service.getObjectService().idToString(getObjectId());
    }
    
    /**
    * Prepare concatenated string values of key 'RemoteNodeGuid' fields
    * 
    * @return     Concatenated string values of key 'toRemoteNodeGuid' fields
    */
    public String toRemoteNodeGuid() {
        return service.getObjectService().idToString(getRemoteNodeId()) + "," + getNodeGuid();
    }
}
