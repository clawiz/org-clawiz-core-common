package org.clawiz.core.common.metadata.data.database.table;


import org.clawiz.core.common.metadata.install.AbstractMetadataNodeInstaller;
import org.clawiz.core.common.metadata.install.database.TableInstaller;

public class Table extends TablePrototype {

    @Override
    public <T extends AbstractMetadataNodeInstaller> Class<T> getInstallerClass() {
        return (Class<T>) TableInstaller.class;
    }
}
