/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.installer;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class InstallParameterList implements Iterable<InstallParameter> {

    private ArrayList<InstallParameter> parameters = new ArrayList<>();
    private HashMap<String, InstallParameter> parameterNamesCache = new HashMap<>();


    public String get(String name) {
        InstallParameter parameter = name != null ? parameterNamesCache.get(name.toUpperCase()) : null;
        return parameter != null ? parameter.getValue() : null;
    }

    public boolean getAsBoolean(String name) {
        return StringUtils.toBoolean(get(name));
    }

    public void set(String name, String value) {
        if (StringUtils.isEmpty(name ) ) {
            throw new CoreException("Install parameter name cannot be null or empty string");
        }
        String key = name.toUpperCase();
        InstallParameter parameter = parameterNamesCache.get(key);
        if ( parameter != null ) {
            parameter.setValue(value);
        } else {
            parameter = new InstallParameter(name, value);
            parameters.add(parameter);
            parameterNamesCache.put(name.toUpperCase(), parameter);
        }

    }

    public void addAll(InstallParameterList parameters) {
        for (InstallParameter parameter : parameters ) {
            set(parameter.getName(), parameter.getValue());
        }
    }

    @Override
    public Iterator<InstallParameter> iterator() {
        return parameters.iterator();
    }

    @Override
    public void forEach(Consumer<? super InstallParameter> action) {
        parameters.forEach(action);
    }

    @Override
    public Spliterator<InstallParameter> spliterator() {
        return parameters.spliterator();
    }
}
