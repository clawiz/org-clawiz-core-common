/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.database.metadata;

public class DatabaseTableIndex {

    private DatabaseTable                table;
    private String                       name;
    private DatabaseTableIndexColumnList columns = new DatabaseTableIndexColumnList(this);
    private boolean                      unique;

    public DatabaseTableIndex() {
    }

    public DatabaseTableIndex(String name, boolean unique) {
        this.name = name;
        this.unique = unique;
    }

    public DatabaseTable getTable() {
        return table;
    }

    public void setTable(DatabaseTable table) {
        this.table = table;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DatabaseTableIndexColumnList getColumns() {
        return columns;
    }

    public void addColumn(DatabaseTableIndexColumn column) {
        columns.add(column);
    }

    public boolean isUnique() {
        return unique;
    }

    public void setUnique(boolean unique) {
        this.unique = unique;
    }

    public String getColumnsString() {
        StringBuilder sb = new StringBuilder();
        for ( int i=0; i < columns.size(); i++ ){
            if ( i > 0 ) {
                sb.append(", ");
            }
            sb.append(columns.get(i).getName());
        }
        return sb.toString();
    }

    public boolean equals(DatabaseTableIndex checkIndex) {

       if ( this.unique != checkIndex.isUnique() ) {
           return false;
       }

       if (! this.columns.equals(checkIndex.getColumns()) ) {
           return false;
       }

       return true;
    }
}
