package org.clawiz.core.common.storage.typefieldvalue;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.typefield.TypeFieldService;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;
import org.clawiz.core.common.system.service.NotInitializeService;

public class TypeFieldValueModelPrototype extends TypeModel {
    
    private static TypeField TYPE_FIELD_ID_FIELD;
    
    private static TypeField ORDER_NO_FIELD;
    
    private static TypeField VALUE_FIELD;
    
    private static TypeField DESCRIPTION_FIELD;
    
    private static Type type;
    
    private TypeFieldValueService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public TypeFieldValueService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(TypeFieldValueService.class).getType();
        
        TypeFieldService typeFieldService = getService(TypeFieldService.class);
        
        TYPE_FIELD_ID_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeFieldValue.TypeField");
        if ( TYPE_FIELD_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeFieldValue.TypeField"}); }
        
        ORDER_NO_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeFieldValue.OrderNo");
        if ( ORDER_NO_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeFieldValue.OrderNo"}); }
        
        VALUE_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeFieldValue.Value");
        if ( VALUE_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeFieldValue.Value"}); }
        
        DESCRIPTION_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeFieldValue.Description");
        if ( DESCRIPTION_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeFieldValue.Description"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _typeFieldId;
    
    public TypeFieldModel typeFieldId() {
        
        if ( _typeFieldId != null ) {
            return _typeFieldId;
        }
        
        _typeFieldId = new TypeFieldModel(this, TYPE_FIELD_ID_FIELD);
        return _typeFieldId;
        
    }
    
    private TypeFieldModel _orderNo;
    
    public TypeFieldModel orderNo() {
        
        if ( _orderNo != null ) {
            return _orderNo;
        }
        
        _orderNo = new TypeFieldModel(this, ORDER_NO_FIELD);
        return _orderNo;
        
    }
    
    private TypeFieldModel _value;
    
    public TypeFieldModel value() {
        
        if ( _value != null ) {
            return _value;
        }
        
        _value = new TypeFieldModel(this, VALUE_FIELD);
        return _value;
        
    }
    
    private TypeFieldModel _description;
    
    public TypeFieldModel description() {
        
        if ( _description != null ) {
            return _description;
        }
        
        _description = new TypeFieldModel(this, DESCRIPTION_FIELD);
        return _description;
        
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "TYPEFIELD" : return typeFieldId();
        case "ORDERNO" : return orderNo();
        case "VALUE" : return value();
        case "DESCRIPTION" : return description();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
