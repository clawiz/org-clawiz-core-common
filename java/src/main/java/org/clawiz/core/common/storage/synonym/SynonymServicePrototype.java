package org.clawiz.core.common.storage.synonym;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;
import org.clawiz.core.common.system.service.NotInitializeService;

public class SynonymServicePrototype extends AbstractTypeService<SynonymObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.core.common.storage", "Synonym");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Synonym"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      synonym Checked object
    */
    public void check(SynonymObject synonym) {
        
        if ( synonym == null ) {
            throwException("Cannot check null ?", new Object[]{"SynonymObject"});
        }
        
        synonym.fillDefaults();
        
        
        if ( objectSynonymTypeValueToId(synonym.getObjectId(), synonym.getSynonymTypeId(), synonym.getValue(), synonym.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "Synonym", synonym.toObjectSynonymTypeValue() });
        }
        
        if ( synonymTypeValueToId(synonym.getSynonymTypeId(), synonym.getValue(), synonym.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "Synonym", synonym.toSynonymTypeValue() });
        }
        
    }
    
    /**
    * Create new SynonymObject instance and fill default values
    * 
    * @return     Created object
    */
    public SynonymObject create() {
        
        SynonymObject synonym = new SynonymObject();
        synonym.setService((SynonymService) this);
        
        synonym.fillDefaults();
        
        return synonym;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public SynonymObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select object_id, synonym_type_id, value from cw_core_synonyms where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"Synonym", id});
        }
        
        SynonymObject result = new SynonymObject();
        
        result.setService((SynonymService) this);
        result.setId(id);
        result.setObjectId(statement.getBigDecimal(1));
        result.setSynonymTypeId(statement.getBigDecimal(2));
        result.setValue(statement.getString(3));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of SynonymObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public SynonymList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of SynonymObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public SynonymList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        SynonymList result = new SynonymList();
        
        
        Statement statement = executeQuery("select id, object_id, synonym_type_id, value from cw_core_synonyms"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            SynonymObject object = new SynonymObject();
        
            object.setService((SynonymService) this);
            object.setId(statement.getBigDecimal(1));
            object.setObjectId(statement.getBigDecimal(2));
            object.setSynonymTypeId(statement.getBigDecimal(3));
            object.setValue(statement.getString(4));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'ObjectSynonymTypeValue' fields
    * 
    * @param      objectId      Object
    * @param      synonymTypeId SynonymType
    * @param      value         Value
    * @return     Id of found record or null
    */
    public BigDecimal objectSynonymTypeValueToId(java.math.BigDecimal objectId, java.math.BigDecimal synonymTypeId, java.lang.String value) {
        return objectSynonymTypeValueToId(objectId, synonymTypeId, value, null);
    }
    
    /**
    * Find id of record by key 'ObjectSynonymTypeValue' fields with id not equal skipId
    * 
    * @param      objectId      Object
    * @param      synonymTypeId SynonymType
    * @param      value         Value
    * @param      skipId        Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal objectSynonymTypeValueToId(java.math.BigDecimal objectId, java.math.BigDecimal synonymTypeId, java.lang.String value, BigDecimal skipId) {
        
        if ( objectId == null || synonymTypeId == null || value == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_synonyms where object_id = ? and synonym_type_id = ? and upper_value = ?", objectId, synonymTypeId, value.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_synonyms where object_id = ? and synonym_type_id = ? and upper_value = ? and id != ?", objectId, synonymTypeId, value.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'ObjectSynonymTypeValue' fields or create new record with values set to given parameters
    * 
    * @param      objectId            Object
    * @param      synonymTypeId       SynonymType
    * @param      value               Value
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal objectSynonymTypeValueToId(java.math.BigDecimal objectId, java.math.BigDecimal synonymTypeId, java.lang.String value, boolean createNewIfNotFound) {
        
        BigDecimal id = objectSynonymTypeValueToId(objectId, synonymTypeId, value, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        SynonymObject object = create();
        object.setObjectId(objectId);
        object.setSynonymTypeId(synonymTypeId);
        object.setValue(value);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'ObjectSynonymTypeValue' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToObjectSynonymTypeValue(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select object_id, synonym_type_id, value from cw_core_synonyms where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        SynonymObject object = new SynonymObject();
        object.setService((SynonymService)this);
        object.setId(id);
        object.setObjectId(statement.getBigDecimal(1));
        object.setSynonymTypeId(statement.getBigDecimal(2));
        object.setValue(statement.getString(3));
        
        statement.close();
        
        return object.toObjectSynonymTypeValue();
    }
    
    /**
    * Find id of record by key 'SynonymTypeValue' fields
    * 
    * @param      synonymTypeId SynonymType
    * @param      value         Value
    * @return     Id of found record or null
    */
    public BigDecimal synonymTypeValueToId(java.math.BigDecimal synonymTypeId, java.lang.String value) {
        return synonymTypeValueToId(synonymTypeId, value, null);
    }
    
    /**
    * Find id of record by key 'SynonymTypeValue' fields with id not equal skipId
    * 
    * @param      synonymTypeId SynonymType
    * @param      value         Value
    * @param      skipId        Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal synonymTypeValueToId(java.math.BigDecimal synonymTypeId, java.lang.String value, BigDecimal skipId) {
        
        if ( synonymTypeId == null || value == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_synonyms where synonym_type_id = ? and upper_value = ?", synonymTypeId, value.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_synonyms where synonym_type_id = ? and upper_value = ? and id != ?", synonymTypeId, value.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'SynonymTypeValue' fields or create new record with values set to given parameters
    * 
    * @param      synonymTypeId       SynonymType
    * @param      value               Value
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal synonymTypeValueToId(java.math.BigDecimal synonymTypeId, java.lang.String value, boolean createNewIfNotFound) {
        
        BigDecimal id = synonymTypeValueToId(synonymTypeId, value, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        SynonymObject object = create();
        object.setSynonymTypeId(synonymTypeId);
        object.setValue(value);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'SynonymTypeValue' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToSynonymTypeValue(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select synonym_type_id, value from cw_core_synonyms where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        SynonymObject object = new SynonymObject();
        object.setService((SynonymService)this);
        object.setId(id);
        object.setSynonymTypeId(statement.getBigDecimal(1));
        object.setValue(statement.getString(2));
        
        statement.close();
        
        return object.toSynonymTypeValue();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToObjectSynonymTypeValue(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      synonymObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(SynonymObject synonymObject) {
        return synonymObject.toObjectSynonymTypeValue();
    }
    
    protected void saveAudit(TransactionAction transactionAction, SynonymObject oldSynonymObject, SynonymObject newSynonymObject) {
        
        SynonymObject o = oldSynonymObject != null ? oldSynonymObject : new SynonymObject();
        SynonymObject n = newSynonymObject != null ? newSynonymObject : new SynonymObject();
        
        
        executeUpdate("insert into a_cw_core_synonyms (scn, action_type, id , o_object_id, o_synonym_type_id, o_value, o_upper_value, n_object_id, n_synonym_type_id, n_value, n_upper_value) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getObjectId(), o.getSynonymTypeId(), o.getValue(), o.getValue() != null ? o.getValue().toUpperCase() : null, n.getObjectId(), n.getSynonymTypeId(), n.getValue(), n.getValue() != null ? n.getValue().toUpperCase() : null);
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      synonym Saved object
    */
    public void save(SynonymObject synonym) {
        
        if ( synonym == null ) {
            throwException("Cannot save NULL ?", new Object[]{"SynonymObject"});
        }
        
        TransactionAction transactionAction;
        SynonymObject oldSynonym;
        if ( synonym.getService() == null ) {
            synonym.setService((SynonymService)this);
        }
        
        check(synonym);
        
        if ( synonym.getId() == null ) {
        
            synonym.setId(getObjectService().createObject(getTypeId()));
        
            oldSynonym = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, synonym.getId());
        
            executeUpdate("insert into cw_core_synonyms" 
                          + "( id, object_id, synonym_type_id, value, upper_value ) "
                          + "values"
                          + "(?, ?, ?, ?, ?)",
                          synonym.getId(), synonym.getObjectId(), synonym.getSynonymTypeId(), synonym.getValue(), ( synonym.getValue() != null ? synonym.getValue().toUpperCase() : null ) );
        
            if ( synonym.getObjectId() != null ) { getObjectService().setLink(synonym.getObjectId(), synonym.getId()); }
            if ( synonym.getSynonymTypeId() != null ) { getObjectService().setLink(synonym.getSynonymTypeId(), synonym.getId()); }
        
        } else {
        
            oldSynonym = load(synonym.getId());
            if ( oldSynonym.equals(synonym) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, synonym.getId());
        
            executeUpdate("update cw_core_synonyms set "
                          + "object_id = ?, synonym_type_id = ?, value = ?, upper_value = ? "
                          + "where id = ?",
                          synonym.getObjectId(), synonym.getSynonymTypeId(), synonym.getValue(), ( synonym.getValue() != null ? synonym.getValue().toUpperCase() : null ), synonym.getId() );
        
            getObjectService().changeLinkParent(oldSynonym.getObjectId(), synonym.getObjectId(), synonym.getId());
            getObjectService().changeLinkParent(oldSynonym.getSynonymTypeId(), synonym.getSynonymTypeId(), synonym.getId());
        
        }
        
        saveAudit(transactionAction, oldSynonym, synonym);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        SynonymObject oldSynonymObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldSynonymObject, null);
        
        getObjectService().deleteObject(id);
        
        if (oldSynonymObject.getObjectId() != null) { getObjectService().deleteLink(oldSynonymObject.getObjectId(), id); }
        if (oldSynonymObject.getSynonymTypeId() != null) { getObjectService().deleteLink(oldSynonymObject.getSynonymTypeId(), id); }
        
        executeUpdate("delete from cw_core_synonyms where id = ?", id);
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.synonymtype.SynonymTypeService synonymTypeService;
    
    public org.clawiz.core.common.storage.synonymtype.SynonymTypeService getSynonymTypeService() {
        if ( synonymTypeService == null ) {
            synonymTypeService = getService(org.clawiz.core.common.storage.synonymtype.SynonymTypeService.class);
        }
        return synonymTypeService;
    }
    
    public org.clawiz.core.common.storage.synonymtype.SynonymTypeObject createSynonymType() {
        return getSynonymTypeService().create();
    }
    
    public org.clawiz.core.common.storage.synonymtype.SynonymTypeObject loadSynonymType(BigDecimal id) {
        return getSynonymTypeService().load(id);
    }
}
