package org.clawiz.core.common.storage.user;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class UserObjectPrototype extends AbstractObject {
    
    /**
    * Name
    */
    private java.lang.String name;
    
    /**
    * Password
    */
    private java.lang.String password;
    
    /**
    * PasswordHash
    */
    private java.lang.String passwordHash;
    
    /**
    * PreviousPassword
    */
    private java.lang.String previousPassword;
    
    /**
    * email
    */
    private java.lang.String email;
    
    /**
    * FirstName
    */
    private java.lang.String firstName;
    
    /**
    * LastName
    */
    private java.lang.String lastName;
    
    public UserService service;
    
    /**
    * 
    * @return     Name
    */
    public java.lang.String getName() {
        return this.name;
    }
    
    /**
    * Set 'Name' value
    * 
    * @param      name Name
    */
    public void setName(java.lang.String name) {
        
        if ( name != null && name.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "User.Name", "250");
        }
        
         this.name = name;
    }
    
    /**
    * Set 'Name' value and return this object
    * 
    * @param      name Name
    * @return     User object
    */
    public UserObjectPrototype withName(java.lang.String name) {
        setName(name);
        return this;
    }
    
    /**
    * 
    * @return     Password
    */
    public java.lang.String getPassword() {
        return this.password;
    }
    
    /**
    * Set 'Password' value
    * 
    * @param      password Password
    */
    public void setPassword(java.lang.String password) {
        
        if ( password != null && password.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "User.Password", "250");
        }
        
         this.password = password;
    }
    
    /**
    * Set 'Password' value and return this object
    * 
    * @param      password Password
    * @return     User object
    */
    public UserObjectPrototype withPassword(java.lang.String password) {
        setPassword(password);
        return this;
    }
    
    /**
    * 
    * @return     PasswordHash
    */
    public java.lang.String getPasswordHash() {
        return this.passwordHash;
    }
    
    /**
    * Set 'PasswordHash' value
    * 
    * @param      passwordHash PasswordHash
    */
    public void setPasswordHash(java.lang.String passwordHash) {
        
        if ( passwordHash != null && passwordHash.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "User.PasswordHash", "250");
        }
        
         this.passwordHash = passwordHash;
    }
    
    /**
    * Set 'PasswordHash' value and return this object
    * 
    * @param      passwordHash PasswordHash
    * @return     User object
    */
    public UserObjectPrototype withPasswordHash(java.lang.String passwordHash) {
        setPasswordHash(passwordHash);
        return this;
    }
    
    /**
    * 
    * @return     PreviousPassword
    */
    public java.lang.String getPreviousPassword() {
        return this.previousPassword;
    }
    
    /**
    * Set 'PreviousPassword' value
    * 
    * @param      previousPassword PreviousPassword
    */
    public void setPreviousPassword(java.lang.String previousPassword) {
        
        if ( previousPassword != null && previousPassword.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "User.PreviousPassword", "250");
        }
        
         this.previousPassword = previousPassword;
    }
    
    /**
    * Set 'PreviousPassword' value and return this object
    * 
    * @param      previousPassword PreviousPassword
    * @return     User object
    */
    public UserObjectPrototype withPreviousPassword(java.lang.String previousPassword) {
        setPreviousPassword(previousPassword);
        return this;
    }
    
    /**
    * 
    * @return     email
    */
    public java.lang.String getEmail() {
        return this.email;
    }
    
    /**
    * Set 'email' value
    * 
    * @param      email email
    */
    public void setEmail(java.lang.String email) {
        
        if ( email != null && email.length() > 120) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "User.email", "120");
        }
        
         this.email = email;
    }
    
    /**
    * Set 'email' value and return this object
    * 
    * @param      email email
    * @return     User object
    */
    public UserObjectPrototype withEmail(java.lang.String email) {
        setEmail(email);
        return this;
    }
    
    /**
    * 
    * @return     FirstName
    */
    public java.lang.String getFirstName() {
        return this.firstName;
    }
    
    /**
    * Set 'FirstName' value
    * 
    * @param      firstName FirstName
    */
    public void setFirstName(java.lang.String firstName) {
        
        if ( firstName != null && firstName.length() > 120) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "User.FirstName", "120");
        }
        
         this.firstName = firstName;
    }
    
    /**
    * Set 'FirstName' value and return this object
    * 
    * @param      firstName FirstName
    * @return     User object
    */
    public UserObjectPrototype withFirstName(java.lang.String firstName) {
        setFirstName(firstName);
        return this;
    }
    
    /**
    * 
    * @return     LastName
    */
    public java.lang.String getLastName() {
        return this.lastName;
    }
    
    /**
    * Set 'LastName' value
    * 
    * @param      lastName LastName
    */
    public void setLastName(java.lang.String lastName) {
        
        if ( lastName != null && lastName.length() > 120) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "User.LastName", "120");
        }
        
         this.lastName = lastName;
    }
    
    /**
    * Set 'LastName' value and return this object
    * 
    * @param      lastName LastName
    * @return     User object
    */
    public UserObjectPrototype withLastName(java.lang.String lastName) {
        setLastName(lastName);
        return this;
    }
    
    public UserService getService() {
        return this.service;
    }
    
    public void setService(UserService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(UserObjectPrototype  target) {
        target.setName(getName());
        target.setPassword(getPassword());
        target.setPasswordHash(getPasswordHash());
        target.setPreviousPassword(getPreviousPassword());
        target.setEmail(getEmail());
        target.setFirstName(getFirstName());
        target.setLastName(getLastName());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((UserObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        service.save((UserObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(UserObjectPrototype object) {
        return 
               isObjectsEquals(this.getName(), object.getName() ) 
            && isObjectsEquals(this.getPassword(), object.getPassword() ) 
            && isObjectsEquals(this.getPasswordHash(), object.getPasswordHash() ) 
            && isObjectsEquals(this.getPreviousPassword(), object.getPreviousPassword() ) 
            && isObjectsEquals(this.getEmail(), object.getEmail() ) 
            && isObjectsEquals(this.getFirstName(), object.getFirstName() ) 
            && isObjectsEquals(this.getLastName(), object.getLastName() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getName() != null ? getName().hashCode() : 0);
        result = result * 31 + (getPassword() != null ? getPassword().hashCode() : 0);
        result = result * 31 + (getPasswordHash() != null ? getPasswordHash().hashCode() : 0);
        result = result * 31 + (getPreviousPassword() != null ? getPreviousPassword().hashCode() : 0);
        result = result * 31 + (getEmail() != null ? getEmail().hashCode() : 0);
        result = result * 31 + (getFirstName() != null ? getFirstName().hashCode() : 0);
        result = result * 31 + (getLastName() != null ? getLastName().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'name' fields
    * 
    * @return     Concatenated string values of key 'toName' fields
    */
    public String toName() {
        return getName();
    }
    
    /**
    * Prepare concatenated string values of key 'email' fields
    * 
    * @return     Concatenated string values of key 'toEmail' fields
    */
    public String toEmail() {
        return getEmail();
    }
}
