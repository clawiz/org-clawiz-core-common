package org.clawiz.core.common.metadata.data.language.node.element.field;

import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.data.language.node.element.Structure;
import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;

public class AbstractStructureField extends MetadataNode {

    Structure structure;

    @ExchangeAttribute
    boolean                   required = false;

    @ExchangeAttribute
    boolean                   array    = false;


    public Structure getStructure() {
        return structure;
    }

    public void setStructure(Structure structure) {
        this.structure = structure;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean isArray() {
        return array;
    }

    public void setArray(boolean array) {
        this.array = array;
    }

    public String getSetMethodName() {
        return "set" + getJavaClassName();
    }

    public String getGetMethodName() {
        return "get" + getJavaClassName();
    }

}
