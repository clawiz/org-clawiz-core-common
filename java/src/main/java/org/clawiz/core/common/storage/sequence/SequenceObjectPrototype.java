package org.clawiz.core.common.storage.sequence;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class SequenceObjectPrototype extends AbstractObject {
    
    /**
    * Name
    */
    private java.lang.String name;
    
    /**
    * Object1
    */
    private java.math.BigDecimal object1Id;
    
    /**
    * Object2
    */
    private java.math.BigDecimal object2Id;
    
    /**
    * Object3
    */
    private java.math.BigDecimal object3Id;
    
    /**
    * StartValue
    */
    private java.math.BigDecimal startValue;
    
    /**
    * MaxValue
    */
    private java.math.BigDecimal maxValue;
    
    /**
    * LastCachedValue
    */
    private java.math.BigDecimal lastCachedValue;
    
    /**
    * CacheLength
    */
    private java.math.BigDecimal cacheLength;
    
    public SequenceService service;
    
    /**
    * 
    * @return     Name
    */
    public java.lang.String getName() {
        return this.name;
    }
    
    /**
    * Set 'Name' value
    * 
    * @param      name Name
    */
    public void setName(java.lang.String name) {
        
        if ( name != null && name.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "Sequence.Name", "250");
        }
        
         this.name = name;
    }
    
    /**
    * Set 'Name' value and return this object
    * 
    * @param      name Name
    * @return     Sequence object
    */
    public SequenceObjectPrototype withName(java.lang.String name) {
        setName(name);
        return this;
    }
    
    /**
    * 
    * @return     Object1
    */
    public java.math.BigDecimal getObject1Id() {
        return this.object1Id;
    }
    
    /**
    * Set 'Object1' value
    * 
    * @param      object1Id Object1
    */
    public void setObject1Id(java.math.BigDecimal object1Id) {
         this.object1Id = object1Id;
    }
    
    /**
    * Set 'Object1' value and return this object
    * 
    * @param      object1Id Object1
    * @return     Sequence object
    */
    public SequenceObjectPrototype withObject1Id(java.math.BigDecimal object1Id) {
        setObject1Id(object1Id);
        return this;
    }
    
    /**
    * 
    * @return     Object2
    */
    public java.math.BigDecimal getObject2Id() {
        return this.object2Id;
    }
    
    /**
    * Set 'Object2' value
    * 
    * @param      object2Id Object2
    */
    public void setObject2Id(java.math.BigDecimal object2Id) {
         this.object2Id = object2Id;
    }
    
    /**
    * Set 'Object2' value and return this object
    * 
    * @param      object2Id Object2
    * @return     Sequence object
    */
    public SequenceObjectPrototype withObject2Id(java.math.BigDecimal object2Id) {
        setObject2Id(object2Id);
        return this;
    }
    
    /**
    * 
    * @return     Object3
    */
    public java.math.BigDecimal getObject3Id() {
        return this.object3Id;
    }
    
    /**
    * Set 'Object3' value
    * 
    * @param      object3Id Object3
    */
    public void setObject3Id(java.math.BigDecimal object3Id) {
         this.object3Id = object3Id;
    }
    
    /**
    * Set 'Object3' value and return this object
    * 
    * @param      object3Id Object3
    * @return     Sequence object
    */
    public SequenceObjectPrototype withObject3Id(java.math.BigDecimal object3Id) {
        setObject3Id(object3Id);
        return this;
    }
    
    /**
    * 
    * @return     StartValue
    */
    public java.math.BigDecimal getStartValue() {
        return this.startValue;
    }
    
    /**
    * Set 'StartValue' value
    * 
    * @param      startValue StartValue
    */
    public void setStartValue(java.math.BigDecimal startValue) {
         this.startValue = startValue;
    }
    
    /**
    * Set 'StartValue' value and return this object
    * 
    * @param      startValue StartValue
    * @return     Sequence object
    */
    public SequenceObjectPrototype withStartValue(java.math.BigDecimal startValue) {
        setStartValue(startValue);
        return this;
    }
    
    /**
    * 
    * @return     MaxValue
    */
    public java.math.BigDecimal getMaxValue() {
        return this.maxValue;
    }
    
    /**
    * Set 'MaxValue' value
    * 
    * @param      maxValue MaxValue
    */
    public void setMaxValue(java.math.BigDecimal maxValue) {
         this.maxValue = maxValue;
    }
    
    /**
    * Set 'MaxValue' value and return this object
    * 
    * @param      maxValue MaxValue
    * @return     Sequence object
    */
    public SequenceObjectPrototype withMaxValue(java.math.BigDecimal maxValue) {
        setMaxValue(maxValue);
        return this;
    }
    
    /**
    * 
    * @return     LastCachedValue
    */
    public java.math.BigDecimal getLastCachedValue() {
        return this.lastCachedValue;
    }
    
    /**
    * Set 'LastCachedValue' value
    * 
    * @param      lastCachedValue LastCachedValue
    */
    public void setLastCachedValue(java.math.BigDecimal lastCachedValue) {
         this.lastCachedValue = lastCachedValue;
    }
    
    /**
    * Set 'LastCachedValue' value and return this object
    * 
    * @param      lastCachedValue LastCachedValue
    * @return     Sequence object
    */
    public SequenceObjectPrototype withLastCachedValue(java.math.BigDecimal lastCachedValue) {
        setLastCachedValue(lastCachedValue);
        return this;
    }
    
    /**
    * 
    * @return     CacheLength
    */
    public java.math.BigDecimal getCacheLength() {
        return this.cacheLength;
    }
    
    /**
    * Set 'CacheLength' value
    * 
    * @param      cacheLength CacheLength
    */
    public void setCacheLength(java.math.BigDecimal cacheLength) {
         this.cacheLength = cacheLength;
    }
    
    /**
    * Set 'CacheLength' value and return this object
    * 
    * @param      cacheLength CacheLength
    * @return     Sequence object
    */
    public SequenceObjectPrototype withCacheLength(java.math.BigDecimal cacheLength) {
        setCacheLength(cacheLength);
        return this;
    }
    
    public SequenceService getService() {
        return this.service;
    }
    
    public void setService(SequenceService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(SequenceObjectPrototype  target) {
        target.setName(getName());
        target.setObject1Id(getObject1Id());
        target.setObject2Id(getObject2Id());
        target.setObject3Id(getObject3Id());
        target.setStartValue(getStartValue());
        target.setMaxValue(getMaxValue());
        target.setLastCachedValue(getLastCachedValue());
        target.setCacheLength(getCacheLength());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((SequenceObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        service.save((SequenceObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(SequenceObjectPrototype object) {
        return 
               isObjectsEquals(this.getName(), object.getName() ) 
            && isObjectsEquals(this.getObject1Id(), object.getObject1Id() ) 
            && isObjectsEquals(this.getObject2Id(), object.getObject2Id() ) 
            && isObjectsEquals(this.getObject3Id(), object.getObject3Id() ) 
            && isObjectsEquals(this.getStartValue(), object.getStartValue() ) 
            && isObjectsEquals(this.getMaxValue(), object.getMaxValue() ) 
            && isObjectsEquals(this.getLastCachedValue(), object.getLastCachedValue() ) 
            && isObjectsEquals(this.getCacheLength(), object.getCacheLength() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getName() != null ? getName().hashCode() : 0);
        result = result * 31 + (getObject1Id() != null ? getObject1Id().hashCode() : 0);
        result = result * 31 + (getObject2Id() != null ? getObject2Id().hashCode() : 0);
        result = result * 31 + (getObject3Id() != null ? getObject3Id().hashCode() : 0);
        result = result * 31 + (getStartValue() != null ? getStartValue().hashCode() : 0);
        result = result * 31 + (getMaxValue() != null ? getMaxValue().hashCode() : 0);
        result = result * 31 + (getLastCachedValue() != null ? getLastCachedValue().hashCode() : 0);
        result = result * 31 + (getCacheLength() != null ? getCacheLength().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'Name' fields
    * 
    * @return     Concatenated string values of key 'toName' fields
    */
    public String toName() {
        return getName() + "," + service.getObjectService().idToString(getObject1Id()) + "," + service.getObjectService().idToString(getObject2Id()) + "," + service.getObjectService().idToString(getObject3Id());
    }
}
