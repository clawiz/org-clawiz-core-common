package org.clawiz.core.common.metadata.data.language.node.element;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.data.language.node.AbstractLanguageNode;
import org.clawiz.core.common.metadata.data.language.node.element.field.AttributeStructureField;
import org.clawiz.core.common.metadata.data.language.node.element.field.ClassLanguageNodeFieldList;
import org.clawiz.core.common.metadata.data.language.node.element.field.ElementStructureField;
import org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType;
import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;

public class Structure extends AbstractLanguageNode {

    @ExchangeElement
    ClassLanguageNodeFieldList fields = new ClassLanguageNodeFieldList();

    @ExchangeAttribute
    String                     parentFieldName;

    public ClassLanguageNodeFieldList getFields() {
        return fields;
    }

    public AttributeStructureField addAttribute(String name, AbstractValueType valueType) {
        AttributeStructureField field = new AttributeStructureField();
        field.setStructure(this);
        field.setName(name);
        field.setValueType(valueType);
        fields.add(field);
        return field;
    }

    public <V extends AbstractValueType> AttributeStructureField addAttribute(String name, Class<V> valueTypeClass) {
        AbstractValueType valueType = null;
        try {
            valueType = valueTypeClass.newInstance();
        } catch (Exception e) {
            throw new CoreException("Exception on create value type class ? instance : ?", valueTypeClass.getName(), e.getMessage(), e);
        }
        return addAttribute(name, valueType);
    }

    public ElementStructureField addElement(String name) {
        ElementStructureField field = new ElementStructureField();
        field.setStructure(this);
        field.setName(name);
        fields.add(field);
        return field;
    }

    @Override
    public void setPackageName(String packageName) {
        super.setPackageName(packageName);
        clearCaches();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
        clearCaches();
    }

    public String getParentFieldName() {
        return parentFieldName;
    }

    public void setParentFieldName(String parentFieldName) {
        this.parentFieldName = parentFieldName;
    }

    @Override
    public void setJavaName(String javaName) {
        super.setJavaName(javaName);
        clearCaches();
    }

    String _dataClassName = null;
    public String getDataClassName() {
        if ( _dataClassName == null ) {
            _dataClassName = getPackageName() + "." + getJavaClassName();
        }
        return _dataClassName;
    }

    String _dataListClassName = null;
    public String getDataListClassName() {
        if ( _dataListClassName == null ) {

            String str = getJavaClassName();
            if ( str.toUpperCase().indexOf("ABSTRACT") == 0 ) {
                str = str.substring("ABSTRACT".length());
            }
            _dataListClassName = getPackageName() + "." + str + "List";

        }
        return _dataListClassName;
    }

    private void clearCaches() {
        _dataClassName = null;
        _dataListClassName = null;
    }

}
