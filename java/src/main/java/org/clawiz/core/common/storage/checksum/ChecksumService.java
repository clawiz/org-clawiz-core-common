/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.storage.checksum;


import org.clawiz.core.common.storage.checksumtype.ChecksumTypeService;

import java.math.BigDecimal;

public class ChecksumService extends ChecksumServicePrototype {

    ChecksumTypeService checksumTypeService;

    public void saveObjectChecksum(BigDecimal objectId, String checksumType, String checksum) {
        if ( objectId == null ) {
            throwException("Cannot save checksum for NULL object id");
        }
        if ( checksumType == null ) {
            throwException("Cannot save checksum with NULL type");
        }
        ChecksumObject checksumObject = load(objectChecksumTypeToId(objectId, checksumTypeService.nameToId(checksumType, true), true));
        checksumObject.setChecksum(checksum);
        checksumObject.save();

    }

    public String getObjectChecksum(BigDecimal objectId, String checksumType) {
        BigDecimal checksumTypeId = checksumTypeService.nameToId(checksumType);
        if ( checksumType == null) {
            return null;
        }
        BigDecimal checksumId = objectChecksumTypeToId(objectId, checksumTypeId);
        if ( checksumId == null ) {
            return null;
        }
        ChecksumObject checksumObject = load(checksumId);
        return checksumObject.getChecksum();
    }

}
