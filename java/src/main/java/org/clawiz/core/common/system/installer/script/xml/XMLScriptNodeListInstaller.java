/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.installer.script.xml;

import org.clawiz.core.common.system.installer.AbstractInstaller;
import org.clawiz.core.common.utils.StringUtils;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import static org.clawiz.core.common.system.Install.PARAMETER_DESTINATION_PATH;

public class XMLScriptNodeListInstaller extends AbstractXMLScriptElementInstaller {

    NodeList nodeList;

    public NodeList getNodeList() {
        return nodeList;
    }

    public void setNodeList(NodeList nodeList) {
        this.nodeList = nodeList;
    }

    @Override
    public void process() {

        for(int i = 0; i < getNodeList().getLength(); i++ ) {
            if( ! (getNodeList().item(i) instanceof Element) ) {
                continue;
            }
            Element element = (Element) getNodeList().item(i);

            String name = element.getTagName();

            Class<AbstractInstaller> clazz = getInstallerMap(element.getTagName());
            if ( clazz != null ) {

                if ( ! AbstractXMLScriptElementInstaller.class.isAssignableFrom(clazz) ) {
                    throwException("Installer '?' class ? must extends AbstractXMLScriptElementInstaller to use in XML install script", name, clazz.getName());
                }

                AbstractXMLScriptElementInstaller installer =(AbstractXMLScriptElementInstaller) addInstaller(clazz);
                installer.setElement(element);

                String elementDestinationPath = element.getAttribute(PARAMETER_DESTINATION_PATH);
                if ( ! StringUtils.isEmpty(elementDestinationPath) ) {
                    installer.setDestinationPath(elementDestinationPath);
                }


            } else if ( name.equalsIgnoreCase("include") ) {

                String fileName = element.getAttribute("script");
                if (StringUtils.isEmpty(fileName)) {
                    throwException("Attribute 'script' not defined for 'include' element in script ?", getScriptFileName());
                }

                fileName = relativeToFullFileName(fileName);

                addInstaller(XMLScriptInstaller.class).setScriptFileName(fileName);

            } else if ( name.equalsIgnoreCase("installer-alias") ) {

                String installerName = element.getAttribute("name");
                if ( StringUtils.isEmpty(installerName) ) {
                    throwException("Attribute 'name' for 'installer' element not defined in script ?", getScriptFileName());
                }
                String installerClassName = element.getAttribute("class");
                if ( StringUtils.isEmpty(installerClassName) ) {
                    throwException("Attribute 'class' for 'installer' element not defined in script ?", getScriptFileName());
                }

                setInstallerMap(installerName, installerClassName);

            } else if ( name.equalsIgnoreCase("package") ) {

                String packageName = element.getAttribute("name");
                if ( StringUtils.isEmpty(packageName)) {
                    throwException("Attribute 'name' for 'package' element not defined in script ?", getScriptFileName());
                }
                setChildsPackageName(packageName);
                addInstaller(XMLScriptNodeListInstaller.class).setNodeList(element.getChildNodes());
            } else {

                // wait for process previous nodes of current script
                // (for example, to have filled installerMap from previous <include> tags
                XMLScriptNamedInstaller installer = addInstaller(XMLScriptNamedInstaller.class);
                installer.setInstallerName(name);
                installer.setElement(element);

            }

            if ( clazz != null ) {
            }

        }

    }
}
