/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.database.adapter.postgresql;

import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.database.adapter.DatabaseAdapter;
import org.clawiz.core.common.system.database.metadata.*;

import java.math.BigDecimal;
import java.sql.SQLException;

/**
 * Created by abdrashitovta on 21.09.2015.
 */
public class PostgreSQLDatabaseAdapter extends DatabaseAdapter {


    protected String getSchemaName() {
        return "public";
    }

    @Override
    protected String getColumnNumberTypeName() {
        return "numeric";
    }

    @Override
    protected String getColumnDateTypeName() {
        return "timestamp without time zone";
    }

    @Override
    protected String getColumnVarcharTypeName() {
        return "character varying";
    }

    @Override
    protected String getStringConcatenationOperator() {
        return "||";
    }

    @Override
    public boolean isSequenceExists(DatabaseSequence sequence) {
        Statement statement = executeQuery("SELECT 0 FROM pg_class where relname = ?", sequence.getName().toLowerCase());
        boolean found = statement.next();
        statement.close();
        return found;
    }

    @Override
    public void createSequence(DatabaseSequence sequence) {
        executeUpdate("create sequence "+sequence.getName().toLowerCase()+" start " + sequence.getStartWith());
    }

    @Override
    public BigDecimal getSequenceNextValue(String sequenceName) {
        return executeQueryBigDecimal("select nextval('"+sequenceName.toLowerCase()+"')");
    }

    @Override
    protected DatabaseTable loadTableDefinition(String tableName) throws SQLException {
//        logError("Add PostgreSQL schema check to catalog load statement 'where'");
        Statement statement = executeQuery("select table_name from information_schema.tables where table_schema = ? and upper(table_name) = ?"
                , getSchemaName(), tableName.toUpperCase());

        DatabaseTable table = statement.next() ? new DatabaseTable(statement.getString(1)) : null;

        statement.close();
        return table;
    }

    @Override
    protected void loadTableColumns(DatabaseTable table) throws SQLException {

        Statement statement = executeQuery(
                "SELECT COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, NUMERIC_PRECISION, NUMERIC_SCALE, IS_NULLABLE \n" +
                "FROM information_schema.COLUMNS WHERE TABLE_SCHEMA  = ? AND TABLE_NAME  = ?", getSchemaName(), table.getName().toLowerCase());
        while ( statement.next() ) {

            DatabaseTableColumn column = new DatabaseTableColumn();
            table.addColumn(column);
            column.setName(statement.getString(1));
            column.setRequired("N".equalsIgnoreCase(statement.getString(6)));
            column.setType(metadataColumnTypeToDatabaseColumnType(column, statement.getString(2)));
            if ( column.getType() == DatabaseTableColumn.Type.NUMBER ) {
                column.setPrecision(statement.getResultSet().getInt(4));
                column.setScale(statement.getResultSet().getInt(5));
            } else {
                column.setPrecision(statement.getResultSet().getInt(3));
            }

        }
        statement.close();

    }

    @Override
    protected void loadTableIndexes(DatabaseTable table) throws SQLException {

        DatabaseTableIndex index = null;
        Statement statement = executeQuery(
                "select\n" +
                "    i.relname      as indexname,\n" +
                "    ix.indisunique as indisunique,\n" +
                "    a.attname      as column_name\n" +
                "from\n" +
                "    pg_class t,\n" +
                "    pg_class i,\n" +
                "    pg_index ix,\n" +
                "    pg_attribute a\n" +
                "where\n" +
                "    t.oid = ix.indrelid\n" +
                "    and i.oid = ix.indexrelid\n" +
                "    and a.attrelid = t.oid\n" +
                "    and a.attnum = ANY(ix.indkey)\n" +
                "    and t.relkind = 'r'\n" +
                "    and t.relname = ?\n" +
                "order by\n" +
                "    i.relname", table.getName().toLowerCase());
        while ( statement.next() ) {
            String indexName = statement.getString(1);
            if ( index == null || ! indexName.equalsIgnoreCase(index.getName())) {
                index = new DatabaseTableIndex();
                index.setName(indexName);
                index.setUnique(statement.getResultSet().getBoolean(2));
                table.addIndex(index);
            }
            index.addColumn(new DatabaseTableIndexColumn(statement.getString(3)));
        }
        statement.close();
    }

    @Override
    protected String getCreateTableDDL(DatabaseTable table) {
        return super.getCreateTableDDL(table) + " with ( oids = false )";
    }

    @Override
    public String getAlterTableModifyColumnStatement(DatabaseTable table, DatabaseTableColumn column) {
        return "alter table " + table.getName() + " alter column " + column.getName() + " type " + getTableColumnTypeDDL(column);
    }

    @Override
    public void setStatementNullParameter(Statement statement, int index) {
        try {
            statement.getPreparedStatement().setNull(index, statement.getPreparedStatement().getParameterMetaData().getParameterType(index));
        } catch (SQLException e) {
            throwException("Exception on set null value for parameter '?' of statement : ?", index, statement.getSql(), e.getMessage(), e);
        }
    }
}
