/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator.component.service.element;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.data.type.property.extension.AfterExtensionTypeProperty;
import org.clawiz.core.common.metadata.data.type.property.extension.BeforeExtensionTypeProperty;
import org.clawiz.core.common.metadata.data.type.property.extension.method.CheckExtensionTypePropertyMethod;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.type.key.TypeKey;
import org.clawiz.core.common.metadata.data.type.key.TypeKeyField;
import org.clawiz.core.common.metadata.data.type.field.property.validator.AbstractTypeFieldValidator;
import org.clawiz.core.common.metadata.data.type.field.property.validator.TypeFieldValidatorMoreOrEqualZero;
import org.clawiz.core.common.metadata.data.type.field.property.validator.TypeFieldValidatorMoreZero;
import org.clawiz.core.common.utils.StringUtils;

public class TypeServiceCheckMethodElement extends AbstractTypeServiceMethodElement {

    String parameterName;
    
    protected void addFieldValidator(TypeField field, AbstractTypeFieldValidator validator) {
        
        String getMethodName = parameterName + "." + field.getGetMethodName() + "()";
        String description   =  field.getDescription().replaceAll("\"","\\\\\"");

        if ( validator instanceof TypeFieldValidatorMoreZero) {
            
            addText("if (" + getMethodName + " != null && " + getMethodName + ".compareTo(___zero) <= 0) {");
            addText("    throwException(\"'"+description+"' must be greater than zero\");");
            addText("}");
            
        } else if ( validator  instanceof TypeFieldValidatorMoreOrEqualZero) {
            
            addText("if (" + getMethodName + " != null && " + getMethodName + ".compareTo(___zero) < 0) {");
            addText("    throwException(\"'" + description + "' must be greater or equal to zero\");");
            addText("}");
            
        } else {
            throw new CoreException("Wrong field validator class '?'", new Object[]{validator.getClass()});
        }
        
    }
    
    protected void addFieldCheck(TypeField field) {

        String getMethodName = parameterName + "." + field.getGetMethodName();
        String setMethodName = parameterName + "." + field.getSetMethodName();


        if ( field.isRequired() ) {

            addText("if (" + getMethodName + "() == null && ! " + parameterName + ".isSkipNotNullCheck() ) {");
            String description =  field.getDescription().replaceAll("\"","\\\\\"");
            addText(" throwException(\"Value of field '?' cannot be null\", new Object[]{\"" + description + "\"});");
            addText("}");

        }

        for(AbstractTypeFieldValidator v : field.getProperties().getNodeListByClass(AbstractTypeFieldValidator.class) ) {
            throwException("Not implemented");
            addFieldValidator(field, v);
        }
        
    }
    
    protected void addKeyCheck(TypeKey key) {

        if ( ! key.isUnique() ) {
            return;
        }

        addText("");
        String parametersLine   = "";

        for(TypeKeyField keyField : key.getFields() ) {
            TypeField field = keyField.getField();

            String getter = parameterName + "." + field.getGetMethodName() + "()";

            parametersLine += getter + ", ";

        }

        addText("if ( " + StringUtils.toLowerFirstChar(key.getJavaClassName()) + "ToId(" + parametersLine + parameterName + ".getId() ) != null ) {");
        addText("    throwException(\"? '?' already exists\", new Object[]{ \"" + getGenerator().getType().getName() + "\", " + parameterName + ".to" + key.getJavaClassName() + "() });");
        addText("}");

    }
    
    @Override
    public void process() {
        super.process();

        setName("check");

        parameterName = getTypeJavaVariableName();

        addParameter(getDataObjectClassName(), parameterName, "Checked object");

        addJavaDoc("Fill object default values and check data consistency");

        addText("");
        addExtensions(BeforeExtensionTypeProperty.class, CheckExtensionTypePropertyMethod.class);

        addText("if ( " + parameterName + " == null ) {");
        addText("    throwException(\"Cannot check null ?\", new Object[]{\"" + getDataObjectClassName() + "\"});");
        addText("}");

        addText("");
        addText(parameterName + ".fillDefaults();");
        addText("");

        for( TypeField field : getFields() ) {
            addFieldCheck(field);
        }
        
        for ( TypeKey key : getKeys() ) {
            addKeyCheck(key);
        }

        addText("");
        addExtensions(AfterExtensionTypeProperty.class, CheckExtensionTypePropertyMethod.class);


    }
    
}
