package org.clawiz.core.common.metadata.data.service.method.valuetype;


import org.clawiz.core.common.CoreException;

public class AbstractServiceMethodValueType extends AbstractServiceMethodValueTypePrototype {

    public Class getJavaClass() {
        throw new CoreException("Method ?.getJavaClass not implemented", this.getClass().getName());
    }

}
