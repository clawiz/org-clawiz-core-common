package org.clawiz.core.common.metadata.data.language.node.enumeration;

import org.clawiz.core.common.metadata.data.language.node.AbstractLanguageNode;
import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;

public class Enumeration extends AbstractLanguageNode {

    @ExchangeElement
    EnumerationValueList values = new EnumerationValueList();

    public EnumerationValueList getValues() {
        return values;
    }

    public EnumerationValue addValue(String name, String description) {
        EnumerationValue value = new EnumerationValue();
        value.setEnumeration(this);
        value.setName(name);
        value.setDescription(description);
        values.add(value);
        return value;
    }

}
