package org.clawiz.core.common.storage.user;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;

public class UserServicePrototype extends AbstractTypeService<UserObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.core.common.storage", "User");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.User"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      user Checked object
    */
    public void check(UserObject user) {
        
        if ( user == null ) {
            throwException("Cannot check null ?", new Object[]{"UserObject"});
        }
        
        user.fillDefaults();
        
        
        if ( nameToId(user.getName(), user.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "User", user.toName() });
        }
        
    }
    
    /**
    * Create new UserObject instance and fill default values
    * 
    * @return     Created object
    */
    public UserObject create() {
        
        UserObject user = new UserObject();
        user.setService((UserService) this);
        
        user.fillDefaults();
        
        return user;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public UserObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select name, password, password_hash, previous_password, email, first_name, last_name from cw_core_users where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"User", id});
        }
        
        UserObject result = new UserObject();
        
        result.setService((UserService) this);
        result.setId(id);
        result.setName(statement.getString(1));
        result.setPassword(statement.getString(2));
        result.setPasswordHash(statement.getString(3));
        result.setPreviousPassword(statement.getString(4));
        result.setEmail(statement.getString(5));
        result.setFirstName(statement.getString(6));
        result.setLastName(statement.getString(7));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of UserObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public UserList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of UserObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public UserList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        UserList result = new UserList();
        
        
        Statement statement = executeQuery("select id, name, password, password_hash, previous_password, email, first_name, last_name from cw_core_users"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            UserObject object = new UserObject();
        
            object.setService((UserService) this);
            object.setId(statement.getBigDecimal(1));
            object.setName(statement.getString(2));
            object.setPassword(statement.getString(3));
            object.setPasswordHash(statement.getString(4));
            object.setPreviousPassword(statement.getString(5));
            object.setEmail(statement.getString(6));
            object.setFirstName(statement.getString(7));
            object.setLastName(statement.getString(8));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'name' fields
    * 
    * @param      name Name
    * @return     Id of found record or null
    */
    public BigDecimal nameToId(java.lang.String name) {
        return nameToId(name, null);
    }
    
    /**
    * Find id of record by key 'name' fields with id not equal skipId
    * 
    * @param      name   Name
    * @param      skipId Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal nameToId(java.lang.String name, BigDecimal skipId) {
        
        if ( name == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_users where upper_name = ?", name.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_users where upper_name = ? and id != ?", name.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'name' fields or create new record with values set to given parameters
    * 
    * @param      name                Name
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal nameToId(java.lang.String name, boolean createNewIfNotFound) {
        
        BigDecimal id = nameToId(name, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        UserObject object = create();
        object.setName(name);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'name' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select name from cw_core_users where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        UserObject object = new UserObject();
        object.setService((UserService)this);
        object.setId(id);
        object.setName(statement.getString(1));
        
        statement.close();
        
        return object.toName();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'email' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToEmail(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select email from cw_core_users where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        UserObject object = new UserObject();
        object.setService((UserService)this);
        object.setId(id);
        object.setEmail(statement.getString(1));
        
        statement.close();
        
        return object.toEmail();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToName(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      userObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(UserObject userObject) {
        return userObject.toName();
    }
    
    protected void saveAudit(TransactionAction transactionAction, UserObject oldUserObject, UserObject newUserObject) {
        
        UserObject o = oldUserObject != null ? oldUserObject : new UserObject();
        UserObject n = newUserObject != null ? newUserObject : new UserObject();
        
        
        executeUpdate("insert into a_cw_core_users (scn, action_type, id , o_name, o_upper_name, o_password, o_password_hash, o_previous_password, o_email, o_upper_email, o_first_name, o_last_name, n_name, n_upper_name, n_password, n_password_hash, n_previous_password, n_email, n_upper_email, n_first_name, n_last_name) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getName(), o.getName() != null ? o.getName().toUpperCase() : null, o.getPassword(), o.getPasswordHash(), o.getPreviousPassword(), o.getEmail(), o.getEmail() != null ? o.getEmail().toUpperCase() : null, o.getFirstName(), o.getLastName(), n.getName(), n.getName() != null ? n.getName().toUpperCase() : null, n.getPassword(), n.getPasswordHash(), n.getPreviousPassword(), n.getEmail(), n.getEmail() != null ? n.getEmail().toUpperCase() : null, n.getFirstName(), n.getLastName());
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      user Saved object
    */
    public void save(UserObject user) {
        
        if ( user == null ) {
            throwException("Cannot save NULL ?", new Object[]{"UserObject"});
        }
        
        TransactionAction transactionAction;
        UserObject oldUser;
        if ( user.getService() == null ) {
            user.setService((UserService)this);
        }
        
        check(user);
        
        if ( user.getId() == null ) {
        
            user.setId(getObjectService().createObject(getTypeId()));
        
            oldUser = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, user.getId());
        
            executeUpdate("insert into cw_core_users" 
                          + "( id, name, upper_name, password, password_hash, previous_password, email, upper_email, first_name, last_name ) "
                          + "values"
                          + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                          user.getId(), user.getName(), ( user.getName() != null ? user.getName().toUpperCase() : null ), user.getPassword(), user.getPasswordHash(), user.getPreviousPassword(), user.getEmail(), ( user.getEmail() != null ? user.getEmail().toUpperCase() : null ), user.getFirstName(), user.getLastName() );
        
        } else {
        
            oldUser = load(user.getId());
            if ( oldUser.equals(user) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, user.getId());
        
            executeUpdate("update cw_core_users set "
                          + "name = ?, upper_name = ?, password = ?, password_hash = ?, previous_password = ?, email = ?, upper_email = ?, first_name = ?, last_name = ? "
                          + "where id = ?",
                          user.getName(), ( user.getName() != null ? user.getName().toUpperCase() : null ), user.getPassword(), user.getPasswordHash(), user.getPreviousPassword(), user.getEmail(), ( user.getEmail() != null ? user.getEmail().toUpperCase() : null ), user.getFirstName(), user.getLastName(), user.getId() );
        
        }
        
        saveAudit(transactionAction, oldUser, user);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        UserObject oldUserObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldUserObject, null);
        
        getObjectService().deleteObject(id);
        
        executeUpdate("delete from cw_core_users where id = ?", id);
        
    }
}
