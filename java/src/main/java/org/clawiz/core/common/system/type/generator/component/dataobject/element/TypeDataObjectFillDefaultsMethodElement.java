/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator.component.dataobject.element;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.type.field.property.defaultvalue.*;

public class TypeDataObjectFillDefaultsMethodElement extends AbstractTypeDataObjectMethodElement {

    @Override
    public void process() {
        super.process();

        setName("fillDefaults");

        addJavaDoc("Fill object field default values");

        addText("if ( service == null ) {");
        addText("    throw new CoreException(\"Service must be defined before fill object default values\");");
        addText("}");


        for (TypeField field : getFields() ) {

            DefaultValueTypeFieldProperty defaultValue = field.getProperties().getNodeByClass(DefaultValueTypeFieldProperty.class);
            if ( defaultValue == null ) {
                continue;
            }

/*
            if ( defaultValue instanceof TypeFieldDefaultValueNumerator) {

                addJavaDoc(" - set " + field.getName() + " to next numerator value");

                addText("if ( this." + field.getJavaVariableName() + " == null ) {");

                BigDecimal sv = ((TypeFieldDefaultValueNumerator) defaultValue).getStartValue();
                if ( sv == null ) {
                    sv = new BigDecimal(1);
                }

                String str = "getNextSequenceValue(session, \"" + getComponent().getType().getFullName() + "\", new BigDecimal(" + sv  + "))";
                if ( field.getValueType() instanceof ValueTypeString) {
                    str = str + ".toString()";
                } else if ( field.getValueType() instanceof ValueTypeNumber) {
                    throwException("Default numerator for NUMBER fields not implemented yet");
                        */
/* Нумеруем как максимум значения внгутри блока *//*

                } else {
                    throw new CoreException("Cannot implement DefaultValueNumerator for field '?' of type '?'", new Object[]{field.getFullName(), field.getValueType()});
                }

                addText("    this." + field.getJavaVariableName() + " = " + str + ";");
                addText("}");
            } else if ( defaultValue instanceof TypeFieldDefaultValueCurrentUserId) {
                addJavaDoc(" - set " + field.getName() + " to current user ID");
                addText("if ( this." + field.getJavaVariableName() + " == null ) {");
                addText("    this." + field.getJavaVariableName() + " = session.getUserId();");
                addText("}");
            } else if ( defaultValue instanceof CurrentDateTypeFieldDefaultValueProperty) {
                addJavaDoc(" - set " + field.getName() + " to current date (without time)");
                addText("if ( this." + field.getJavaVariableName() + " == null ) {");
                addText("    this." + field.getJavaVariableName() + " = org.clawiz.core.xdk.utils.date.DateUtils.truncToDay(new java.util.Date());");
                addText("}");
            } else if ( defaultValue instanceof CurrentDateTimeTypeFieldDefaultValueProperty) {
                addJavaDoc(" - set " + field.getName() + " to current date and time");
                addText("if ( this." + field.getJavaVariableName() + " == null ) {");
                addText("    this." + field.getJavaVariableName() + " = new java.util.Date();");
                addText("}");
            } else if ( defaultValue instanceof TypeFieldDefaultValueFalse) {
                addJavaDoc(" - set " + field.getName() + " to FALSE");
                addText("if ( this." + field.getJavaVariableName() + " == null ) {");
                addText("    this." + field.getJavaVariableName() + " = new Boolean(false);");
                addText("}");
            } else if ( defaultValue instanceof TypeFieldDefaultValueOrderNo) {
                throwException("Default orderNo for NUMBER fields not implemented yet");
            } else if ( defaultValue instanceof TypeFieldDefaultValueServiceMethod) {
                TypeFieldDefaultValueServiceMethod dvm = (TypeFieldDefaultValueServiceMethod) defaultValue;
                addJavaDoc(" - set " + field.getName() + " to " + dvm.getServiceName() + " " + dvm.getMethodName() + "(this)");
                String callStr = "service.getService("+dvm.getServiceName()+".class)." + dvm.getMethodName() + "(this)";
                addText("if ( this." + field.getJavaVariableName() + " == null ) {");
                addText("    this." + field.getJavaVariableName() + " =  "+callStr+";");
                addText("}");
            } else {
                throw new CoreException("Wrong default value class '?' for field '?'", new Object[]{defaultValue.getClass(), field.getFullName()});
            }




*/
        }


    }
}
