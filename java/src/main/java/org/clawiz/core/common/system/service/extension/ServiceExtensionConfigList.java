/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.service.extension;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.installer.module.ModuleInstaller;
import org.clawiz.core.common.system.service.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

public class ServiceExtensionConfigList implements Iterable<ServiceExtensionConfig> {

    private HashMap<Class, HashMap<Class, ServiceExtensionConfig>>      serviceExtensionsCache            = new HashMap<>();
    private HashMap<Class, ArrayList<ServiceExtensionConfig>>           serviceExtensionsConfigListsCache = new HashMap<>();
    private ArrayList<ServiceExtensionConfig>                           configs                           = new ArrayList<>();
    private ConcurrentHashMap<Class, ArrayList<ServiceExtensionConfig>> getServiceExtensionsCache         = new ConcurrentHashMap<>();

    public void clear() {
        if ( ! ModuleInstaller.isProtectedMode() ) {
            throw new CoreException("Wrong call");
        }
        serviceExtensionsCache            = new HashMap<>();
        serviceExtensionsConfigListsCache = new HashMap<>();
        configs                           = new ArrayList<>();
        getServiceExtensionsCache         = new ConcurrentHashMap<>();
    }

    @Override
    public Iterator<ServiceExtensionConfig> iterator() {
        return configs.iterator();
    }

    @Override
    public void forEach(Consumer<? super ServiceExtensionConfig> action) {
        configs.forEach(action);
    }

    @Override
    public Spliterator<ServiceExtensionConfig> spliterator() {
        return configs.spliterator();
    }

    public void add(ServiceExtensionConfig config) {

        synchronized (configs) {

            HashMap<Class, ServiceExtensionConfig> serviceCache = serviceExtensionsCache.get(config.getServiceClass());
            if ( serviceCache == null ) {
                serviceCache = new HashMap<>();
                serviceExtensionsCache.put(config.getServiceClass(), serviceCache);
            }

            ServiceExtensionConfig currentConfig = serviceCache.get(config.getExtensionClass());
            if ( currentConfig != null ) {
                if ( ! currentConfig.equals(config)) {
                    throw new CoreException("Service ? extension ? already defined with different config", config.getServiceClass().getName(), config.getExtensionClass().getName());
                }
                return;
            }

            serviceCache.put(config.getExtensionClass(), config);

            ArrayList<ServiceExtensionConfig> serviceList = serviceExtensionsConfigListsCache.get(config.getServiceClass());
            if ( serviceList == null ) {
                serviceList = new ArrayList<>();
                serviceExtensionsConfigListsCache.put(config.getServiceClass(), serviceList);
            }

            serviceList.add(config);
            configs.add(config);
            getServiceExtensionsCache.clear();

        }

        return;
    }


    public <S extends Service, T extends ServiceExtension> ServiceExtensionConfigList add(Class<S> serviceClass, Class<T> extensionClass) {
        add(new ServiceExtensionConfig(serviceClass, extensionClass));
        return this;
    }

    public ServiceExtensionConfigList add(ServiceExtensionConfigList configs) {
        for (ServiceExtensionConfig config : configs) {
            add(config);
        }
        return this;
    }


    private ArrayList<ServiceExtensionConfig> prepareServiceExtensionsGetter(Class serviceClass) {
        ArrayList<ServiceExtensionConfig> result = new ArrayList<>();

        ArrayList<ServiceExtensionConfig> list = serviceExtensionsConfigListsCache.get(serviceClass);
        if ( list != null ) {
            result.addAll(list);
        }

        if ( ! serviceClass.getSuperclass().equals(Service.class) ) {
            result.addAll(prepareServiceExtensionsGetter(serviceClass.getSuperclass()));
        }

        return result;
    }

    public ArrayList<ServiceExtensionConfig> getServiceExtensions(Class serviceClass) {
        ArrayList<ServiceExtensionConfig> result = getServiceExtensionsCache.get(serviceClass);
        if ( result != null ) {
            return result;
        }
        result = prepareServiceExtensionsGetter(serviceClass);
        getServiceExtensionsCache.put(serviceClass, result);
        return result;
    }

}
