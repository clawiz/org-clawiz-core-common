package org.clawiz.core.common.storage.roleinheritance;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;
import org.clawiz.core.common.system.service.NotInitializeService;

public class RoleInheritanceServicePrototype extends AbstractTypeService<RoleInheritanceObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.core.common.storage", "RoleInheritance");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.RoleInheritance"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      roleInheritance Checked object
    */
    public void check(RoleInheritanceObject roleInheritance) {
        
        if ( roleInheritance == null ) {
            throwException("Cannot check null ?", new Object[]{"RoleInheritanceObject"});
        }
        
        roleInheritance.fillDefaults();
        
        
        if ( roleInheritedToId(roleInheritance.getInheritedRoleId(), roleInheritance.getRoleId(), roleInheritance.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "RoleInheritance", roleInheritance.toRoleInherited() });
        }
        
    }
    
    /**
    * Create new RoleInheritanceObject instance and fill default values
    * 
    * @return     Created object
    */
    public RoleInheritanceObject create() {
        
        RoleInheritanceObject roleInheritance = new RoleInheritanceObject();
        roleInheritance.setService((RoleInheritanceService) this);
        
        roleInheritance.fillDefaults();
        
        return roleInheritance;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public RoleInheritanceObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select role_id, inherited_role_id from cw_core_role_inheritances where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"RoleInheritance", id});
        }
        
        RoleInheritanceObject result = new RoleInheritanceObject();
        
        result.setService((RoleInheritanceService) this);
        result.setId(id);
        result.setRoleId(statement.getBigDecimal(1));
        result.setInheritedRoleId(statement.getBigDecimal(2));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of RoleInheritanceObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public RoleInheritanceList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of RoleInheritanceObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public RoleInheritanceList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        RoleInheritanceList result = new RoleInheritanceList();
        
        
        Statement statement = executeQuery("select id, role_id, inherited_role_id from cw_core_role_inheritances"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            RoleInheritanceObject object = new RoleInheritanceObject();
        
            object.setService((RoleInheritanceService) this);
            object.setId(statement.getBigDecimal(1));
            object.setRoleId(statement.getBigDecimal(2));
            object.setInheritedRoleId(statement.getBigDecimal(3));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'RoleInherited' fields
    * 
    * @param      inheritedRoleId InheritedRole
    * @param      roleId          Role
    * @return     Id of found record or null
    */
    public BigDecimal roleInheritedToId(java.math.BigDecimal inheritedRoleId, java.math.BigDecimal roleId) {
        return roleInheritedToId(inheritedRoleId, roleId, null);
    }
    
    /**
    * Find id of record by key 'RoleInherited' fields with id not equal skipId
    * 
    * @param      inheritedRoleId InheritedRole
    * @param      roleId          Role
    * @param      skipId          Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal roleInheritedToId(java.math.BigDecimal inheritedRoleId, java.math.BigDecimal roleId, BigDecimal skipId) {
        
        if ( inheritedRoleId == null || roleId == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_role_inheritances where inherited_role_id = ? and role_id = ?", inheritedRoleId, roleId);
        } else {
            return executeQueryBigDecimal("select id from cw_core_role_inheritances where inherited_role_id = ? and role_id = ? and id != ?", inheritedRoleId, roleId, skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'RoleInherited' fields or create new record with values set to given parameters
    * 
    * @param      inheritedRoleId     InheritedRole
    * @param      roleId              Role
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal roleInheritedToId(java.math.BigDecimal inheritedRoleId, java.math.BigDecimal roleId, boolean createNewIfNotFound) {
        
        BigDecimal id = roleInheritedToId(inheritedRoleId, roleId, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        RoleInheritanceObject object = create();
        object.setInheritedRoleId(inheritedRoleId);
        object.setRoleId(roleId);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'RoleInherited' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToRoleInherited(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select inherited_role_id, role_id from cw_core_role_inheritances where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        RoleInheritanceObject object = new RoleInheritanceObject();
        object.setService((RoleInheritanceService)this);
        object.setId(id);
        object.setInheritedRoleId(statement.getBigDecimal(1));
        object.setRoleId(statement.getBigDecimal(2));
        
        statement.close();
        
        return object.toRoleInherited();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToRoleInherited(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      roleInheritanceObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(RoleInheritanceObject roleInheritanceObject) {
        return roleInheritanceObject.toRoleInherited();
    }
    
    protected void saveAudit(TransactionAction transactionAction, RoleInheritanceObject oldRoleInheritanceObject, RoleInheritanceObject newRoleInheritanceObject) {
        
        RoleInheritanceObject o = oldRoleInheritanceObject != null ? oldRoleInheritanceObject : new RoleInheritanceObject();
        RoleInheritanceObject n = newRoleInheritanceObject != null ? newRoleInheritanceObject : new RoleInheritanceObject();
        
        
        executeUpdate("insert into a_cw_core_role_inheritances (scn, action_type, id , o_role_id, o_inherited_role_id, n_role_id, n_inherited_role_id) values ( ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getRoleId(), o.getInheritedRoleId(), n.getRoleId(), n.getInheritedRoleId());
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      roleInheritance Saved object
    */
    public void save(RoleInheritanceObject roleInheritance) {
        
        if ( roleInheritance == null ) {
            throwException("Cannot save NULL ?", new Object[]{"RoleInheritanceObject"});
        }
        
        TransactionAction transactionAction;
        RoleInheritanceObject oldRoleInheritance;
        if ( roleInheritance.getService() == null ) {
            roleInheritance.setService((RoleInheritanceService)this);
        }
        
        check(roleInheritance);
        
        if ( roleInheritance.getId() == null ) {
        
            roleInheritance.setId(getObjectService().createObject(getTypeId()));
        
            oldRoleInheritance = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, roleInheritance.getId());
        
            executeUpdate("insert into cw_core_role_inheritances" 
                          + "( id, role_id, inherited_role_id ) "
                          + "values"
                          + "(?, ?, ?)",
                          roleInheritance.getId(), roleInheritance.getRoleId(), roleInheritance.getInheritedRoleId() );
        
            if ( roleInheritance.getRoleId() != null ) { getObjectService().setLink(roleInheritance.getRoleId(), roleInheritance.getId()); }
            if ( roleInheritance.getInheritedRoleId() != null ) { getObjectService().setLink(roleInheritance.getInheritedRoleId(), roleInheritance.getId()); }
        
        } else {
        
            oldRoleInheritance = load(roleInheritance.getId());
            if ( oldRoleInheritance.equals(roleInheritance) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, roleInheritance.getId());
        
            executeUpdate("update cw_core_role_inheritances set "
                          + "role_id = ?, inherited_role_id = ? "
                          + "where id = ?",
                          roleInheritance.getRoleId(), roleInheritance.getInheritedRoleId(), roleInheritance.getId() );
        
            getObjectService().changeLinkParent(oldRoleInheritance.getRoleId(), roleInheritance.getRoleId(), roleInheritance.getId());
            getObjectService().changeLinkParent(oldRoleInheritance.getInheritedRoleId(), roleInheritance.getInheritedRoleId(), roleInheritance.getId());
        
        }
        
        saveAudit(transactionAction, oldRoleInheritance, roleInheritance);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        RoleInheritanceObject oldRoleInheritanceObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldRoleInheritanceObject, null);
        
        getObjectService().deleteObject(id);
        
        if (oldRoleInheritanceObject.getRoleId() != null) { getObjectService().deleteLink(oldRoleInheritanceObject.getRoleId(), id); }
        if (oldRoleInheritanceObject.getInheritedRoleId() != null) { getObjectService().deleteLink(oldRoleInheritanceObject.getInheritedRoleId(), id); }
        
        executeUpdate("delete from cw_core_role_inheritances where id = ?", id);
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.role.RoleService roleService;
    
    public org.clawiz.core.common.storage.role.RoleService getRoleService() {
        if ( roleService == null ) {
            roleService = getService(org.clawiz.core.common.storage.role.RoleService.class);
        }
        return roleService;
    }
    
    public org.clawiz.core.common.storage.role.RoleObject createRole() {
        return getRoleService().create();
    }
    
    public org.clawiz.core.common.storage.role.RoleObject loadRole(BigDecimal id) {
        return getRoleService().load(id);
    }
}
