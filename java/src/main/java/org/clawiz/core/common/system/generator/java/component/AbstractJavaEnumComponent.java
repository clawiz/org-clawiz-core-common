/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.java.component;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;

import java.util.ArrayList;
import java.util.HashMap;

public class AbstractJavaEnumComponent extends AbstractJavaClassComponent {

    ArrayList<String> values            = new ArrayList<>();
    HashMap<String, String> valuesCache = new HashMap<>();

    

    @SuppressWarnings("Duplicates")
    public void addValue(String value) {
        if ( value == null ) {
            throwException("Enumeration ? value cannot be null", getName());
        }
        String key = value.toUpperCase();
        if ( valuesCache.containsKey(key)) {
            throwException("Enumeration ? already have value '?'", getName(), value);
        }
        valuesCache.put(key,key);
        values.add(key);
    }

    public ArrayList<String> getValues() {
        return values;
    }

    public JavaMethodElement addStringToEnumMethod(String exceptionParameters) {

        addImport(CoreException.class);

        JavaMethodElement method = addElement(JavaMethodElement.class);
        method.setName("to" + getName());
        method.setType(getName());
        method.addParameter("String", "string");
        method.setStatic(true);

        method.addText("if ( string == null ) {");
        method.addText("    return null;");
        method.addText("}");
        method.addText("");
        method.addText("try {");
        method.addText("    return " + getName() + ".valueOf(string.toUpperCase());");
        method.addText("} catch (IllegalArgumentException e) {");
        method.addText("    throw new CoreException(" + exceptionParameters + ");");
        method.addText("}");
        method.addText("");

        return method;
    }

    @Override
    public void write() {

        if ( getDestinationPath() == null ) {
            logDebug("Destination path is null, skip write " + getFullName());
            return;
        }

        pln("package " + packageName + ";");
        pln("");
        for (String s : imports) {
            pln("import " + s + ";");
        }
        pln("");

        pln("public enum " + getName()
                + " {");

        String str    = "";
        String prefix = "";
        for (String value : getValues() ) {
            str += prefix + value;
            prefix = ", ";
        }

        pln("");
        pln("    " + str + ";");
        pln("");

        writeElements();

        pln("}");

    }
}
