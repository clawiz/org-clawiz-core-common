package org.clawiz.core.common.storage.module;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;

public class ModuleModelPrototype extends TypeModel {
    
    private static TypeField PACKAGE_NAME_FIELD;
    
    private static TypeField MODULE_CLASS_NAME_FIELD;
    
    private static TypeField MODULE_VERSION_FIELD;
    
    private static Type type;
    
    private ModuleService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public ModuleService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(ModuleService.class).getType();
        
        
        PACKAGE_NAME_FIELD = type.getFields().get("PackageName");
        if ( PACKAGE_NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Module.PackageName"}); }
        
        MODULE_CLASS_NAME_FIELD = type.getFields().get("ModuleClassName");
        if ( MODULE_CLASS_NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Module.ModuleClassName"}); }
        
        MODULE_VERSION_FIELD = type.getFields().get("ModuleVersion");
        if ( MODULE_VERSION_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Module.ModuleVersion"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _packageName;
    
    public TypeFieldModel packageName() {
        
        if ( _packageName != null ) {
            return _packageName;
        }
        
        _packageName = new TypeFieldModel(this, PACKAGE_NAME_FIELD);
        return _packageName;
        
    }
    
    private TypeFieldModel _moduleClassName;
    
    public TypeFieldModel moduleClassName() {
        
        if ( _moduleClassName != null ) {
            return _moduleClassName;
        }
        
        _moduleClassName = new TypeFieldModel(this, MODULE_CLASS_NAME_FIELD);
        return _moduleClassName;
        
    }
    
    private TypeFieldModel _moduleVersion;
    
    public TypeFieldModel moduleVersion() {
        
        if ( _moduleVersion != null ) {
            return _moduleVersion;
        }
        
        _moduleVersion = new TypeFieldModel(this, MODULE_VERSION_FIELD);
        return _moduleVersion;
        
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "PACKAGENAME" : return packageName();
        case "MODULECLASSNAME" : return moduleClassName();
        case "MODULEVERSION" : return moduleVersion();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
