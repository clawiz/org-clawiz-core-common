package org.clawiz.core.common.metadata.generator.node.enumeration;

import org.clawiz.core.common.metadata.generator.node.AbstractMetadataNodeGenerator;
import org.clawiz.core.common.metadata.generator.node.AbstractMetadataNodeGeneratorContext;
import org.clawiz.core.common.metadata.generator.node.enumeration.component.MetadataNodeJavaEnumComponent;

public class MetadataNodeJavaEnumGenerator extends AbstractMetadataNodeGenerator  {

    private MetadataNodeJavaEnumGeneratorContext context;

    @Override
    public MetadataNodeJavaEnumGeneratorContext getContext() {
        return context;
    }

    @Override
    public void setContext(AbstractMetadataNodeGeneratorContext context) {
        super.setContext(context);
        this.context = (MetadataNodeJavaEnumGeneratorContext) context;
    }

    @Override
    protected void process() {
        super.process();

        addComponent(MetadataNodeJavaEnumComponent.class);
    }
}

