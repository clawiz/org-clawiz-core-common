/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.metadata.node.storage;

import org.clawiz.core.common.metadata.MetadataBase;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeList;
import org.clawiz.core.common.metadata.node.loader.MetadataLoader;
import org.clawiz.core.common.metadata.storage.AbstractMetadataStorage;
import org.clawiz.core.common.storage.metadatastorage.MetadataStorageObject;
import org.clawiz.core.common.utils.file.FileDefinition;
import org.clawiz.core.common.utils.file.FileUtils;

import java.util.concurrent.ConcurrentHashMap;

public class MetadataNodeResourcesXMLStorage extends AbstractMetadataStorage {

    MetadataLoader metadataLoader;
    FileUtils      fileUtils;
    MetadataBase   metadataBase;

    private ConcurrentHashMap<String, String> nodeUrlsXmlCache = new ConcurrentHashMap<>();

    @Override
    public MetadataNodeList fetchNodeList(MetadataStorageObject storageObject) {
        String xml = nodeUrlsXmlCache.get(storageObject.getUrl());

        if ( xml == null ) {
            xml = fileUtils.readToString("/metadata/data" + storageObject.getUrl());
        }

        logDebug("Parsing " + storageObject.getUrl());

        MetadataNodeList nodeList = metadataLoader.xmlToNodes(xml);


        return nodeList;
    }

    private String getNodeResourcesUrl(String nodeName) {
        return"/" + nodeName.replaceAll("\\.", "/") + ".xml";
    }

    public String getNodeResourcesUrl(MetadataNode node) {
        if ( node == null ) {
            return null;
        }
        return getNodeResourcesUrl(node.getFullName());
    }

    public String  getNodeResourcesPath(String nodeName) {
        if ( nodeName == null ) {
            return null;
        }
        return "/metadata/data" + getNodeResourcesUrl(nodeName);
    }


    public void saveNode(MetadataNode node, String destinationPath) {

        if ( destinationPath == null ) {
            throwException("Cannot save node ? to null destination path", node.getFullName());
        }

        String xml = metadataLoader.nodeToXml(node);
        String path     = "/" + node.getPackageName().replaceAll("\\.", "/");
        String fileName = node.getName() + ".xml";

        FileDefinition fd = new FileDefinition();
        fd.setPath(destinationPath
                    + "/main/resources/metadata/data/" + path);
        fd.setName(fileName);
        fd.setBytes(xml.getBytes());
        fileUtils.createFile(fd);

        String storageUrl = path + "/" + fileName;

        metadataBase.saveNode(node, this.getClass(), storageUrl);
        nodeUrlsXmlCache.put(storageUrl, xml);


    }

}
