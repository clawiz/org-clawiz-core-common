/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.query.element;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.query.Query;
import org.clawiz.core.common.query.QueryBuilder;
import org.clawiz.core.common.query.element.column.QueryColumnList;
import org.clawiz.core.common.query.element.filter.component.QueryFilterComponent;
import org.clawiz.core.common.query.element.order.QueryOrderList;
import org.clawiz.core.common.system.session.Session;

public class AbstractQueryElement {

    Query query;

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    protected QueryBuilder getBuilder() {
        return query.getBuilder();
    }

    protected Session getSession() {
        return getBuilder().getSession();
    }

    public void throwException(String code, Object... parameters ) {
        getBuilder().throwException(code, parameters);
    }

    protected <T extends AbstractQueryElement> T addElement(Class<T> clazz) {

        try {
            T element = clazz.newInstance();
            element.setQuery(query);
            return element;
        } catch (Exception e) {
            throw new CoreException(getSession(), "Exception on create instance of query element class ? : ?", clazz.getName(), e.getMessage(), e);
        }
    }

    public QueryColumnList select() {
        return query.select();
    }

    public QueryFilterComponent where() {
        return getQuery().where();
    }

    public QueryOrderList orderBy() {
        return getQuery().orderBy();
    }

}
