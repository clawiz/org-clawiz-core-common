/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.database;

import org.clawiz.core.common.CoreException;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;

public class Statement {

    private Connection                 connection;
    private String                     sql;
    private PreparedStatement          preparedStatement;
    private ResultSet                  resultSet;
    private ArrayList<Column>          columns;

    public Statement(Connection connection, String sql) {
        this.connection = connection;
        this.sql = sql;
    }

    public void prepare() {
        try {
            preparedStatement = connection.getJDBCConnection().prepareStatement(sql);
        } catch (SQLException e) {
            throw new CoreException("SQL exception on prepare statement '?' : ?", sql, e.getMessage(), e);
        }
    }

    public PreparedStatement getPreparedStatement() {
        return preparedStatement;
    }

    // Do not add surround with try/catch, because use of ResultSet methods produce SQLException too,
    // so that surround actually do not add any convenience
    public ResultSet getResultSet() throws SQLException {
        return preparedStatement.getResultSet();
    }

    public String getSql() {
        return sql;
    }

    public ArrayList<Column> getColumns() {
        if ( columns != null ) {
            return columns;
        }
        ArrayList<Column> result = new ArrayList<>();

        try {
            // column with 0 index to uniform calls with set/get values
            Column column = new Column();
            result.add(column);
            ResultSetMetaData rsmd = getResultSet().getMetaData();
            for(int i=1; i <= rsmd.getColumnCount(); i++) {
                column = new Column();
                column.setName(rsmd.getColumnName(i));
                column.setLabel(rsmd.getColumnLabel(i));
                column.setJdbcType(rsmd.getColumnType(i));
                column.setJdbcTypeName(rsmd.getColumnTypeName(i));
                result.add(column);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        columns = result;
        return columns;
    }

    public void setObject(int index, Object parameter) {
        try {
            if ( parameter == null ) {
                connection.getDatabaseAdapter().setStatementNullParameter(this, index);
            } else if ( parameter instanceof BigDecimal) {
                preparedStatement.setBigDecimal(index, (BigDecimal) parameter);
            } else if ( parameter instanceof String ) {
                preparedStatement.setString(index, (String) parameter);
            } else if ( parameter  instanceof Date) {
                preparedStatement.setTimestamp(index, new java.sql.Timestamp(((Date) parameter).getTime()));
            } else {
                throw new CoreException("Wrong class '?' of statement parameter '?'", parameter.getClass().getName(), parameter.toString());
            }
        } catch (SQLException e) {
            throw new CoreException("Exception on set parameter ? value ? : ?", index, parameter, e.getMessage(), e);
        }
    }

    public void setParameters(Object... parameters) {
        for ( int i=0; i < parameters.length; i++) {
            setObject(i+1, parameters[i]);
        }
    }

    public void executeQuery(Object... parameters) {
        try {
            setParameters(parameters);
            resultSet = preparedStatement.executeQuery();
        } catch (SQLException e) {
            try {
                close();
            } catch (CoreException e1) {
                throw new CoreException("SQL exceptions on close statement with execute query failed '?' : \n? \n?", sql, e.getMessage(), e1.getMessage(), e);
            }
            throw new CoreException("SQL exception on execute statement '?' : ?", sql, e.getMessage(), e);
        }
    }

    public BigDecimal executeQueryBigDecimal(Object... parameters) {
        executeQuery(parameters);
        BigDecimal result = null;
        if ( next() ) {
            result = getBigDecimal(1);
        }
        if ( next() ) { close(); throw new CoreException("Exact fetch query return more than one row for '?'", sql); }
        close();
        return result;
    }

    public String executeQueryString(Object... parameters) {
        executeQuery(parameters);
        String result = null;
        if ( next() ) {
            result = getString(1);
        }
        if ( next() ) { close(); throw new CoreException("Exact fetch query return more than one row for '?'", sql); }
        close();
        return result;
    }

    public Date executeQueryDate(Object... parameters) {
        executeQuery(parameters);
        Date result = null;
        if ( next() ) {
            result = getDate(1);
        }
        if ( next() ) { close(); throw new CoreException("Exact fetch query return more than one row for '?'", sql); }
        close();
        return result;
    }

    public int executeUpdate(Object... parameters) {
        setParameters(parameters);
        int result;
        try {
            result = preparedStatement.executeUpdate();
            close();
        } catch (SQLException e) {
            throw new CoreException("SQL exception on execute statement '?' : ?", sql, e.getMessage(), e);
        } finally {
            close();
        }
        return result;
    }

    public boolean next() {
        try {
            return resultSet.next();
        } catch (SQLException e) {
            throw new CoreException("SQL exception on next row of statement '?' : ?", sql, e.getMessage(), e);
        }
    }

    public Object getObject(int columnIndex) {
        try {
            return resultSet.getObject(columnIndex);
        } catch (SQLException e) {
            throw new CoreException("SQL exception on get '?' column of statement '?' : ?", columnIndex, sql, e.getMessage(), e);
        }
    }
    
    public String getString(int columnIndex) {
        try {
            return resultSet.getString(columnIndex);
        } catch (SQLException e) {
            throw new CoreException("SQL exception on get '?' column of statement '?' : ?", columnIndex, sql, e.getMessage(), e);
        }
    }

    public String getString(String columnLabel) {
        try {
            return resultSet.getString(columnLabel);
        } catch (SQLException e) {
            throw new CoreException("SQL exception on get '?' column of statement '?' : ?", columnLabel, sql, e.getMessage(), e);
        }
    }

    public BigDecimal getBigDecimal(int columnIndex) {
        try {
            return resultSet.getBigDecimal(columnIndex);
        } catch (SQLException e) {
            throw new CoreException("SQL exception on get '?' column of statement '?' : ?", columnIndex, sql, e.getMessage(), e);
        }
    }

    public BigDecimal getBigDecimal(String columnLabel) {
        try {
            return resultSet.getBigDecimal(columnLabel);
        } catch (SQLException e) {
            throw new CoreException("SQL exception on get '?' column of statement '?' : ?", columnLabel, sql, e.getMessage(), e);
        }
    }

    public Date getDate(int columnIndex) {
        try {
            Timestamp timestamp = resultSet.getTimestamp(columnIndex);
            return timestamp != null ? new Date(timestamp.getTime()) : null;
        } catch (SQLException e) {
            throw new CoreException("SQL exception on get '?' column of statement '?' : ?", columnIndex, sql, e.getMessage(), e);
        }
    }

    public Date getDate(String columnLabel) {
        try {
            Timestamp timestamp = resultSet.getTimestamp(columnLabel);
            return timestamp != null ? new Date(timestamp.getTime()) : null;
        } catch (SQLException e) {
            throw new CoreException("SQL exception on get '?' column of statement '?' : ?", columnLabel, sql, e.getMessage(), e);
        }
    }

    public void close() {
        try {
            preparedStatement.close();
        } catch (SQLException e) {
            throw new CoreException("SQL exception on close statement '?' : ?", sql, e.getMessage(), e);
        }
    }

    @Override
    protected void finalize() throws Throwable {
        this.close();
        super.finalize();

    }
}
