/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator.component.service.element;

import org.clawiz.core.common.metadata.data.type.property.extension.AfterExtensionTypeProperty;
import org.clawiz.core.common.metadata.data.type.property.extension.BeforeExtensionTypeProperty;
import org.clawiz.core.common.metadata.data.type.property.extension.method.DeleteExtensionTypePropertyMethod;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;
import org.clawiz.core.common.utils.StringUtils;

public class TypeServiceDeleteMethodElement extends AbstractTypeServiceMethodElement {

    @Override
    public void process() {
        super.process();

        setName("delete");

        addParameter("BigDecimal", "id", "ID of deleted object");

        addJavaDoc("Delete object from database");

        String oldObjectName = "old" + StringUtils.toUpperFirstChar(getDataObjectParameterName());

        addText("");
        addText(getDataObjectClassName() + " " + oldObjectName + " = load(id);");
        addText("");
        addExtensions(BeforeExtensionTypeProperty.class, DeleteExtensionTypePropertyMethod.class);
        addText("TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);");
        addText("saveAudit(transactionAction, " + oldObjectName + ", null);");
        addText("");
        addText("getObjectService().deleteObject(id);");
        addText("");

        boolean addNewLine = false;
        for ( TypeField field : getFields() ) {
            if ( ! ( field.getValueType() instanceof ValueTypeObject )  ) {
                continue;
            }
            addNewLine = true;

            addText("if ("+ oldObjectName + "." + field.getGetMethodName() + "() != null) { getObjectService().deleteLink(" + oldObjectName + "." + field.getGetMethodName() + "(), id); }");
        }
        if ( addNewLine ) {
            addText("");
        }

        addText("executeUpdate(\"delete from " + getTableName() + " where id = ?\", id);");
        addText("");
        addExtensions(AfterExtensionTypeProperty.class, DeleteExtensionTypePropertyMethod.class);


    }
}
