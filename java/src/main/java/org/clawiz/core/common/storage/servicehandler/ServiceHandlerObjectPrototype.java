package org.clawiz.core.common.storage.servicehandler;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class ServiceHandlerObjectPrototype extends AbstractObject {
    
    /**
    * ServiceName
    */
    private java.lang.String serviceName;
    
    /**
    * Description
    */
    private java.lang.String description;
    
    public ServiceHandlerService service;
    
    /**
    * 
    * @return     ServiceName
    */
    public java.lang.String getServiceName() {
        return this.serviceName;
    }
    
    /**
    * Set 'ServiceName' value
    * 
    * @param      serviceName ServiceName
    */
    public void setServiceName(java.lang.String serviceName) {
        
        if ( serviceName != null && serviceName.length() > 512) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "ServiceHandler.ServiceName", "512");
        }
        
         this.serviceName = serviceName;
    }
    
    /**
    * Set 'ServiceName' value and return this object
    * 
    * @param      serviceName ServiceName
    * @return     ServiceHandler object
    */
    public ServiceHandlerObjectPrototype withServiceName(java.lang.String serviceName) {
        setServiceName(serviceName);
        return this;
    }
    
    /**
    * 
    * @return     Description
    */
    public java.lang.String getDescription() {
        return this.description;
    }
    
    /**
    * Set 'Description' value
    * 
    * @param      description Description
    */
    public void setDescription(java.lang.String description) {
        
        if ( description != null && description.length() > 512) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "ServiceHandler.Description", "512");
        }
        
         this.description = description;
    }
    
    /**
    * Set 'Description' value and return this object
    * 
    * @param      description Description
    * @return     ServiceHandler object
    */
    public ServiceHandlerObjectPrototype withDescription(java.lang.String description) {
        setDescription(description);
        return this;
    }
    
    public ServiceHandlerService getService() {
        return this.service;
    }
    
    public void setService(ServiceHandlerService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(ServiceHandlerObjectPrototype  target) {
        target.setServiceName(getServiceName());
        target.setDescription(getDescription());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((ServiceHandlerObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        service.save((ServiceHandlerObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(ServiceHandlerObjectPrototype object) {
        return 
               isObjectsEquals(this.getServiceName(), object.getServiceName() ) 
            && isObjectsEquals(this.getDescription(), object.getDescription() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getServiceName() != null ? getServiceName().hashCode() : 0);
        result = result * 31 + (getDescription() != null ? getDescription().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'Name' fields
    * 
    * @return     Concatenated string values of key 'toName' fields
    */
    public String toName() {
        return getServiceName();
    }
}
