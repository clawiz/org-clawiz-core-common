/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.module;

import org.clawiz.core.common.CoreCommonModule;
import org.clawiz.core.common.system.config.Config;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.system.service.extension.ServiceExtensionConfigList;

/**
 * (c) Clawiz
 */
public abstract class Module extends Service {

    private Config config;

    protected void fillDefinition(ModuleDefinition definition) {

    }


    public ModuleDefinition createDefinition() {
        ModuleDefinition definition = new ModuleDefinition();
        definition.setPackage(this.getClass().getPackage().getName());

        if ( this.getClass() != CoreCommonModule.class ) {
            definition.addRequire(CoreCommonModule.class);
        }
        fillDefinition(definition);
        return definition;
    }

    ModuleDefinition _definition;
    public ModuleDefinition getDefinition() {
        if ( _definition != null ) {
            return _definition;
        }
        _definition = createDefinition();
        return _definition;
    }

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }

    public ServiceExtensionConfigList getServiceExtensionConfigList() {
        ServiceExtensionConfigList configs = new ServiceExtensionConfigList();

        for (ModuleRequire require : getDefinition().getRequires() ) {

            configs.add( ((Module) getService(require.getModuleClass())).getServiceExtensionConfigList() );
        }

        return configs;
    }

    public void prepare() {
    }

    public void start() {
    }

    public void stop() {
    }


}
