package org.clawiz.core.common.storage.language;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class LanguageObjectPrototype extends AbstractObject {
    
    /**
    * Code
    */
    private java.lang.String code;
    
    /**
    * Name
    */
    private java.lang.String name;
    
    /**
    * Description
    */
    private java.lang.String description;
    
    public LanguageService service;
    
    /**
    * 
    * @return     Code
    */
    public java.lang.String getCode() {
        return this.code;
    }
    
    /**
    * Set 'Code' value
    * 
    * @param      code Code
    */
    public void setCode(java.lang.String code) {
        
        if ( code != null && code.length() > 7) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "Language.Code", "7");
        }
        
         this.code = code;
    }
    
    /**
    * Set 'Code' value and return this object
    * 
    * @param      code Code
    * @return     Language object
    */
    public LanguageObjectPrototype withCode(java.lang.String code) {
        setCode(code);
        return this;
    }
    
    /**
    * 
    * @return     Name
    */
    public java.lang.String getName() {
        return this.name;
    }
    
    /**
    * Set 'Name' value
    * 
    * @param      name Name
    */
    public void setName(java.lang.String name) {
        
        if ( name != null && name.length() > 75) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "Language.Name", "75");
        }
        
         this.name = name;
    }
    
    /**
    * Set 'Name' value and return this object
    * 
    * @param      name Name
    * @return     Language object
    */
    public LanguageObjectPrototype withName(java.lang.String name) {
        setName(name);
        return this;
    }
    
    /**
    * 
    * @return     Description
    */
    public java.lang.String getDescription() {
        return this.description;
    }
    
    /**
    * Set 'Description' value
    * 
    * @param      description Description
    */
    public void setDescription(java.lang.String description) {
        
        if ( description != null && description.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "Language.Description", "250");
        }
        
         this.description = description;
    }
    
    /**
    * Set 'Description' value and return this object
    * 
    * @param      description Description
    * @return     Language object
    */
    public LanguageObjectPrototype withDescription(java.lang.String description) {
        setDescription(description);
        return this;
    }
    
    public LanguageService getService() {
        return this.service;
    }
    
    public void setService(LanguageService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(LanguageObjectPrototype  target) {
        target.setCode(getCode());
        target.setName(getName());
        target.setDescription(getDescription());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((LanguageObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        service.save((LanguageObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(LanguageObjectPrototype object) {
        return 
               isObjectsEquals(this.getCode(), object.getCode() ) 
            && isObjectsEquals(this.getName(), object.getName() ) 
            && isObjectsEquals(this.getDescription(), object.getDescription() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getCode() != null ? getCode().hashCode() : 0);
        result = result * 31 + (getName() != null ? getName().hashCode() : 0);
        result = result * 31 + (getDescription() != null ? getDescription().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'code' fields
    * 
    * @return     Concatenated string values of key 'toCode' fields
    */
    public String toCode() {
        return getCode();
    }
    
    /**
    * Prepare concatenated string values of key 'name' fields
    * 
    * @return     Concatenated string values of key 'toName' fields
    */
    public String toName() {
        return getName();
    }
}
