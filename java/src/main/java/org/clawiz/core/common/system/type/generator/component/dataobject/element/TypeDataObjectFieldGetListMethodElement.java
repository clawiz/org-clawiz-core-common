/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator.component.dataobject.element;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;
import org.clawiz.core.common.utils.StringUtils;

public class TypeDataObjectFieldGetListMethodElement extends AbstractTypeDataObjectMethodElement {

    TypeField field;

    public TypeField getField() {
        return field;
    }

    public void setField(TypeField field) {
        this.field = field;
    }

    @Override
    public void process() {
        super.process();

        ValueTypeList valueTypeList = (ValueTypeList) field.getValueType();

        setName("get" + StringUtils.toUpperFirstChar(field.getJavaVariableName()));
        setType(field.getValueTypeJavaClassName());

/*
        addText("if ( " + field.getJavaVariableName() + " == null ) {");
        addText("    " + field.getJavaVariableName() + " = service.load" + valueTypeList.getReferencedType().getJavaClassName() + "List(id);");
        addText("}");
        addText("return " + field.getJavaVariableName() + ";");
*/

        addText("return new " + field.getValueTypeJavaClassName() + "();");


    }
}
