/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.utils.date;

import org.clawiz.core.common.CoreException;

/**
 * Created by IntelliJ IDEA.
 * User: abdrashitovta
 * Date: 04.08.11
 * Time: 15:03
 * To change this template use File | Settings | File Templates.
 */
public enum IntervalType {
    //После добавления нового типа интервала обязательно подправляй интервальные функции в DateUtils !
    MINUTE, HOUR, DAY, MONTH, WEEK, YEAR, QUARTER;

    public static IntervalType toIntervalType(String type) {

        if (type==null) {
            throw new CoreException("NULL interval type", new Object[]{type});
        }

        if ( type.equalsIgnoreCase("минута"))     { return MINUTE; }
        if ( type.equalsIgnoreCase("час"))     { return HOUR; }
        if ( type.equalsIgnoreCase("день"))    { return DAY; }
        if ( type.equalsIgnoreCase("неделя"))  { return WEEK; }
        if ( type.equalsIgnoreCase("месяц"))   { return MONTH; }
        if ( type.equalsIgnoreCase("квартал")) { return QUARTER; }
        if ( type.equalsIgnoreCase("год"))     { return YEAR; }

        try {
            return IntervalType.valueOf(type.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new CoreException("Wrong interval type ?", new Object[]{type});
        }
    }

}
