package org.clawiz.core.common.metadata.data.type.property.extension.action;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractExtensionTypePropertyActionPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    public AbstractExtensionTypePropertyAction withName(String value) {
        setName(value);
        return (AbstractExtensionTypePropertyAction) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
