/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.database.adapter.hsqldb;


import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.database.metadata.DatabaseSequence;
import org.clawiz.core.common.system.database.metadata.DatabaseTable;
import org.clawiz.core.common.system.database.metadata.DatabaseTableColumn;
import org.clawiz.core.common.system.database.adapter.DatabaseAdapter;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

public class HsqldbDatabaseAdapter extends DatabaseAdapter {


    @Override
    public boolean isSequenceExists(DatabaseSequence sequence) {
       return executeQueryBigDecimal(
               "select count(sequence_name) from information_schema.system_sequences where sequence_name = ?"
               , sequence.getName().toUpperCase()
       ).intValue() > 0;

    }

    @Override
    public void createSequence(DatabaseSequence sequence) {
        executeUpdate("create sequence " + sequence.getName() + " as bigint start with " + sequence.getStartWith(), new Object[]{});
    }

    public BigDecimal getSequenceNextValue(String sequenceName) {
        return executeQueryBigDecimal("select next value for " + sequenceName + " as nextval from dual");
    }

    @Override
    protected String getColumnDateTypeName() {
        return "DATE";
    }

    @Override
    protected String getColumnNumberTypeName() {
        return "NUMERIC";
    }

    @Override
    protected void loadTableColumns(DatabaseTable table) throws SQLException {
        Statement statement = executeQuery(
                "select column_name, type_name, column_size, num_prec_radix, decimal_digits, nullable " +
                        "from information_schema.system_columns where upper(table_name) = ?", table.getName().toUpperCase());
        ResultSet rs = statement.getResultSet();
        while (statement.next()) {
            DatabaseTableColumn column = new DatabaseTableColumn();
            table.addColumn(column);
            column.setName(statement.getString("column_name"));
            String typeName = statement.getString("type_name");
            if (typeName.equalsIgnoreCase(getColumnNumberTypeName())) {
                prepareDatabaseColumnType(column, typeName, rs.getInt("num_prec_radix"), rs.getInt("decimal_digits"));
            } else {
                prepareDatabaseColumnType(column, typeName, rs.getInt("column_size"), 0);
            }
            column.setRequired(rs.getInt("nullable") == 0);
        }
    }

    @Override
    protected void loadTableIndexes(DatabaseTable table) throws SQLException {
        throwException("HSQLDB database index processing not implemented");
    }

    @Override
    protected String getTableColumnTypeDDL(DatabaseTableColumn column) {
        switch (column.getType()) {
            case DATETIME:
            case DATE: return "TIMESTAMP(0)";
        }
        return super.getTableColumnTypeDDL(column);
    }

    @Override
    protected String getStringConcatenationOperator() {
        return "||";
    }



    @Override
    public void shutdownDatabase() {
        executeUpdate( "SHUTDOWN" );
    }

}
