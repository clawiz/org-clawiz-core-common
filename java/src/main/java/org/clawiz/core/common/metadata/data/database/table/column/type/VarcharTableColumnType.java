package org.clawiz.core.common.metadata.data.database.table.column.type;


import org.clawiz.core.common.system.database.metadata.DatabaseTableColumn;

import java.math.BigDecimal;

public class VarcharTableColumnType extends VarcharTableColumnTypePrototype {


    @Override
    public DatabaseTableColumn.Type getDatabaseType() {
        return DatabaseTableColumn.Type.VARCHAR;
    }

    @Override
    public BigDecimal getPrecision() {
        return super.getLength();
    }

    @Override
    public BigDecimal getScale() {
        return null;
    }
}
