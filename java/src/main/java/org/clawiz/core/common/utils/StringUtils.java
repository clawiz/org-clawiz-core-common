/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.utils;

import org.clawiz.core.common.CoreException;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * (C) Clawiz
 * User: abdrashitovta
 * Date: 11.06.2010
 * Time: 7?6?5
 */
public class StringUtils {

    public static Boolean toBoolean(String str) {
        if ( str == null ) {
            return false;
        }

        return (str.equalsIgnoreCase("true") || str.equalsIgnoreCase("yes")  || str.equalsIgnoreCase("t") || str.equalsIgnoreCase("да") || str.equalsIgnoreCase("1"));
    }

    public static String toUpperCase(String s) {
        return s != null ? s.toUpperCase() : null;
    }


    public static String toUpperFirstChar(String s) {
        if ( s == null ) {
            return null;
        }
        if ( s.length() < 1) {
            return s;
        }
        if ( s.length() == 1) {
            return s.toUpperCase();
        }
        return s.substring(0,1).toUpperCase() + substring(s, 1, s.length());
    }

    public static String toLowerFirstChar(String s) {
        if ( s == null ) {
            return null;
        }
        if ( s.length() < 1) {
            return s;
        }
        if ( s.length() == 1) {
            return s.toLowerCase();
        }
        return s.substring(0,1).toLowerCase() + substring(s, 1, s.length());
    }

    public static BigDecimal toBigDecimal(String str) {
        if ( str == null || str.length() == 0 ) {
            return null;
        }
        if ( str.equalsIgnoreCase("null")) {
            return null;
        }
        str = str.replace(',', '.').replaceAll(" ","").replaceAll(""+(char) 160,"");
        char[] chars = str.toCharArray();
        String s2 = "";
        for ( int i =0; i < chars.length; i++) {
            if ( chars[i] != ' ') {
                s2 += chars[i];
            }
        }
        str = str.replace(' ', ' ');
        try {
            return new BigDecimal(s2);
        } catch (Exception e) {
            throw new CoreException("Error преобразования числа '?' \n ?", new Object[]{str, e.toString()}, e);
        }
    }

    public static String trim(String str) {
        if ( str == null) {
            return null;
        }

        String result = str.trim();

        int lPos = 0;
        for ( ; lPos < result.length() && result.charAt(lPos)==160; lPos++);
        int rPos = result.length()-1;
        for ( ; rPos > lPos && result.charAt(rPos)==160; rPos--);

        return result.substring(lPos, rPos+1);
    }

    public static boolean isLike(String value, String pattern) {
        if ( value == null || pattern == null) {
           return false;
        }
        Pattern p = Pattern.compile(pattern.replaceAll("\\*", ".*").toUpperCase(), Pattern.DOTALL);

        Matcher m = p.matcher(value.toUpperCase());
        return m != null && m.matches();
    }

    public static String[] splitAndTrim(String str, String token ) {
        if ( str == null ) {
            return new String[]{};
        }
        String[] result = str.split(token);
        for ( int i=0; i < result.length; i++) {
            result[i] = trim(result[i]);
        }
        return result;
    }

    public static String[] splitByLastTokenOccurrence(String str, String token ) {
        if ( str == null ) {
            return new String[]{};
        }
        String[] tokens = str.split(token);

        String xtoken = null;
        if ( tokens.length > 1 ) {
            xtoken = str.substring(tokens[0].length(),  str.indexOf(tokens[1], tokens[0].length()+1)  );
        } else {
            return new String[]{str};
        }

        String[] result  = new String[2];
        result[1]        = tokens[tokens.length-1];
        StringBuilder sb = new StringBuilder();
        for ( int i=0; i < tokens.length-1; i++) {
            sb.append( i > 0 ? xtoken : "" ).append(trim(tokens[i]));
        }
        result[0] = sb.toString();
        return result;
    }

    public static String extractSurroundedString(String source, String leftStr, String rightStr) {
        if ( source == null
                || leftStr == null  || leftStr.length() < 1
                || rightStr == null || rightStr.length() < 1
                ) {
            return null;
        }
        int pos = source.toUpperCase().indexOf(leftStr.toUpperCase());
        if ( pos == -1 ) {
            return null;
        }

        String result = source.substring(pos + leftStr.length(), source.length());
        pos = result.toUpperCase().indexOf(rightStr.toUpperCase());
        if ( pos == -1 ) {
            return null;
        }

        return trim(result.substring(0, pos));
    }


    public static String removeDuplicates(String str, String searchStr) {
        if ( str == null || searchStr == null) {
            return null;
        }
        String newStr = str;
        int length = newStr.length()-1;
        while ( length != newStr.length()) {
            length = newStr.length();
            newStr = newStr.replaceAll(searchStr + searchStr, searchStr);
        }
        return newStr;
    }

    public static String substring(String str, int beginIndex, int lastIndex) {
        if ( str == null) {
            return null;
        }
        if ( beginIndex > str.length()) {
            return "";
        }
        int li = lastIndex;
        if ( li > str.length()) {
            li = str.length();
        }
        return str.substring(beginIndex, li);
    }

    public static ArrayList<String> splitByLength(String str, int length) {
        if ( str == null ) {
            return new ArrayList<String>();
        }
        ArrayList<String> lines = new ArrayList<String>();
        int index = 0;
        while ( index < str.length()) {
            lines.add(substring(str, index, index + length));
            index += length;
        }
        return lines;
    }

    public static boolean isEmpty(String string) {
        if ( string == null ) {
            return true;
        }
        return trim(string).length() == 0;
    }


    public static final String _spaceString = "                                                                                                   "
            +"                                                                                                    "
            +"                                                                                                    "
            +"                                                                                                    "
            +"                                                                                                    "
            +"                                                                                                    "
            +"                                                                                                    "
            +"                                                                                                    "
            +"                                                                                                    "
            +"                                                                                                    "
            +"                                                                                                    "
            +"                                                                                                    "
            +"                                                                                                    "
            +"                                                                                                    "
            +"                                                                                                    "
            +"                                                                                                    "
            +"                                                                                                    "
            +"                                                                                                    "
            +"                                                                                                    "
            +"                                                                                                    "
            +"                                                                                                    "
            +"                                                                                                    "
            ;

    public static String space(int length){
        return _spaceString.substring(0, length >= 0 ? (length < 2048 ? length : 2048) : 0);
    }

    public static String concatenateStrings(String delimiter, String... strings) {
        StringBuilder sb = new StringBuilder();
        String prefix = "";
        for (String str : strings ) {
            if ( str == null ) {
                continue;
            }
            sb.append(prefix);
            sb.append(str);
            prefix = delimiter;
        }

        return sb.toString();
    }

    public static String concatenateStrings(String delimiter, ArrayList<String> strings) {
        StringBuilder sb = new StringBuilder();
        String prefix = "";
        for (String str : strings ) {
            sb.append(prefix);
            sb.append(str);
            prefix = delimiter;
        }

        return sb.toString();
    }

    public static String escapeJavaString(String str) {
        if ( str == null ) {
            return str;
        }
        return str
                .replaceAll("\n", "\\\\n")
                .replaceAll("\r", "\\\\r")
                .replaceAll("\"", "\\\\\"");

    }

    public static String decodeUtf8(String utf8String) {
        byte[] bytes = utf8String.getBytes(StandardCharsets.UTF_8);
        return new String(bytes, StandardCharsets.UTF_8);    }

}
