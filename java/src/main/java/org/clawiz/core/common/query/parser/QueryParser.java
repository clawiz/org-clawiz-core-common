/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.query.parser;

import org.clawiz.core.common.query.Query;
import org.clawiz.core.common.query.element.column.AbstractQueryColumn;
import org.clawiz.core.common.query.element.column.QueryColumnTypeField;
import org.clawiz.core.common.query.element.column.QueryColumnType;
import org.clawiz.core.common.query.element.column.QueryColumnTypeRecordId;
import org.clawiz.core.common.query.element.filter.component.QueryFilterComponent;
import org.clawiz.core.common.query.element.filter.condition.AbstractQueryFilterCondition;
import org.clawiz.core.common.query.element.filter.condition.column.AbstractQueryFilterColumnCondition;
import org.clawiz.core.common.query.element.filter.condition.column.QueryFilterConditionTypeFieldColumn;
import org.clawiz.core.common.query.element.order.AbstractQueryOrder;
import org.clawiz.core.common.query.element.order.QueryOrderTypeField;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.type.key.TypeKeyField;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class QueryParser extends Service {

    private static final HashMap<String, String> reserverdWords;
    static {
        HashMap<String, String> words = new HashMap<>();
        words.put("NAME",      "");
        words.put("TIMESTAMP", "");
        words.put("PASSWORD",  "");
        words.put("DATA",      "");
        reserverdWords = words;
    }

    Query query;

    protected class Table {
        String                 tableName;
        String                 parentJoinColumn;
        String                 parentJoinAlias;
        String                 alias;
        ArrayList<Table>       childs = new ArrayList<>();

        @Override
        public String toString() {
            return "("+ tableName + " " + alias + (parentJoinColumn != null ? " -> " + parentJoinAlias + "." + parentJoinColumn : "" ) + ")";
        }
    }
    ArrayList<Table> tablesTree;
    ArrayList<Table> tables;

    protected class Column {
        Table       table;
        String      expression;
        String      alias;

        public Column(Table table, String expression, String alias) {
            this.table = table;
            this.expression = expression;
            this.alias = alias;
        }

        @Override
        public String toString() {
            return table.alias + "." + expression + " " + alias;
        }

    }
    ArrayList<Column>          columns;
    HashMap<String, String>    columnAliases;
    HashMap<TypeField, Column> typeFieldColumnsCache;

    protected void initCaches() {
        columns               = new ArrayList<>();
        tablesTree            = new ArrayList<>();
        tables                = new ArrayList<>();
        columnAliases         = new HashMap<>();
        typeFieldColumnsCache = new HashMap<>();
    }

    private String notNull(String s1) {
        return s1 != null ? s1 : "";
    }


    private Table checkTableInList(ArrayList<Table> lookupTables, ArrayList<Table> path, int pathIndex, Table parentTable) {

        Table table   = path.get(pathIndex);

        Table current = null;
        for (Table lt : lookupTables) {
            if (       table.tableName.equalsIgnoreCase(lt.tableName)
                    && notNull(table.parentJoinColumn).equalsIgnoreCase(notNull(lt.parentJoinColumn))
                    ) {
                current = lt;
                break;
            }
        }

        if ( current == null ) {

            lookupTables.add(table);
            current = table;
            tables.add(table);
            table.alias = "t" + tables.size();

        }

        if ( pathIndex > 0 ) {

            if ( current.parentJoinAlias != null && ! current.parentJoinAlias.equals(parentTable.alias) ) {
                // Algorithm error
                trace();
                throwException("System exception : parent join aliases not same : '?' != '?'", new Object[]{current.parentJoinAlias, parentTable.alias});
            }

            current.parentJoinAlias = parentTable.alias;
        }


        if ( pathIndex+1 < path.size() ) {
            return checkTableInList(current.childs, path, pathIndex+1, current);
        } else {
            return current;
        }



    }

    protected String prepareExpression(String expression, String replaceValue) {
        if ( expression == null ) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for(char c : expression.toCharArray()) {
            sb.append(c == '$' ? replaceValue : c);
        }
        return sb.toString();
    }

    protected void addColumn(Column column) {

        String alias = StringUtils.substring(column.alias != null ? column.alias : "c", 0, 27);
        int    i     = 1;
        while ( columnAliases.containsKey(alias) || reserverdWords.containsKey(alias.toUpperCase())) {
            alias = StringUtils.substring(column.alias, 0, 25) + "_" + i++;
        }

        column.alias = alias;
        columnAliases.put(alias, alias);

        columns.add(column);

    }

    private void addTypeModelToPath(TypeModel typeModel, ArrayList<Table> path) {
        Table table       = new Table();
        table.tableName  = typeModel.getType().getTableName();
        if ( typeModel.getJoinField() != null ) {
            table.parentJoinColumn = typeModel.getJoinField().getField().getColumnName();
        }

        path.add(table);
    }


    private void addTypeFieldColumnToPath(TypeFieldModel fieldModel, ArrayList<Table> path) {

        if ( fieldModel.getTypeModel().getJoinField() != null ) {
            addTypeFieldColumnToPath(fieldModel.getTypeModel().getJoinField(), path);
        }

        addTypeModelToPath(fieldModel.getTypeModel(), path);
    }

    protected void processTypeFieldColumn(QueryColumnTypeField queryColumn) {
        ArrayList<Table> path = new ArrayList<>();
        addTypeFieldColumnToPath(queryColumn.getFieldModel(), path);

        Table columnTable = checkTableInList(tablesTree, path, 0, null);

        Column parsedColumn = new Column(columnTable,
                prepareExpression(queryColumn.getExpression(), columnTable.alias + "." + queryColumn.getFieldModel().getField().getColumnName()),
                queryColumn.getName());
        addColumn(parsedColumn);
        typeFieldColumnsCache.put(queryColumn.getFieldModel().getField(), parsedColumn);

    }

    protected void processTypeRecordIdColumn(QueryColumnTypeRecordId queryColumn) {
        ArrayList<Table> path = new ArrayList<>();

        if ( queryColumn.getTypeModel().getJoinField() != null ) {
            addTypeFieldColumnToPath(queryColumn.getTypeModel().getJoinField(), path);
        }
        addTypeModelToPath(queryColumn.getTypeModel(), path);

        Table columnTable = checkTableInList(tablesTree, path, 0, null);

        addColumn(new Column(columnTable,
                prepareExpression(queryColumn.getExpression(), columnTable.alias + ".id"),
                queryColumn.getName()));
    }

    protected void processTypeColumn(Query query, QueryColumnType queryColumn) {

        TypeModel typeModel = queryColumn.getTypeModel();

        queryColumn.index(query.select().size());

        query.select().column(typeModel.id());
        for (TypeKeyField keyField : typeModel.getType().getToStringKey().getFields()) {
            query.select().column(typeModel.getByField(keyField.getField()));
        }

    }

    protected void prepareFilterComponent(QueryFilterComponent component) {
        for (QueryFilterComponent child : component.getComponents() ) {
            prepareFilterComponent(child);
        }
        for ( AbstractQueryFilterCondition condition : component.getConditions() ) {
            if ( condition instanceof QueryFilterConditionTypeFieldColumn ) {
                TypeFieldModel fieldModel = ((QueryFilterConditionTypeFieldColumn) condition).getFieldModel();
                if ( ! typeFieldColumnsCache.containsKey(fieldModel.getField()) ) {
                    QueryColumnTypeField qc = new QueryColumnTypeField();
                    qc.setQuery(query);
                    qc.fieldModel(fieldModel);
                    processTypeFieldColumn(qc);
                }
            }
        }
    }

    protected void checkConsistency() {

        if ( tables.size() == 0 ) {
            throwException("Query has not source tables");
        }
        if ( tablesTree.size() > 1 ) {
            StringBuilder sb = new StringBuilder();
            for ( int i=0; i < tablesTree.size(); i++) {
                sb.append((i > 0 ? ", " : "") + tablesTree.get(i).tableName);
            }
            throwException("Query has more than one root tables : ?", new Object[]{sb.toString()});
        }
    }


    protected void printFilterComponent(StringBuilder sb, QueryFilterComponent component) {

        if ( component.getComponents().size() > 0 ) {
            throwException("Nested components not implemented");
        }
        int lastIndex = component.getConditions().size() - 1;
        for (  int i=0; i <= lastIndex; i++) {

            sb.append("       ");

            AbstractQueryFilterCondition condition = component.getConditions().get(i);
            String expression = null;

            if ( condition instanceof QueryFilterConditionTypeFieldColumn ) {

                TypeField typeField = ((QueryFilterConditionTypeFieldColumn) condition).getFieldModel().getField();
                Column column = typeFieldColumnsCache.get(typeField);
                if ( column == null ) {
                    throwException("Type field ? from where clause not present in query columns", typeField.getFullName());
                }
                expression = column.expression;

            } else {
                throwException("Wrong query condition class ? ", condition.getClass().getName());
            }
            sb.append(expression);

            if ( condition instanceof AbstractQueryFilterColumnCondition ) {
                switch (((QueryFilterConditionTypeFieldColumn) condition).getOperator()) {
                    case EQUAL:            sb.append(" = ?"); break;
                    case NOT_EQUAL:        sb.append(" != ?"); break;
                    case LESS_THEN:        sb.append(" < ?"); break;
                    case GREATER_THEN:     sb.append(" > ?"); break;
                    case LESS_OR_EQUAL:    sb.append(" <= ?"); break;
                    case GREATER_OR_EQUAL: sb.append(" >= ?"); break;
                    case BETWEEN:          sb.append(" between ? and ?"); break;
                    case NULL:             sb.append(" is null"); break;
                    case NOTNULL:          sb.append(" is not null"); break;
                }
            }


            if ( i < lastIndex ) {
                if ( condition.getOperatorAfter() == null ) {
                    throwException("'AND' or 'OR' not defined after ?", expression);
                }
                sb.append(" " + condition.getOperatorAfter().name().toLowerCase());
            }

            sb.append("\n");

        }
    }

    protected void printQueryOrder(StringBuilder sb, AbstractQueryOrder order) {
        if ( order instanceof QueryOrderTypeField ) {
            TypeField typeField = ((QueryOrderTypeField) order).getFieldModel().getField();
            Column column = typeFieldColumnsCache.get(typeField);
            if ( column == null ) {
                throwException("Type field ? from order by clause not present in query columns", typeField.getFullName());
            }
            sb.append(column.expression + " " + order.getDirection().toString().toLowerCase());
        } else {
            throwException("Wrong query order class ?", order.getClass().getName());
        }
    }

    protected void trace() {
        logDebug("Query trace >>>>>");

        logDebug("Columns:");
        for(Column column : columns) {
            logDebug(column.expression + " " + column.alias );
        }

        logDebug("Tables:");
        for(Table table : tables) {
            logDebug(table.toString());
        }
        logDebug("           <<<<<");
    }

    protected String makeSql() {
        StringBuilder sb = new StringBuilder();

        sb.append("select\n");
        String prefix = "       ";
        for (Column column : columns ) {
            sb.append(prefix).append(column.expression).append(" ");
            if ( ! column.expression.equalsIgnoreCase(column.alias)) {
                sb.append(column.alias);
            }
            sb.append('\n');
            prefix = "      ,";
        }
        sb.append(" from\n");

        for (Table table : tables) {
            sb.append("       ");
            if ( table.parentJoinAlias == null ) {
                sb.append(table.tableName).append(" ").append(table.alias);
            } else {
                sb.append("left outer join ").append(table.tableName).append(" ").append(table.alias);
                sb.append(" on ").append(table.alias).append(".id");
                sb.append(" = ").append(table.parentJoinAlias).append(".").append(table.parentJoinColumn);
            }
            sb.append('\n');
        }


        StringBuilder where = new StringBuilder();
        printFilterComponent(where, query.where());
        if ( where.length() > 0 ) {
            sb.append(" where\n");
            sb.append(where);
        }

        boolean isFirst = true;
        for (AbstractQueryOrder order : query.orderBy() ) {
            sb.append(isFirst ? " order by " : ", ");
            isFirst = false;
            printQueryOrder(sb, order);
        }

        return sb.toString();
    }

    protected void processOrderBy() {

    }

    public String toSQL(Query query) {

        this.query = query;

        initCaches();

        for (int i = 0; i < query.select().size(); i++ ) {
            AbstractQueryColumn column = query.select().get(i);
            column.index(i);
            if ( column instanceof QueryColumnTypeField) {
                processTypeFieldColumn((QueryColumnTypeField) column);
            } else if ( column instanceof QueryColumnTypeRecordId) {
                processTypeRecordIdColumn((QueryColumnTypeRecordId) column);
            } else if ( column instanceof QueryColumnType) {
                processTypeColumn(query, (QueryColumnType) column);
            } else {
                throwException("Wrong query column class '?'", new Object[]{column});
            }
        }

        checkConsistency();

        prepareFilterComponent(query.where());

        processOrderBy();

        return makeSql();
    }




}
