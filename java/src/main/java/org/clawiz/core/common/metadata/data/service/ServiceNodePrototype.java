package org.clawiz.core.common.metadata.data.service;

import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ServiceNodePrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeElement
    private org.clawiz.core.common.metadata.data.service.method.ServiceMethodList methods = new org.clawiz.core.common.metadata.data.service.method.ServiceMethodList();
    
    public ServiceNode withName(String value) {
        setName(value);
        return (ServiceNode) this;
    }
    
    public org.clawiz.core.common.metadata.data.service.method.ServiceMethodList getMethods() {
        return this.methods;
    }
    
    public ServiceNode withMethod(org.clawiz.core.common.metadata.data.service.method.ServiceMethod value) {
        getMethods().add(value);
        return (ServiceNode) this;
    }
    
    public <T extends org.clawiz.core.common.metadata.data.service.method.ServiceMethod> T createMethod(Class<T> nodeClass) {
        org.clawiz.core.common.metadata.data.service.method.ServiceMethod value = createChildNode(nodeClass, "methods");
        getMethods().add(value);
        return (T) value;
    }
    
    public org.clawiz.core.common.metadata.data.service.method.ServiceMethod createMethod() {
        return createMethod(org.clawiz.core.common.metadata.data.service.method.ServiceMethod.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        for (MetadataNode node : getMethods()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        for (MetadataNode node : getMethods()) {
            references.add(node);
        }
        
    }
}
