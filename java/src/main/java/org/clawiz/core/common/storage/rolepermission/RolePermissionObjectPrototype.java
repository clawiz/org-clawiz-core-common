package org.clawiz.core.common.storage.rolepermission;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class RolePermissionObjectPrototype extends AbstractObject {
    
    /**
    * Role
    */
    private java.math.BigDecimal roleId;
    
    private org.clawiz.core.common.storage.role.RoleObject role;
    
    /**
    * Object
    */
    private java.math.BigDecimal objectId;
    
    private org.clawiz.core.common.storage.type.TypeObject object;
    
    /**
    * PermissionType
    */
    private java.math.BigDecimal permissionTypeId;
    
    private org.clawiz.core.common.storage.permissiontype.PermissionTypeObject permissionType;
    
    public RolePermissionService service;
    
    /**
    * 
    * @return     Role
    */
    public java.math.BigDecimal getRoleId() {
        return this.roleId;
    }
    
    public org.clawiz.core.common.storage.role.RoleObject getRole() {
        return getRole(false);
    }
    
    @SuppressWarnings("Duplicates")
    public org.clawiz.core.common.storage.role.RoleObject getRole(boolean autoCreate) {
        
        if ( role != null ) { return role; }
        
        if ( roleId != null ) {
            role = service.loadRole(roleId);
        } else if ( autoCreate ) {
            role = service.createRole();
        }
        
        return role;
    }
    
    /**
    * Set 'Role' value
    * 
    * @param      roleId Role
    */
    public void setRoleId(java.math.BigDecimal roleId) {
         this.roleId = roleId;
    }
    
    /**
    * Set 'Role' value and return this object
    * 
    * @param      roleId Role
    * @return     RolePermission object
    */
    public RolePermissionObjectPrototype withRoleId(java.math.BigDecimal roleId) {
        setRoleId(roleId);
        return this;
    }
    
    /**
    * 
    * @return     Object
    */
    public java.math.BigDecimal getObjectId() {
        return this.objectId;
    }
    
    public org.clawiz.core.common.storage.type.TypeObject getObject() {
        return getObject(false);
    }
    
    @SuppressWarnings("Duplicates")
    public org.clawiz.core.common.storage.type.TypeObject getObject(boolean autoCreate) {
        
        if ( object != null ) { return object; }
        
        if ( objectId != null ) {
            object = service.loadType(objectId);
        } else if ( autoCreate ) {
            object = service.createType();
        }
        
        return object;
    }
    
    /**
    * Set 'Object' value
    * 
    * @param      objectId Object
    */
    public void setObjectId(java.math.BigDecimal objectId) {
         this.objectId = objectId;
    }
    
    /**
    * Set 'Object' value and return this object
    * 
    * @param      objectId Object
    * @return     RolePermission object
    */
    public RolePermissionObjectPrototype withObjectId(java.math.BigDecimal objectId) {
        setObjectId(objectId);
        return this;
    }
    
    /**
    * 
    * @return     PermissionType
    */
    public java.math.BigDecimal getPermissionTypeId() {
        return this.permissionTypeId;
    }
    
    public org.clawiz.core.common.storage.permissiontype.PermissionTypeObject getPermissionType() {
        return getPermissionType(false);
    }
    
    @SuppressWarnings("Duplicates")
    public org.clawiz.core.common.storage.permissiontype.PermissionTypeObject getPermissionType(boolean autoCreate) {
        
        if ( permissionType != null ) { return permissionType; }
        
        if ( permissionTypeId != null ) {
            permissionType = service.loadPermissionType(permissionTypeId);
        } else if ( autoCreate ) {
            permissionType = service.createPermissionType();
        }
        
        return permissionType;
    }
    
    /**
    * Set 'PermissionType' value
    * 
    * @param      permissionTypeId PermissionType
    */
    public void setPermissionTypeId(java.math.BigDecimal permissionTypeId) {
         this.permissionTypeId = permissionTypeId;
    }
    
    /**
    * Set 'PermissionType' value and return this object
    * 
    * @param      permissionTypeId PermissionType
    * @return     RolePermission object
    */
    public RolePermissionObjectPrototype withPermissionTypeId(java.math.BigDecimal permissionTypeId) {
        setPermissionTypeId(permissionTypeId);
        return this;
    }
    
    public RolePermissionService getService() {
        return this.service;
    }
    
    public void setService(RolePermissionService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(RolePermissionObjectPrototype  target) {
        target.setRoleId(getRoleId());
        target.setObjectId(getObjectId());
        target.setPermissionTypeId(getPermissionTypeId());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((RolePermissionObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        if ( role != null ) {
            role.save();
            roleId = role.getId();
        }
        if ( object != null ) {
            object.save();
            objectId = object.getId();
        }
        if ( permissionType != null ) {
            permissionType.save();
            permissionTypeId = permissionType.getId();
        }
        service.save((RolePermissionObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(RolePermissionObjectPrototype object) {
        return 
               isObjectsEquals(this.getRoleId(), object.getRoleId() ) 
            && isObjectsEquals(this.getObjectId(), object.getObjectId() ) 
            && isObjectsEquals(this.getPermissionTypeId(), object.getPermissionTypeId() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getRoleId() != null ? getRoleId().hashCode() : 0);
        result = result * 31 + (getObjectId() != null ? getObjectId().hashCode() : 0);
        result = result * 31 + (getPermissionTypeId() != null ? getPermissionTypeId().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'RoleObjectPermission' fields
    * 
    * @return     Concatenated string values of key 'toRoleObjectPermission' fields
    */
    public String toRoleObjectPermission() {
        return service.getObjectService().idToString(getRoleId()) + "," + service.getObjectService().idToString(getObjectId()) + "," + service.getObjectService().idToString(getPermissionTypeId());
    }
}
