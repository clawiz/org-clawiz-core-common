package org.clawiz.core.common.storage.typefieldvalue;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;

public class TypeFieldValueServicePrototype extends AbstractTypeService<TypeFieldValueObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).fullNameToId("org.clawiz.core.common.storage.TypeFieldValue");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeFieldValue"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      typeFieldValue Checked object
    */
    public void check(TypeFieldValueObject typeFieldValue) {
        
        if ( typeFieldValue == null ) {
            throwException("Cannot check null ?", new Object[]{"TypeFieldValueObject"});
        }
        
        typeFieldValue.fillDefaults();
        
        
        if ( typeFieldOrderNoToId(typeFieldValue.getTypeFieldId(), typeFieldValue.getOrderNo(), typeFieldValue.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "TypeFieldValue", typeFieldValue.toTypeFieldOrderNo() });
        }
        
        if ( typeFieldValueToId(typeFieldValue.getTypeFieldId(), typeFieldValue.getValue(), typeFieldValue.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "TypeFieldValue", typeFieldValue.toTypeFieldValue() });
        }
        
    }
    
    /**
    * Create new TypeFieldValueObject instance and fill default values
    * 
    * @return     Created object
    */
    public TypeFieldValueObject create() {
        
        TypeFieldValueObject typeFieldValue = new TypeFieldValueObject();
        typeFieldValue.setService((TypeFieldValueService) this);
        
        typeFieldValue.fillDefaults();
        
        return typeFieldValue;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public TypeFieldValueObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select type_field_id, order_no, value, description from cw_core_type_field_values where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"TypeFieldValue", id});
        }
        
        TypeFieldValueObject result = new TypeFieldValueObject();
        
        result.setService((TypeFieldValueService) this);
        result.setId(id);
        result.setTypeFieldId(statement.getBigDecimal(1));
        result.setOrderNo(statement.getBigDecimal(2));
        result.setValue(statement.getString(3));
        result.setDescription(statement.getString(4));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of TypeFieldValueObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public TypeFieldValueList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of TypeFieldValueObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public TypeFieldValueList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        TypeFieldValueList result = new TypeFieldValueList();
        
        
        Statement statement = executeQuery("select id, type_field_id, order_no, value, description from cw_core_type_field_values"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            TypeFieldValueObject object = new TypeFieldValueObject();
        
            object.setService((TypeFieldValueService) this);
            object.setId(statement.getBigDecimal(1));
            object.setTypeFieldId(statement.getBigDecimal(2));
            object.setOrderNo(statement.getBigDecimal(3));
            object.setValue(statement.getString(4));
            object.setDescription(statement.getString(5));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'TypeFieldOrderNo' fields
    * 
    * @param      typeFieldId TypeField
    * @param      orderNo     OrderNo
    * @return     Id of found record or null
    */
    public BigDecimal typeFieldOrderNoToId(java.math.BigDecimal typeFieldId, java.math.BigDecimal orderNo) {
        return typeFieldOrderNoToId(typeFieldId, orderNo, null);
    }
    
    /**
    * Find id of record by key 'TypeFieldOrderNo' fields with id not equal skipId
    * 
    * @param      typeFieldId TypeField
    * @param      orderNo     OrderNo
    * @param      skipId      Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal typeFieldOrderNoToId(java.math.BigDecimal typeFieldId, java.math.BigDecimal orderNo, BigDecimal skipId) {
        
        if ( typeFieldId == null || orderNo == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_type_field_values where type_field_id = ? and order_no = ?", typeFieldId, orderNo);
        } else {
            return executeQueryBigDecimal("select id from cw_core_type_field_values where type_field_id = ? and order_no = ? and id != ?", typeFieldId, orderNo, skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'TypeFieldOrderNo' fields or create new record with values set to given parameters
    * 
    * @param      typeFieldId         TypeField
    * @param      orderNo             OrderNo
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal typeFieldOrderNoToId(java.math.BigDecimal typeFieldId, java.math.BigDecimal orderNo, boolean createNewIfNotFound) {
        
        BigDecimal id = typeFieldOrderNoToId(typeFieldId, orderNo, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        TypeFieldValueObject object = create();
        object.setTypeFieldId(typeFieldId);
        object.setOrderNo(orderNo);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'TypeFieldOrderNo' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToTypeFieldOrderNo(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select type_field_id, order_no from cw_core_type_field_values where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        TypeFieldValueObject object = new TypeFieldValueObject();
        object.setService((TypeFieldValueService)this);
        object.setId(id);
        object.setTypeFieldId(statement.getBigDecimal(1));
        object.setOrderNo(statement.getBigDecimal(2));
        
        statement.close();
        
        return object.toTypeFieldOrderNo();
    }
    
    /**
    * Find id of record by key 'TypeFieldValue' fields
    * 
    * @param      typeFieldId TypeField
    * @param      value       Value
    * @return     Id of found record or null
    */
    public BigDecimal typeFieldValueToId(java.math.BigDecimal typeFieldId, java.lang.String value) {
        return typeFieldValueToId(typeFieldId, value, null);
    }
    
    /**
    * Find id of record by key 'TypeFieldValue' fields with id not equal skipId
    * 
    * @param      typeFieldId TypeField
    * @param      value       Value
    * @param      skipId      Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal typeFieldValueToId(java.math.BigDecimal typeFieldId, java.lang.String value, BigDecimal skipId) {
        
        if ( typeFieldId == null || value == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_type_field_values where type_field_id = ? and upper_value = ?", typeFieldId, value.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_type_field_values where type_field_id = ? and upper_value = ? and id != ?", typeFieldId, value.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'TypeFieldValue' fields or create new record with values set to given parameters
    * 
    * @param      typeFieldId         TypeField
    * @param      value               Value
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal typeFieldValueToId(java.math.BigDecimal typeFieldId, java.lang.String value, boolean createNewIfNotFound) {
        
        BigDecimal id = typeFieldValueToId(typeFieldId, value, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        TypeFieldValueObject object = create();
        object.setTypeFieldId(typeFieldId);
        object.setValue(value);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'TypeFieldValue' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToTypeFieldValue(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select type_field_id, value from cw_core_type_field_values where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        TypeFieldValueObject object = new TypeFieldValueObject();
        object.setService((TypeFieldValueService)this);
        object.setId(id);
        object.setTypeFieldId(statement.getBigDecimal(1));
        object.setValue(statement.getString(2));
        
        statement.close();
        
        return object.toTypeFieldValue();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToTypeFieldValue(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      typeFieldValueObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(TypeFieldValueObject typeFieldValueObject) {
        return typeFieldValueObject.toTypeFieldValue();
    }
    
    protected void saveAudit(TransactionAction transactionAction, TypeFieldValueObject oldTypeFieldValueObject, TypeFieldValueObject newTypeFieldValueObject) {
        
        TypeFieldValueObject o = oldTypeFieldValueObject != null ? oldTypeFieldValueObject : new TypeFieldValueObject();
        TypeFieldValueObject n = newTypeFieldValueObject != null ? newTypeFieldValueObject : new TypeFieldValueObject();
        
        
        executeUpdate("insert into a_cw_core_type_field_values (scn, action_type, id , o_type_field_id, o_order_no, o_value, o_upper_value, o_description, n_type_field_id, n_order_no, n_value, n_upper_value, n_description) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getTypeFieldId(), o.getOrderNo(), o.getValue(), o.getValue() != null ? o.getValue().toUpperCase() : null, o.getDescription(), n.getTypeFieldId(), n.getOrderNo(), n.getValue(), n.getValue() != null ? n.getValue().toUpperCase() : null, n.getDescription());
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      typeFieldValue Saved object
    */
    public void save(TypeFieldValueObject typeFieldValue) {
        
        if ( typeFieldValue == null ) {
            throwException("Cannot save NULL ?", new Object[]{"TypeFieldValueObject"});
        }
        
        TransactionAction transactionAction;
        TypeFieldValueObject oldTypeFieldValue;
        if ( typeFieldValue.getService() == null ) {
            typeFieldValue.setService((TypeFieldValueService)this);
        }
        
        check(typeFieldValue);
        
        if ( typeFieldValue.getId() == null ) {
        
            typeFieldValue.setId(getObjectService().createObject(getTypeId()));
        
            oldTypeFieldValue = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, typeFieldValue.getId());
        
            executeUpdate("insert into cw_core_type_field_values" 
                          + "( id, type_field_id, order_no, value, upper_value, description ) "
                          + "values"
                          + "(?, ?, ?, ?, ?, ?)",
                          typeFieldValue.getId(), typeFieldValue.getTypeFieldId(), typeFieldValue.getOrderNo(), typeFieldValue.getValue(), ( typeFieldValue.getValue() != null ? typeFieldValue.getValue().toUpperCase() : null ), typeFieldValue.getDescription() );
        
            if ( typeFieldValue.getTypeFieldId() != null ) { getObjectService().setLink(typeFieldValue.getTypeFieldId(), typeFieldValue.getId()); }
        
        } else {
        
            oldTypeFieldValue = load(typeFieldValue.getId());
            if ( oldTypeFieldValue.equals(typeFieldValue) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, typeFieldValue.getId());
        
            executeUpdate("update cw_core_type_field_values set "
                          + "type_field_id = ?, order_no = ?, value = ?, upper_value = ?, description = ? "
                          + "where id = ?",
                          typeFieldValue.getTypeFieldId(), typeFieldValue.getOrderNo(), typeFieldValue.getValue(), ( typeFieldValue.getValue() != null ? typeFieldValue.getValue().toUpperCase() : null ), typeFieldValue.getDescription(), typeFieldValue.getId() );
        
            getObjectService().changeLinkParent(oldTypeFieldValue.getTypeFieldId(), typeFieldValue.getTypeFieldId(), typeFieldValue.getId());
        
        }
        
        saveAudit(transactionAction, oldTypeFieldValue, typeFieldValue);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        TypeFieldValueObject oldTypeFieldValueObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldTypeFieldValueObject, null);
        
        getObjectService().deleteObject(id);
        
        if (oldTypeFieldValueObject.getTypeFieldId() != null) { getObjectService().deleteLink(oldTypeFieldValueObject.getTypeFieldId(), id); }
        
        executeUpdate("delete from cw_core_type_field_values where id = ?", id);
        
    }
}
