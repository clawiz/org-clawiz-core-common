/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.config;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.utils.StringUtils;

/**
 * (c) Clawiz
 * Date: 05.03.2010
 * Time: 13?2?4
 */
public abstract class AbstractConfig implements Config {

    private ConfigList childs = new ConfigList();
    private String     name;
    private String     value;
    private Config     parent;


    public String getPath() {
        return (parent != null ? parent.getName() + "." : "") + name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Config getParent() {
        return parent;
    }

    public void setParent(Config parent) {
        this.parent = parent;
    }

    public Config addChild() {
        Config child = null;
        try {
            child = this.getClass().newInstance();
        } catch (InstantiationException e) {
            throw new CoreException("InstantiationException on '?' new instance", this.getClass(), e);
        } catch (IllegalAccessException e) {
            throw new CoreException("IllegalAccessException on '?' new instance", this.getClass(), e);
        }
        child.setParent(this);
        childs.add(child);
        return child;
    }


    public ConfigList getChilds() {
        return childs;
    }


    private void _lookupTokensToList(AbstractConfig parent, ConfigList list, String[] tokens, int level) {
        String mask       = tokens[level];
        boolean lastLevel = tokens.length-1 == (level);
        for (Config child : parent.getChilds()) {
            if ( mask.equalsIgnoreCase(child.getName())) {
                if ( lastLevel ) {
                    list.add(child);
                } else {
                    ((AbstractConfig)child)._lookupTokensToList(((AbstractConfig) child), list, tokens, level + 1);
                }
            }
        }
    }

    @Override
    public ConfigList getConfigList(String path) {
        ConfigList result = new ConfigList();
        if ( path == null ) {
            return result;
        }
        String[] tokens = StringUtils.splitAndTrim(path, "\\.");
        _lookupTokensToList(this, result, tokens, 0);

        return result;
    }

    public Config getConfig(String path) {
        return getConfig(path, false);
    }

    @Override
    public Config getConfig(String path, boolean exceptionOnNull) {
        ConfigList list = getConfigList(path);
        if ( list.size() > 1 ) {
            throw new CoreException("Found more than one config node '?' in node '?'", path, getPath());
        }
        if ( list.size() == 0 ) {
            if ( exceptionOnNull ) {
                throw new CoreException("Config node '?' not found in node '?'", path, getPath());
            }
            return null;
        }
        return list.get(0);
    }


    @Override
    public String getString(String path) {
        return getString(path, false);
    }

    @Override
    public String getString(String path, String notNullValue) {
        String result = getString(path, false);
        return result != null ? result : notNullValue;
    }

    @Override
    public String getString(String path, boolean exceptionOnNull) {
        Config config = getConfig(path, exceptionOnNull);
        return config != null ? config.getValue() : null;
    }
}