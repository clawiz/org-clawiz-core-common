/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.security;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.ObjectService;
import org.clawiz.core.common.system.service.Service;



import java.math.BigDecimal;

/**
 * (C) Clawiz
 * User: abdrashitovta
 * Date: 20.11.2005
 * Time: 20?3?3
 * To change this template use File | Settings | File Templates.
 */
public class SecurityService extends Service {

    private BigDecimal localAccessGroupId;
    private BigDecimal localListsAccessGroupId;
    private BigDecimal localAllListsAccessGroupId;
//    private GroupService groupService;
    private ObjectService objectService;
    private boolean protectedMode;

    private  BigDecimal vAccessTypesGroupId;
    private  BigDecimal vAccessKeysGroupId;

    private BigDecimal readAccessTypeId;
    private BigDecimal writeAccessTypeId;
    private BigDecimal delAccessTypeId;
    private BigDecimal adminAccessTypeId;
    private BigDecimal grantAdminAccessTypeId;

    private BigDecimal adminKeyNo;
    private BigDecimal publicKeyNo;
    private String sessionKey;



    public void init() {

/*
        protectedMode = false;
        localAccessGroupId          = groupService.nameToId("DATA ACCESS", groupService.getPrimaryGroupId());
        localListsAccessGroupId     = groupService.nameToId("LISTS", localAccessGroupId);
        localAllListsAccessGroupId  = groupService.nameToId("ALL LISTS", localListsAccessGroupId);
        sessionKey = getSession().getKey();

        vAccessTypesGroupId = groupService.pathToId(SecurityService.CT_DICTIONARY_ACCESS_TYPES_GROUP_PATH, dictionaryService.getLocalListGroupId());
        vAccessKeysGroupId = groupService.pathToId(SecurityService.CT_DICTIONARY_ACCESS_KEYS_GROUP_PATH, dictionaryService.getLocalListGroupId());


        // if on install phase then skip resolve
        if ( vAccessKeysGroupId != null && vAccessTypesGroupId != null) {
            readAccessTypeId            = dictionaryService.codeToId(CT_ACCESS_TYPE_READ_CODE, vAccessTypesGroupId);
            writeAccessTypeId           = dictionaryService.codeToId(CT_ACCESS_TYPE_WRITE_CODE, vAccessTypesGroupId);
            delAccessTypeId             = dictionaryService.codeToId(CT_ACCESS_TYPE_DEL_CODE, vAccessTypesGroupId);
            adminAccessTypeId           = dictionaryService.codeToId(CT_ACCESS_TYPE_ADMIN_CODE, vAccessTypesGroupId);
            grantAdminAccessTypeId      = dictionaryService.codeToId(CT_ACCESS_TYPE_GRANT_ADMIN_CODE, vAccessTypesGroupId);

            adminKeyNo                  = dictionaryService.codeToId(CT_ACESS_KEY_ADMIN_CODE, vAccessKeysGroupId);
            publicKeyNo                 = dictionaryService.codeToId(CT_ACESS_KEY_PUBLIC_CODE, vAccessKeysGroupId);
        }
*/


    }

    public void destroy() {

    }

    public boolean isPermitted(BigDecimal id, int atnNo) {
//        if ( atnNo == ATN_READ_id ) {
//            return isPermitted(id, readAccessTypeId);
//        }
//        if ( atnNo == ATN_WRITE_id ) {
//            return isPermitted(id, writeAccessTypeId);
//        }
//        if ( atnNo == ATN_DEL_id ) {
//            return isPermitted(id, delAccessTypeId);
//        }
//        if ( atnNo == ATN_ADMIN_id ) {
//            return isPermitted(id, adminAccessTypeId);
//        }
        return isPermitted(id, new BigDecimal(atnNo));
    }


    public boolean isPermitted(BigDecimal id, BigDecimal atnNo) {
/*
        if ( isAdminSession() ) { return true; }

        if ( atnNo.equals(readAccessTypeId)) {
            if ( executeSelectBigDecimal("select object_id from cw_core_object_shares sh, cw_core_session_roles rl "+
                                           "where sh.object_id      = :o and "+
                                           "      rl.key_id         = sh.key_id and "+
                                           "      rl.access_type_id = :a and rl.session_id = :s", new Object[]{id, atnNo, sessionKey}) != null) {
                return true;
            }
        } else {
            if ( executeSelectBigDecimal("select object_id from cw_core_object_shares sh, cw_core_session_roles rl "+
                                           "where sh.object_id      = :o and sh.read_only = :f and "+
                                           "      rl.key_id         = sh.key_id and "+
                                           "      rl.access_type_id = :a and rl.session_id = :s", new Object[]{id, "F", atnNo, sessionKey}) != null) {
                return true;
            }
        }
        if ( ! atnNo.equals(this.grantAdminAccessTypeId)
                ) {
            if ( executeSelectBigDecimal("select object_id from cw_core_object_shares sh, cw_core_session_roles rl "+
                                           "where sh.object_id      = :o and "+
                                           "      rl.key_id         = sh.key_id and "+
                                           "      rl.access_type_id = :a and rl.session_id = :s", new Object[]{id, adminAccessTypeId, sessionKey}) != null) {
                return true;
            }

        }
        return false;
*/
        return true;
    }

    public boolean isAdminSession() {
        return true;
    }


    protected void makeAddShare(BigDecimal objectId, BigDecimal keyNo, boolean readOnly) {
/*
        if ( objectId == null || keyNo == null ) {
            return;
        }
        if ( ! protectedMode && (! isPermitted(objectId, adminAccessTypeId) || ! isPermitted(keyNo, adminAccessTypeId)) ) {
            throwError("Not permitted to add share", new Object[]{objectId, keyNo});
        }

        String line = "F";
        if ( readOnly ) {
            line="T";
        }
        String check = executeSelectString("select read_only from cw_core_object_shares where object_id = :o and key_id = :k", new Object[]{objectId, keyNo});
        if ( check == null ) {
            executeStatement("insert into cw_core_object_shares (object_id, key_id, read_only) values(:o,:k,:r)", new Object[]{objectId, keyNo, line});
        } else {
            if ( readOnly && check.equals("T")) {
                executeStatement("update cw_core_object_shares set read_only = :r where object_id = :o and key_id = :k", new Object[]{line, objectId,  keyNo});
            }
        }
*/

    }

    private void addShareToParents(BigDecimal objectId, BigDecimal keyNo, BigDecimal skipparentId) {
/*
        DataSet parentsSet;
        BigDecimal groupId;

        try {
            if ( skipparentId != null ) {
                parentsSet = executeSelect("select group_id from cw_core_group_objects where object_id = :o and group_id <> :s", new Object[]{objectId, skipparentId});
            } else {
                parentsSet = executeSelect("select group_id from cw_core_group_objects where object_id = :o", new Object[]{objectId});
            }
            while ( ! parentsSet.isEOF() ) {
                java.math.BigDecimal check = executeSelectBigDecimal("select object_id from cw_core_object_shares where object_id = :o and key_id = :k", new Object[]{objectId, keyNo});
                if ( check == null) {
                    groupId = parentsSet.getBigDecimal(0);
                    makeAddShare(groupId, keyNo, true);
                    addShareToParents(groupId, keyNo, skipparentId);
                }
                parentsSet.next();
            }
            parentsSet.close();
        } catch ( Exception e) {
            throwError("Add share to parents exception", e);
        }
*/
    }

    private void addShareToChilds(BigDecimal groupId, BigDecimal keyNo) {
/*
        DataSet childsSet;
        BigDecimal childId;

        try {
                childsSet = executeSelect("select group_id from cw_core_group_objects where group_id = :o", new Object[]{groupId});
            while ( ! childsSet.isEOF() ) {
                java.math.BigDecimal check = executeSelectBigDecimal("select object_id from cw_core_object_shares where object_id = :o and key_id = :k", new Object[]{groupId, keyNo});
                if ( check == null) {
                    childId = childsSet.getBigDecimal(0);
                    makeAddShare(childId, keyNo, false);
                    addShareToChilds(childId, keyNo);
                }
                childsSet.next();
            }
            childsSet.close();
        } catch ( Exception e) {
            throwError("Add share to childs exception", e);
        }
*/
    }

    public void addShare(BigDecimal objectId, BigDecimal keyNo) {
/*
        if (objectId == null || keyNo == null) {
            throwError("Set both objects for add share", new Object[]{objectId, keyNo});
        }
        String check = executeSelectString("select read_only from cw_core_object_shares where object_id = :o and key_id = :k", new Object[]{objectId,  keyNo});
        if ( check != null && check.equals("F")) {
            return;
        }
        makeAddShare(objectId, keyNo, false);
        addShareToParents(objectId, keyNo, null);
        addShareToChilds(objectId, keyNo);
*/
    }

    private void makeDelShare(BigDecimal objectId, BigDecimal keyNo) {
/*
        if ( ! protectedMode && (! isPermitted(objectId, adminAccessTypeId) || ! isPermitted(keyNo, adminAccessTypeId)) ) {
            throwException("Not permitted to del share", new Object[]{objectId, keyNo});
        }

        DataSet resultSet;
        try {
            resultSet = executeSelect("select group_id from cw_core_group_objects where object_id = :o", new Object[]{objectId});
            while ( ! resultSet.isEOF() ) {
                if ( executeSelectBigDecimal("select key_id from cw_core_object_shares where object_id = :g and key_id = :k and read_only = :r", new Object[]{resultSet.getBigDecimal(0), keyNo, "F"}) != null) {
                    resultSet.close();
                    return;
                }
                resultSet.next();
            }
            resultSet.close();

            executeStatement("delete from cw_core_object_shares where object_id = :o and key_id = :k", new Object[]{objectId, keyNo});

            resultSet = executeSelect("select object_id from cw_core_group_objects where group_id = :g", new Object[]{objectId});
            while (! resultSet.isEOF()) {
                makeDelShare( resultSet.getBigDecimal(0), keyNo);
                resultSet.next();
            }
            resultSet.close();


        }   catch ( Exception e ) {
            throwException("Exception on del share", e);
        }

*/
    }

    private void delShareFromParents(BigDecimal objectId, BigDecimal keyNo) {
/*
        try {
            DataSet resultSet;
            boolean found;
            resultSet = executeSelect("select group_id from cw_core_group_objects where object_id = :o", new Object[]{objectId});
            while (resultSet.next()) {
                found = false;
                DataSet childsSet = executeSelect("select object_id from cw_core_group_objects where group_id = :g and object_id <> :o", new Object[]{resultSet.getBigDecimal(0), objectId});
                while (! childsSet.isEOF()) {
                    if (executeSelectBigDecimal("select key_id from cw_core_object_shares where object_id = :o and key_id = :k", new Object[]{childsSet.getBigDecimal(0), keyNo}) != null) {
                        found = true;
                        break;
                    }
                    childsSet.next();
                }
                childsSet.close();
                if ( ! found ) {
                    executeStatement("delete from cw_core_object_shares where object_id = :o and key_id = :k and read_only=:r", new Object[]{resultSet.getBigDecimal(0), keyNo, "T"});
                }
            }
            resultSet.close();
        }   catch ( Exception e) {
            throwError("Exception on del share", e);
        }
*/
    }

    public void delShare(BigDecimal objectId, BigDecimal keyNo) {
/*
        if ( executeSelectString("select read_only from cw_core_object_shares where object_id = :o and key_id = :k", new Object[]{objectId,  keyNo}) == null ) {
            return;
        }
        makeDelShare(objectId, keyNo);
        delShareFromParents(objectId,  keyNo);
        executeStatement("delete from cw_core_object_shares where object_id = :o and key_id = :k", new Object[]{objectId, keyNo});
*/
    }

    public void addRole(BigDecimal objectId, BigDecimal keyNo, BigDecimal accessTypeId) {
/*
        if ( executeSelectBigDecimal("select object_id from cw_core_object_roles where object_id = :o and key_id = :k and access_type_id = :a", new Object[]{objectId, keyNo, accessTypeId}) != null) {
            return;
        }

        if ( ! isPermitted(objectId, adminAccessTypeId) || ! isPermitted(keyNo, adminAccessTypeId) ) {
            throwException("Not permitted to add role", new Object[]{objectId, keyNo});
        }

        if ( accessTypeId.equals(adminAccessTypeId) &&
             ( ! isPermitted(objectId, grantAdminAccessTypeId) || ! isPermitted(keyNo, grantAdminAccessTypeId))) {
            throwException("Not permitted to add role", new Object[]{objectId, keyNo});
        }

        executeStatement("insert into cw_core_object_roles (object_id, key_id, access_type_id) values (:o, :k, :a)", new Object[]{objectId, keyNo, accessTypeId});

        try {
            DataSet objectsSet = executeSelect("select object_id from cw_core_group_objects where group_id = :g", new Object[]{objectId});
            while ( ! objectsSet.isEOF()) {
                addRole(objectsSet.getBigDecimal(0), keyNo, accessTypeId);
                objectsSet.next();
            }
            objectsSet.close();
        }   catch( Exception e) {
            throwException("Exception on add role", e);
        }

*/
    }

    public void delRole(BigDecimal objectId, BigDecimal keyNo, BigDecimal accessTypeId) {
/*
        if ( executeSelectBigDecimal("select object_id from cw_core_object_roles where object_id = :o and key_id = :k and access_type_id = :a", new Object[]{objectId, keyNo, accessTypeId}) == null) {
            return;
        }

        if ( ! isPermitted(objectId, adminAccessTypeId) || ! isPermitted(keyNo, adminAccessTypeId) ) {
            throwException("Not permitted to del role", new Object[]{objectId, keyNo});
        }

        if ( accessTypeId.equals(adminAccessTypeId) &&
             ( ! isPermitted(objectId, grantAdminAccessTypeId) || ! isPermitted(keyNo, grantAdminAccessTypeId))) {
            throwException("Not permitted to del role", new Object[]{objectId, keyNo});
        }

        try {

            DataSet parentsSet = executeSelect("select group_id from cw_core_group_objects where object_id = :o", new Object[]{objectId});
            while (! parentsSet.isEOF()) {
                if ( executeSelectBigDecimal("select object_id from cw_core_object_roles where object_id = :g and key_id = :k and access_type_id = :a", new Object[]{parentsSet.getBigDecimal(0), keyNo, accessTypeId}) != null ) {
                    parentsSet.close();
                    return;
                }
                parentsSet.next();
            }
            parentsSet.close();


            executeStatement("delete from cw_core_object_roles where object_id = :o and key_id = :k and access_type_id = :a", new Object[]{objectId, keyNo, accessTypeId});
            DataSet objectsSet = executeSelect("select object_id from cw_core_group_objects where group_id = :g", new Object[]{objectId});
            while ( ! objectsSet.isEOF()) {
                delRole( objectsSet.getBigDecimal(0), keyNo, accessTypeId);
                objectsSet.next();
            }
            objectsSet.close();
        }   catch( Exception e) {
            throwException("Exception on add role", e);
        }

*/
    }

    public BigDecimal getLocalAccessGroupId() {
        return localAccessGroupId;
    }

    public BigDecimal getLocalListsAccessGroupId() {
        return localListsAccessGroupId;
    }

    public BigDecimal getLocalAllListsAccessGroupId() {
        return localAllListsAccessGroupId;
    }

    public BigDecimal getReadAccessTypeId() {
        return readAccessTypeId;
    }

    public BigDecimal getWriteAccessTypeId() {
        return writeAccessTypeId;
    }

    public BigDecimal getDelAccessTypeId() {
        return delAccessTypeId;
    }

    public BigDecimal getAdminAccessTypeId() {
        return adminAccessTypeId;
    }

    public BigDecimal getAdminKeyId() {
        return adminKeyNo;
    }

    public BigDecimal getPublicKeyId() {
        return publicKeyNo;
    }

    public void handleNewObject(BigDecimal objectId, BigDecimal typeId) {


/*
        if ( ! objectService.isProtectedMode()) {
            throwException("handle new object protected");
        }
        protectedMode = true;
        try {
            makeAddShare(objectId, adminKeyNo, false);
            if ( objectId.compareTo(new java.math.BigDecimal(10000000)) < 0 ) {
                makeAddShare(objectId, publicKeyNo, false);
            }   else {
                makeAddShare(objectId, getSession().getSessionSiteId(), false);
            }
        }   catch ( CoreException e) {
            throwException("handleNewError", e);
        }   finally {
            protectedMode = false;
        }
*/


    }


}
