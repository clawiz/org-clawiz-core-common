package org.clawiz.core.common.metadata.generator.node;

import org.clawiz.core.common.metadata.data.language.node.AbstractLanguageNode;

public class AbstractMetadataNodeGeneratorContext {

    AbstractLanguageNode languageNode;

    public AbstractLanguageNode getLanguageNode() {
        return languageNode;
    }

    public void setLanguageNode(AbstractLanguageNode languageNode) {
        this.languageNode = languageNode;
    }
}
