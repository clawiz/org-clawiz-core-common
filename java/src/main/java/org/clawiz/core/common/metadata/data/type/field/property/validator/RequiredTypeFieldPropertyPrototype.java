package org.clawiz.core.common.metadata.data.type.field.property.validator;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class RequiredTypeFieldPropertyPrototype extends org.clawiz.core.common.metadata.data.type.field.property.AbstractTypeFieldProperty {
    
    public RequiredTypeFieldProperty withName(String value) {
        setName(value);
        return (RequiredTypeFieldProperty) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
