package org.clawiz.core.common.storage.typekeyfield;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class TypeKeyFieldObjectPrototype extends AbstractObject {
    
    /**
    * Key
    */
    private java.math.BigDecimal keyId;
    
    /**
    * OrderNo
    */
    private java.math.BigDecimal orderNo;
    
    /**
    * Field
    */
    private java.math.BigDecimal fieldId;
    
    public TypeKeyFieldService service;
    
    /**
    * 
    * @return     Key
    */
    public java.math.BigDecimal getKeyId() {
        return this.keyId;
    }
    
    /**
    * Set 'Key' value
    * 
    * @param      keyId Key
    */
    public void setKeyId(java.math.BigDecimal keyId) {
         this.keyId = keyId;
    }
    
    /**
    * Set 'Key' value and return this object
    * 
    * @param      keyId Key
    * @return     TypeKeyField object
    */
    public TypeKeyFieldObjectPrototype withKeyId(java.math.BigDecimal keyId) {
        setKeyId(keyId);
        return this;
    }
    
    /**
    * 
    * @return     OrderNo
    */
    public java.math.BigDecimal getOrderNo() {
        return this.orderNo;
    }
    
    /**
    * Set 'OrderNo' value
    * 
    * @param      orderNo OrderNo
    */
    public void setOrderNo(java.math.BigDecimal orderNo) {
         this.orderNo = orderNo;
    }
    
    /**
    * Set 'OrderNo' value and return this object
    * 
    * @param      orderNo OrderNo
    * @return     TypeKeyField object
    */
    public TypeKeyFieldObjectPrototype withOrderNo(java.math.BigDecimal orderNo) {
        setOrderNo(orderNo);
        return this;
    }
    
    /**
    * 
    * @return     Field
    */
    public java.math.BigDecimal getFieldId() {
        return this.fieldId;
    }
    
    /**
    * Set 'Field' value
    * 
    * @param      fieldId Field
    */
    public void setFieldId(java.math.BigDecimal fieldId) {
         this.fieldId = fieldId;
    }
    
    /**
    * Set 'Field' value and return this object
    * 
    * @param      fieldId Field
    * @return     TypeKeyField object
    */
    public TypeKeyFieldObjectPrototype withFieldId(java.math.BigDecimal fieldId) {
        setFieldId(fieldId);
        return this;
    }
    
    public TypeKeyFieldService getService() {
        return this.service;
    }
    
    public void setService(TypeKeyFieldService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(TypeKeyFieldObjectPrototype  target) {
        target.setKeyId(getKeyId());
        target.setOrderNo(getOrderNo());
        target.setFieldId(getFieldId());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((TypeKeyFieldObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        service.save((TypeKeyFieldObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(TypeKeyFieldObjectPrototype object) {
        return 
               isObjectsEquals(this.getKeyId(), object.getKeyId() ) 
            && isObjectsEquals(this.getOrderNo(), object.getOrderNo() ) 
            && isObjectsEquals(this.getFieldId(), object.getFieldId() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getKeyId() != null ? getKeyId().hashCode() : 0);
        result = result * 31 + (getOrderNo() != null ? getOrderNo().hashCode() : 0);
        result = result * 31 + (getFieldId() != null ? getFieldId().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'KeyField' fields
    * 
    * @return     Concatenated string values of key 'toKeyField' fields
    */
    public String toKeyField() {
        return service.getObjectService().idToString(getKeyId()) + "," + service.getObjectService().idToString(getFieldId());
    }
    
    /**
    * Prepare concatenated string values of key 'KeyOrderNo' fields
    * 
    * @return     Concatenated string values of key 'toKeyOrderNo' fields
    */
    public String toKeyOrderNo() {
        return service.getObjectService().idToString(getKeyId()) + "," + ( getOrderNo() != null ? getOrderNo().toString() : null );
    }
}
