/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.utils.date;

import org.clawiz.core.common.CoreException;

/**
 * (c) Clawiz
 */
public enum Weekday {

    MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY;

    public static Weekday toWeekday(String day) {

        if (day==null) {
            throw new CoreException("NULL weekday");
        }

        if ( day.equalsIgnoreCase("Понедельник"))   { return MONDAY; }
        if ( day.equalsIgnoreCase("Вторник"))       { return TUESDAY; }
        if ( day.equalsIgnoreCase("Среда"))         { return WEDNESDAY; }
        if ( day.equalsIgnoreCase("Четверг"))       { return THURSDAY; }
        if ( day.equalsIgnoreCase("Пятница"))       { return FRIDAY; }
        if ( day.equalsIgnoreCase("Суббота"))       { return SATURDAY; }
        if ( day.equalsIgnoreCase("Воскресенье"))   { return SUNDAY; }

        try {
            return Weekday.valueOf(day.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new CoreException("Wrong weekday '?'", new Object[]{day});
        }
    }
}
