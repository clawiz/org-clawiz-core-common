package org.clawiz.core.common.storage.objectrelation;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class ObjectRelationObjectPrototype extends AbstractObject {
    
    /**
    * SourceObject
    */
    private java.math.BigDecimal sourceObjectId;
    
    /**
    * DestinationObject
    */
    private java.math.BigDecimal destinationObjectId;
    
    /**
    * RelationType
    */
    private java.math.BigDecimal relationTypeId;
    
    /**
    * Probability
    */
    private java.math.BigDecimal probability;
    
    /**
    * distance
    */
    private java.math.BigDecimal distance;
    
    /**
    * weight
    */
    private java.math.BigDecimal weight;
    
    public ObjectRelationService service;
    
    /**
    * 
    * @return     SourceObject
    */
    public java.math.BigDecimal getSourceObjectId() {
        return this.sourceObjectId;
    }
    
    /**
    * Set 'SourceObject' value
    * 
    * @param      sourceObjectId SourceObject
    */
    public void setSourceObjectId(java.math.BigDecimal sourceObjectId) {
         this.sourceObjectId = sourceObjectId;
    }
    
    /**
    * Set 'SourceObject' value and return this object
    * 
    * @param      sourceObjectId SourceObject
    * @return     ObjectRelation object
    */
    public ObjectRelationObjectPrototype withSourceObjectId(java.math.BigDecimal sourceObjectId) {
        setSourceObjectId(sourceObjectId);
        return this;
    }
    
    /**
    * 
    * @return     DestinationObject
    */
    public java.math.BigDecimal getDestinationObjectId() {
        return this.destinationObjectId;
    }
    
    /**
    * Set 'DestinationObject' value
    * 
    * @param      destinationObjectId DestinationObject
    */
    public void setDestinationObjectId(java.math.BigDecimal destinationObjectId) {
         this.destinationObjectId = destinationObjectId;
    }
    
    /**
    * Set 'DestinationObject' value and return this object
    * 
    * @param      destinationObjectId DestinationObject
    * @return     ObjectRelation object
    */
    public ObjectRelationObjectPrototype withDestinationObjectId(java.math.BigDecimal destinationObjectId) {
        setDestinationObjectId(destinationObjectId);
        return this;
    }
    
    /**
    * 
    * @return     RelationType
    */
    public java.math.BigDecimal getRelationTypeId() {
        return this.relationTypeId;
    }
    
    /**
    * Set 'RelationType' value
    * 
    * @param      relationTypeId RelationType
    */
    public void setRelationTypeId(java.math.BigDecimal relationTypeId) {
         this.relationTypeId = relationTypeId;
    }
    
    /**
    * Set 'RelationType' value and return this object
    * 
    * @param      relationTypeId RelationType
    * @return     ObjectRelation object
    */
    public ObjectRelationObjectPrototype withRelationTypeId(java.math.BigDecimal relationTypeId) {
        setRelationTypeId(relationTypeId);
        return this;
    }
    
    /**
    * 
    * @return     Probability
    */
    public java.math.BigDecimal getProbability() {
        return this.probability;
    }
    
    /**
    * Set 'Probability' value
    * 
    * @param      probability Probability
    */
    public void setProbability(java.math.BigDecimal probability) {
         this.probability = probability;
    }
    
    /**
    * Set 'Probability' value and return this object
    * 
    * @param      probability Probability
    * @return     ObjectRelation object
    */
    public ObjectRelationObjectPrototype withProbability(java.math.BigDecimal probability) {
        setProbability(probability);
        return this;
    }
    
    /**
    * 
    * @return     distance
    */
    public java.math.BigDecimal getDistance() {
        return this.distance;
    }
    
    /**
    * Set 'distance' value
    * 
    * @param      distance distance
    */
    public void setDistance(java.math.BigDecimal distance) {
         this.distance = distance;
    }
    
    /**
    * Set 'distance' value and return this object
    * 
    * @param      distance distance
    * @return     ObjectRelation object
    */
    public ObjectRelationObjectPrototype withDistance(java.math.BigDecimal distance) {
        setDistance(distance);
        return this;
    }
    
    /**
    * 
    * @return     weight
    */
    public java.math.BigDecimal getWeight() {
        return this.weight;
    }
    
    /**
    * Set 'weight' value
    * 
    * @param      weight weight
    */
    public void setWeight(java.math.BigDecimal weight) {
         this.weight = weight;
    }
    
    /**
    * Set 'weight' value and return this object
    * 
    * @param      weight weight
    * @return     ObjectRelation object
    */
    public ObjectRelationObjectPrototype withWeight(java.math.BigDecimal weight) {
        setWeight(weight);
        return this;
    }
    
    public ObjectRelationService getService() {
        return this.service;
    }
    
    public void setService(ObjectRelationService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(ObjectRelationObjectPrototype  target) {
        target.setSourceObjectId(getSourceObjectId());
        target.setDestinationObjectId(getDestinationObjectId());
        target.setRelationTypeId(getRelationTypeId());
        target.setProbability(getProbability());
        target.setDistance(getDistance());
        target.setWeight(getWeight());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((ObjectRelationObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        service.save((ObjectRelationObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(ObjectRelationObjectPrototype object) {
        return 
               isObjectsEquals(this.getSourceObjectId(), object.getSourceObjectId() ) 
            && isObjectsEquals(this.getDestinationObjectId(), object.getDestinationObjectId() ) 
            && isObjectsEquals(this.getRelationTypeId(), object.getRelationTypeId() ) 
            && isObjectsEquals(this.getProbability(), object.getProbability() ) 
            && isObjectsEquals(this.getDistance(), object.getDistance() ) 
            && isObjectsEquals(this.getWeight(), object.getWeight() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getSourceObjectId() != null ? getSourceObjectId().hashCode() : 0);
        result = result * 31 + (getDestinationObjectId() != null ? getDestinationObjectId().hashCode() : 0);
        result = result * 31 + (getRelationTypeId() != null ? getRelationTypeId().hashCode() : 0);
        result = result * 31 + (getProbability() != null ? getProbability().hashCode() : 0);
        result = result * 31 + (getDistance() != null ? getDistance().hashCode() : 0);
        result = result * 31 + (getWeight() != null ? getWeight().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'SourceDestinationType' fields
    * 
    * @return     Concatenated string values of key 'toSourceDestinationType' fields
    */
    public String toSourceDestinationType() {
        return service.getObjectService().idToString(getSourceObjectId()) + "," + service.getObjectService().idToString(getDestinationObjectId()) + "," + service.getObjectService().idToString(getRelationTypeId());
    }
    
    /**
    * Prepare concatenated string values of key 'SourceRelation' fields
    * 
    * @return     Concatenated string values of key 'toSourceRelation' fields
    */
    public String toSourceRelation() {
        return service.getObjectService().idToString(getSourceObjectId()) + "," + service.getObjectService().idToString(getRelationTypeId());
    }
    
    /**
    * Prepare concatenated string values of key 'DestinationRelation' fields
    * 
    * @return     Concatenated string values of key 'toDestinationRelation' fields
    */
    public String toDestinationRelation() {
        return service.getObjectService().idToString(getDestinationObjectId()) + "," + service.getObjectService().idToString(getRelationTypeId());
    }
}
