/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.xml.component.element;

import org.clawiz.core.common.system.generator.abstractgenerator.component.element.AbstractElement;
import org.clawiz.core.common.system.generator.file.component.element.StringTextElement;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class AbstractXMLElement extends StringTextElement {

    public abstract String getTagName();

    private HashMap<String, XMLAttribute> attributesCache = new HashMap<>();
    private ArrayList<XMLAttribute> attributes = new ArrayList<>();

    public XMLAttribute addAttribute(String name, String value) {
        if ( name == null ) {
            throwException("XML attribute name cannot be null");
        }
        String key = name.toUpperCase();
        if ( attributesCache.containsKey(key)) {
            throwException("XML attribute '?' already defined", new Object[]{name});
        }
        XMLAttribute attribute = new XMLAttribute(name, value);
        attributesCache.put(key, attribute);
        attributes.add(attribute);
        return attribute;
    }

    public XMLAttribute addAttribute(String name, boolean value) {
        return addAttribute(name, value ? "true" : "false");
    }

    public void removeAttribute(String name) {
        if ( name == null ) {
            throwException("XML attribute name cannot be null");
        }
        String key = name.toUpperCase();
        if ( attributesCache.containsKey(key)) {
            attributesCache.remove(key);
            for (int i=0; i < attributes.size(); i++) {
                if ( attributes.get(i).getName().toUpperCase().equals(key)) {
                    attributes.remove(i);
                    break;
                }
            }
        }
    }

    public XMLAttribute getAttribute(String name) {
        return name != null ? attributesCache.get(name.toUpperCase()) : null;
    }

    public ArrayList<XMLAttribute> getAttributes() {
        return attributes;
    }


    public String getSomeCheckAttrubutes() {
        return null;
    }

    public boolean someAsNode(Node node) {
        if ( getTagName() != null && (! getTagName().equals(node.getNodeName()))) {
            return false;
        }
        return true;
    }

    public void mergeWithNode(Node node) {

    }

    @Override
    public <T extends AbstractElement> T addElement(Class<T> clazz) {
        AbstractElement element = super.addElement(clazz);
        ((StringTextElement) element).setLpad(getLpad() + "  ");
        return (T) element;
    }

    @Override
    public <T extends AbstractElement> T addElement(Class<T> clazz, String childName) {
        AbstractElement element = super.addElement(clazz, childName);
        ((StringTextElement) element).setLpad(getLpad() + "  ");
        return (T) element;
    }

    @Override
    public void write() {

        p("<" + getTagName());

        String backupLpad = getLpad();
        setLpad("");

        for (XMLAttribute attribute: attributes) {
            if ( attribute.getValue() != null ) {
                p(" " + attribute.getName() + "=\"" + attribute.getValue() + "\"");
            }
        }

        if ( getElements().size() > 0 ) {
            pln(">");
            setLpad(backupLpad);
            for(AbstractElement element : getElements() ) {
                element.write();
            }
            pln("</" + getTagName() + ">");
        } else {
            pln(" />");
            setLpad(backupLpad);
        }

    }

}
