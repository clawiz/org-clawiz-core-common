/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import static java.math.BigDecimal.ZERO;


/**
 * (c) Clawiz
 * Date: 24.03.11
 * Time: 16?1
 */
public class BigDecimalUtils {

    public static BigDecimal max(BigDecimal value1, BigDecimal value2) {
        if (value1 == null)
            return null;
        if (value2 == null)
            return null;

        return value1.compareTo(value2) > 0 ? value1 : value2;
    }

    public static BigDecimal round(BigDecimal source, int scale ) {
        if (source == null ) {
            return  null;
        }
        return source.setScale(scale, BigDecimal.ROUND_HALF_UP);
    }

    public static BigDecimal multiply(BigDecimal d1, BigDecimal d2, int scale ) {
        return round(multiply(d1,d2), scale);
    }

    public static BigDecimal multiply(BigDecimal d1, BigDecimal d2 ) {
        if (d1 == null || d2 == null ) {
            return  null;
        }
        return d1.multiply(d2);
    }

    public static BigDecimal divide(BigDecimal d1, BigDecimal d2, int scale ) {
        return round(divide(d1,d2), scale);
    }

    public static BigDecimal divide(BigDecimal d1, BigDecimal d2 ) {
        if (d1 == null || d2 == null || d2.compareTo(new BigDecimal(0))==0) {
            return  null;
        }
        return d1.divide(d2,15, BigDecimal.ROUND_HALF_UP);
    }

    public static BigDecimal subtract(BigDecimal d1, BigDecimal d2 ) {
        if (d1 == null || d2 == null ) {
            return  null;
        }
        return d1.subtract(d2);
    }

    public static BigDecimal add(BigDecimal d1, BigDecimal d2 ) {
        if (d1 == null || d2 == null ) {
            return  null;
        }
        return d1.add(d2);
    }

    public static BigDecimal ln(BigDecimal v) {
        if (v == null) {
            return null;
        }

        return new BigDecimal(Math.log(v.doubleValue()));
    }

    public static BigDecimal sqrt(BigDecimal v) {
        if(v == null)
            return null;
        
        return new BigDecimal(Math.sqrt(v.doubleValue()));
    }
    public static BigDecimal exp(BigDecimal v) {
        if(v == null)
            return null;

        return new BigDecimal(Math.exp(v.doubleValue()));
    }

    public static BigDecimal pow(BigDecimal value, BigDecimal pow) {
        if(value == null || pow == null)
            return null;

        return new BigDecimal(Math.pow(value.doubleValue(), pow.doubleValue()));
    }

    public static BigDecimal nonNegative(BigDecimal value) {
        if (value == null) {
            return null;
        }
        return (value.compareTo(ZERO) >= 0) ? value : ZERO;
    }

    public static boolean equals(BigDecimal lhs, BigDecimal rhs) {
        return lhs != null && rhs != null && lhs.compareTo(rhs) == 0;
    }

    public static String toString(BigDecimal value, DecimalFormat decimalFormat) {
        if ( value == null ) {
            return null;
        }
        return decimalFormat.format(value);
    }

    public static String toString(BigDecimal value) {
        if ( value == null ) {
            return null;
        }
        return value.toString();
    }

    public static Double toDouble(BigDecimal value) {
        if ( value == null ) {
            return null;
        }
        return value.doubleValue();
    }

    public static boolean toBoolean(BigDecimal value) {
        return (value != null) && !equals(value, ZERO);
    }

    public static final BigDecimal zero = new BigDecimal(0);
    public static BigDecimal nullToZero(BigDecimal value) {
        return value != null ? value : zero;
    }

}
