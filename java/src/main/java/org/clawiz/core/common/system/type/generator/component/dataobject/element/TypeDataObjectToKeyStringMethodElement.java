/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator.component.dataobject.element;

import org.clawiz.core.common.metadata.data.common.valuetype.*;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.type.key.TypeKey;
import org.clawiz.core.common.metadata.data.type.key.TypeKeyField;

public class TypeDataObjectToKeyStringMethodElement extends AbstractTypeDataObjectMethodElement {

    TypeKey key;

    public TypeKey getKey() {
        return key;
    }

    public void setKey(TypeKey key) {
        this.key = key;
    }

    @Override
    public void process() {
        super.process();

        setName("to" + key.getJavaClassName());
        setType("String", "Concatenated string values of key '" + getName() + "' fields");

        addJavaDoc("Prepare concatenated string values of key '" + key.getName() + "' fields");

        String expression = "";
        String prefix     = "";

        for (TypeKeyField keyField : key.getFields() ) {
            TypeField field = keyField.getField();

            String getter = field.getGetMethodName() + "()";
            if ( field.getValueType() instanceof ValueTypeString) {
                expression += prefix + getter;
            } else if ( field.getValueType() instanceof ValueTypeObject) {
                expression += prefix + "service.getObjectService().idToString(" + getter + ")";
            } else if ( field.getValueType() instanceof ValueTypeEnumeration
                     || field.getValueType() instanceof ValueTypeNumber
                     || field.getValueType() instanceof ValueTypeBoolean) {
                expression += prefix + "( " + getter + " != null ? " + getter + ".toString() : null )";
            } else if ( field.getValueType() instanceof ValueTypeDate) {
                expression += prefix + "DateUtils.toString(" + getter + ", DateUtils.DEFAULT_DATE_FORMAT)";
            } else if ( field.getValueType() instanceof ValueTypeDateTime) {
                expression += prefix + "DateUtils.toString(" + getter + ", DateUtils.DEFAULT_DATE_TIME_FORMAT)";
            } else {
                throwException("Wrong key ? toString field ? value type ?", key.getFullName(), field.getName(), field.getValueType().toString());
            }

            prefix = " + \",\" + ";
        }

        addText("return " + expression + ";");

    }
}
