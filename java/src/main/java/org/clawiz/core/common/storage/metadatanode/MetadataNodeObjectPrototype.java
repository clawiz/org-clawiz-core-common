package org.clawiz.core.common.storage.metadatanode;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class MetadataNodeObjectPrototype extends AbstractObject {
    
    /**
    * PackageName
    */
    private java.lang.String packageName;
    
    /**
    * Name
    */
    private java.lang.String name;
    
    /**
    * NodeClass
    */
    private java.math.BigDecimal nodeClassId;
    
    private org.clawiz.core.common.storage.metadatanodeclass.MetadataNodeClassObject nodeClass;
    
    /**
    * Storage
    */
    private java.math.BigDecimal storageId;
    
    private org.clawiz.core.common.storage.metadatastorage.MetadataStorageObject storage;
    
    /**
    * NodeObject
    */
    private java.math.BigDecimal nodeObjectId;
    
    public MetadataNodeService service;
    
    /**
    * 
    * @return     PackageName
    */
    public java.lang.String getPackageName() {
        return this.packageName;
    }
    
    /**
    * Set 'PackageName' value
    * 
    * @param      packageName PackageName
    */
    public void setPackageName(java.lang.String packageName) {
        
        if ( packageName != null && packageName.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "MetadataNode.PackageName", "250");
        }
        
         this.packageName = packageName;
    }
    
    /**
    * Set 'PackageName' value and return this object
    * 
    * @param      packageName PackageName
    * @return     MetadataNode object
    */
    public MetadataNodeObjectPrototype withPackageName(java.lang.String packageName) {
        setPackageName(packageName);
        return this;
    }
    
    /**
    * 
    * @return     Name
    */
    public java.lang.String getName() {
        return this.name;
    }
    
    /**
    * Set 'Name' value
    * 
    * @param      name Name
    */
    public void setName(java.lang.String name) {
        
        if ( name != null && name.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "MetadataNode.Name", "250");
        }
        
         this.name = name;
    }
    
    /**
    * Set 'Name' value and return this object
    * 
    * @param      name Name
    * @return     MetadataNode object
    */
    public MetadataNodeObjectPrototype withName(java.lang.String name) {
        setName(name);
        return this;
    }
    
    /**
    * 
    * @return     NodeClass
    */
    public java.math.BigDecimal getNodeClassId() {
        return this.nodeClassId;
    }
    
    public org.clawiz.core.common.storage.metadatanodeclass.MetadataNodeClassObject getNodeClass() {
        return getNodeClass(false);
    }
    
    @SuppressWarnings("Duplicates")
    public org.clawiz.core.common.storage.metadatanodeclass.MetadataNodeClassObject getNodeClass(boolean autoCreate) {
        
        if ( nodeClass != null ) { return nodeClass; }
        
        if ( nodeClassId != null ) {
            nodeClass = service.loadMetadataNodeClass(nodeClassId);
        } else if ( autoCreate ) {
            nodeClass = service.createMetadataNodeClass();
        }
        
        return nodeClass;
    }
    
    /**
    * Set 'NodeClass' value
    * 
    * @param      nodeClassId NodeClass
    */
    public void setNodeClassId(java.math.BigDecimal nodeClassId) {
         this.nodeClassId = nodeClassId;
    }
    
    /**
    * Set 'NodeClass' value and return this object
    * 
    * @param      nodeClassId NodeClass
    * @return     MetadataNode object
    */
    public MetadataNodeObjectPrototype withNodeClassId(java.math.BigDecimal nodeClassId) {
        setNodeClassId(nodeClassId);
        return this;
    }
    
    /**
    * 
    * @return     Storage
    */
    public java.math.BigDecimal getStorageId() {
        return this.storageId;
    }
    
    public org.clawiz.core.common.storage.metadatastorage.MetadataStorageObject getStorage() {
        return getStorage(false);
    }
    
    @SuppressWarnings("Duplicates")
    public org.clawiz.core.common.storage.metadatastorage.MetadataStorageObject getStorage(boolean autoCreate) {
        
        if ( storage != null ) { return storage; }
        
        if ( storageId != null ) {
            storage = service.loadMetadataStorage(storageId);
        } else if ( autoCreate ) {
            storage = service.createMetadataStorage();
        }
        
        return storage;
    }
    
    /**
    * Set 'Storage' value
    * 
    * @param      storageId Storage
    */
    public void setStorageId(java.math.BigDecimal storageId) {
         this.storageId = storageId;
    }
    
    /**
    * Set 'Storage' value and return this object
    * 
    * @param      storageId Storage
    * @return     MetadataNode object
    */
    public MetadataNodeObjectPrototype withStorageId(java.math.BigDecimal storageId) {
        setStorageId(storageId);
        return this;
    }
    
    /**
    * 
    * @return     NodeObject
    */
    public java.math.BigDecimal getNodeObjectId() {
        return this.nodeObjectId;
    }
    
    /**
    * Set 'NodeObject' value
    * 
    * @param      nodeObjectId NodeObject
    */
    public void setNodeObjectId(java.math.BigDecimal nodeObjectId) {
         this.nodeObjectId = nodeObjectId;
    }
    
    /**
    * Set 'NodeObject' value and return this object
    * 
    * @param      nodeObjectId NodeObject
    * @return     MetadataNode object
    */
    public MetadataNodeObjectPrototype withNodeObjectId(java.math.BigDecimal nodeObjectId) {
        setNodeObjectId(nodeObjectId);
        return this;
    }
    
    public MetadataNodeService getService() {
        return this.service;
    }
    
    public void setService(MetadataNodeService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(MetadataNodeObjectPrototype  target) {
        target.setPackageName(getPackageName());
        target.setName(getName());
        target.setNodeClassId(getNodeClassId());
        target.setStorageId(getStorageId());
        target.setNodeObjectId(getNodeObjectId());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((MetadataNodeObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        if ( nodeClass != null ) {
            nodeClass.save();
            nodeClassId = nodeClass.getId();
        }
        if ( storage != null ) {
            storage.save();
            storageId = storage.getId();
        }
        service.save((MetadataNodeObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(MetadataNodeObjectPrototype object) {
        return 
               isObjectsEquals(this.getPackageName(), object.getPackageName() ) 
            && isObjectsEquals(this.getName(), object.getName() ) 
            && isObjectsEquals(this.getNodeClassId(), object.getNodeClassId() ) 
            && isObjectsEquals(this.getStorageId(), object.getStorageId() ) 
            && isObjectsEquals(this.getNodeObjectId(), object.getNodeObjectId() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getPackageName() != null ? getPackageName().hashCode() : 0);
        result = result * 31 + (getName() != null ? getName().hashCode() : 0);
        result = result * 31 + (getNodeClassId() != null ? getNodeClassId().hashCode() : 0);
        result = result * 31 + (getStorageId() != null ? getStorageId().hashCode() : 0);
        result = result * 31 + (getNodeObjectId() != null ? getNodeObjectId().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'Name' fields
    * 
    * @return     Concatenated string values of key 'toName' fields
    */
    public String toName() {
        return getName();
    }
    
    /**
    * Prepare concatenated string values of key 'NodeClassPackageName' fields
    * 
    * @return     Concatenated string values of key 'toNodeClassPackageName' fields
    */
    public String toNodeClassPackageName() {
        return service.getObjectService().idToString(getNodeClassId()) + "," + getPackageName() + "," + getName();
    }
    
    /**
    * Prepare concatenated string values of key 'Storage' fields
    * 
    * @return     Concatenated string values of key 'toStorage' fields
    */
    public String toStorage() {
        return service.getObjectService().idToString(getStorageId());
    }
    
    /**
    * Prepare concatenated string values of key 'Object' fields
    * 
    * @return     Concatenated string values of key 'toObject' fields
    */
    public String toObject() {
        return service.getObjectService().idToString(getNodeObjectId());
    }
}
