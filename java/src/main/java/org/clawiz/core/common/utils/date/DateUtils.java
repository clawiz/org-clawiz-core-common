/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.utils.date;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.utils.StringUtils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * (C) Clawiz
 * User: abdrashitovta
 * Date: 22.02.2010
 * Time: 3?9?3
 */
public class DateUtils {

    public static Date truncToDay( Date date ) {
        if ( date == null ) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();

        calendar.setTime( date );
        calendar.set( Calendar.MILLISECOND, 0 );
        calendar.set( Calendar.SECOND, 0 );
        calendar.set( Calendar.MINUTE, 0 );
        calendar.set( Calendar.HOUR_OF_DAY, 0 );

        Date x= calendar.getTime();
        return x;
    }

    public static Date truncToMonth( Date date ) {
        if ( date == null ) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();

        calendar.setTime( date );
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set( Calendar.SECOND, 0 );
        calendar.set( Calendar.MINUTE, 0 );
        calendar.set( Calendar.HOUR_OF_DAY, 0 );
        calendar.set( Calendar.DAY_OF_MONTH, 1 );

        Date x= calendar.getTime();
        return x;
    }

    public static Date truncToYear( Date date ) {
        if ( date == null ) {
            return null;
        }

        String s = toString(date, "yyyy");

        return toDate("01.01."+s, "dd.MM.yyyy");

    }


    private static int[] _monthToQuarter = new int[]{0,1,1,1,4,4,4,7,7,7,10,10,10};

    public static Date truncToQuarter( Date date ) {
        if ( date == null ) {
            return null;
        }



        String s = toString(date, "yyyy");
        // Кому не лень, напишите правильную формулу с делением
        int month = _monthToQuarter[StringUtils.toBigDecimal(toString(date, "MM")).intValue()];

        return toDate("01."+month+"."+s, "dd.MM.yyyy");

    }

    public static Date truncToHour( Date date ) {
        if ( date == null ) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();

        calendar.setTime( date );
        calendar.set( Calendar.MILLISECOND, 0 );
        calendar.set( Calendar.SECOND, 0 );
        calendar.set( Calendar.MINUTE, 0 );

        Date x= calendar.getTime();
        return x;
    }



    public static Date getLastMonthDay(Date date) {
        if ( date == null) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.DAY_OF_MONTH, maxDay);
        return calendar.getTime();
    }

    public static Date addDays(Date date, BigDecimal daysCount) {
        if ( daysCount == null ) {
            return date;
        }
        return addDays(date, daysCount.intValue());
    }

    public static Date addDays(Date date, int daysCount) {
        if ( date == null) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, daysCount);
        return calendar.getTime();
    }

    public static int getDayOfMonth(Date date)  {
        if ( date == null ) {
            return 0;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    public static Date addMonths(Date date, int monthsCount) {
        if ( date == null) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, monthsCount);
        return calendar.getTime();
    }

    public static Date addDaysDouble(Date date, double daysCount) {
        if ( date == null ) {
            return null;
        }
        if ( daysCount == 0 ) {
            return date;
        }

        double x = daysCount;
        // подправляем возможную ошибку округления
        x += (double) 1/24/60/60/10;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        calendar.add(Calendar.DAY_OF_MONTH, (int)x);
        x = ( x - (int) x ) * 24;

        calendar.add(Calendar.HOUR, (int)x);
        x = ( x - (int) x ) * 60;

        calendar.add(Calendar.MINUTE, (int)x);
        x = ( x - (int) x ) * 60;

        calendar.add(Calendar.SECOND, (int)x);



        return calendar.getTime();
    }


    public static Date addYears( Date date, int yearsCount ) {
        if ( date == null ) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();

        calendar.setTime( date );
        calendar.add( Calendar.YEAR, yearsCount );

        return calendar.getTime();
    }

    public static Date addHours( Date date, BigDecimal hoursCount ) {
        if ( hoursCount == null ) {
            return date;
        }
        return addHours(date, hoursCount.intValue());
    }


    public static Date addHours( Date date, int hoursCount ) {
        if ( date == null ) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();

        calendar.setTime( date );
        int sourceHour = calendar.get(Calendar.HOUR_OF_DAY);
        calendar.add( Calendar.HOUR_OF_DAY, hoursCount );
        int targetHour = calendar.get(Calendar.HOUR_OF_DAY);
        int sourceDayDelta = hoursCount % 24;
        int resultDayDelta = (targetHour - sourceHour);
        while (resultDayDelta < 0) {
            resultDayDelta += 24;
        }
        /*
        Обходим переход летнего/зимнего времени в java-машине. Работает только для положительно прибавляемых часов !
         */
        if ( sourceDayDelta != resultDayDelta && hoursCount > 0 ) {
            calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY)+ sourceDayDelta - resultDayDelta );
        }

        return calendar.getTime();
    }


    public static Date addMinutes(Date date, BigDecimal minutesCount) {
        if ( minutesCount == null ) {
            return date;
        }
        return addMinutes(date, minutesCount.intValue());
    }

    public static Date addMinutes(Date date, int minutesCount) {
        if ( date == null) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, minutesCount);
        return calendar.getTime();
    }

    public static Date addSeconds(Date date, int secondsCount) {
        if ( date == null) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.SECOND, secondsCount);
        return calendar.getTime();
    }

    public static String DEFAULT_DATE_FORMAT = "dd.MM.yyyy";
    public static String ISO8601_DATE_FORMAT = "yyyy-MM-dd";
    public static String DEFAULT_DATE_TIME_FORMAT = "dd.MM.yyyy HH:mm:ss";
    public static String ISO8601_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
//    public static String ISO8601_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static String toString(Date date) {
        if ( date == null ) {
            return null;
        }

        if ( truncToDay(date).equals(date)) {
            return toString(date, DEFAULT_DATE_FORMAT);
        } else {
            return toString(date, DEFAULT_DATE_TIME_FORMAT);
        }

    }

    public static String toString(Date date, String dateFormat) {
        if ( date == null ) {
            return null;
        }
        SimpleDateFormat df = new SimpleDateFormat(dateFormat);
        return df.format(date);
    }

    public static Date toDate(String str) {
        return toDate(str, DEFAULT_DATE_FORMAT);
    }

    public static Date toDateTime(String str) {
        return toDate(str, DateUtils.DEFAULT_DATE_TIME_FORMAT);
    }

    public static Date toDate(String str, String dateFormat) {
        if ( str == null || dateFormat == null || str.length() == 0) {
            return null;
        }

        SimpleDateFormat df = new SimpleDateFormat(dateFormat);
        try {
            Date result = df.parse(str);
            return result;
        } catch (ParseException e) {
            throw new CoreException("String to date parse exception ? (?) ?", new Object[]{str, dateFormat, e.getMessage()}, e);
        }
    }


    public static Date truncToInterval(Date date, IntervalType interval) {

        if ( interval == IntervalType.HOUR ) {
            return DateUtils.truncToHour(date);
        }
        if ( interval == IntervalType.DAY ) {
            return DateUtils.truncToDay(date);
        }
        if ( interval == IntervalType.MONTH ) {
            return DateUtils.truncToMonth(date);
        }
        if ( interval == IntervalType.QUARTER ) {
            return DateUtils.truncToQuarter(date);
        }
        if ( interval == IntervalType.YEAR ) {
            return DateUtils.truncToYear(date);
        }
        throw new CoreException("Unknown interval '?' for date truncate ", new Object[]{interval});
    }

    public static Date addIntervals(Date date, IntervalType interval, int count ) {
        if ( interval == IntervalType.MINUTE ) {
            return addMinutes(date, count);
        }
        if ( interval == IntervalType.HOUR ) {
            return addHours(date, count);
        }
        if ( interval == IntervalType.DAY ) {
            return addDays(date, count);
        }
        if ( interval == IntervalType.MONTH ) {
            return addMonths(date, count);
        }
        if ( interval == IntervalType.QUARTER ) {
            return addMonths(date, count*3);
        }
        if ( interval == IntervalType.YEAR ) {
            return addYears(date, count);
        }
        throw new CoreException("Unknown interval '?' for add to date ", new Object[]{interval});
    }

    public static Date addInterval(Date date, IntervalType interval) {
        return addIntervals(date, interval, 1);
    }

    public static Date[] splitByIntervalType(Date date, IntervalType intervalType, IntervalType step) {
        ArrayList<Date> dates = new ArrayList<Date>();
        Date currentDate = truncToInterval(date, intervalType);
        Date endDate = addInterval(currentDate, intervalType);

        dates.add(currentDate);
        currentDate = addInterval(date, step);

        while ( currentDate.compareTo(endDate) < 0 ) {
            dates.add(currentDate);
            currentDate = addInterval(currentDate, step);
        }

        Date[] result = new Date[1];
        result = dates.toArray(result);
/*
        for ( int i=0; i < dates.size(); i++ ) {
            result[i] = dates.get(i);
        }
*/

        return result;
    }


    public static long toUnixTime(Date date) {
        if ( date == null ) {
            return 0;
        }
        return date.getTime()/1000L;
    }


    /**
     * <p>Checks if two dates are on the same day ignoring time.</p>
     * @param date1  the first date, not altered, not null
     * @param date2  the second date, not altered, not null
     * @return true if they represent the same day
     * @throws IllegalArgumentException if either date is <code>null</code>
     */
    public static boolean isSameDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isSameDay(cal1, cal2);
    }

    /**
     * <p>Checks if two calendars represent the same day ignoring time.</p>
     * @param cal1  the first calendar, not altered, not null
     * @param cal2  the second calendar, not altered, not null
     * @return true if they represent the same day
     * @throws IllegalArgumentException if either calendar is <code>null</code>
     */
    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }

    /**
     * <p>Checks if a date is today.</p>
     * @param date the date, not altered, not null.
     * @return true if the date is today.
     * @throws IllegalArgumentException if the date is <code>null</code>
     */
    public static boolean isToday(Date date) {
        return isSameDay(date, Calendar.getInstance().getTime());
    }

    /**
     * <p>Checks if a calendar date is today.</p>
     * @param cal  the calendar, not altered, not null
     * @return true if cal date is today
     * @throws IllegalArgumentException if the calendar is <code>null</code>
     */
    public static boolean isToday(Calendar cal) {
        return isSameDay(cal, Calendar.getInstance());
    }

    /**
     * <p>Checks if the first date is before the second date ignoring time.</p>
     * @param date1 the first date, not altered, not null
     * @param date2 the second date, not altered, not null
     * @return true if the first date day is before the second date day.
     * @throws IllegalArgumentException if the date is <code>null</code>
     */
    public static boolean isBeforeDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isBeforeDay(cal1, cal2);
    }

    /**
     * <p>Checks if the first calendar date is before the second calendar date ignoring time.</p>
     * @param cal1 the first calendar, not altered, not null.
     * @param cal2 the second calendar, not altered, not null.
     * @return true if cal1 date is before cal2 date ignoring time.
     * @throws IllegalArgumentException if either of the calendars are <code>null</code>
     */
    public static boolean isBeforeDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        if (cal1.get(Calendar.ERA) < cal2.get(Calendar.ERA)) return true;
        if (cal1.get(Calendar.ERA) > cal2.get(Calendar.ERA)) return false;
        if (cal1.get(Calendar.YEAR) < cal2.get(Calendar.YEAR)) return true;
        if (cal1.get(Calendar.YEAR) > cal2.get(Calendar.YEAR)) return false;
        return cal1.get(Calendar.DAY_OF_YEAR) < cal2.get(Calendar.DAY_OF_YEAR);
    }

    /**
     * <p>Checks if the first date is after the second date ignoring time.</p>
     * @param date1 the first date, not altered, not null
     * @param date2 the second date, not altered, not null
     * @return true if the first date day is after the second date day.
     * @throws IllegalArgumentException if the date is <code>null</code>
     */
    public static boolean isAfterDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isAfterDay(cal1, cal2);
    }

    /**
     * <p>Checks if the first calendar date is after the second calendar date ignoring time.</p>
     * @param cal1 the first calendar, not altered, not null.
     * @param cal2 the second calendar, not altered, not null.
     * @return true if cal1 date is after cal2 date ignoring time.
     * @throws IllegalArgumentException if either of the calendars are <code>null</code>
     */
    public static boolean isAfterDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        if (cal1.get(Calendar.ERA) < cal2.get(Calendar.ERA)) return false;
        if (cal1.get(Calendar.ERA) > cal2.get(Calendar.ERA)) return true;
        if (cal1.get(Calendar.YEAR) < cal2.get(Calendar.YEAR)) return false;
        if (cal1.get(Calendar.YEAR) > cal2.get(Calendar.YEAR)) return true;
        return cal1.get(Calendar.DAY_OF_YEAR) > cal2.get(Calendar.DAY_OF_YEAR);
    }

    /**
     * <p>Checks if a date is after today and within a number of days in the future.</p>
     * @param date the date to check, not altered, not null.
     * @param days the number of days.
     * @return true if the date day is after today and within days in the future .
     * @throws IllegalArgumentException if the date is <code>null</code>
     */
    public static boolean isWithinDaysFuture(Date date, int days) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return isWithinDaysFuture(cal, days);
    }

    /**
     * <p>Checks if a calendar date is after today and within a number of days in the future.</p>
     * @param cal the calendar, not altered, not null
     * @param days the number of days.
     * @return true if the calendar date day is after today and within days in the future .
     * @throws IllegalArgumentException if the calendar is <code>null</code>
     */
    public static boolean isWithinDaysFuture(Calendar cal, int days) {
        if (cal == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar today = Calendar.getInstance();
        Calendar future = Calendar.getInstance();
        future.add(Calendar.DAY_OF_YEAR, days);
        return (isAfterDay(cal, today) && ! isAfterDay(cal, future));
    }

    public static Date getStart(Date date) {
        return clearTime(date);
    }

    public static Date clearTime(Date date) {
        if (date == null) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    /**
     * Determines whether or not a date has any time values.
     * @param date The date.
     * @return true iff the date is not null and any of the date's hour, minute,
     * seconds or millisecond values are greater than zero.
     */
    public static boolean hasTime(Date date) {
        if (date == null) {
            return false;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        if (c.get(Calendar.HOUR_OF_DAY) > 0) {
            return true;
        }
        if (c.get(Calendar.MINUTE) > 0) {
            return true;
        }
        if (c.get(Calendar.SECOND) > 0) {
            return true;
        }
        if (c.get(Calendar.MILLISECOND) > 0) {
            return true;
        }
        return false;
    }

    public static Date getEnd(Date date) {
        if (date == null) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 999);
        return c.getTime();
    }

    public static Date max(Date d1, Date d2) {
        if (d1 == null && d2 == null) return null;
        if (d1 == null) return d2;
        if (d2 == null) return d1;
        return (d1.after(d2)) ? d1 : d2;
    }

    public static Date min(Date d1, Date d2) {
        if (d1 == null && d2 == null) return null;
        if (d1 == null) return d2;
        if (d2 == null) return d1;
        return (d1.before(d2)) ? d1 : d2;
    }


    private static Date minDate;
    private static Date maxDate;

    public static Date getMinDate() {
        if ( minDate == null ) {
            try {
                minDate = toDate("01.01.1800", "dd.MM.yyyy");
            } catch (CoreException e) {
                e.printStackTrace();
            }
        }
        return minDate;
    }

    public static Date getMaxDate() {
        if ( maxDate == null ) {
            try {
                maxDate = toDate("01.01.2100", "dd.MM.yyyy");
            } catch (CoreException e) {
                e.printStackTrace();
            }
        }
        return maxDate;
    }
}
