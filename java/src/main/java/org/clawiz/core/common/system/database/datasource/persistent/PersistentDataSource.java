package org.clawiz.core.common.system.database.datasource.persistent;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.database.datasource.DataSource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class PersistentDataSource extends DataSource {

    @Override
    public Connection getJdbcConnection() {
        Properties connectionProperties = new Properties();
        connectionProperties.put("user", getConfig().getUser());
        connectionProperties.put("password", getConfig().getPassword());


        try {
            Connection connection = DriverManager.getConnection(getConfig().getUrl(), connectionProperties);
            connection.setAutoCommit(getConfig().isAutoCommit());
            return connection;
        } catch (SQLException e) {
            throw new CoreException("Exception on create new connection for ? : ?", getConfig().getUrl(), e.getMessage(), e);
        }
    }
}
