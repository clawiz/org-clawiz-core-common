package org.clawiz.core.common.metadata.data.language.node.element.field;

import org.clawiz.core.common.metadata.data.language.node.element.Structure;
import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;

public class ElementStructureField extends AbstractStructureField {

    @ExchangeReference
    Structure                 elementStructure;

    @ExchangeAttribute
    boolean                   reference = false;

    public Structure getElementStructure() {
        return elementStructure;
    }

    public void setElementStructure(Structure elementStructure) {
        this.elementStructure = elementStructure;
    }

    public boolean isReference() {
        return reference;
    }

    public void setReference(boolean reference) {
        this.reference = reference;
    }

}
