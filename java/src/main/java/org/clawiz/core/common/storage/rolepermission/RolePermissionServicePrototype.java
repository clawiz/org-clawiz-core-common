package org.clawiz.core.common.storage.rolepermission;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;
import org.clawiz.core.common.system.service.NotInitializeService;

public class RolePermissionServicePrototype extends AbstractTypeService<RolePermissionObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.core.common.storage", "RolePermission");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.RolePermission"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      rolePermission Checked object
    */
    public void check(RolePermissionObject rolePermission) {
        
        if ( rolePermission == null ) {
            throwException("Cannot check null ?", new Object[]{"RolePermissionObject"});
        }
        
        rolePermission.fillDefaults();
        
        
        if ( roleObjectPermissionToId(rolePermission.getRoleId(), rolePermission.getObjectId(), rolePermission.getPermissionTypeId(), rolePermission.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "RolePermission", rolePermission.toRoleObjectPermission() });
        }
        
    }
    
    /**
    * Create new RolePermissionObject instance and fill default values
    * 
    * @return     Created object
    */
    public RolePermissionObject create() {
        
        RolePermissionObject rolePermission = new RolePermissionObject();
        rolePermission.setService((RolePermissionService) this);
        
        rolePermission.fillDefaults();
        
        return rolePermission;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public RolePermissionObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select role_id, object_id, permission_type_id from cw_core_role_permissions where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"RolePermission", id});
        }
        
        RolePermissionObject result = new RolePermissionObject();
        
        result.setService((RolePermissionService) this);
        result.setId(id);
        result.setRoleId(statement.getBigDecimal(1));
        result.setObjectId(statement.getBigDecimal(2));
        result.setPermissionTypeId(statement.getBigDecimal(3));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of RolePermissionObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public RolePermissionList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of RolePermissionObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public RolePermissionList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        RolePermissionList result = new RolePermissionList();
        
        
        Statement statement = executeQuery("select id, role_id, object_id, permission_type_id from cw_core_role_permissions"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            RolePermissionObject object = new RolePermissionObject();
        
            object.setService((RolePermissionService) this);
            object.setId(statement.getBigDecimal(1));
            object.setRoleId(statement.getBigDecimal(2));
            object.setObjectId(statement.getBigDecimal(3));
            object.setPermissionTypeId(statement.getBigDecimal(4));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'RoleObjectPermission' fields
    * 
    * @param      roleId           Role
    * @param      objectId         Object
    * @param      permissionTypeId PermissionType
    * @return     Id of found record or null
    */
    public BigDecimal roleObjectPermissionToId(java.math.BigDecimal roleId, java.math.BigDecimal objectId, java.math.BigDecimal permissionTypeId) {
        return roleObjectPermissionToId(roleId, objectId, permissionTypeId, null);
    }
    
    /**
    * Find id of record by key 'RoleObjectPermission' fields with id not equal skipId
    * 
    * @param      roleId           Role
    * @param      objectId         Object
    * @param      permissionTypeId PermissionType
    * @param      skipId           Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal roleObjectPermissionToId(java.math.BigDecimal roleId, java.math.BigDecimal objectId, java.math.BigDecimal permissionTypeId, BigDecimal skipId) {
        
        if ( roleId == null || objectId == null || permissionTypeId == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_role_permissions where role_id = ? and object_id = ? and permission_type_id = ?", roleId, objectId, permissionTypeId);
        } else {
            return executeQueryBigDecimal("select id from cw_core_role_permissions where role_id = ? and object_id = ? and permission_type_id = ? and id != ?", roleId, objectId, permissionTypeId, skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'RoleObjectPermission' fields or create new record with values set to given parameters
    * 
    * @param      roleId              Role
    * @param      objectId            Object
    * @param      permissionTypeId    PermissionType
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal roleObjectPermissionToId(java.math.BigDecimal roleId, java.math.BigDecimal objectId, java.math.BigDecimal permissionTypeId, boolean createNewIfNotFound) {
        
        BigDecimal id = roleObjectPermissionToId(roleId, objectId, permissionTypeId, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        RolePermissionObject object = create();
        object.setRoleId(roleId);
        object.setObjectId(objectId);
        object.setPermissionTypeId(permissionTypeId);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'RoleObjectPermission' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToRoleObjectPermission(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select role_id, object_id, permission_type_id from cw_core_role_permissions where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        RolePermissionObject object = new RolePermissionObject();
        object.setService((RolePermissionService)this);
        object.setId(id);
        object.setRoleId(statement.getBigDecimal(1));
        object.setObjectId(statement.getBigDecimal(2));
        object.setPermissionTypeId(statement.getBigDecimal(3));
        
        statement.close();
        
        return object.toRoleObjectPermission();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToRoleObjectPermission(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      rolePermissionObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(RolePermissionObject rolePermissionObject) {
        return rolePermissionObject.toRoleObjectPermission();
    }
    
    protected void saveAudit(TransactionAction transactionAction, RolePermissionObject oldRolePermissionObject, RolePermissionObject newRolePermissionObject) {
        
        RolePermissionObject o = oldRolePermissionObject != null ? oldRolePermissionObject : new RolePermissionObject();
        RolePermissionObject n = newRolePermissionObject != null ? newRolePermissionObject : new RolePermissionObject();
        
        
        executeUpdate("insert into a_cw_core_role_permissions (scn, action_type, id , o_role_id, o_object_id, o_permission_type_id, n_role_id, n_object_id, n_permission_type_id) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getRoleId(), o.getObjectId(), o.getPermissionTypeId(), n.getRoleId(), n.getObjectId(), n.getPermissionTypeId());
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      rolePermission Saved object
    */
    public void save(RolePermissionObject rolePermission) {
        
        if ( rolePermission == null ) {
            throwException("Cannot save NULL ?", new Object[]{"RolePermissionObject"});
        }
        
        TransactionAction transactionAction;
        RolePermissionObject oldRolePermission;
        if ( rolePermission.getService() == null ) {
            rolePermission.setService((RolePermissionService)this);
        }
        
        check(rolePermission);
        
        if ( rolePermission.getId() == null ) {
        
            rolePermission.setId(getObjectService().createObject(getTypeId()));
        
            oldRolePermission = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, rolePermission.getId());
        
            executeUpdate("insert into cw_core_role_permissions" 
                          + "( id, role_id, object_id, permission_type_id ) "
                          + "values"
                          + "(?, ?, ?, ?)",
                          rolePermission.getId(), rolePermission.getRoleId(), rolePermission.getObjectId(), rolePermission.getPermissionTypeId() );
        
            if ( rolePermission.getRoleId() != null ) { getObjectService().setLink(rolePermission.getRoleId(), rolePermission.getId()); }
            if ( rolePermission.getObjectId() != null ) { getObjectService().setLink(rolePermission.getObjectId(), rolePermission.getId()); }
            if ( rolePermission.getPermissionTypeId() != null ) { getObjectService().setLink(rolePermission.getPermissionTypeId(), rolePermission.getId()); }
        
        } else {
        
            oldRolePermission = load(rolePermission.getId());
            if ( oldRolePermission.equals(rolePermission) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, rolePermission.getId());
        
            executeUpdate("update cw_core_role_permissions set "
                          + "role_id = ?, object_id = ?, permission_type_id = ? "
                          + "where id = ?",
                          rolePermission.getRoleId(), rolePermission.getObjectId(), rolePermission.getPermissionTypeId(), rolePermission.getId() );
        
            getObjectService().changeLinkParent(oldRolePermission.getRoleId(), rolePermission.getRoleId(), rolePermission.getId());
            getObjectService().changeLinkParent(oldRolePermission.getObjectId(), rolePermission.getObjectId(), rolePermission.getId());
            getObjectService().changeLinkParent(oldRolePermission.getPermissionTypeId(), rolePermission.getPermissionTypeId(), rolePermission.getId());
        
        }
        
        saveAudit(transactionAction, oldRolePermission, rolePermission);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        RolePermissionObject oldRolePermissionObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldRolePermissionObject, null);
        
        getObjectService().deleteObject(id);
        
        if (oldRolePermissionObject.getRoleId() != null) { getObjectService().deleteLink(oldRolePermissionObject.getRoleId(), id); }
        if (oldRolePermissionObject.getObjectId() != null) { getObjectService().deleteLink(oldRolePermissionObject.getObjectId(), id); }
        if (oldRolePermissionObject.getPermissionTypeId() != null) { getObjectService().deleteLink(oldRolePermissionObject.getPermissionTypeId(), id); }
        
        executeUpdate("delete from cw_core_role_permissions where id = ?", id);
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.role.RoleService roleService;
    
    public org.clawiz.core.common.storage.role.RoleService getRoleService() {
        if ( roleService == null ) {
            roleService = getService(org.clawiz.core.common.storage.role.RoleService.class);
        }
        return roleService;
    }
    
    public org.clawiz.core.common.storage.role.RoleObject createRole() {
        return getRoleService().create();
    }
    
    public org.clawiz.core.common.storage.role.RoleObject loadRole(BigDecimal id) {
        return getRoleService().load(id);
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.type.TypeService typeService;
    
    public org.clawiz.core.common.storage.type.TypeService getTypeService() {
        if ( typeService == null ) {
            typeService = getService(org.clawiz.core.common.storage.type.TypeService.class);
        }
        return typeService;
    }
    
    public org.clawiz.core.common.storage.type.TypeObject createType() {
        return getTypeService().create();
    }
    
    public org.clawiz.core.common.storage.type.TypeObject loadType(BigDecimal id) {
        return getTypeService().load(id);
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.permissiontype.PermissionTypeService permissionTypeService;
    
    public org.clawiz.core.common.storage.permissiontype.PermissionTypeService getPermissionTypeService() {
        if ( permissionTypeService == null ) {
            permissionTypeService = getService(org.clawiz.core.common.storage.permissiontype.PermissionTypeService.class);
        }
        return permissionTypeService;
    }
    
    public org.clawiz.core.common.storage.permissiontype.PermissionTypeObject createPermissionType() {
        return getPermissionTypeService().create();
    }
    
    public org.clawiz.core.common.storage.permissiontype.PermissionTypeObject loadPermissionType(BigDecimal id) {
        return getPermissionTypeService().load(id);
    }
}
