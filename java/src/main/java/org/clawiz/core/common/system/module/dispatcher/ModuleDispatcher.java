/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.module.dispatcher;

import org.clawiz.core.common.CoreCommonModule;
import org.clawiz.core.common.Core;
import org.clawiz.core.common.storage.module.ModuleObject;
import org.clawiz.core.common.storage.module.ModuleService;
import org.clawiz.core.common.system.config.Config;
import org.clawiz.core.common.system.config.xml.XMLConfig;
import org.clawiz.core.common.system.module.Module;
import org.clawiz.core.common.system.server.dispatcher.AbstractDispatcher;
import org.clawiz.core.common.system.service.extension.ServiceExtensionManager;
import org.clawiz.core.common.system.session.Session;

import java.util.Collection;
import java.util.HashMap;

/**
 *
 */
public class ModuleDispatcher extends AbstractDispatcher  {

    public static final String CONFIG_MODULES_LIST="modules";

    public static final String CONFIG_MODULE_SECTION="module";
    public static final String CONFIG_MODULE_CLASS="class";

    private HashMap<String, Module> moduleClassesCache = new HashMap<>();
    private HashMap<String, Module> moduleNamesCache   = new HashMap<>();

    public Module registerModule(String moduleClassName, Config moduleConfig) {
        if ( moduleClassName == null ) {
            throwException("Module class name must be defined");
        }

        Session moduleSession  = getSession();
        Module  module         = (Module) moduleSession.getService(Core.getClassByName(moduleClassName));
        module.setConfig(moduleConfig);

        if ( moduleClassesCache.containsKey(moduleClassName)) {
            throwException("Module '?' already registered", new Object[]{moduleClassName});
        }

        ServiceExtensionManager.registerServiceExtensions(module.getServiceExtensionConfigList());
        moduleClassesCache.put(moduleClassName, module);

        logInfo("Module '" + moduleClassName + "' registered");

        return module;
    }

    public Module registerModule(Config moduleConfig) {

        return registerModule(moduleConfig.getString(CONFIG_MODULE_CLASS), moduleConfig);

    }

    @Override
    protected void prepare() {
        Config modulesConfig = Core.getConfig().getConfig(CONFIG_MODULES_LIST);

        if ( modulesConfig == null ) {
            return;
        }

        registerModule(CoreCommonModule.class.getName(), Core.getConfig().getConfig("org.clawiz.core"));
        for(Config moduleConfig : modulesConfig.getConfigList(CONFIG_MODULE_SECTION)) {
            registerModule(moduleConfig);
        }

        // register installed but not configured modules
        if ( Core.isModuleInstalled(CoreCommonModule.class)) {
            for (ModuleObject moduleObject : getSession().getService(ModuleService.class).loadList("1=1", new Object[]{})) {
                if ( moduleObject.getModuleClassName() != null && ! moduleClassesCache.containsKey(moduleObject.getModuleClassName()) ) {
                    registerModule(moduleObject.getModuleClassName(), new XMLConfig());
                }
            }
        }

        commit();



    }

    @Override
    public void start() {
        prepare();
    }

    @Override
    protected void onTime() {

    }

    public Module getModule(String name) {
        return name != null ? moduleClassesCache.get(name.toUpperCase()) : null;
    }

    public Collection<Module> getModules() {
        return moduleClassesCache.values();
    }

    public  <T extends Module> Module getModule(Class<T> clazz) {
        return moduleClassesCache.get(clazz.getName());
    }

}
