/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator.component.service.element.key;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.type.key.TypeKey;
import org.clawiz.core.common.metadata.data.type.key.TypeKeyField;
import org.clawiz.core.common.system.type.generator.component.service.element.AbstractTypeServiceMethodElement;
import org.clawiz.core.common.utils.StringUtils;

public class TypeServiceKeyToIdAutoCreateMethodElement extends AbstractTypeServiceMethodElement {

    TypeKey key;

    public TypeKey getKey() {
        return key;
    }

    public void setKey(TypeKey key) {
        this.key = key;
    }

    @Override
    public void process() {
        super.process();

        setName(StringUtils.toLowerFirstChar(key.getJavaClassName()) + "ToId");
        setType("BigDecimal", "Id of found record, null or id of new record if createNewIfNotFound set to true");

        String parametersLine = "";
        for (TypeKeyField keyField : key.getFields() ) {
            TypeField field = keyField.getField();
            addParameter(field.getValueTypeJavaClassName(), field.getJavaVariableName(), field.getDescription());
            parametersLine += field.getJavaVariableName() + ", ";
        }

        addParameter("boolean", "createNewIfNotFound", "Create new record with passed field values if not matched records exists in database");

        addJavaDoc("Find record id of record by key '" + key.getName() + "' fields or create new record with values set to given parameters");


        addText("");
        addText("BigDecimal id = " + getName() + "(" + parametersLine + "null);");
        addText("if ( id != null || ! createNewIfNotFound ) {");
        addText("    return id;");
        addText("}");
        addText("");
        addText(getDataObjectClassName() + " object = create();");
        for (TypeKeyField keyField : getKey().getFields()) {
            TypeField field = keyField.getField();
            addText("object." + field.getSetMethodName() + "(" + field.getJavaVariableName() + ");");
        }
        addText("");



        addText("save(object);");
        addText("");
        addText("return object.getId();");

    }
}
