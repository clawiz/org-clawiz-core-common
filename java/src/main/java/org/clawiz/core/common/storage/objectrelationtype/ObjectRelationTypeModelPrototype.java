package org.clawiz.core.common.storage.objectrelationtype;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;

public class ObjectRelationTypeModelPrototype extends TypeModel {
    
    private static TypeField NAME_FIELD;
    
    private static TypeField DESCRIPTION_FIELD;
    
    private static TypeField REVERSE_TYPE_ID_FIELD;
    
    private static Type type;
    
    private ObjectRelationTypeService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public ObjectRelationTypeService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(ObjectRelationTypeService.class).getType();
        
        
        NAME_FIELD = type.getFields().get("Name");
        if ( NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.ObjectRelationType.Name"}); }
        
        DESCRIPTION_FIELD = type.getFields().get("Description");
        if ( DESCRIPTION_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.ObjectRelationType.Description"}); }
        
        REVERSE_TYPE_ID_FIELD = type.getFields().get("ReverseType");
        if ( REVERSE_TYPE_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.ObjectRelationType.ReverseType"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _name;
    
    public TypeFieldModel name() {
        
        if ( _name != null ) {
            return _name;
        }
        
        _name = new TypeFieldModel(this, NAME_FIELD);
        return _name;
        
    }
    
    private TypeFieldModel _description;
    
    public TypeFieldModel description() {
        
        if ( _description != null ) {
            return _description;
        }
        
        _description = new TypeFieldModel(this, DESCRIPTION_FIELD);
        return _description;
        
    }
    
    private TypeFieldModel _reverseTypeId;
    
    public TypeFieldModel reverseTypeId() {
        
        if ( _reverseTypeId != null ) {
            return _reverseTypeId;
        }
        
        _reverseTypeId = new TypeFieldModel(this, REVERSE_TYPE_ID_FIELD);
        return _reverseTypeId;
        
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "NAME" : return name();
        case "DESCRIPTION" : return description();
        case "REVERSETYPE" : return reverseTypeId();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
