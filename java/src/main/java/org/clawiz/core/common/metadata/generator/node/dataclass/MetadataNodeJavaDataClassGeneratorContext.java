package org.clawiz.core.common.metadata.generator.node.dataclass;

import org.clawiz.core.common.metadata.generator.node.AbstractMetadataNodeGeneratorContext;
import org.clawiz.core.common.metadata.data.language.node.AbstractLanguageNode;
import org.clawiz.core.common.metadata.data.language.node.element.Structure;

public class MetadataNodeJavaDataClassGeneratorContext extends AbstractMetadataNodeGeneratorContext {

    Structure structure;

    @Override
    public Structure getLanguageNode() {
        return structure;
    }

    @Override
    public void setLanguageNode(AbstractLanguageNode structure) {
        super.setLanguageNode(structure);
        this.structure = (Structure) structure;
    }
}
