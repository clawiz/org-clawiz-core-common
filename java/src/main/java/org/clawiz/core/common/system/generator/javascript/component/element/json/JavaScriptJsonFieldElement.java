/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.javascript.component.element.json;

import org.clawiz.core.common.system.generator.javascript.component.element.AbstractJavaScriptElement;
import org.clawiz.core.common.utils.BigDecimalUtils;
import org.clawiz.core.common.system.generator.file.component.element.StringTextElement;
import org.clawiz.core.common.utils.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: abdrashitovta
 * Date: 01.05.13
 * Time: 13?9
 * To change this template use File | Settings | File Templates.
 */
public class JavaScriptJsonFieldElement extends AbstractJavaScriptElement {

    public enum Type {
        STRING, NUMBER, OBJECT, ARRAY, BOOLEAN
    }
    
    private Type              type;
    private Object            value;
    private ArrayList<Object> values;
    private boolean           addComma = false;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
        switch (type) {
            case ARRAY  : if ( values == null ) { values = new ArrayList<>(); } break;
        }
    }


    public Object getValue() {
        if ( type != Type.ARRAY ) {
            return value;
        }
        return values;
    }

    public ArrayList<Object> getValueList() {
        if ( type != Type.ARRAY ) {
            throwException("Cannot get value list of non-array field ?", getName());
        }
        return values;
    }

    public void setValue(Object value) {
        if ( type == Type.ARRAY ) {
            throwException("Cannot set single value for array field ?", getName());
        }

        this.value = value;
        if ( value == null ) {
            this.type  = null;
            return;
        }

        if ( value instanceof String ) {
            setType(Type.STRING);
        } else if ( value instanceof BigDecimal ) {
            setType(Type.NUMBER);
        } else if ( value instanceof Boolean ) {
            setType(Type.BOOLEAN);
        } else if ( value instanceof JavaScriptJsonObjectElement ) {
            setType(Type.OBJECT);
        } else {
            throwException("Wrong json field ? value class ?",getName(), value.getClass());
        }
    }

    public void setValue(boolean value) {
        setValue(new Boolean(value));
    }

    public void setValue(int value) {
        setValue(new BigDecimal(value));
    }

    public void addValue(Object value) {
        if ( type != Type.ARRAY ) {
            throwException("Cannot add value for non-array field ?", getName());
        }
        values.add(value);
    }

    @Override
    public StringTextElement addText(String text) {
        StringTextElement te = super.addText(text);
        addValue(te);
        return te;
    }

    public void clearValues(Object value) {
        if ( type != Type.ARRAY ) {
            throwException("Cannot clear value list of non-array field ?", getName());
        }
        values.add(value);
    }

    int namesColumnLength = 0;

    public void setNamesColumnLength(int namesColumnLength) {
        this.namesColumnLength = namesColumnLength;
    }


    public void setAddComma(boolean addComma) {
        this.addComma = addComma;
    }


    private void writeValue(Object value) {

        if (value instanceof String ) {
            p("\"" + value + "\"");
        }  else if (value instanceof BigDecimal) {
            p(BigDecimalUtils.toString((BigDecimal) value));
        }  else if (value instanceof Boolean) {
            p(value.toString());
        }  else if (value instanceof JavaScriptJsonObjectElement) {
            ((JavaScriptJsonObjectElement) value).setLpad(getLpad() + "  ");
            ((JavaScriptJsonObjectElement) value).write();
        }  else if (value instanceof StringTextElement) {
            p(((StringTextElement) value).getText());
        } else {
            throwException("Wrong json field ? value class ?", getName(), value != null ? value.getClass() : "NULL");
        }
    }

    @Override
    public void write() {
        if ( type == null || (type != Type.ARRAY && value == null ) ) {
            return;
        }

        p("\"" + getName() + "\""
                +  (namesColumnLength != 0 ? StringUtils.space(namesColumnLength - getName().length()) : "")
                + " : ");
        
        String lpad = getLpad();
        setLpad("");

        if ( type != Type.ARRAY ) {
            writeValue(getValue());
        } else {
            if ( values.size() != 0 ) {
                pln("[");
                for ( int i=0; i < values.size(); i++ ) {
                    boolean addComma = i < values.size()-1;
                    Object value = values.get(i);
                    if ( value instanceof  JavaScriptJsonObjectElement ) {
                        JavaScriptJsonObjectElement objectElement = (JavaScriptJsonObjectElement) value;
                        objectElement.setLpad(getLpad() + "    ");
                        objectElement.setAddComma(addComma);
                        objectElement.write();

                    } else {
                        writeValue(values.get(i));
                        if ( addComma ) {
                            pln(",");
                        }
                    }
                }
                p(lpad + "]");
            } else {
                p("[]");
            }
        }


        pln(addComma ? "," : "");
    }
}
