/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.metadata.node.loader.xml;

import org.clawiz.core.common.metadata.MetadataBase;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeList;
import org.clawiz.core.common.metadata.node.loader.MetadataLoader;
import org.clawiz.core.common.metadata.node.loader.data.MetadataLoaderNodeClassField;
import org.clawiz.core.common.metadata.node.loader.data.MetadataLoaderNodeClass;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.utils.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;

import static org.clawiz.core.common.metadata.node.loader.MetadataLoader.CLAWIZ_EXCHANGE_ELEMENT_NAME;
import static org.clawiz.core.common.metadata.node.loader.MetadataLoader.DICTIONARY_ELEMENT_NAME;
import static org.clawiz.core.common.metadata.node.loader.MetadataLoader.NODES_ELEMENT_NAME;

public class MetadataNodeXMLParser extends Service {

    MetadataLoader metadataLoader;
    MetadataBase   metadataBase;


    private class DeferredReferenceResolve {
        MetadataNode                 node;
        MetadataLoaderNodeClassField field;
        String                       path;

        public DeferredReferenceResolve(MetadataNode node, MetadataLoaderNodeClassField field, String path) {
            this.node        = node;
            this.field       = field;
            this.path        = path;
        }
    }

    private class ParseContext {
        Document       document;
        Element        metadataElement;
        Element        dictionaryElement;
        Element        nodesElement;

        HashMap<String, String>             classAliases              = new HashMap<>();
        ArrayList<DeferredReferenceResolve> deferredReferenceResolves = new ArrayList<>();
        HashMap<String, MetadataNode>       parsedNodesCache          = new HashMap<>();

        public void addDeferredReferenceResolve(MetadataNode node,MetadataLoaderNodeClassField field, String path) {
            deferredReferenceResolves.add(new DeferredReferenceResolve(node, field, path));
        }

    }

    protected void parseElementField(ParseContext context, MetadataNode node, Element element, MetadataLoaderNodeClassField field) {

        if ( element == null ) {
            return;
        }

        if ( field.isArray() ) {

            Object listObject = field.getObject(node);
            if ( listObject == null ) {
                throwException("Field '?' list in node class ? not initialized", field.getName(), node.getClass().getName());
            }
            MetadataNodeList metadataNodeList = (MetadataNodeList) listObject;

            for (int i=0; i < element.getChildNodes().getLength(); i++) {

                if ( ! (element.getChildNodes().item(i) instanceof Element) ) {
                    continue;
                }
                MetadataNode parsedNode = parseElement(context, (Element) element.getChildNodes().item(i), node, field.getName());
                if ( ! field.isReference() ) {
                    parsedNode.setParentNode(node, field.getName());
                }
                metadataNodeList.add(parsedNode);
                context.parsedNodesCache.put(parsedNode.getReferencePath(), parsedNode);

            }

        } else {
            MetadataNode parsedNode = null;
            for (int i=0; i < element.getChildNodes().getLength(); i++) {
                if ( ! (element.getChildNodes().item(i) instanceof Element) ) {
                    continue;
                }
                if ( parsedNode != null ) {
                    throwException("Field '?' is not array and cannot have more than one element childs in exchange file", field.getName());
                }
                parsedNode = parseElement(context, (Element) element.getChildNodes().item(i), node, field.getName());
                field.setObject(node, parsedNode);

                context.parsedNodesCache.put(parsedNode.getReferencePath(), parsedNode);

            }
        }

    }

    protected String getReferencePath(MetadataNode node, Element element, MetadataLoaderNodeClassField field) {

        Element nodeElement = null;
        if ( element.getTagName().equals("node")) {
            nodeElement = element;
        } else {
            for ( int i = 0; i < element.getChildNodes().getLength(); i++) {
                if (!(element.getChildNodes().item(i) instanceof Element)) {
                    continue;
                }
                Element e = (Element) element.getChildNodes().item(i);
                if ( e.getTagName().equals("node") ) {
                    nodeElement = e;
                    break;
                }
            }
        }


        if ( nodeElement == null ) {
            throwException("'node' element not found in reference element node '?' of metadata node '?' (?)", element.getTagName(), node.getFullName(), node.getClass().getName());
        }

        String path        = null;
        for (int in = 0; in < nodeElement.getChildNodes().getLength(); in++) {
            if (!(nodeElement.getChildNodes().item(in) instanceof Element)) {
                continue;
            }
            Element e = (Element) nodeElement.getChildNodes().item(in);
            if ( e.getTagName().equals("path") ) {
                path   = e.getTextContent();
            } else {
                throwException("Wrong metadata node '?' (?) reference field ? tag '?'", node.getFullName(), node.getClass().getName(), field.getName(), element.getTagName());
            }
        }

        if ( path == null ) {
            throwException("Metadata node ? (?) reference field ? element '?' not defined", node.getFullName(), node.getClass().getName(), field.getName(), "path");
        }

        return path;
    }

    protected void parseReferenceField(ParseContext context, MetadataNode node, Element element, MetadataLoaderNodeClassField field) {

        if ( element == null ) {
            return;
        }

        if ( field.isArray() ) {
            Object listObject = field.getObject(node);
            if ( listObject == null ) {
                throwException("Field '?' list in node class ? not initialized", field.getName(), node.getClass().getName());
            }
            for ( int i = 0; i < element.getChildNodes().getLength(); i++) {
                if ( ! (element.getChildNodes().item(i) instanceof Element) ) {
                    continue;
                }
                context.addDeferredReferenceResolve(node, field, getReferencePath(node, (Element) element.getChildNodes().item(i), field));
            }
        } else {
            context.addDeferredReferenceResolve(node, field, getReferencePath(node, element, field));
        }

    }

    private static <T> ArrayList<T> createListOfType(Class<T> type) {
        return new ArrayList<T>();
    }

    protected void parseAttributeField(ParseContext context, MetadataNode node, Element element, MetadataLoaderNodeClassField field) {

        if ( element == null ) {
            return;
        }

        if ( field.getValueTypeClass() == null ) {
            throwException("Cannot parse field ? for null value type class", field.getName());
        }

        if ( field.isArray()) {

            ArrayList list = createListOfType(field.getValueTypeClass());

            NodeList nodeList = element.getChildNodes();
            for ( int i = 0; i < nodeList.getLength(); i++) {
                Node child = nodeList.item(i);
                if ( ! (child instanceof Element ) ) {
                    continue;
                }
                list.add(parseElement(context, (Element) child, node, field.getName()));
            }


            field.setObject(node, list);

        } else {

            String  value = element != null ? element.getTextContent() : null;

            if ( ! StringUtils.isEmpty(value) ) {
                field.setFromString(node, value);
            } else if ( field.isRequired() ) {
                throwException("Required metadata node ? (?) field ? value is null", node.getFullName(), node.getClass().getName(), field.getName());
            }

        }


    }


    public MetadataNode parseElement(ParseContext context, Element element, MetadataNode parentNode, String parentNodeFieldName) {

        String nodeClassName = context.classAliases.get(element.getTagName());
        if ( nodeClassName == null ) {
            nodeClassName = element.getTagName();
        }

        MetadataLoaderNodeClass structure = metadataLoader.getNodeClassStructure(nodeClassName);

        MetadataNode node = null;
        try {
            node = structure.getNodeClass().newInstance();
        } catch (Exception e) {
            throwException("Exception on create metadata node ? instance : ?", structure.getNodeClass().getName(), e.getMessage(), e);
        }
        node.setParentNode(parentNode, parentNodeFieldName);

        HashMap<String, Element> childsCache        = new HashMap<>();
        NodeList nodeList = element.getChildNodes();
        for ( int i = 0; i < nodeList.getLength(); i++) {
            Node child = nodeList.item(i);
            if ( child instanceof Element ) {
                Element e = (Element) child;
                childsCache.put(e.getTagName(), e);
            }
        }

        for (MetadataLoaderNodeClassField field : structure.getFields()) {

            Element child = childsCache.get(field.getName());
            if ( child == null && field.isRequired() ) {
                throwException("Required metadata node ? (?) field ? element is not present", node.getFullName(), node.getClass().getName(), field.getName());
            }

            if ( field.isElement() ) {

                parseElementField(context, node, child, field);

            } else if ( field.isReference() ) {

                parseReferenceField(context, node, child, field);

            } else {
                parseAttributeField(context, node, child, field);

            }

        }

        context.parsedNodesCache.put(node.getReferencePath(), node);

        return node;
    }

    protected void parseDictionary(ParseContext context) {

        for (int i=0; i < context.dictionaryElement.getChildNodes().getLength(); i++) {
            Node child = context.dictionaryElement.getChildNodes().item(i);
            if ( ! (child instanceof Element ) ) {
                continue;
            }
            Element element = (Element) child;

            String alias     = element.getAttribute("alias");
            String className = element.getAttribute("class");

            if ( StringUtils.isEmpty(alias)) {
                throwException("Class alias cannot be null");
            }

            if ( StringUtils.isEmpty(className)) {
                throwException("Class name in alias element cannot be null");
            }

            context.classAliases.put(alias, className);

        }

    }

    protected void parseHeader(ParseContext context) {

        Element clawizMetadata = context.document.getDocumentElement();
        if ( ! clawizMetadata.getTagName().equals(CLAWIZ_EXCHANGE_ELEMENT_NAME)) {
            throwException("Wrong metadata xml format - document element is not '?'", CLAWIZ_EXCHANGE_ELEMENT_NAME);
        }
        context.metadataElement = clawizMetadata;

        for ( int i = 0; i < clawizMetadata.getChildNodes().getLength(); i++) {

            Node child = clawizMetadata.getChildNodes().item(i);
            if ( ! (child instanceof Element ) ) {
                continue;
            }
            Element element = (Element) child;

            if ( element.getTagName().equals(DICTIONARY_ELEMENT_NAME) ) {

                if ( context.dictionaryElement != null ) {
                    throwException("Metadata data xml can have only one element '?'", DICTIONARY_ELEMENT_NAME);
                }
                context.dictionaryElement = element;
                parseDictionary(context);

            } else if ( element.getTagName().equals(NODES_ELEMENT_NAME) ) {

                if ( context.nodesElement != null ) {
                    throwException("Metadata data xml can have only one element '?'", NODES_ELEMENT_NAME);
                }
                context.nodesElement = element;


            } else {
                throwException("Wrong metadata xml element tag name '?'", element.getTagName());
            }


        }

    }

    public MetadataNodeList parse(String xml) {

        Document document = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throwException("Exception on create XML document builder : ?", e.getMessage(), e);
        }
        InputSource is = new InputSource(new StringReader(xml));
        try {
            document = builder.parse( is );
        } catch (Exception e) {
            throwException("Exception on parse XML document : ? '\n<xml>\n?\n</xml>", e.getMessage(), e, xml);
        }

        ParseContext context = new ParseContext();
        context.document = document;

        parseHeader(context);

        MetadataNodeList result = new MetadataNodeList();

        if ( context.nodesElement != null ) {
            for (int i=0; i < context.nodesElement.getChildNodes().getLength(); i++) {
                Node child = context.nodesElement.getChildNodes().item(i);
                if (!(child instanceof Element)) {
                    continue;
                }
                result.add(parseElement(context, (Element) child, null, null));
            }
        } else {
            throwException("Element '?' not found clawiz metadata xml", NODES_ELEMENT_NAME);
        }

        for(DeferredReferenceResolve resolve : context.deferredReferenceResolves ) {
            MetadataNode resolvedNode = context.parsedNodesCache.get(resolve.path );
            if ( resolvedNode == null ) {
                resolvedNode = metadataBase.getNodeByPath(resolve.path, false);
            }
            if ( resolvedNode == null ) {
                throwException("Cannot found reference '?' for node '?' of class ?", resolve.path
                        ,resolve.node.getFullName(), resolve.node.getClass().getName());
            }
            if ( resolve.field.isArray() ) {
                resolve.field.getAsNodeList(resolve.node).add(resolvedNode);
            } else {
                resolve.field.setObject(resolve.node, resolvedNode);
            }
        }

        return result;

    }

}
