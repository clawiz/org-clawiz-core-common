package org.clawiz.core.common.storage.remotenode;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;
import org.clawiz.core.common.system.service.NotInitializeService;

public class RemoteNodeModelPrototype extends TypeModel {
    
    private static TypeField NAME_FIELD;
    
    private static TypeField PASSWORD_FIELD;
    
    private static TypeField PASSWORD_HASH_FIELD;
    
    private static TypeField PREVIOUS_PASSWORD_FIELD;
    
    private static TypeField LAST_RECEIVED_SCN_FIELD;
    
    private static TypeField REGISTERED_USER_ID_FIELD;
    
    private static Type type;
    
    private RemoteNodeService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public RemoteNodeService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(RemoteNodeService.class).getType();
        
        
        NAME_FIELD = type.getFields().get("Name");
        if ( NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.RemoteNode.Name"}); }
        
        PASSWORD_FIELD = type.getFields().get("Password");
        if ( PASSWORD_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.RemoteNode.Password"}); }
        
        PASSWORD_HASH_FIELD = type.getFields().get("PasswordHash");
        if ( PASSWORD_HASH_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.RemoteNode.PasswordHash"}); }
        
        PREVIOUS_PASSWORD_FIELD = type.getFields().get("PreviousPassword");
        if ( PREVIOUS_PASSWORD_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.RemoteNode.PreviousPassword"}); }
        
        LAST_RECEIVED_SCN_FIELD = type.getFields().get("LastReceivedScn");
        if ( LAST_RECEIVED_SCN_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.RemoteNode.LastReceivedScn"}); }
        
        REGISTERED_USER_ID_FIELD = type.getFields().get("RegisteredUser");
        if ( REGISTERED_USER_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.RemoteNode.RegisteredUser"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _name;
    
    public TypeFieldModel name() {
        
        if ( _name != null ) {
            return _name;
        }
        
        _name = new TypeFieldModel(this, NAME_FIELD);
        return _name;
        
    }
    
    private TypeFieldModel _password;
    
    public TypeFieldModel password() {
        
        if ( _password != null ) {
            return _password;
        }
        
        _password = new TypeFieldModel(this, PASSWORD_FIELD);
        return _password;
        
    }
    
    private TypeFieldModel _passwordHash;
    
    public TypeFieldModel passwordHash() {
        
        if ( _passwordHash != null ) {
            return _passwordHash;
        }
        
        _passwordHash = new TypeFieldModel(this, PASSWORD_HASH_FIELD);
        return _passwordHash;
        
    }
    
    private TypeFieldModel _previousPassword;
    
    public TypeFieldModel previousPassword() {
        
        if ( _previousPassword != null ) {
            return _previousPassword;
        }
        
        _previousPassword = new TypeFieldModel(this, PREVIOUS_PASSWORD_FIELD);
        return _previousPassword;
        
    }
    
    private TypeFieldModel _lastReceivedScn;
    
    public TypeFieldModel lastReceivedScn() {
        
        if ( _lastReceivedScn != null ) {
            return _lastReceivedScn;
        }
        
        _lastReceivedScn = new TypeFieldModel(this, LAST_RECEIVED_SCN_FIELD);
        return _lastReceivedScn;
        
    }
    
    private TypeFieldModel _registeredUserId;
    
    public TypeFieldModel registeredUserId() {
        
        if ( _registeredUserId != null ) {
            return _registeredUserId;
        }
        
        _registeredUserId = new TypeFieldModel(this, REGISTERED_USER_ID_FIELD);
        return _registeredUserId;
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.user.UserModel _registeredUserTypeModel;
    
    public org.clawiz.core.common.storage.user.UserModel registeredUser() {
        
        if ( _registeredUserTypeModel == null ) {
            org.clawiz.core.common.storage.user.UserModel model = getService(org.clawiz.core.common.storage.user.UserModel.class, this.toString() + "_RegisteredUser");
            model.setJoinField(registeredUserId());
            _registeredUserTypeModel = model;
        }
        
        return _registeredUserTypeModel;
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "NAME" : return name();
        case "PASSWORD" : return password();
        case "PASSWORDHASH" : return passwordHash();
        case "PREVIOUSPASSWORD" : return previousPassword();
        case "LASTRECEIVEDSCN" : return lastReceivedScn();
        case "REGISTEREDUSER" : return registeredUserId();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
