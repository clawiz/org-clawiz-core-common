package org.clawiz.core.common.metadata.data.language.node.element.field;

import org.clawiz.core.common.metadata.data.language.node.enumeration.Enumeration;
import org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeBoolean;
import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;

public class AttributeStructureField extends AbstractStructureField {

    @ExchangeElement
    AbstractValueType       valueType;

    @ExchangeReference
    Enumeration             enumeration;

    public AbstractValueType getValueType() {
        return valueType;
    }

    public void setValueType(AbstractValueType valueType) {
        this.valueType = valueType;
    }

    public String getGetMethodName() {
        return ( valueType instanceof ValueTypeBoolean ? "is" : "get")
                + getJavaClassName();
    }

    public Enumeration getEnumeration() {
        return enumeration;
    }

    public void setEnumeration(Enumeration enumeration) {
        this.enumeration = enumeration;
    }
}
