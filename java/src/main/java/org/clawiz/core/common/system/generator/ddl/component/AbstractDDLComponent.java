/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.ddl.component;

import org.clawiz.core.common.system.database.Connection;
import org.clawiz.core.common.system.generator.abstractgenerator.component.AbstractComponent;
import org.clawiz.core.common.system.generator.abstractgenerator.writer.AbstractWriter;
import org.clawiz.core.common.system.generator.ddl.AbstractDDLGenerator;
import org.clawiz.core.common.system.generator.ddl.AbstractDDLWriter;

/**
 * (c) Clawiz
 */
public abstract class AbstractDDLComponent extends AbstractComponent {

    Connection targetConnection;

    public Connection getTargetConnection() {
        return targetConnection;
    }

    public void setTargetConnection(Connection targetConnection) {
        this.targetConnection = targetConnection;
    }

    @Override
    public void prepare() {
        setTargetConnection(((AbstractDDLGenerator)getGenerator()).getTargetConnection());
        super.prepare();
    }

    @Override
    protected AbstractWriter createWriter() {
        AbstractDDLWriter writer = new AbstractDDLWriter();
        if ( getTargetConnection() == null ) {
            throwException("Target DDL connection not defined for component '?'", new Object[]{getName()});
        }
        writer.setDdlConnection(getTargetConnection());
        return writer;
    }
}
