package org.clawiz.core.common.storage.metadatanode;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;
import org.clawiz.core.common.system.service.NotInitializeService;

public class MetadataNodeModelPrototype extends TypeModel {
    
    private static TypeField PACKAGE_NAME_FIELD;
    
    private static TypeField NAME_FIELD;
    
    private static TypeField NODE_CLASS_ID_FIELD;
    
    private static TypeField STORAGE_ID_FIELD;
    
    private static TypeField NODE_OBJECT_ID_FIELD;
    
    private static Type type;
    
    private MetadataNodeService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public MetadataNodeService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(MetadataNodeService.class).getType();
        
        
        PACKAGE_NAME_FIELD = type.getFields().get("PackageName");
        if ( PACKAGE_NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.MetadataNode.PackageName"}); }
        
        NAME_FIELD = type.getFields().get("Name");
        if ( NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.MetadataNode.Name"}); }
        
        NODE_CLASS_ID_FIELD = type.getFields().get("NodeClass");
        if ( NODE_CLASS_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.MetadataNode.NodeClass"}); }
        
        STORAGE_ID_FIELD = type.getFields().get("Storage");
        if ( STORAGE_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.MetadataNode.Storage"}); }
        
        NODE_OBJECT_ID_FIELD = type.getFields().get("NodeObject");
        if ( NODE_OBJECT_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.MetadataNode.NodeObject"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _packageName;
    
    public TypeFieldModel packageName() {
        
        if ( _packageName != null ) {
            return _packageName;
        }
        
        _packageName = new TypeFieldModel(this, PACKAGE_NAME_FIELD);
        return _packageName;
        
    }
    
    private TypeFieldModel _name;
    
    public TypeFieldModel name() {
        
        if ( _name != null ) {
            return _name;
        }
        
        _name = new TypeFieldModel(this, NAME_FIELD);
        return _name;
        
    }
    
    private TypeFieldModel _nodeClassId;
    
    public TypeFieldModel nodeClassId() {
        
        if ( _nodeClassId != null ) {
            return _nodeClassId;
        }
        
        _nodeClassId = new TypeFieldModel(this, NODE_CLASS_ID_FIELD);
        return _nodeClassId;
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.metadatanodeclass.MetadataNodeClassModel _nodeClassTypeModel;
    
    public org.clawiz.core.common.storage.metadatanodeclass.MetadataNodeClassModel nodeClass() {
        
        if ( _nodeClassTypeModel == null ) {
            org.clawiz.core.common.storage.metadatanodeclass.MetadataNodeClassModel model = getService(org.clawiz.core.common.storage.metadatanodeclass.MetadataNodeClassModel.class, this.toString() + "_NodeClass");
            model.setJoinField(nodeClassId());
            _nodeClassTypeModel = model;
        }
        
        return _nodeClassTypeModel;
    }
    
    private TypeFieldModel _storageId;
    
    public TypeFieldModel storageId() {
        
        if ( _storageId != null ) {
            return _storageId;
        }
        
        _storageId = new TypeFieldModel(this, STORAGE_ID_FIELD);
        return _storageId;
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.metadatastorage.MetadataStorageModel _storageTypeModel;
    
    public org.clawiz.core.common.storage.metadatastorage.MetadataStorageModel storage() {
        
        if ( _storageTypeModel == null ) {
            org.clawiz.core.common.storage.metadatastorage.MetadataStorageModel model = getService(org.clawiz.core.common.storage.metadatastorage.MetadataStorageModel.class, this.toString() + "_Storage");
            model.setJoinField(storageId());
            _storageTypeModel = model;
        }
        
        return _storageTypeModel;
    }
    
    private TypeFieldModel _nodeObjectId;
    
    public TypeFieldModel nodeObjectId() {
        
        if ( _nodeObjectId != null ) {
            return _nodeObjectId;
        }
        
        _nodeObjectId = new TypeFieldModel(this, NODE_OBJECT_ID_FIELD);
        return _nodeObjectId;
        
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "PACKAGENAME" : return packageName();
        case "NAME" : return name();
        case "NODECLASS" : return nodeClassId();
        case "STORAGE" : return storageId();
        case "NODEOBJECT" : return nodeObjectId();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
