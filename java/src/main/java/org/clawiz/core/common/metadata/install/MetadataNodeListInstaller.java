/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.metadata.install;

import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeList;
import org.clawiz.core.common.metadata.node.storage.MetadataNodeResourcesXMLStorage;
import org.clawiz.core.common.system.installer.AbstractInstaller;

public class MetadataNodeListInstaller extends AbstractInstaller {

    MetadataNodeList<MetadataNode>  nodes;
    MetadataNodeResourcesXMLStorage metadataNodeResourcesXMLStorage;


    public MetadataNodeList getNodes() {
        return nodes;
    }

    public void setNodes(MetadataNodeList nodes) {
        this.nodes = nodes;
    }

    public static final String EXTENSION_BEFORE_INSTALL_NODE = "beforeInstallNode";

    public void beforeInstallNode(MetadataNode node) {
        processExtensions(EXTENSION_BEFORE_INSTALL_NODE, (MetadataNode) node);
    }


    public void installMetadataNode(MetadataNode node) {

        Class<AbstractMetadataNodeInstaller> installerClass = node.getInstallerClass();
        if ( installerClass != null ) {
            AbstractMetadataNodeInstaller installer = addInstaller(installerClass);
            installer.setNode(node);
        } else {
//            logDebug("Node " + node.getName() + " of class " + node.getClass().getName() + " installer class not defined. Installation skipped");
            throwException("Installer class not defined for node ? of class ?", node.getName(), node.getClass().getName());
        }

    }

    public void saveMetadataNodes() {

        if ( getDestinationPath() == null  ) {
            return;
        }

        for ( MetadataNode node : nodes ) {
            metadataNodeResourcesXMLStorage.saveNode(node, getDestinationPath());
        }

    }

    @Override
    public void process() {

        saveMetadataNodes();

        nodes.forEach(this::beforeInstallNode);
        nodes.forEach(this::installMetadataNode);

    }


}
