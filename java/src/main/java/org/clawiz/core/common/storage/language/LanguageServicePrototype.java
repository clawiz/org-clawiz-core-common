package org.clawiz.core.common.storage.language;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;

public class LanguageServicePrototype extends AbstractTypeService<LanguageObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.core.common.storage", "Language");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Language"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      language Checked object
    */
    public void check(LanguageObject language) {
        
        if ( language == null ) {
            throwException("Cannot check null ?", new Object[]{"LanguageObject"});
        }
        
        language.fillDefaults();
        
        
        if ( codeToId(language.getCode(), language.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "Language", language.toCode() });
        }
        
        if ( nameToId(language.getName(), language.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "Language", language.toName() });
        }
        
    }
    
    /**
    * Create new LanguageObject instance and fill default values
    * 
    * @return     Created object
    */
    public LanguageObject create() {
        
        LanguageObject language = new LanguageObject();
        language.setService((LanguageService) this);
        
        language.fillDefaults();
        
        return language;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public LanguageObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select code, name, description from cw_core_languages where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"Language", id});
        }
        
        LanguageObject result = new LanguageObject();
        
        result.setService((LanguageService) this);
        result.setId(id);
        result.setCode(statement.getString(1));
        result.setName(statement.getString(2));
        result.setDescription(statement.getString(3));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of LanguageObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public LanguageList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of LanguageObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public LanguageList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        LanguageList result = new LanguageList();
        
        
        Statement statement = executeQuery("select id, code, name, description from cw_core_languages"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            LanguageObject object = new LanguageObject();
        
            object.setService((LanguageService) this);
            object.setId(statement.getBigDecimal(1));
            object.setCode(statement.getString(2));
            object.setName(statement.getString(3));
            object.setDescription(statement.getString(4));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'code' fields
    * 
    * @param      code Code
    * @return     Id of found record or null
    */
    public BigDecimal codeToId(java.lang.String code) {
        return codeToId(code, null);
    }
    
    /**
    * Find id of record by key 'code' fields with id not equal skipId
    * 
    * @param      code   Code
    * @param      skipId Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal codeToId(java.lang.String code, BigDecimal skipId) {
        
        if ( code == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_languages where upper_code = ?", code.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_languages where upper_code = ? and id != ?", code.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'code' fields or create new record with values set to given parameters
    * 
    * @param      code                Code
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal codeToId(java.lang.String code, boolean createNewIfNotFound) {
        
        BigDecimal id = codeToId(code, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        LanguageObject object = create();
        object.setCode(code);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'code' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToCode(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select code from cw_core_languages where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        LanguageObject object = new LanguageObject();
        object.setService((LanguageService)this);
        object.setId(id);
        object.setCode(statement.getString(1));
        
        statement.close();
        
        return object.toCode();
    }
    
    /**
    * Find id of record by key 'name' fields
    * 
    * @param      name Name
    * @return     Id of found record or null
    */
    public BigDecimal nameToId(java.lang.String name) {
        return nameToId(name, null);
    }
    
    /**
    * Find id of record by key 'name' fields with id not equal skipId
    * 
    * @param      name   Name
    * @param      skipId Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal nameToId(java.lang.String name, BigDecimal skipId) {
        
        if ( name == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_languages where upper_name = ?", name.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_languages where upper_name = ? and id != ?", name.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'name' fields or create new record with values set to given parameters
    * 
    * @param      name                Name
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal nameToId(java.lang.String name, boolean createNewIfNotFound) {
        
        BigDecimal id = nameToId(name, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        LanguageObject object = create();
        object.setName(name);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'name' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select name from cw_core_languages where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        LanguageObject object = new LanguageObject();
        object.setService((LanguageService)this);
        object.setId(id);
        object.setName(statement.getString(1));
        
        statement.close();
        
        return object.toName();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToName(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      languageObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(LanguageObject languageObject) {
        return languageObject.toName();
    }
    
    protected void saveAudit(TransactionAction transactionAction, LanguageObject oldLanguageObject, LanguageObject newLanguageObject) {
        
        LanguageObject o = oldLanguageObject != null ? oldLanguageObject : new LanguageObject();
        LanguageObject n = newLanguageObject != null ? newLanguageObject : new LanguageObject();
        
        
        executeUpdate("insert into a_cw_core_languages (scn, action_type, id , o_code, o_upper_code, o_name, o_upper_name, o_description, n_code, n_upper_code, n_name, n_upper_name, n_description) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getCode(), o.getCode() != null ? o.getCode().toUpperCase() : null, o.getName(), o.getName() != null ? o.getName().toUpperCase() : null, o.getDescription(), n.getCode(), n.getCode() != null ? n.getCode().toUpperCase() : null, n.getName(), n.getName() != null ? n.getName().toUpperCase() : null, n.getDescription());
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      language Saved object
    */
    public void save(LanguageObject language) {
        
        if ( language == null ) {
            throwException("Cannot save NULL ?", new Object[]{"LanguageObject"});
        }
        
        TransactionAction transactionAction;
        LanguageObject oldLanguage;
        if ( language.getService() == null ) {
            language.setService((LanguageService)this);
        }
        
        check(language);
        
        if ( language.getId() == null ) {
        
            language.setId(getObjectService().createObject(getTypeId()));
        
            oldLanguage = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, language.getId());
        
            executeUpdate("insert into cw_core_languages" 
                          + "( id, code, upper_code, name, upper_name, description ) "
                          + "values"
                          + "(?, ?, ?, ?, ?, ?)",
                          language.getId(), language.getCode(), ( language.getCode() != null ? language.getCode().toUpperCase() : null ), language.getName(), ( language.getName() != null ? language.getName().toUpperCase() : null ), language.getDescription() );
        
        } else {
        
            oldLanguage = load(language.getId());
            if ( oldLanguage.equals(language) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, language.getId());
        
            executeUpdate("update cw_core_languages set "
                          + "code = ?, upper_code = ?, name = ?, upper_name = ?, description = ? "
                          + "where id = ?",
                          language.getCode(), ( language.getCode() != null ? language.getCode().toUpperCase() : null ), language.getName(), ( language.getName() != null ? language.getName().toUpperCase() : null ), language.getDescription(), language.getId() );
        
        }
        
        saveAudit(transactionAction, oldLanguage, language);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        LanguageObject oldLanguageObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldLanguageObject, null);
        
        getObjectService().deleteObject(id);
        
        executeUpdate("delete from cw_core_languages where id = ?", id);
        
    }
}
