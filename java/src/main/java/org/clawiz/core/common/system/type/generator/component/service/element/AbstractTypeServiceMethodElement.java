/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator.component.service.element;

import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.property.extension.AbstractExtensionTypeProperty;
import org.clawiz.core.common.metadata.data.type.property.extension.action.CallServiceMethodExtensionTypePropertyAction;
import org.clawiz.core.common.metadata.data.type.property.extension.method.AbstractExtensionTypePropertyMethod;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.generator.component.AbstractTypeGeneratorJavaClassMethodElement;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeBoolean;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.core.common.utils.StringUtils;

public class AbstractTypeServiceMethodElement extends AbstractTypeGeneratorJavaClassMethodElement {

    public String getFieldStatementGetter(TypeField field, int index) {
        if ( field.getValueType() instanceof ValueTypeBoolean) {
            return "(\"T\".equals(statement.getString(" + index + ")))";
        } else if ( field.getValueType() instanceof ValueTypeEnumeration) {
            return field.getValueTypeJavaClassName() + ".to" + field.getValueTypeJavaClassName()
                    + "(statement.getString(" + index + "))";
        } else {
            String getter = "";
            switch (field.getValueType().getDatabaseTableColumnType()) {
                case VARCHAR  : getter = "String"; break;
                case NUMBER   : getter = "BigDecimal"; break;
                case DATE     : getter = "Date"; break;
                case DATETIME : getter = "Date"; break;
                default: throwException("Cannot define select statement getter for field ? database column type ?", field.getFullName(), field.getValueType().getDatabaseTableColumnType().toString());
            }
            return "statement.get" + getter + "(" + index + ")";
        }
    }

    public <E extends AbstractExtensionTypeProperty, M extends AbstractExtensionTypePropertyMethod> void addExtensions(Class<E> extensionClass, Class<M> extensionMethodClass) {

        Type type = getGenerator().getType();

        for ( AbstractExtensionTypeProperty extension : type.getProperties().getNodeListByClass(AbstractExtensionTypeProperty.class)) {
            if ( extension.getClass() != extensionClass ) {
                continue;
            }
            if ( extension.getMethod().getClass() != extensionMethodClass ) {
                continue;
            }

            if ( extension.getAction() instanceof CallServiceMethodExtensionTypePropertyAction) {

                CallServiceMethodExtensionTypePropertyAction csa = (CallServiceMethodExtensionTypePropertyAction) extension.getAction();
                String variableType  = csa.getServiceMethod().getService().getFullName();
                String variableName  = StringUtils.toLowerFirstChar(GeneratorUtils.toJavaClassName(variableType));

                addText(variableName + "." + csa.getServiceMethod().getName() + "(" + getTypeJavaVariableName() + ");");

            } else {
                throwException("Wrong type ? extension ?.? action class ?", type.getFullName(), extensionClass.getName(), extensionMethodClass.getName(), extension.getAction().getClass().getName());
            }
            addText("");
        }

    }

}
