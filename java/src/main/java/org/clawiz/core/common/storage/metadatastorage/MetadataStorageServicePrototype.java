package org.clawiz.core.common.storage.metadatastorage;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;
import org.clawiz.core.common.system.service.NotInitializeService;

public class MetadataStorageServicePrototype extends AbstractTypeService<MetadataStorageObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.core.common.storage", "MetadataStorage");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.MetadataStorage"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      metadataStorage Checked object
    */
    public void check(MetadataStorageObject metadataStorage) {
        
        if ( metadataStorage == null ) {
            throwException("Cannot check null ?", new Object[]{"MetadataStorageObject"});
        }
        
        metadataStorage.fillDefaults();
        
        
        if ( storageClassPackageUrlToId(metadataStorage.getStorageClassId(), metadataStorage.getPackageName(), metadataStorage.getUrl(), metadataStorage.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "MetadataStorage", metadataStorage.toStorageClassPackageUrl() });
        }
        
    }
    
    /**
    * Create new MetadataStorageObject instance and fill default values
    * 
    * @return     Created object
    */
    public MetadataStorageObject create() {
        
        MetadataStorageObject metadataStorage = new MetadataStorageObject();
        metadataStorage.setService((MetadataStorageService) this);
        
        metadataStorage.fillDefaults();
        
        return metadataStorage;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public MetadataStorageObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select storage_class_id, package_name, url from cw_core_mdata_storages where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"MetadataStorage", id});
        }
        
        MetadataStorageObject result = new MetadataStorageObject();
        
        result.setService((MetadataStorageService) this);
        result.setId(id);
        result.setStorageClassId(statement.getBigDecimal(1));
        result.setPackageName(statement.getString(2));
        result.setUrl(statement.getString(3));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of MetadataStorageObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public MetadataStorageList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of MetadataStorageObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public MetadataStorageList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        MetadataStorageList result = new MetadataStorageList();
        
        
        Statement statement = executeQuery("select id, storage_class_id, package_name, url from cw_core_mdata_storages"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            MetadataStorageObject object = new MetadataStorageObject();
        
            object.setService((MetadataStorageService) this);
            object.setId(statement.getBigDecimal(1));
            object.setStorageClassId(statement.getBigDecimal(2));
            object.setPackageName(statement.getString(3));
            object.setUrl(statement.getString(4));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'StorageClassPackageUrl' fields
    * 
    * @param      storageClassId StorageClass
    * @param      packageName    PackageName
    * @param      url            Url
    * @return     Id of found record or null
    */
    public BigDecimal storageClassPackageUrlToId(java.math.BigDecimal storageClassId, java.lang.String packageName, java.lang.String url) {
        return storageClassPackageUrlToId(storageClassId, packageName, url, null);
    }
    
    /**
    * Find id of record by key 'StorageClassPackageUrl' fields with id not equal skipId
    * 
    * @param      storageClassId StorageClass
    * @param      packageName    PackageName
    * @param      url            Url
    * @param      skipId         Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal storageClassPackageUrlToId(java.math.BigDecimal storageClassId, java.lang.String packageName, java.lang.String url, BigDecimal skipId) {
        
        if ( storageClassId == null || packageName == null || url == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_mdata_storages where storage_class_id = ? and upper_package_name = ? and upper_url = ?", storageClassId, packageName.toUpperCase(), url.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_mdata_storages where storage_class_id = ? and upper_package_name = ? and upper_url = ? and id != ?", storageClassId, packageName.toUpperCase(), url.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'StorageClassPackageUrl' fields or create new record with values set to given parameters
    * 
    * @param      storageClassId      StorageClass
    * @param      packageName         PackageName
    * @param      url                 Url
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal storageClassPackageUrlToId(java.math.BigDecimal storageClassId, java.lang.String packageName, java.lang.String url, boolean createNewIfNotFound) {
        
        BigDecimal id = storageClassPackageUrlToId(storageClassId, packageName, url, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        MetadataStorageObject object = create();
        object.setStorageClassId(storageClassId);
        object.setPackageName(packageName);
        object.setUrl(url);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'StorageClassPackageUrl' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToStorageClassPackageUrl(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select storage_class_id, package_name, url from cw_core_mdata_storages where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        MetadataStorageObject object = new MetadataStorageObject();
        object.setService((MetadataStorageService)this);
        object.setId(id);
        object.setStorageClassId(statement.getBigDecimal(1));
        object.setPackageName(statement.getString(2));
        object.setUrl(statement.getString(3));
        
        statement.close();
        
        return object.toStorageClassPackageUrl();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'Url' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToUrl(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select url from cw_core_mdata_storages where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        MetadataStorageObject object = new MetadataStorageObject();
        object.setService((MetadataStorageService)this);
        object.setId(id);
        object.setUrl(statement.getString(1));
        
        statement.close();
        
        return object.toUrl();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToStorageClassPackageUrl(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      metadataStorageObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(MetadataStorageObject metadataStorageObject) {
        return metadataStorageObject.toStorageClassPackageUrl();
    }
    
    protected void saveAudit(TransactionAction transactionAction, MetadataStorageObject oldMetadataStorageObject, MetadataStorageObject newMetadataStorageObject) {
        
        MetadataStorageObject o = oldMetadataStorageObject != null ? oldMetadataStorageObject : new MetadataStorageObject();
        MetadataStorageObject n = newMetadataStorageObject != null ? newMetadataStorageObject : new MetadataStorageObject();
        
        
        executeUpdate("insert into a_cw_core_mdata_storages (scn, action_type, id , o_storage_class_id, o_package_name, o_upper_package_name, o_url, o_upper_url, n_storage_class_id, n_package_name, n_upper_package_name, n_url, n_upper_url) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getStorageClassId(), o.getPackageName(), o.getPackageName() != null ? o.getPackageName().toUpperCase() : null, o.getUrl(), o.getUrl() != null ? o.getUrl().toUpperCase() : null, n.getStorageClassId(), n.getPackageName(), n.getPackageName() != null ? n.getPackageName().toUpperCase() : null, n.getUrl(), n.getUrl() != null ? n.getUrl().toUpperCase() : null);
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      metadataStorage Saved object
    */
    public void save(MetadataStorageObject metadataStorage) {
        
        if ( metadataStorage == null ) {
            throwException("Cannot save NULL ?", new Object[]{"MetadataStorageObject"});
        }
        
        TransactionAction transactionAction;
        MetadataStorageObject oldMetadataStorage;
        if ( metadataStorage.getService() == null ) {
            metadataStorage.setService((MetadataStorageService)this);
        }
        
        check(metadataStorage);
        
        if ( metadataStorage.getId() == null ) {
        
            metadataStorage.setId(getObjectService().createObject(getTypeId()));
        
            oldMetadataStorage = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, metadataStorage.getId());
        
            executeUpdate("insert into cw_core_mdata_storages" 
                          + "( id, storage_class_id, package_name, upper_package_name, url, upper_url ) "
                          + "values"
                          + "(?, ?, ?, ?, ?, ?)",
                          metadataStorage.getId(), metadataStorage.getStorageClassId(), metadataStorage.getPackageName(), ( metadataStorage.getPackageName() != null ? metadataStorage.getPackageName().toUpperCase() : null ), metadataStorage.getUrl(), ( metadataStorage.getUrl() != null ? metadataStorage.getUrl().toUpperCase() : null ) );
        
            if ( metadataStorage.getStorageClassId() != null ) { getObjectService().setLink(metadataStorage.getStorageClassId(), metadataStorage.getId()); }
        
        } else {
        
            oldMetadataStorage = load(metadataStorage.getId());
            if ( oldMetadataStorage.equals(metadataStorage) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, metadataStorage.getId());
        
            executeUpdate("update cw_core_mdata_storages set "
                          + "storage_class_id = ?, package_name = ?, upper_package_name = ?, url = ?, upper_url = ? "
                          + "where id = ?",
                          metadataStorage.getStorageClassId(), metadataStorage.getPackageName(), ( metadataStorage.getPackageName() != null ? metadataStorage.getPackageName().toUpperCase() : null ), metadataStorage.getUrl(), ( metadataStorage.getUrl() != null ? metadataStorage.getUrl().toUpperCase() : null ), metadataStorage.getId() );
        
            getObjectService().changeLinkParent(oldMetadataStorage.getStorageClassId(), metadataStorage.getStorageClassId(), metadataStorage.getId());
        
        }
        
        saveAudit(transactionAction, oldMetadataStorage, metadataStorage);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        MetadataStorageObject oldMetadataStorageObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldMetadataStorageObject, null);
        
        getObjectService().deleteObject(id);
        
        if (oldMetadataStorageObject.getStorageClassId() != null) { getObjectService().deleteLink(oldMetadataStorageObject.getStorageClassId(), id); }
        
        executeUpdate("delete from cw_core_mdata_storages where id = ?", id);
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.metadatastorageclass.MetadataStorageClassService metadataStorageClassService;
    
    public org.clawiz.core.common.storage.metadatastorageclass.MetadataStorageClassService getMetadataStorageClassService() {
        if ( metadataStorageClassService == null ) {
            metadataStorageClassService = getService(org.clawiz.core.common.storage.metadatastorageclass.MetadataStorageClassService.class);
        }
        return metadataStorageClassService;
    }
    
    public org.clawiz.core.common.storage.metadatastorageclass.MetadataStorageClassObject createMetadataStorageClass() {
        return getMetadataStorageClassService().create();
    }
    
    public org.clawiz.core.common.storage.metadatastorageclass.MetadataStorageClassObject loadMetadataStorageClass(BigDecimal id) {
        return getMetadataStorageClassService().load(id);
    }
}
