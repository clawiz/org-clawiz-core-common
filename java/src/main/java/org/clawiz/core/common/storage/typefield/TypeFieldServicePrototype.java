package org.clawiz.core.common.storage.typefield;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;

public class TypeFieldServicePrototype extends AbstractTypeService<TypeFieldObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).fullNameToId("org.clawiz.core.common.storage.TypeField");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeField"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      typeField Checked object
    */
    public void check(TypeFieldObject typeField) {
        
        if ( typeField == null ) {
            throwException("Cannot check null ?", new Object[]{"TypeFieldObject"});
        }
        
        typeField.fillDefaults();
        
        
        if ( typeNameToId(typeField.getTypeId(), typeField.getName(), typeField.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "TypeField", typeField.toTypeName() });
        }
        
        if ( typeColumnNameToId(typeField.getTypeId(), typeField.getColumnName(), typeField.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "TypeField", typeField.toTypeColumnName() });
        }
        
    }
    
    /**
    * Create new TypeFieldObject instance and fill default values
    * 
    * @return     Created object
    */
    public TypeFieldObject create() {
        
        TypeFieldObject typeField = new TypeFieldObject();
        typeField.setService((TypeFieldService) this);
        
        typeField.fillDefaults();
        
        return typeField;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public TypeFieldObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select type_id, order_no, name, description, column_name, value_type, referenced_type_id, value_precision, value_scale, default_value_definition, calculate_expression_definitio, validate_definition, auto_uppercase, required from cw_core_type_fields where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"TypeField", id});
        }
        
        TypeFieldObject result = new TypeFieldObject();
        
        result.setService((TypeFieldService) this);
        result.setId(id);
        result.setTypeId(statement.getBigDecimal(1));
        result.setOrderNo(statement.getBigDecimal(2));
        result.setName(statement.getString(3));
        result.setDescription(statement.getString(4));
        result.setColumnName(statement.getString(5));
        result.setValueType(statement.getString(6));
        result.setReferencedTypeId(statement.getBigDecimal(7));
        result.setValuePrecision(statement.getBigDecimal(8));
        result.setValueScale(statement.getBigDecimal(9));
        result.setDefaultValueDefinition(statement.getString(10));
        result.setCalculateExpressionDefinition(statement.getString(11));
        result.setValidateDefinition(statement.getString(12));
        result.setAutoUppercase(("T".equals(statement.getString(13))));
        result.setRequired(("T".equals(statement.getString(14))));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of TypeFieldObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public TypeFieldList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of TypeFieldObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public TypeFieldList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        TypeFieldList result = new TypeFieldList();
        
        
        Statement statement = executeQuery("select id, type_id, order_no, name, description, column_name, value_type, referenced_type_id, value_precision, value_scale, default_value_definition, calculate_expression_definitio, validate_definition, auto_uppercase, required from cw_core_type_fields"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            TypeFieldObject object = new TypeFieldObject();
        
            object.setService((TypeFieldService) this);
            object.setId(statement.getBigDecimal(1));
            object.setTypeId(statement.getBigDecimal(2));
            object.setOrderNo(statement.getBigDecimal(3));
            object.setName(statement.getString(4));
            object.setDescription(statement.getString(5));
            object.setColumnName(statement.getString(6));
            object.setValueType(statement.getString(7));
            object.setReferencedTypeId(statement.getBigDecimal(8));
            object.setValuePrecision(statement.getBigDecimal(9));
            object.setValueScale(statement.getBigDecimal(10));
            object.setDefaultValueDefinition(statement.getString(11));
            object.setCalculateExpressionDefinition(statement.getString(12));
            object.setValidateDefinition(statement.getString(13));
            object.setAutoUppercase(("T".equals(statement.getString(14))));
            object.setRequired(("T".equals(statement.getString(15))));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'TypeName' fields
    * 
    * @param      typeId Type
    * @param      name   Name
    * @return     Id of found record or null
    */
    public BigDecimal typeNameToId(java.math.BigDecimal typeId, java.lang.String name) {
        return typeNameToId(typeId, name, null);
    }
    
    /**
    * Find id of record by key 'TypeName' fields with id not equal skipId
    * 
    * @param      typeId Type
    * @param      name   Name
    * @param      skipId Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal typeNameToId(java.math.BigDecimal typeId, java.lang.String name, BigDecimal skipId) {
        
        if ( typeId == null || name == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_type_fields where type_id = ? and upper_name = ?", typeId, name.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_type_fields where type_id = ? and upper_name = ? and id != ?", typeId, name.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'TypeName' fields or create new record with values set to given parameters
    * 
    * @param      typeId              Type
    * @param      name                Name
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal typeNameToId(java.math.BigDecimal typeId, java.lang.String name, boolean createNewIfNotFound) {
        
        BigDecimal id = typeNameToId(typeId, name, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        TypeFieldObject object = create();
        object.setTypeId(typeId);
        object.setName(name);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'TypeName' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToTypeName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select type_id, name from cw_core_type_fields where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        TypeFieldObject object = new TypeFieldObject();
        object.setService((TypeFieldService)this);
        object.setId(id);
        object.setTypeId(statement.getBigDecimal(1));
        object.setName(statement.getString(2));
        
        statement.close();
        
        return object.toTypeName();
    }
    
    /**
    * Find id of record by key 'TypeColumnName' fields
    * 
    * @param      typeId     Type
    * @param      columnName ColumnName
    * @return     Id of found record or null
    */
    public BigDecimal typeColumnNameToId(java.math.BigDecimal typeId, java.lang.String columnName) {
        return typeColumnNameToId(typeId, columnName, null);
    }
    
    /**
    * Find id of record by key 'TypeColumnName' fields with id not equal skipId
    * 
    * @param      typeId     Type
    * @param      columnName ColumnName
    * @param      skipId     Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal typeColumnNameToId(java.math.BigDecimal typeId, java.lang.String columnName, BigDecimal skipId) {
        
        if ( typeId == null || columnName == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_type_fields where type_id = ? and upper_column_name = ?", typeId, columnName.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_type_fields where type_id = ? and upper_column_name = ? and id != ?", typeId, columnName.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'TypeColumnName' fields or create new record with values set to given parameters
    * 
    * @param      typeId              Type
    * @param      columnName          ColumnName
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal typeColumnNameToId(java.math.BigDecimal typeId, java.lang.String columnName, boolean createNewIfNotFound) {
        
        BigDecimal id = typeColumnNameToId(typeId, columnName, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        TypeFieldObject object = create();
        object.setTypeId(typeId);
        object.setColumnName(columnName);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'TypeColumnName' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToTypeColumnName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select type_id, column_name from cw_core_type_fields where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        TypeFieldObject object = new TypeFieldObject();
        object.setService((TypeFieldService)this);
        object.setId(id);
        object.setTypeId(statement.getBigDecimal(1));
        object.setColumnName(statement.getString(2));
        
        statement.close();
        
        return object.toTypeColumnName();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToTypeName(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      typeFieldObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(TypeFieldObject typeFieldObject) {
        return typeFieldObject.toTypeName();
    }
    
    protected void saveAudit(TransactionAction transactionAction, TypeFieldObject oldTypeFieldObject, TypeFieldObject newTypeFieldObject) {
        
        TypeFieldObject o = oldTypeFieldObject != null ? oldTypeFieldObject : new TypeFieldObject();
        TypeFieldObject n = newTypeFieldObject != null ? newTypeFieldObject : new TypeFieldObject();
        
        
        executeUpdate("insert into a_cw_core_type_fields (scn, action_type, id , o_type_id, o_order_no, o_name, o_upper_name, o_description, o_column_name, o_upper_column_name, o_value_type, o_referenced_type_id, o_value_precision, o_value_scale, o_default_value_definition, o_calculate_expression_definit, o_validate_definition, o_auto_uppercase, o_required, n_type_id, n_order_no, n_name, n_upper_name, n_description, n_column_name, n_upper_column_name, n_value_type, n_referenced_type_id, n_value_precision, n_value_scale, n_default_value_definition, n_calculate_expression_definit, n_validate_definition, n_auto_uppercase, n_required) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getTypeId(), o.getOrderNo(), o.getName(), o.getName() != null ? o.getName().toUpperCase() : null, o.getDescription(), o.getColumnName(), o.getColumnName() != null ? o.getColumnName().toUpperCase() : null, o.getValueType(), o.getReferencedTypeId(), o.getValuePrecision(), o.getValueScale(), o.getDefaultValueDefinition(), o.getCalculateExpressionDefinition(), o.getValidateDefinition(), o.isAutoUppercase() ? "T" : "F", o.isRequired() ? "T" : "F", n.getTypeId(), n.getOrderNo(), n.getName(), n.getName() != null ? n.getName().toUpperCase() : null, n.getDescription(), n.getColumnName(), n.getColumnName() != null ? n.getColumnName().toUpperCase() : null, n.getValueType(), n.getReferencedTypeId(), n.getValuePrecision(), n.getValueScale(), n.getDefaultValueDefinition(), n.getCalculateExpressionDefinition(), n.getValidateDefinition(), n.isAutoUppercase() ? "T" : "F", n.isRequired() ? "T" : "F");
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      typeField Saved object
    */
    public void save(TypeFieldObject typeField) {
        
        if ( typeField == null ) {
            throwException("Cannot save NULL ?", new Object[]{"TypeFieldObject"});
        }
        
        TransactionAction transactionAction;
        TypeFieldObject oldTypeField;
        if ( typeField.getService() == null ) {
            typeField.setService((TypeFieldService)this);
        }
        
        check(typeField);
        
        if ( typeField.getId() == null ) {
        
            typeField.setId(getObjectService().createObject(getTypeId()));
        
            oldTypeField = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, typeField.getId());
        
            executeUpdate("insert into cw_core_type_fields" 
                          + "( id, type_id, order_no, name, upper_name, description, column_name, upper_column_name, value_type, referenced_type_id, value_precision, value_scale, default_value_definition, calculate_expression_definitio, validate_definition, auto_uppercase, required ) "
                          + "values"
                          + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                          typeField.getId(), typeField.getTypeId(), typeField.getOrderNo(), typeField.getName(), ( typeField.getName() != null ? typeField.getName().toUpperCase() : null ), typeField.getDescription(), typeField.getColumnName(), ( typeField.getColumnName() != null ? typeField.getColumnName().toUpperCase() : null ), typeField.getValueType(), typeField.getReferencedTypeId(), typeField.getValuePrecision(), typeField.getValueScale(), typeField.getDefaultValueDefinition(), typeField.getCalculateExpressionDefinition(), typeField.getValidateDefinition(), typeField.isAutoUppercase() ? "T" : "F", typeField.isRequired() ? "T" : "F" );
        
            if ( typeField.getTypeId() != null ) { getObjectService().setLink(typeField.getTypeId(), typeField.getId()); }
            if ( typeField.getReferencedTypeId() != null ) { getObjectService().setLink(typeField.getReferencedTypeId(), typeField.getId()); }
        
        } else {
        
            oldTypeField = load(typeField.getId());
            if ( oldTypeField.equals(typeField) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, typeField.getId());
        
            executeUpdate("update cw_core_type_fields set "
                          + "type_id = ?, order_no = ?, name = ?, upper_name = ?, description = ?, column_name = ?, upper_column_name = ?, value_type = ?, referenced_type_id = ?, value_precision = ?, value_scale = ?, default_value_definition = ?, calculate_expression_definitio = ?, validate_definition = ?, auto_uppercase = ?, required = ? "
                          + "where id = ?",
                          typeField.getTypeId(), typeField.getOrderNo(), typeField.getName(), ( typeField.getName() != null ? typeField.getName().toUpperCase() : null ), typeField.getDescription(), typeField.getColumnName(), ( typeField.getColumnName() != null ? typeField.getColumnName().toUpperCase() : null ), typeField.getValueType(), typeField.getReferencedTypeId(), typeField.getValuePrecision(), typeField.getValueScale(), typeField.getDefaultValueDefinition(), typeField.getCalculateExpressionDefinition(), typeField.getValidateDefinition(), typeField.isAutoUppercase() ? "T" : "F", typeField.isRequired() ? "T" : "F", typeField.getId() );
        
            getObjectService().changeLinkParent(oldTypeField.getTypeId(), typeField.getTypeId(), typeField.getId());
            getObjectService().changeLinkParent(oldTypeField.getReferencedTypeId(), typeField.getReferencedTypeId(), typeField.getId());
        
        }
        
        saveAudit(transactionAction, oldTypeField, typeField);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        TypeFieldObject oldTypeFieldObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldTypeFieldObject, null);
        
        getObjectService().deleteObject(id);
        
        if (oldTypeFieldObject.getTypeId() != null) { getObjectService().deleteLink(oldTypeFieldObject.getTypeId(), id); }
        if (oldTypeFieldObject.getReferencedTypeId() != null) { getObjectService().deleteLink(oldTypeFieldObject.getReferencedTypeId(), id); }
        
        executeUpdate("delete from cw_core_type_fields where id = ?", id);
        
    }
}
