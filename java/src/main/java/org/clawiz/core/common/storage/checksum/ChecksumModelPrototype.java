package org.clawiz.core.common.storage.checksum;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;
import org.clawiz.core.common.system.service.NotInitializeService;

public class ChecksumModelPrototype extends TypeModel {
    
    private static TypeField OBJECT_ID_FIELD;
    
    private static TypeField CHECKSUM_TYPE_ID_FIELD;
    
    private static TypeField CHECKSUM_FIELD;
    
    private static Type type;
    
    private ChecksumService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public ChecksumService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(ChecksumService.class).getType();
        
        
        OBJECT_ID_FIELD = type.getFields().get("Object");
        if ( OBJECT_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Checksum.Object"}); }
        
        CHECKSUM_TYPE_ID_FIELD = type.getFields().get("ChecksumType");
        if ( CHECKSUM_TYPE_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Checksum.ChecksumType"}); }
        
        CHECKSUM_FIELD = type.getFields().get("Checksum");
        if ( CHECKSUM_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Checksum.Checksum"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _objectId;
    
    public TypeFieldModel objectId() {
        
        if ( _objectId != null ) {
            return _objectId;
        }
        
        _objectId = new TypeFieldModel(this, OBJECT_ID_FIELD);
        return _objectId;
        
    }
    
    private TypeFieldModel _checksumTypeId;
    
    public TypeFieldModel checksumTypeId() {
        
        if ( _checksumTypeId != null ) {
            return _checksumTypeId;
        }
        
        _checksumTypeId = new TypeFieldModel(this, CHECKSUM_TYPE_ID_FIELD);
        return _checksumTypeId;
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.checksumtype.ChecksumTypeModel _checksumTypeTypeModel;
    
    public org.clawiz.core.common.storage.checksumtype.ChecksumTypeModel checksumType() {
        
        if ( _checksumTypeTypeModel == null ) {
            org.clawiz.core.common.storage.checksumtype.ChecksumTypeModel model = getService(org.clawiz.core.common.storage.checksumtype.ChecksumTypeModel.class, this.toString() + "_ChecksumType");
            model.setJoinField(checksumTypeId());
            _checksumTypeTypeModel = model;
        }
        
        return _checksumTypeTypeModel;
    }
    
    private TypeFieldModel _checksum;
    
    public TypeFieldModel checksum() {
        
        if ( _checksum != null ) {
            return _checksum;
        }
        
        _checksum = new TypeFieldModel(this, CHECKSUM_FIELD);
        return _checksum;
        
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "OBJECT" : return objectId();
        case "CHECKSUMTYPE" : return checksumTypeId();
        case "CHECKSUM" : return checksum();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
