/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.typescript.component;

import org.clawiz.core.common.system.generator.abstractgenerator.component.element.AbstractElement;
import org.clawiz.core.common.system.generator.typescript.component.element.AbstractTypeScriptElement;
import org.clawiz.core.common.system.generator.typescript.component.element.TypeScriptFieldElement;
import org.clawiz.core.common.system.generator.typescript.component.element.TypeScriptMethodElement;
import org.clawiz.core.common.system.generator.typescript.component.element.decorator.AbstractTypeScriptDecoratorElement;

import java.util.ArrayList;

public class AbstractTypeScriptClassComponent extends AbstractTypeScriptComponent {

    public enum ComponentType {
        CLASS, INTERFACE
    }

    String extendsName;

    ArrayList<String> implementsList = new ArrayList<>();

    String typeParameters;

    ComponentType componentType = ComponentType.CLASS;

    public ComponentType getComponentType() {
        return componentType;
    }

    public void setComponentType(ComponentType componentType) {
        this.componentType = componentType;
    }

    public String getExtends() {
        return extendsName;
    }

    public void setExtends(String extendsName) {
        this.extendsName = extendsName;
    }

    public void setExtends(AbstractTypeScriptClassComponent component) {
        addImport(component);
        setExtends(component.getName());
    }


    public String getTypeParameters() {
        return typeParameters;
    }

    public void setTypeParameters(String typeParameters) {
        this.typeParameters = typeParameters;
    }

    @Override
    public void setName(String name) {
        super.setName(name);

        setFileName(nameToDottedFileName(getName()) + ".ts");
    }

    @Override
    public <T extends AbstractElement> T addElement(Class<T> clazz) {
        T element = super.addElement(clazz);
        if ( !AbstractTypeScriptDecoratorElement.class.isAssignableFrom(element.getClass()) ) {
            ((AbstractTypeScriptElement) element).setLpad("  ");
        }
        return element;
    }

    @Override
    public <T extends AbstractElement> T addElement(String elementName, Class<T> clazz) {
        T element = super.addElement(elementName, clazz);
        if ( !AbstractTypeScriptDecoratorElement.class.isAssignableFrom(element.getClass()) ) {
            ((AbstractTypeScriptElement) element).setLpad("  ");
        }
        return element;
    }

    public <T extends AbstractTypeScriptDecoratorElement> T addDecorator(Class<T> clazz) {
        return addElement(clazz);
    }

    protected void writeDecorators() {
        for (AbstractTypeScriptDecoratorElement decorator : getElementsByClass(AbstractTypeScriptDecoratorElement.class, false) ) {
            decorator.write();
        }
    }

    public TypeScriptMethodElement addMethod(String name) {
        return addElement(name, TypeScriptMethodElement.class);
    }

    public TypeScriptMethodElement addMethod(String name, String type) {
        TypeScriptMethodElement method = addElement(name, TypeScriptMethodElement.class);
        method.setType(type);
        return method;
    }

    private ArrayList<TypeScriptFieldElement> fields = new ArrayList<>();
    private int maxFieldNameLength = 0;
    public TypeScriptFieldElement addField(String name, String type) {
        return addField(name, type, false);
    }

    public TypeScriptFieldElement addField(String _name, String type, boolean addGetSet) {

        String name = (addGetSet ? "_" : "") + _name;

        TypeScriptFieldElement fe = addElement(name, TypeScriptFieldElement.class);
        fe.setType(type);
        fields.add(fe);

        if ( name.length() > maxFieldNameLength ) {
            maxFieldNameLength = name.length();
        }

        if ( addGetSet ) {
            TypeScriptMethodElement getter = addMethod(_name);
            getter.setMethodModifier(TypeScriptMethodElement.MethodModifier.GET);
            getter.setType(type);
            getter.addText("return this." + name + ";");

            TypeScriptMethodElement setter = addMethod(_name);
            setter.setMethodModifier(TypeScriptMethodElement.MethodModifier.SET);
            setter.addParameter("value", type);
            setter.addText("this." + name + " = value;");

        }

        return fe;
    }

    protected void addImplements(String implementsName) {
        implementsList.add(implementsName);
    }

    public int getMaxFieldNameLength() {
        return maxFieldNameLength;
    }

    @Override
    public void write() {
        writeImports();

        writeDecorators();

        String implementsLine = "";
        for ( int i=0; i < implementsList.size(); i++) {
            implementsLine +=
                    i == 0 ?
                    ( " implements " + implementsList.get(i))
                    :
                    (", " + implementsList.get(i));
        }
        if ( implementsList.size() > 0 ) {
            implementsLine += " ";
        }

        pln("export " + componentType.toString().toLowerCase() + " " + getName()
                + ( getTypeParameters() != null ? "<" + getTypeParameters() + ">" : "")
                + ( getExtends() != null ? " extends " + getExtends() + " " : "" )
                + implementsLine
                + " {");

        for ( AbstractElement element : getElements()) {
            if ( TypeScriptFieldElement.class.isAssignableFrom(element.getClass()) ) {
                pln("");
                element.write();
            }
        }
        for ( AbstractElement element : getElements()) {
            if ( ! AbstractTypeScriptDecoratorElement.class.isAssignableFrom(element.getClass())
                 &&  ! TypeScriptFieldElement.class.isAssignableFrom(element.getClass()) ) {
                pln("");
                element.write();
            }
        }
        pln("");
        pln("}");

    }

    public void setExtendsOfPrototype() {

        String extendsName = getName() + "Prototype";
        setExtends(extendsName);

        addImport(extendsName, "./" + nameToDottedFileName(extendsName));

    }

}
