/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.utils.file;

import org.clawiz.core.common.Core;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.utils.StringUtils;

import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;


public class FileUtils extends Service {

    /**
     * Функция получает расширение файла
     *
     * @param file
     * @return расширение, если расширения у файла нет, то возвращается пустая строка ""
     * @see #getExtension(String)
     */
    public String getExtension( File file ) {
        return getExtension( file.getName() );
    }

    /**
     * Функция возвращает расширение файла.
     * (без точек, только то что стоит после последней точки в имени файла)
     *
     * @param fileName имя файла
     * @return расширение, если расширения у файла нет, то возвращается пустая строка "".
     *         Расширение всегда возвращается в нижнем регистре.
     */
    public String getExtension( String fileName ) {
        String ext = "";

        int i = fileName.lastIndexOf( '.' );

        if( i != -1 ) {
            ext = fileName.substring( i + 1 ).toLowerCase();
        }

        return ext.trim();
    }

    /**
     * Функция формирует новый файл с указаным расширением
     *
     * @param file      Имя исходного файла
     * @param extension новое расширение для файла (без точек)
     * @return
     */
    public File setExtension( File file, String extension ) {

        String ext = getExtension(file);

        String name;

        if( ext.equalsIgnoreCase( ext ) ) {
            return file;
        } else if( "".equals( ext ) ) {
            name = file.getAbsolutePath() + "." + extension;
        } else {
            String f = file.getAbsolutePath();

            int i = f.lastIndexOf( '.' );

            name = f.substring( 0, i - 1 ) + "." + extension;
        }

        return new File( name );
    }

    /**
     * Получить имя файла
     * @param fileName
     * @return
     */
    public String getName( String fileName ) {
        File file = new File( fileName );

        return file.getName();
    }

    /**
     * Получить список всех файлов директории рекурсивно
     * @param directory Директория в которой мы ищем
     * @param ext расширение файлов (пустая строка если интересуют файлы без расширения),
     *          если ext равно null, то вернуться все файлы
     *
     * @return
     */
    public File[] getDirectoryFiles( File directory, String ext ) {
        LinkedList list = new LinkedList();

        listFiles( directory, list, ext, false );

        return (File[])list.toArray( new File[0] );
    }

    public File[] getDirectoryFiles( String directory, String ext ) {
        return getDirectoryFiles(new File(directory), ext);
    }

    public File[] getDirectoryFiles( File directory) {
        return getDirectoryFiles(directory, null);
    }
    /**
     * Получить список всех файлов и вложенных директорий рекурсивно
     * @param directory Директория в которой мы ищем
     * @param ext расширение файлов (пустая строка если интересуют файлы без расширения),
     *          если ext равно null, то вернуться все файлы
     * @return
     */
    public File[] getDirectoryFilesAndDirs( File directory, String ext ) {
        LinkedList list = new LinkedList();

        listFiles( directory, list, ext, true );

        return (File[])list.toArray( new File[0] );
    }

    /**
     * Получить список всех файлов и вложенных директорий рекурсивно
     * @param directory Директория в которой мы ищем
     * @return список всех файлов директории в порядке:
     *      1. Файлы диретории
     *      2. директория
     */
    public File[] getDirectoryFilesAndDirs( File directory ) {
        LinkedList list = new LinkedList();

        listFiles( directory, list );

        return (File[])list.toArray( new File[0] );
    }

    private void listFiles( File directory, LinkedList list ) {
        File[] sf = directory.listFiles();

        if( sf == null ) {
            return;
        }

        for( int i = 0; i < sf.length; i++ ) {
            if( sf[i].isDirectory() ) {
                listFiles( sf[i], list );
            }
            list.add( sf[i] );
        }
    }

    private void listFiles( File directory, LinkedList list, String ext, boolean includeDir ) {
        File[] sf = directory.listFiles();

        if( sf == null ) {
            return;
        }

        for( int i = 0; i < sf.length; i++ ) {
            String name = sf[i].getName();

            if( sf[i].isDirectory() ) {
                if( includeDir && checkExtension( name, ext ) ) {
                    list.add( sf[i] );
                } else {
                    listFiles( sf[i], list, ext, includeDir );
                }
            } else {
                if( checkExtension( name, ext ) ) {
                    list.add( sf[i] );
                }
            }
        }

    }

    private boolean checkExtension( String name, String ext ) {
        if( ext == null ) {
            return true;
        }

        String extension = getExtension( name );

        return StringUtils.isLike( extension, ext );
    }

    public void createDirectory(String path) {
        File dir = new File(path);
        if ( ! dir.exists() ) {
            dir.mkdirs();
        }
    }

    public void copyFile(String inputFileName, String outputFileName) {

        File file = new File(outputFileName);
        createDirectory(file.getParent());

        InputStream  inputStream = getInputStream(inputFileName);
        OutputStream outputStream = getOutputStream(outputFileName);

        BufferedInputStream in = new BufferedInputStream(inputStream);
        BufferedOutputStream out = new BufferedOutputStream(outputStream);

        byte[] buffer = new byte[16*1024];
        int len;

        try {
            while((len=in.read(buffer)) > 0) out.write(buffer, 0, len);

            in.close();
            out.close();
        } catch (IOException e) {
            try {
                in.close();
                out.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            throwException("Exception on copy file '?' to '?' : ?", new Object[]{inputFileName, outputFileName, e.getMessage()}, e);
        }

    }
    public void copyFile(InputStream inputStream, FileOutputStream outputStream) throws IOException {

        BufferedInputStream in = new BufferedInputStream(inputStream);
        BufferedOutputStream out = new BufferedOutputStream(outputStream);

        byte[] buffer = new byte[16*1024];
        int len;

        while((len=in.read(buffer)) > 0) out.write(buffer, 0, len);

        in.close();
        out.close();
    }

    private byte[] readToArray(InputStream inputStream) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            baos.write(buffer, 0, length);
        }
        return baos.toByteArray();
    }

    public void runCommand(String command, InputStream inputStream, OutputStream outputStream) {
        runCommand(command, 60, inputStream,  outputStream);
    }


    class UnArchiveControlThread extends Thread {
        private Process process;
        private long time = 600; // 10 минут
        private Timer timer;


        public void setTime(long time) {
            this.time = time;
        }

        public void setProcess(Process process) {
            this.process = process;
        }

        public Timer getTimer() {
            return timer;
        }

        public UnArchiveControlThread() {
            timer = new Timer();
            timer.schedule(new TimerTask() {
                public void run() {
//                    System.out.println("FileUtils.ControlThread time = " + time);
                    time--;
                    if ( time < 0 ) {
                        timer.cancel();
                        process.destroy();
                    }
                }
            }, 1000, 1000);
        }

        public void run() {
            while (time > 0) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                }
            }
//            System.out.println("process destroyed by control thread in FileUtils.class");
            timer.cancel();
            process.destroy();
        }
    }

    public void runCommand(String command, long maxTime, InputStream inputStream, OutputStream outputStream) {

        UnArchiveControlThread thread = new UnArchiveControlThread();

//        System.out.println("FileUtils.start command : " + command);

        //Если убрать вывод в лог то вот такая команда зависает :
        // unzip -o C:\cs\dev\rer-enmart\work\inbox\dex\110815\10177551\20110117_TUMENEGK_cfrfrm.zip -d C:\cs\dev\rer-enmart\work\inbox\dex\110815\10177551\20110117_TUMENEGK_cfrfrm.zip.dir
        try {
            Process process = Runtime.getRuntime().exec(command);// + " >> /temp/command.log"); //; "rundll32 url.dll,FileProtocolHandler " + outFile
            if (inputStream != null) {
                copy(inputStream, process.getOutputStream(), false);
                process.getOutputStream().write("\r\n".getBytes());
                process.getOutputStream().flush();
            }

            thread.setProcess(process);
            thread.setTime(maxTime);

            thread.start();

            Date dateBefore = new Date();
            process.waitFor();
            Date dateAfter = new Date();

            thread.getTimer().cancel();

            if (dateAfter.getTime() - dateBefore.getTime() > maxTime*1000) {
                throw new CoreException("Команда ? превысила допустимое время исполнения", new Object[]{command});
            }

            if (outputStream != null) {
                copy(process.getInputStream(), outputStream, false);
            }

            process.destroy();
            thread.setProcess(null);
            thread = null;
            process = null;

        } catch (Exception e) {
            e.printStackTrace();
            throw new CoreException("Error выполения команды ?", new Object[]{command}, e);
        }
    }

    private void copy(InputStream in, OutputStream out, boolean autoClose) {
         byte[] buffer = new byte[16*1024];
         int len;

        try {
            while((len = in.read(buffer)) > 0)
            {
                out.write(buffer, 0, len);
            }

            if (autoClose) {
                in.close();
                out.close();
            }
        }
        catch (Exception ex) {
            throw new CoreException("Не удалось скопировать данные потоков", ex);
        }
    }

    public File[] unzip(File file) {
        String targetDir = file.getAbsolutePath()+".dir";
        createDirectory(targetDir);

        runCommand(getUnzipCommand(file.getAbsolutePath(), targetDir), 1*30, null, null);

        return getDirectoryFilesAndDirs(new File(targetDir));

    }
    
    public String getUnrarCommand(String filePath, String targetDir)  {
        return getUnarchiveCommand("fileutils.unrar", "winrar x -y $FILE $TARGETDIR", filePath, targetDir);
    }

    public String getUnzipCommand(String filePath, String targetDir)  {
        return getUnarchiveCommand("fileutils.unzip", "unzip -o $FILE -d $TARGETDIR", filePath, targetDir);
    }

    private String getUnarchiveCommand(String configKey, String defaultValue, String filePath, String targetDir) {
        final String command = Core.getConfig().getString(configKey, defaultValue);
        return command.replace("$FILE", filePath).replace("$TARGETDIR", targetDir);
    }

    public File[] unrar(File file) {
        String targetDir = file.getAbsolutePath()+".dir";
        createDirectory(targetDir);

        runCommand(getUnrarCommand(file.getAbsolutePath(), targetDir) , 1*30, null, null);

        return getDirectoryFilesAndDirs(new File(targetDir));

    }

    public OutputStream getOutputStream(String fileName) {
        try {
            OutputStream stream = new FileOutputStream(fileName);
            return stream;
        } catch (FileNotFoundException e) {
            throwException("FileNotFoundException at get output stream to file '?'",new Object[]{fileName});
        }
        return null;
    }

    public InputStream getInputStream(String fileName, boolean useClassLoader) {

        InputStream inputStream;

        try {
            inputStream = new FileInputStream( fileName);
        } catch (FileNotFoundException e) {

            if ( ! useClassLoader ) {
                throwException("File '?' not found", new Object[]{fileName}, e);
            }

            String jarFileName = fileName.replaceAll("\\\\", "/");
            inputStream = getClass().getResourceAsStream(jarFileName);
            if ( inputStream == null ) {
                throwException("File '?' not found in classpath", jarFileName, e);
            }

        }

        return inputStream;
    }

    public String readToString(String fileName, boolean useClassLoader) {

        BufferedReader buff  = new BufferedReader( new InputStreamReader( getInputStream(fileName, useClassLoader) ) );
        StringBuilder     contents = new StringBuilder();

        try {
            try {
                String line = null;
                while (( line = buff.readLine()) != null){
                    contents.append(line);
                    contents.append(System.getProperty("line.separator"));
                }
            }
            catch ( IOException e) {
                throwException("File '?' read error '?'", new Object[]{fileName, e.getMessage(), e});
            }
            finally {
                buff.close();
            }
        } catch (IOException e) {
            throwException("File '?' read error '?'", new Object[]{fileName, e.getMessage(), e});
        }

        return contents.toString();

    }

    public InputStream getInputStream(String fileName) {
        return getInputStream(fileName, true);
    }

    public String readToString(String fileName) {
        return readToString(fileName, true);
    }

    public String readToString(File file) {
        return readToString(file.getPath());
    }

    public byte[] getBytesFromInputStream(InputStream is) {
        try (ByteArrayOutputStream os = new ByteArrayOutputStream();)
        {
            byte[] buffer = new byte[0xFFFF];

            for (int len; (len = is.read(buffer)) != -1;)
                os.write(buffer, 0, len);

            os.flush();

            return os.toByteArray();
        }
        catch (IOException e)
        {
            throw new CoreException("Exception on read from input stream ?", new Object[]{e.getMessage()}, e);
        }
    }


    private String getFileHash(String fileName, String algorithm) {

        try {
            InputStream inputStream = getInputStream(fileName);

            MessageDigest digest = MessageDigest.getInstance(algorithm);

            byte[] bytesBuffer = new byte[1024];
            int bytesRead = -1;

            while ((bytesRead = inputStream.read(bytesBuffer)) != -1) {
                digest.update(bytesBuffer, 0, bytesRead);
            }

            byte[] hashedBytes = digest.digest();

            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < hashedBytes.length; i++) {
                stringBuffer.append(Integer.toString((hashedBytes[i] & 0xff) + 0x100, 16)
                        .substring(1));
            }
            return stringBuffer.toString();
        } catch (Exception e) {
            throwException("Exception on get file '?' ? hash : ?", new Object[]{fileName, algorithm, e.getMessage()}, e);
        }
        return null;
    }

    public String getMd5FileHash(String fileName) {
        return getFileHash(fileName, "MD5");
    }

    public String getSHA1FileHash(String fileName) {
        return getFileHash(fileName, "SHA-1");
    }

    public String getSHA256FileHash(String fileName) {
        return getFileHash(fileName, "SHA-256");
    }

    public boolean isFilesEquals(String file1Name, String file2Name) {
        InputStream stream1 = getInputStream(file1Name);
        InputStream stream2 = getInputStream(file2Name);
        byte[] buf1 = new byte[64 *1024];
        byte[] buf2 = new byte[64 *1024];
        try {
            DataInputStream d2 = new DataInputStream(stream2);
            int len;
            while ((len = stream1.read(buf1)) > 0) {
                d2.readFully(buf2, 0, len);
                for (int i = 0; i < len; i++)
                    if (buf1[i] != buf2[i]) {
                        stream1.close();
                        stream2.close();
                        return false;
                    }
            }
            boolean result = d2.read() < 0;
            stream1.close();
            stream2.close();
            return result;
        } catch (Exception e ) {
            try {
                stream1.close();
                stream2.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            throwException("Exception on compare files '?' and '?' : ?", new Object[]{file1Name,file2Name, e.getMessage()}, e);
        }
        return false;
    }

    private String[] _getResourcesListAtPath(Class clazz, String _path) {
        String path = _path;
        if ( File.separator.equals("\\") ) {
            path = path.replaceAll("/", "\\\\");
        }
        path = path.replaceAll("\\\\", "/");
        if ( path.charAt(0) != '/' ) {
            path = "/" + path;
        }

        try {
        System.out.println("FileUtils.getResourceList for " + path);

            URL dirURL = clazz.getClassLoader().getResource(path);
            if ( dirURL == null ) {
                dirURL = clazz.getClassLoader().getResource(path.substring(1));
            }

            if (dirURL != null && dirURL.getProtocol().equals("file")) {

                return new File(dirURL.toURI()).list();

            }


            if ( dirURL == null) {

                String me = clazz.getName().replace(".", "/")+".class";
                dirURL = clazz.getClassLoader().getResource(me);
            }

            if (dirURL.getProtocol().equals("jar")) {

                path = path.substring(1);
                String jarPath = dirURL.getPath().substring(5, dirURL.getPath().indexOf("!")); //strip out only the JAR file
                JarFile jar = new JarFile(URLDecoder.decode(jarPath, "UTF-8"));
                Enumeration<JarEntry> entries = jar.entries(); //gives ALL entries in jar
                Set<String> result = new HashSet<String>(); //avoid duplicates in case it is a subdirectory
                while(entries.hasMoreElements()) {
                    String name = entries.nextElement().getName();
                    //                System.out.println(" jar name = " + name);
                    if (name.startsWith(path + "/")) { //filter according to the path
                        //                    System.out.println(" found");
                        String entry = name.substring(path.length());
                        int index = entry.indexOf("/");
                        if (index >= 0) {
                            // if it is a subdirectory, we just return the directory name
                            entry = entry.substring(index+1);
                        }
                        if ( entry.length() > 0 ) {
                            result.add(entry);
                        }
                    }
                }

                return result.toArray(new String[result.size()]);
            }

            throwException("Cannot list files for URL ? protocol ? ", dirURL, dirURL.getProtocol());
        } catch (Exception e) {
            throwException("Exception on read resources list at path '?' for class '?' : ?", new Object[]{path, clazz, e.getMessage()}, e);

        }

        return null;
    }

    private void _getRecursiveResourcesList(Class clazz, String path, String subPath, ArrayList<String> result) {
        String[] names = _getResourcesListAtPath(clazz, path);
        for (String name : names) {
            String newPath =  path + (StringUtils.isEmpty(path) ? "" : "/") + name;
            try {
                // is directory
                String[] subdir = _getResourcesListAtPath(clazz, newPath);
                if ( subdir.length == 0 ) {
                    if ( name.substring(name.length()-1).equalsIgnoreCase("/")) {
//                        System.out.println("path " + newPath + " skipped as dirnname");
                    } else {
//                        System.out.println("path " + newPath + " added as file '" + subPath + name + "'");
                        result.add(subPath + name);
                    }
                } else {
//                    System.out.println("path " + newPath + " is dir");
                    _getRecursiveResourcesList(clazz, newPath, subPath + name + "/", result);
                }

            } catch (Exception e) {
                result.add(subPath + name);
            }
        }
    }

    public String[] getResourcesList(Class clazz, String _path) {
        ArrayList<String> list = new ArrayList<>();
        String path = _path;
        _getRecursiveResourcesList(clazz, path, "", list);

        String[] result = new String[list.size()];
        for( int i=0; i < list.size(); i++) {
            result[i] = list.get(i);
        }
        return result;
    }

    public String[] getResourcesList(String path) {
        return getResourcesList(getClass(), path);
    }

    public void createFile(FileDefinition fileDefinition) {
        createDirectory(fileDefinition.getPath());
        File file = Paths.get(fileDefinition.getPath(), fileDefinition.getName()).toFile();
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(fileDefinition.getBytes());
        }
        catch (IOException e) {
            throw new CoreException("Exception on create file ? : ?", fileDefinition.getName(), e.getMessage(), e);
        }
    }

    public static String nameToDottedFileName(String name) {
        StringBuilder fn    = new StringBuilder();
        String        _name = name;

        for ( int i=0; i < _name.length(); i++) {
            char c = _name.charAt(i);
            if ( i > 0 && Character.isUpperCase(c) ) {
                fn.append('.');
            }
            fn.append(Character.toLowerCase(c));
        }
        return fn.toString();
    }

    public boolean fileExists(String fileName, boolean useClassLoader) {
        File f = new File(fileName);
        if ( f.exists() ) {
            return true;
        }

        if ( ! useClassLoader ) {
            return false;
        }

        URL url = getClass().getResource(fileName);
        return  url != null;
    }

}
