package org.clawiz.core.common.storage.roleinheritance;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class RoleInheritanceObjectPrototype extends AbstractObject {
    
    /**
    * Role
    */
    private java.math.BigDecimal roleId;
    
    private org.clawiz.core.common.storage.role.RoleObject role;
    
    /**
    * InheritedRole
    */
    private java.math.BigDecimal inheritedRoleId;
    
    private org.clawiz.core.common.storage.role.RoleObject inheritedRole;
    
    public RoleInheritanceService service;
    
    /**
    * 
    * @return     Role
    */
    public java.math.BigDecimal getRoleId() {
        return this.roleId;
    }
    
    public org.clawiz.core.common.storage.role.RoleObject getRole() {
        return getRole(false);
    }
    
    @SuppressWarnings("Duplicates")
    public org.clawiz.core.common.storage.role.RoleObject getRole(boolean autoCreate) {
        
        if ( role != null ) { return role; }
        
        if ( roleId != null ) {
            role = service.loadRole(roleId);
        } else if ( autoCreate ) {
            role = service.createRole();
        }
        
        return role;
    }
    
    /**
    * Set 'Role' value
    * 
    * @param      roleId Role
    */
    public void setRoleId(java.math.BigDecimal roleId) {
         this.roleId = roleId;
    }
    
    /**
    * Set 'Role' value and return this object
    * 
    * @param      roleId Role
    * @return     RoleInheritance object
    */
    public RoleInheritanceObjectPrototype withRoleId(java.math.BigDecimal roleId) {
        setRoleId(roleId);
        return this;
    }
    
    /**
    * 
    * @return     InheritedRole
    */
    public java.math.BigDecimal getInheritedRoleId() {
        return this.inheritedRoleId;
    }
    
    public org.clawiz.core.common.storage.role.RoleObject getInheritedRole() {
        return getInheritedRole(false);
    }
    
    @SuppressWarnings("Duplicates")
    public org.clawiz.core.common.storage.role.RoleObject getInheritedRole(boolean autoCreate) {
        
        if ( inheritedRole != null ) { return inheritedRole; }
        
        if ( inheritedRoleId != null ) {
            inheritedRole = service.loadRole(inheritedRoleId);
        } else if ( autoCreate ) {
            inheritedRole = service.createRole();
        }
        
        return inheritedRole;
    }
    
    /**
    * Set 'InheritedRole' value
    * 
    * @param      inheritedRoleId InheritedRole
    */
    public void setInheritedRoleId(java.math.BigDecimal inheritedRoleId) {
         this.inheritedRoleId = inheritedRoleId;
    }
    
    /**
    * Set 'InheritedRole' value and return this object
    * 
    * @param      inheritedRoleId InheritedRole
    * @return     RoleInheritance object
    */
    public RoleInheritanceObjectPrototype withInheritedRoleId(java.math.BigDecimal inheritedRoleId) {
        setInheritedRoleId(inheritedRoleId);
        return this;
    }
    
    public RoleInheritanceService getService() {
        return this.service;
    }
    
    public void setService(RoleInheritanceService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(RoleInheritanceObjectPrototype  target) {
        target.setRoleId(getRoleId());
        target.setInheritedRoleId(getInheritedRoleId());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((RoleInheritanceObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        if ( role != null ) {
            role.save();
            roleId = role.getId();
        }
        if ( inheritedRole != null ) {
            inheritedRole.save();
            inheritedRoleId = inheritedRole.getId();
        }
        service.save((RoleInheritanceObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(RoleInheritanceObjectPrototype object) {
        return 
               isObjectsEquals(this.getRoleId(), object.getRoleId() ) 
            && isObjectsEquals(this.getInheritedRoleId(), object.getInheritedRoleId() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getRoleId() != null ? getRoleId().hashCode() : 0);
        result = result * 31 + (getInheritedRoleId() != null ? getInheritedRoleId().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'RoleInherited' fields
    * 
    * @return     Concatenated string values of key 'toRoleInherited' fields
    */
    public String toRoleInherited() {
        return service.getObjectService().idToString(getInheritedRoleId()) + "," + service.getObjectService().idToString(getRoleId());
    }
}
