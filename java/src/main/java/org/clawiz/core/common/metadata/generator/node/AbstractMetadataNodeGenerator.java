package org.clawiz.core.common.metadata.generator.node;

import org.clawiz.core.common.system.generator.java.AbstractJavaClassGenerator;

public class AbstractMetadataNodeGenerator extends AbstractJavaClassGenerator {

    AbstractMetadataNodeGeneratorContext context;

    public AbstractMetadataNodeGeneratorContext getContext() {
        return context;
    }

    public void setContext(AbstractMetadataNodeGeneratorContext context) {
        this.context = context;
        this.setPackageName(context.getLanguageNode().getPackageName());
    }

    @Override
    protected void process() {
        super.process();
    }
}
