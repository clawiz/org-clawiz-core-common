/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.utils.xml;

import com.sun.org.apache.xerces.internal.dom.DeferredTextImpl;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.utils.StringUtils;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.w3c.dom.*;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;

public class XMLUtils extends Service {

    /**
     * Записать документ в файл с указанием кодировки UTF-8
     * @param document
     * @param out
     */
    public void write( Document document, OutputStream out ) {
        defaultFormat(document);
        write( document, out, "UTF-8" );
    }

    /**
     * Записать документ в файл с указанием кодировки
     * @param document
     * @param out
     * @param encoding
     */
    public void write( Document document, OutputStream out, String encoding ) {
        Source source = new DOMSource( document );

        try {
            Result result = new StreamResult( new PrintWriter( new OutputStreamWriter( out, encoding ) ) );

            Transformer xformer = TransformerFactory.newInstance().newTransformer();

            xformer.setOutputProperty( OutputKeys.ENCODING, encoding );

            xformer.transform( source, result );
        } catch (UnsupportedEncodingException e) {
            throwException("UnsupportedEncodingException on XML document write '?'", e.getMessage(), e);
        } catch (TransformerException e) {
            throwException("TransformerException on XML document write '?'", e.getMessage(), e);
        }
    }



    /**
     * добавляет в документ дополнительные переносы строк и пробелы для большей наглядности документа
     */
    public void defaultFormat( Document document ) {
        defaultFormat( "", document.getDocumentElement(), document );
    }

    /**
     * добавляет в документ дополнительные переносы строк и пробелы для большей наглядности документа
     */
    private void defaultFormat( String rootIndent, Node node, Document document ) {
        if( node == null || node.getNodeType() != Node.ELEMENT_NODE ) {
            return;
        }

        String indent = rootIndent + "    ";

        NodeList list = node.getChildNodes();
        Node[] nodes = new Node[ list.getLength() ];
        for( int i = 0; i < nodes.length; i++ ) {
            nodes[i] = list.item( i );
        }

        boolean wasElements = false;
        for( int i = 0; i < nodes.length; i++ ) {
            Node child = nodes[i];
            if( child.getNodeType() == Node.ELEMENT_NODE ) {
                wasElements = true;
                node.insertBefore( document.createTextNode( indent ), child );
                if( i == ( nodes.length - 1 ) ) {
                    node.appendChild( document.createTextNode( "\n" ) );
                } else {
                    node.insertBefore( document.createTextNode( "\n" ), nodes[ i + 1 ] );
                }
                defaultFormat( indent, child, document );
                node.appendChild( document.createTextNode( rootIndent ) );
            }
        }

        if( wasElements && node.getFirstChild() != null ) {
            node.insertBefore( document.createTextNode( "\n" ), node.getFirstChild() );
        }
    }

    /**
     * Создание нового документа
     */
    public Document newDocument() throws ParserConfigurationException {
        // Получаем фабрику для создания DOM модели
        DocumentBuilder builder;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        builder = factory.newDocumentBuilder();

        // Создаем новый документ
        return builder.newDocument();
    }

    /**
     * Добавляем элемент c секцией простым текстом
     */
    public Element addElementWithText( Document document, Element node, String tagName, String textData ) {
        Element element = addElement( document, node, tagName );

        element.appendChild( document.createTextNode( textData ) );

        return element;
    }

    /**
     * Добавляем элемент c секцией CDATA
     */
    public  Element addElementWithCDATA( Document document, Element node, String tagName, String CDATA ) {
        Element element = addElement( document, node, tagName );

        element.appendChild( document.createCDATASection( CDATA ) );

        return element;
    }

    /**
     * Добавляем элемент
     */
    public Element addElement( Document document, Element node, String tagName ) {
        Element element = document.createElement( tagName );

        node.appendChild( element );

        return element;
    }

    /**
     * Так добавляются атрибуты (Документ везде тягаем за собой)
     */
    public void addAttribute( Document document, Element element, String name, String value ) {
        if( value != null ) {
            Attr attribute = document.createAttribute( name );

            attribute.setValue( value );

            element.setAttributeNode( attribute );
        }
    }

    public <T extends Object> T parseToObject(String xml, Class<T> clazz) {
        if ( xml == null ) {
            return null;
        }
        Serializer serializer = new Persister();
        Object obj= null;
        try {
            obj = serializer.read(clazz, xml);
        } catch (Exception e) {
            throwException("XML parse exception : ?", e.getMessage(), e);
        }
        return (T)obj;
    }

    public Document parseToDocument(String xml) {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder  builder = factory.newDocumentBuilder();
            InputSource      is      = new InputSource(new StringReader(xml));
            Document         doc     = builder.parse(is);
            return doc;
        } catch (Exception e) {
            throwException("XML parse exception : ?", e.getMessage(), e);
        }
        return null;
    }

    public String objectToXML(Object object) {
        if ( object == null ) {
            return null;
        }
        Serializer serializer = new Persister();
        StringWriter writer = new StringWriter();
        try {
            serializer.write(object, writer);
        } catch (Exception e) {
            throwException("To xml serialization exception : ?", e.getMessage(), e);
        }
        return writer.toString();
    }

    public String getNodeText(Node node) {

        NodeList list = node.getChildNodes();

        for (int i = 0; i < list.getLength(); i++) {
            Node childIdde = list.item(i); // текущий нод
            if ( childIdde instanceof DeferredTextImpl) {
                return StringUtils.trim(((DeferredTextImpl) childIdde).getData());
            }
        }

        return null;
    }

    public String documentToString(Document document) {

        TransformerFactory tf   = TransformerFactory.newInstance();
        Transformer transformer = null;
        try {
            transformer = tf.newTransformer();
        } catch (TransformerConfigurationException e) {
            throwException("Exception on create XML transformer : ?", e.getMessage(), e);
        }
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        StringWriter writer     = new StringWriter();
        try {
            transformer.transform( new DOMSource( document ), new StreamResult( writer ) );
        } catch (TransformerException e) {
            throwException("Exception on transform XML document to string : ?", e.getMessage(), e);
        }

        return writer.getBuffer().toString();


    }

}
