package org.clawiz.core.common.metadata.data.common.valuetype;

import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ValueTypeEnumerationPrototype extends org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType {
    
    @ExchangeElement
    private org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumerationValueList values = new org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumerationValueList();
    
    public ValueTypeEnumeration withName(String value) {
        setName(value);
        return (ValueTypeEnumeration) this;
    }
    
    public org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumerationValueList getValues() {
        return this.values;
    }
    
    public ValueTypeEnumeration withValue(org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumerationValue value) {
        getValues().add(value);
        return (ValueTypeEnumeration) this;
    }
    
    public <T extends org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumerationValue> T createValue(Class<T> nodeClass) {
        org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumerationValue value = createChildNode(nodeClass, "values");
        getValues().add(value);
        return (T) value;
    }
    
    public org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumerationValue createValue() {
        return createValue(org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumerationValue.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        for (MetadataNode node : getValues()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        for (MetadataNode node : getValues()) {
            references.add(node);
        }
        
    }
}
