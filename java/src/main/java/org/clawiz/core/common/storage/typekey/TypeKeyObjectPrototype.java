package org.clawiz.core.common.storage.typekey;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class TypeKeyObjectPrototype extends AbstractObject {
    
    /**
    * Type
    */
    private java.math.BigDecimal typeId;
    
    /**
    * Name
    */
    private java.lang.String name;
    
    /**
    * Description
    */
    private java.lang.String description;
    
    /**
    * Unique
    */
    private java.lang.Boolean unique;
    
    /**
    * UseAsToString
    */
    private java.lang.Boolean useAsToString;
    
    public TypeKeyService service;
    
    /**
    * 
    * @return     Type
    */
    public java.math.BigDecimal getTypeId() {
        return this.typeId;
    }
    
    /**
    * Set 'Type' value
    * 
    * @param      typeId Type
    */
    public void setTypeId(java.math.BigDecimal typeId) {
         this.typeId = typeId;
    }
    
    /**
    * Set 'Type' value and return this object
    * 
    * @param      typeId Type
    * @return     TypeKey object
    */
    public TypeKeyObjectPrototype withTypeId(java.math.BigDecimal typeId) {
        setTypeId(typeId);
        return this;
    }
    
    /**
    * 
    * @return     Name
    */
    public java.lang.String getName() {
        return this.name;
    }
    
    /**
    * Set 'Name' value
    * 
    * @param      name Name
    */
    public void setName(java.lang.String name) {
        
        if ( name != null && name.length() > 75) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "TypeKey.Name", "75");
        }
        
         this.name = name;
    }
    
    /**
    * Set 'Name' value and return this object
    * 
    * @param      name Name
    * @return     TypeKey object
    */
    public TypeKeyObjectPrototype withName(java.lang.String name) {
        setName(name);
        return this;
    }
    
    /**
    * 
    * @return     Description
    */
    public java.lang.String getDescription() {
        return this.description;
    }
    
    /**
    * Set 'Description' value
    * 
    * @param      description Description
    */
    public void setDescription(java.lang.String description) {
        
        if ( description != null && description.length() > 255) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "TypeKey.Description", "255");
        }
        
         this.description = description;
    }
    
    /**
    * Set 'Description' value and return this object
    * 
    * @param      description Description
    * @return     TypeKey object
    */
    public TypeKeyObjectPrototype withDescription(java.lang.String description) {
        setDescription(description);
        return this;
    }
    
    /**
    * 
    * @return     Unique
    */
    public java.lang.Boolean isUnique() {
        return this.unique != null ? this.unique : false;
    }
    
    /**
    * Set 'Unique' value
    * 
    * @param      unique Unique
    */
    public void setUnique(java.lang.Boolean unique) {
         this.unique = unique;
    }
    
    /**
    * Set 'Unique' value
    * 
    * @param      unique Unique
    */
    public void setUnique(boolean unique) {
        this.unique= new Boolean(unique);
    }
    
    /**
    * Set 'Unique' value and return this object
    * 
    * @param      unique Unique
    * @return     TypeKey object
    */
    public TypeKeyObjectPrototype withUnique(java.lang.Boolean unique) {
        setUnique(unique);
        return this;
    }
    
    /**
    * Set 'Unique' value
    * 
    * @param      unique Unique
    * @return     
    */
    public TypeKeyObjectPrototype withUnique(boolean unique) {
        setUnique(unique);
        return this;
    }
    
    /**
    * 
    * @return     UseAsToString
    */
    public java.lang.Boolean isUseAsToString() {
        return this.useAsToString != null ? this.useAsToString : false;
    }
    
    /**
    * Set 'UseAsToString' value
    * 
    * @param      useAsToString UseAsToString
    */
    public void setUseAsToString(java.lang.Boolean useAsToString) {
         this.useAsToString = useAsToString;
    }
    
    /**
    * Set 'UseAsToString' value
    * 
    * @param      useAsToString UseAsToString
    */
    public void setUseAsToString(boolean useAsToString) {
        this.useAsToString= new Boolean(useAsToString);
    }
    
    /**
    * Set 'UseAsToString' value and return this object
    * 
    * @param      useAsToString UseAsToString
    * @return     TypeKey object
    */
    public TypeKeyObjectPrototype withUseAsToString(java.lang.Boolean useAsToString) {
        setUseAsToString(useAsToString);
        return this;
    }
    
    /**
    * Set 'UseAsToString' value
    * 
    * @param      useAsToString UseAsToString
    * @return     
    */
    public TypeKeyObjectPrototype withUseAsToString(boolean useAsToString) {
        setUseAsToString(useAsToString);
        return this;
    }
    
    public TypeKeyService getService() {
        return this.service;
    }
    
    public void setService(TypeKeyService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(TypeKeyObjectPrototype  target) {
        target.setTypeId(getTypeId());
        target.setName(getName());
        target.setDescription(getDescription());
        target.setUnique(isUnique());
        target.setUseAsToString(isUseAsToString());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((TypeKeyObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        service.save((TypeKeyObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(TypeKeyObjectPrototype object) {
        return 
               isObjectsEquals(this.getTypeId(), object.getTypeId() ) 
            && isObjectsEquals(this.getName(), object.getName() ) 
            && isObjectsEquals(this.getDescription(), object.getDescription() ) 
            && isObjectsEquals(this.isUnique(), object.isUnique() ) 
            && isObjectsEquals(this.isUseAsToString(), object.isUseAsToString() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getTypeId() != null ? getTypeId().hashCode() : 0);
        result = result * 31 + (getName() != null ? getName().hashCode() : 0);
        result = result * 31 + (getDescription() != null ? getDescription().hashCode() : 0);
        result = result * 31 + (isUnique() != null ? isUnique().hashCode() : 0);
        result = result * 31 + (isUseAsToString() != null ? isUseAsToString().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'TypeName' fields
    * 
    * @return     Concatenated string values of key 'toTypeName' fields
    */
    public String toTypeName() {
        return service.getObjectService().idToString(getTypeId()) + "," + getName();
    }
}
