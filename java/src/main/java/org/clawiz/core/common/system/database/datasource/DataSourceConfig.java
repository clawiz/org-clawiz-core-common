/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.database.datasource;

import org.clawiz.core.common.Core;
import org.clawiz.core.common.system.config.Config;
import org.clawiz.core.common.system.database.datasource.hikaricp.HikariDataSource;
import org.clawiz.core.common.utils.StringUtils;

public class DataSourceConfig {

    public static String DATASOURCE_CONFIG_PARAMETER_CLASS                = "class";
    public static String DATASOURCE_CONFIG_PARAMETER_CONNECTION_URL       = "connection.url";
    public static String DATASOURCE_CONFIG_PARAMETER_CONNECTION_USER      = "connection.user";
    public static String DATASOURCE_CONFIG_PARAMETER_CONNECTION_PASSWORD  = "connection.password";
    public static String DATASOURCE_CONFIG_PARAMETER_CONNECTION_POOL_SIZE = "connection.pool-size";

    Class   dataSourceClass;
    String  url;
    String  user;
    String  password;
    int     connectionPoolSize;
    boolean autoCommit;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getConnectionPoolSize() {
        return connectionPoolSize;
    }

    public void setConnectionPoolSize(int connectionPoolSize) {
        this.connectionPoolSize = connectionPoolSize;
    }

    public boolean isAutoCommit() {
        return autoCommit;
    }

    public void setAutoCommit(boolean autoCommit) {
        this.autoCommit = autoCommit;
    }

    public <T extends DataSource> Class<T> getDataSourceClass() {
        return dataSourceClass;
    }

    public <T extends DataSource> void setDataSourceClass(Class<T> dataSourceClass) {
        this.dataSourceClass = dataSourceClass;
    }

    public void loadConfig(Config config) {

        String dataSourceClassName = config.getString(DATASOURCE_CONFIG_PARAMETER_CLASS);
        if ( dataSourceClassName == null ) {
            dataSourceClassName = HikariDataSource.class.getName();
        }

        setDataSourceClass(Core.getClassByName(dataSourceClassName));
        setUrl(config.getString(DATASOURCE_CONFIG_PARAMETER_CONNECTION_URL));
        setUser(config.getString(DATASOURCE_CONFIG_PARAMETER_CONNECTION_USER));
        setPassword(config.getString(DATASOURCE_CONFIG_PARAMETER_CONNECTION_PASSWORD));

        String maxPoolSize = config.getString(DATASOURCE_CONFIG_PARAMETER_CONNECTION_POOL_SIZE);
        if (maxPoolSize != null) {
            setConnectionPoolSize(StringUtils.toBigDecimal(maxPoolSize).intValue());
        }

    }
}
