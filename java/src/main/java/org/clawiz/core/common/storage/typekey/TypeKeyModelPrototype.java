package org.clawiz.core.common.storage.typekey;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.typefield.TypeFieldService;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;
import org.clawiz.core.common.system.service.NotInitializeService;

public class TypeKeyModelPrototype extends TypeModel {
    
    private static TypeField TYPE_ID_FIELD;
    
    private static TypeField NAME_FIELD;
    
    private static TypeField DESCRIPTION_FIELD;
    
    private static TypeField UNIQUE_FIELD;
    
    private static TypeField USE_AS_TO_STRING_FIELD;
    
    private static Type type;
    
    private TypeKeyService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public TypeKeyService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(TypeKeyService.class).getType();
        
        TypeFieldService typeFieldService = getService(TypeFieldService.class);
        
        TYPE_ID_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeKey.Type");
        if ( TYPE_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeKey.Type"}); }
        
        NAME_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeKey.Name");
        if ( NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeKey.Name"}); }
        
        DESCRIPTION_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeKey.Description");
        if ( DESCRIPTION_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeKey.Description"}); }
        
        UNIQUE_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeKey.Unique");
        if ( UNIQUE_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeKey.Unique"}); }
        
        USE_AS_TO_STRING_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeKey.UseAsToString");
        if ( USE_AS_TO_STRING_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeKey.UseAsToString"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _typeId;
    
    public TypeFieldModel typeId() {
        
        if ( _typeId != null ) {
            return _typeId;
        }
        
        _typeId = new TypeFieldModel(this, TYPE_ID_FIELD);
        return _typeId;
        
    }
    
    private TypeFieldModel _name;
    
    public TypeFieldModel name() {
        
        if ( _name != null ) {
            return _name;
        }
        
        _name = new TypeFieldModel(this, NAME_FIELD);
        return _name;
        
    }
    
    private TypeFieldModel _description;
    
    public TypeFieldModel description() {
        
        if ( _description != null ) {
            return _description;
        }
        
        _description = new TypeFieldModel(this, DESCRIPTION_FIELD);
        return _description;
        
    }
    
    private TypeFieldModel _unique;
    
    public TypeFieldModel unique() {
        
        if ( _unique != null ) {
            return _unique;
        }
        
        _unique = new TypeFieldModel(this, UNIQUE_FIELD);
        return _unique;
        
    }
    
    private TypeFieldModel _useAsToString;
    
    public TypeFieldModel useAsToString() {
        
        if ( _useAsToString != null ) {
            return _useAsToString;
        }
        
        _useAsToString = new TypeFieldModel(this, USE_AS_TO_STRING_FIELD);
        return _useAsToString;
        
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "TYPE" : return typeId();
        case "NAME" : return name();
        case "DESCRIPTION" : return description();
        case "UNIQUE" : return unique();
        case "USEASTOSTRING" : return useAsToString();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
