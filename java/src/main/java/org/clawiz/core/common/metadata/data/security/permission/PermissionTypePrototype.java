package org.clawiz.core.common.metadata.data.security.permission;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class PermissionTypePrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeAttribute
    private String description;
    
    public PermissionType withName(String value) {
        setName(value);
        return (PermissionType) this;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String value) {
        this.description = value;
    }
    
    public PermissionType withDescription(String value) {
        setDescription(value);
        return (PermissionType) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
