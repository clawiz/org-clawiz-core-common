/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.file.component;

import org.clawiz.core.common.system.generator.file.component.element.StringTextElement;
import org.clawiz.core.common.system.generator.abstractgenerator.writer.AbstractWriter;
import org.clawiz.core.common.system.generator.file.writer.TextFileWriter;

public abstract class AbstractTextFileComponent extends AbstractFileComponent {

    private TextFileWriter textFileWriter;

    public TextFileWriter getTextFileWriter() {
        if ( textFileWriter != null ) {
            return textFileWriter;
        }
        textFileWriter = (TextFileWriter) getWriter();
        return textFileWriter;
    }

    public StringTextElement addText(String text) {
        StringTextElement te = addElement(StringTextElement.class);
        te.setText(text);
        return te;
    }

    public void p(String str) {
        getTextFileWriter().write(str);
    }

    public void pln(String str) {
        p(str + "\n");
    }


    @Override
    protected <T extends AbstractWriter> Class<T> getWriterClass() {
        return (Class<T>) TextFileWriter.class;
    }

    protected AbstractWriter createWriter() {

        TextFileWriter writer = (TextFileWriter) super.createWriter();

        writer.setEncoding("UTF-8");

        return writer;
    }



}
