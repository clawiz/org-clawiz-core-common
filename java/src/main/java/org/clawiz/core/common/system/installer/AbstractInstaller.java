/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.installer;

import org.clawiz.core.common.Core;
import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.utils.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public abstract class AbstractInstaller extends Service {

    String               packageName;
    String               childsPackageName;
    String               destinationPath;
    InstallParameterList parameters = new InstallParameterList();
    InstallerList        installers = new InstallerList();

    @NotInitializeService
    AbstractInstaller    parent;

    HashMap<String, Class> installerNamesMap = new HashMap<>();

    private boolean incrementalMode;

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getChildsPackageName() {
        return childsPackageName;
    }

    public void setChildsPackageName(String childsPackageName) {
        this.childsPackageName = childsPackageName;
    }

    public String getDestinationPath() {
        return destinationPath;
    }

    public void setDestinationPath(String destinationPath) {

        String path = destinationPath;
        if ( path != null ) {
            if ( path.length() > 3 && path.substring(0, 3).equals("../")) {
                File parentFolder = new File(System.getProperty("user.dir").replaceAll("\\\\", "/"));
                File b = new File(parentFolder, path);
                try {
                    path = b.getCanonicalPath();
                } catch (IOException e) {
                    throwException("Exception on get destination path  for (? , ?) : ? ", parentFolder, path, e.getMessage(), e);
                }
            }
        }

        this.destinationPath = path;
    }

    public InstallParameterList getParameters() {
        return parameters;
    }

    public String getParameter(String name) {
        return parameters.get(name);
    }

    public boolean getParameterAsBoolean(String name) {
        return parameters.getAsBoolean(name);
    }

    public InstallerList getInstallers() {
        return installers;
    }

    public AbstractInstaller getParent() {
        return parent;
    }

    public void setParent(AbstractInstaller parent) {
        this.parent = parent;
    }

    public boolean isIncrementalMode() {
        return incrementalMode;
    }

    public void setIncrementalMode(boolean incrementalMode) {
        this.incrementalMode = incrementalMode;
    }

    public <T extends AbstractInstaller> void setInstallerMap(String name, Class<T> clazz) {
        if ( StringUtils.isEmpty(name) ) {
            throwException("Cannot set installer class map for null or empty name");
        }
        if ( clazz == null ) {
            throwException("Cannot set installer class map '?' for null class", name);
        }
        installerNamesMap.put(name.toUpperCase(), clazz);
        if ( getParent() != null ) {
            getParent().setInstallerMap(name, clazz);
        }
    }

    public <T extends AbstractInstaller> void setInstallerMap(String name, String className) {

        if ( StringUtils.isEmpty(name) ) {
            throwException("Cannot set installer class map for null or empty name");
        }
        if ( StringUtils.isEmpty(className) ) {
            throwException("Cannot set installer class map '?' for null or empty class name", name);
        }

        Class<T> clazz = null;
        try {
            clazz = Core.getClassByName(className);
        } catch (Exception e) {
            throwException("Exception on get class for installer '?' : ?", name, e.getMessage(), e);
        }

        installerNamesMap.put(name.toUpperCase(), clazz);
        if ( getParent() != null ) {
            getParent().setInstallerMap(name, clazz);
        }
    }

    public <T extends AbstractInstaller> Class<T> getInstallerMap(String name) {

        if ( name == null  ) {
            return null;
        }
        Class<T> result = installerNamesMap.get(name.toUpperCase());
        if ( result != null ) {
            return result;
        }

        return getParent() != null ? getParent().getInstallerMap(name) : null;
    }


    public <T extends AbstractInstaller> T addInstaller(Class<T> clazz) {

        T installer = getService(clazz, true);

        installer.setPackageName( notNull(getChildsPackageName(), getPackageName()) );
        installer.setDestinationPath(getDestinationPath());
        installer.getParameters().addAll(getParameters());
        installer.setParent(this);
        installer.setIncrementalMode(isIncrementalMode());

        installer.prepare();

        installers.add(installer);

        return installer;

    }

    public void prepare() {

    }

    public abstract void process();

    public void done() {

    }

    protected void prepareChilds(InstallerList installers) {
        for (AbstractInstaller installer : installers ) {
            installer.prepare();
            installer.prepareChilds(installer.getInstallers());
        }
    }

    protected void processChilds(InstallerList installers) {
        for (AbstractInstaller installer : installers ) {
            installer.process();
            installer.processChilds(installer.getInstallers());
        }
    }

    protected void doneChilds(InstallerList installers) {
        for (AbstractInstaller installer : installers ) {
            installer.done();
            installer.doneChilds(installer.getInstallers());
        }
    }

    public void run() {

        prepare();
        prepareChilds(getInstallers());

        process();
        processChilds(getInstallers());

        done();
        doneChilds(getInstallers());

        Core.clearStaticCaches();

    }


}
