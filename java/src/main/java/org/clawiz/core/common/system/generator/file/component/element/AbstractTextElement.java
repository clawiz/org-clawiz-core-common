/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.file.component.element;

import org.clawiz.core.common.system.generator.abstractgenerator.component.AbstractComponent;
import org.clawiz.core.common.system.generator.abstractgenerator.component.element.AbstractElement;
import org.clawiz.core.common.system.generator.file.component.AbstractTextFileComponent;

public class AbstractTextElement extends AbstractElement {

    private AbstractTextFileComponent textComponent;

    private String lpad = "";

    @Override
    public void setComponent(AbstractComponent component) {
        super.setComponent(component);
        textComponent = (AbstractTextFileComponent) component;
    }

    public String getLpad() {
        return lpad;
    }

    public void setLpad(String lpad) {
        this.lpad = lpad;
    }

    public StringTextElement addText(String text) {
        StringTextElement te = addElement(StringTextElement.class);
        te.setText(text);
        return te;
    }

    protected void p(String str) {
        textComponent.p(lpad + str);
    }

    protected void pln(String str) {
        textComponent.pln(lpad + str);
    }


}
