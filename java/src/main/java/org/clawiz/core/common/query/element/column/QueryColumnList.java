/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.query.element.column;

import org.clawiz.core.common.query.element.AbstractQueryElement;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class QueryColumnList extends AbstractQueryElement implements Iterable<AbstractQueryColumn> {

    private ArrayList<AbstractQueryColumn>                    columns       = new ArrayList<>();
    private HashMap<AbstractQueryColumn, AbstractQueryColumn> columnsCache  = new HashMap<>();
    private HashMap<String, AbstractQueryColumn>              namesCache    = new HashMap<>();
    private HashMap<Integer, AbstractQueryColumn>             indexesCache  = new HashMap<>();


    public AbstractQueryColumn get(String name) {
        if ( name == null ) {
            return null;
        }
        return namesCache.get(name.toUpperCase());
    }

    public AbstractQueryColumn get(int index) {
        return columns.get(index);
    }

    public int size() {
        return columns.size();
    }

    private void add(AbstractQueryColumn column) {
        if ( column.getName() != null && namesCache.containsKey(column.getName().toUpperCase()) ) {
            throwException("Column with name '?' already exists in query", column.getName());
        }

        if ( column.getIndex() != 0 && indexesCache.containsKey(new Integer(column.getIndex())) ) {
            throwException("Column with index '?' already exists in query", column.getIndex());
        }

        columnsCache.put(column, column);
        if ( column.getName() != null ) {
            namesCache.put(column.getName().toUpperCase(), column);
        }

        if ( column.getIndex() != 0 ) {
            indexesCache.put(new Integer(column.getIndex()), column);
        }

        columns.add(column);
    }

    public void changeCachedColumnName(AbstractQueryColumn column, String name) {
        if ( name == null ) {
            throwException("Cannot set query column name to null");
        }
        String key = name.toUpperCase();
        AbstractQueryColumn c = namesCache.get(key);
        if ( c != null && c != column) {
            throwException("Column with name '?' already exists in query", name);
        }
        namesCache.put(key, column);
    }

    public void changeCachedColumnIndex(AbstractQueryColumn column, Integer index) {
        if ( index == null ) {
            throwException("Cannot set query column index to null");
        }
        AbstractQueryColumn c = indexesCache.get(index);
        if ( c != null && c != column ) {
            throwException("Column with index '?' already exists in query", index);
        }
        indexesCache.put(index, column);
    }

    public QueryColumnType column(TypeModel model) {
        QueryColumnType column = addElement(QueryColumnType.class);
        column.typeModel(model);
        add(column);
        return column;
    }

    public QueryColumnType column(TypeModel model, String name) {
        return column(model).name(name);
    }
    
    public QueryColumnTypeField column(TypeFieldModel model) {
        QueryColumnTypeField column = addElement(QueryColumnTypeField.class);
        column.fieldModel(model);
        add(column);
        return column;
    }

    public QueryColumnTypeField column(TypeFieldModel model, String name) {
        return column(model).name(name);
    }

    public QueryColumnTypeRecordId column(TypeRecordIdModel model) {
        QueryColumnTypeRecordId column = addElement(QueryColumnTypeRecordId.class);
        column.recordIdModel(model);
        add(column);
        return column;
    }

    public QueryColumnTypeRecordId column(TypeRecordIdModel model, String name) {
        return column(model).name(name);
    }

    @Override
    public Iterator<AbstractQueryColumn> iterator() {
        return columns.iterator();
    }

    @Override
    public void forEach(Consumer<? super AbstractQueryColumn> action) {
        columns.forEach(action);
    }

    @Override
    public Spliterator<AbstractQueryColumn> spliterator() {
        return columns.spliterator();
    }
}
