package org.clawiz.core.common.metadata.generator.node.dataclass.component;

public class MetadataNodeDataClassComponent extends AbstractMetadataNodeDataClassComponent {

    @Override
    public void process() {
        super.process();

        setOverwriteMode(OverwriteMode.SKIP);

        setName(getStructure().getJavaClassName());

        setExtends(getName() + "Prototype");

    }
}
