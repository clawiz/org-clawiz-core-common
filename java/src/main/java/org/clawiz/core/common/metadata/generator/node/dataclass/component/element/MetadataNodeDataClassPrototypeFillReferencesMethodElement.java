package org.clawiz.core.common.metadata.generator.node.dataclass.component.element;

import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;
import org.clawiz.core.common.metadata.data.language.node.element.field.AbstractStructureField;
import org.clawiz.core.common.metadata.data.language.node.element.field.AttributeStructureField;

public class MetadataNodeDataClassPrototypeFillReferencesMethodElement extends AbstracMetadataNodeDataClassPrototypeMethodElement {


    protected void processField(AbstractStructureField field) {

        if ( field instanceof AttributeStructureField) {
            return;
        }

        addText("");
        if ( field.isArray() ) {
            getComponent().addImport(MetadataNode.class);
            addText("for (MetadataNode node : " + field.getGetMethodName() + "()) {");
            addText("    references.add(node);");
            addText("}");

        } else {
            addText("references.add(" + field.getGetMethodName() + "());");
        }

    }


    @Override
    public void process() {
        super.process();

        setName("fillReferences");

        addParameter(MetadataNodeReferencesList.class, "references");

        addText("super.fillReferences(references);");

        for (AbstractStructureField field : getStructure().getFields()) {
            processField(field);
        }

        addText("");


    }
}
