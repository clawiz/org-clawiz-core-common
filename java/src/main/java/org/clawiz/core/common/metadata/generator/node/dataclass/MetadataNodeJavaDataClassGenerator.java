package org.clawiz.core.common.metadata.generator.node.dataclass;

import org.clawiz.core.common.metadata.generator.node.AbstractMetadataNodeGenerator;
import org.clawiz.core.common.metadata.generator.node.AbstractMetadataNodeGeneratorContext;
import org.clawiz.core.common.metadata.generator.node.dataclass.component.MetadataNodeDataClassComponent;
import org.clawiz.core.common.metadata.generator.node.dataclass.component.MetadataNodeDataClassPrototypeComponent;

public class MetadataNodeJavaDataClassGenerator extends AbstractMetadataNodeGenerator  {

    private MetadataNodeJavaDataClassGeneratorContext context;

    @Override
    public MetadataNodeJavaDataClassGeneratorContext getContext() {
        return context;
    }

    @Override
    public void setContext(AbstractMetadataNodeGeneratorContext context) {
        super.setContext(context);
        this.context = (MetadataNodeJavaDataClassGeneratorContext) context;
    }

    @Override
    protected void process() {
        super.process();

        addComponent(MetadataNodeDataClassComponent.class);
        addComponent(MetadataNodeDataClassPrototypeComponent.class);

    }
}
