package org.clawiz.core.common.metadata.data.common.valuetype;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ValueTypeDatePrototype extends org.clawiz.core.common.metadata.data.common.valuetype.AbstractValueType {
    
    public ValueTypeDate withName(String value) {
        setName(value);
        return (ValueTypeDate) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
