/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.object;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.session.Session;

import java.math.BigDecimal;

/**
 * (C) Clawiz
 * User: abdrashitovta
 * Date: 12.11.2005
 * Time: 17?8?7
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractObject {
    protected BigDecimal id;

    public AbstractObject() {
    }

    public AbstractObject(BigDecimal id) {
        this.id = id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    protected static BigDecimal ___zero = new BigDecimal(0);


    public AbstractTypeService getService() {
        return null;
    }

    public void setService(AbstractTypeService service) {
    }

    public void fillDefaults() {
        throw new CoreException("Not implemented");
    }

    public void check() {
        throw new CoreException("Not implemented");
    }

    public void save() {
        throw new CoreException("Not implemented");
    }

    protected boolean isStringsEquals( String str1, String str2 ){
        String s1 = str1 != null ? (str1.length() > 0 ? str1 : null) : null;
        String s2 = str2 != null ? (str2.length() > 0 ? str2 : null) : null;

        if (s1 == null && s2 == null) {
            return true;
        }

        if ( s1 != null && s2 == null || s1 == null && s2 != null ) {
            return false;
        }

        return s1.equals(s2);
    }

    protected boolean isObjectsEquals( Object o1, Object o2 ){


        if ( o1 instanceof String || o2 instanceof String ) {
            return isStringsEquals((String) o1, (String) o2);
        }

        if (o1 == null && o2 == null) {
            return true;
        }

        if ( o1 != null && o2 == null || o1 == null && o2 != null ) {
            return false;
        }

        if ( o1 instanceof BigDecimal && o2 instanceof BigDecimal ) {
            return ((BigDecimal) o1).compareTo((BigDecimal )o2) == 0;
        }

        return o1.equals(o2);

    }



    boolean skipNotNullCheck = false;

    public boolean isSkipNotNullCheck() {
        return skipNotNullCheck;
    }

    public void setSkipNotNullCheck(boolean skipNotNullCheck) {
        this.skipNotNullCheck = skipNotNullCheck;
    }

    public void setSession(Session session) {

    }

    public void throwException(String code, Object... parameters ) {
        if ( getService() != null ) {
            getService().throwException(code, parameters);
        } else {
            throw new CoreException(code, parameters);
        }
    }


}
