/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system;

import org.clawiz.core.common.system.installer.AbstractInstaller;
import org.clawiz.core.common.system.installer.module.ModuleListInstaller;
import org.clawiz.core.common.system.installer.script.ScriptInstaller;
import org.clawiz.core.common.system.shell.command.AbstractCommand;
import org.clawiz.core.common.system.shell.command.CommandParameter;
import org.clawiz.core.common.utils.StringUtils;

/**
 * (C) Clawiz
 */
public class Install extends AbstractCommand {

    public static final String PARAMETER_DESTINATION_PATH = "destination-path";
    public static final String PARAMETER_INCREMENTAL      = "incremental";
    public static final String PARAMETER_SCRIPT           = "script";
    public static final String PARAMETER_MODULES          = "modules";

    @Override
    protected void process() {

        AbstractInstaller installer = null;

        if ( getParameter("modules") != null ) {

            installer = getService(ModuleListInstaller.class);

            for (String className : StringUtils.splitAndTrim(getParameter(PARAMETER_MODULES), ",") ) {
                ((ModuleListInstaller) installer).getModuleClassNames().add(className);
            }


        } else if ( getParameter("script") != null ) {

            installer = getService(ScriptInstaller.class);
            ((ScriptInstaller) installer).setScriptFileName(getParameter(PARAMETER_SCRIPT));


        } else {
            throwException("Parameter 'modules' or 'script' must be defined");
        }


        installer.setDestinationPath(getParameter(PARAMETER_DESTINATION_PATH));
        installer.setIncrementalMode(StringUtils.toBoolean(getParameter(PARAMETER_INCREMENTAL)));

        for (CommandParameter parameter : getParameters() ) {
            installer.getParameters().set(parameter.getName(), parameter.getValue());
        }

        installer.run();

    }

}
