/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator.component.service.element.save;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.generator.component.ddl.TypeDatabaseObjectsInstallService;
import org.clawiz.core.common.system.type.generator.component.service.element.AbstractTypeServiceMethodElement;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeBLOB;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeList;

public class TypeServiceSaveAuditMethodElement extends AbstractTypeServiceMethodElement {

    public static String getFieldDatabaseValueGetter(String objectName, TypeField field) {
        return TypeServiceSaveMethodElement.getFieldDatabaseValueGetter(objectName, field);
    }

    @Override
    public void process() {
        super.process();

        setName("saveAudit");
        String pold = "old" + getDataObjectClassName();
        String pnew = "new" + getDataObjectClassName();
        addParameter("TransactionAction", "transactionAction");
        addParameter(getDataObjectClassName(), pold);
        addParameter(getDataObjectClassName(), pnew);

        setAccessLevel(AccessLevel.PROTECTED);

        addText("");
        addText(getDataObjectClassName() + " o = old" + getDataObjectClassName() + " != null ? old" + getDataObjectClassName() + " : new " + getDataObjectClassName() + "();");
        addText(getDataObjectClassName() + " n = new" + getDataObjectClassName() + " != null ? new" + getDataObjectClassName() + " : new " + getDataObjectClassName() + "();");
        addText("");
        addText("");

        String oldFields = "";
        String newFields = "";
        String oldObjects = "";
        String newObjects = "";
        int    fCount    = 3;

        for (TypeField field : getFields()) {

            if ( field.getValueType() instanceof ValueTypeBLOB || field.getValueType() instanceof ValueTypeList) {
                continue;
            }

            oldFields += ", " + TypeDatabaseObjectsInstallService.getOldFieldColumnName(field);
            newFields += ", " + TypeDatabaseObjectsInstallService.getNewFieldColumnName(field);

            oldObjects += ", " + getFieldDatabaseValueGetter("o", field);
            newObjects += ", " + getFieldDatabaseValueGetter("n", field);

            if ( field.isUpperFieldExists() ) {
                oldFields += ", " + TypeDatabaseObjectsInstallService.getOldFieldUpperColumnName(field);
                newFields += ", " + TypeDatabaseObjectsInstallService.getNewFieldUpperColumnName(field);

                oldObjects += ", " + getFieldDatabaseValueGetter("o", field) + " != null ? " + getFieldDatabaseValueGetter("o", field) + ".toUpperCase() : null";
                newObjects += ", " + getFieldDatabaseValueGetter("n", field) + " != null ? " + getFieldDatabaseValueGetter("n", field) + ".toUpperCase() : null";
                fCount += 2;
            }
            fCount += 2;
        }

        String valuesStr = "?, ?";
        for ( int i=3; i <= fCount; i++) {
            valuesStr += ", ?";
        }

        addText("executeUpdate(\"insert into " + TypeDatabaseObjectsInstallService.getTypeAuditTableName(getGenerator().getType()) + " (scn, action_type, id " + oldFields + newFields +
                ") values ( "+ valuesStr +
                ")\", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId()" + oldObjects + newObjects + ");");

    }
}
