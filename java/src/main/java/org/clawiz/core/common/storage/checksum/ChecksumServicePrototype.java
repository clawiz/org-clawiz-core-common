package org.clawiz.core.common.storage.checksum;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;
import org.clawiz.core.common.system.service.NotInitializeService;

public class ChecksumServicePrototype extends AbstractTypeService<ChecksumObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.core.common.storage", "Checksum");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Checksum"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      checksum Checked object
    */
    public void check(ChecksumObject checksum) {
        
        if ( checksum == null ) {
            throwException("Cannot check null ?", new Object[]{"ChecksumObject"});
        }
        
        checksum.fillDefaults();
        
        
        if ( objectChecksumTypeToId(checksum.getObjectId(), checksum.getChecksumTypeId(), checksum.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "Checksum", checksum.toObjectChecksumType() });
        }
        
    }
    
    /**
    * Create new ChecksumObject instance and fill default values
    * 
    * @return     Created object
    */
    public ChecksumObject create() {
        
        ChecksumObject checksum = new ChecksumObject();
        checksum.setService((ChecksumService) this);
        
        checksum.fillDefaults();
        
        return checksum;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public ChecksumObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select object_id, checksum_type_id, checksum from cw_core_checksums where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"Checksum", id});
        }
        
        ChecksumObject result = new ChecksumObject();
        
        result.setService((ChecksumService) this);
        result.setId(id);
        result.setObjectId(statement.getBigDecimal(1));
        result.setChecksumTypeId(statement.getBigDecimal(2));
        result.setChecksum(statement.getString(3));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of ChecksumObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public ChecksumList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of ChecksumObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public ChecksumList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        ChecksumList result = new ChecksumList();
        
        
        Statement statement = executeQuery("select id, object_id, checksum_type_id, checksum from cw_core_checksums"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            ChecksumObject object = new ChecksumObject();
        
            object.setService((ChecksumService) this);
            object.setId(statement.getBigDecimal(1));
            object.setObjectId(statement.getBigDecimal(2));
            object.setChecksumTypeId(statement.getBigDecimal(3));
            object.setChecksum(statement.getString(4));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'ObjectChecksumType' fields
    * 
    * @param      objectId       Object
    * @param      checksumTypeId ChecksumType
    * @return     Id of found record or null
    */
    public BigDecimal objectChecksumTypeToId(java.math.BigDecimal objectId, java.math.BigDecimal checksumTypeId) {
        return objectChecksumTypeToId(objectId, checksumTypeId, null);
    }
    
    /**
    * Find id of record by key 'ObjectChecksumType' fields with id not equal skipId
    * 
    * @param      objectId       Object
    * @param      checksumTypeId ChecksumType
    * @param      skipId         Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal objectChecksumTypeToId(java.math.BigDecimal objectId, java.math.BigDecimal checksumTypeId, BigDecimal skipId) {
        
        if ( objectId == null || checksumTypeId == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_checksums where object_id = ? and checksum_type_id = ?", objectId, checksumTypeId);
        } else {
            return executeQueryBigDecimal("select id from cw_core_checksums where object_id = ? and checksum_type_id = ? and id != ?", objectId, checksumTypeId, skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'ObjectChecksumType' fields or create new record with values set to given parameters
    * 
    * @param      objectId            Object
    * @param      checksumTypeId      ChecksumType
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal objectChecksumTypeToId(java.math.BigDecimal objectId, java.math.BigDecimal checksumTypeId, boolean createNewIfNotFound) {
        
        BigDecimal id = objectChecksumTypeToId(objectId, checksumTypeId, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        ChecksumObject object = create();
        object.setObjectId(objectId);
        object.setChecksumTypeId(checksumTypeId);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'ObjectChecksumType' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToObjectChecksumType(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select object_id, checksum_type_id from cw_core_checksums where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        ChecksumObject object = new ChecksumObject();
        object.setService((ChecksumService)this);
        object.setId(id);
        object.setObjectId(statement.getBigDecimal(1));
        object.setChecksumTypeId(statement.getBigDecimal(2));
        
        statement.close();
        
        return object.toObjectChecksumType();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToObjectChecksumType(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      checksumObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(ChecksumObject checksumObject) {
        return checksumObject.toObjectChecksumType();
    }
    
    protected void saveAudit(TransactionAction transactionAction, ChecksumObject oldChecksumObject, ChecksumObject newChecksumObject) {
        
        ChecksumObject o = oldChecksumObject != null ? oldChecksumObject : new ChecksumObject();
        ChecksumObject n = newChecksumObject != null ? newChecksumObject : new ChecksumObject();
        
        
        executeUpdate("insert into a_cw_core_checksums (scn, action_type, id , o_object_id, o_checksum_type_id, o_checksum, n_object_id, n_checksum_type_id, n_checksum) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getObjectId(), o.getChecksumTypeId(), o.getChecksum(), n.getObjectId(), n.getChecksumTypeId(), n.getChecksum());
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      checksum Saved object
    */
    public void save(ChecksumObject checksum) {
        
        if ( checksum == null ) {
            throwException("Cannot save NULL ?", new Object[]{"ChecksumObject"});
        }
        
        TransactionAction transactionAction;
        ChecksumObject oldChecksum;
        if ( checksum.getService() == null ) {
            checksum.setService((ChecksumService)this);
        }
        
        check(checksum);
        
        if ( checksum.getId() == null ) {
        
            checksum.setId(getObjectService().createObject(getTypeId()));
        
            oldChecksum = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, checksum.getId());
        
            executeUpdate("insert into cw_core_checksums" 
                          + "( id, object_id, checksum_type_id, checksum ) "
                          + "values"
                          + "(?, ?, ?, ?)",
                          checksum.getId(), checksum.getObjectId(), checksum.getChecksumTypeId(), checksum.getChecksum() );
        
            if ( checksum.getObjectId() != null ) { getObjectService().setLink(checksum.getObjectId(), checksum.getId()); }
            if ( checksum.getChecksumTypeId() != null ) { getObjectService().setLink(checksum.getChecksumTypeId(), checksum.getId()); }
        
        } else {
        
            oldChecksum = load(checksum.getId());
            if ( oldChecksum.equals(checksum) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, checksum.getId());
        
            executeUpdate("update cw_core_checksums set "
                          + "object_id = ?, checksum_type_id = ?, checksum = ? "
                          + "where id = ?",
                          checksum.getObjectId(), checksum.getChecksumTypeId(), checksum.getChecksum(), checksum.getId() );
        
            getObjectService().changeLinkParent(oldChecksum.getObjectId(), checksum.getObjectId(), checksum.getId());
            getObjectService().changeLinkParent(oldChecksum.getChecksumTypeId(), checksum.getChecksumTypeId(), checksum.getId());
        
        }
        
        saveAudit(transactionAction, oldChecksum, checksum);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        ChecksumObject oldChecksumObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldChecksumObject, null);
        
        getObjectService().deleteObject(id);
        
        if (oldChecksumObject.getObjectId() != null) { getObjectService().deleteLink(oldChecksumObject.getObjectId(), id); }
        if (oldChecksumObject.getChecksumTypeId() != null) { getObjectService().deleteLink(oldChecksumObject.getChecksumTypeId(), id); }
        
        executeUpdate("delete from cw_core_checksums where id = ?", id);
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.checksumtype.ChecksumTypeService checksumTypeService;
    
    public org.clawiz.core.common.storage.checksumtype.ChecksumTypeService getChecksumTypeService() {
        if ( checksumTypeService == null ) {
            checksumTypeService = getService(org.clawiz.core.common.storage.checksumtype.ChecksumTypeService.class);
        }
        return checksumTypeService;
    }
    
    public org.clawiz.core.common.storage.checksumtype.ChecksumTypeObject createChecksumType() {
        return getChecksumTypeService().create();
    }
    
    public org.clawiz.core.common.storage.checksumtype.ChecksumTypeObject loadChecksumType(BigDecimal id) {
        return getChecksumTypeService().load(id);
    }
}
