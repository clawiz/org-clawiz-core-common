/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator.component.dataobject.element;

import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeString;

public class TypeDataObjectFieldSetMethodElement extends AbstractTypeDataObjectMethodElement {

    TypeField field;

    public TypeField getField() {
        return field;
    }

    public void setField(TypeField field) {
        this.field = field;
    }

    @Override
    public void process() {
        super.process();

        setName(field.getSetMethodName());
        String parameterName = field.getJavaVariableName();
        addParameter( field.getValueTypeJavaClassName(), field.getJavaVariableName(), field.getDescription() );

        addJavaDoc("Set '" + ( field.getDescription() != null ? field.getDescription() : field.getName() ) + "' value" );

        if ( field.getValueType() instanceof ValueTypeString) {

            addText("");
            addText("if ( " + parameterName + " != null && " + parameterName + ".length() > " + field.getValueType().getPrecision() + ") {");
            addText("    throw new CoreException(\"Length of field '?' value cannot be more then ?\", \""
                    + field.getType().getName() + "." + field.getName() + "\", \"" + field.getValueType().getPrecision() + "\");");
            addText("}");
            addText("");

            if ( field.isAutoUppercase() ) {
                addText(" this." + parameterName + " = " + parameterName + " != null ? value.toUpperCas() : null;");
            } else {
                addText(" this." + parameterName + " = " + parameterName + ";");
            }

        } else {

            addText( " this." + parameterName + " = " + parameterName + ";" );

        }

    }
}
