package org.clawiz.core.common.storage.metadatanodeclass;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;

public class MetadataNodeClassServicePrototype extends AbstractTypeService<MetadataNodeClassObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.core.common.storage", "MetadataNodeClass");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.MetadataNodeClass"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      metadataNodeClass Checked object
    */
    public void check(MetadataNodeClassObject metadataNodeClass) {
        
        if ( metadataNodeClass == null ) {
            throwException("Cannot check null ?", new Object[]{"MetadataNodeClassObject"});
        }
        
        metadataNodeClass.fillDefaults();
        
        
        if ( nameToId(metadataNodeClass.getName(), metadataNodeClass.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "MetadataNodeClass", metadataNodeClass.toName() });
        }
        
    }
    
    /**
    * Create new MetadataNodeClassObject instance and fill default values
    * 
    * @return     Created object
    */
    public MetadataNodeClassObject create() {
        
        MetadataNodeClassObject metadataNodeClass = new MetadataNodeClassObject();
        metadataNodeClass.setService((MetadataNodeClassService) this);
        
        metadataNodeClass.fillDefaults();
        
        return metadataNodeClass;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public MetadataNodeClassObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select name from cw_core_mdata_nclasses where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"MetadataNodeClass", id});
        }
        
        MetadataNodeClassObject result = new MetadataNodeClassObject();
        
        result.setService((MetadataNodeClassService) this);
        result.setId(id);
        result.setName(statement.getString(1));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of MetadataNodeClassObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public MetadataNodeClassList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of MetadataNodeClassObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public MetadataNodeClassList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        MetadataNodeClassList result = new MetadataNodeClassList();
        
        
        Statement statement = executeQuery("select id, name from cw_core_mdata_nclasses"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            MetadataNodeClassObject object = new MetadataNodeClassObject();
        
            object.setService((MetadataNodeClassService) this);
            object.setId(statement.getBigDecimal(1));
            object.setName(statement.getString(2));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'Name' fields
    * 
    * @param      name Name
    * @return     Id of found record or null
    */
    public BigDecimal nameToId(java.lang.String name) {
        return nameToId(name, null);
    }
    
    /**
    * Find id of record by key 'Name' fields with id not equal skipId
    * 
    * @param      name   Name
    * @param      skipId Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal nameToId(java.lang.String name, BigDecimal skipId) {
        
        if ( name == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_mdata_nclasses where upper_name = ?", name.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_mdata_nclasses where upper_name = ? and id != ?", name.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'Name' fields or create new record with values set to given parameters
    * 
    * @param      name                Name
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal nameToId(java.lang.String name, boolean createNewIfNotFound) {
        
        BigDecimal id = nameToId(name, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        MetadataNodeClassObject object = create();
        object.setName(name);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'Name' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select name from cw_core_mdata_nclasses where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        MetadataNodeClassObject object = new MetadataNodeClassObject();
        object.setService((MetadataNodeClassService)this);
        object.setId(id);
        object.setName(statement.getString(1));
        
        statement.close();
        
        return object.toName();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToName(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      metadataNodeClassObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(MetadataNodeClassObject metadataNodeClassObject) {
        return metadataNodeClassObject.toName();
    }
    
    protected void saveAudit(TransactionAction transactionAction, MetadataNodeClassObject oldMetadataNodeClassObject, MetadataNodeClassObject newMetadataNodeClassObject) {
        
        MetadataNodeClassObject o = oldMetadataNodeClassObject != null ? oldMetadataNodeClassObject : new MetadataNodeClassObject();
        MetadataNodeClassObject n = newMetadataNodeClassObject != null ? newMetadataNodeClassObject : new MetadataNodeClassObject();
        
        
        executeUpdate("insert into a_cw_core_mdata_nclasses (scn, action_type, id , o_name, o_upper_name, n_name, n_upper_name) values ( ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getName(), o.getName() != null ? o.getName().toUpperCase() : null, n.getName(), n.getName() != null ? n.getName().toUpperCase() : null);
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      metadataNodeClass Saved object
    */
    public void save(MetadataNodeClassObject metadataNodeClass) {
        
        if ( metadataNodeClass == null ) {
            throwException("Cannot save NULL ?", new Object[]{"MetadataNodeClassObject"});
        }
        
        TransactionAction transactionAction;
        MetadataNodeClassObject oldMetadataNodeClass;
        if ( metadataNodeClass.getService() == null ) {
            metadataNodeClass.setService((MetadataNodeClassService)this);
        }
        
        check(metadataNodeClass);
        
        if ( metadataNodeClass.getId() == null ) {
        
            metadataNodeClass.setId(getObjectService().createObject(getTypeId()));
        
            oldMetadataNodeClass = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, metadataNodeClass.getId());
        
            executeUpdate("insert into cw_core_mdata_nclasses" 
                          + "( id, name, upper_name ) "
                          + "values"
                          + "(?, ?, ?)",
                          metadataNodeClass.getId(), metadataNodeClass.getName(), ( metadataNodeClass.getName() != null ? metadataNodeClass.getName().toUpperCase() : null ) );
        
        } else {
        
            oldMetadataNodeClass = load(metadataNodeClass.getId());
            if ( oldMetadataNodeClass.equals(metadataNodeClass) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, metadataNodeClass.getId());
        
            executeUpdate("update cw_core_mdata_nclasses set "
                          + "name = ?, upper_name = ? "
                          + "where id = ?",
                          metadataNodeClass.getName(), ( metadataNodeClass.getName() != null ? metadataNodeClass.getName().toUpperCase() : null ), metadataNodeClass.getId() );
        
        }
        
        saveAudit(transactionAction, oldMetadataNodeClass, metadataNodeClass);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        MetadataNodeClassObject oldMetadataNodeClassObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldMetadataNodeClassObject, null);
        
        getObjectService().deleteObject(id);
        
        executeUpdate("delete from cw_core_mdata_nclasses where id = ?", id);
        
    }
}
