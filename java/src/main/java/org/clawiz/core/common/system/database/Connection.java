/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.database;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.database.adapter.DatabaseAdapter;
import org.clawiz.core.common.system.database.datasource.DataSource;
import org.clawiz.core.common.system.session.Session;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;

public class Connection {

    private Session                   session;
    private java.sql.Connection       jdbcConnection;
    private DatabaseAdapter           databaseAdapter;

    public Connection(Session session) {
        this.session = session;
    }

    public synchronized java.sql.Connection getJDBCConnection() {
        if ( jdbcConnection != null ) {
            return jdbcConnection;
        }
        jdbcConnection = session.getDataSource().getJdbcConnection();
        getDatabaseAdapter().initDatabaseConnection();
        return jdbcConnection;
    }

    public synchronized DatabaseAdapter getDatabaseAdapter() {
        if ( databaseAdapter == null ) {
            databaseAdapter = session.getService(session.getDataSource().getDatabaseAdapterClass());
        }
        return databaseAdapter;
    }

    public void init() {

    }

    public boolean isAutoCommit() {
        try {
            return jdbcConnection.getAutoCommit();
        } catch (SQLException e) {
            throw new CoreException("SQL exception on getAutoCommon : ?", e.getMessage(), e);
        }
    }

    public void setAutoCommit(boolean autoCommit) {
        try {
            jdbcConnection.setAutoCommit(autoCommit);
        } catch (SQLException e) {
            throw new CoreException("SQL exception on setAutoCommon : ?", e.getMessage(), e);
        }
    }

    public void commit() {
        try {
            if ( jdbcConnection != null ) {
                jdbcConnection.commit();
            }
        } catch (SQLException e) {
            throw new CoreException("SQL exception on setAutoCommon : ?", e.getMessage(), e);
        }
    }

    public void rollback() {
        try {
            if ( jdbcConnection != null ) {
                jdbcConnection.rollback();
            }
        } catch (SQLException e) {
            throw new CoreException("SQL exception on setAutoCommon : ?", e.getMessage(), e);
        }
    }

    public void close() {
        try {
            if ( jdbcConnection != null ) {
                jdbcConnection.close();
                jdbcConnection = null;
            }
        } catch (SQLException e) {
            throw new CoreException("SQL exception on setAutoCommon : ?", e.getMessage(), e);
        }
    }

    public Statement prepareStatement(String sql) {
        Statement context = new Statement(this, getDatabaseAdapter().prepareSQL(sql));
        context.prepare();
        return context;
    }

    public Statement executeQuery(String sql, Object... parameters) {
        Statement statement = prepareStatement(sql);
        statement.executeQuery(parameters);
        return statement;
    }

    public int executeUpdate(String sql, Object... parameters) {
        Statement statement = prepareStatement(sql);
        return statement.executeUpdate(parameters);
    }

    public BigDecimal executeQueryBigDecimal(String sql, Object... parameters) {
        return prepareStatement(sql).executeQueryBigDecimal(parameters);
    }

    public String executeQueryString(String sql, Object... parameters) {
        return prepareStatement(sql).executeQueryString(parameters);
    }

    public Date executeQueryDate(String sql, Object... parameters) {
        return prepareStatement(sql).executeQueryDate(parameters);
    }

}

