package org.clawiz.core.common.metadata.data.common.enumeration;

import org.clawiz.core.common.CoreException;

public enum BooleanDefaultTrueEnumeration {

    TRUE, FALSE;

    
    public static BooleanDefaultTrueEnumeration toBooleanDefaultTrueEnumeration(String value) {
        if ( value == null ) {
            return null;
        }
        
        try {
            return BooleanDefaultTrueEnumeration.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new CoreException("Wrong BooleanDefaultTrueEnumeration value '?'", value);
        }
        
    }
    
    public static String toDescription(BooleanDefaultTrueEnumeration value) {
        if ( value == null ) {
            return null;
        }
        
        switch (value) {
            case TRUE : return "true";
            case FALSE: return "false";
        }
        
        return value.toString();
    }
}
