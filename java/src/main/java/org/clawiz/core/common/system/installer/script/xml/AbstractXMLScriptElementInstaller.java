/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.installer.script.xml;

import org.clawiz.core.common.system.installer.AbstractInstaller;
import org.clawiz.core.common.utils.StringUtils;
import org.w3c.dom.Element;

import java.io.File;

import static org.clawiz.core.common.system.Install.PARAMETER_DESTINATION_PATH;

public abstract class AbstractXMLScriptElementInstaller extends AbstractInstaller {

    XMLScriptInstaller scriptInstaller;
    Element            element;

    public XMLScriptInstaller getScriptInstaller() {
        return scriptInstaller;
    }

    public void setScriptInstaller(XMLScriptInstaller xmlScriptInstaller) {
        this.scriptInstaller = xmlScriptInstaller;
    }

    public Element getElement() {
        return element;
    }

    public void setElement(Element element) {
        this.element = element;
    }

    public String getScriptFileName() {
        return getScriptInstaller().getScriptFileName();
    }

    @Override
    public <T extends AbstractInstaller> T addInstaller(Class<T> clazz) {
        AbstractInstaller installer = super.addInstaller(clazz);

        if ( AbstractXMLScriptElementInstaller.class.isAssignableFrom(clazz) ) {
            ((AbstractXMLScriptElementInstaller) installer).setScriptInstaller(getScriptInstaller());
        }

        return (T) installer;
    }

    public String getAttribute(String name) {
        String str = element.getAttribute(name);
        return StringUtils.isEmpty(str) ? null : str;
    }

    public String getAttribute(String name, String notNullValue) {
        String str = element.getAttribute(name);
        return StringUtils.isEmpty(str) ? notNullValue : str;
    }

    public String getAttribute(Element element, String name) {
        String str = element.getAttribute(name);
        return StringUtils.isEmpty(str) ? null : str;
    }

    public String relativeToFullFileName(String relativePath) {
        String currentPath = new File(getScriptFileName()).getParent();

        String path = relativePath;
        if ( currentPath != null && ! ( path.charAt(0) == '/' || path.charAt(0) == '\\' ) ) {
            path = currentPath + File.separator + path;
        }

        return path;
    }

}
