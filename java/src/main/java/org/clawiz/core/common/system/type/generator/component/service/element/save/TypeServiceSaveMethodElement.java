/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator.component.service.element.save;

import org.clawiz.core.common.metadata.data.common.valuetype.*;
import org.clawiz.core.common.metadata.data.type.property.extension.AfterExtensionTypeProperty;
import org.clawiz.core.common.metadata.data.type.property.extension.BeforeExtensionTypeProperty;
import org.clawiz.core.common.metadata.data.type.property.extension.method.SaveExtensionTypePropertyMethod;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.generator.component.service.element.AbstractTypeServiceMethodElement;
import org.clawiz.core.common.utils.StringUtils;

public class TypeServiceSaveMethodElement extends AbstractTypeServiceMethodElement {

    public static final String AFTER_SAVE_EXTENSION_NAME = "afterTypeServiceSave";

    public static String getFieldDatabaseValueGetter(String objectName, TypeField field) {
        String getter =  objectName + "." + field.getGetMethodName() + "()"
                + (field.getValueType() instanceof ValueTypeBoolean ? " ? \"T\" : \"F\"" : "" );

        if ( field.getValueType() instanceof ValueTypeEnumeration) {
            getter = "( " + getter + " != null ? " + getter + ".toString() : null )";
        }
        return getter;
    }

    @Override
    public void process() {
        super.process();

        setName("save");

        String parameterName = getTypeJavaVariableName();
        String oldObjectName = "old" + StringUtils.toUpperFirstChar(parameterName);

        addParameter(getDataObjectClassName(), parameterName, "Saved object");

        addJavaDoc("Save object to database:");
        addJavaDoc(" - check data consistency with check() call");
        addJavaDoc(" - INSERT into database for newly created objects or");
        addJavaDoc("   UPDATE if object is loaded from existing database record");

        addText("");
        addText("if ( " + parameterName + " == null ) {");
        addText("    throwException(\"Cannot save NULL ?\", new Object[]{\"" + getDataObjectClassName() + "\"});");
        addText("}");

        addText("");
        addText("TransactionAction transactionAction;");
        addText(getDataObjectClassName() + " " + oldObjectName + ";");
        addText("if ( " + parameterName + ".getService() == null ) {");
        addText("    " + parameterName + ".setService((" + getServiceClassName() + ")this);");
        addText("}");
        addText("");
        addText("check(" + parameterName + ");");
        addText("");
        addExtensions(BeforeExtensionTypeProperty.class, SaveExtensionTypePropertyMethod.class);


        String insertFields = "";
        String insertValues = "";
        String updateFields = "";
        String parameters   = "";
        String prefix       = "";
        int    index        = 1;


        for (TypeField field : getFields() ) {

            if ( field.getValueType() instanceof ValueTypeBLOB || field.getValueType() instanceof ValueTypeList) {
                continue;
            }

            insertFields += prefix + field.getColumnName();
            insertValues += prefix + "?";
            updateFields += prefix + field.getColumnName() + " = ?";
            parameters += prefix + getFieldDatabaseValueGetter(parameterName, field);

            prefix = ", ";

            if ( field.isUpperFieldExists() ) {
                insertFields += prefix + field.getUpperColumnName();
                insertValues += prefix + "?";
                updateFields += prefix + field.getUpperColumnName() + " = ?";
                parameters += prefix + "( " + getFieldDatabaseValueGetter(parameterName, field) + " != null ? " + getFieldDatabaseValueGetter(parameterName, field) + ".toUpperCase() : null )";
            }

        }

        addText("if ( " + parameterName + ".getId() == null ) {");
        addText("");
        addText("    " + parameterName + ".setId(getObjectService().createObject(getTypeId()));");
        addText("");
        addText("    " + oldObjectName + " = null;");
        addText("    transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, " + parameterName + ".getId());");
        addText("");
        addText("    executeUpdate(\"insert into " + getTableName() + "\" ");
        addText("                  + \"( id, " + insertFields + " ) \"");
        addText("                  + \"values\"");
        addText("                  + \"(" + insertValues + ", ?" + ")\",");
        addText("                  " + parameterName + ".getId(), " + parameters + " );");
        addText("");

        boolean addEmptyLine = false;
        for ( TypeField field : getFields() ) {
            if ( field.getValueType() instanceof ValueTypeObject) {
                addText("    if ( " + parameterName + "." + field.getGetMethodName() + "() != null ) { getObjectService().setLink(" + parameterName + "." + field.getGetMethodName() + "(), " + parameterName + ".getId()); }");
                addEmptyLine = true;
            }
        }
        if ( addEmptyLine ) {
            addText("");
        }

        addText("} else {");
        addText("");
        addText("    " + oldObjectName + " = load(" + parameterName + ".getId());");
        addText("    if ( " + oldObjectName + ".equals(" + parameterName + ") ) { return; }");
        addText("");
        addText("    transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, " + parameterName + ".getId());");
        addText("");
        addText("    executeUpdate(\"update " + getTableName() + " set \"");
        addText("                  + \"" + updateFields + " \"");
        addText("                  + \"where id = ?" + "\"," );
        addText("                  " + parameters + ", " + parameterName + ".getId() );");
        addText("");

        addEmptyLine = false;
        for ( TypeField field : getFields() ) {
            if ( field.getValueType() instanceof ValueTypeObject ) {
                addText("    getObjectService().changeLinkParent(" + oldObjectName + "." + field.getGetMethodName() + "(), " + parameterName + "." + field.getGetMethodName() + "(), " + parameterName + ".getId());");
                addEmptyLine = true;
            }
        }
        if ( addEmptyLine ) {
            addText("");
        }

        addText("}");

        addText("");
        addText("saveAudit(transactionAction, " + oldObjectName + ", " + parameterName + ");");
        addText("");
        addExtensions(AfterExtensionTypeProperty.class, SaveExtensionTypePropertyMethod.class);
        processExtensions(AFTER_SAVE_EXTENSION_NAME);

    }
}
