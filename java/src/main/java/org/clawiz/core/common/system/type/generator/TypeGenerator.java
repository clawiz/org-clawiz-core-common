/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator;

import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.storage.MetadataNodeResourcesXMLStorage;
import org.clawiz.core.common.storage.type.TypeService;
import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.generator.component.dataobject.TypeDataObjectClassComponent;
import org.clawiz.core.common.system.type.generator.component.dataobject.TypeDataObjectClassPrototypeComponent;
import org.clawiz.core.common.system.type.generator.component.dataobject.TypeDataObjectFieldValueTypeEnumComponent;
import org.clawiz.core.common.system.type.generator.component.dataobjectlist.TypeDataObjectListClassComponent;
import org.clawiz.core.common.system.type.generator.component.dataobjectlist.TypeDataObjectListClassPrototypeComponent;
import org.clawiz.core.common.system.type.generator.component.ddl.TypeDatabaseObjectsInstallService;
import org.clawiz.core.common.system.type.generator.component.model.TypeModelClassComponent;
import org.clawiz.core.common.system.type.generator.component.model.TypeModelClassPrototypeComponent;
import org.clawiz.core.common.system.type.generator.component.service.TypeServiceClassComponent;
import org.clawiz.core.common.system.type.generator.component.service.TypeServiceClassPrototypeComponent;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeEnumeration;
import org.clawiz.core.common.metadata.data.common.valuetype.ValueTypeObject;
import org.clawiz.core.common.utils.StringUtils;

import java.math.BigDecimal;
import java.util.HashMap;

public class TypeGenerator extends AbstractGenerator {

    GenerateTypeContext context;

    public GenerateTypeContext getContext() {
        return context;
    }

    public void setContext(GenerateTypeContext context) {
        this.context = context;
    }

    TypeDataObjectClassComponent     dataObjectClassComponent;
    TypeDataObjectListClassComponent dataObjectListClassComponent;
    TypeServiceClassComponent        serviceClassComponent;
    TypeModelClassComponent          modelClassComponent;

    public String getDataObjectClassName() {
        return dataObjectClassComponent.getName();
    }

    String dataDataObjectParameterName;
    public String getDataObjectParameterName() {
        if ( dataDataObjectParameterName == null ) {
            dataDataObjectParameterName = StringUtils.toLowerFirstChar(getDataObjectClassName());
        }
        return dataDataObjectParameterName;
    }

    public String getDataObjectListClassName() {
        return dataObjectListClassComponent.getName();
    }

    public String getServiceClassName() {
        return serviceClassComponent.getName();
    }

    public String getModelClassName() {
        return modelClassComponent.getName();
    }

    public Type getType() {
        return context.getType();
    }

    String typeJavaVariableName = null;
    public String getTypeJavaVariableName() {
        return typeJavaVariableName;
    }

    @Override
    public MetadataNode getMetadataNode() {
        return getType();
    }

    @Override
    public String getDestinationPath() {
        return this.context.getDestinationPath() != null ? this.context.getDestinationPath() : super.getDestinationPath();
    }

    @Override
    public void process() {
        super.process();


        typeJavaVariableName         = StringUtils.toLowerFirstChar(getType().getJavaClassName());
        dataDataObjectParameterName  = null;

        logDebug("Start generating type " + getType().getName() + " (" + getType().getPackageName() + ")" );

        TypeDatabaseObjectsInstallService databaseInstall = getService(TypeDatabaseObjectsInstallService.class);
        databaseInstall.setType(getType());
        databaseInstall.run();

        for( TypeField field : getType().getFields() ) {
            if ( field.getValueType() instanceof ValueTypeEnumeration ) {
                addComponent(TypeDataObjectFieldValueTypeEnumComponent.class).setField(field);
            }
        }

        dataObjectClassComponent     = addComponent(TypeDataObjectClassComponent.class);
        dataObjectListClassComponent = addComponent(TypeDataObjectListClassComponent.class);
        serviceClassComponent        = addComponent(TypeServiceClassComponent.class);
        modelClassComponent          = addComponent(TypeModelClassComponent.class);

        addComponent(TypeDataObjectClassPrototypeComponent.class);
        addComponent(TypeDataObjectListClassPrototypeComponent.class);
        addComponent(TypeServiceClassPrototypeComponent.class);
        addComponent(TypeModelClassPrototypeComponent.class);


    }


    HashMap<Type, TypeField> referencedTypeLinkedFields = new HashMap<>();
    public TypeField getReferencedTypeLinkedField(Type referencedType) {

        TypeField linkedField = referencedTypeLinkedFields.get(referencedType);
        if ( linkedField != null ) {
            return linkedField;
        }

        for (TypeField refField : referencedType.getFields() ) {

            if ( refField.getValueType() instanceof ValueTypeObject) {

                ValueTypeObject vto = (ValueTypeObject) refField.getValueType();
                if ( vto.getReferencedType() != null
                        && vto.getReferencedType().getFullName().equals(getType().getFullName()) ) {
                    referencedTypeLinkedFields.put(referencedType, refField);
                    return refField;
                }

            }

        }
        throwException("Cannot define reference linked field from type ? to ?", referencedType.getFullName(), getType().getFullName());
        return null;
    }


    @Override
    protected void done() {
        super.done();
        logDebug("Done generating type " + getType().getName() + " (" + getType().getPackageName() + ")" );
        commit();
    }
}
