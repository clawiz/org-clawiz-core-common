package org.clawiz.core.common.storage.remotenode;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.object.AbstractObject;
import org.clawiz.core.common.system.session.Session;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Clob;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.date.DateUtils;
import java.lang.SuppressWarnings;

public class RemoteNodeObjectPrototype extends AbstractObject {
    
    /**
    * Name
    */
    private java.lang.String name;
    
    /**
    * Password
    */
    private java.lang.String password;
    
    /**
    * PasswordHash
    */
    private java.lang.String passwordHash;
    
    /**
    * PreviousPassword
    */
    private java.lang.String previousPassword;
    
    /**
    * LastReceivedScn
    */
    private java.math.BigDecimal lastReceivedScn;
    
    /**
    * RegisteredUser
    */
    private java.math.BigDecimal registeredUserId;
    
    private org.clawiz.core.common.storage.user.UserObject registeredUser;
    
    public RemoteNodeService service;
    
    /**
    * 
    * @return     Name
    */
    public java.lang.String getName() {
        return this.name;
    }
    
    /**
    * Set 'Name' value
    * 
    * @param      name Name
    */
    public void setName(java.lang.String name) {
        
        if ( name != null && name.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "RemoteNode.Name", "250");
        }
        
         this.name = name;
    }
    
    /**
    * Set 'Name' value and return this object
    * 
    * @param      name Name
    * @return     RemoteNode object
    */
    public RemoteNodeObjectPrototype withName(java.lang.String name) {
        setName(name);
        return this;
    }
    
    /**
    * 
    * @return     Password
    */
    public java.lang.String getPassword() {
        return this.password;
    }
    
    /**
    * Set 'Password' value
    * 
    * @param      password Password
    */
    public void setPassword(java.lang.String password) {
        
        if ( password != null && password.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "RemoteNode.Password", "250");
        }
        
         this.password = password;
    }
    
    /**
    * Set 'Password' value and return this object
    * 
    * @param      password Password
    * @return     RemoteNode object
    */
    public RemoteNodeObjectPrototype withPassword(java.lang.String password) {
        setPassword(password);
        return this;
    }
    
    /**
    * 
    * @return     PasswordHash
    */
    public java.lang.String getPasswordHash() {
        return this.passwordHash;
    }
    
    /**
    * Set 'PasswordHash' value
    * 
    * @param      passwordHash PasswordHash
    */
    public void setPasswordHash(java.lang.String passwordHash) {
        
        if ( passwordHash != null && passwordHash.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "RemoteNode.PasswordHash", "250");
        }
        
         this.passwordHash = passwordHash;
    }
    
    /**
    * Set 'PasswordHash' value and return this object
    * 
    * @param      passwordHash PasswordHash
    * @return     RemoteNode object
    */
    public RemoteNodeObjectPrototype withPasswordHash(java.lang.String passwordHash) {
        setPasswordHash(passwordHash);
        return this;
    }
    
    /**
    * 
    * @return     PreviousPassword
    */
    public java.lang.String getPreviousPassword() {
        return this.previousPassword;
    }
    
    /**
    * Set 'PreviousPassword' value
    * 
    * @param      previousPassword PreviousPassword
    */
    public void setPreviousPassword(java.lang.String previousPassword) {
        
        if ( previousPassword != null && previousPassword.length() > 250) {
            throw new CoreException("Length of field '?' value cannot be more then ?", "RemoteNode.PreviousPassword", "250");
        }
        
         this.previousPassword = previousPassword;
    }
    
    /**
    * Set 'PreviousPassword' value and return this object
    * 
    * @param      previousPassword PreviousPassword
    * @return     RemoteNode object
    */
    public RemoteNodeObjectPrototype withPreviousPassword(java.lang.String previousPassword) {
        setPreviousPassword(previousPassword);
        return this;
    }
    
    /**
    * 
    * @return     LastReceivedScn
    */
    public java.math.BigDecimal getLastReceivedScn() {
        return this.lastReceivedScn;
    }
    
    /**
    * Set 'LastReceivedScn' value
    * 
    * @param      lastReceivedScn LastReceivedScn
    */
    public void setLastReceivedScn(java.math.BigDecimal lastReceivedScn) {
         this.lastReceivedScn = lastReceivedScn;
    }
    
    /**
    * Set 'LastReceivedScn' value and return this object
    * 
    * @param      lastReceivedScn LastReceivedScn
    * @return     RemoteNode object
    */
    public RemoteNodeObjectPrototype withLastReceivedScn(java.math.BigDecimal lastReceivedScn) {
        setLastReceivedScn(lastReceivedScn);
        return this;
    }
    
    /**
    * 
    * @return     RegisteredUser
    */
    public java.math.BigDecimal getRegisteredUserId() {
        return this.registeredUserId;
    }
    
    public org.clawiz.core.common.storage.user.UserObject getRegisteredUser() {
        return getRegisteredUser(false);
    }
    
    @SuppressWarnings("Duplicates")
    public org.clawiz.core.common.storage.user.UserObject getRegisteredUser(boolean autoCreate) {
        
        if ( registeredUser != null ) { return registeredUser; }
        
        if ( registeredUserId != null ) {
            registeredUser = service.loadUser(registeredUserId);
        } else if ( autoCreate ) {
            registeredUser = service.createUser();
        }
        
        return registeredUser;
    }
    
    /**
    * Set 'RegisteredUser' value
    * 
    * @param      registeredUserId RegisteredUser
    */
    public void setRegisteredUserId(java.math.BigDecimal registeredUserId) {
         this.registeredUserId = registeredUserId;
    }
    
    /**
    * Set 'RegisteredUser' value and return this object
    * 
    * @param      registeredUserId RegisteredUser
    * @return     RemoteNode object
    */
    public RemoteNodeObjectPrototype withRegisteredUserId(java.math.BigDecimal registeredUserId) {
        setRegisteredUserId(registeredUserId);
        return this;
    }
    
    public RemoteNodeService getService() {
        return this.service;
    }
    
    public void setService(RemoteNodeService service) {
        this.service = service;
    }
    
    /**
    * Copy values of all fields to 'target'
    * 
    * @param       target Target object to copy data
    */
    @SuppressWarnings("Duplicates")
    public void copy(RemoteNodeObjectPrototype  target) {
        target.setName(getName());
        target.setPassword(getPassword());
        target.setPasswordHash(getPasswordHash());
        target.setPreviousPassword(getPreviousPassword());
        target.setLastReceivedScn(getLastReceivedScn());
        target.setRegisteredUserId(getRegisteredUserId());
    }
    
    /**
    * Fill object field default values
    */
    public void fillDefaults() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before fill object default values");
        }
    }
    
    /**
    * Check object data consistency
    */
    public void check() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before check object data");
        }
        service.check((RemoteNodeObject) this);
    }
    
    /**
    * Save object to database
    */
    @SuppressWarnings("Duplicates")
    public void save() {
        if ( service == null ) {
            throw new CoreException("Service must be defined before save object");
        }
        if ( registeredUser != null ) {
            registeredUser.save();
            registeredUserId = registeredUser.getId();
        }
        service.save((RemoteNodeObject) this);
    }
    
    /**
    * Return true if all field values equals to 'object' field values or both value of field is null
    * 
    * @param      object Equal check object
    * @return     field-by-field equals check
    */
    public boolean equals(RemoteNodeObjectPrototype object) {
        return 
               isObjectsEquals(this.getName(), object.getName() ) 
            && isObjectsEquals(this.getPassword(), object.getPassword() ) 
            && isObjectsEquals(this.getPasswordHash(), object.getPasswordHash() ) 
            && isObjectsEquals(this.getPreviousPassword(), object.getPreviousPassword() ) 
            && isObjectsEquals(this.getLastReceivedScn(), object.getLastReceivedScn() ) 
            && isObjectsEquals(this.getRegisteredUserId(), object.getRegisteredUserId() ) 
               ;
    }
    
    /**
    * Calculate hash value of object data
    * 
    * @return     int hash code of object
    */
    public int hashCode() {
        int result = 1;
        
        result = result * 31 + (getName() != null ? getName().hashCode() : 0);
        result = result * 31 + (getPassword() != null ? getPassword().hashCode() : 0);
        result = result * 31 + (getPasswordHash() != null ? getPasswordHash().hashCode() : 0);
        result = result * 31 + (getPreviousPassword() != null ? getPreviousPassword().hashCode() : 0);
        result = result * 31 + (getLastReceivedScn() != null ? getLastReceivedScn().hashCode() : 0);
        result = result * 31 + (getRegisteredUserId() != null ? getRegisteredUserId().hashCode() : 0);
        
        return result;
    }
    
    /**
    * Prepare concatenated string values of key 'Name' fields
    * 
    * @return     Concatenated string values of key 'toName' fields
    */
    public String toName() {
        return getName();
    }
}
