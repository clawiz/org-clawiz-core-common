package org.clawiz.core.common.storage.sequence;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;

public class SequenceModelPrototype extends TypeModel {
    
    private static TypeField NAME_FIELD;
    
    private static TypeField OBJECT1ID_FIELD;
    
    private static TypeField OBJECT2ID_FIELD;
    
    private static TypeField OBJECT3ID_FIELD;
    
    private static TypeField START_VALUE_FIELD;
    
    private static TypeField MAX_VALUE_FIELD;
    
    private static TypeField LAST_CACHED_VALUE_FIELD;
    
    private static TypeField CACHE_LENGTH_FIELD;
    
    private static Type type;
    
    private SequenceService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public SequenceService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(SequenceService.class).getType();
        
        
        NAME_FIELD = type.getFields().get("Name");
        if ( NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Sequence.Name"}); }
        
        OBJECT1ID_FIELD = type.getFields().get("Object1");
        if ( OBJECT1ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Sequence.Object1"}); }
        
        OBJECT2ID_FIELD = type.getFields().get("Object2");
        if ( OBJECT2ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Sequence.Object2"}); }
        
        OBJECT3ID_FIELD = type.getFields().get("Object3");
        if ( OBJECT3ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Sequence.Object3"}); }
        
        START_VALUE_FIELD = type.getFields().get("StartValue");
        if ( START_VALUE_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Sequence.StartValue"}); }
        
        MAX_VALUE_FIELD = type.getFields().get("MaxValue");
        if ( MAX_VALUE_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Sequence.MaxValue"}); }
        
        LAST_CACHED_VALUE_FIELD = type.getFields().get("LastCachedValue");
        if ( LAST_CACHED_VALUE_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Sequence.LastCachedValue"}); }
        
        CACHE_LENGTH_FIELD = type.getFields().get("CacheLength");
        if ( CACHE_LENGTH_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Sequence.CacheLength"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _name;
    
    public TypeFieldModel name() {
        
        if ( _name != null ) {
            return _name;
        }
        
        _name = new TypeFieldModel(this, NAME_FIELD);
        return _name;
        
    }
    
    private TypeFieldModel _object1Id;
    
    public TypeFieldModel object1Id() {
        
        if ( _object1Id != null ) {
            return _object1Id;
        }
        
        _object1Id = new TypeFieldModel(this, OBJECT1ID_FIELD);
        return _object1Id;
        
    }
    
    private TypeFieldModel _object2Id;
    
    public TypeFieldModel object2Id() {
        
        if ( _object2Id != null ) {
            return _object2Id;
        }
        
        _object2Id = new TypeFieldModel(this, OBJECT2ID_FIELD);
        return _object2Id;
        
    }
    
    private TypeFieldModel _object3Id;
    
    public TypeFieldModel object3Id() {
        
        if ( _object3Id != null ) {
            return _object3Id;
        }
        
        _object3Id = new TypeFieldModel(this, OBJECT3ID_FIELD);
        return _object3Id;
        
    }
    
    private TypeFieldModel _startValue;
    
    public TypeFieldModel startValue() {
        
        if ( _startValue != null ) {
            return _startValue;
        }
        
        _startValue = new TypeFieldModel(this, START_VALUE_FIELD);
        return _startValue;
        
    }
    
    private TypeFieldModel _maxValue;
    
    public TypeFieldModel maxValue() {
        
        if ( _maxValue != null ) {
            return _maxValue;
        }
        
        _maxValue = new TypeFieldModel(this, MAX_VALUE_FIELD);
        return _maxValue;
        
    }
    
    private TypeFieldModel _lastCachedValue;
    
    public TypeFieldModel lastCachedValue() {
        
        if ( _lastCachedValue != null ) {
            return _lastCachedValue;
        }
        
        _lastCachedValue = new TypeFieldModel(this, LAST_CACHED_VALUE_FIELD);
        return _lastCachedValue;
        
    }
    
    private TypeFieldModel _cacheLength;
    
    public TypeFieldModel cacheLength() {
        
        if ( _cacheLength != null ) {
            return _cacheLength;
        }
        
        _cacheLength = new TypeFieldModel(this, CACHE_LENGTH_FIELD);
        return _cacheLength;
        
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "NAME" : return name();
        case "OBJECT1" : return object1Id();
        case "OBJECT2" : return object2Id();
        case "OBJECT3" : return object3Id();
        case "STARTVALUE" : return startValue();
        case "MAXVALUE" : return maxValue();
        case "LASTCACHEDVALUE" : return lastCachedValue();
        case "CACHELENGTH" : return cacheLength();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
