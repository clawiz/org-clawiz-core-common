/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.abstractgenerator.component.element;

import org.clawiz.core.common.system.service.Service;
import org.clawiz.core.common.system.service.NotInitializeService;
import org.clawiz.core.common.system.generator.abstractgenerator.component.AbstractComponent;
import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;

import java.util.ArrayList;
import java.util.HashMap;
//import ru.cs.system.service.core.generator.abstractgenerator.abstractcomponent.Ab

/**
 * Created with IntelliJ IDEA.
 * User: abdrashitovta
 * Date: 17.01.13
 * Time: 17?1
 * To change this template use File | Settings | File Templates.
 */
@NotInitializeService
public class AbstractElement extends Service {

    @NotInitializeService
    private AbstractComponent component;

    @NotInitializeService
    private AbstractElement parent;

    private HashMap<String, AbstractElement> childsCache = new HashMap<>();
    private ArrayList<AbstractElement> childs = new ArrayList<>();

    private String name;

    public void setComponent(AbstractComponent component) {
        this.component = component;
        for ( AbstractElement e : childs ) {
            e.setComponent(component);
        }
    }

    public ArrayList<AbstractElement> getElements() {
        return childs;
    }

    public AbstractElement getElement(String name) {
        if ( name == null ) {
            return null;
        }
        return childsCache.get(name.toUpperCase());
    }


    public void setName(String name) {
        this.name = name;
    }

    public AbstractElement getParent() {
        return parent;
    }

    public void setParent(AbstractElement parent) {
        this.parent = parent;
    }

    public AbstractComponent getComponent() {
        return component;
    }

    public AbstractGenerator getGenerator() {
        return component.getGenerator();
    }

    public <T extends AbstractElement> T addElement(Class<T> clazz){
        return addElement(clazz, getGenerator().createUID());
    }

    public <T extends AbstractElement> T addElement(Class<T> clazz, String childName){

        if ( childName == null ) {
            throwException("Element name for '?' cannot be NULL", new Object[]{clazz.getName()});
        }
        String key = childName.toUpperCase();

        T child = getService(clazz, getGenerator().createUID());
        childs.add(child);
        childsCache.put(key, child);
        child.setComponent(getComponent());
        child.setName(childName);
        child.setParent(this);

        child.prepare();

        return child;
    }


    protected void writeElements() {
        for ( AbstractElement child : childs) {
            child.write();
        }
    }

    public void prepare() {

    }

    public void process() {

    }

    public void callChildsProcess() {
        for ( AbstractElement element : childs ) {
            element.process();
            element.callChildsProcess();
        }
    }

    public void done() {

    }

    public void callChildsDone() {
        for ( AbstractElement element : childs ) {
            element.done();
            element.callChildsDone();
        }
    }

    public void write() {
        writeElements();
    }

    public String getName() {
        return name;
    }

    @Override
    public void destroy() {
        for(AbstractElement element : getElements() ) {
            element.destroy();
        }
        getElements().clear();
        super.destroy();
    }

    public <T extends AbstractElement> ArrayList<T> getElementsByClass(Class<T> elementClass) {
        return getElementsByClass(elementClass, true);
    }

    public <T extends AbstractElement> ArrayList<T> getElementsByClass(Class<T> elementClass, boolean withAllChildLevels) {
        ArrayList<T> result = new ArrayList<>();

        for (AbstractElement element : getElements() ) {
            if ( elementClass.isAssignableFrom(element.getClass())) {
                result.add((T)element);
            }
            if ( withAllChildLevels ) {
                result.addAll(element.getElementsByClass(elementClass, true));
            }
        }

        return result;
    }

}