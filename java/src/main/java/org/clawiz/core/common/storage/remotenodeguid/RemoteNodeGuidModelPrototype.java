package org.clawiz.core.common.storage.remotenodeguid;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;
import org.clawiz.core.common.system.service.NotInitializeService;

public class RemoteNodeGuidModelPrototype extends TypeModel {
    
    private static TypeField REMOTE_NODE_ID_FIELD;
    
    private static TypeField OBJECT_ID_FIELD;
    
    private static TypeField NODE_GUID_FIELD;
    
    private static Type type;
    
    private RemoteNodeGuidService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public RemoteNodeGuidService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(RemoteNodeGuidService.class).getType();
        
        
        REMOTE_NODE_ID_FIELD = type.getFields().get("RemoteNode");
        if ( REMOTE_NODE_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.RemoteNodeGuid.RemoteNode"}); }
        
        OBJECT_ID_FIELD = type.getFields().get("Object");
        if ( OBJECT_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.RemoteNodeGuid.Object"}); }
        
        NODE_GUID_FIELD = type.getFields().get("NodeGuid");
        if ( NODE_GUID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.RemoteNodeGuid.NodeGuid"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _remoteNodeId;
    
    public TypeFieldModel remoteNodeId() {
        
        if ( _remoteNodeId != null ) {
            return _remoteNodeId;
        }
        
        _remoteNodeId = new TypeFieldModel(this, REMOTE_NODE_ID_FIELD);
        return _remoteNodeId;
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.remotenode.RemoteNodeModel _remoteNodeTypeModel;
    
    public org.clawiz.core.common.storage.remotenode.RemoteNodeModel remoteNode() {
        
        if ( _remoteNodeTypeModel == null ) {
            org.clawiz.core.common.storage.remotenode.RemoteNodeModel model = getService(org.clawiz.core.common.storage.remotenode.RemoteNodeModel.class, this.toString() + "_RemoteNode");
            model.setJoinField(remoteNodeId());
            _remoteNodeTypeModel = model;
        }
        
        return _remoteNodeTypeModel;
    }
    
    private TypeFieldModel _objectId;
    
    public TypeFieldModel objectId() {
        
        if ( _objectId != null ) {
            return _objectId;
        }
        
        _objectId = new TypeFieldModel(this, OBJECT_ID_FIELD);
        return _objectId;
        
    }
    
    private TypeFieldModel _nodeGuid;
    
    public TypeFieldModel nodeGuid() {
        
        if ( _nodeGuid != null ) {
            return _nodeGuid;
        }
        
        _nodeGuid = new TypeFieldModel(this, NODE_GUID_FIELD);
        return _nodeGuid;
        
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "REMOTENODE" : return remoteNodeId();
        case "OBJECT" : return objectId();
        case "NODEGUID" : return nodeGuid();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
