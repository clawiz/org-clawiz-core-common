package org.clawiz.core.common.metadata.data.database.table.column.type;

import java.math.BigDecimal;
import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class VarcharTableColumnTypePrototype extends org.clawiz.core.common.metadata.data.database.table.column.type.AbstractTableColumnType {
    
    @ExchangeAttribute
    private BigDecimal length;
    
    public VarcharTableColumnType withName(String value) {
        setName(value);
        return (VarcharTableColumnType) this;
    }
    
    public BigDecimal getLength() {
        return this.length;
    }
    
    public void setLength(BigDecimal value) {
        this.length = value;
    }
    
    public VarcharTableColumnType withLength(BigDecimal value) {
        setLength(value);
        return (VarcharTableColumnType) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
