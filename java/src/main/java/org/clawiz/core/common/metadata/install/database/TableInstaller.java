/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.metadata.install.database;

import org.clawiz.core.common.metadata.data.database.table.Table;
import org.clawiz.core.common.metadata.data.database.table.column.TableColumn;
import org.clawiz.core.common.metadata.install.AbstractMetadataNodeInstaller;
import org.clawiz.core.common.system.database.metadata.DatabaseTable;
import org.clawiz.core.common.system.database.metadata.DatabaseTableColumn;
import org.clawiz.core.common.system.installer.database.DatabaseTableInstaller;

public class TableInstaller extends AbstractMetadataNodeInstaller {

    @Override
    public void process() {
        super.process();


        Table table = (Table) getNode();

        DatabaseTable databaseTable = new DatabaseTable(table.getName());

        for ( TableColumn column : table.getColumns() ) {
            DatabaseTableColumn databaseColumn = new DatabaseTableColumn();
            databaseTable.addColumn(databaseColumn);
            databaseColumn.setName(column.getName());
            databaseColumn.setType(column.getType().getDatabaseType());
            databaseColumn.setPrecision(column.getType().getPrecision() != null ? column.getType().getPrecision().intValue() : 0);
            databaseColumn.setScale(column.getType().getScale() != null ? column.getType().getScale().intValue() : 0);
        }

        DatabaseTableInstaller databaseTableInstaller = addInstaller(DatabaseTableInstaller.class);
        databaseTableInstaller.setTable(databaseTable);

    }
}
