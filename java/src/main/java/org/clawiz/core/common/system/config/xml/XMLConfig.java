/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.config.xml;

import com.sun.org.apache.xerces.internal.dom.DeferredAttrImpl;
import com.sun.org.apache.xerces.internal.dom.DeferredTextImpl;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.config.AbstractConfig;
import org.clawiz.core.common.system.config.Config;
import org.clawiz.core.common.utils.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * (c) Clawiz
 */
public class XMLConfig extends AbstractConfig implements Config {


    Node        node;

    public Node getNode() {
        return node;
    }

    public static String trim(String str) {
        if ( str == null) {
            return null;
        }

        int lPos = 0;
        for ( ; lPos < str.length() && (str.charAt(lPos)==160 || str.charAt(lPos)==32 || str.charAt(lPos)==10 || str.charAt(lPos)==13); lPos++);
        int rPos = str.length()-1;
        for ( ; rPos > lPos && (str.charAt(lPos)==160 || str.charAt(lPos)==32 || str.charAt(lPos)==10 || str.charAt(lPos)==13); rPos--);

        return str.substring(lPos, rPos+1);
    }

    public void setNode(Node node) {
        getChilds().clear();
        this.node = node;
        setName(node.getNodeName());

        if ( node instanceof Element) {
            Element e = (Element) node;
            for (int i=0; i < e.getAttributes().getLength(); i++) {
                Node a = e.getAttributes().item(i);
                DeferredAttrImpl da = (DeferredAttrImpl) a;
                if ( a instanceof DeferredAttrImpl ) {
                    XMLConfig child = (XMLConfig) addChild();
                    child.setName(da.getName());
                    child.setValue(da.getValue());
                }
            }
        }

        NodeList list = node.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
            Node childIdde = list.item(i); // текущий нод

            if (childIdde instanceof Element){
                XMLConfig child = (XMLConfig) addChild();
                child.setNode(childIdde);
            }

            if ( childIdde instanceof DeferredTextImpl ) {
                DeferredTextImpl text = (DeferredTextImpl) childIdde;
                String data = trim(text.getData());
                setValue(StringUtils.isEmpty(data) ? null : data);
            }

        }
    }

    @Override
    public void load(String fileName, String charsetName) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            FileInputStream in = new FileInputStream(new File(fileName));
            Document document = dBuilder.parse(in, "UTF-8");

            setNode(document.getDocumentElement());

        } catch (ParserConfigurationException e) {
            throw new CoreException("Parse configuration exception for '?' : ?", fileName, e.getMessage(), e);
        } catch (SAXException e) {
            throw new CoreException("SAX exception for '?' : ?", fileName, e.getMessage(), e);
        } catch (IOException e) {
            throw new CoreException("IO exception for '?' : ?", fileName, e.getMessage(), e);
        }
    }

}
