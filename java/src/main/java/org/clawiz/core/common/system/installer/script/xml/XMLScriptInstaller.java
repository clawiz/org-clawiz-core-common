/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.installer.script.xml;

import org.clawiz.core.common.system.installer.script.ScriptInstaller;
import org.clawiz.core.common.utils.file.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;

public class XMLScriptInstaller extends ScriptInstaller {

    public static final String CLAWIZ_INSTALL_SCRIPT = "clawiz-install-script";

    FileUtils fileUtils;

    @Override
    public void process() {


        logInfo( "Parsing file " +  getScriptFileName());
        InputStream inputStream = getService(FileUtils.class).getInputStream(getScriptFileName());

        Document document = null;

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse( inputStream );
        } catch (SAXException e) {
            throwException("SAX Exception on XML file parsing  ", new Object[]{getScriptFileName()}, e);
        } catch (IOException e) {
            throwException("IO Exception on XML file parsing  ", new Object[]{getScriptFileName()}, e);
        } catch( ParserConfigurationException e ) {
            throwException("ParserConfiguration Exception on XML file parsing  ", new Object[]{getScriptFileName()}, e);
        }

        Element topElement = document.getDocumentElement();

        if ( ! topElement.getTagName().equalsIgnoreCase(CLAWIZ_INSTALL_SCRIPT)) {
            throwException("XML install script ? top node name not equal '?'", getScriptFileName(), CLAWIZ_INSTALL_SCRIPT);
        }

        XMLScriptNodeListInstaller nodeListInstaller = addInstaller(XMLScriptNodeListInstaller.class);

        nodeListInstaller.setNodeList(topElement.getChildNodes());
        nodeListInstaller.setScriptInstaller(this);


    }
}
