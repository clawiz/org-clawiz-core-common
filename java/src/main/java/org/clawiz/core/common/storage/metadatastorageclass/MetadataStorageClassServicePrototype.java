package org.clawiz.core.common.storage.metadatastorageclass;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;

public class MetadataStorageClassServicePrototype extends AbstractTypeService<MetadataStorageClassObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.core.common.storage", "MetadataStorageClass");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.MetadataStorageClass"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      metadataStorageClass Checked object
    */
    public void check(MetadataStorageClassObject metadataStorageClass) {
        
        if ( metadataStorageClass == null ) {
            throwException("Cannot check null ?", new Object[]{"MetadataStorageClassObject"});
        }
        
        metadataStorageClass.fillDefaults();
        
        
        if ( nameToId(metadataStorageClass.getName(), metadataStorageClass.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "MetadataStorageClass", metadataStorageClass.toName() });
        }
        
    }
    
    /**
    * Create new MetadataStorageClassObject instance and fill default values
    * 
    * @return     Created object
    */
    public MetadataStorageClassObject create() {
        
        MetadataStorageClassObject metadataStorageClass = new MetadataStorageClassObject();
        metadataStorageClass.setService((MetadataStorageClassService) this);
        
        metadataStorageClass.fillDefaults();
        
        return metadataStorageClass;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public MetadataStorageClassObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select name from cw_core_mdata_sclasses where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"MetadataStorageClass", id});
        }
        
        MetadataStorageClassObject result = new MetadataStorageClassObject();
        
        result.setService((MetadataStorageClassService) this);
        result.setId(id);
        result.setName(statement.getString(1));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of MetadataStorageClassObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public MetadataStorageClassList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of MetadataStorageClassObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public MetadataStorageClassList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        MetadataStorageClassList result = new MetadataStorageClassList();
        
        
        Statement statement = executeQuery("select id, name from cw_core_mdata_sclasses"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            MetadataStorageClassObject object = new MetadataStorageClassObject();
        
            object.setService((MetadataStorageClassService) this);
            object.setId(statement.getBigDecimal(1));
            object.setName(statement.getString(2));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'Name' fields
    * 
    * @param      name Name
    * @return     Id of found record or null
    */
    public BigDecimal nameToId(java.lang.String name) {
        return nameToId(name, null);
    }
    
    /**
    * Find id of record by key 'Name' fields with id not equal skipId
    * 
    * @param      name   Name
    * @param      skipId Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal nameToId(java.lang.String name, BigDecimal skipId) {
        
        if ( name == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_mdata_sclasses where upper_name = ?", name.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_mdata_sclasses where upper_name = ? and id != ?", name.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'Name' fields or create new record with values set to given parameters
    * 
    * @param      name                Name
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal nameToId(java.lang.String name, boolean createNewIfNotFound) {
        
        BigDecimal id = nameToId(name, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        MetadataStorageClassObject object = create();
        object.setName(name);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'Name' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select name from cw_core_mdata_sclasses where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        MetadataStorageClassObject object = new MetadataStorageClassObject();
        object.setService((MetadataStorageClassService)this);
        object.setId(id);
        object.setName(statement.getString(1));
        
        statement.close();
        
        return object.toName();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToName(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      metadataStorageClassObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(MetadataStorageClassObject metadataStorageClassObject) {
        return metadataStorageClassObject.toName();
    }
    
    protected void saveAudit(TransactionAction transactionAction, MetadataStorageClassObject oldMetadataStorageClassObject, MetadataStorageClassObject newMetadataStorageClassObject) {
        
        MetadataStorageClassObject o = oldMetadataStorageClassObject != null ? oldMetadataStorageClassObject : new MetadataStorageClassObject();
        MetadataStorageClassObject n = newMetadataStorageClassObject != null ? newMetadataStorageClassObject : new MetadataStorageClassObject();
        
        
        executeUpdate("insert into a_cw_core_mdata_sclasses (scn, action_type, id , o_name, o_upper_name, n_name, n_upper_name) values ( ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getName(), o.getName() != null ? o.getName().toUpperCase() : null, n.getName(), n.getName() != null ? n.getName().toUpperCase() : null);
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      metadataStorageClass Saved object
    */
    public void save(MetadataStorageClassObject metadataStorageClass) {
        
        if ( metadataStorageClass == null ) {
            throwException("Cannot save NULL ?", new Object[]{"MetadataStorageClassObject"});
        }
        
        TransactionAction transactionAction;
        MetadataStorageClassObject oldMetadataStorageClass;
        if ( metadataStorageClass.getService() == null ) {
            metadataStorageClass.setService((MetadataStorageClassService)this);
        }
        
        check(metadataStorageClass);
        
        if ( metadataStorageClass.getId() == null ) {
        
            metadataStorageClass.setId(getObjectService().createObject(getTypeId()));
        
            oldMetadataStorageClass = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, metadataStorageClass.getId());
        
            executeUpdate("insert into cw_core_mdata_sclasses" 
                          + "( id, name, upper_name ) "
                          + "values"
                          + "(?, ?, ?)",
                          metadataStorageClass.getId(), metadataStorageClass.getName(), ( metadataStorageClass.getName() != null ? metadataStorageClass.getName().toUpperCase() : null ) );
        
        } else {
        
            oldMetadataStorageClass = load(metadataStorageClass.getId());
            if ( oldMetadataStorageClass.equals(metadataStorageClass) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, metadataStorageClass.getId());
        
            executeUpdate("update cw_core_mdata_sclasses set "
                          + "name = ?, upper_name = ? "
                          + "where id = ?",
                          metadataStorageClass.getName(), ( metadataStorageClass.getName() != null ? metadataStorageClass.getName().toUpperCase() : null ), metadataStorageClass.getId() );
        
        }
        
        saveAudit(transactionAction, oldMetadataStorageClass, metadataStorageClass);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        MetadataStorageClassObject oldMetadataStorageClassObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldMetadataStorageClassObject, null);
        
        getObjectService().deleteObject(id);
        
        executeUpdate("delete from cw_core_mdata_sclasses where id = ?", id);
        
    }
}
