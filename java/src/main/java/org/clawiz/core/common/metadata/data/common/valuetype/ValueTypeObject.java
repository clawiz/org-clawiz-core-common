/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.metadata.data.common.valuetype;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.system.database.metadata.DatabaseTableColumn;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.system.type.AbstractTypeService;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;

public class ValueTypeObject extends ValueTypeObjectPrototype {

    @Override
    public Class getJavaClass() {
        return BigDecimal.class;
    }

    @Override
    public DatabaseTableColumn.Type getDatabaseTableColumnType() {
        return DatabaseTableColumn.Type.NUMBER;
    }

    private static final BigDecimal valueTypeObjectDatabaseTableColumnPrecision = new BigDecimal(15);

    @Override
    public BigDecimal getDatabaseTableColumnPrecision() {
        return valueTypeObjectDatabaseTableColumnPrecision;
    }

    @Override
    public String getStatementValueTypeName() {
        return "BigDecimal";
    }

    public String getValueTypeFullClassName() {
        return getReferencedType() != null ? getReferencedType().getServicePackageName() + "." + getReferencedType().getJavaClassName() + "Object" : null;
    }

    protected <T extends AbstractTypeService> Class<T> getTypeServiceClass() {
        return null;
    }

    private static final ConcurrentHashMap<Class, Type> typeServiceClassesCache = new ConcurrentHashMap<>();

    @Override
    public void prepare(Session session) {
        super.prepare(session);

        if ( super.getReferencedType() == null && getTypeServiceClass() != null ) {
            Type type = typeServiceClassesCache.get(getTypeServiceClass());
            if ( type == null ) {
                type = session.getService(getTypeServiceClass()).getType();
                typeServiceClassesCache.put(getTypeServiceClass(), type);
            }
            setReferencedType(type);
        }

    }
}
