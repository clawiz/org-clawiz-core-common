package org.clawiz.core.common.storage.type;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;

public class TypeServicePrototype extends AbstractTypeService<TypeObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.core.common.storage", "Type");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Type"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      type Checked object
    */
    public void check(TypeObject type) {
        
        if ( type == null ) {
            throwException("Cannot check null ?", new Object[]{"TypeObject"});
        }
        
        type.fillDefaults();
        
        
        if ( packageNameToId(type.getPackageName(), type.getName(), type.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "Type", type.toPackageName() });
        }
        
        if ( packageJavaNameToId(type.getPackageName(), type.getJavaName(), type.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "Type", type.toPackageJavaName() });
        }
        
        if ( tableNameToId(type.getTableName(), type.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "Type", type.toTableName() });
        }
        
    }
    
    /**
    * Create new TypeObject instance and fill default values
    * 
    * @return     Created object
    */
    public TypeObject create() {
        
        TypeObject type = new TypeObject();
        type.setService((TypeService) this);
        
        type.fillDefaults();
        
        return type;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public TypeObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select package_name, name, java_name, description, table_name from cw_core_types where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"Type", id});
        }
        
        TypeObject result = new TypeObject();
        
        result.setService((TypeService) this);
        result.setId(id);
        result.setPackageName(statement.getString(1));
        result.setName(statement.getString(2));
        result.setJavaName(statement.getString(3));
        result.setDescription(statement.getString(4));
        result.setTableName(statement.getString(5));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of TypeObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public TypeList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of TypeObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public TypeList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        TypeList result = new TypeList();
        
        
        Statement statement = executeQuery("select id, package_name, name, java_name, description, table_name from cw_core_types"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            TypeObject object = new TypeObject();
        
            object.setService((TypeService) this);
            object.setId(statement.getBigDecimal(1));
            object.setPackageName(statement.getString(2));
            object.setName(statement.getString(3));
            object.setJavaName(statement.getString(4));
            object.setDescription(statement.getString(5));
            object.setTableName(statement.getString(6));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'PackageName' fields
    * 
    * @param      packageName PackageName
    * @param      name        Name
    * @return     Id of found record or null
    */
    public BigDecimal packageNameToId(java.lang.String packageName, java.lang.String name) {
        return packageNameToId(packageName, name, null);
    }
    
    /**
    * Find id of record by key 'PackageName' fields with id not equal skipId
    * 
    * @param      packageName PackageName
    * @param      name        Name
    * @param      skipId      Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal packageNameToId(java.lang.String packageName, java.lang.String name, BigDecimal skipId) {
        
        if ( packageName == null || name == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_types where upper_package_name = ? and upper_name = ?", packageName.toUpperCase(), name.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_types where upper_package_name = ? and upper_name = ? and id != ?", packageName.toUpperCase(), name.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'PackageName' fields or create new record with values set to given parameters
    * 
    * @param      packageName         PackageName
    * @param      name                Name
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal packageNameToId(java.lang.String packageName, java.lang.String name, boolean createNewIfNotFound) {
        
        BigDecimal id = packageNameToId(packageName, name, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        TypeObject object = create();
        object.setPackageName(packageName);
        object.setName(name);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'PackageName' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToPackageName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select package_name, name from cw_core_types where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        TypeObject object = new TypeObject();
        object.setService((TypeService)this);
        object.setId(id);
        object.setPackageName(statement.getString(1));
        object.setName(statement.getString(2));
        
        statement.close();
        
        return object.toPackageName();
    }
    
    /**
    * Find id of record by key 'PackageJavaName' fields
    * 
    * @param      packageName PackageName
    * @param      javaName    JavaName
    * @return     Id of found record or null
    */
    public BigDecimal packageJavaNameToId(java.lang.String packageName, java.lang.String javaName) {
        return packageJavaNameToId(packageName, javaName, null);
    }
    
    /**
    * Find id of record by key 'PackageJavaName' fields with id not equal skipId
    * 
    * @param      packageName PackageName
    * @param      javaName    JavaName
    * @param      skipId      Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal packageJavaNameToId(java.lang.String packageName, java.lang.String javaName, BigDecimal skipId) {
        
        if ( packageName == null || javaName == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_types where upper_package_name = ? and upper_java_name = ?", packageName.toUpperCase(), javaName.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_types where upper_package_name = ? and upper_java_name = ? and id != ?", packageName.toUpperCase(), javaName.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'PackageJavaName' fields or create new record with values set to given parameters
    * 
    * @param      packageName         PackageName
    * @param      javaName            JavaName
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal packageJavaNameToId(java.lang.String packageName, java.lang.String javaName, boolean createNewIfNotFound) {
        
        BigDecimal id = packageJavaNameToId(packageName, javaName, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        TypeObject object = create();
        object.setPackageName(packageName);
        object.setJavaName(javaName);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'PackageJavaName' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToPackageJavaName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select package_name, java_name from cw_core_types where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        TypeObject object = new TypeObject();
        object.setService((TypeService)this);
        object.setId(id);
        object.setPackageName(statement.getString(1));
        object.setJavaName(statement.getString(2));
        
        statement.close();
        
        return object.toPackageJavaName();
    }
    
    /**
    * Find id of record by key 'TableName' fields
    * 
    * @param      tableName TableName
    * @return     Id of found record or null
    */
    public BigDecimal tableNameToId(java.lang.String tableName) {
        return tableNameToId(tableName, null);
    }
    
    /**
    * Find id of record by key 'TableName' fields with id not equal skipId
    * 
    * @param      tableName TableName
    * @param      skipId    Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal tableNameToId(java.lang.String tableName, BigDecimal skipId) {
        
        if ( tableName == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_types where upper_table_name = ?", tableName.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_types where upper_table_name = ? and id != ?", tableName.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'TableName' fields or create new record with values set to given parameters
    * 
    * @param      tableName           TableName
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal tableNameToId(java.lang.String tableName, boolean createNewIfNotFound) {
        
        BigDecimal id = tableNameToId(tableName, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        TypeObject object = create();
        object.setTableName(tableName);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'TableName' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToTableName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select table_name from cw_core_types where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        TypeObject object = new TypeObject();
        object.setService((TypeService)this);
        object.setId(id);
        object.setTableName(statement.getString(1));
        
        statement.close();
        
        return object.toTableName();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToPackageName(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      typeObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(TypeObject typeObject) {
        return typeObject.toPackageName();
    }
    
    protected void saveAudit(TransactionAction transactionAction, TypeObject oldTypeObject, TypeObject newTypeObject) {
        
        TypeObject o = oldTypeObject != null ? oldTypeObject : new TypeObject();
        TypeObject n = newTypeObject != null ? newTypeObject : new TypeObject();
        
        
        executeUpdate("insert into a_cw_core_types (scn, action_type, id , o_package_name, o_upper_package_name, o_name, o_upper_name, o_java_name, o_upper_java_name, o_description, o_table_name, o_upper_table_name, n_package_name, n_upper_package_name, n_name, n_upper_name, n_java_name, n_upper_java_name, n_description, n_table_name, n_upper_table_name) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getPackageName(), o.getPackageName() != null ? o.getPackageName().toUpperCase() : null, o.getName(), o.getName() != null ? o.getName().toUpperCase() : null, o.getJavaName(), o.getJavaName() != null ? o.getJavaName().toUpperCase() : null, o.getDescription(), o.getTableName(), o.getTableName() != null ? o.getTableName().toUpperCase() : null, n.getPackageName(), n.getPackageName() != null ? n.getPackageName().toUpperCase() : null, n.getName(), n.getName() != null ? n.getName().toUpperCase() : null, n.getJavaName(), n.getJavaName() != null ? n.getJavaName().toUpperCase() : null, n.getDescription(), n.getTableName(), n.getTableName() != null ? n.getTableName().toUpperCase() : null);
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      type Saved object
    */
    public void save(TypeObject type) {
        
        if ( type == null ) {
            throwException("Cannot save NULL ?", new Object[]{"TypeObject"});
        }

        TransactionAction transactionAction;
        TypeObject oldType;
        if ( type.getService() == null ) {
            type.setService((TypeService)this);
        }
        
        check(type);

        if ( type.getPackageName() == null || type.getJavaName() == null ) {
            throwException("Type package name or java cannot be null");
        }
        if ( type.getId() == null ) {
        
            type.setId(getObjectService().createObject(getTypeId()));
        
            oldType = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, type.getId());
        
            executeUpdate("insert into cw_core_types" 
                          + "( id, package_name, upper_package_name, name, upper_name, java_name, upper_java_name, description, table_name, upper_table_name ) "
                          + "values"
                          + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                          type.getId(), type.getPackageName(), ( type.getPackageName() != null ? type.getPackageName().toUpperCase() : null ), type.getName(), ( type.getName() != null ? type.getName().toUpperCase() : null ), type.getJavaName(), ( type.getJavaName() != null ? type.getJavaName().toUpperCase() : null ), type.getDescription(), type.getTableName(), ( type.getTableName() != null ? type.getTableName().toUpperCase() : null ) );
        
        } else {
        
            oldType = load(type.getId());
            if ( oldType.equals(type) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, type.getId());
        
            executeUpdate("update cw_core_types set "
                          + "package_name = ?, upper_package_name = ?, name = ?, upper_name = ?, java_name = ?, upper_java_name = ?, description = ?, table_name = ?, upper_table_name = ? "
                          + "where id = ?",
                          type.getPackageName(), ( type.getPackageName() != null ? type.getPackageName().toUpperCase() : null ), type.getName(), ( type.getName() != null ? type.getName().toUpperCase() : null ), type.getJavaName(), ( type.getJavaName() != null ? type.getJavaName().toUpperCase() : null ), type.getDescription(), type.getTableName(), ( type.getTableName() != null ? type.getTableName().toUpperCase() : null ), type.getId() );
        
        }
        
        saveAudit(transactionAction, oldType, type);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        TypeObject oldTypeObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldTypeObject, null);
        
        getObjectService().deleteObject(id);
        
        executeUpdate("delete from cw_core_types where id = ?", id);
        
    }
}
