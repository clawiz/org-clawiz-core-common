package org.clawiz.core.common.metadata.data.common.enumeration;

import org.clawiz.core.common.CoreException;

public enum BooleanDefaultFalseEnumeration {

    TRUE, FALSE;

    
    public static BooleanDefaultFalseEnumeration toBooleanDefaultFalseEnumeration(String value) {
        if ( value == null ) {
            return null;
        }
        
        try {
            return BooleanDefaultFalseEnumeration.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new CoreException("Wrong BooleanDefaultFalseEnumeration value '?'", value);
        }
        
    }
    
    public static String toDescription(BooleanDefaultFalseEnumeration value) {
        if ( value == null ) {
            return null;
        }
        
        switch (value) {
            case TRUE : return "true";
            case FALSE: return "false";
        }
        
        return value.toString();
    }
}
