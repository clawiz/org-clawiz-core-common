package org.clawiz.core.common.storage.objectrelationtype;

import org.clawiz.core.common.system.type.AbstractTypeService;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.type.TypeService;
import java.math.BigDecimal;
import org.clawiz.core.common.utils.date.DateUtils;
import java.util.Date;
import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.session.transaction.TransactionAction;
import org.clawiz.core.common.system.session.transaction.TransactionActionType;

public class ObjectRelationTypeServicePrototype extends AbstractTypeService<ObjectRelationTypeObject> {
    
    private static BigDecimal _typeId;
    
    public void init() {
        
        super.init();
        
        if ( _typeId == null ) {
            _typeId = getService(TypeService.class).packageNameToId("org.clawiz.core.common.storage", "ObjectRelationType");
            if ( _typeId == null ) { 
                throwException("Type '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.ObjectRelationType"});
            }
        }
        
        setTypeId(_typeId);
    }
    
    /**
    * Fill object default values and check data consistency
    * 
    * @param      objectRelationType Checked object
    */
    public void check(ObjectRelationTypeObject objectRelationType) {
        
        if ( objectRelationType == null ) {
            throwException("Cannot check null ?", new Object[]{"ObjectRelationTypeObject"});
        }
        
        objectRelationType.fillDefaults();
        
        
        if ( nameToId(objectRelationType.getName(), objectRelationType.getId() ) != null ) {
            throwException("? '?' already exists", new Object[]{ "ObjectRelationType", objectRelationType.toName() });
        }
        
    }
    
    /**
    * Create new ObjectRelationTypeObject instance and fill default values
    * 
    * @return     Created object
    */
    public ObjectRelationTypeObject create() {
        
        ObjectRelationTypeObject objectRelationType = new ObjectRelationTypeObject();
        objectRelationType.setService((ObjectRelationTypeService) this);
        
        objectRelationType.fillDefaults();
        
        return objectRelationType;
    }
    
    /**
    * Load object from database by record id
    * 
    * @param      id Id of loaded record
    * @return     Loaded object
    */
    public ObjectRelationTypeObject load(BigDecimal id) {
        
        Statement statement = executeQuery("select name, description, reverse_type_id from cw_core_obj_rel_types where id = ?", id);
        if ( ! statement.next() ) { 
            statement.close();
            throwException("? with id ? not found in database", new Object[]{"ObjectRelationType", id});
        }
        
        ObjectRelationTypeObject result = new ObjectRelationTypeObject();
        
        result.setService((ObjectRelationTypeService) this);
        result.setId(id);
        result.setName(statement.getString(1));
        result.setDescription(statement.getString(2));
        result.setReverseTypeId(statement.getBigDecimal(3));
        
        statement.close();
        
        return result;
    }
    
    /**
    * Load list of ObjectRelationTypeObject from database
    * 
    * @param      whereClause Where part of SQL statement
    * @param      parameters  Where parameter values
    * @return     List of found objects
    */
    public ObjectRelationTypeList loadList(String whereClause, Object... parameters) {
        return loadOrderedList(whereClause, null, parameters);
    }
    
    /**
    * Load list of ObjectRelationTypeObject from database
    * 
    * @param      whereClause   Where part of SQL statement
    * @param      orderByClause Order by part of SQL statement
    * @param      parameters    Where parameter values
    * @return     List of found objects
    */
    public ObjectRelationTypeList loadOrderedList(String whereClause, String orderByClause, Object... parameters) {
        
        
        ObjectRelationTypeList result = new ObjectRelationTypeList();
        
        
        Statement statement = executeQuery("select id, name, description, reverse_type_id from cw_core_obj_rel_types"
                                                   + (whereClause   != null ? " where " + whereClause : "")
                                                   + (orderByClause != null ? " order by " + orderByClause : "")
                                                   , parameters);
        while ( statement.next() ) {
            ObjectRelationTypeObject object = new ObjectRelationTypeObject();
        
            object.setService((ObjectRelationTypeService) this);
            object.setId(statement.getBigDecimal(1));
            object.setName(statement.getString(2));
            object.setDescription(statement.getString(3));
            object.setReverseTypeId(statement.getBigDecimal(4));
        
            result.add(object);
        }
        statement.close();
        
        return result;
    }
    
    /**
    * Find id of record by key 'Name' fields
    * 
    * @param      name Name
    * @return     Id of found record or null
    */
    public BigDecimal nameToId(java.lang.String name) {
        return nameToId(name, null);
    }
    
    /**
    * Find id of record by key 'Name' fields with id not equal skipId
    * 
    * @param      name   Name
    * @param      skipId Skip records with this id
    * @return     Id of found record or null
    */
    public BigDecimal nameToId(java.lang.String name, BigDecimal skipId) {
        
        if ( name == null ) {
            return null;
        }
        
        if ( skipId == null ) { 
            return executeQueryBigDecimal("select id from cw_core_obj_rel_types where upper_name = ?", name.toUpperCase());
        } else {
            return executeQueryBigDecimal("select id from cw_core_obj_rel_types where upper_name = ? and id != ?", name.toUpperCase(), skipId);
        }
        
    }
    
    /**
    * Find record id of record by key 'Name' fields or create new record with values set to given parameters
    * 
    * @param      name                Name
    * @param      createNewIfNotFound Create new record with passed field values if not matched records exists in database
    * @return     Id of found record, null or id of new record if createNewIfNotFound set to true
    */
    public BigDecimal nameToId(java.lang.String name, boolean createNewIfNotFound) {
        
        BigDecimal id = nameToId(name, null);
        if ( id != null || ! createNewIfNotFound ) {
            return id;
        }
        
        ObjectRelationTypeObject object = create();
        object.setName(name);
        
        save(object);
        
        return object.getId();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'Name' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToName(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select name from cw_core_obj_rel_types where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        ObjectRelationTypeObject object = new ObjectRelationTypeObject();
        object.setService((ObjectRelationTypeService)this);
        object.setId(id);
        object.setName(statement.getString(1));
        
        statement.close();
        
        return object.toName();
    }
    
    /**
    * Load record by id and prepare concatenated string values of key 'ReverseType' fields
    * 
    * @param      id Id of record
    * @return     Concatenated string values of key fields
    */
    public String idToReverseType(BigDecimal id) {
        
        if ( id == null ) { return null; }
        
        Statement statement = executeQuery("select reverse_type_id from cw_core_obj_rel_types where id = ?", id);
        if ( ! statement.next()  ) { statement.close(); return null; }
        
        ObjectRelationTypeObject object = new ObjectRelationTypeObject();
        object.setService((ObjectRelationTypeService)this);
        object.setId(id);
        object.setReverseTypeId(statement.getBigDecimal(1));
        
        statement.close();
        
        return object.toReverseType();
    }
    
    /**
    * Load record by id and prepare toString() value
    * 
    * @param      id Id of record
    * @return     toString() value for record id
    */
    public String idToString(BigDecimal id) {
        return idToName(id);
    }
    
    /**
    * Return toString() value for object
    * 
    * @param      objectRelationTypeObject Object for toString transformation
    * @return     toString() value for object
    */
    public String objectToString(ObjectRelationTypeObject objectRelationTypeObject) {
        return objectRelationTypeObject.toName();
    }
    
    protected void saveAudit(TransactionAction transactionAction, ObjectRelationTypeObject oldObjectRelationTypeObject, ObjectRelationTypeObject newObjectRelationTypeObject) {
        
        ObjectRelationTypeObject o = oldObjectRelationTypeObject != null ? oldObjectRelationTypeObject : new ObjectRelationTypeObject();
        ObjectRelationTypeObject n = newObjectRelationTypeObject != null ? newObjectRelationTypeObject : new ObjectRelationTypeObject();
        
        
        executeUpdate("insert into a_cw_core_obj_rel_types (scn, action_type, id , o_name, o_upper_name, o_description, o_reverse_type_id, n_name, n_upper_name, n_description, n_reverse_type_id) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", transactionAction.getScn(), transactionAction.getType().toString(), o.getId() != null ? o.getId() : n.getId(), o.getName(), o.getName() != null ? o.getName().toUpperCase() : null, o.getDescription(), o.getReverseTypeId(), n.getName(), n.getName() != null ? n.getName().toUpperCase() : null, n.getDescription(), n.getReverseTypeId());
    }
    
    /**
    * Save object to database:
    *  - check data consistency with check() call
    *  - INSERT into database for newly created objects or
    *    UPDATE if object is loaded from existing database record
    * 
    * @param      objectRelationType Saved object
    */
    public void save(ObjectRelationTypeObject objectRelationType) {
        
        if ( objectRelationType == null ) {
            throwException("Cannot save NULL ?", new Object[]{"ObjectRelationTypeObject"});
        }
        
        TransactionAction transactionAction;
        ObjectRelationTypeObject oldObjectRelationType;
        if ( objectRelationType.getService() == null ) {
            objectRelationType.setService((ObjectRelationTypeService)this);
        }
        
        check(objectRelationType);
        
        if ( objectRelationType.getId() == null ) {
        
            objectRelationType.setId(getObjectService().createObject(getTypeId()));
        
            oldObjectRelationType = null;
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, objectRelationType.getId());
        
            executeUpdate("insert into cw_core_obj_rel_types" 
                          + "( id, name, upper_name, description, reverse_type_id ) "
                          + "values"
                          + "(?, ?, ?, ?, ?)",
                          objectRelationType.getId(), objectRelationType.getName(), ( objectRelationType.getName() != null ? objectRelationType.getName().toUpperCase() : null ), objectRelationType.getDescription(), objectRelationType.getReverseTypeId() );
        
            if ( objectRelationType.getReverseTypeId() != null ) { getObjectService().setLink(objectRelationType.getReverseTypeId(), objectRelationType.getId()); }
        
        } else {
        
            oldObjectRelationType = load(objectRelationType.getId());
            if ( oldObjectRelationType.equals(objectRelationType) ) { return; }
        
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, objectRelationType.getId());
        
            executeUpdate("update cw_core_obj_rel_types set "
                          + "name = ?, upper_name = ?, description = ?, reverse_type_id = ? "
                          + "where id = ?",
                          objectRelationType.getName(), ( objectRelationType.getName() != null ? objectRelationType.getName().toUpperCase() : null ), objectRelationType.getDescription(), objectRelationType.getReverseTypeId(), objectRelationType.getId() );
        
            getObjectService().changeLinkParent(oldObjectRelationType.getReverseTypeId(), objectRelationType.getReverseTypeId(), objectRelationType.getId());
        
        }
        
        saveAudit(transactionAction, oldObjectRelationType, objectRelationType);
        
    }
    
    /**
    * Delete object from database
    * 
    * @param      id ID of deleted object
    */
    public void delete(BigDecimal id) {
        
        ObjectRelationTypeObject oldObjectRelationTypeObject = load(id);
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        saveAudit(transactionAction, oldObjectRelationTypeObject, null);
        
        getObjectService().deleteObject(id);
        
        if (oldObjectRelationTypeObject.getReverseTypeId() != null) { getObjectService().deleteLink(oldObjectRelationTypeObject.getReverseTypeId(), id); }
        
        executeUpdate("delete from cw_core_obj_rel_types where id = ?", id);
        
    }
}
