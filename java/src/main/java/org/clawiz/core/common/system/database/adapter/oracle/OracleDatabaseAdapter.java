/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.database.adapter.oracle;

import org.clawiz.core.common.system.database.Statement;
import org.clawiz.core.common.system.database.adapter.DatabaseAdapter;
import org.clawiz.core.common.system.database.metadata.*;

import java.math.BigDecimal;
import java.sql.SQLException;


public class OracleDatabaseAdapter extends DatabaseAdapter {


    @Override
    protected String getStringConcatenationOperator() {
        return "||";
    }

    @Override
    protected String getColumnVarcharTypeName() {
        return "VARCHAR2";
    }

    @Override
    public boolean isTableExists(String name) {
        return executeQueryBigDecimal("select count(1) from user_tables where table_name = ?", name.toUpperCase()).intValue() > 0;
    }

    @Override
    protected DatabaseTable loadTableDefinition(String tableName) throws SQLException {
        String name = executeQueryString("select table_name from user_tables where table_name = ?", tableName.toUpperCase());
        return name != null ? new DatabaseTable(name) : null; 
    }

    @Override
    protected void loadTableColumns(DatabaseTable table) throws SQLException {

        Statement statement = executeQuery(
                "select column_name, data_type, data_precision, data_scale, char_length, nullable " +
                        "from user_tab_cols where table_name = ?", table.getName().toUpperCase());
        while ( statement.next() ) {
            DatabaseTableColumn column = new DatabaseTableColumn();
            table.addColumn(column);
            column.setName(statement.getString(1));

            DatabaseTableColumn.Type columnType = metadataColumnTypeToDatabaseColumnType(column, statement.getString(2));

            prepareDatabaseColumnType(column, statement.getString(2)
                    , statement.getResultSet().getInt(columnType != DatabaseTableColumn.Type.VARCHAR ? 3 : 5)
                    , statement.getResultSet().getInt(4)
            );

            column.setRequired("N".equals(statement.getString(6)));
        }
        statement.close();
    }

    @Override
    protected void loadTableIndexes(DatabaseTable table) throws SQLException {
        DatabaseTableIndex index = null;
        Statement statement = executeQuery("select i.index_name, i.uniqueness, c.column_name" +
                " from user_indexes i, user_ind_columns c " +
                " where c.index_name = i.index_name and c.table_name = ? " +
                " order by c.index_name, c.column_position", table.getName().toUpperCase());
        while ( statement.next() ) {

            String indexName = statement.getString(1);
            if ( index == null || ! index.getName().equalsIgnoreCase(indexName) ) {
                index = new DatabaseTableIndex();
                index.setName(indexName);
                index.setUnique("UNIQUE".equals(statement.getString(2)));
                table.addIndex(index);
            }
            index.addColumn(new DatabaseTableIndexColumn(statement.getString(3)));
        }
        statement.close();

    }

    @Override
    protected String getTableColumnTypeDDL(DatabaseTableColumn column) {

        String ddl;

        if ( column.getType() == DatabaseTableColumn.Type.VARCHAR ) {
            ddl = "VARCHAR2(" + column.getPrecision() + " char)";
        } else {
            ddl = super.getTableColumnTypeDDL(column);
        }
        if ( column.isRequired() ) {
            ddl += " not null";
        }
        return ddl;
    }

    @Override
    public void implementTable(DatabaseTable table) {
        super.implementTable(table);

        if ( table.getComment() != null ) {
            executeUpdate("comment on table " + table.getName() + " is '" + table.getComment() + "'");
        }
        for (DatabaseTableColumn column : table.getColumns() ) {
            if ( column.getComment() != null ) {
                executeUpdate("comment on column " + table.getName() + "." + column.getName() + " is '" + table.getComment() + "'");
            }
        }
    }

    @Override
    public boolean isSequenceExists(DatabaseSequence sequence) {
        return executeQueryBigDecimal("select count(sequence_name) from user_sequences where sequence_name = ?", sequence.getName().toUpperCase()).intValue() > 0;
    }

    @Override
    public void createSequence(DatabaseSequence sequence) {
        executeUpdate("create sequence " + sequence.getName() + " start with " + sequence.getStartWith().toString());
    }

    @Override
    public BigDecimal getSequenceNextValue(String sequenceName) {
        return executeQueryBigDecimal("select " + sequenceName + ".nextval from dual");
    }
}
