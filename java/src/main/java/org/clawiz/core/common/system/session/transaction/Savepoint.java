/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.session.transaction;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: abdrashitovta
 * Date: 31.07.13
 * Time: 15?2
 * To change this template use File | Settings | File Templates.
 */
public class Savepoint {

    Transaction transaction;
    String      name;
    BigDecimal  scn;

    java.sql.Savepoint javaSqlSavepoint;


    public Savepoint(Transaction transaction, String name) {
        this.transaction = transaction;
        this.name = name;
    }


    public Transaction getTransaction() {
        return transaction;
    }

    public String getName() {
        return name;
    }

    public java.sql.Savepoint getJavaSqlSavepoint() {
        return javaSqlSavepoint;
    }

    public void setJavaSqlSavepoint(java.sql.Savepoint javaSqlSavepoint) {
        this.javaSqlSavepoint = javaSqlSavepoint;
    }

    public BigDecimal getScn() {
        return scn;
    }

    public void setScn(BigDecimal scn) {
        this.scn = scn;
    }
}
