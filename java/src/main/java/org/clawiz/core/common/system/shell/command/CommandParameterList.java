/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.shell.command;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * (C) Clawiz
 */
public class CommandParameterList implements Iterable<CommandParameter> {

    ArrayList<CommandParameter> parameters;

    public CommandParameterList() {
        this.parameters = new ArrayList<CommandParameter>();
    }

    public void parseArgs(String argv[]) throws Exception {

        for (String arg : argv) {

            int i = arg.indexOf("=");
            String name;
            String value = null;
            if (i == -1) {
                name = arg;
            } else {
                name = arg.substring(0, i);

                if (i < arg.length() + 1) {
                    value = arg.substring(i + 1);
                }
            }

            name = name.toUpperCase();
            CommandParameter p = new CommandParameter();
            if (name.equals("XXX")) {

            } else {
                p.setName(name);
                p.setValue(value);
            }
            p.validate();

            parameters.add(p);

        }

    }

    public CommandParameter getParameter(String name) {
        for (CommandParameter p : parameters) {
            if (p.getName() != null && p.getName().equalsIgnoreCase(name)) {
                return p;
            }
        }
        return null;
    }

    public String getValue(String name) {
        CommandParameter p = getParameter(name);
        if (p == null) {
            return null;
        }
        return p.getValue();
    }

    public ArrayList<CommandParameter> toArray() {
        return parameters;
    }

    @Override
    public Iterator<CommandParameter> iterator() {
        return parameters.iterator();
    }

    @Override
    public void forEach(Consumer<? super CommandParameter> action) {
        parameters.forEach(action);
    }

    @Override
    public Spliterator<CommandParameter> spliterator() {
        return parameters.spliterator();
    }
}
