/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.shell;

import org.clawiz.core.common.Core;
import org.clawiz.core.common.system.config.xml.XMLConfig;
import org.clawiz.core.common.system.logger.ConsoleLogger;
import org.clawiz.core.common.system.logger.Logger;
import org.clawiz.core.common.system.module.Module;
import org.clawiz.core.common.system.shell.command.AbstractCommand;
import org.clawiz.core.common.system.shell.command.CommandParameterList;
import org.clawiz.core.common.system.session.Session;

import java.io.File;

/**
 * (c) Clawiz
 */

public class Main {

    private static Logger log;

    private static final String PARAMETER_CONFIG  = "config";
    private static final String PARAMETER_COMMAND = "command";

    public static void printHelp() {
        log.info("Usage :    shell PARAMETER=VALUE");
        log.info("");
        log.info("Parameter            Description");
        log.info("------------------------------------------------------------");
        log.info("CONFIG               Configuration file name");
        log.info("COMMAND              Command service name");
    }


    public static void main(String[] argv) throws Exception {

        try {
            log = new ConsoleLogger();
            
            log.info("Clawiz shell");
            log.info("");

            log.info("Parse parameters ...");

            CommandParameterList parameters = new CommandParameterList();

            try {
                parameters.parseArgs(argv);
            } catch (Exception e) {
                log.error("Parameters error : " + e.getMessage());
                log.info("");
                printHelp();
                return;
            }


            log.info("Parameters parsed");

            String configName = parameters.getValue(PARAMETER_CONFIG);
            if ( configName == null) {
//                configName = "config" + File.separator + "server.properties";;
                configName = "config" + File.separator + "server.xml";
            }

            XMLConfig config = new XMLConfig();
            config.load(configName, "utf-8");

            log.info("Config loaded");

            Core.setConfig(config);
            Core.init();

            Session session = Core.newSession();

            log.info("Session created : " + session.getKey());

            String command = parameters.getValue(PARAMETER_COMMAND);
            if (command == null) {
                log.error("Command not defined");
                log.info("");
                printHelp();
                return;
            }

            log.info("Start execute command " + command);

            Class commandClazz = Core.getClassByName(command);
            ((AbstractCommand) session.getService(commandClazz)).run(parameters);

            for(Module module : Core.getModules() ) {
                module.stop();
            }

            Core.newSession().getConnection().getDatabaseAdapter().shutdownDatabase();

        } catch (Exception e) {
            System.out.println("Main exception :");
            e.printStackTrace();
        }

        log.info("Main thread finished");

    }




}
