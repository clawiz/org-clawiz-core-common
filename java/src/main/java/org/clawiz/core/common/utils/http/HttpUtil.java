package org.clawiz.core.common.utils.http;

import org.clawiz.core.common.system.service.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpUtil extends Service {


    public String getString(String url) {
        HttpURLConnection connection = null;
        String            result     = null;

        StringBuilder content;
        try {
            URL _url = new URL(url);

            connection = (HttpURLConnection) _url.openConnection();


            connection.setRequestMethod("GET");

            try (BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()))) {

                String line;
                content = new StringBuilder();

                while ((line = in.readLine()) != null) {
                    content.append(line);
                    content.append(System.lineSeparator());
                }
            }

            result = content.toString();
        } catch (IOException e) {
            throwException("Exception on make HTTP GET call ? : ?", url, e.getMessage(), e);
        } finally {
            connection.disconnect();
        }

        return result;
    }

}
