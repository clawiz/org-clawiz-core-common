/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.metadata.node.loader.data;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeList;
import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.core.common.utils.reflection.ReflectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.util.ArrayList;

public class MetadataLoaderNodeClassField {

    MetadataLoaderNodeClass    nodeStructure;
    Field                      field;
    Method                     getter;
    Method                     setter;

    boolean                    required;

    boolean                    attribute;
    boolean                    element;
    boolean                    reference;
    boolean                    array;
    boolean                    enumeration;

    ExchangeAttribute nodeAttribute;
    ExchangeElement nodeElement;
    ExchangeReference nodeReference;


    private enum ValueType {
        INTEGER, BOOLEAN, STRING, BIGDECIMAL
    }

    private ValueType valueType;
    private Class     valueTypeClass;

    public MetadataLoaderNodeClass getNodeStructure() {
        return nodeStructure;
    }

    public void setNodeStructure(MetadataLoaderNodeClass nodeStructure) {
        this.nodeStructure = nodeStructure;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public String getName() {
        return field.getName();
    }

    public Class getType() {
        return field.getType();
    }

    public Method getGetter() {
        return getter;
    }

    public void setGetter(Method getter) {
        this.getter = getter;
    }

    public Method getSetter() {
        return setter;
    }

    public void setSetter(Method setter) {
        this.setter = setter;
    }

    public boolean isArray() {
        return array;
    }

    public boolean isElement() {
        return element;
    }

    public boolean isAttribute() {
        return attribute;
    }

    public boolean isReference() {
        return reference;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public ValueType getValueType() {
        return valueType;
    }

    public void setValueType(ValueType valueType) {
        this.valueType = valueType;
    }

    public Class getValueTypeClass() {
        return valueTypeClass;
    }

    public void setValueTypeClass(Class valueTypeClass) {
        this.valueTypeClass = valueTypeClass;
    }


    public boolean isEnumeration() {
        return enumeration;
    }

    public void setEnumeration(boolean enumeration) {
        this.enumeration = enumeration;
    }

    private void throwException(Session session, String code, Object... parameters ) {
        throw new CoreException(session, code, parameters);
    }


    protected void prepareAttributeField(Session session, ExchangeAttribute annotation) {

        attribute = true;
        required  = annotation.required();

        if ( field.getType() == Integer.TYPE) {
            setValueType(ValueType.INTEGER);
        } else if ( field.getType() == BigDecimal.class) {
            setValueType(ValueType.BIGDECIMAL);
        } else if ( field.getType() == String.class ) {
            setValueType(ValueType.STRING);
        } else if ( field.getType() == Boolean.class || field.getType() == boolean.class) {
            setValueType(ValueType.BOOLEAN);
        } else if ( Enum.class.isAssignableFrom(field.getType())) {
            setEnumeration(true);
        } else {
            throwException(session, "Wrong node ? field '?' type ?", nodeStructure.nodeClass.getName(), field.getName(), field.getType().getName());
        }
        setValueTypeClass(field.getType());

    }

    protected void prepareElementField(Session session, ExchangeElement annotation) {
        element   = true;
        required  = annotation.required();
        array     = MetadataNodeList.class.isAssignableFrom(field.getType());
        setValueTypeClass(field.getType());
    }

    protected void prepareReferenceField(Session session, ExchangeReference annotation) {
        reference = true;
        required  = annotation.required();
        array     = MetadataNodeList.class.isAssignableFrom(field.getType());
    }

    public void prepare(Session session) {

        nodeAttribute = field.getAnnotation(ExchangeAttribute.class);
        nodeElement   = field.getAnnotation(ExchangeElement.class);
        nodeReference = field.getAnnotation(ExchangeReference.class);

        if (
                (
                    (nodeAttribute != null ? 1 : 0)
                    + (nodeElement != null ? 1 : 0)
                    + (nodeReference != null ? 1 : 0)
                )
                > 1 ) {
            throwException(session, "Node ? field ? ? can have only one of (MetadataNodeAttribute, MetadataNodeElement, MetadataNodeReference) annotation"
                    ,nodeStructure.nodeClass.getName(), field.getName());
        }

        if ( nodeAttribute != null ) {
            prepareAttributeField(session, nodeAttribute);
        } else if ( nodeElement != null ) {
            prepareElementField(session, nodeElement);
        } else if ( nodeReference != null ) {
            prepareReferenceField(session, nodeReference);
        } else {
            throwException(session, "Node ? field ? ? must have at least on of (MetadataNodeAttribute, MetadataNodeElement, MetadataNodeReference) annotation"
                    ,nodeStructure.nodeClass.getName(), field.getName());

        }

        String getterPrefix = valueType == ValueType.BOOLEAN ? "is" : "get" ;
        String getterName   = getterPrefix + StringUtils.toUpperFirstChar(field.getName());
        String setterName   = "set"        + StringUtils.toUpperFirstChar(field.getName());

        getter = ReflectionUtils.findMethod(nodeStructure.getNodeClass(), getterName);
        setter = ReflectionUtils.findMethod(nodeStructure.getNodeClass(), setterName, field.getType());
    }

    private void handleGetValueException(Exception e, MetadataNode node) {
        throw new CoreException("Exception on call node ? field ? getter ? for node ? : ?", nodeStructure.nodeClass.getName(), field.getName(), getter != null ? getter.getName() : null, node.getFullName(), e.getMessage(), e);
    }

    private void handleSetValueException(Exception e, MetadataNode node) {
        throw new CoreException("Exception on call node ? field ? setter ? for node ? : ?", nodeStructure.nodeClass.getName(), field.getName(), setter != null ? setter.getName() : null, node.getFullName(), e.getMessage(), e);
    }


    public Object getObject(MetadataNode node) {
        try {
            if ( getter == null ) {
                throw new CoreException("Get method nod defined for metadata node field ?", getName());
            }
            return getter.invoke(node);
        } catch (Exception e) {
            handleGetValueException(e, node);
        }
        return null;
    }

    public void setObject(MetadataNode node, Object value) {
        try {
            if ( setter == null ) {
                throw new CoreException("Set method nod defined for metadata node field ?", getName());
            }
            setter.invoke(node, value);
        } catch (Exception e) {
            handleSetValueException(e, node);
        }
    }

    public int getInt(MetadataNode node) {
        Object object = getObject(node);
        return object != null ? ((Integer) object).intValue() : 0;
    }

    public BigDecimal getBigDecimal(MetadataNode node) {
        Object object = getObject(node);
        return object != null ? ((BigDecimal) object) : null;
    }

    public String getString(MetadataNode node) {
        Object object = getObject(node);
        return object != null ? ((String) object) : null;
    }

    public Boolean getBoolean(MetadataNode node) {
        Object object = getObject(node);
        return object != null ? (Boolean) object : null;
    }

    public void setInt(MetadataNode node, int value) {
        setObject(node, new Integer(value));
    }

    public void setBigDecimal(MetadataNode node, BigDecimal value) {
        setObject(node, value);
    }

    public void setBoolean(MetadataNode node, boolean value) {
        setObject(node, new Boolean(value));
    }


    public String getAsString(MetadataNode node) {

        if ( enumeration ) {
            Object object = getObject(node);
            return object != null ? object.toString() : null;
        }

        switch (valueType) {
            case INTEGER: return "" + getInt(node);
            case BIGDECIMAL: BigDecimal value = getBigDecimal(node); return value != null ? value.toString() : null;
            case STRING: return  getString(node);
            case BOOLEAN:
                Boolean result = getBoolean(node);
                return result != null ? result.toString() : null;
            default: throw new CoreException("Cannot get as string value for node ? field ? of type ?", nodeStructure.nodeClass.getName(), field.getName(), field.getType().getName());
        }


    }

    public MetadataNode getAsNode(MetadataNode node) {
        return (MetadataNode) getObject(node);
    }

    public MetadataNodeList<MetadataNode> getAsNodeList(MetadataNode node) {
        return (MetadataNodeList) getObject(node);
    }

    public void setFromString(MetadataNode node, String value) {

        if ( value == null ) {
            throw new CoreException("Cannot set NULL string value for node ? field ? of type ?", nodeStructure.nodeClass.getName(), field.getName(), field.getType().getName());
        }

        if ( enumeration ) {
            setObject(node, Enum.valueOf((Class<Enum>) valueTypeClass, value));
            return;
        }

        switch (valueType) {
            case INTEGER: BigDecimal v = StringUtils.toBigDecimal(value); setInt(node, v != null ? v.intValue() : 0); break;
            case BIGDECIMAL: setBigDecimal(node, StringUtils.toBigDecimal(value)); break;
            case STRING: setObject(node, value); break;
            case BOOLEAN: setBoolean(node, StringUtils.toBoolean(value)); break;
            default: throw new CoreException("Cannot set as string value for node ? field ? of type ?", nodeStructure.nodeClass.getName(), field.getName(), field.getType().getName());
        }

    }

}
