/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.file.writer;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.generator.abstractgenerator.writer.AbstractWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * (c) Clawiz
 */
public class TextFileWriter extends FileWriter {

    String encoding;


    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    @Override
    public void open() {
        try {
            outputStreamWriter = new OutputStreamWriter( new FileOutputStream( file ), encoding );
        } catch (Exception e) {
            throw new CoreException("Exception on open file writer for component '?' file '?' : ?", new Object[]{getComponent().getName(), file.getPath(), e.getMessage()}, e);
        }
    }

    public void write(String line) {
        try {
            outputStreamWriter.write(line);
        } catch (IOException e) {
            throw new CoreException("IOException exception on write component '?' to file '?' : \n?", new Object[]{getComponent().getName(), file.getPath(),  e.getMessage()}, e);
        }
    }

}
