/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.query.element.column;

import org.clawiz.core.common.query.element.AbstractQueryElement;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;

public abstract class AbstractQueryColumn extends AbstractQueryElement {

    private String            name;
    private int               index;

    public String getName() {
        return name;
    }

    public AbstractQueryColumn name(String name) {
        getQuery().select().changeCachedColumnName(this, name);
        this.name = name;
        return this;
    }

    public int getIndex() {
        return index;
    }

    public AbstractQueryColumn index(int index) {
        getQuery().select().changeCachedColumnIndex(this, new Integer(index));
        this.index = index;
        return this;
    }

    abstract public String getExpression();

    public QueryColumnType column(TypeModel model) {
        return getQuery().select().column(model);
    }

    public QueryColumnType column(TypeModel model, String name) {
        return column(model).name(name);
    }

    public QueryColumnTypeField column(TypeFieldModel model) {
        return getQuery().select().column(model);
    }

    public QueryColumnTypeField column(TypeFieldModel model, String name) {
        return column(model).name(name);
    }

    public QueryColumnTypeRecordId column(TypeRecordIdModel model) {
        return getQuery().select().column(model);
    }

    public QueryColumnTypeRecordId column(TypeRecordIdModel model, String name) {
        return column(model).name(name);
    }


}
