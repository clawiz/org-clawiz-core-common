package org.clawiz.core.common.storage.language;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;

public class LanguageModelPrototype extends TypeModel {
    
    private static TypeField CODE_FIELD;
    
    private static TypeField NAME_FIELD;
    
    private static TypeField DESCRIPTION_FIELD;
    
    private static Type type;
    
    private LanguageService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public LanguageService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(LanguageService.class).getType();
        
        
        CODE_FIELD = type.getFields().get("Code");
        if ( CODE_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Language.Code"}); }
        
        NAME_FIELD = type.getFields().get("Name");
        if ( NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Language.Name"}); }
        
        DESCRIPTION_FIELD = type.getFields().get("Description");
        if ( DESCRIPTION_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Language.Description"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _code;
    
    public TypeFieldModel code() {
        
        if ( _code != null ) {
            return _code;
        }
        
        _code = new TypeFieldModel(this, CODE_FIELD);
        return _code;
        
    }
    
    private TypeFieldModel _name;
    
    public TypeFieldModel name() {
        
        if ( _name != null ) {
            return _name;
        }
        
        _name = new TypeFieldModel(this, NAME_FIELD);
        return _name;
        
    }
    
    private TypeFieldModel _description;
    
    public TypeFieldModel description() {
        
        if ( _description != null ) {
            return _description;
        }
        
        _description = new TypeFieldModel(this, DESCRIPTION_FIELD);
        return _description;
        
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "CODE" : return code();
        case "NAME" : return name();
        case "DESCRIPTION" : return description();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
