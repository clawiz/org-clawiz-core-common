/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.query.element.filter.condition;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class QueryFilterConditionList implements Iterable<AbstractQueryFilterCondition> {

    ArrayList<AbstractQueryFilterCondition> conditions = new ArrayList<>();

    public void add(AbstractQueryFilterCondition condition) {
        conditions.add(condition);
    }

    public AbstractQueryFilterCondition get(int index) {
        return conditions.get(index);
    }

    public int size() {
        return conditions.size();
    }

    @Override
    public Iterator<AbstractQueryFilterCondition> iterator() {
        return conditions.iterator();
    }

    @Override
    public void forEach(Consumer<? super AbstractQueryFilterCondition> action) {
        conditions.forEach(action);
    }

    @Override
    public Spliterator<AbstractQueryFilterCondition> spliterator() {
        return conditions.spliterator();
    }


}
