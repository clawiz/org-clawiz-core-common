/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.database.metadata;

import java.util.ArrayList;
import java.util.Iterator;

public class DatabaseTableIndexList implements Iterable<DatabaseTableIndex> {

    DatabaseTable                  table;
    ArrayList<DatabaseTableIndex>  indexes = new ArrayList();

    public DatabaseTableIndexList(DatabaseTable table) {
        this.table = table;
    }

    public void add(DatabaseTableIndex tableIndex) {
        indexes.add(tableIndex);
        tableIndex.setTable(table);
    }

    public int size() {
        return indexes.size();
    }

    public DatabaseTableIndex get(int index) {
        return indexes.get(index);
    }

    public DatabaseTableIndex get(String name) {
        if ( name == null ) {
            return null;
        }
        for (DatabaseTableIndex index : indexes ) {
            if ( name.equalsIgnoreCase(index.getName())) {
                return index;
            }
        }
        return null;
    }

    @Override
    public Iterator<DatabaseTableIndex> iterator() {
        return indexes.iterator();
    }
}
