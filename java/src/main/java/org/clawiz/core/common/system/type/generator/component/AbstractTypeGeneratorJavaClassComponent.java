/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.type.generator.component;

import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.core.common.system.generator.java.component.AbstractJavaClassComponent;
import org.clawiz.core.common.metadata.data.type.field.TypeFieldList;
import org.clawiz.core.common.metadata.data.type.key.TypeKeyList;
import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.system.type.generator.GenerateTypeContext;
import org.clawiz.core.common.system.type.generator.TypeGenerator;

import java.io.File;

public class AbstractTypeGeneratorJavaClassComponent extends AbstractJavaClassComponent {


    private TypeGenerator _generator;
    private GenerateTypeContext context;

    public TypeGenerator getGenerator() {
        return _generator;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public void setGenerator(AbstractGenerator generator) {
        super.setGenerator(generator);
        _generator = (TypeGenerator) super.getGenerator();

        setContext(getGenerator().getContext());
        setPackageName(getContext().getPackageName() + "." + getType().getJavaClassName().toLowerCase());

        if ( getDestinationPath() != null ) {
            setDestinationPath(getContext().getDestinationPath() + File.separator + "main" + File.separator + "java" + File.separator + getPackageName().replace('.', File.separatorChar));
        }
    }

    public GenerateTypeContext getContext() {
        return context;
    }

    public void setContext(GenerateTypeContext context) {
        this.context = context;
    }

    public Type getType() {
        return getContext().getType();
    }

    public TypeFieldList getFields() {
        return getType().getFields();
    }

    public TypeKeyList getKeys() {
        return getType().getKeys();
    }

    public String getDataObjectClassName() {
        return getGenerator().getDataObjectClassName();
    }

    public String getDataObjectListClassName() {
        return getGenerator().getDataObjectListClassName();
    }

    public String getServiceClassName() {
        return getGenerator().getServiceClassName();
    }

    public String getModelClassName() {
        return getGenerator().getModelClassName();
    }

}
