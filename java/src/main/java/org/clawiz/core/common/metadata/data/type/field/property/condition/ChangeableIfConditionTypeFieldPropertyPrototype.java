package org.clawiz.core.common.metadata.data.type.field.property.condition;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ChangeableIfConditionTypeFieldPropertyPrototype extends org.clawiz.core.common.metadata.data.type.field.property.condition.AbstractConditionTypeFieldProperty {
    
    public ChangeableIfConditionTypeFieldProperty withName(String value) {
        setName(value);
        return (ChangeableIfConditionTypeFieldProperty) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
