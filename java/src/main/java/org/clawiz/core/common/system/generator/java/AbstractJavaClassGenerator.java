/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.java;

import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: abdrashitovta
 * Date: 26.05.13
 * Time: 15?5
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractJavaClassGenerator extends AbstractGenerator {

    String packageName;
    String rootDestinationPath;

    public String getRootDestinationPath() {
        return rootDestinationPath != null ? rootDestinationPath : getDestinationPath();
    }

    public void setRootDestinationPath(String rootDestinationPath) {
        this.rootDestinationPath = rootDestinationPath;
        setPackageName(this.packageName);
    }

    public String getPackageName() {
        return packageName;
    }


    private boolean protectedDestinationPathMode = false;

    @Override
    public void setDestinationPath(String destinationPath) {
        if ( ! protectedDestinationPathMode ) {
            throwException("Call setDestinationPath of AbstractJavaClassGenerator is prohibited. Use setRootDestinationPath instead");
        }
        super.setDestinationPath(destinationPath);
        protectedDestinationPathMode = false;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
        if ( getRootDestinationPath() == null ) {
            return;
        }
        protectedDestinationPathMode = true;
        if ( packageName != null ) {
            setDestinationPath(getRootDestinationPath() + File.separator + "main" + File.separator + "java" + File.separator +
                    getPackageName().replace('.', File.separatorChar));
        } else {
            setDestinationPath(null);
        }
    }

}
