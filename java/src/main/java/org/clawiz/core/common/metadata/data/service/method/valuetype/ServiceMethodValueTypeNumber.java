package org.clawiz.core.common.metadata.data.service.method.valuetype;


import java.math.BigDecimal;

public class ServiceMethodValueTypeNumber extends ServiceMethodValueTypeNumberPrototype {

    @Override
    public Class getJavaClass() {
        return BigDecimal.class;
    }

}
