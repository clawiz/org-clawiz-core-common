package org.clawiz.core.common.storage.typefield;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.storage.typefield.TypeFieldService;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;
import org.clawiz.core.common.system.service.NotInitializeService;

public class TypeFieldModelPrototype extends TypeModel {
    
    private static TypeField TYPE_ID_FIELD;
    
    private static TypeField ORDER_NO_FIELD;
    
    private static TypeField NAME_FIELD;
    
    private static TypeField DESCRIPTION_FIELD;
    
    private static TypeField COLUMN_NAME_FIELD;
    
    private static TypeField VALUE_TYPE_FIELD;
    
    private static TypeField REFERENCED_TYPE_ID_FIELD;
    
    private static TypeField VALUE_PRECISION_FIELD;
    
    private static TypeField VALUE_SCALE_FIELD;
    
    private static TypeField DEFAULT_VALUE_DEFINITION_FIELD;
    
    private static TypeField CALCULATE_EXPRESSION_DEFINITION_FIELD;
    
    private static TypeField VALIDATE_DEFINITION_FIELD;
    
    private static TypeField AUTO_UPPERCASE_FIELD;
    
    private static TypeField REQUIRED_FIELD;
    
    private static Type type;
    
    private TypeFieldService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public TypeFieldService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(TypeFieldService.class).getType();
        
        TypeFieldService typeFieldService = getService(TypeFieldService.class);
        
        TYPE_ID_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeField.Type");
        if ( TYPE_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeField.Type"}); }
        
        ORDER_NO_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeField.OrderNo");
        if ( ORDER_NO_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeField.OrderNo"}); }
        
        NAME_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeField.Name");
        if ( NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeField.Name"}); }
        
        DESCRIPTION_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeField.Description");
        if ( DESCRIPTION_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeField.Description"}); }
        
        COLUMN_NAME_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeField.ColumnName");
        if ( COLUMN_NAME_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeField.ColumnName"}); }
        
        VALUE_TYPE_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeField.ValueType");
        if ( VALUE_TYPE_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeField.ValueType"}); }
        
        REFERENCED_TYPE_ID_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeField.ReferencedType");
        if ( REFERENCED_TYPE_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeField.ReferencedType"}); }
        
        VALUE_PRECISION_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeField.ValuePrecision");
        if ( VALUE_PRECISION_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeField.ValuePrecision"}); }
        
        VALUE_SCALE_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeField.ValueScale");
        if ( VALUE_SCALE_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeField.ValueScale"}); }
        
        DEFAULT_VALUE_DEFINITION_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeField.DefaultValueDefinition");
        if ( DEFAULT_VALUE_DEFINITION_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeField.DefaultValueDefinition"}); }
        
        CALCULATE_EXPRESSION_DEFINITION_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeField.CalculateExpressionDefinition");
        if ( CALCULATE_EXPRESSION_DEFINITION_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeField.CalculateExpressionDefinition"}); }
        
        VALIDATE_DEFINITION_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeField.ValidateDefinition");
        if ( VALIDATE_DEFINITION_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeField.ValidateDefinition"}); }
        
        AUTO_UPPERCASE_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeField.AutoUppercase");
        if ( AUTO_UPPERCASE_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeField.AutoUppercase"}); }
        
        REQUIRED_FIELD = typeFieldService.getByFullName("org.clawiz.core.common.storage.TypeField.Required");
        if ( REQUIRED_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.TypeField.Required"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _typeId;
    
    public TypeFieldModel typeId() {
        
        if ( _typeId != null ) {
            return _typeId;
        }
        
        _typeId = new TypeFieldModel(this, TYPE_ID_FIELD);
        return _typeId;
        
    }
    
    private TypeFieldModel _orderNo;
    
    public TypeFieldModel orderNo() {
        
        if ( _orderNo != null ) {
            return _orderNo;
        }
        
        _orderNo = new TypeFieldModel(this, ORDER_NO_FIELD);
        return _orderNo;
        
    }
    
    private TypeFieldModel _name;
    
    public TypeFieldModel name() {
        
        if ( _name != null ) {
            return _name;
        }
        
        _name = new TypeFieldModel(this, NAME_FIELD);
        return _name;
        
    }
    
    private TypeFieldModel _description;
    
    public TypeFieldModel description() {
        
        if ( _description != null ) {
            return _description;
        }
        
        _description = new TypeFieldModel(this, DESCRIPTION_FIELD);
        return _description;
        
    }
    
    private TypeFieldModel _columnName;
    
    public TypeFieldModel columnName() {
        
        if ( _columnName != null ) {
            return _columnName;
        }
        
        _columnName = new TypeFieldModel(this, COLUMN_NAME_FIELD);
        return _columnName;
        
    }
    
    private TypeFieldModel _valueType;
    
    public TypeFieldModel valueType() {
        
        if ( _valueType != null ) {
            return _valueType;
        }
        
        _valueType = new TypeFieldModel(this, VALUE_TYPE_FIELD);
        return _valueType;
        
    }
    
    private TypeFieldModel _referencedTypeId;
    
    public TypeFieldModel referencedTypeId() {
        
        if ( _referencedTypeId != null ) {
            return _referencedTypeId;
        }
        
        _referencedTypeId = new TypeFieldModel(this, REFERENCED_TYPE_ID_FIELD);
        return _referencedTypeId;
        
    }
    
    private TypeFieldModel _valuePrecision;
    
    public TypeFieldModel valuePrecision() {
        
        if ( _valuePrecision != null ) {
            return _valuePrecision;
        }
        
        _valuePrecision = new TypeFieldModel(this, VALUE_PRECISION_FIELD);
        return _valuePrecision;
        
    }
    
    private TypeFieldModel _valueScale;
    
    public TypeFieldModel valueScale() {
        
        if ( _valueScale != null ) {
            return _valueScale;
        }
        
        _valueScale = new TypeFieldModel(this, VALUE_SCALE_FIELD);
        return _valueScale;
        
    }
    
    private TypeFieldModel _defaultValueDefinition;
    
    public TypeFieldModel defaultValueDefinition() {
        
        if ( _defaultValueDefinition != null ) {
            return _defaultValueDefinition;
        }
        
        _defaultValueDefinition = new TypeFieldModel(this, DEFAULT_VALUE_DEFINITION_FIELD);
        return _defaultValueDefinition;
        
    }
    
    private TypeFieldModel _calculateExpressionDefinition;
    
    public TypeFieldModel calculateExpressionDefinition() {
        
        if ( _calculateExpressionDefinition != null ) {
            return _calculateExpressionDefinition;
        }
        
        _calculateExpressionDefinition = new TypeFieldModel(this, CALCULATE_EXPRESSION_DEFINITION_FIELD);
        return _calculateExpressionDefinition;
        
    }
    
    private TypeFieldModel _validateDefinition;
    
    public TypeFieldModel validateDefinition() {
        
        if ( _validateDefinition != null ) {
            return _validateDefinition;
        }
        
        _validateDefinition = new TypeFieldModel(this, VALIDATE_DEFINITION_FIELD);
        return _validateDefinition;
        
    }
    
    private TypeFieldModel _autoUppercase;
    
    public TypeFieldModel autoUppercase() {
        
        if ( _autoUppercase != null ) {
            return _autoUppercase;
        }
        
        _autoUppercase = new TypeFieldModel(this, AUTO_UPPERCASE_FIELD);
        return _autoUppercase;
        
    }
    
    private TypeFieldModel _required;
    
    public TypeFieldModel required() {
        
        if ( _required != null ) {
            return _required;
        }
        
        _required = new TypeFieldModel(this, REQUIRED_FIELD);
        return _required;
        
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "TYPE" : return typeId();
        case "ORDERNO" : return orderNo();
        case "NAME" : return name();
        case "DESCRIPTION" : return description();
        case "COLUMNNAME" : return columnName();
        case "VALUETYPE" : return valueType();
        case "REFERENCEDTYPE" : return referencedTypeId();
        case "VALUEPRECISION" : return valuePrecision();
        case "VALUESCALE" : return valueScale();
        case "DEFAULTVALUEDEFINITION" : return defaultValueDefinition();
        case "CALCULATEEXPRESSIONDEFINITION" : return calculateExpressionDefinition();
        case "VALIDATEDEFINITION" : return validateDefinition();
        case "AUTOUPPERCASE" : return autoUppercase();
        case "REQUIRED" : return required();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
