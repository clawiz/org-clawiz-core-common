/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.logger;

import org.clawiz.core.common.CoreException;

/**
 * (c) Clawiz
 */
public class Log4JLogger extends AbstractLogger {

    org.apache.logging.log4j.Logger logger;

    public void init(Class clazz)  {
        logger = org.apache.logging.log4j.LogManager.getLogger(clazz);
    }

    @Override
    public void error(String msg, Exception e) {
        logger.error(msg, e);
    }

    @Override
    public void error(Exception e) {
        logger.error(e);
    }

    @Override
    public void log(Level level, String msg) {
        switch (level) {
            case INFO:    logger.info(msg); break;
            case DEBUG:   logger.debug(msg); break;
            case WARNING: logger.warn(msg); break;
            case ERROR:   logger.error(msg); break;
            case TRACE:   logger.trace(msg); break;
            default: logger.info(msg); break;
        }
    }
}
