/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common;

import org.clawiz.core.common.system.object.ObjectService;
import org.clawiz.core.common.system.session.Session;

import java.math.BigDecimal;

/**
 * (c) Clawiz
 */
// Extending RuntimeException for checked exceptions in Iterator and so on
public class CoreException extends RuntimeException {

    private Throwable _cause;

    private static String prepareMessage(Session _session, String message, Object...parameters) {

        Object[] values = new Object[0];

        if ( parameters.length > 0 ) {
            int lastIndex = parameters.length - 1;
            if ( parameters[lastIndex] != null && Throwable.class.isAssignableFrom(parameters[lastIndex].getClass() ) ) {
                lastIndex--;
            }
            if ( lastIndex >= 0 ) {
                values = new Object[lastIndex+1];
                System.arraycopy(parameters, 0, values, 0, lastIndex+1);
            }
        }

        StringBuilder sb            = new StringBuilder();
        ObjectService objectService = null;
        int           index         = 0;
        Session       session       = null;
        for (int i=0; i < message.length(); i++) {
            char c = message.charAt(i);
            if ( c == '?' && index < values.length ) {
                sb.append(values[index] != null ? values[index].toString() : "NULL");
                index++;
            } else if (c == '@' && index < values.length) {
                Object obj = values[index];
                if ( obj == null ) {
                    sb.append("NULL");
                } else if ( obj instanceof Class ) {
                    sb.append(((Class) obj).getName());
                } else if ( obj instanceof BigDecimal) {

                    if ( objectService == null ) {
                        session = _session;
                        if ( session == null ) {
                            session = Core.getSession(CoreException.class.getName());
                            if ( session == null ) {
                                session = Core.newSession(CoreException.class.getName());
                            }
                        }
                        objectService = session.getService(ObjectService.class);
                    }
                    try {
                        sb.append(objectService.idToString((BigDecimal) obj));
                    } catch (Exception e) {
                        e.printStackTrace();
                        sb.append(obj);
                    }
                }

                index++;
            } else {
                sb.append(c);
            }
        }

        if (session != null && _session == null ) {
            session.getConnection().close();
        }

        return sb.toString();
    }

    public CoreException(String message, Object...parameters){
        super(prepareMessage(null, message, parameters)
                , (parameters.length > 0
                        && parameters[parameters.length-1] != null
                        && Throwable.class.isAssignableFrom(parameters[parameters.length-1].getClass())) ? ((Throwable) parameters[parameters.length-1]) : null );
   }

    public CoreException(Session session, String message, Object...parameters){
        super(prepareMessage(session, message, parameters)
                , (parameters.length > 0
                        && parameters[parameters.length-1] != null
                        && Throwable.class.isAssignableFrom(parameters[parameters.length-1].getClass())) ? ((Throwable) parameters[parameters.length-1]) : null );
    }


}
