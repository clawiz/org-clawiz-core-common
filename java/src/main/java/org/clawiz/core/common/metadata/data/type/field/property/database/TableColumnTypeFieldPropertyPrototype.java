package org.clawiz.core.common.metadata.data.type.field.property.database;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class TableColumnTypeFieldPropertyPrototype extends org.clawiz.core.common.metadata.data.type.field.property.AbstractTypeFieldProperty {
    
    @ExchangeAttribute
    private String columnName;
    
    public TableColumnTypeFieldProperty withName(String value) {
        setName(value);
        return (TableColumnTypeFieldProperty) this;
    }
    
    public String getColumnName() {
        return this.columnName;
    }
    
    public void setColumnName(String value) {
        this.columnName = value;
    }
    
    public TableColumnTypeFieldProperty withColumnName(String value) {
        setColumnName(value);
        return (TableColumnTypeFieldProperty) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
