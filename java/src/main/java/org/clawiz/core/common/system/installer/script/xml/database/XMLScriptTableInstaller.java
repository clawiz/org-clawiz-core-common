/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.installer.script.xml.database;

import org.clawiz.core.common.system.database.metadata.*;
import org.clawiz.core.common.system.installer.database.DatabaseTableInstaller;
import org.clawiz.core.common.system.installer.script.xml.AbstractXMLScriptElementInstaller;
import org.clawiz.core.common.utils.StringUtils;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class XMLScriptTableInstaller extends AbstractXMLScriptElementInstaller {

    DatabaseTable table;

    @Override
    public void process() {

        String tableName = getAttribute( "name" );
        if ( tableName == null ) {
            throwException("Attribute 'name' not defined for 'install-database-table' in script ?", getScriptFileName() );
        }

        table = new DatabaseTable( tableName );

        table.setComment(getAttribute( "description" ));

        // колонки
        NodeList columnNodes = getElement().getElementsByTagName( "column" );
        DatabaseTableColumnList columns = table.getColumns();

        for( int j = 0; j < columnNodes.getLength(); j++ ) {
            Element columnElement = (Element) columnNodes.item( j );

            String columnName      = getAttribute(columnElement, "name");
            String type            = getAttribute(columnElement, "type");
            String precisionString = getAttribute(columnElement, "precision");
            String scaleString     = getAttribute(columnElement, "scale");
            String notNull         = getAttribute(columnElement, "notnull");
            String comment         = getAttribute(columnElement, "description");

            int precisionInt = 0;
            try {
                precisionInt = precisionString != null ? StringUtils.toBigDecimal(precisionString).intValue() : 0;
            } catch (Exception e) {
                throwException("Exception in number format in precision value '?' of table '?' column '?'", precisionString, tableName,  columnName);
            }
            int scaleInt     = 0;
            try {
                scaleInt = scaleString     != null ? StringUtils.toBigDecimal(scaleString).intValue()     : 0;
            } catch (Exception e) {
                throwException("Exception in number format in scale value '?' of table '?' column '?'", scaleString, tableName,  columnName);
            }

            columns.add( new DatabaseTableColumn(columnName
                    , DatabaseTableColumn.Type.valueOf(type.toUpperCase())
                    , precisionInt
                    , scaleInt
                    , StringUtils.toBoolean(notNull)
                    , comment));
        }

        NodeList indexNodes = getElement().getElementsByTagName( "index" );
        DatabaseTableIndexList indexes = table.getIndexes();

        for( int j = 0; j < indexNodes.getLength(); j++ ) {
            Element indexElement = (Element) indexNodes.item( j );
            boolean unique = StringUtils.toBoolean(indexElement.getAttribute( "unique" ));

            DatabaseTableIndex tableIndex = new DatabaseTableIndex(null, unique);

            DatabaseTableIndexColumnList indexColumnList = tableIndex.getColumns();
            NodeList indexColumnsList = indexElement.getElementsByTagName( "index_column" );
            for( int c = 0; c < indexColumnsList.getLength(); c++ ) {
                Element indexColumnElement = (Element) indexColumnsList.item( c );
                DatabaseTableIndexColumn column = new DatabaseTableIndexColumn(getAttribute(indexColumnElement, "name" ));
                indexColumnList.add(column);
            }

            indexes.add( tableIndex);
        }

        addInstaller(DatabaseTableInstaller.class).setTable(table);


    }
}
