/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.queue;


import org.clawiz.core.common.system.service.Service;

/**
 * User: krusanovasa
 * Date: 28.03.2008
 * Time: 17?8?8
 */
public class QueueBase extends Service {
/*
    QueueService queueService;
    QueueJobService queueJobService;
    DictionaryService dictionaryService;

    BigDecimal stringType;
    BigDecimal numberType;
    BigDecimal dateType;


    public void init() {

        stringType = dictionaryService.nameToId("STRING");
        numberType = dictionaryService.nameToId("NUMBER");
        dateType = dictionaryService.nameToId("DATE");

    }

    public void destroy() {

    }

    */
/**
     * Метод для получения списка всех очередей задач.
     *
     * @return QueueList
     * @CoreException
     *//*

    public QueueList getQueueList() {

        return queueService.fetchQueueList(null, "name", new Object[0]);

    }

    public BigDecimal getQueueNoByName(String name) {

        QueueList list = queueService.fetchQueueList("upper_name like ?", new Object[]{name.toUpperCase()});

        if (list.size() > 0) {
            return list.get(0).getId();
        }
        return null;
    }


    public BigDecimal queueNameToId(String name) {
        return queueService.nameToId(name);
    }

    */
/**
     * Получение всех задач, необходимых для запуска, ожидающих своей очереди,
     *
     * @param queueNo - номер очереди
     * @return QueuejobList
     * @CoreException
     *//*

    public ArrayList<BigDecimal> getQueueJobListForRun(BigDecimal queueNo, int maxCount) {

        ExecuteSelectParameters selectParameters = new ExecuteSelectParameters();
        selectParameters.setRowsCount(maxCount);

        DataSet dataSet = executeSelect("select id from cw_core_queue_jobs where queue_id = ? and upper_state like ?",new Object[]{queueNo, "CREATED"}, selectParameters);

        ArrayList<BigDecimal> result = new ArrayList<BigDecimal>();
        while ( ! dataSet.isEOF() ) {
            result.add(dataSet.getBigDecimal(0));
            dataSet.next();

        }
        dataSet.close();
        return result;

    }

    public String getServiceNameByQueueId(BigDecimal queueNo) {

        QueueObject queueObject = queueService.fetchQueue(queueNo);
        return queueObject.getServiceName();
    }

    public String getMethodNameByQueueId(BigDecimal queueNo) {
        QueueObject queueObject = queueService.fetchQueue(queueNo);
        return queueObject.getMethodName();
    }

    public InstallerContext getParameterListByQueueId(BigDecimal queueNo) {
        return queueService.fetchParameterList("Queue_id = ?", "order_no", new Object[]{queueNo});
    }

    //todo
    public Class getQueueParameterClass(BigDecimal queueNo, String parameterName) {

        BigDecimal typeId = getQueueParameterType(queueNo, parameterName);

        if (stringType.equals(typeId)) {
            return String.class;
        }
        if (numberType.equals(typeId)) {
            return BigDecimal.class;
        }
        if (dateType.equals(typeId)) {
            return Date.class;
        }

        return null;
    }


    public BigDecimal getJobNoByParameter(String parameterName, Object parameterValue) {

        DataSet dataSet = executeSelect("select queuejob_id from cw_core_queue_job_params where parameter_name like ? and number_value = ?", new Object[]{parameterName, (BigDecimal) parameterValue});

        return dataSet.getBigDecimal(0);
    }

    public String getJobState(BigDecimal jobNo) {

        JobObject job = queueJobService.fetchJob(jobNo);

        if (job != null) {
            return job.getState();
        }
        return null;
    }

    public BigDecimal createQueue(String queueName, String description, BigDecimal processesCount, String serviceName, String methodName) {

        QueueObject queueObject = new QueueObject(null);
        queueObject.setName(queueName);
        queueObject.setCode(queueName);
        queueObject.setDescription(description);
        queueObject.setProcessesCount(processesCount);
        queueObject.setServiceName(serviceName);
        queueObject.setMethodName(methodName);

        queueService.saveQueue(queueObject);
        return queueObject.getId();
    }

    // Возвращает наименование параметра по порядковому номеру

    public String parameterOrderNoToName(BigDecimal queueNo, BigDecimal orderNo) {

        return executeSelectString("select name from cw_core_queue_parameters  where  queue_id=? and order_no=?", new Object[]{queueNo, orderNo});
    }

    // Возвращает порядковый номер по наименованию
    public BigDecimal parameterNameToOrderNo(BigDecimal queueNo, String parameterName) {

        return executeSelectBigDecimal("select order_no from cw_core_queue_parameters  where  queue_id=? and upper_name=?", new Object[]{queueNo, parameterName.toUpperCase()});

    }

    // Сохраняет запись о параметре
    // проверяем нет ли совпадаений по наименванию
    // По переданному порядковому номеру проверяем наличие записи и делаем обновление либо добавление
    private void saveQueueParameter(BigDecimal queueNo, BigDecimal orderNo, String name, String description,
                                    BigDecimal typeId, String defaultStringValue, BigDecimal defaultNumberValue, Date defaultDateValue, boolean isMandatory) {

        String check = executeSelectString("select name from cw_core_queue_parameters  where  queue_id=? and upper_name=? and order_no<>?", new Object[]{queueNo, name.toUpperCase(), orderNo});

        if (check != null) {
            throwException("Параметр '" + name + "' уже существует! ", new Object[]{});
        }

        BigDecimal id = queueService.orderNoToId(queueNo, orderNo);
        ParameterObject p;

        if (id != null) {
            p = queueService.fetchParameter(id);
        } else {
            p = new ParameterObject(null);
        }

        p.setQueueId(queueNo);
        p.setOrderNo(orderNo);
        p.setName(name);
        p.setDescription(description);
        throwException("Check typeId field of parameter object");
//        p.setTypeId(typeId);
        p.setDefaultStringValue(defaultStringValue);
        p.setDefaultNumberValue(defaultNumberValue);
        p.setDefaultDateValue(defaultDateValue);
        p.setIsRequired(isMandatory);

        queueService.saveParameter(p);

    }


    // Добавление/редактирование параметров
    public void setQueueStringParameter(BigDecimal queueNo, BigDecimal orderNo, String name, String description, String defaultValue, boolean isMandatory) {

        saveQueueParameter(queueNo, orderNo, name, description, stringType, defaultValue, null, null, isMandatory);

    }

    public void setQueueNumberParameter(BigDecimal queueNo, BigDecimal orderNo, String name, String description, BigDecimal defaultValue, boolean isMandatory) {

        saveQueueParameter(queueNo, orderNo, name, description, numberType, null, defaultValue, null, isMandatory);

    }

    public void setQueueDateParameter(BigDecimal queueNo, BigDecimal orderNo, String name, String description, Date defaultValue, boolean isMandatory) {

        saveQueueParameter(queueNo, orderNo, name, description, dateType, null, null, defaultValue, isMandatory);

    }

    // Добавляет задание
    public BigDecimal addQueueJob(BigDecimal queueNo) {

        //так как у задания сейчас ключ состоит из номера очереди и порядкового номера задачи, то
        //сначала узнаем максимальный order_no для данной очереди
        Date sysDate = new Date();

        JobObject j = new JobObject(null);

        j.setQueueId(queueNo);
        j.setCreated(sysDate);

        BigDecimal maxOrderNo = executeSelectBigDecimal("select max(job_order_no) from cw_core_queue_jobs where queue_id=?", new Object[]{queueNo});

        if (maxOrderNo != null) {
            maxOrderNo = maxOrderNo.add(new BigDecimal(1));
        } else {
            maxOrderNo = new BigDecimal(1);
        }
        j.setOrderNo(maxOrderNo);

        queueJobService.saveJob(j);
        return j.getId();
    }

    //////////////////////////
    //Установка параметров задания
    //
    private void saveQueueJobParameter(BigDecimal queueJobNo, BigDecimal orderNo, String name,
                                       String stringValue,
                                       BigDecimal numberValue,
                                       Date dateValue) {

        if ((orderNo == null) & (name == null)) {
            throwException("Не указан ни порядковый номер ни наименование", new Object[]{});
        }

        JobObject j = queueJobService.fetchJob(queueJobNo);

        BigDecimal queueNo = j.getQueueId();


        if (orderNo == null) {
            orderNo = parameterNameToOrderNo(queueNo, name);
        }
        if (name == null) {
            name = parameterOrderNoToName(queueNo, orderNo);
        }

        if ((orderNo == null) & (name == null)) {
            throwException("Не удалось идентифицировать параметр", new Object[]{});
        }

        if ((orderNo != null) & (name != null)) {
            if (!name.equals(parameterOrderNoToName(queueNo, orderNo))) {
                throwException("Не удалось идентифицировать параметр", new Object[]{});
            }
        }


        BigDecimal parNo = queueService.orderNoToId(queueNo, orderNo);
        ParameterObject p;

        if (parNo != null) {
            p = queueService.fetchParameter(parNo);
        } else {
            p = new ParameterObject(null);

        }

        throwException("Check typeId field of parameter object");
*/
/*
        BigDecimal typeId = p.getTypeId();
        if ((typeId.equals(stringType)) && (stringValue == null)) {
            stringValue = p.getDefaultStringValue();
            numberValue = null;
            dateValue = null;
        }
        if ((typeId.equals(numberType)) && (numberValue == null)) {
            numberValue = p.getDefaultNumberValue();
            dateValue = null;
            stringValue = null;
        }
        if ((typeId.equals(dateType)) && (dateValue == null)) {
            dateValue = p.getDefaultDateValue();
            stringValue = null;
            numberValue = null;
        }
*//*


        //
        BigDecimal id = queueJobService.orderNoToId(queueJobNo, orderNo);
        JobParameterObject pj;

        if (id != null) {
            pj = queueJobService.fetchJobParameter(id);
        } else {
            pj = new JobParameterObject(null);
        }

        pj.setJobId(queueJobNo);
        pj.setParameterName(name);
        pj.setOrderNo(orderNo);
        pj.setStringValue(stringValue);
        pj.setNumberValue(numberValue);
        pj.setDateValue(dateValue);

        queueJobService.saveJobParameter(pj);

    }

    // по наименованию
    public void setJobStringParameter(BigDecimal jobNo, String parameterName, String value) {
        saveQueueJobParameter(jobNo, null, parameterName, value, null, null);
    }

    public void setJobNumberParameter(BigDecimal jobNo, String parameterName, BigDecimal value) {
        saveQueueJobParameter(jobNo, null, parameterName, null, value, null);

    }

    public void setJobDateParameter(BigDecimal jobNo, String parameterName, Date value) {
        saveQueueJobParameter(jobNo, null, parameterName, null, null, value);

    }

    // по порядкововму номеру
    public void setJobParameterString(BigDecimal jobNo, BigDecimal orderNo, String value) {
        saveQueueJobParameter(jobNo, orderNo, null, value, null, null);
    }

    public void setJobParameterNumber(BigDecimal jobNo, BigDecimal orderNo, BigDecimal value) {
        saveQueueJobParameter(jobNo, orderNo, null, null, value, null);

    }

    public void setJobParameterDate(BigDecimal jobNo, BigDecimal orderNo, Date value) {
        saveQueueJobParameter(jobNo, orderNo, null, null, null, value);

    }

////

    public String getQueueJobParameterString(BigDecimal queueJobNo, BigDecimal orderNo) {
        BigDecimal id = queueJobService.orderNoToId(queueJobNo, orderNo);
        JobParameterObject pj = null;
        if (id != null) {
            pj = queueJobService.fetchJobParameter(id);
        }
        if (pj != null) {
            return pj.getStringValue();
        }
        return null;
    }

    public BigDecimal getQueueJobParameterNumber(BigDecimal queueJobNo, BigDecimal orderNo) {
        BigDecimal id = queueJobService.orderNoToId(queueJobNo, orderNo);
        JobParameterObject pj = null;
        if (id != null) {
            pj = queueJobService.fetchJobParameter(id);
        }
        if (pj != null) {
            return pj.getNumberValue();
        }
        return null;
    }

    public Date getQueueJobParameterDate(BigDecimal queueJobNo, BigDecimal orderNo) {
        BigDecimal id = queueJobService.orderNoToId(queueJobNo, orderNo);
        JobParameterObject pj = null;
        if (id != null) {
            pj = queueJobService.fetchJobParameter(id);
        }
        if (pj != null) {
            return pj.getDateValue();
        }
        return null;
    }

    public String getQueueJobParameterString(BigDecimal queueJobNo, String parameterName) {
        BigDecimal id = queueJobService.parameterNameToId(queueJobNo, parameterName);
        JobParameterObject pj = null;
        if (id != null) {
            pj = queueJobService.fetchJobParameter(id);
        }
        if (pj != null) {
            return pj.getStringValue();
        }
        return null;
    }

    public BigDecimal getQueueJobParameterNumber(BigDecimal queueJobNo, String parameterName) {
        BigDecimal id = queueJobService.parameterNameToId(queueJobNo, parameterName);
        JobParameterObject pj = null;
        if (id != null) {
            pj = queueJobService.fetchJobParameter(id);
        }
        if (pj != null) {
            return pj.getNumberValue();
        }
        return null;
    }

    public Date getQueueJobParameterDate(BigDecimal queueJobNo, String parameterName) {
        BigDecimal id = queueJobService.parameterNameToId(queueJobNo, parameterName);
        JobParameterObject pj = null;
        if (id != null) {
            pj = queueJobService.fetchJobParameter(id);
        }
        if (pj != null) {
            return pj.getDateValue();
        }
        return null;
    }


    public BigDecimal getQueueParameterType(BigDecimal queueNo, BigDecimal orderNo) {
        BigDecimal parNo = queueService.orderNoToId(queueNo, orderNo);
        ParameterObject p;

        if (parNo != null) {
            p = queueService.fetchParameter(parNo);
        } else {
            p = new ParameterObject(null);

        }

        if (p != null) {
            throwException("Check typeId field of parameter object");
//            return p.getTypeId();
        }
        return null;
    }

    public BigDecimal getQueueParameterType(BigDecimal queueNo, String parameterName) {
        BigDecimal parNo = queueService.parameterNameToId(queueNo, parameterName);
        ParameterObject p;

        if (parNo != null) {
            p = queueService.fetchParameter(parNo);
        } else {
            p = new ParameterObject(null);

        }
        if (p != null) {
            throwException("Check typeId field of parameter object");
//            return p.getTypeId();
        }
        return null;
    }


    public void setJobState(BigDecimal jobNo, String State) {
        JobObject j = queueJobService.fetchJob(jobNo);
        j.setState(State);
        j.setLastStateTime(new Date()); //ставим время последнего изменения состояния задачи
        queueJobService.saveJob(j);
    }

    public void setStartTime(BigDecimal jobNo, Date startTime) {
        if (startTime == null) {
            startTime = new Date();
        }

        JobObject j = queueJobService.fetchJob(jobNo);
        j.setStartTime(startTime);
        queueJobService.saveJob(j);
    }

    public void setCompletePercent(BigDecimal jobNo, BigDecimal percent) {
        JobObject j = queueJobService.fetchJob(jobNo);
        j.setCompletePercent(percent);
        queueJobService.saveJob(j);
    }

    public void setCurrentStateLine(BigDecimal jobNo, String stateLine) {
        JobObject j = queueJobService.fetchJob(jobNo);
        Date sysDate = new Date();

        j.setLastStateLine(stateLine);
        j.setLastStateTime(sysDate);

        queueJobService.saveJob(j);
    }

    public void setSessionName(BigDecimal jobNo, String sessionName) {
        JobObject j = queueJobService.fetchJob(jobNo);
        j.setSessionKey(sessionName);
        queueJobService.saveJob(j);

    }

    public void setThreadName(BigDecimal jobNo, String threadName) {
        JobObject j = queueJobService.fetchJob(jobNo);
        j.setThreadName(threadName);
        queueJobService.saveJob(j);

    }

    public void addTextLine(BigDecimal jobNo, String textType, String textLine) {

        if ( textLine == null ) {
            return;
        }

        BigDecimal maxOrderNo = executeSelectBigDecimal("select max(text_order_no) from cw_core_queue_job_texts  where  job_id=?", new Object[]{jobNo});

        if (maxOrderNo != null) {
            maxOrderNo = maxOrderNo.add(new BigDecimal(1));
        } else {
            maxOrderNo = new BigDecimal(1);
        }

        Date sysDate = new Date();

        JobLogLineObject l = new JobLogLineObject(null);
        l.setJobId(jobNo);
        l.setOrderNo(maxOrderNo);
        l.setLineType(textType);

        String vLine = textLine;
        if ( vLine.length() > 1024 ) {
            vLine = vLine.substring(0,1023);
        }

        l.setLine(textLine);
        l.setLineTime(sysDate);

        queueJobService.saveJobLogLine(l);
    }
*/
}
