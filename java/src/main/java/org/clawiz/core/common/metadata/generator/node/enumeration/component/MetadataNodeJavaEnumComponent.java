package org.clawiz.core.common.metadata.generator.node.enumeration.component;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.data.language.node.enumeration.EnumerationValue;
import org.clawiz.core.common.metadata.generator.node.enumeration.MetadataNodeJavaEnumGenerator;
import org.clawiz.core.common.metadata.generator.node.enumeration.MetadataNodeJavaEnumGeneratorContext;
import org.clawiz.core.common.metadata.data.language.node.AbstractLanguageNode;
import org.clawiz.core.common.metadata.data.language.node.enumeration.Enumeration;
import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.core.common.system.generator.java.component.AbstractJavaEnumComponent;
import org.clawiz.core.common.system.generator.java.component.element.JavaMemberElement;
import org.clawiz.core.common.system.generator.java.component.element.JavaMethodElement;
import org.clawiz.core.common.utils.StringUtils;

public class MetadataNodeJavaEnumComponent extends AbstractJavaEnumComponent {

    private MetadataNodeJavaEnumGenerator generator;
    private MetadataNodeJavaEnumGeneratorContext context;
    private Enumeration structure;

    @Override
    public MetadataNodeJavaEnumGenerator getGenerator() {
        return generator;
    }

    @Override
    public void setGenerator(AbstractGenerator generator) {
        super.setGenerator(generator);
        this.generator = (MetadataNodeJavaEnumGenerator) generator;
        setContext(getGenerator().getContext());
    }

    public MetadataNodeJavaEnumGeneratorContext getContext() {
        return context;
    }

    public void setContext(MetadataNodeJavaEnumGeneratorContext context) {
        this.context = context;
        setStructure(getContext().getLanguageNode());
    }

    public Enumeration getStructure() {
        return structure;
    }

    public void setStructure(AbstractLanguageNode structure) {
        this.structure = (Enumeration) structure;
    }

    protected void addToEnumeration() {

        JavaMethodElement method = addMethod(getName(), "to" + getName());
        method.setAccessLevel(JavaMemberElement.AccessLevel.PUBLIC);
        method.setStatic(true);

        method.addParameter("String", "value");

        method.addText("if ( value == null ) {");
        method.addText("    return null;");
        method.addText("}");
        method.addText("");
        method.addText("try {");
        method.addText("    return " + this.getName() + ".valueOf(value.toUpperCase());");
        method.addText("} catch (IllegalArgumentException e) {");
        method.addText("    throw new CoreException(\"Wrong " + getName() + " value '?'\", value);");
        method.addText("}");
        method.addText("");

    }

    protected void addToDescription() {
        JavaMethodElement method = addMethod("String", "toDescription");
        method.setAccessLevel(JavaMemberElement.AccessLevel.PUBLIC);
        method.setStatic(true);

        method.addParameter(getName(), "value");

        method.addText("if ( value == null ) {");
        method.addText("    return null;");
        method.addText("}");
        method.addText("");
        method.addText("switch (value) {");

        int maxLength = 0;
        for (EnumerationValue value : structure.getValues() ) {
            if ( value.getName().length() > maxLength ) {
                maxLength = value.getName().length();
            }
        }

        for (EnumerationValue value : structure.getValues() ) {
            method.addText("    case " + value.getName().toUpperCase() + StringUtils.space(maxLength - value.getName().length()) + ": return \""
                    + StringUtils.escapeJavaString(value.getDescription()) + "\";");
        }
        method.addText("}");
        method.addText("");

        method.addText("return value.toString();");

    }

    @Override
    public void process() {
        super.process();
        setName(getStructure().getJavaClassName());

        for (EnumerationValue value : structure.getValues() ) {
            addValue(value.getName().toUpperCase());
        }

        addImport(CoreException.class);

        addToEnumeration();
        addToDescription();
    }
}
