package org.clawiz.core.common.storage.synonym;

import org.clawiz.core.common.metadata.data.type.Type;
import org.clawiz.core.common.metadata.data.type.field.TypeField;
import org.clawiz.core.common.system.type.model.TypeModel;
import org.clawiz.core.common.system.type.model.TypeFieldModel;
import org.clawiz.core.common.system.type.model.TypeRecordIdModel;
import org.clawiz.core.common.system.service.NotInitializeService;

public class SynonymModelPrototype extends TypeModel {
    
    private static TypeField OBJECT_ID_FIELD;
    
    private static TypeField SYNONYM_TYPE_ID_FIELD;
    
    private static TypeField VALUE_FIELD;
    
    private static Type type;
    
    private SynonymService typeService;
    
    private static boolean staticMembersInitialized;
    
    public Type getType() {
        return this.type;
    }
    
    public SynonymService getTypeService() {
        return this.typeService;
    }
    
    public void init() {
        super.init();
        
        if ( staticMembersInitialized ) { 
            return;
        }
        
        type = getService(SynonymService.class).getType();
        
        
        OBJECT_ID_FIELD = type.getFields().get("Object");
        if ( OBJECT_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Synonym.Object"}); }
        
        SYNONYM_TYPE_ID_FIELD = type.getFields().get("SynonymType");
        if ( SYNONYM_TYPE_ID_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Synonym.SynonymType"}); }
        
        VALUE_FIELD = type.getFields().get("Value");
        if ( VALUE_FIELD == null ) { throwException("Type field '?' not registered in database", new Object[]{"org.clawiz.core.common.storage.Synonym.Value"}); }
        
        staticMembersInitialized = true;
    }
    
    public TypeRecordIdModel id() {
        
        if ( _id == null ) {
            _id = new TypeRecordIdModel(this);
        }
        
        return _id;
    }
    
    private TypeRecordIdModel _id;
    
    private TypeFieldModel _objectId;
    
    public TypeFieldModel objectId() {
        
        if ( _objectId != null ) {
            return _objectId;
        }
        
        _objectId = new TypeFieldModel(this, OBJECT_ID_FIELD);
        return _objectId;
        
    }
    
    private TypeFieldModel _synonymTypeId;
    
    public TypeFieldModel synonymTypeId() {
        
        if ( _synonymTypeId != null ) {
            return _synonymTypeId;
        }
        
        _synonymTypeId = new TypeFieldModel(this, SYNONYM_TYPE_ID_FIELD);
        return _synonymTypeId;
        
    }
    
    @NotInitializeService
    private org.clawiz.core.common.storage.synonymtype.SynonymTypeModel _synonymTypeTypeModel;
    
    public org.clawiz.core.common.storage.synonymtype.SynonymTypeModel synonymType() {
        
        if ( _synonymTypeTypeModel == null ) {
            org.clawiz.core.common.storage.synonymtype.SynonymTypeModel model = getService(org.clawiz.core.common.storage.synonymtype.SynonymTypeModel.class, this.toString() + "_SynonymType");
            model.setJoinField(synonymTypeId());
            _synonymTypeTypeModel = model;
        }
        
        return _synonymTypeTypeModel;
    }
    
    private TypeFieldModel _value;
    
    public TypeFieldModel value() {
        
        if ( _value != null ) {
            return _value;
        }
        
        _value = new TypeFieldModel(this, VALUE_FIELD);
        return _value;
        
    }
    
    public TypeFieldModel getByFieldName(String fieldName) {
        if ( fieldName == null ) {
            throwException("Cannot return field model for null field name");
        }
        switch (fieldName.toUpperCase()) {
        case "OBJECT" : return objectId();
        case "SYNONYMTYPE" : return synonymTypeId();
        case "VALUE" : return value();
            default : throwException("Wrong field name '?' in call to get model field of '?'", new Object[]{fieldName, getType().getFullName()});
        }
        return null;
    }
}
