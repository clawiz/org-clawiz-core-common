/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.system.generator.ddl;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.database.Connection;
import org.clawiz.core.common.system.generator.abstractgenerator.writer.AbstractWriter;

import java.sql.SQLException;
import java.sql.Statement;

/**
 * (c) Clawiz
 */
public class AbstractDDLWriter extends AbstractWriter {

    StringBuilder stringBuilder;

    Connection ddlConnection;

    public Connection getDdlConnection() {
        return ddlConnection;
    }

    public void setDdlConnection(Connection ddlConnection) {
        this.ddlConnection = ddlConnection;
    }

    @Override
    public void open() {
        stringBuilder = new StringBuilder();
    }

    @Override
    public void close() {
        String ddl = stringBuilder.toString();
        stringBuilder = null;
        logDebug("DDL STATEMENT: \n" + ddl);
        Statement statement = null;
        try {
            statement = ddlConnection.getJDBCConnection().createStatement();
            statement.execute(ddl);
        } catch (SQLException e) {
            throw new CoreException("Exception on execute ddl statement ? \n ?", ddl, e.getMessage(), e);
        } finally {
            try {
                statement.close();
            } catch (SQLException e) {
                throw new CoreException("Exception on close ddl statement ? \n ?", ddl, e.getMessage(), e);
            }
        }

    }
}
