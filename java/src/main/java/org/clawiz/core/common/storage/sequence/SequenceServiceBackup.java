/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.core.common.storage.sequence;


public class SequenceServiceBackup {


/*
    private static ArrayList<AbstractSequenceServiceHandler> _immediateServiceHandlers;

    private static BigDecimal _typeId;

    private static BigDecimal _localListGroupId;

    private static ConcurrentHashMap<String, Optional<BigDecimal>> resolveByNameCache = new ConcurrentHashMap<>();

    private static ConcurrentHashMap<String, Optional<BigDecimal>> resolveBySysSequenceValuesPkCache = new ConcurrentHashMap<>();

    public void init()throws CoreException{
         
        super.init();
        try {
            typeName           = "SEQUENCE";
            if ( _typeId == null ) { _typeId = objectService.typeNameToId(module, typeName); }
            typeId             = _typeId;
            if ( _localListGroupId == null ) { _localListGroupId = groupService.nameToId("org.clawiz.core.common.storage.Sequence", groupService.getListsGroupId()); }
            localListGroupId   = _localListGroupId;
        } catch ( CoreException e ) {
            throw new CoreException("Service initialization exception '" + getServiceName() + "' :\n?", new Object[]{e.getMessage()}, e);
        } catch (Exception e) {
            throw new CoreException("Service initialization exception '" + getServiceName() + "' :\n?", new Object[]{e.getMessage()}, e);
        }
    }

    public void destroy(){
    }

    private ArrayList<AbstractSequenceServiceHandler> getImmediateServiceHandlers()throws CoreException{
        if ( _immediateServiceHandlers == null ) {
            ArrayList<AbstractSequenceServiceHandler> hl = new ArrayList<>();
            for ( org.clawiz.core.common.system.servicehandler.ServiceHandler handler : getService(org.clawiz.core.common.system.type.TypeBase.class).getServiceHandlers(typeId)) {
                if ( handler.getExecuteMode() == org.clawiz.core.common.system.servicehandler.ExecuteMode.IMMEDIATE) {
                    hl.add((AbstractSequenceServiceHandler) handler);
                }
            }
            _immediateServiceHandlers = hl;
        }
        return _immediateServiceHandlers;
    }

    private void checkSequence( SequenceObject sequence,BigDecimal parentGroupId )throws CoreException{
        
        sequence.fillDefaultValues(getSession());
        
        for (AbstractSequenceServiceHandler handler : getImmediateServiceHandlers()) {
            handler.handleCheckSequence(sequence);
        }
        
        if ( sequence.getLastCachedValue() == null) {
            sequence.setLastCachedValue( new BigDecimal(0));
        }
        BigDecimal result = null;
        result = nameToId(sequence.getName(),sequence.getObject1Id(),sequence.getObject2Id(),sequence.getObject3Id());
        if ((result != null)&&(!result.equals(sequence.getId()))) {
            throwException("Sequence '?,@,@,@' already exists", new Object[]{sequence.getName(),sequence.getObject1Id(),sequence.getObject2Id(),sequence.getObject3Id()});
        }
    }

    private void checkValue( ValueObject value )throws CoreException{
        
        value.fillDefaultValues(getSession());
        
        for (AbstractSequenceServiceHandler handler : getImmediateServiceHandlers()) {
            handler.handleCheckValue(value);
        }
        
        BigDecimal result = null;
        result = sysSequenceValuesPkToId(value.getSequenceId(),value.getValue());
        if ((result != null)&&(!result.equals(value.getId()))) {
            throwException("Value '@,?' already exists", new Object[]{value.getSequenceId(),value.getValue()});
        }
    }

    public AbstractTypeServiceHandlerContext fetchServiceHandlerContext(String dataTableName, BigDecimal scn )throws CoreException{
        if ( dataTableName == null ) {
            throwException("Data table name fetched service handler context for scn '?' cannot be null", new Object[]{scn});
        }
        if ( dataTableName.toUpperCase().equals("CW_CORE_SEQUENCES") ) {
            return fetchSequenceServiceHandlerContext(scn);
        }
        if ( dataTableName.toUpperCase().equals("CW_CORE_SEQUENCE_VALUES") ) {
            return fetchValueServiceHandlerContext(scn);
        }
        throwException("Wrong data table name '?' for fetch service handler context scn '?' at class '?'", new Object[]{dataTableName, scn, this.getClass().getName()});
        return null;
    }

    public SequenceObject fetchSequence( BigDecimal id )throws CoreException{
        SequenceObject result;
         
        DataSet resultSet = executeSelect("select  name,object1_id,object2_id,object3_id,start_value,max_value,last_cached_value,cache_length from cw_core_sequences where  id = :id", new Object[]{id});
        if ( resultSet.isEOF() ) { resultSet.close(); return null; } 
        result = new SequenceObject(id, true);
        result._setService(this);
        result.setName(resultSet.getString( 0));
        result.setObject1Id(resultSet.getBigDecimal( 1));
        result.setObject2Id(resultSet.getBigDecimal( 2));
        result.setObject3Id(resultSet.getBigDecimal( 3));
        result.setStartValue(resultSet.getBigDecimal( 4));
        result.setMaxValue(resultSet.getBigDecimal( 5));
        result.setLastCachedValue(resultSet.getBigDecimal( 6));
        result.setCacheLength(resultSet.getBigDecimal( 7));
        resultSet.close();
        return result;
    }

    public SequenceServiceHandlerContext fetchSequenceServiceHandlerContext( BigDecimal scn )throws CoreException{
        SequenceServiceHandlerContext    serviceHandlerContext    =  new SequenceServiceHandlerContext();
        SequenceTransactionActionContext transactionActionContext =  new SequenceTransactionActionContext();
        TransactionAction              transactionAction        = new TransactionAction();
        
        serviceHandlerContext.setTypeId(this.typeId);
        serviceHandlerContext.setTypeService(this);
        serviceHandlerContext.setTransactionActionContext(transactionActionContext);
        
        transactionActionContext.setTransactionAction(transactionAction);
        transactionAction.setScn(scn);
        
        SequenceObject newValue = new SequenceObject();
        SequenceObject oldValue = new SequenceObject();
        transactionActionContext.setNewSequence(newValue);
        transactionActionContext.setOldSequence(oldValue);
        
        DataSet resultSet = executeSelect("select id, action_type, n_name, o_name, n_object1_id, o_object1_id, n_object2_id, o_object2_id, n_object3_id, o_object3_id, n_start_value, o_start_value, n_max_value, o_max_value, n_last_cached_value, o_last_cached_value, n_cache_length, o_cache_length from a_cw_core_sequences where scn = ?", new Object[]{scn});
        if ( resultSet.isEOF() ) { resultSet.close(); return null; }
        newValue.setId(resultSet.getBigDecimal(0));
        oldValue.setId(resultSet.getBigDecimal(0));
        transactionAction.setType(TransactionActionType.toTransactionActionType(resultSet.getString(1)));
        
        newValue.setName(resultSet.getString( 2));
        oldValue.setName(resultSet.getString( 3));
        newValue.setObject1Id(resultSet.getBigDecimal( 4));
        oldValue.setObject1Id(resultSet.getBigDecimal( 5));
        newValue.setObject2Id(resultSet.getBigDecimal( 6));
        oldValue.setObject2Id(resultSet.getBigDecimal( 7));
        newValue.setObject3Id(resultSet.getBigDecimal( 8));
        oldValue.setObject3Id(resultSet.getBigDecimal( 9));
        newValue.setStartValue(resultSet.getBigDecimal( 10));
        oldValue.setStartValue(resultSet.getBigDecimal( 11));
        newValue.setMaxValue(resultSet.getBigDecimal( 12));
        oldValue.setMaxValue(resultSet.getBigDecimal( 13));
        newValue.setLastCachedValue(resultSet.getBigDecimal( 14));
        oldValue.setLastCachedValue(resultSet.getBigDecimal( 15));
        newValue.setCacheLength(resultSet.getBigDecimal( 16));
        oldValue.setCacheLength(resultSet.getBigDecimal( 17));
        resultSet.close();
        return serviceHandlerContext;
    }

    private BigDecimal _sequenceToobjectId( SequenceObject sequence )throws CoreException{
         
        return sequence.getId();
    }

    private void saveAuditSequence( SequenceObject oldData,SequenceObject newData, TransactionAction transactionAction )throws CoreException{
        SequenceObject _old = oldData != null ? oldData : new SequenceObject();
        SequenceObject _new = newData != null ? newData : new SequenceObject();
        executeStatement("insert into a_cw_core_sequences (scn, action_type, id , o_name, o_upper_name, o_object1_id, o_object2_id, o_object3_id, o_start_value, o_max_value, o_last_cached_value, o_cache_length, n_name, n_upper_name, n_object1_id, n_object2_id, n_object3_id, n_start_value, n_max_value, n_last_cached_value, n_cache_length) values ( ?, ?, ?, ?, ?, ?, ?, ?, :9, ?0, ?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?0, ?1)", new Object[]{transactionAction.getScn(), transactionAction.getType().toString(), _old.getId() != null ? _old.getId() : _new.getId(), _old.getNameDatabaseValue(), stringToUpperCase(_old.getNameDatabaseValue()), _old.getObject1IdDatabaseValue(), _old.getObject2IdDatabaseValue(), _old.getObject3IdDatabaseValue(), _old.getStartValueDatabaseValue(), _old.getMaxValueDatabaseValue(), _old.getLastCachedValueDatabaseValue(), _old.getCacheLengthDatabaseValue(), _new.getNameDatabaseValue(), stringToUpperCase(_new.getNameDatabaseValue()), _new.getObject1IdDatabaseValue(), _new.getObject2IdDatabaseValue(), _new.getObject3IdDatabaseValue(), _new.getStartValueDatabaseValue(), _new.getMaxValueDatabaseValue(), _new.getLastCachedValueDatabaseValue(), _new.getCacheLengthDatabaseValue()});
    }

    public void saveSequence( SequenceObject sequence )throws CoreException{
        saveSequence(sequence, localListGroupId);
    }

    public void saveSequence( SequenceObject sequence, BigDecimal parentGroupId )throws CoreException{
         
        TransactionAction transactionAction = null;
        SequenceObject backupObject = null;
        sequence._setService(this);
        if( ! sequence.isFetchedFromDatabase() ) {
            // creating new object
            sequence.setId(objectService.createObject(this.typeId));
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.NEW, sequence.getId());
            checkSequence(sequence, parentGroupId);
            executeStatement("insert into cw_core_sequences (id, name, upper_name, object1_id, object2_id, object3_id, start_value, max_value, last_cached_value, cache_length) values (:id, :name, :upper_name, :object1_id, :object2_id, :object3_id, :start_value, :max_value, :last_cached_value, :cache_length)",
                             new Object[]{sequence.getId(), sequence.getNameDatabaseValue(), stringToUpperCase(sequence.getName()), sequence.getObject1IdDatabaseValue(), sequence.getObject2IdDatabaseValue(), sequence.getObject3IdDatabaseValue(), sequence.getStartValueDatabaseValue(), sequence.getMaxValueDatabaseValue(), sequence.getLastCachedValueDatabaseValue(), sequence.getCacheLengthDatabaseValue()});
            
            objectService.addLink(sequence.getObject1Id(), _sequenceToobjectId(sequence));
            objectService.addLink(sequence.getObject2Id(), _sequenceToobjectId(sequence));
            objectService.addLink(sequence.getObject3Id(), _sequenceToobjectId(sequence));
            if ( parentGroupId != null ) { 
             groupService.addObject(parentGroupId, sequence.getId(), true);
            }
            sequence.setFetchedFromDatabase(true);;
        } else {
            // updating record
            backupObject = fetchSequence(sequence.getId());
            if ( backupObject.equals(sequence) ) { return;} 
            transactionAction = getSession().newObjectTransactionAction(TransactionActionType.CHANGE, sequence.getId());
            checkSequence(sequence, parentGroupId);
            executeStatement("update cw_core_sequences set  name=:name,upper_name=:upper_name, object1_id=:object1_id, object2_id=:object2_id, object3_id=:object3_id, start_value=:start_value, max_value=:max_value, last_cached_value=:last_cached_value, cache_length=:cache_length where id = :id", new Object[]{ sequence.getNameDatabaseValue(),stringToUpperCase(sequence.getName()), sequence.getObject1IdDatabaseValue(), sequence.getObject2IdDatabaseValue(), sequence.getObject3IdDatabaseValue(), sequence.getStartValueDatabaseValue(), sequence.getMaxValueDatabaseValue(), sequence.getLastCachedValueDatabaseValue(), sequence.getCacheLengthDatabaseValue(), sequence.getId()});
            
            objectService.changeLinkParent(backupObject.getObject1Id(),sequence.getObject1Id(), _sequenceToobjectId(sequence));
            objectService.changeLinkParent(backupObject.getObject2Id(),sequence.getObject2Id(), _sequenceToobjectId(sequence));
            objectService.changeLinkParent(backupObject.getObject3Id(),sequence.getObject3Id(), _sequenceToobjectId(sequence));;
        }
        saveAuditSequence(backupObject, sequence, transactionAction);
        handleObjectChange(sequence.getId());
        
        SequenceTransactionActionContext transactionActionContext = new SequenceTransactionActionContext();
        transactionActionContext.setTransactionAction(transactionAction);
        transactionActionContext.setNewSequence(sequence);
        transactionActionContext.setOldSequence(backupObject);
        SequenceServiceHandlerContext serviceHandlerContext = new SequenceServiceHandlerContext();
        serviceHandlerContext.setTypeService(this);
        serviceHandlerContext.setTypeId(this.typeId);
        serviceHandlerContext.setMethodName("saveSequence");
        serviceHandlerContext.setTransactionActionContext(transactionActionContext);
        for (AbstractSequenceServiceHandler handler : getImmediateServiceHandlers()) {
            handler.execute(serviceHandlerContext);
        }
        
        resolveByNameCache.clear();
        serviceHandlerBase.addToHandlersQueue(serviceHandlerContext);
    }

    public SequenceObject newSequence()throws CoreException{
        return newSequence(localListGroupId);
    }

    public SequenceObject newSequence( BigDecimal parentGroupId )throws CoreException{
        SequenceObject obj = new SequenceObject();
        obj._setService(this);
        obj.fillDefaultValues(getSession());
        obj.setSkipNotNullCheck(true);
        try {
            saveSequence(obj, parentGroupId);
        } catch (CoreException e) {
            obj.setSkipNotNullCheck(false);
            throw e;
        }
        obj.setSkipNotNullCheck(false);
        return obj;
    }

    public void delSequence( BigDecimal id )throws CoreException{
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        SequenceObject backupObject;
        backupObject = fetchSequence(id);
        if ( backupObject==null) {
            throwException("Object @ of type '@' not found", new Object[]{id, "ORG.CLAWIZ.CORE.COMMON.STORAGE.SEQUENCE"});
        }
        DataSet dsValue = executeSelect("select id from cw_core_sequence_values where Sequence_id=?", new Object[]{id});
        while (! dsValue.isEOF()) {
            delValue(dsValue.getBigDecimal(0));
            dsValue.next();
        }
        objectService.deleteObject(id);
        objectService.deleteLink(backupObject.getObject1Id(), _sequenceToobjectId(backupObject));
        objectService.deleteLink(backupObject.getObject2Id(), _sequenceToobjectId(backupObject));
        objectService.deleteLink(backupObject.getObject3Id(), _sequenceToobjectId(backupObject));
        executeStatement("delete from cw_core_sequences where id = :pid", new Object[]{id});
        saveAuditSequence(backupObject, null, transactionAction);
        SequenceTransactionActionContext transactionActionContext = new SequenceTransactionActionContext();
        transactionActionContext.setTransactionAction(transactionAction);
        transactionActionContext.setOldSequence(backupObject);
        SequenceServiceHandlerContext serviceHandlerContext = new SequenceServiceHandlerContext();
        serviceHandlerContext.setTypeService(this);
        serviceHandlerContext.setTypeId(this.typeId);
        serviceHandlerContext.setMethodName("delSequence");
        serviceHandlerContext.setTransactionActionContext(transactionActionContext);
        for (AbstractSequenceServiceHandler handler : getImmediateServiceHandlers()) {
            handler.execute(serviceHandlerContext);
        }
        
        resolveByNameCache.clear();
        serviceHandlerBase.addToHandlersQueue(serviceHandlerContext);
    }

    public SequenceList fetchSequenceList( String whereClause, Object[] parameters )throws CoreException{
        return fetchSequenceList(whereClause, null, parameters);
    }

    public SequenceList fetchSequenceList( String whereClause, String orderByClause, Object[] parameters )throws CoreException{
        SequenceList result = new SequenceList();
        String sql = "select id, name, object1_id, object2_id, object3_id, start_value, max_value, last_cached_value, cache_length from cw_core_sequences";
        if ( whereClause != null && whereClause.length() > 0 ) {
            sql += " where " + whereClause;
        }
        if ( orderByClause != null && orderByClause.length() > 0 ) {
            sql += " order by  " + orderByClause;
        }
        DataSet resultSet = executeSelect(sql, parameters);
        while ( ! resultSet.isEOF() ) {
            SequenceObject object = new SequenceObject(resultSet.getBigDecimal(0), true);
            object._setService(this);
            object.setName(resultSet.getString( 1));
            object.setObject1Id(resultSet.getBigDecimal( 2));
            object.setObject2Id(resultSet.getBigDecimal( 3));
            object.setObject3Id(resultSet.getBigDecimal( 4));
            object.setStartValue(resultSet.getBigDecimal( 5));
            object.setMaxValue(resultSet.getBigDecimal( 6));
            object.setLastCachedValue(resultSet.getBigDecimal( 7));
            object.setCacheLength(resultSet.getBigDecimal( 8));
            result.addSequence(object);
            resultSet.next();
        }
        resultSet.close();
        return result;
    }

    public SequenceList fetchSequenceListByObject1Id( BigDecimal object1Id )throws CoreException{
        return fetchSequenceList("OBJECT1_ID = ?", new Object[]{object1Id});
    }

    public SequenceList fetchSequenceListByObject2Id( BigDecimal object2Id )throws CoreException{
        return fetchSequenceList("OBJECT2_ID = ?", new Object[]{object2Id});
    }

    public SequenceList fetchSequenceListByObject3Id( BigDecimal object3Id )throws CoreException{
        return fetchSequenceList("OBJECT3_ID = ?", new Object[]{object3Id});
    }

    public ValueObject fetchValue( BigDecimal id )throws CoreException{
        ValueObject result;
         
        DataSet resultSet = executeSelect("select Sequence_id, value from cw_core_sequence_values where  id = :id", new Object[]{id});
        if ( resultSet.isEOF() ) { resultSet.close(); return null; } 
        result = new ValueObject(id, true);
        result._setService(this);
        result.setSequenceId(resultSet.getBigDecimal(0));
        result.setValue(resultSet.getBigDecimal( 1));
        resultSet.close();
        return result;
    }

    public ValueServiceHandlerContext fetchValueServiceHandlerContext( BigDecimal scn )throws CoreException{
        ValueServiceHandlerContext    serviceHandlerContext    =  new ValueServiceHandlerContext();
        ValueTransactionActionContext transactionActionContext =  new ValueTransactionActionContext();
        TransactionAction              transactionAction        = new TransactionAction();
        
        serviceHandlerContext.setTypeId(this.typeId);
        serviceHandlerContext.setTypeService(this);
        serviceHandlerContext.setTransactionActionContext(transactionActionContext);
        
        transactionActionContext.setTransactionAction(transactionAction);
        transactionAction.setScn(scn);
        
        ValueObject newValue = new ValueObject();
        ValueObject oldValue = new ValueObject();
        transactionActionContext.setNewValue(newValue);
        transactionActionContext.setOldValue(oldValue);
        
        DataSet resultSet = executeSelect("select id, action_type, n_Sequence_id, o_Sequence_id, n_value, o_value from a_cw_core_sequence_values where scn = ?", new Object[]{scn});
        if ( resultSet.isEOF() ) { resultSet.close(); return null; }
        newValue.setId(resultSet.getBigDecimal(0));
        oldValue.setId(resultSet.getBigDecimal(0));
        transactionAction.setType(TransactionActionType.toTransactionActionType(resultSet.getString(1)));
        newValue.setSequenceId(resultSet.getBigDecimal(2));
        oldValue.setSequenceId(resultSet.getBigDecimal(3));
        
        newValue.setValue(resultSet.getBigDecimal( 4));
        oldValue.setValue(resultSet.getBigDecimal( 5));
        resultSet.close();
        return serviceHandlerContext;
    }

    private BigDecimal _valueToobjectId( ValueObject value )throws CoreException{
         
        SequenceObject sequence = fetchSequence(value.getSequenceId());
        return _sequenceToobjectId(sequence);
    }

    private void saveAuditValue( ValueObject oldData,ValueObject newData, TransactionAction transactionAction )throws CoreException{
        ValueObject _old = oldData != null ? oldData : new ValueObject();
        ValueObject _new = newData != null ? newData : new ValueObject();
        executeStatement("insert into a_cw_core_sequence_values (scn, action_type, id , o_Sequence_id, o_value, n_Sequence_id, n_value) values ( ?, ?, ?, ?, ?, ?, ?)", new Object[]{transactionAction.getScn(), transactionAction.getType().toString(), _old.getId() != null ? _old.getId() : _new.getId(), _old.getSequenceId(), _old.getValueDatabaseValue(), _new.getSequenceId(), _new.getValueDatabaseValue()});
    }

    public void saveValue( ValueObject value )throws CoreException{
         
        TransactionAction transactionAction = null;
        ValueObject backupObject = null;
        value._setService(this);
        if( ! value.isFetchedFromDatabase() ) {
            // creating new object
            value.setId(objectService.createBlock(this.typeId));
            transactionAction = getSession().newBlockTransactionAction(TransactionActionType.NEW, value.getId());
            checkValue(value);
            executeStatement("insert into cw_core_sequence_values (id, Sequence_id, cw_core_object_id, value) values (:id,:no_of_parent_block, :objlistno, :value)",
                             new Object[]{value.getId(), value.getSequenceId(), value.getSequenceId(), value.getValueDatabaseValue()});
            
            value.setFetchedFromDatabase(true);;
        } else {
            // updating record
            backupObject = fetchValue(value.getId());
            if ( backupObject.equals(value) ) { return;} 
            transactionAction = getSession().newBlockTransactionAction(TransactionActionType.CHANGE, value.getId());
            checkValue(value);
            executeStatement("update cw_core_sequence_values set  value=:value where id = :id", new Object[]{ value.getValueDatabaseValue(), value.getId()});
            ;
        }
        saveAuditValue(backupObject, value, transactionAction);
        
        ValueTransactionActionContext transactionActionContext = new ValueTransactionActionContext();
        transactionActionContext.setTransactionAction(transactionAction);
        transactionActionContext.setNewValue(value);
        transactionActionContext.setOldValue(backupObject);
        ValueServiceHandlerContext serviceHandlerContext = new ValueServiceHandlerContext();
        serviceHandlerContext.setTypeService(this);
        serviceHandlerContext.setTypeId(this.typeId);
        serviceHandlerContext.setMethodName("saveValue");
        serviceHandlerContext.setTransactionActionContext(transactionActionContext);
        for (AbstractSequenceServiceHandler handler : getImmediateServiceHandlers()) {
            handler.execute(serviceHandlerContext);
        }
        
        resolveBySysSequenceValuesPkCache.clear();
        serviceHandlerBase.addToHandlersQueue(serviceHandlerContext);
    }

    public ValueObject newValue()throws CoreException{
        ValueObject obj = new ValueObject();
        obj._setService(this);
        obj.fillDefaultValues(getSession());
        obj.setSkipNotNullCheck(true);
        try {
            saveValue(obj);
        } catch (CoreException e) {
            obj.setSkipNotNullCheck(false);
            throw e;
        }
        obj.setSkipNotNullCheck(false);
        return obj;
    }

    public void delValue( BigDecimal id )throws CoreException{
        
        TransactionAction transactionAction = getSession().newObjectTransactionAction(TransactionActionType.DELETE, id);
        ValueObject backupObject;
        backupObject = fetchValue(id);
        if ( backupObject==null) {
            throwException("Object @ of type '@' not found", new Object[]{id, "ORG.CLAWIZ.CORE.COMMON.STORAGE.SEQUENCE"});
        }
        objectService.deleteBlock(id);
        executeStatement("delete from cw_core_sequence_values where id = :pid", new Object[]{id});
        saveAuditValue(backupObject, null, transactionAction);
        ValueTransactionActionContext transactionActionContext = new ValueTransactionActionContext();
        transactionActionContext.setTransactionAction(transactionAction);
        transactionActionContext.setOldValue(backupObject);
        ValueServiceHandlerContext serviceHandlerContext = new ValueServiceHandlerContext();
        serviceHandlerContext.setTypeService(this);
        serviceHandlerContext.setTypeId(this.typeId);
        serviceHandlerContext.setMethodName("delValue");
        serviceHandlerContext.setTransactionActionContext(transactionActionContext);
        for (AbstractSequenceServiceHandler handler : getImmediateServiceHandlers()) {
            handler.execute(serviceHandlerContext);
        }
        
        resolveBySysSequenceValuesPkCache.clear();
        serviceHandlerBase.addToHandlersQueue(serviceHandlerContext);
    }

    public ValueList fetchValueList( String whereClause, Object[] parameters )throws CoreException{
        return fetchValueList(whereClause, null, parameters);
    }

    public ValueList fetchValueList( BigDecimal parentId )throws CoreException{
        return fetchValueList("Sequence_id = ?", new Object[]{parentId});
    }

    public ValueList fetchValueList( String whereClause, String orderByClause, Object[] parameters )throws CoreException{
        ValueList result = new ValueList();
        String sql = "select id, Sequence_id, value from cw_core_sequence_values";
        if ( whereClause != null && whereClause.length() > 0 ) {
            sql += " where " + whereClause;
        }
        if ( orderByClause != null && orderByClause.length() > 0 ) {
            sql += " order by  " + orderByClause;
        }
        DataSet resultSet = executeSelect(sql, parameters);
        while ( ! resultSet.isEOF() ) {
            ValueObject object = new ValueObject(resultSet.getBigDecimal(0), true);
            object._setService(this);
            object.setSequenceId(resultSet.getBigDecimal(1));
            object.setValue(resultSet.getBigDecimal( 2));
            result.addValue(object);
            resultSet.next();
        }
        resultSet.close();
        return result;
    }

    public BigDecimal nameToId( String name, BigDecimal object1Id, BigDecimal object2Id, BigDecimal object3Id )throws CoreException{
        BigDecimal result = null;
        result = nameToId(name, object1Id, object2Id, object3Id, null);
        return result;
    }

    public BigDecimal nameToId( String name, BigDecimal object1Id, BigDecimal object2Id, BigDecimal object3Id, BigDecimal objectIdToSkip )throws CoreException{
        BigDecimal result = null;
        if (name == null || object1Id == null || object2Id == null || object3Id == null ) { return null; }
            DataSet resultSet;
            if (objectIdToSkip == null) {
               resultSet = executeSelect("select id from cw_core_sequences where upper_name=:name and object1_id=:object1_id and object2_id=:object2_id and object3_id=:object3_id", new Object[]{StringUtils.toUpperCase(StringUtils.substring(name, 0, 250)), object1Id, object2Id, object3Id});
            } else {
               resultSet = executeSelect("select id from cw_core_sequences where upper_name=:name and object1_id=:object1_id and object2_id=:object2_id and object3_id=:object3_id and id <> :objectIdToSkip", new Object[]{StringUtils.toUpperCase(StringUtils.substring(name, 0, 250)), object1Id, object2Id, object3Id, objectIdToSkip});
            }    
            while ( ! resultSet.isEOF() ) { 
                BigDecimal id = resultSet.getBigDecimal(0);
                if ( securityService == null || securityService.isPermitted(id, securityService.getReadAccessTypeId())) {
                    resultSet.close(); return id;
                }
                resultSet.next();
            }
        resultSet.close(); return null;
    }

    public String idToName( BigDecimal id )throws CoreException{
        String result;
        SequenceObject sequence;
        sequence = fetchSequence(id);
        if (sequence == null) { return null; }
        result = sequence.getName() + " " +objectService.idToString(sequence.getObject1Id()) + " " +objectService.idToString(sequence.getObject2Id()) + " " +objectService.idToString(sequence.getObject3Id());
        return result;
    }

    public BigDecimal resolveByName( String name, BigDecimal object1Id, BigDecimal object2Id, BigDecimal object3Id, boolean autoCreate )throws CoreException{
        
        if ( name == null ) {
            if ( ! autoCreate ) { return null; }
            throwException("Значение 'Name' не определено");
        }
        if ( object1Id == null ) {
            if ( ! autoCreate ) { return null; }
            throwException("Значение 'Object1' не определено");
        }
        if ( object2Id == null ) {
            if ( ! autoCreate ) { return null; }
            throwException("Значение 'Object2' не определено");
        }
        if ( object3Id == null ) {
            if ( ! autoCreate ) { return null; }
            throwException("Значение 'Object3' не определено");
        }
        
        String ___key = name+ "~" + object1Id.toString()+ "~" + object2Id.toString()+ "~" + object3Id.toString();
        Optional isId = resolveByNameCache.get(___key);
        if ( isId != null ) { return (BigDecimal)( isId.orNull() ); }
        BigDecimal id = nameToId(name, object1Id, object2Id, object3Id);
        if ( id == null ) {
            if ( ! autoCreate ) {
                isId = Optional.absent();
            } else {
                SequenceObject o = new SequenceObject();
                o._setService(this);
                o.setName(name);
                o.setObject1Id(object1Id);
                o.setObject2Id(object2Id);
                o.setObject3Id(object3Id);
                saveSequence(o);
                id   = o.getId();
                isId = Optional.of(id);
            }
        
        } else {
            isId = Optional.of(id);
        }
        resolveByNameCache.put(___key, isId);
        return id;
    }

    public String idToString( BigDecimal id )throws CoreException{
        String result = null;
        SequenceObject sequence;
        sequence = fetchSequence(id);
        if (sequence == null) { return null; }
        return result;
    }

    public BigDecimal sysSequenceValuesPkToId( BigDecimal sequenceId,BigDecimal value )throws CoreException{
        BigDecimal result = null;
        result = sysSequenceValuesPkToId(sequenceId, value, null);
        return result;
    }

    public BigDecimal sysSequenceValuesPkToId( BigDecimal sequenceId,BigDecimal value, BigDecimal objectIdToSkip )throws CoreException{
        BigDecimal result = null;
        if (sequenceId == null || value == null ) { return null; }
            DataSet resultSet;
            if (objectIdToSkip == null) {
               resultSet = executeSelect("select id from cw_core_sequence_values where Sequence_id = :Sequence_id and value=:value", new Object[]{sequenceId, value});
            } else {
               resultSet = executeSelect("select id from cw_core_sequence_values where Sequence_id = :Sequence_id and value=:value and id <> :objectIdToSkip", new Object[]{sequenceId, value, objectIdToSkip});
            }    
            while ( ! resultSet.isEOF() ) { 
                BigDecimal id = resultSet.getBigDecimal(0);
                if ( securityService == null || securityService.isPermitted(id, securityService.getReadAccessTypeId())) {
                    resultSet.close(); return id;
                }
                resultSet.next();
            }
        resultSet.close(); return null;
    }

    public BigDecimal resolveBySysSequenceValuesPk( BigDecimal sequenceId,BigDecimal value, boolean autoCreate )throws CoreException{
        
        if ( sequenceId == null ) {
            if ( ! autoCreate ) { return null; }
            throwException("Значение 'sequenceId' не определено");
        }
        if ( value == null ) {
            if ( ! autoCreate ) { return null; }
            throwException("Значение 'Value' не определено");
        }
        
        String ___key = sequenceId.toString()+ "~" + value.toString();
        Optional isId = resolveBySysSequenceValuesPkCache.get(___key);
        if ( isId != null ) { return (BigDecimal)( isId.orNull() ); }
        BigDecimal id = sysSequenceValuesPkToId(sequenceId, value);
        if ( id == null ) {
            if ( ! autoCreate ) {
                isId = Optional.absent();
            } else {
                ValueObject o = new ValueObject();
                o._setService(this);
                o.setSequenceId(sequenceId);
                o.setValue(value);
                saveValue(o);
                id   = o.getId();
                isId = Optional.of(id);
            }
        
        } else {
            isId = Optional.of(id);
        }
        resolveBySysSequenceValuesPkCache.put(___key, isId);
        return id;
    }

    public boolean equalsExistsInGroup( SequenceObject object, BigDecimal parentGroupId )throws CoreException{
        
        return false;
    }

    public boolean equalsExistsInGroup( SequenceObject object, BigDecimal parentGroupId, BigDecimal objectIdToSkip )throws CoreException{
        
        return false;
    }

    public boolean equalsExistsInGroup( BigDecimal id, BigDecimal parentGroupId )throws CoreException{
        return equalsExistsInGroup(fetchSequence(id), parentGroupId);
    }

    public boolean equalsExistsInGroup( BigDecimal id, BigDecimal parentGroupId, BigDecimal objectIdToSkip )throws CoreException{
        return equalsExistsInGroup(fetchSequence(id), parentGroupId, objectIdToSkip);
    }

    public BigDecimal exportUIDElementToId( Element element )throws CoreException{
        
        if ( ! element.getTagName().equalsIgnoreCase("object-uid")) {
            throwException("Wrong call to exportUIDElementToId method");
        }
        ArrayList parentGroups = new ArrayList();
        BigDecimal resultNo = null;
        
        NodeList list = element.getChildNodes();
        for ( int i=0; i < list.getLength(); i++) {
            Node node = list.item(i);
            if( node instanceof Element ) {
                Element nodeElement = (Element) node;
                if ( nodeElement.getTagName().equalsIgnoreCase("group-path")) {
                    String path = uploadService.getElementCData(nodeElement);
                    BigDecimal groupId = groupService.pathToId(path, getLocalListGroupId());
                    parentGroups.add(groupId);
                }
            }
        }
        
        
        
        for(int i=0; i < list.getLength(); i++) 
        {
            
            Node node = list.item(i);
            if( node instanceof Element ) {
                
                Element nodeElement = (Element) node;
                String tag = nodeElement.getTagName();
                
                
                
                ;
                if( tag.equalsIgnoreCase("name") && resultNo == null ) {
                    
                    String xname = null;
                    BigDecimal xobject1_id = null;
                    BigDecimal xobject2_id = null;
                    BigDecimal xobject3_id = null;;
                    
                    NodeList keyFieldList = nodeElement.getChildNodes();
                    for (int fni=0; fni < keyFieldList.getLength(); fni ++) {
                        Node fldNode = keyFieldList.item(fni);
                        if ( fldNode instanceof Element ) {
                            Element fldElement = (Element) fldNode;
                            if ( fldElement.getTagName().equalsIgnoreCase("name")) {
                                xname = uploadService.getElementCData(fldElement);
                            }
                            if ( fldElement.getTagName().equalsIgnoreCase("object1_id")) {
                                xobject1_id  = uploadService.resolveReferencedobjectIdFromElementChild(fldElement);
                            }
                            if ( fldElement.getTagName().equalsIgnoreCase("object2_id")) {
                                xobject2_id  = uploadService.resolveReferencedobjectIdFromElementChild(fldElement);
                            }
                            if ( fldElement.getTagName().equalsIgnoreCase("object3_id")) {
                                xobject3_id  = uploadService.resolveReferencedobjectIdFromElementChild(fldElement);
                            }
                        }
                    };
                     
                    resultNo = nameToId(xname, xobject1_id, xobject2_id, xobject3_id);;
                }
            }
        }
        
        return resultNo;
    }

    public Element exportObjectUIDToElement( BigDecimal id, Document document )throws CoreException{
        
        Element resultElement;
        Element keyElement;
        Element fieldElement;
        Element uidElement;
        Element dataElement;
        resultElement = document.createElement("object-uid");
        resultElement.setAttribute("type", objectService.noToServiceName(id));
        resultElement.setAttribute("guid", objectService.idToGUID(id));
        org.clawiz.core.common.storage.group.GroupObjectList groupObjectsList = groupService.fetchGroupObjectList("object_id = ?", new Object[]{id});
        for ( int i=0; i < groupObjectsList.size(); i++) {
            String groupPath="";
            if ( ! groupObjectsList.getGroupObject(i).getGroupId().equals(this.localListGroupId)) {
                groupPath = groupService.noToPath(groupObjectsList.getGroupObject(i).getGroupId(), this.localListGroupId);
            }
            Element groupPathElement = document.createElement("group-path");
            resultElement.appendChild(groupPathElement);
            groupPathElement.appendChild( document.createCDATASection(groupPath));
        }
        SequenceObject object = fetchSequence(id);
        if ( object == null ) {
         throwException("Object for export UID not found");
        }
        keyElement = document.createElement("name");
        resultElement.appendChild(keyElement);
        fieldElement = document.createElement("name");
        keyElement.appendChild(fieldElement);
        fieldElement.appendChild(document.createCDATASection(object.getName()));
        fieldElement = document.createElement("object1_id");
        keyElement.appendChild(fieldElement);
            uidElement = exportService.getObjectUIDElement(object.getObject1Id(), document);
            if (uidElement != null) {
                fieldElement.appendChild(uidElement);
            }
        fieldElement = document.createElement("object2_id");
        keyElement.appendChild(fieldElement);
            uidElement = exportService.getObjectUIDElement(object.getObject2Id(), document);
            if (uidElement != null) {
                fieldElement.appendChild(uidElement);
            }
        fieldElement = document.createElement("object3_id");
        keyElement.appendChild(fieldElement);
            uidElement = exportService.getObjectUIDElement(object.getObject3Id(), document);
            if (uidElement != null) {
                fieldElement.appendChild(uidElement);
            }
        
        return resultElement;
        
    }

    private BigDecimal saveSequenceElement( Element element )throws CoreException{
        BigDecimal resultNo = null;
        SequenceObject data = new SequenceObject(null);
        NodeList nodes = element.getChildNodes();
        ArrayList<BigDecimal> parentGroups = new ArrayList();
        String objectGUID = element.getAttribute("guid");
        for(int i=0; i < nodes.getLength(); i++) 
        {
            
            Node node = nodes.item(i);
            if( node instanceof Element ) {
                
                Element tagElement = (Element) node;
                String  tag        = tagElement.getTagName();;
                if( tag.equalsIgnoreCase("group-path") ) {
                    
                    String path = uploadService.getElementCData(tagElement);
                    BigDecimal pathGroupId = groupService.pathToId(path, getLocalListGroupId());
                    if (pathGroupId == null) {
                        throwException("Group path not found", new Object[]{path});
                    }
                    parentGroups.add(pathGroupId);
                } else if( tag.equalsIgnoreCase("name") ) {
                    String line = uploadService.getElementCData(tagElement);
                    data.setName(line);;
                } else if( tag.equalsIgnoreCase("object1_id") ) {
                    data.setObject1Id(uploadService.resolveReferencedobjectIdFromElementChild(tagElement));
                    if ( data.getObject1Id() == null ) {
                        uploadService.addToDeferredResolveIdByGuidQueue(element, this, "Sequence", "Object1", tagElement);
                    };
                } else if( tag.equalsIgnoreCase("object2_id") ) {
                    data.setObject2Id(uploadService.resolveReferencedobjectIdFromElementChild(tagElement));
                    if ( data.getObject2Id() == null ) {
                        uploadService.addToDeferredResolveIdByGuidQueue(element, this, "Sequence", "Object2", tagElement);
                    };
                } else if( tag.equalsIgnoreCase("object3_id") ) {
                    data.setObject3Id(uploadService.resolveReferencedobjectIdFromElementChild(tagElement));
                    if ( data.getObject3Id() == null ) {
                        uploadService.addToDeferredResolveIdByGuidQueue(element, this, "Sequence", "Object3", tagElement);
                    };
                } else if( tag.equalsIgnoreCase("start_value") ) {
                    String line = uploadService.getElementCData(tagElement);
                    data.setStartValue(new BigDecimal(line));;
                } else if( tag.equalsIgnoreCase("max_value") ) {
                    String line = uploadService.getElementCData(tagElement);
                    data.setMaxValue(new BigDecimal(line));;
                } else if( tag.equalsIgnoreCase("last_cached_value") ) {
                    String line = uploadService.getElementCData(tagElement);
                    data.setLastCachedValue(new BigDecimal(line));;
                } else if( tag.equalsIgnoreCase("cache_length") ) {
                    String line = uploadService.getElementCData(tagElement);
                    data.setCacheLength(new BigDecimal(line));;
                } else if( tag.equalsIgnoreCase("Value") ) {
                } else {
                    throwException("Unknown tag in data node", new Object[]{tag});;
                }
            }
        }
        if ( parentGroups.size() == 0 ) {
            parentGroups.add(this.getLocalListGroupId());
        }
        BigDecimal id = null;
        id = objectService.GUIDToId(objectGUID);
        data.setId(id);
        if( data.getId() == null ) {
            
            id = nameToId(data.getName(), data.getObject1Id(), data.getObject2Id(), data.getObject3Id());
            if ( id != null ) { data.setId(id); };
        }
        
        for (BigDecimal pgId : parentGroups) {
            checkSequence(data, pgId);
        }
        //Rescan id after check (for cases when in check calculated surrogate search keys);
        if( data.getId() == null ) {
            {
                if( data.getId() == null ) {
                    
                    id = nameToId(data.getName(), data.getObject1Id(), data.getObject2Id(), data.getObject3Id());
                    if ( id != null ) { data.setId(id); };
                }
            }}
        
        if ( data.getId() == null) {
            saveSequence(data, (BigDecimal) parentGroups.get(0));
            resultNo =  data.getId();
        } else {
            SequenceObject dbData = fetchSequence(data.getId());
            dbData.setName(data.getName());
            dbData.setObject1Id(data.getObject1Id());
            dbData.setObject2Id(data.getObject2Id());
            dbData.setObject3Id(data.getObject3Id());
            dbData.setStartValue(data.getStartValue());
            dbData.setMaxValue(data.getMaxValue());
            dbData.setLastCachedValue(data.getLastCachedValue());
            dbData.setCacheLength(data.getCacheLength());
            resultNo = dbData.getId();
            saveSequence(dbData, (BigDecimal) parentGroups.get(0));
        }
        
            for ( int i=0; i < parentGroups.size(); i++) {
                if (    groupService.groupObjectToId(  (BigDecimal) parentGroups.get(i), data.getId() ) == null ) {
                    groupService.addObject((BigDecimal) parentGroups.get(i), data.getId());
                }
            }
            
        
        for (int i=0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            if (node instanceof Element) {
                Element tagElement = (Element) node;
                String  tag        = tagElement.getTagName();
                if ( tag.equalsIgnoreCase("VALUE")) {
                       saveValueElement(tagElement, resultNo);
                }
             }
        }
         
        return resultNo;
    }

    private BigDecimal saveValueElement( Element element,BigDecimal sequenceId )throws CoreException{
        BigDecimal resultNo = null;
        ValueObject data = new ValueObject(null, sequenceId);
        NodeList nodes = element.getChildNodes();
        for(int i=0; i < nodes.getLength(); i++) 
        {
            
            Node node = nodes.item(i);
            if( node instanceof Element ) {
                
                Element tagElement = (Element) node;
                String  tag        = tagElement.getTagName();;
                if( tag.equalsIgnoreCase("value") ) {
                    String line = uploadService.getElementCData(tagElement);
                    data.setValue(new BigDecimal(line));;
                } else {
                    throwException("Unknown tag in data node", new Object[]{tag});;
                }
            }
        }
        BigDecimal id = null;
        if( data.getId() == null ) {
            
            id = sysSequenceValuesPkToId(sequenceId, data.getValue());
            if ( id != null ) { data.setId(id); };
        }
        
        checkValue(data);
        //Rescan id after check (for cases when in check calculated surrogate search keys);
        if( data.getId() == null ) {
            {
                if( data.getId() == null ) {
                    
                    id = sysSequenceValuesPkToId(sequenceId, data.getValue());
                    if ( id != null ) { data.setId(id); };
                }
            }}
        
        if ( data.getId() == null) {
            saveValue(data);
            resultNo =  data.getId();
        } else {
            ValueObject dbData = fetchValue(data.getId());
            dbData.setValue(data.getValue());
            resultNo = dbData.getId();
            saveValue(dbData);
        }
        
         
        return resultNo;
    }

    public BigDecimal uploadExportElement( Element element )throws CoreException{
        BigDecimal resultobjectId;
        resultobjectId = saveSequenceElement(element);
        return resultobjectId;
    }

    public Element exportObjectToElement( BigDecimal id, Document document )throws CoreException{
        
        Element resultElement;
        Element fieldElement = null;
        Element uidElement = null;
        Element dataElement = null;
        {
            {
                
                SequenceObject sequence;
                Element SequenceNode = document.createElement("SEQUENCE");
                sequence = fetchSequence(id);
                if ( sequence == null ) { return null; }
                resultElement = SequenceNode;
                resultElement.setAttribute("type", objectService.noToServiceName(id));
                resultElement.setAttribute("guid", objectService.idToGUID(id));
                org.clawiz.core.common.storage.group.GroupObjectList groupObjectsList = groupService.fetchGroupObjectList("object_id = ?", new Object[]{id});
                for ( int i=0; i < groupObjectsList.size(); i++) {
                    if ( ! groupObjectsList.getGroupObject(i).getGroupId().equals(this.localListGroupId)) {
                        String groupPath = groupService.noToPath(groupObjectsList.getGroupObject(i).getGroupId(), this.localListGroupId);
                        Element groupPathElement = document.createElement("group-path");
                        resultElement.appendChild(groupPathElement);
                        groupPathElement.appendChild( document.createCDATASection(groupPath));
                    }
                }
                if ( sequence.getName() != null ) {
                    fieldElement = document.createElement("NAME");
                    SequenceNode.appendChild(fieldElement);
                    fieldElement.appendChild( document.createCDATASection( sequence.getName() ) );
                }
                if ( sequence.getObject1Id() != null ) {
                    fieldElement = document.createElement("OBJECT1_ID");
                    SequenceNode.appendChild(fieldElement);
                    uidElement = exportService.getObjectUIDElement(sequence.getObject1Id(), document);
                    if (uidElement != null) {
                        fieldElement.appendChild(uidElement);
                    }
                }
                if ( sequence.getObject2Id() != null ) {
                    fieldElement = document.createElement("OBJECT2_ID");
                    SequenceNode.appendChild(fieldElement);
                    uidElement = exportService.getObjectUIDElement(sequence.getObject2Id(), document);
                    if (uidElement != null) {
                        fieldElement.appendChild(uidElement);
                    }
                }
                if ( sequence.getObject3Id() != null ) {
                    fieldElement = document.createElement("OBJECT3_ID");
                    SequenceNode.appendChild(fieldElement);
                    uidElement = exportService.getObjectUIDElement(sequence.getObject3Id(), document);
                    if (uidElement != null) {
                        fieldElement.appendChild(uidElement);
                    }
                }
                if ( sequence.getStartValue() != null ) {
                    fieldElement = document.createElement("START_VALUE");
                    SequenceNode.appendChild(fieldElement);
                    fieldElement.appendChild( document.createCDATASection( sequence.getStartValue().toString() ) );
                }
                if ( sequence.getMaxValue() != null ) {
                    fieldElement = document.createElement("MAX_VALUE");
                    SequenceNode.appendChild(fieldElement);
                    fieldElement.appendChild( document.createCDATASection( sequence.getMaxValue().toString() ) );
                }
                if ( sequence.getLastCachedValue() != null ) {
                    fieldElement = document.createElement("LAST_CACHED_VALUE");
                    SequenceNode.appendChild(fieldElement);
                    fieldElement.appendChild( document.createCDATASection( sequence.getLastCachedValue().toString() ) );
                }
                if ( sequence.getCacheLength() != null ) {
                    fieldElement = document.createElement("CACHE_LENGTH");
                    SequenceNode.appendChild(fieldElement);
                    fieldElement.appendChild( document.createCDATASection( sequence.getCacheLength().toString() ) );
                }
                {
                    {
                        
                        ValueObject value;
                        ValueList valueList = fetchValueList("Sequence_id = :id ", new Object[]{sequence.getId()});
                        for (int valueI =0; valueI<valueList.size();valueI++) {
                        value = valueList.getValue(valueI);
                        Element ValueNode = document.createElement("VALUE");
                        SequenceNode.appendChild(ValueNode);
                        if ( value.getValue() != null ) {
                            fieldElement = document.createElement("VALUE");
                            ValueNode.appendChild(fieldElement);
                            fieldElement.appendChild( document.createCDATASection( value.getValue().toString() ) );
                        }
                        
                        } // for loop;
                    }}
            }}
        return resultElement;
    }

    public BigDecimal resolve( String name, BigDecimal[] objects, boolean autoCreate )throws CoreException{
        
        
                
        String sql = "select id from cw_core_sequences where upper_name=?";
        Object[] parameters = new Object[]{name};
        ArrayList array = new ArrayList(0);
        array.add(name.toUpperCase());
        for ( int i=0; i < objects.length; i++ ){
            sql+= " and object"+i+"_id = :"+(i+2);
            array.add(objects[i]);
        }
        BigDecimal result = executeSelectBigDecimal(sql, array.toArray());
        if ( result != null || ! autoCreate) {
            return result;
        }
        SequenceObject sequence = new SequenceObject(null);
        sequence.setName(name);
        if ( objects.length >= 1) { sequence.setObject1Id(objects[0]);}
        if ( objects.length >= 2) { sequence.setObject1Id(objects[1]);}
        if ( objects.length >= 3) { sequence.setObject1Id(objects[2]);}
        sequence.setCacheLength(new BigDecimal(500));
        saveSequence(sequence);
        fillCache(sequence.getId());
        return sequence.getId();
                
                
    }

    public BigDecimal resolve( String name )throws CoreException{
        
        
                
        return resolve(name, new BigDecimal[]{}, true);
                
                
    }

    public BigDecimal resolve( String name, BigDecimal[] objects )throws CoreException{
        
        
                
        return resolve(name, objects, true);
                
                
    }

    public void fillCache( BigDecimal id )throws CoreException{
        
        SequenceObject sequence = fetchSequence(id);
        
                    
        if (sequence == null) {
            throwException("Sequence not found", new Object[]{id});
        }
        
        BigDecimal count = executeSelectBigDecimal("select count(value) from cw_core_sequence_values where sequence_id = ?", new Object[]{id});
        if ( sequence.getCacheLength().divide(new BigDecimal(5)).compareTo(count) < 0) {
            count = new BigDecimal(0);
        } else {
            count = sequence.getCacheLength().subtract(count);
        }
        
        BigDecimal lastValue = sequence.getLastCachedValue();
        for ( int i=0; i < count.intValue(); i++ ) {
            lastValue = lastValue.add(new BigDecimal(1));
            ValueObject value = new ValueObject(null);
            value.setSequenceId(id);
            value.setValue(lastValue);
                    try {
                        saveValue(value);
                    } catch (Exception e) {
                        e.printStackTrace();
                        break;
                    }
        }
        sequence.setLastCachedValue(lastValue);
        saveSequence(sequence);
            
                    
        saveSequence( sequence ) ;
    }

    public BigDecimal getNextValue( BigDecimal id )throws CoreException{
        
        
                    
        
        
        //todo-- разобраться с локировками забираемых значений  и автоматическим пополнением кэша !
        
        BigDecimal result = executeSelectBigDecimal("select value from cw_core_sequence_values where sequence_id = ? order by value", new Object[]{id});
        if ( result == null) {
            fillCache(id);
            result = executeSelectBigDecimal("select value from cw_core_sequence_values where sequence_id = ? order by value", new Object[]{id});
            if ( result == null ) {
                throwException("Cannot get next value from sequence", new Object[]{id});
            }
        }
        executeStatement("delete from cw_core_sequence_values where sequence_id = ? and value = ?", new Object[]{id, result});
        return result;
                    
                    
    }

    public void clearCache( BigDecimal id )throws CoreException{
        
        
                    
        if ( ! securityService.isPermitted(id, securityService.getWriteAccessTypeId())) {
            throwException("Not permitted to change", new Object[]{id});
        }
        executeStatement("delete from cw_core_sequence_values where sequence_id = ?", new Object[]{id});
                    
                    
    }

    public void returnValue( BigDecimal id, BigDecimal value )throws CoreException{
        
        SequenceObject sequence = fetchSequence(id);
        
                    
            if (sequence == null) {
            throwException("Sequence not found", new Object[]{id});
            }
            ValueObject v = new ValueObject(null);
            v.setSequenceId(id);
            v.setValue(value);
            saveValue(v);
                    
                    
    }
*/

}
